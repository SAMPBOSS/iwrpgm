#pragma dynamic 1000000

#include <a_samp>
#include <core>

//all non-samp includes below
#undef MAX_PLAYERS
#define MAX_PLAYERS 200 //update this with the server slots was 120
#undef MAX_ACTORS
#define MAX_ACTORS 200
#include <dns>
#include <streamer>
#include <a_mysql>
#include <foreach>
#include <sscanf2>
#include <progress> //for fuel bars taken from old script
//#include <mSelection>
#include <irc>
#include <IWPlugin>	//Directories
//#include <timerfix>

#define RELEASENUMBER 		"Revision 77"
#define SERVER_HOST 		"www.iw-rp.com" //This is just here for irc debugging purposes
#define chc-debug false
#define west-debug true

#if chc-debug
	#define debug 1
	#define dbg-callbacks 0
	#define ip_remote_check 0
	#define debugirc 1
	#define streamer_debug 1
#elseif west-debug
	#define debug 1
	#define dbg-callbacks 0
	#define ip_remote_check 0
	#define debugirc 1
	#define streamer_debug 1
#else
	#define debug 0
	#define dbg-callbacks 0
	#define ip_remote_check 0 //1
	#define debugirc 0
	#define streamer_debug 0
#endif

#if debug
	#define VERSION ""RELEASENUMBER"-dev"
#else
	#define VERSION RELEASENUMBER
#endif

#define MODE_NAME "RolePlay | IW:RP "VERSION

#include <YSI\y_commands>
#include <YSI\y_master>
#include <YSI\y_colours>
#include <YSI\y_timers>
#include <dini>

/*
	EDialogBase specifies the start offset of all dialog IDs
*/
enum {
	EAccountDialog_Base = 100,
	EBusiness_Base = EAccountDialog_Base + 100,
	EJob_Base = EBusiness_Base + 100,
	EAdminDialog_Base = EJob_Base + 100,
	EFamilyDialog_Base = EAdminDialog_Base + 100,
	EVIPShopDialog_Base = EFamilyDialog_Base + 100,
	EAccessoriesDialog_Base = EVIPShopDialog_Base + 100,
	ERPDialog_Base = EAccessoriesDialog_Base + 100,
	ETurfDialog_Base = ERPDialog_Base + 100,
	EVehicles_Base = ETurfDialog_Base + 100,
	EFactionsDialog_Base = EVehicles_Base + 100,
	EGovDialog_Base = EFactionsDialog_Base + 100,
	EPaintball_Base = EGovDialog_Base + 100,
	ELockers_Base = EPaintball_Base + 100,
	EHelp_Base = ELockers_Base + 100,
	EGPS_Base = EHelp_Base + 100,
	EKeyBind_Base = EGPS_Base + 100,
	ERacing_Base = EKeyBind_Base + 100,
	EHouseFurniture_Base = ERacing_Base + 100,
	EGunFactory_Base = EHouseFurniture_Base + 100,
	EWallTag_Base = EGunFactory_Base + 100,
	ECarToys_Base = EWallTag_Base + 100,
	EPointDialog_Base = ECarToys_Base + 100,
	EChequeSystem_Base = EPointDialog_Base + 100,
	EWareHouses_Base = EChequeSystem_Base + 100,
	EBackpacks_Base = EWareHouses_Base + 100,
	EContactSystem_Base = EBackpacks_Base + 100,
	EDrugs_Base = EContactSystem_Base + 100,
	EItems_Base = EDrugs_Base + 100,
	EWaste_Base = EItems_Base + 100,
	EDialogBase_End = EWaste_Base + 100,
};
//global variables used within all scripts
new g_mysql_handle;
new levelexp = 4;
new tempstr[128];
new query[1024];
new dialogstr[4500];

#define COLOR_WHITE 			0xFFFFFFAA
#define COLOR_FADE1 			0xE6E6E6E6
#define COLOR_FADE2 			0xC8C8C8C8
#define COLOR_FADE3 			0xAAAAAAAA
#define COLOR_FADE4 			0x8C8C8C8C
#define COLOR_FADE5 			0x6E6E6E6E
#define COLOR_GRAD1 			0xB4B5B7FF
#define COLOR_GRAD2 			0xBFC0C2FF
#define COLOR_GRAD3 			0xCBCCCEFF
#define COLOR_GRAD4 			0xD8D8D8FF
#define COLOR_GRAD5 			0xE3E3E3FF
#define COLOR_GRAD6 			0xF0F0F0FF
#define COLOR_PURPLE 			0xC2A2DAAA
#define COLOR_DBLUE 			0x2641FEAA
#define COLOR_ALLDEPT 			0xFF8282AA
#define COLOR_NEWS 				0xFFA500AA
#define COLOR_OOC 				0xE0FFFFAA
#define COLOR_LIGHTGREEN 		0x7CFC00AA
#define COLOR_MEDIUMAQUA 		0x83BFBFFF
#define COLOR_GREY 				0xAFAFAFAA
#define COLOR_LIGHTBLUE 		0x00BFFFAA
#define COLOR_GREENISHGOLD 		0xCCFFDD56
#define COLOR_LIGHTBLUEGREEN 	0x0FFDD349
#define COLOR_LIGHTCYAN 		0xAAFFCC33
#define COLOR_GREEN 			0x33AA33AA
#define COLOR_NEWS 				0xFFA500AA
#define COLOR_OOC 				0xE0FFFFAA
#define COLOR_BRIGHTRED 		0xFF0000FF
#define COLOR_RED 				0xAA3333AA
#define COLOR_ADMINCMD 			0xAFAFAFAA
#define COLOR_LIGHTRED 			0xFF6347AA
#define COLOR_YELLOW 			0xFFFF00AA
#define COLOR_YELLOW2 			0xFFCC00AA
#define COLOR_LIGHTYELLOW2 		0xF5DEB3AA
#define OBJECTIVE_COLOR 		0x64000064
#define TEAM_GREEN_COLOR 		0xFFFFFFAA
#define TEAM_JOB_COLOR 			0xFFB6C1AA
#define TEAM_HIT_COLOR 			0xFFFFFF00
#define TEAM_BLUE_COLOR 		0x8D8DFF00
#define COLOR_ADD 				0x63FF60AA
#define TEAM_GROVE_COLOR 		0x00D900C8
#define TEAM_VAGOS_COLOR 		0xFFC801C8
#define TEAM_BALLAS_COLOR 		0xD900D3C8
#define TEAM_AZTECAS_COLOR 		0x01FCFFC8
#define TEAM_CYAN_COLOR 		0xFF8282AA
#define COLOUR_DONATEPURPLE 	0xFF33FFAA
#define COLOR_PINK 				0xFF66FFAA
#define COLOR_HELPERCHAT 		0x40808096
#define COLOR_CUSTOMGOLD 		0xF7EEB5AA
#define COLOR_DARKGREEN			0x627D62FF

#define DP_VALUE 				0.10 //value of 1 DP in real dollars
#define MAX_HEALTH 				98.0
#define MAX_ARMOUR 				98.0
#define ENTEREXIT_COOLDOWN 		2
#define NAMETAG_DRAW_DISTANCE 	15.0
#define INFINITE_AMMO 			22767
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#define KEY_AIM     (128) //Defaults to SPACEBAR but it works as the aim key as well.
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#define HOLDING(%0) \
	((newkeys & (%0)) == (%0))	//Holding key definition
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
/* General if defines used to tell the compiler to enable / disable modules of code (So we don't need to modify the script after) */
#define BUYGUN_LIMIT			1
#define BIKE_RESTRICTION		1
#define MINIMUM_GUN_LEVEL		2 //Gun level restriction everyone below this level cannot get any guns
#define DECIDEBAN_ONAJAIL		0
/* End of defines (1 - Enabled, 0 - Disabled) */
enum ELicenseFlags (<<= 1) {
	ELicense_Drivers = 1,
	ELicense_Flying,
	ELicense_Fishing,
	ELicense_Sailing,
	ELicense_Gun,
	ELicense_IsTempGunLic,
};

enum EAccountFlags (<<= 1) {
	EAccountFlags_NoNewb = 1,
	EAccountFlags_NoAds,
	EAccountFlags_NoOOC,
	EAccountFlags_HideAdminMessages,
	EAccountFlags_NoHints,
	EAccountFlags_NoJobMessages,
	EAccountFlags_AdBanned,
	EAccountFlags_NoChatAnim,
	EAccountFlags_FurnitureBanned,
	EAccountFlags_MustRedoTest,
	EAccountFlags_MovieMode,
	EAccountFlags_ShowRedScreen,
	EAccountFlags_BlockPMS,
	EAccountFlags_NoHelperChat,
	EAccountFlags_NoHungerTD,
	EAccountFlags_NoFamilyChat,
	EAccountFlags_FToEnterExit,
	EAccountFlags_IsBikeRestricted,
	EAccountFlags_IsMaskRestricted,
};

enum FactionType {
	EFactionType_None,
	EFactionType_LEO,
	EFactionType_Government,
	EFactionType_SanNews,
	EFactionType_EMS,
	EFactionType_Education,
	EFactionType_Transport,
	EFactionType_WasteMgmt,
};

enum ESettingsInfo {
	SettingsName[64],
	EAccountFlags:SettingsFlag,
}
new SettingsInfo[][ESettingsInfo] = {{"No Newb Chat",EAccountFlags_NoNewb},
									{"No Ads/News",EAccountFlags_NoAds},
									{"No Hints",EAccountFlags_NoHints},
									{"Hide Admin Messages",EAccountFlags_HideAdminMessages},
									{"No Job Messages",EAccountFlags_NoJobMessages},
									{"No Chat Animations",EAccountFlags_NoChatAnim},
									{"Movie Mode",EAccountFlags_MovieMode},
									{"No PM's",EAccountFlags_BlockPMS},
									{"No Helper Chat",EAccountFlags_NoHelperChat},
									{"No Hunger Text Draw",EAccountFlags_NoHungerTD},
									{"No Family Chat",EAccountFlags_NoFamilyChat},
									{"Disable the 'F' key to enter or exit interiors",EAccountFlags_FToEnterExit}
									};
									
									
//functions with tag definitions(to prevent compiler reparse(increasing compile time))
Float:getLockDistance(ELockType:lock); //vehicles.pwn
FactionType:getFactionType(faction); //factions.pwn
FactionType:getPlayerFactionType(playerid);
ESafeItemType:findItemType(index); //lockers.pwn
Float:getPlantLength(i); //plants.pwn
HouseHasSafe(id); //safes.pwn
DeleteHouseSafe(id); //safes.pwn
ESafeOwnerType:getSafeOwnerType(safeid); //safes.pwn
SendRadioMsgToListeners(sender, msg[]);//specialitems.pwn
SendDepartmentMsgToListeners(sender, msg[]);//specialitems.pwn
SendWTMsgToListeners(sender, msg[], channel); //specialitems.pwn

#include "wcrp\utils.pwn"
#include "wcrp\generaltextdraws\generaltextdraws.pwn"
#include "wcrp\checkpoints.pwn"
#include "wcrp\colourcodes.pwn"
#include "wcrp\modelpreview.pwn"
#include "wcrp\admin.pwn"
#include "wcrp\account.pwn" //account/character related functions get forwarded here
#include "wcrp\weather.pwn"
#include "wcrp\hunger.pwn"
#include "wcrp\rp.pwn" //generic RP related stuff here(such as /me, /do)
#include "wcrp\anticheat.pwn"
#include "wcrp\radio.pwn"
#include "wcrp\vehicles.pwn"
#include "wcrp\fishing.pwn"
#include "wcrp\business.pwn"
//jobs was here
#include "wcrp\itemdropping.pwn"
#include "wcrp\interiors.pwn"
#include "wcrp\families.pwn"
#include "wcrp\houses.pwn"
#include "wcrp\drugs.pwn" //Everything that is related with the new drug system is here
#include "wcrp\jobs.pwn"
#include "wcrp\safes.pwn"
#include "wcrp\turfs.pwn"
#include "wcrp\VIP.pwn"
#include "wcrp\accessories.pwn"
#include "wcrp\hospital.pwn"
#include "wcrp\factions.pwn"
#include "wcrp\mapping.pwn" //generic NON-DONATOR mapping
#include "wcrp\anims.pwn"
#include "wcrp\gov.pwn"
#include "wcrp\training.pwn"
#include "wcrp\paintball.pwn"
#include "wcrp\boxing.pwn"
#include "wcrp\cameras.pwn"
#include "wcrp\lockers.pwn"
#include "wcrp\IRC.pwn"
#include "wcrp\seatbelt.pwn"
#include "wcrp\anticbug.pwn"
#include "wcrp\help.pwn"
#include "wcrp\adminirc.pwn"
#include "wcrp\logging.pwn"
#include "wcrp\npc.pwn"
#include "wcrp\GPS.pwn"
//radio.pwn was here
#include "wcrp\keybinder.pwn"
//#include "wcrp\donatormapping.pwn"
#include "wcrp\plants.pwn"
#include "wcrp\forumsync.pwn"
#include "wcrp\warehouses.pwn"
#include "wcrp\bomb.pwn"
#include "wcrp\disease.pwn"
//weather was here
#include "wcrp\racing.pwn"
#include "wcrp\externalfuncs.pwn" //functions for use with CallRemoteFunction in other scripts
#include "wcrp\lotto.pwn"
#include "wcrp\housefurniture.pwn"
#include "wcrp\evidencesystem.pwn" //Must be before the damage system
#include "wcrp\damagesystem.pwn"
#include "wcrp\payphones.pwn" //Payphones
#include "wcrp\apartments.pwn" //Apartments
#include "wcrp\gunfactory.pwn" //Gun factory, including dialogs
#include "wcrp\servermottos.pwn" //Server auto namechanger
#include "wcrp\walltags.pwn" //Wall tags (families only)
#include "wcrp\specialitems.pwn"
#include "wcrp\votekick.pwn"
#include "wcrp\speedtraps.pwn"
#include "wcrp\fpscamplusdb.pwn"
#include "wcrp\sqlqueries.pwn"
#include "wcrp\cartoys.pwn"
#include "wcrp\basketball.pwn"
#include "wcrp\materials.pwn"
#include "wcrp\floodcontrol.pwn"
#include "wcrp\cheques.pwn"
#include "wcrp\backpacks.pwn"
#include "wcrp\phonecontacts.pwn"
#include "wcrp\flammable.pwn"
#include "wcrp\storageareas.pwn"
/*
Script Documentation:

All Player PVars are below:

Account Related:
AdminLevel(int) - Players Admin
AccountName(string) - Account Name
AccountID(int) - mysql Account ID
*/

new UsedMapIDs[99]; 


main()
{
}

YCMD:commands(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, 0xFF0000AA, "Lists all the commands a player can use.");
    }
    else
    {
        new
            count = Command_GetPlayerCommandCount(playerid);
        for (new i = 0; i != count; ++i)
        {
            SendClientMessage(playerid, 0xFF0000AA, Command_GetNext(i, playerid));
        }
    }
    return 1;
}
public OnGameModeInit()
{
	SetGameModeText(MODE_NAME);
	SendRconCommand("mapname Los Angeles");
	SendRconCommand("language English");
	#if debug
		mysql_debug(1);
	#else 
		mysql_debug(0);
	#endif
	#if streamer_debug
		Streamer_ToggleErrorCallback(true);
	#endif
	enable_mutex(false);
	g_mysql_handle = mysql_connect(dini_Get("iwrp.ini", "sql_host"),dini_Get("iwrp.ini", "sql_user"),dini_Get("iwrp.ini", "sql_database"),dini_Get("iwrp.ini", "sql_password"));
	Command_SetDeniedReturn( false );
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(0);
	ShowPlayerMarkers(0);
	ShowNameTags(1);
	SetNameTagDrawDistance(NAMETAG_DRAW_DISTANCE);
	ManualVehicleEngineAndLights();
	setEntityStreamerDistance(MAXDEF_ENT_STREAM_DISTANCE); //Set the stream distance for the entities (Vehicles need to load after the mapping, otherwise they fall through) 
	acOnGameModeInit();
	jobsOnGameModeInit();
	mappingOnGameModeInit();
	adminOnGameModeInit();
	businessOnGameModeInit();
	VehOnGameModeInit();
	RPOnGameModeInit();
	interiorsOnGameModeInit();
	familiesOnGameModeInit(); //calls pointsOnGameModeInit() after families are loaded
	VIPOnGameModeInit();
	accessoriesOnGameModeInit();
	turfsOnGameModeInit();
	housesOnGameModeInit();
	factionsOnGameModeInit();
	animsOnGameModeInit();
	govOnGameModeInit();
	camsOnGameModeInit();
	seatbeltOnGameModeInit();
	AntiCBugOnGameModeInit();
	accountOnGameModeInit();
	helpOnGameModeInit();
	loggingOnGameModeInit();
	npcOnGameModeInit();
	wareHousesOnGameModeInit();
	weatherOnGameModeInit();
	payphonesOnGameModeInit();
	apartmentsOnGameModeInit();
	gunFactoryOnGameModeInit();
	wallTagsOnGameModeInit();
	fishOnGameModeInit();
	hungerOnGameModeInit();
	damageOnGameModeInit();
	SpeedTrapsOnGameModeInit();
	onServerMottoGameModeInit();
	SQLQueriesOnGameModeInit();
	basketballOnGameModeInit();
	backpacksOnGameModeInit();
	adminIrcInit();
	drugsOnGameModeInit();
	plantsOnGameModeInit(); //Always load the drugs before the plants, otherwise the plant names are "None"
	storageOnGameModeInit();
	evidenceOnGameModeInit(); //Initialize everything to -1. No way to do it compiler level-wise, gotta do it at run-time.
	wasteOnGameModeInit(); //Everything that has to do with waste
	checkpointsOnGameModeInit();
	return 1;
}

public OnGameModeExit()
{
	seatbeltOnGameModeExit();
	loggingOnGameModeExit();
	lottoOnGameModeExit();
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			SendClientMessage(i, COLOR_RED, "SYSTEM: The server is shutting down, you are being kicked to save your information.");
			KickEx(i, "GMX");
		}
	}
	if(g_mysql_handle)
		mysql_close(g_mysql_handle);
	while(mysql_ping() == 1) {
		#emit NOP
	}
	return 1;
}

public OnPlayerRequestClass(playerid, classid)
{
	return 1;
}

public OnPlayerConnect(playerid)
{
	if(IsPlayerNPC(playerid)) {
		SpawnPlayer(playerid);
		return 0;
	}
	#if ip_remote_check
	LookupPlayerIp(playerid);
	#else
	OnPlayerConnectEx(playerid);
	#endif
	return 1;
}
forward OnPlayerConnectEx(playerid);
public OnPlayerConnectEx(playerid) {
	
	if(numUsersOnIP(playerid) > 6) {
		#if ip_remote_check
			KickEx(playerid, "Too many simultaneous sessions");
		#else
			Ban(playerid);
		#endif
		return 0;
	}
	textdrawsOnPlayerConnect(playerid); //Initialize / load textdraws (Do this first)
	pointsOnPlayerConnect(playerid);
	mappingOnPlayerConnect(playerid);
	adminOnPlayerConnect(playerid); //used for checking bans, etc
	accountOnPlayerConnect(playerid); 
	interiorsOnPlayerConnect(playerid);
	animsOnPlayerConnect(playerid);
	seatbeltOnPlayerConnect(playerid);
	AntiCBugOnPlayerConnect(playerid);
	camOnPlayerConnect(playerid);
	//hungerOnPlayerConnect(playerid);
	weatherOnPlayerConnect(playerid);
	return 0;
}



public OnPlayerDisconnect(playerid, reason)
{
	if(IsPlayerNPC(playerid)) {
		return 0;
	}
	paintballOnPlayerDisconnect(playerid, reason);
	vehOnPlayerDisconnect(playerid, reason);
	RPOnPlayerDisconnect(playerid, reason);
	accountOnPlayerDisconnect(playerid, reason);
	jobsOnPlayerDisconnect(playerid, reason);
	trainingOnPlayerDisconnect(playerid, reason);
	boxingOnPlayerDisconnect(playerid, reason);
	govOnPlayerDisconnect(playerid, reason);
	loggingOnPlayerDisconnect(playerid, reason);
	acOnPlayerDisconnect(playerid, reason);
	adminOnPlayerDisconnect(playerid, reason);
	medicOnPlayerDisconnect(playerid, reason);
	racingOnPlayerDisconnect(playerid, reason);
	//deleteRadioURLPVars(playerid);
	payphonesOnPayPhoneDisconnect(playerid, reason);
	damageSystemOnPlayerDisconnect(playerid, reason);
	weatherOnPlayerDisconnect(playerid, reason);
	evidenceOnPlayerDisconnect(playerid);
	specialItemsOnPlayerDisconnect(playerid, reason);
	hungerOnPlayerDisconnect(playerid, reason);
	fishingOnPlayerDisconnect(playerid, reason);
	voteKickOnPlayerDisconnect(playerid, reason);
	SpeedTrapsOnPlayerDisconnect(playerid, reason);
	fpsCamOnPlayerDisconnect(playerid, reason);
	SQLQueriesOnPlayerDisconnect(playerid, reason);
	basketballOnPlayerDisconnect(playerid, reason);
	rpTestOnPlayerDisconnect(playerid, reason); //destroy the rp test text labels (account.pwn)
	onFloodCheckerDisconnect(playerid, reason); //Remove anything from the flood checker
	textdrawsOnTextDrawDisconnect(playerid, reason); //Remove all textdraws
	materialsOnPlayerDisconnect(playerid, reason); //Used to kill the van timer once a player disconnects
	contactsOnPlayerDisconnect(playerid, reason); //Used to remove any phone contact variable data once a player disconnects
	flammableOnPlayerDisconnect(playerid, reason); //Remove all the player variables related to flammables, etc
	drugsOnPlayerDisconnect(playerid, reason); //Remove all the player variables related to the drug module
	leoOnPlayerDisconnect(playerid, reason); //Tasers, etc
	wasteOnPlayerDisconnect(playerid); //The trash cleaner job is handled here as well
	cpsOnPlayerDisconnect(playerid, reason); //Checkpoints
	return 1;
}

public OnPlayerSpawn(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerSpawn(%d)",playerid);
		printf(query);
	#endif
	if(!IsPlayerNPC(playerid)) {
		if(!IsPlayerConnectEx(playerid)) {
			Kick(playerid);
		}
	}
	PreloadAnimLib(playerid,"MISC");
	PreloadAnimLib(playerid,"ped");
	PreloadAnimLib(playerid,"BEACH");
	PreloadAnimLib(playerid,"SMOKING");
	PreloadAnimLib(playerid,"BOMBER");
	PreloadAnimLib(playerid,"RAPPING");
	PreloadAnimLib(playerid,"SHOP");
	PreloadAnimLib(playerid,"COP_AMBIENT");
	PreloadAnimLib(playerid,"FOOD");
	PreloadAnimLib(playerid,"ON_LOOKERS");
	PreloadAnimLib(playerid,"SWEET");
	PreloadAnimLib(playerid,"DEALER");
	PreloadAnimLib(playerid,"KNIFE");
	PreloadAnimLib(playerid,"CRACK");
	PreloadAnimLib(playerid,"BLOWJOBZ");
	PreloadAnimLib(playerid,"PARK");
	PreloadAnimLib(playerid,"GYMNASIUM");
	PreloadAnimLib(playerid,"PAULNMAC");
	PreloadAnimLib(playerid,"CAR");
	PreloadAnimLib(playerid,"GANGS");
	PreloadAnimLib(playerid,"GHANDS");
	PreloadAnimLib(playerid,"MEDIC");
	PreloadAnimLib(playerid,"Attractors");
	PreloadAnimLib(playerid,"HEIST9");
	PreloadAnimLib(playerid,"RIOT");
	PreloadAnimLib(playerid,"CARRY");
	PreloadAnimLib(playerid,"KISSING");
	PreloadAnimLib(playerid,"INT_SHOP");
	PreloadAnimLib(playerid,"WUZI");
	PreloadAnimLib(playerid,"SWORD");
	PreloadAnimLib(playerid,"LOWRIDER");
	PreloadAnimLib(playerid,"FREEWEIGHTS");
	PreloadAnimLib(playerid,"BENCHPRESS");
	PreloadAnimLib(playerid,"CAR_CHAT");
	acOnPlayerSpawn(playerid);
	accountOnPlayerSpawn(playerid); //for HP setting, etc
	hospitalOnPlayerSpawn(playerid);
	turfsOnPlayerSpawn(playerid);
	initializeTextDraws(playerid); //Show some TD's
	return 1;
}

public OnPlayerDeath(playerid, killerid, reason)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerDeath(%d,%d,%d)",playerid,killerid, reason);
		printf(query);
	#endif
	if(!isFloodingPlayerDeaths(playerid)) {
		adminOnPlayerDeath(playerid, killerid, reason);
		hospitalOnPlayerDeath(playerid, killerid, reason);
		turfsOnPlayerDeath(playerid, killerid, reason);
		trainingOnPlayerDeath(playerid);
		seatbeltOnPlayerDeath(playerid, killerid, reason);
		loggingOnPlayerDeath(playerid, killerid, reason);
		boxingOnPlayerDeath(playerid, killerid, reason);
		racingOnPlayerDeath(playerid, killerid, reason);
		rpOnPlayerDeath(playerid, killerid, reason);
		damageSystemOnPlayerDeath(playerid, killerid, reason);
		backpacksOnPlayerDeath(playerid);
	}
	return 1;
}

public OnVehicleSpawn(vehicleid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehicleSpawn(%d)",vehicleid);
		printf(query);
	#endif
	vehOnVehicleSpawn(vehicleid);
	return 1;
}

public OnVehicleDeath(vehicleid, killerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehicleDeath(%d,%d)",vehicleid, killerid);
		printf(query);
	#endif
	vehOnVehicleDeath(vehicleid, killerid);
	return 1;
}

public OnPlayerText(playerid, text[])
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerText(%d,\"%s\")",playerid, text);
		printf(query);
	#endif
	if(!IsPlayerConnectEx(playerid)) {
		return 0;
	}
	new ret = adminsOnPlayerText(playerid, text);
	if(ret == 2) {
		return 1;
	} else if(ret == 1) {
		return 0;
	}
	if(isMuted(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Unbannable) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are muted!");
		return 0;
	}
	newsOnPlayerText(playerid, text);
	RPOnPlayerText(playerid, text);
	return 0;
}


public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEnterVehicle(%d,%d,%d)",playerid, vehicleid, ispassenger);
		printf(query);
	#endif
	vehOnPlayerEnterVehicle(playerid, vehicleid, ispassenger);
	acOnPlayerEnterVehicle(playerid, vehicleid, ispassenger);
	ctvOnPlayerEnterVehicle(playerid, vehicleid, ispassenger);
	return 1;
}

public OnPlayerExitVehicle(playerid, vehicleid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerExitVehicle(%d,%d)",playerid, vehicleid);
		printf(query);
	#endif
	seatbeltOnPlayerExitVehicle(playerid, vehicleid);
	drivebyOnPlayerExitVehicle(playerid, vehicleid);
	return 1;
}

public OnPlayerStateChange(playerid, newstate, oldstate)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerStateChange(%d,%d,%d)",playerid, newstate, oldstate);
		printf(query);
	#endif
	setLastCarPVar(playerid, newstate, oldstate);
	acOnPlayerStateChange(playerid, newstate, oldstate);
	adminOnPlayerStateChange(playerid, newstate, oldstate);
	vehOnPlayerStateChange(playerid, newstate, oldstate);
	trainingOnPlayerStateChange(playerid, newstate, oldstate);
	govOnPlayerStateChange(playerid, newstate, oldstate);
	seatbeltOnPlayerStateChange(playerid, newstate, oldstate);
	racingOnPlayerStateChange(playerid, newstate, oldstate);
	drivebyOnPlayerStateChange(playerid, newstate, oldstate);
	return 1;
}

public OnPlayerEnterCheckpoint(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEnterCheckpoint(%d)",playerid);
		printf(query);
	#endif
	jobOnPlayerEnterCheckpoint(playerid);
	govOnPlayerEnterCheckpoint(playerid);
	leoOnPlayerEnterCheckpoint(playerid);
	ctvOnPlayerEnterCheckpoint(playerid);
	return 1;
}

public OnPlayerEnterDynamicCP(playerid, checkpointid) 
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEnterDynamicCP(%d, %d)",playerid, checkpointid);
		printf(query);
	#endif
	jobOnPlayerEnterDynamicCP(playerid, checkpointid);
	cpsOnPlayerEnterDynamicCP(playerid, checkpointid);
}
public OnPlayerEnterDynamicRaceCP(playerid, checkpointid) 
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEnterDynamicRaceCP(%d, %d)",playerid, checkpointid);
		printf(query);
	#endif
	jobOnPlayerEnterDynamicCP(playerid, checkpointid);
}

public OnPlayerLeaveCheckpoint(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerLeaveCheckpoint(%d)",playerid);
		printf(query);
	#endif
	return 1;
}

public OnPlayerEnterRaceCheckpoint(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEnterRaceCheckpoint(%d)",playerid);
		printf(query);
	#endif
	trainingOnPlayerEnterRacePoint(playerid);
	govOnPlayerEnterCheckpoint(playerid);
	racingOnEnterCheckpoint(playerid);
	return 1;
}

public OnPlayerLeaveRaceCheckpoint(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerLeaveRaceCheckpoint(%d)",playerid);
		printf(query);
	#endif
	return 1;
}

public OnRconCommand(cmd[])
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnRconCommand(\"%s\")",cmd);
		printf(query);
	#endif
	return 1;
}

public OnPlayerRequestSpawn(playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerRequestSpawn(%d)",playerid);
		printf(query);
	#endif
	if(IsPlayerNPC(playerid)) return 1;
	if(!IsPlayerConnectEx(playerid)) {
		return 0;
	}
	return 1;
}

public OnObjectMoved(objectid)
{
	return 1;
}
public OnDynamicObjectMoved(objectid) 
{
	return 1;
}
public OnPlayerObjectMoved(playerid, objectid)
{
	return 1;
}

public OnPlayerPickUpPickup(playerid, pickupid)
{
	return 1;
}

public OnVehicleMod(playerid, vehicleid, componentid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehicleMod(%d,%d,%d)",playerid,vehicleid, componentid);
		printf(query);
	#endif
	acOnVehicleMod(playerid, vehicleid, componentid);
	return 1;
}

public OnVehiclePaintjob(playerid, vehicleid, paintjobid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehiclePaintjob(%d,%d,%d)",playerid, vehicleid, paintjobid);
		printf(query);
	#endif
	vehOnVehiclePaintjob(vehicleid, playerid, paintjobid);
	return 1;
}

public OnVehicleRespray(playerid, vehicleid, color1, color2)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehiclePaintjob(%d,%d,%d,%d)",playerid, vehicleid, color1, color2);
		printf(query);
	#endif
	vehOnVehicleRespray(playerid, vehicleid, color1, color2);
	return 1;
}

public OnPlayerSelectedMenuRow(playerid, row)
{
	return 1;
}

public OnPlayerExitedMenu(playerid)
{
	return 1;
}

public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerInteriorChange(%d,%d,%d)",playerid, newinteriorid, oldinteriorid);
		printf(query);
	#endif
	onInteriorFloodCheck(playerid);
	DynamicStreamerIntCheck(playerid);
	updateSpectators(playerid);
	paintballOnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid);
	return 1;
}

public OnPlayerKeyStateChange(playerid, newkeys, oldkeys)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerKeyStateChange(%d,%d,%d)",playerid, newkeys, oldkeys);
		printf(query);
	#endif
	keybindOnKeyStateChange(playerid, newkeys, oldkeys);
	animsOnKeyStateChange(playerid, newkeys, oldkeys);
	fishOnPlayerKeyState(playerid, newkeys, oldkeys);
	camsOnPlayerKeyState(playerid, newkeys, oldkeys);
	acOnKeyStateChange(playerid, newkeys, oldkeys);
	dbkeysOnPlayerKeyState(playerid, newkeys, oldkeys);
	basketballKeysOnPlayerKeyState(playerid, newkeys, oldkeys);
	textdrawsOnPlayerKeyStateChange(playerid, newkeys, oldkeys);
	damageSystemOnPlayerKeyState(playerid, newkeys, oldkeys);
	flammableOnPlayerKeyState(playerid, newkeys, oldkeys);
	evidenceOnPlayerKeyState(playerid, newkeys, oldkeys);
	if(newkeys & KEY_SECONDARY_ATTACK)
	{
		new time = GetPVarInt(playerid, "EnterExitCooldown");
		new timenow = gettime();
		if(ENTEREXIT_COOLDOWN-(timenow-time) > 0 || EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_FToEnterExit) {
			return 1;
		}
		businessTryEnter(playerid);
		interiorTryEnterExit(playerid);
		familiesTryEnterExit(playerid);
		housesTryEnterExit(playerid);
		updateSpectators(playerid);
		factionTryEnterExit(playerid);
		apartmentTryEnterExit(playerid);
		joiningOrLeavingBBall(playerid); //basketball
		warehouseTryEnter(playerid);
		storageareaTryEnterExit(playerid);
		SetPVarInt(playerid, "EnterExitCooldown", gettime());
	}
	if(newkeys & KEY_CTRL_BACK) {
		ShamalTryEnterExit(playerid);
	}
	if(newkeys & KEY_LOOK_BEHIND) {
		vehOnEngineTry(playerid);
	}
	return 1;
}

public OnRconLoginAttempt(ip[], password[], success)
{
	new msg[128];
	if(success) {
		format(msg, sizeof(msg), "AdmWarn: %s logged in via RCON",ip);
	} else {
		format(msg, sizeof(msg), "AdmWarn: %s failed to login via RCON",ip);
	}
	ABroadcast(X11_RED3,msg,EAdminFlags_ServerManager);
	return 1;
}

public OnPlayerUpdate(playerid)
{
	acOnPlayerUpdate(playerid);
	if(IsPlayerConnectEx(playerid)) {
		//should be moved into a local variable
		if(GetPVarType(playerid, "Desynced") != PLAYER_VARTYPE_NONE) {
			return 0;
		}
		rpOnPlayerUpdate(playerid);
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(~aflags & EAdminFlags_AntiCheat) {
			if(AntiCBugOnPlayerUpdate(playerid) == 2) {
				return 0;
			}
		}
		camsOnPlayerUpdate(playerid);
	}
	return 1;
}

public OnPlayerStreamIn(playerid, forplayerid)
{
	RPOnPlayerStreamIn(playerid, forplayerid);
	return 1;
}

public OnPlayerStreamOut(playerid, forplayerid)
{
	return 1;
}

public OnVehicleStreamIn(vehicleid, forplayerid)
{
	vehOnVehicleStreamIn(vehicleid, forplayerid);
	return 1;
}

public OnVehicleStreamOut(vehicleid, forplayerid)
{
	return 1;
}

public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[])
{
	if(dialogid >= EAccountDialog_Base && dialogid < EBusiness_Base) {
		if(accountOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EBusiness_Base && dialogid < EJob_Base) {
		if(bOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EJob_Base && dialogid < EAdminDialog_Base) {
		if(jobOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EAdminDialog_Base && dialogid < EFamilyDialog_Base) {
		if(adminOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EFamilyDialog_Base && dialogid < EVIPShopDialog_Base) {
		if(familyOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EVIPShopDialog_Base && dialogid < EAccessoriesDialog_Base) {
		if(VIPOnDialogResponse(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EAccessoriesDialog_Base && dialogid < ERPDialog_Base) {
		if(accessoriesOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= ERPDialog_Base && dialogid < ETurfDialog_Base) {
		if(RPOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= ETurfDialog_Base && dialogid < EVehicles_Base) {
		if(turfsOnDialogResponse(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EVehicles_Base && dialogid < EFactionsDialog_Base) {
		if(vehOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EFactionsDialog_Base && dialogid < EGovDialog_Base) {
		if(factionsOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}  else if(dialogid >= EGovDialog_Base && dialogid < EPaintball_Base) {
		if(govOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EPaintball_Base && dialogid < ELockers_Base) {
		if(paintballOnDialogResponse(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}else if(dialogid >= ELockers_Base && dialogid < EHelp_Base) {
		if(lockersOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}else if(dialogid >= EHelp_Base && dialogid < EGPS_Base) {
		if(helpOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EGPS_Base && dialogid < EKeyBind_Base) {
		if(gpsOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EKeyBind_Base && dialogid < ERacing_Base) {
		if(keybindOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= ERacing_Base && dialogid < EHouseFurniture_Base) {
		if(RacingOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EHouseFurniture_Base && dialogid < EGunFactory_Base) {
		if(HouseFurnOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EGunFactory_Base && dialogid < EWallTag_Base) {
		if(GunFactoryOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EWallTag_Base && dialogid < ECarToys_Base) {
		if(WallTagOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= ECarToys_Base && dialogid < EPointDialog_Base) {
		if(CarToysOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	} else if(dialogid >= EPointDialog_Base && dialogid < EChequeSystem_Base) {
		//we don't handle responses from point dialogs atm
	} else if(dialogid >= EChequeSystem_Base && dialogid < EWareHouses_Base) {
		if(ChequesOnDialogResponse(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}
	else if(dialogid >= EWareHouses_Base && dialogid < EBackpacks_Base) {
		if(WareHouseOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}
	else if(dialogid >= EBackpacks_Base && dialogid < EContactSystem_Base) {
		if(BackpacksOnDialogResp(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}
	else if(dialogid >= EContactSystem_Base && dialogid < EDrugs_Base) {
		if(phoneCOnDialogResponse(playerid, dialogid, response, listitem, inputtext)) {
			return 1;
		}
	}
	else if(dialogid >= EDrugs_Base && dialogid < EItems_Base) {
		if(DrugsOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	}
	else if(dialogid >= EItems_Base && dialogid < EWaste_Base) {
		if(itemsOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	} else if(dialogid >= EWaste_Base && dialogid < EDialogBase_End) {
		if(wasteOnDialogResponse(playerid, dialogid, response, listitem, inputtext) == 1) {
			return 1;
		}
	}
	//EWallTag_Base EDialogBase_End
	return 0;
}



public OnPlayerClickPlayer(playerid, clickedplayerid, source)
{
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(aflags & EAdminFlags_BasicAdmin) {
		if(!IsPlayerConnectEx(clickedplayerid)) {
			return 0;
		}
		if(source == CLICK_SOURCE_SCOREBOARD && playerid != clickedplayerid) {
			if(GetPVarType(playerid, "SpecX") == PLAYER_VARTYPE_NONE) {
				new Float:X, Float:Y, Float:Z, interior, vw;
				GetPlayerPos(clickedplayerid, X, Y, Z);
				interior = GetPlayerInterior(clickedplayerid);
				vw = GetPlayerVirtualWorld(clickedplayerid);
				SetPlayerPos(playerid, X, Y, Z);
				SetPlayerInterior(playerid, interior);
				SetPlayerVirtualWorld(playerid, vw);
				SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
			} else {
				format(query, sizeof(query), "/spectate player %d",clickedplayerid);
				Command_ReProcess(playerid, query, false);
			}
		}
	}
	return 1;
}
public OnPlayerCommandReceived(playerid, cmdtext[]) {
	if(isFloodingCommands(playerid)) return 0;
	loggingOnPlayerCommand(playerid, cmdtext);
	if(isMuted(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Unbannable) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are muted!");
		return 0;
	}
	if(CheckText(cmdtext) == 0 && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_ServerManager) {
		new msg[128];
		format(msg, sizeof(msg), "AdmWarn: %s[%d] may have just tried to advertise: %s",GetPlayerNameEx(playerid, ENameType_CharName),playerid, cmdtext);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_BasicAdmin);
		return -1;
	}
	if(!IsPlayerNPC(playerid)) {
		if(!IsPlayerConnectEx(playerid)) {
			//they haven't logged in, so don't process the command
			return -1;
		}
	}
	if(adminOnCmdRecieved(playerid, cmdtext) == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be on admin duty to do this command.");
		return 0;
	}
	return 1;
}
public OnPlayerCommandPerformed(playerid, cmdtext[], success)
{
	if(!success) {
		new msg[128];
		format(msg, sizeof(msg), "Unknown command: {FF6347}'%s'{FFFFFF} doesn't exist, try '/help' or '/requesthelp' if you need further assistance.", cmdtext);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	return 1;
}

public OnPlayerPickUpDynamicPickup(playerid, pickupid) {
	businessPickupPickup(playerid, pickupid);
	jobsOnPickupPickup(playerid, pickupid);
	VIPOnPickupPickup(playerid, pickupid);
	RPOnPlayerPickupPickup(playerid, pickupid);
	accessoriesOnPlayerPickupPickup(playerid, pickupid);
	govOnPlayerPickupPickup(playerid, pickupid);
	return 1;
}
public OnPlayerEditAttachedObject(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY, Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ, Float:fScaleX, Float:fScaleY, Float:fScaleZ)
{
	accessoriesOnPlayerEditObject(playerid, response, index, modelid, boneid, fOffsetX, fOffsetY, fOffsetZ, fRotX, fRotY, fRotZ, fScaleX, fScaleY, fScaleZ);
}
public OnQueryError( errorid, error[], callback[], query[], connectionHandle ) {
	#if debug
		new msg[256];
		format(msg,sizeof(msg),"SQL ERROR: %d %s",errorid, error);
		printf(msg);
		ABroadcast(X11_RED, msg, EAdminFlags_All);
	#endif
	return 0;
}
allocMapID() {
	for(new i=0;i<sizeof(UsedMapIDs);i++) {
		if(UsedMapIDs[i] == 0) {
			UsedMapIDs[i] = 1;
			return i;
		}
	}
	return -1;
}
freeMapID(index) {
	if(index > sizeof(UsedMapIDs)) {
		return -1;
	}
	UsedMapIDs[index] = 0;
	return 0;
}
PreloadAnimLib(playerid, animlib[]) {
	ApplyAnimation(playerid,animlib,"null",0.0,0,0,0,0,0,0);
	return;
}
public OnPlayerGiveDamage(playerid, damagedid, Float: amount, weaponid, bodypart) {
	//factionsOnPlayerGiveDamage(playerid, damagedid, amount, weaponid);
	acOnPlayerGiveDamage(playerid, damagedid, amount, weaponid);
}
public OnPlayerTakeDamage(playerid, issuerid, Float: amount, weaponid, bodypart) {
	boxingOnPlayerTakeDamage(playerid, issuerid, amount, weaponid);
	damageSystemOnPlayerTakeDamage(playerid, issuerid, amount, weaponid, bodypart);
}
public OnPlayerWeaponShot(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ) {
    new msg[128];
    #if debug
	    format(msg, sizeof(msg), "Weapon %i fired. hittype: %i hitid: %i pos: %f, %f, %f", weaponid, hittype, hitid, fX, fY, fZ);
	   	printf(msg);
 	#endif
 	evidenceOnPlayerShootWeapon(playerid, weaponid, hittype, hitid, fX, fY, fZ);
 	factionsOnPlayerShootWeapon(playerid, weaponid, hittype, hitid, fX, fY, fZ);
    return 1;
}
public OnPlayerEditObject(playerid, playerobject, objectid, response, Float:fX, Float:fY, Float:fZ, Float:fRotX, Float:fRotY, Float:fRotZ) {
//	accessoriesOnEditObject(playerid, playerobject, objectid, response, fX, fY, fZ, fRotX, fRotY, fRotZ);
	return 1;
}
public OnPlayerSelectObject(playerid, type, objectid, modelid, Float:fX, Float:fY, Float:fZ) {
	return 1;
}
public OnPlayerEditDynamicObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerEditDynamicObject(%d,%d,%d,%f,%f,%f,%f,%f,%f)", playerid, objectid, response, x, y, z, rx, ry, rz);
		printf(query);
	#endif
	safesOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	houseFurnOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	leoOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	walltagsOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	carToysOnEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	itemsOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	wasteOnPlayerEditObject(playerid, objectid, response, x, y, z, rx, ry, rz);
	Streamer_Update(playerid);
}
public OnPlayerSelectDynamicObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerSelectDynamicObject(%d,%d,%d,%f,%f,%f)", playerid, objectid, response, x, y, z);
		printf(query);
	#endif
	walltagsOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	safesOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	houseFurnOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	leoOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	cartoysOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	mappingOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	itemsOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	wasteOnPlayerSelectObject(playerid, objectid, modelid, x, y, z);
	Streamer_Update(playerid);
}
public OnEnterExitModShop(playerid, enterexit, interiorid) {
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnEnterExitModShop(%d,%d,%d)", playerid, enterexit, interiorid);
		printf(query);
	#endif
	acOnPlayerRemoveNitro(playerid);
	return 1;
}
public OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnPlayerClickMap(%d,%f,%f,%f)", playerid, fX, fY, fZ);
		printf(query);
	#endif
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(aflags & EAdminFlags_BasicAdmin) {
		SetPlayerPosFindZ(playerid, fX, fY, fZ); 
		SetPlayerInterior(playerid, 0);
		SetPlayerVirtualWorld(playerid, 0);
		SendClientMessage(playerid, X11_TOMATO_2, "You have been teleported.");
	}
    return 1;
}
public OnUnoccupiedVehicleUpdate(vehicleid, playerid, passenger_seat) { 
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnUnoccupiedVehicleUpdate(%d,%d,%d)", vehicleid, playerid, passenger_seat);
		printf(query);
	#endif
	return 1;
}
public OnVehicleDamageStatusUpdate(vehicleid, playerid)
{
	#if dbg-callbacks
		format(query, sizeof(query), "DBG: OnVehicleDamageStatusUpdate(%d,%d)", vehicleid, playerid);
		printf(query);
	#endif
	vehOnUpdateDamage(vehicleid, playerid);
	return 1;
}
task SystemTimer[1000]() {
	checkAJail();
	//checkAFK();
	checkPayday();
	checkCaptureTheVan();
	JailTimer();
	DiseaseTimer();
	leoSysTimer();
	CheckGates();
	checkCarRadios();
	checkJobsTime();
	checkDrivingTestTimer();
	vehicleMileAgeTimer();
	gpsOnUpdateLocation();
	updateBodies();
	//checkCustomPrivateRadios();
}
task ChargeTimer[15000]() {
	//used for things which msg the user
	doTaxiCharges();
	destroyAllOldEvidence();
	checkDrugEffects();
	//destroybackpacks was here
}
task SellTimer[21600000]() {
	sellInactiveHouses();
	sellInactiveBusinesses();
	sellInactiveStorage();
}
public OnPlayerClickTextDraw(playerid, Text:clickedid) {
	if(GetPVarType(playerid, "ClearedTD") && clickedid == INVALID_TEXT_DRAW) {
		DeletePVar(playerid, "ClearedTD");
		return 1;
	}
	if(mdlprevOnClickTD(playerid, clickedid)) {
		return 1;
	}
	return 0;
}
public OnPlayerClickPlayerTextDraw(playerid, PlayerText:playertextid) {
	if(mdlprevOnClickPlayerTD(playerid, playertextid)) {
		return 1;
	}
	return 0;
}
public OnPlayerEnterDynamicArea(playerid, areaid) {
	weatherOnPlayerEnterDynamicArea(playerid, areaid);
}
public OnPlayerLeaveDynamicArea(playerid, areaid) {
	weatherOnPlayerLeaveDynamicArea(playerid, areaid);
}
#if streamer_debug
public Streamer_OnPluginError(error[]) {
	printf("Streamer [ERROR]: %s");
	return 1;
}
#endif