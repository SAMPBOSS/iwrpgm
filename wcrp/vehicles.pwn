enum {
	EVehiclesDialog_TrunkMenu = EVehicles_Base + 1,
	EVehiclesDialog_TrunkSlotMenu,
	EVehiclesDialog_TrunkPutMenu,
	EVehiclesDialog_TrunkAmount,
	EVehiclesDialog_SellCar,
	EVehiclesDialog_EditAlarmFlags,
	EVehiclesDialog_ShowAlarmMenu,
	EVehiclesDialog_StorageMenu,
};

enum EVehicleType {
	EVehicleType_Uninit,//used to signify uninitalized vehicle type, aka unused slot in array
	EVehicleType_None,
	EVehicleType_Owned,
	EVehicleType_Faction,
	EVehicleType_Family,
	EVehicleType_JobCar,
	EVehicleType_PublicCar,
	EVehicleType_RentCar,
	EVehicleType_CaptureCar,
};

enum ELockType {
	ELockType_Default,
	ELockType_Simple,
	ELockType_Remote,
	ELockType_Advanced,
	ELockType_Satellite,
	ELockType_HighTech,
	ELockType_TitaniumLaser,
	ELockType_BioMetric, 
	ELockType_Impossible, //a lock type which doesn't even allow you to attempt to pick it(not an RP lock, used for tempcars)
};
enum EVehicleFlags (<<= 1)  {
	EVehicleFlags_InsuranceImpound = 1, //locked in the insurance place!
	EVehicleFlags_LSPDImpounded, //locked in LSPD!
	EVehicleFlags_WindowsDown,
	EVehicleFlags_PaintJob, //For some reason the paintjob always bugs so I just work with a flag
	EVehicleFlags_Totaled, //Won't start anymore unless it gets repaired
	EVehicleFlags_WipersOn, //Wipers
	EVehicleFlags_AlarmRunning, //Is the alarm running?
	EVehicleFlags_AlarmActivated, //Is the alarm activated?
	EVehicleFlags_Stored, //Is the vehicle in storage?
	EVehicleFlags_AIRPImpound, //Airport Impound
	EVehicleFlags_HasSirens, //Does the vehicle have sirens or not
	EVehicleFlags_CantBeSold, //When toggled the car can't be sold
};
#define MAX_CAR_TOYS 4
enum EVehicleInfo {
	EVehicleType:EVType,
	EVPlate[32],
	EVColour[2],
	EVLocked,
	EVEngine,
	EVFuel,//0-100 fuel
	EVOwner, //playerid of owner if type is EVehicleType_Owned, otherwise faction/family id
	ELockType:EVLockType, //type of lock on car, 0 = default, 6 = titanium laser
	EVToDelete,
	EVSQLID,
	EVPaintjob,
	EVRadioStation,
	ECarToyObject[MAX_CAR_TOYS],
	EVehicleFlags:EVFlags,
	Float:EVMileAge,
	EVImpoundPrice,
	AlarmFlags:EAlarmFlags,
	EVCreationTime, //Will be just used for the Capture the vehicle thing, that's it
};
new VehicleInfo[MAX_VEHICLES][EVehicleInfo];

enum AlarmFlags (<<= 1) {
	EAlarmFlags_WhiteWire = 1,
	EAlarmFlags_RedWire,
	EAlarmFlags_MainPowerBox,
	EAlarmFlags_AlarmAll = -1,
};

enum EAlarmFlagInfo {
	AlarmFlags:EAlarmFlagID, //flag ID
	EAlarmFlagDesc[64],//description of the alarm flag
};

new AlarmFlagDescription[][EAlarmFlagInfo] = {
	{EAlarmFlags_WhiteWire, "White Wire"},
	{EAlarmFlags_RedWire, "Red Wire"},
	{EAlarmFlags_MainPowerBox, "Main Power Wire"}
};

findFreeCarObjectSlot(carid) {
	for(new i=0;i<MAX_CAR_TOYS;i++) {
		if(VehicleInfo[carid][ECarToyObject][i] == 0){
			return i;
		}
	}
	return -1;
}
#pragma unused findFreeCarObjectSlot
addCarAccessory(carid, model, Float:X, Float:Y, Float:Z, Float:rX, Float:rY, Float:rZ) {
	new slot = findFreeCarObjectSlot(carid);
	if(slot == -1) return 0;
	new objid = CreateObject(model, 0.0,0.0, 0.0,0.0,0.0,0.0,300.0);
	AttachObjectToVehicle(objid, carid, X, Y, Z, rX, rY, rZ);
	return 1;	
}
#pragma unused addCarAccessory
enum ETrunkType {
	ETrunkType_Empty,
	ETrunkType_Gun,
	ETrunkType_Money,
	ETrunkType_Pot,
	ETrunkType_Coke,
	ETrunkType_Meth,
	ETrunkType_MaterialsA,
	ETrunkType_MaterialsB,
	ETrunkType_MaterialsC,
	ETrunkType_SpecialItem,
}
enum EVehTrunkInfo {
	ETrunkType:EVehTrunkType, 
	ETrunkAmount,
};

#define NUMBER_TRUNK_SLOTS 6

new VehicleTrunkInfo[MAX_VEHICLES][NUMBER_TRUNK_SLOTS][EVehTrunkInfo];

enum (<<= 1)
{
	MODEL_CAR = 1,
	MODEL_BOAT,
	MODEL_TRUCK,
	MODEL_PLANE,
	MODEL_HELI,
	MODEL_FLY,
	MODEL_BIKE,
	MODEL_BICYCLE,
	MODEL_AMBULANCE,
	MODEL_BUS,
	MODEL_TAXI,
	MODEL_COPCAR,
	MODEL_TOW,
	MODEL_4SEAT,
	MODEL_2SEAT,
	MODEL_1SEAT,
	MODEL_TRUNK,
	MODEL_TRAILER,
	MODEL_TRASHCOLLECT,
	MODEL_BUYABLE,
	MODEL_RC,
	MODEL_TRAIN,
	MODEL_MRWHOOPEE,
	MODEL_HOTDOG
};

new gModels[212] =
{
    MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //400 - Landstalker
    MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //401 - Bravura
    MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //402 - Buffalo
    MODEL_CAR | MODEL_TRUCK | MODEL_2SEAT, //403 - Linerunner
    MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //404 - Perenniel
    MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //405 - Sentinel
    MODEL_TRUCK | MODEL_1SEAT | MODEL_CAR, //406 - Dumper
    MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //407 -Firetruck
    MODEL_TRUCK | MODEL_2SEAT | MODEL_TRASHCOLLECT | MODEL_CAR, //408 - Trashmaster
    MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //409 -Stretch
    MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //410 - Manana
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_TRUNK, //411 - Infernus
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_TRUNK, //412 - Voodoo
	MODEL_CAR | MODEL_4SEAT | MODEL_BUYABLE | MODEL_TRUNK, //413 - Pony
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_TRUNK, //414 - Mule
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_TRUNK, //415 - Cheetah
	MODEL_AMBULANCE | MODEL_4SEAT | MODEL_CAR, //416 - Ambulance
	MODEL_HELI | MODEL_2SEAT | MODEL_FLY, // 417 - Leviathan
	MODEL_CAR | MODEL_4SEAT | MODEL_BUYABLE, //418 - Moonbeam
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //419 - Esperanto
	MODEL_CAR | MODEL_4SEAT | MODEL_TAXI | MODEL_TRUNK, //420 - Taxi
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //421 - Washington
	MODEL_2SEAT |MODEL_BUYABLE | MODEL_CAR, //422 - Bobcat
	MODEL_2SEAT | MODEL_CAR | MODEL_BUYABLE | MODEL_MRWHOOPEE, //423 - Mr Whoopee
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //424 - BF Injection
	MODEL_HELI | MODEL_1SEAT | MODEL_FLY, //425 - Hunter
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //426 - Premier
	MODEL_TRUCK | MODEL_4SEAT | MODEL_COPCAR | MODEL_CAR, //427 - Enforcer
	MODEL_TRUCK | MODEL_4SEAT | MODEL_CAR, //428 - Securicar
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //429 - Banshee
	MODEL_BOAT | MODEL_1SEAT | MODEL_COPCAR, //430 - Predator
	MODEL_BUS | MODEL_CAR, //431 - Bus
	MODEL_COPCAR | MODEL_1SEAT | MODEL_CAR, //432 - Rhino
	MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //433 - Barracks
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK, //434 - Hotknife
	MODEL_TRAILER, // 435 - Article Triler
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //436 - Previon
	MODEL_BUS | MODEL_CAR, //437 - Coach
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_TAXI, //438 - Cabbie
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //439 - Stallion
	MODEL_CAR | MODEL_4SEAT | MODEL_BUYABLE, //440 - Rumpo
	MODEL_RC, //441 - RC Bandit
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //442 - Romero
	MODEL_TRUCK | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //443 - Packer
	MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //444 - Monster MODEL_BUYABLE
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //445 - Admiral
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //446 - Squallo
	MODEL_HELI| MODEL_2SEAT | MODEL_FLY, //447 - Seasparrow
	MODEL_BIKE | MODEL_1SEAT | MODEL_CAR, //448 - Pizzaboy
	MODEL_TRAIN | MODEL_1SEAT, //449 - Tram
	MODEL_TRAILER, //450 - Article Trailer 2
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //451 - Turismo
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //452 - Speeder
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //453 - Reefer
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //454 - Tropic
	MODEL_TRUCK | MODEL_2SEAT | MODEL_BUYABLE, // 455 - Flatbed
	MODEL_TRUCK | MODEL_2SEAT | MODEL_BUYABLE, //456 - Yankee
 	MODEL_2SEAT, //457 - Caddy
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //458 - Solair
	MODEL_CAR | MODEL_4SEAT, //459 - Berkley's RC Van
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //460 - Skimmer
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //461 - PCJ-600
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //462 - Faggio
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //463 - Freeway
	MODEL_RC, //464 - RC Barron
	MODEL_RC, //465 - RC Raider
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //466 - Glendale
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //467 - Oceanic
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_BIKE, //468 - Sanchez
	MODEL_HELI| MODEL_2SEAT | MODEL_FLY, //469 - Sparrow
	MODEL_4SEAT | MODEL_BUYABLE | MODEL_CAR, //470 - Patriot
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //471 - Quad
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //472 - Coastguard
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //473 - Dinghy
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //474 - Hermes
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //475 - Sabre
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //476 - Rustler
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //477 - ZR-350
	MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //478 - Walton
	MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_CAR, //479 - Regina
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //480 - Comet
	MODEL_BICYCLE | MODEL_1SEAT, //481 - BMX
	MODEL_CAR | MODEL_4SEAT | MODEL_BUYABLE, //482 - Burrito
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //483 - Camper
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //484 - Marquis
	MODEL_CAR | MODEL_1SEAT, //485 - Baggage
	MODEL_TRUCK | MODEL_1SEAT, //486 - Dozer
	MODEL_HELI | MODEL_4SEAT | MODEL_FLY, //487 - Maverick
	MODEL_HELI | MODEL_4SEAT | MODEL_FLY, //488 - SAN News Maverick
	MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_CAR, //489 - Rancher
	MODEL_4SEAT | MODEL_TRUNK | MODEL_CAR | MODEL_COPCAR, //490 - FBI Rancher
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //491 - Virgo
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //492 - Greenwood
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //493 - Jetmax
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK, //494 - Hotring Racer MODEL_BUYABLE
	MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_CAR, //495 - Sandking
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //496 - Blista Compact
	MODEL_HELI | MODEL_4SEAT | MODEL_FLY | MODEL_COPCAR, //497 - Police Maverick
	MODEL_TRUCK | MODEL_4SEAT | MODEL_BUYABLE | MODEL_CAR, //498 - Boxville
	MODEL_TRUCK | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //499 - Benson
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_TRUNK, //500 - Mesa
	MODEL_RC, //501 - RC Goblin
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK, //502 - Hotring Racer
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK, //503 - Hotring Racer
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK, //504 - Bloodring Banger MODEL_BUYABLE
	MODEL_2SEAT | MODEL_TRUNK | MODEL_CAR, //505 - Rancher MODEL_BUYABLE (not buyable cuz it's a duplicate)
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_TRUNK, //506 - Super GT
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_TRUNK, //507 - Elegant
 	MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //508 - Journey
	MODEL_BICYCLE | MODEL_1SEAT, //509 - Bike
	MODEL_BICYCLE | MODEL_1SEAT, //510 - Mountain Bike
	MODEL_PLANE | MODEL_2SEAT | MODEL_FLY, //511 - Beagle
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //512 - Cropduster
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //513 - Stuntplane
	MODEL_CAR | MODEL_TRUCK | MODEL_2SEAT, //514 - Tanker
	MODEL_CAR | MODEL_TRUCK | MODEL_2SEAT, //515 - Roadtrain
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //516 - Nebula
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //517 - Majestic
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //518 - Buccaneer
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //519 - Shamal
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //520 - Hydra
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //521 - FCR-900
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //522 - NRG-500
	MODEL_BIKE | MODEL_2SEAT | MODEL_COPCAR | MODEL_CAR, //523 - HPV1000
	MODEL_TRUCK | MODEL_2SEAT | MODEL_TOW | MODEL_CAR, //524 - Cement Truck
	MODEL_TRUCK | MODEL_2SEAT | MODEL_TOW | MODEL_CAR, //525 - Towtruck
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //526 - Fortune
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //527 - Cardona
	MODEL_CAR | MODEL_COPCAR | MODEL_2SEAT, //528 - FBI Truck
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //529 - Willard
	MODEL_CAR | MODEL_1SEAT, //530 - Forklift
	MODEL_CAR | MODEL_1SEAT | MODEL_TOW, //531 - Tractor
	MODEL_TRUCK | MODEL_1SEAT, //532 - Combine Harvester
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //533 - Feltzer
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //534 - Remington
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //535 - Slamvan
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //536 - Blade
	MODEL_TRAIN | MODEL_1SEAT, //537 Freight (Train)
	MODEL_TRAIN | MODEL_1SEAT, //538 - Brownstreak (Train)
	MODEL_CAR | MODEL_1SEAT, //539 - Vortex
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //540 -Vincent
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //541 - Bullet
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //542 - Clover
 	MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //543 - Sadler
	MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //544 - Firetruck LA
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //545 - Hustler MODEL_BUYABLE
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //546 - Intruder
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //547 - Primo
	MODEL_HELI | MODEL_2SEAT | MODEL_FLY, //548 - Cargobob
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //549 - Tampa
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //550 - Sunrise
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //551 - Merit
	MODEL_TRUCK | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //552 - Utility Van
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //563 - Nevada
 	MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //554 - Yosemite
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //555 - Windsor
	MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //556 - Monster "A"
	MODEL_TRUCK | MODEL_2SEAT | MODEL_CAR, //557 - Monster "B"
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //558 - Uranus
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //559 - Jester
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //560 - Sultan
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //561 - Stratum
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //563 - Elegy
	MODEL_HELI | MODEL_2SEAT, //563 - Raindance
	MODEL_RC, //564 - RC Tiger
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //565 - Flash
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //566 - Tahoma
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //567 - Savanna
	MODEL_CAR | MODEL_1SEAT | MODEL_BUYABLE, //568 - Bandito
	MODEL_TRAILER | MODEL_TRAIN, //569 - Freight Flat Trailer (Train)
	MODEL_TRAIN | MODEL_1SEAT | MODEL_TRAILER, //570 - Streak Trailer (Train)
	MODEL_1SEAT | MODEL_CAR, //571 - Kart MODEL_BUYABLE
	MODEL_1SEAT, //572 - Mower MODEL_BUYABLE
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUCK, //573 - Dune MODEL_BUYABLE
	MODEL_CAR | MODEL_2SEAT | MODEL_TRASHCOLLECT, //574 - Sweeper
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //575 - Broadway
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //576 - Tornado
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //577 - AT400
	MODEL_TRUCK | MODEL_2SEAT |MODEL_CAR, //578 - DFT-30
	MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE | MODEL_CAR, //579 - Huntley
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //580 - Stafford
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //581 - BF-400
	MODEL_CAR | MODEL_4SEAT, //582 - Newsvan
	MODEL_CAR | MODEL_1SEAT, //583 - Tug
	MODEL_TRAILER, //584 - Petrol Trailer
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //585 - Emperor
	MODEL_BIKE | MODEL_2SEAT | MODEL_BUYABLE | MODEL_CAR, //586 - Wayfarer
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //587 - Euros
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE | MODEL_HOTDOG, //588 - Hotdog
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //589 - Club
	MODEL_TRAILER | MODEL_TRAIN, //590 - Freight Box Trailer (Train)
	MODEL_TRAILER, //591 - Article Trailer 3
	MODEL_PLANE | MODEL_1SEAT | MODEL_FLY, //592 - Andromada
	MODEL_PLANE | MODEL_2SEAT | MODEL_FLY, //593 - Dodo
	MODEL_RC, //594 - RC Cam
	MODEL_BOAT | MODEL_1SEAT | MODEL_BUYABLE, //595 - Launch
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_COPCAR, //596 - Police Car (LSPD)
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_COPCAR, //596 - Police Car (SFPD)
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_COPCAR, //597 - Police Car (LVPD)
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_COPCAR, //599 - Police Ranger
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //600 - Picador
	MODEL_TRUCK | MODEL_2SEAT | MODEL_COPCAR | MODEL_CAR, //601 - S.W.A.T
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //602 - Alpha
	MODEL_CAR | MODEL_2SEAT | MODEL_TRUNK | MODEL_BUYABLE, //603 - Phoenix
	MODEL_CAR | MODEL_4SEAT | MODEL_TRUNK | MODEL_BUYABLE, //604 - Glendale Broken
	MODEL_CAR | MODEL_2SEAT | MODEL_BUYABLE, //605 - Sadler Broken
	MODEL_TRAILER, //606 - Baggage Trailer "A"
	MODEL_TRAILER, //607 - Baggage Trailer "B"
	MODEL_TRAILER, //608 - Tug Stairs Trailer
	MODEL_TRUCK | MODEL_4SEAT | MODEL_BUYABLE | MODEL_CAR, //609 - Boxville
	MODEL_TRAILER, //610 - Farm Trailer
	MODEL_TRAILER //611 - Utility Trailer
};

new bannedvehicles[] = {522,520,451};

#define IsATowTruck(%0) \
	((%0 >= 400 && %0 < 611) && gModels[(%0) - 400] & MODEL_TOW))

#define IsACar(%0) \
    (((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_CAR)) || IsATruck(%0))
    
#define IsBuyable(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_BUYABLE))

#define IsAnAmbulance(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_AMBULANCE))
    
#define IsACopCar(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_COPCAR))
    
#define IsATruck(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_TRUCK))
    
#define IsATaxi(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_TAXI))
    
#define IsAPlane(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_PLANE))
    
#define IsAHelicopter(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_HELI))
    
#define IsAFlyingVehicle(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_FLY))
    
#define IsABicycle(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_BICYCLE))
    
#define IsABike(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_BIKE))
    
#define IsABoat(%0) \
    ((%0 >= 400 && %0 < 611) && (gModels[(%0) - 400] & MODEL_BOAT))
    
#define IsAGarbageVehicle(%0) \
    ((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_TRASHCOLLECT))
    
#define IsA2Seat(%0) \
    ((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_2SEAT))
    
#define IsA4Seat(%0) \
    ((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_4SEAT))

#define HasATrunk(%0) \
    ((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_TRUNK))

#define IsA1Seat(%0) \
	((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_1SEAT))

#define IsAMrWhoopee(%0) \
	((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_MRWHOOPEE))

#define IsAHotDog(%0) \
	((%0 >= 400 && %0 <= 611) && (gModels[(%0) - 400] & MODEL_HOTDOG))

forward onPlayerLoadVehicles(playerid);
forward onPlayerVehicleCreate(playerid,model,c1,c2, Float:X, Float:Y, Float:Z, Float:Angle, ELockType:locktype, putincar, cantbesold);
forward onCreateJobVehicle(jobid, model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle, plate[]);
forward onLoadJobVehicles(jobid);
forward UnlockCar(carid);
forward LockCar(carid);
forward CheckGas();
forward SetLights(carid, status);
forward ReloadJobCar(carid);
forward onCreateFamilyVehicle(famid, model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle);
forward LoadFamilyCars(id);
forward onLoadFamilyCar(familyid);
forward ReloadFamilyCar(carid);
forward onCreateFactionVehicle(factionid, model, Float:X, Float:Y, Float:Z, Float:Angle, c1, c2, sirens);
#define GAS_TIME 50000
#define GUI_TIME 100
#define DEF_RESPAWN_DELAY 1200
#define ENGINE_COOLDOWN 5
#define LOCKPICK_COOLDOWN 60
#define DROPCAR_COOLDOWN 1800

new gastimer;

new BadDrivebyWeapons[] = {24,27,28,32};

encode_tires(tire1, tire2, tire3, tire4) {
	return tire1 | (tire2 << 1) | (tire3 << 2) | (tire4 << 3);
}
encode_panels(flp, frp, rlp, rrp, windshield, front_bumper, rear_bumper)
{
	return flp | (frp << 4) | (rlp << 8) | (rrp << 12) | (windshield << 16) | (front_bumper << 20) | (rear_bumper << 24);
}
encode_doors(bonnet, boot, driver_door, passenger_door, behind_driver_door, behind_passenger_door)
{
    #pragma unused behind_driver_door
    #pragma unused behind_passenger_door
    return bonnet | (boot << 8) | (driver_door << 16) | (passenger_door << 24);
}
encode_lights(light1, light2, light3, light4)
{
    return light1 | (light2 << 1) | (light3 << 2) | (light4 << 3);
}
Float:getVehicleMileAge(carid) {
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		return 0.0;
	}
	return Float:VehicleInfo[carid][EVMileAge];
}
vehicleMileAgeTimer() {
	new carid;
	new Float:speed, Float:totalMilesPerSec;
	new model;
	foreach(Player, i) {
		if(GetPlayerState(i) == PLAYER_STATE_DRIVER) {
			carid = GetPlayerVehicleID(i); //Lol, there's no checks for the driver so it'll add extra miles
			if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
				model = GetVehicleModel(carid);
				if(!IsABicycle(model)) {
					speed = GetSpeed(i);
					totalMilesPerSec = (speed*1)/1000;
					VehicleInfo[carid][EVMileAge] += totalMilesPerSec;
				}
			}
		}
	}
	return 1;
}
#pragma unused encode_panels
#pragma unused encode_doors
#pragma unused encode_lights

VehOnGameModeInit() {
	gastimer = SetTimer("CheckGas", GAS_TIME, true);
	for(new i=0;i<GetNumJobs();i++) {
		loadJobVehicles(i);
	}
	loadPublicCars();
	//
	CreateDynamicPickup(1239, 16, -1569.463256, 98.068122, 3.554687);
	CreateDynamic3DTextLabel("(( /dropcar ))", 0x2BB00AA, -1569.463256, 98.068122, 3.554687+1, 50.0);
	#pragma unused gastimer
}
public LoadFamilyCars(id) {
	query[0] = 0;//[512];
	format(query,sizeof(query),"SELECT `id`,`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`locktype`,`plate`,`paintjob`,`comp0`,`comp1`,`comp2`,`comp3`,`comp4`,`comp5`,`comp6`,`comp7`,`comp8`,`comp9`,`comp10`,`comp11`,`comp12` FROM `familycars` WHERE `owner` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "onLoadFamilyCar", "d",id);
}
public onLoadFamilyCar(familyid) {
	new rows,fields,carid;
	new id_string[128],id,plate[32];
	new modelid, c1, c2;
	new Float:X,Float:Y,Float:Z,Float:Angle;
	cache_get_data(rows,fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 1, id_string);//model
		modelid = strval(id_string);
		cache_get_row(i, 2, id_string);//c1
		c1 = strval(id_string);
		cache_get_row(i, 3, id_string);//c2
		c2 = strval(id_string);
		
		cache_get_row(i, 4, id_string);//x
		X = floatstr(id_string);
		cache_get_row(i, 5, id_string);//y
		Y = floatstr(id_string);
		cache_get_row(i, 6, id_string);//z
		Z = floatstr(id_string);
		cache_get_row(i, 7, id_string);//angle
		Angle = floatstr(id_string);
		
		cache_get_row(i, 8, id_string);//lock type
		VehicleInfo[carid][EVLockType] = ELockType:strval(id_string);
		
		
		cache_get_row(i, 0, id_string);//id
		
		id = strval(id_string);
		
		carid = CreateVehicle(modelid,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
		for(new x=11;x-11<13;x++) {
			cache_get_row(i, x, id_string);//paintjob
			new componentid = strval(id_string);
			if(islegalcarmod(modelid, componentid))
				AddVehicleComponent(carid,componentid);
		}
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}

		
		VehicleInfo[carid][EVLocked] = 1;//default to locked on load
		
		syncVehicleParams(carid);
		
		VehicleInfo[carid][EVToDelete] = 0;
		VehicleInfo[carid][EVOwner] = familyid;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVFuel] = 100;
		VehicleInfo[carid][EVSQLID] = id;
		VehicleInfo[carid][EVType] = EVehicleType_Family;
		VehicleInfo[carid][EVRadioStation] = -1;
		VehicleInfo[carid][EVPaintjob] = -1;
		cache_get_row(i, 10, id_string);//paintjob
		new paintjob = strval(id_string);
		if(vehicleHasPaintJob(carid)) {
			VehicleInfo[carid][EVPaintjob] = paintjob;
		}
		cache_get_row(i, 9, plate);//plate
		if(strlen(plate) == 0) {
			format(VehicleInfo[carid][EVPlate],32,"IWRP");
		} else {
			format(VehicleInfo[carid][EVPlate],32,"%s",plate);
		}
		SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
		loadTrunks(carid);
		vehOnLoadPaintJob(carid);
	}
}
YCMD:createfamilycar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a family car");
		return 1;
	}
	new famid, model, c1, c2, lock;
	if (!sscanf(params, "ddD(0)D(0)D(0)", famid, model, c1, c2, lock))
    {
		if(!IsValidFamily(famid)) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "Invalid Family");
			return 1;
		}
		if(model < 400 || model > 611) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "Invalid Vehicle!");
			return 1;
		}
		if(c1 < 0 || c1 > 255 || c2 < 0 || c2 > 255) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "Invalid Model!");
			return 1;
		}	
		new Float:X, Float:Y, Float:Z, Float:Angle;
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, Angle);
		famid = SQLIDFromFamily(famid);
		CreateFamilyCar(famid, model, c1, c2, ELockType:lock, X, Y, Z, Angle);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createfamilycar [familyid] [model] [c1] [c2] [lock]");
	}
	return 1;
}
YCMD:destroyfamilycar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a family car");
		return 1;
	}
	new familyid, carid;
	if(!sscanf(params, "dd", familyid, carid)) {
		if(IsValidFamily(familyid)) {
			familyid = SQLIDFromFamily(familyid);
			DestroyFamilyCar(familyid, carid, 1);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Family Vehicle Destroyed");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "There is no vehicle at this slot");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /destroyfamilycar [famid] [slot]");
	}
	return 1;
}
YCMD:gotocar(playerid, params[], help) {
	new carid;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports a player to a car");
		return 1;
	}
	if (!sscanf(params, "d", carid))
    {
		if(carid < 0 || carid > MAX_VEHICLES || GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_WHITE, "Invalid ID");
			return 1;
		}
		new Float:X,Float:Y,Float:Z,vw;
		GetVehiclePos(carid, X, Y, Z);
		vw = GetVehicleVirtualWorld(carid);
		SetPlayerPos(playerid, X, Y, Z);
		SetPlayerVirtualWorld(playerid, vw);
		SetPlayerInterior(playerid, 0);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "You have been teleported.");
	}
	return 1;
}
YCMD:getcar(playerid, params[], help) {
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(carid < 0 || carid > MAX_VEHICLES || GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_WHITE, "Invalid ID");
			return 1;
		}
		if(isVehicleInImpound(carid) != 0) {
			removeVehicleFromImpound(carid);
		}
		new Float:X,Float:Y,Float:Z,vw;
		GetPlayerPos(playerid, X, Y, Z);
		vw = GetPlayerVirtualWorld(playerid);
		SetVehicleVirtualWorld(carid, vw);
		SetVehiclePos(carid, X, Y, Z);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle teleported.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /getcar [playerid/name]");
	}
	return 1;
}
YCMD:dpc(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists a players cars");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_WHITE, "Error: player not connected");
			return 1;
		}
		new msg[128];
		format(msg,sizeof(msg),"|___%s's Vehicles:___|",GetPlayerNameEx(playa, ENameType_CharName));
		SendClientMessage(playerid, X11_WHITE, msg);
		new numfound = 0;
		new locked[] = "Locked";
		new unlocked[] = "Unlocked";
		new insurance[] = "Insurance";
		new impound[] = "Impounded";
		new airpinsurance[] = "{FF8C00}Airport Insurance";
		new stored[] = "Stored";
		new lockmsg[32];
		new hasinsurance = GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
		for(new i=0;i<sizeof(VehicleInfo);i++) {
			if(VehicleInfo[i][EVOwner] == playa && VehicleInfo[i][EVType] == EVehicleType_Owned) {
				new model =  GetVehicleModel(i);
				if(model == 0) continue;
				if(VehicleInfo[i][EVLocked]) {
					format(lockmsg, sizeof(lockmsg), "%s",locked);
				} else {
					format(lockmsg, sizeof(lockmsg), "%s",unlocked);
				}
				if(hasinsurance) {
					if(VehicleInfo[i][EVFlags] & EVehicleFlags_InsuranceImpound) {
						format(lockmsg, sizeof(lockmsg), "%s", insurance);
					} else if(VehicleInfo[i][EVFlags] & EVehicleFlags_AIRPImpound) {
						format(lockmsg, sizeof(lockmsg), "%s", airpinsurance);
					}
				}
				if(VehicleInfo[i][EVFlags] & EVehicleFlags_LSPDImpounded) {
					format(lockmsg, sizeof(lockmsg), "%s", impound);
				}
				if(VehicleInfo[i][EVFlags] & EVehicleFlags_Stored) {
					format(lockmsg, sizeof(lockmsg), "%s", stored);
				}
				format(msg, sizeof(msg), "%d: {ADD8E6}%s{FFFFFF}  | Lock: {ADD8E6}%s{FFFFFF} %s (Model ID: %d | ID: %d | SQLID: %d)"
				, ++numfound, VehiclesName[model-400], LockName(VehicleInfo[i][EVLockType]), lockmsg, model, i, VehicleInfo[i][EVSQLID]);
				SendClientMessage(playerid, X11_WHITE, msg);
			}
		}
		format(msg,sizeof(msg),"Number of cars: %d | Vehicle Limit: %d",numfound,GetPVarInt(playa, "MaxCars"));
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /dpc [playerid/name]");
	}
	return 1;
}
getNumOwnedCars(playerid) {
	new num;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == playerid && VehicleInfo[i][EVType] == EVehicleType_Owned) {
			num++;
		}
	}
	return num;
}
getFamilyCars(famid) {
	new num;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == famid && VehicleInfo[i][EVType] == EVehicleType_Family) {
			num++;
		}
	}
	return num;
}
CMD:getfuel(playerid, params[]) {
	new vehid;
	new msg[64];
	if(!sscanf(params, "d", vehid)) {
		if(GetVehicleModel(vehid) == 0) {
			SendClientMessage(playerid, X11_RED2, "Invalid Vehicle.");
			return 1;
		}
		format(msg, sizeof(msg), "* Vehicle Fuel: %d",VehicleInfo[vehid][EVFuel]);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /getfuel [carid]");
	}
	return 1;
}
CMD:setfuel(playerid,params[]) {
	new vehid,fuel;
	if(!sscanf(params, "dd", vehid,fuel)) {
		if(GetVehicleModel(vehid) == 0) {
			SendClientMessage(playerid, X11_RED2, "Invalid Vehicle.");
			return 1;
		}
		VehicleInfo[vehid][EVFuel] = fuel;
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Fuel Set!");
    	CheckGas();
	} else if(!sscanf(params, "d", fuel)) {
		if(IsPlayerInAnyVehicle(playerid)) {
			VehicleInfo[GetPlayerVehicleID(playerid)][EVFuel] = fuel;
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Fuel Set!");
    		CheckGas();
	   	}
	}
	return 1;
}
CMD:fuelcars(playerid,params[]) {
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(GetVehicleModel(i)) {
			VehicleInfo[i][EVFuel] = 100;
		}
	}
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicles Fueled!");
	CheckGas();
	return 1;
}
YCMD:destroycar(playerid, params[], help) {
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED3, "Invalid Vehicle");
			return 1;
		}
	} else {
		carid = GetPlayerVehicleID(playerid);
		if(carid == INVALID_VEHICLE_ID) {
			SendClientMessage(playerid, X11_WHITE, "USAGE: /destroycar [id]");
			SendClientMessage(playerid, X11_WHITE, "Alternatively, if you are inside a car /destroycar will destroy it without the ID");
			return 1;
		}		
	}
	if(VehicleInfo[carid][EVType] != EVehicleType_None) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this command on this type of car!");
		return 1;
	}
	//DestroyVehicle(carid);
	vehOnVehicleDeath(carid, INVALID_PLAYER_ID); //OnVehicleDeath isn't called when DestroyVehicle is called
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	return 1;
}
YCMD:respawncar(playerid, params[], help) {
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED3, "Invalid Vehicle");
			return 1;
		}
	} else {
		carid = GetPlayerVehicleID(playerid);
		if(carid == INVALID_VEHICLE_ID) {
			SendClientMessage(playerid, X11_WHITE, "USAGE: /respawncar [id]");
			SendClientMessage(playerid, X11_WHITE, "Alternatively, if you are inside a car /respawncar will respawn it without the ID");
			return 1;
		}		
	}
	SetVehicleToRespawn(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	return 1;
}
YCMD:fixcar(playerid, params[], help) {
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED3, "Invalid Vehicle");
			return 1;
		}
	} else {
		carid = GetPlayerVehicleID(playerid);
		if(carid == INVALID_VEHICLE_ID) {
			SendClientMessage(playerid, X11_WHITE, "USAGE: /fixcar [id]");
			SendClientMessage(playerid, X11_WHITE, "Alternatively, if you are inside a car /fixcar will fix it without the ID");
			return 1;
		}		
	}
	RepairVehicle(carid);
	setVehicleTotaled(carid, 0);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	return 1;
}
YCMD:mycars(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists a players cars");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "|___Your vehicles:___|");
	new numfound = 0;
	new msg[128];
	new locked[] = "{FF6347}Locked";
	new unlocked[] = "{ADD8E6}Unlocked";
	new insurance[] = "{FF8C00}Insurance";
	new airpinsurance[] = "{FF8C00}Airport Insurance";
	new impound[] = "{A020F0}Impounded";
	new stored[] = "{A020F0}Stored";
	new hasinsurance = GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
	new lockmsg[32];
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == playerid && (VehicleInfo[i][EVType] == EVehicleType_Owned || VehicleInfo[i][EVType] == EVehicleType_RentCar)) {
			new model = GetVehicleModel(i);
			if(model != 0) {
				if(VehicleInfo[i][EVLocked]) {
					format(lockmsg, sizeof(lockmsg), "%s",locked);
				} else {
					format(lockmsg, sizeof(lockmsg), "%s",unlocked);
				}
				if(hasinsurance) {
					if(VehicleInfo[i][EVFlags] & EVehicleFlags_InsuranceImpound) {
						format(lockmsg, sizeof(lockmsg), "%s", insurance);
					} else if(VehicleInfo[i][EVFlags] & EVehicleFlags_AIRPImpound) {
						format(lockmsg, sizeof(lockmsg), "%s", airpinsurance);
					}
				}
				if(VehicleInfo[i][EVFlags] & EVehicleFlags_LSPDImpounded) {
					format(lockmsg, sizeof(lockmsg), "%s", impound);
				}
				if(VehicleInfo[i][EVFlags] & EVehicleFlags_Stored) {
					format(lockmsg, sizeof(lockmsg), "%s", stored);
				}
				if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
					format(msg, sizeof(msg), "%d: {ADD8E6}%s{FFFFFF} | Rental Car: %s", ++numfound, VehiclesName[model-400],lockmsg);
				} else {
					format(msg, sizeof(msg), "%d: {ADD8E6}%s{FFFFFF} | Lock: {ADD8E6}%s {FFFFFF}(%s{FFFFFF})", ++numfound, VehiclesName[model-400], LockName(VehicleInfo[i][EVLockType]), lockmsg);
				}
				SendClientMessage(playerid, X11_WHITE, msg);
			}
		}
	}
	format(msg,sizeof(msg),"Number of cars: %d | Vehicle Limit: %d",numfound,GetPVarInt(playerid, "MaxCars"));
	SendClientMessage(playerid, X11_WHITE, msg);
	return 1;
}
YCMD:storecar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to (un)store a car they own");
		return 1;
	}
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: Select the car you want to (un)store from the list!");
	ShowStorageMenu(playerid);
	return 1;
}
YCMD:createplayercar(playerid, params[], help) {
	new model,c1,c2,lock;
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a car for a player");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>dddd", playa,model, c1, c2, lock))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_WHITE, "Error: player not connected");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		new Float:X,Float:Y,Float:Z,Float:A;
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, A);
		if(c1 < 0 || c1 > 255 || c2 < 0 || c2 > 255)
		{
			SendClientMessage(playerid, X11_RED2, "   Color ID can't be below 0 or above 255!");
			return 1;
		}
		if(lock < 0 || lock > 7)
		{
			SendClientMessage(playerid, X11_RED2, "   The lock ID can't be below 0 or above 7!");
			return 1;
		}
		if(!isValidModel(model)) {
			SendClientMessage(playerid, X11_RED2, "   Invalid Vehicle Model");
			return 1;
		}
		if(isBannedVehicle(model) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "You can't spawn that vehicle!");
			return 1;
		}
		CreatePlayerCar(playa, model, c1, c2, X, Y, Z, A, ELockType:lock);
		new string[128];
		format(string, sizeof(string), "* Vehicle successfully created.");
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, string);
		format(string, sizeof(string), "An admin added a %s to your cars.", VehiclesName[model-400]);
		SendClientMessage(playa, COLOR_CUSTOMGOLD, string);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string), "* %s has given %s a %s",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName),  VehiclesName[model-400]);
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createplayercar [playerid/name] [model] [colour1] [colour2] [locktype]");
		SendClientMessage(playerid, X11_WHITE, "Lock types: 0 = Default, 1 = Simple, 2 = remote, 3 = remote, 4 = advanced, 5 = satelitte, 6 = titanium laser, 7 = bio");
	}
	return 1;
}
YCMD:destroyplayercar(playerid, params[], help) {
	new index;
	new playa;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a players car");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>d", playa,index))
    {
		index--;
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_WHITE, "Error: player not connected");
			return 1;
		}
		new carid = findPlayerCar(playa, index);
		if(carid == -1) {
			SendClientMessage(playerid, X11_WHITE, "This player doesn't have a car at this slot");
			return 1;
		}
		new string[128];
		new model = GetVehicleModel(carid);
		format(msg,sizeof(msg),"An admin has deleted your %s.",VehiclesName[model-400]);
		SendClientMessage(playa, COLOR_CUSTOMGOLD, msg);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string), "* %s has removed %s's %s",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName),  VehiclesName[model-400]);
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle removed");
		DestroyOwnedVehicle(carid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /destroyplayercar [playerid] [slot]");
	}
	return 1;
}
YCMD:lock(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Locks a players car");
		return 1;
	}
	new index;
	if (!sscanf(params, "D(0)", index))
    {
    	new carid;
    	if(index != 0) {
			index--;
			carid = findPlayerCar(playerid, index);
		} else {
			carid = GetClosestVehicle(playerid);
		}
		if(VehicleInfo[carid][EVOwner] != playerid) {
			SendClientMessage(playerid, X11_RED2, "You either don't have a vehicle at that slot or that's not your car.");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetVehiclePos(carid, X, Y, Z);
		new Float:dist = getLockDistance(VehicleInfo[carid][EVLockType]);
		if(dist != 0.0 && !IsPlayerInRangeOfPoint(playerid, dist, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far from your vehicle");
			return 1;
		}
		if(vehAlarmBroken(carid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Your vehicle alarm seems to be broken, looks like you'll have to repair your alarm.");
			return 1;
		}
		LockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"Your %s has been locked.",VehiclesName[GetVehicleModel(carid)-400]);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /lock [carindex]");
	}
	return 1;
}

YCMD:unlock(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unlocks a players car");
		return 1;
	}
	new index;
	if (!sscanf(params, "D(0)", index))
    {
		new carid;
    	if(index != 0) {
			index--;
			carid = findPlayerCar(playerid, index);
		} else {
			carid = GetClosestVehicle(playerid);
		}
		if(VehicleInfo[carid][EVOwner] != playerid) {
			SendClientMessage(playerid, X11_RED2, "You either don't have a vehicle at that slot or that's not your car.");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetVehiclePos(carid, X, Y, Z);
		new Float:dist = getLockDistance(VehicleInfo[carid][EVLockType]);
		if(dist != 0.0 && !IsPlayerInRangeOfPoint(playerid, dist, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far from your vehicle");
			return 1;
		}
		UnlockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"Your %s has been unlocked.",VehiclesName[GetVehicleModel(carid)-400]);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unlock [carindex]");
	}
	return 1;
}
YCMD:lockcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unlocks a players car");
		return 1;
	}
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED2, "Invalid Vehicle");
			return 1;
		}
		LockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"Vehicle %d locked",carid);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /lockcar [carid]");
	}
	return 1;
}
YCMD:unlockcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unlocks a players car");
		return 1;
	}
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED2, "Invalid Vehicle");
			return 1;
		}
		UnlockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"Vehicle %d unlocked",carid);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unlockcar [carid]");
	}
	return 1;
}
YCMD:fuelcar(playerid, params[], help) {
	new carid;
	if (!sscanf(params, "d", carid))
    {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Vehicle!");
			return 1;
		}
		VehicleInfo[carid][EVFuel] = 100;
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle Fueled");
		CheckGas();
		return 1;
	}
	carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "You must be in a car to fuel it, or specifiy the vehicle id!");
		return 1;
	}
	VehicleInfo[carid][EVFuel] = 100;
	CheckGas();
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle Fueled");
	return 1;
}

findPlayerCar(playerid, index) {
	new numfound;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_Owned) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				if(numfound++ == index) {
					return i;
				}
			}
		}
	}
	return -1;
}
CreatePlayerCar(playerid, model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle, ELockType:locktype, putincar = 0, cantbesold = 0) {
	query[0] = 0;//[256];
	format(query,sizeof(query),"INSERT INTO `playercars` (`owner`,`model`,`X`,`Y`,`Z`,`Angle`,`colour1`,`colour2`,`locktype`) VALUES (%d,%d,%f,%f,%f,%f,%d,%d,%d)",GetPVarInt(playerid, "CharID"),model,X,Y,Z,Angle,c1,c2,_:locktype);
	mysql_function_query(g_mysql_handle, query, true, "onPlayerVehicleCreate", "ddddffffddd",playerid,model,c1,c2,X,Y,Z,Angle,_:locktype,putincar,cantbesold);
	return 0;
}
/*
	This function is called when a character is logged into
*/
LoadPlayerVehicles(playerid) {
	query[0] = 0;//[512];
	format(query,sizeof(query),"SELECT `id`,`model`,`colour1`,`colour2`,`locktype`,`locked`,`fuel`,`tirestatus`,`doorstatus`,`lightstatus`,`X`,`Y`,`Z`,`Angle`,`plate`,`hp`,`panelstatus`,`paintjob`,`flags`,`mileage`,`impoundprice`,`alarmsettings`,`comp0`,`comp1`,`comp2`,`comp3`,`comp4`,`comp5`,`comp6`,`comp7`,`comp8`,`comp9`,`comp10`,`comp11`,`comp12` FROM `playercars` WHERE `owner` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "onPlayerLoadVehicles", "d",playerid);
}
loadJobVehicles(jobid) {
	query[0] = 0;//[256];
	format(query,sizeof(query),"SELECT `id`,`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`plate` FROM `jobcars` WHERE `owner` = %d",jobid);
	mysql_function_query(g_mysql_handle, query, true, "onLoadJobVehicles", "d",jobid);
}
loadFactionVehicles(faction) {
	query[0] = 0;//[512];
	format(query, sizeof(query), "SELECT `id`,`model`,`c1`,`c2`,`X`,`Y`,`Z`,`Angle`,`flags` FROM `factioncars` WHERE `owner` = %d",faction);
	mysql_function_query(g_mysql_handle, query, true, "onLoadFactionVehicles", "d",faction);
}
createFactionVehicle(factionid, model, Float:X, Float:Y, Float:Z, Float:Angle, c1, c2, sirens) {
	query[0] = 0;//[512];
	new EVehicleFlags:xFlags = EVehicleFlags:0;
	if (sirens) {
		xFlags |= EVehicleFlags_HasSirens;
	}
	format(query, sizeof(query), "INSERT INTO `factioncars` (`model`,`c1`,`c2`,`X`,`Y`,`Z`,`Angle`,`owner`,`flags`) VALUES (%d,%d,%d,%f,%f,%f,%f,%d,%d)", model, c1, c2, X, Y, Z, Angle,factionid,_:xFlags);
	mysql_function_query(g_mysql_handle, query, true, "onCreateFactionVehicle", "ddffffddd",factionid, model, X, Y, Z, Angle, c1, c2, sirens);
}
public onCreateFactionVehicle(factionid, model, Float:X, Float:Y, Float:Z, Float:Angle, c1, c2, sirens) {
	new id = mysql_insert_id();
	new msg[128];
	format(msg, sizeof(msg), "* Faction Vehicle SQL ID: %d", id);
	ABroadcast(X11_YELLOW, msg, EAdminFlags_FactionAdmin);
	new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY,sirens == 1 ? 1 : 0);
	if(!IsACar(model)) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVOwner] = factionid;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVLockType] = ELockType_Default;
	VehicleInfo[carid][EVSQLID] = id;
	VehicleInfo[carid][EVType] = EVehicleType_Faction;
	if(sirens) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_HasSirens;
	}
	if(getFactionType(VehicleInfo[carid][EVOwner]) != EFactionType_None) {
		SetVehicleNumberPlate(carid, GetFactionName(factionid));
	}
	SetVehicleStation(carid, 0);
}
YCMD:createfactioncar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a faction vehicle");
		return 1;
	}
	new model, faction, c1, c2, sirens;
	if(!sscanf(params, "ddddD(0)", faction, model, c1, c2, sirens)) {
		if(!IsValidFaction(faction)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Faction!");
			return 1;
		}
		if(model < 400 || model > 611) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Model!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z, Float:A;
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, A);
		createFactionVehicle(faction, model, X, Y, Z, A, c1, c2, sirens);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createfactioncar [factionid] [model] [c1] [c2] [sirens (optional 1 - 0)]");
	}
	return 1;
}
YCMD:savefactioncar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a faction vehicle");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0 || VehicleInfo[carid][EVType] != EVehicleType_Faction) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a faction car to do this!");
		return 1;
	}
	saveFactionCar(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Vehicle Saved!");
	return 1;
}
YCMD:destroyfactioncar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a faction vehicle");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0 || VehicleInfo[carid][EVType] != EVehicleType_Faction) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a faction car to do this!");
		return 1;
	}
	new msg[128];
	format(msg, sizeof(msg), "DELETE FROM `factioncars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
	DestroyVehicle(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Vehicle Deleted!");
	
	return 1;
}
saveFactionCar(carid) {
	if(VehicleInfo[carid][EVType] != EVehicleType_Faction) {
		return 0;
	}
	new msg[256];
	new id, Float:X, Float:Y, Float:Z, Float:Angle, c1, c2;
	id = VehicleInfo[carid][EVSQLID];
	c1 = VehicleInfo[carid][EVColour][0];
	c2 = VehicleInfo[carid][EVColour][1];
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, Angle);
	DestroyVehicle(carid);
	CreateVehicle(GetVehicleModel(carid), X, Y, Z, Angle, c1, c2, DEF_RESPAWN_DELAY, vehicleHasSirens(carid));
	format(msg, sizeof(msg), "UPDATE `factioncars` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f, `c1` = %d, `c2` = %d WHERE `id` = %d",X, Y, Z, Angle, c1, c2, id);
	mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
	return 1;
}
forward onLoadFactionVehicles(factionid);
public onLoadFactionVehicles(factionid) {
	new rows,fields,carid;
	new id_string[128],id;
	new modelid, c1, c2;
	new Float:X,Float:Y,Float:Z,Float:Angle;
	cache_get_data(rows,fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 1, id_string);//model
		modelid = strval(id_string);
		cache_get_row(i, 2, id_string);//c1
		c1 = strval(id_string);
		cache_get_row(i, 3, id_string);//c2
		c2 = strval(id_string);
		
		cache_get_row(i, 4, id_string);//x
		X = floatstr(id_string);
		cache_get_row(i, 5, id_string);//y
		Y = floatstr(id_string);
		cache_get_row(i, 6, id_string);//z
		Z = floatstr(id_string);
		cache_get_row(i, 7, id_string);//angle
		Angle = floatstr(id_string);
		cache_get_row(i, 8, id_string);//flags
		new EVehicleFlags:flags = EVehicleFlags:strval(id_string);

		cache_get_row(i, 0, id_string);//id
		
		id = strval(id_string);
		carid = CreateVehicle(modelid,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY, EVehicleFlags:flags & EVehicleFlags_HasSirens ? 1 : 0);
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}
		new engine,lights,alarm,doors,bonnet,boot,objective;
		GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
		SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
		VehicleInfo[carid][EVToDelete] = 0;
		VehicleInfo[carid][EVOwner] = factionid;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVFuel] = 100;
		VehicleInfo[carid][EVRadioStation] = -1;
		VehicleInfo[carid][EVLockType] = ELockType_Default;
		VehicleInfo[carid][EVSQLID] = id;
		VehicleInfo[carid][EVType] = EVehicleType_Faction;
		VehicleInfo[carid][EVFlags] = EVehicleFlags:flags;
		SetVehicleNumberPlate(carid, GetFactionName(factionid));
	}
}
public onLoadJobVehicles(jobid) {
	new rows,fields,carid;
	new id_string[128],id,plate[32];
	new modelid, c1, c2;
	new Float:X,Float:Y,Float:Z,Float:Angle;
	cache_get_data(rows,fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 1, id_string);//model
		modelid = strval(id_string);
		cache_get_row(i, 2, id_string);//c1
		c1 = strval(id_string);
		cache_get_row(i, 3, id_string);//c2
		c2 = strval(id_string);
		
		cache_get_row(i, 4, id_string);//x
		X = floatstr(id_string);
		cache_get_row(i, 5, id_string);//y
		Y = floatstr(id_string);
		cache_get_row(i, 6, id_string);//z
		Z = floatstr(id_string);
		cache_get_row(i, 7, id_string);//angle
		Angle = floatstr(id_string);
		
		cache_get_row(i, 0, id_string);//id
		
		id = strval(id_string);
		
		cache_get_row(i, 8, plate);//angle
		
		carid = CreateVehicle(modelid,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}
		new engine,lights,alarm,doors,bonnet,boot,objective;
		GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
		SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
		VehicleInfo[carid][EVToDelete] = 0;
		VehicleInfo[carid][EVOwner] = jobid;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVRadioStation] = -1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVFuel] = 100;
		VehicleInfo[carid][EVLockType] = ELockType_Default;
		VehicleInfo[carid][EVSQLID] = id;
		VehicleInfo[carid][EVType] = EVehicleType_JobCar;
		VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
		clearTrunk(carid);
		if(strlen(plate) == 0) {
			format(VehicleInfo[carid][EVPlate],32,"%s",GetJobName(jobid));
		} else {
			format(VehicleInfo[carid][EVPlate],32,"%s",plate);
		}
		SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
	}
}
public onCreateJobVehicle(jobid, model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle, plate[]) {
	new id_string[128],id;
	id = mysql_insert_id();
	new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
	if(!IsACar(GetVehicleModel(carid))) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVOwner] = jobid;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVLockType] = ELockType_Default;
	VehicleInfo[carid][EVSQLID] = id;
	VehicleInfo[carid][EVType] = EVehicleType_JobCar;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	clearTrunk(carid);
	format(VehicleInfo[carid][EVPlate],32,"%s",plate);
	SetVehicleNumberPlate(carid, plate);
	format(id_string, sizeof(id_string), "Job Car SQL ID: %d Vehicle ID: %d",id, carid);
	ABroadcast(X11_RED, id_string, EAdminFlags_Scripter);
	return 1;
}
YCMD:createjobcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a permanent job car");
		return 1;
	}
	new model, jobid, c1, c2;
	if(!sscanf(params, "dddd", model, jobid, c1, c2)) {
		if(!isValidModel(model)) {
			SendClientMessage(playerid, X11_RED2, "   Invalid Vehicle Model");
			return 1;
		}
		new Float:X, Float:Y, Float:Z,Float:A;
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, A);
		CreateJobCar(model, c1, c2, X, Y, Z, A, GetJobName(jobid), jobid, 1);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createjobcar [carid] [jobid] [c1] [c2]");
		SendClientMessage(playerid, X11_WHITE, "use /jobids if you don't know a job id!");
	}
	return 1;
}
YCMD:destroyjobcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes a job car");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0 || VehicleInfo[carid][EVType] != EVehicleType_JobCar) {
		SendClientMessage(playerid, X11_RED3, "You must be in a job car!");
		return 1;
	}
	DeleteJobCar(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Vehicle Destroyed!");
	return 1;
}
YCMD:savejobcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Saves a job cars position");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0 || VehicleInfo[carid][EVType] != EVehicleType_JobCar) {
		SendClientMessage(playerid, X11_RED3, "You must be in a job car!");
		return 1;
	}
	SaveJobCar(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Vehicle Saved!");
	return 1;
}
YCMD:setplate(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Updates a vehicles plate");
		return 1;
	}
	new plate[32];
	new carid;
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_RED3, "You must be in a vehicle!");
		return 1;
	}
	carid = GetPlayerVehicleID(playerid);
	if(!sscanf(params,"s[32]", plate)) {
		SetCarPlate(carid, plate);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Plates updated!");
	}
	return 1;
}
SetCarPlate(carid, plate[]) {
	new plate_esc[(32*2)+1];
	mysql_real_escape_string(plate,plate_esc);
	format(VehicleInfo[carid][EVPlate],32,"%s",plate);
	SetVehicleNumberPlate(carid, plate);
	format(query, sizeof(query), "UPDATE `%s` SET `plate` = \"%s\" WHERE `id` = %d",getVehicleTableName(carid),plate_esc,VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
getVehiclePlate(carid) {
	new ret[32];
	format(ret, sizeof(ret), "%s",VehicleInfo[carid][EVPlate]);
	return ret;
}
public ReloadFamilyCar(carid) {
	new famsql = VehicleInfo[carid][EVOwner];
	//FindFamilyBySQLID(GetPVarInt(playerid, "Family"))
	VehicleInfo[carid][EVOwner] = -1;
	DestroyVehicle(carid);
	format(query,sizeof(query),"SELECT `id`,`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`locktype`,`plate`,`paintjob`,`comp0`,`comp1`,`comp2`,`comp3`,`comp4`,`comp5`,`comp6`,`comp7`,`comp8`,`comp9`,`comp10`,`comp11`,`comp12` FROM `familycars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "onLoadFamilyCar", "d",famsql);
}
public ReloadJobCar(carid) {
	format(query,sizeof(query),"SELECT `id`,`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`plate` FROM `jobcars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	DestroyVehicle(carid);
	VehicleInfo[carid][EVType] = EVehicleType_Uninit;
	new jobid = VehicleInfo[carid][EVOwner];
	VehicleInfo[carid][EVOwner] = -1;
	mysql_function_query(g_mysql_handle, query, true, "onLoadJobVehicles", "d",jobid);
}
DeleteJobCar(carid) {
	format(query, sizeof(query), "DELETE FROM `jobcars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroyVehicle(carid);
	VehicleInfo[carid][EVType] = EVehicleType_Uninit;
}
SaveJobCar(carid) { //saves the vehicles position
	new Float:X,Float:Y,Float:Z,Float:A;
	new plate[(32*2)+1];
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, A);
	mysql_real_escape_string(VehicleInfo[carid][EVPlate],plate);
	format(query, sizeof(query), "UPDATE `jobcars` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f, `plate` = \"%s\" WHERE `id` = %d",X,Y,Z,A,plate,VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "ReloadJobCar", "d",carid);
}
CreateJobCar(model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle, plate[],jobid, permenant = 0) {
	new esc_plate[(32*2)+1];
	mysql_real_escape_string(esc_plate,plate);
	if(permenant) {
		format(query, sizeof(query), "INSERT INTO `jobcars` (`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`plate`,`owner`) VALUES (%d,%d,%d,%f,%f,%f,%f,\"%s\",%d)",model,c1,c2,X,Y,Z,Angle,esc_plate,jobid);
		mysql_function_query(g_mysql_handle, query, true, "onCreateJobVehicle", "ddddffffs", jobid, model, c1, c2, X, Y, Z, Angle, plate);
	} else {
		new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}
		VehicleInfo[carid][EVToDelete] = 0;
		VehicleInfo[carid][EVOwner] = jobid;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVFuel] = 100;
		VehicleInfo[carid][EVLockType] = ELockType_Default;
		VehicleInfo[carid][EVSQLID] = -1;
		VehicleInfo[carid][EVType] = EVehicleType_JobCar;
		VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
		clearTrunk(carid);
		format(VehicleInfo[carid][EVPlate],32,"%s",GetJobName(jobid));
		SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
		return carid;
	}
	return 0;
}
CreatePublicCar(model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle) {
	format(query, sizeof(query), "INSERT INTO `publiccars` (`model`,`c1`,`c2`,`X`,`Y`,`Z`,`Angle`) VALUES (%d,%d,%d,%f,%f,%f,%f)",model,c1,c2,X,Y,Z,Angle);
	mysql_function_query(g_mysql_handle, query, true, "onCreatePublicVehicle", "dddffff", model, c1, c2, X, Y, Z, Angle);
}
forward onCreatePublicVehicle(model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle);
public onCreatePublicVehicle(model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle) {
	new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
	if(!IsACar(GetVehicleModel(carid))) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new id = mysql_insert_id();
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVSQLID] = id;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	VehicleInfo[carid][EVOwner] = -1;
	VehicleInfo[carid][EVType] = EVehicleType_PublicCar;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	format(VehicleInfo[carid][EVPlate],32,"IWRP");
	clearTrunk(carid);
	SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
	new data[64];
	format(data,sizeof(data),"[AdmNotice]: Public Vehicle ID: %d",id);
	ABroadcast(X11_RED,data,EAdminFlags_Scripter);
	return carid;
}

loadPublicCars() {
	query[0] = 0;//[256];
	format(query, sizeof(query), "SELECT `model`,`c1`,`c2`,`X`,`Y`,`Z`,`Angle`,`id` FROM `publiccars`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadPublicCars", "");
}
YCMD:createpubliccar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a public car");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, Float:A;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	new model, c1, c2;
	if(!sscanf(params, "dD(0)D(0)", model, c1, c2)) {
		CreatePublicCar(model, c1, c2, X, Y, Z, A);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createpubliccar [model] (c1) (c2)");
	}
	return 1;
}
YCMD:destroypubliccar(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a public car");
		return 1;
	}
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	query[0] = 0;//[128];
	if(VehicleInfo[carid][EVType] != EVehicleType_PublicCar) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a public vehicle!");
		return 1;
	}
	format(query, sizeof(query), "DELETE FROM `publiccars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroyVehicle(carid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle Destroyed!");
	return 1;
}
YCMD:savepubliccar(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Saves a public car");
		return 1;
	}
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	query[0] = 0;//[256];
	if(VehicleInfo[carid][EVType] != EVehicleType_PublicCar) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a public vehicle!");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, Float:A;
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, A);
	format(query, sizeof(query), "UPDATE `publiccars` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f, `c1` = %f, `c2` = %f WHERE `id` = %d",X,Y,Z,A,VehicleInfo[carid][EVColour][0],VehicleInfo[carid][EVColour][1],VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle Saved!");
	return 1;
}
forward OnLoadPublicCars();
public OnLoadPublicCars() {
	new id_string[64],id;
	new Float:X, Float:Y, Float:Z, Float:A, model, c1, c2;
	new rows, fields;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		model = strval(id_string);
		cache_get_row(i, 1, id_string);
		c1 = strval(id_string);
		cache_get_row(i, 2, id_string);
		c2 = strval(id_string);
		cache_get_row(i, 3, id_string);
		X = floatstr(id_string);
		cache_get_row(i, 4, id_string);
		Y = floatstr(id_string);
		cache_get_row(i, 5, id_string);
		Z = floatstr(id_string);
		cache_get_row(i, 6, id_string);
		A = floatstr(id_string);
		cache_get_row(i, 7, id_string);
		id = strval(id_string);
		new carid = CreateVehicle(model,X,Y,Z,A,c1,c2,DEF_RESPAWN_DELAY);
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}
		new engine,lights,alarm,doors,bonnet,boot,objective;
		GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
		SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
		VehicleInfo[carid][EVToDelete] = 0;
		VehicleInfo[carid][EVSQLID] = id;
		VehicleInfo[carid][EVFuel] = 100;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVOwner] = -1;
		VehicleInfo[carid][EVRadioStation] = -1;
		VehicleInfo[carid][EVType] = EVehicleType_PublicCar;
		VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
		format(VehicleInfo[carid][EVPlate],32,"IWRP");
		SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
		clearTrunk(carid);
	}
	
}
vehOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	new lcar = -1;
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		new c = -1;
		new index = -1;
		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
			while((c = findPlayerCar(playerid, ++index)) != -1) {
				if(c == carid) {
					lcar = index;
					break;
				}
			}
		}
	}
	format(query, sizeof(query), "UPDATE `characters` SET `lastcar` = %d WHERE `id` = %d",lcar,GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == playerid && VehicleInfo[i][EVType] == EVehicleType_Owned) {
			saveVehicle(i);
			VehicleInfo[i][EVType] = EVehicleType_Uninit;
			clearTrunk(i);
			DestroyVehicleToys(i, 0);
			DestroyVehicle(i);
		}
	}
	if(GetPVarType(playerid, "JobCar") != PLAYER_VARTYPE_NONE) {
		new jobcar = GetPVarInt(playerid, "JobCar");
		DestroyVehicle(jobcar);
	}
	DestroyAllRentCars(playerid);
}

setLastCarPVar(playerid, newstate, oldstate) {
	#pragma unused newstate
	#pragma unused oldstate
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		SetPVarInt(playerid, "LastCar", carid);
	}
}
vehOnPlayerStateChange(playerid, newstate, oldstate) {
	if(oldstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_PASSENGER) {
		if(GetPVarType(playerid, "LastCar") != PLAYER_VARTYPE_NONE) {
			new carid = GetPVarInt(playerid, "LastCar");
			if(carid != 0) {
				if(VehicleInfo[carid][EVRadioStation] != -1 || hasCustomRadioStation(carid) != 0) {
					StopAudioStreamForPlayer(playerid);
				}
			}
		}
		if(GetPVarInt(playerid, "FuelShown") == 1) {
			new Bar:bar = Bar:GetPVarInt(playerid, "FuelBar");
			if(bar != INVALID_BAR_ID) {
				HideProgressBarForPlayer(playerid, bar);	
			}
			DeletePVar(playerid, "FuelShown");
		}
	}
	new carid = GetPlayerVehicleID(playerid);
	if(newstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER) {
		if(carid != 0) {
			if(vehicleAlarmRunning(carid) || vehAlarmBroken(carid)) {
				if(VehicleInfo[carid][EVType] == EVehicleType_Owned) { //Only if it's an owned vehicle
					ShowAlarmFlagsMenu(playerid, carid);
				}
			}
			if(VehicleInfo[carid][EVRadioStation] != -1 || hasCustomRadioStation(carid) != 0) {
				new url[128];
				//getRadioURL(VehicleInfo[carid][EVRadioStation], url, sizeof(url));
				getRadioURL(VehicleInfo[carid][EVRadioStation], url, sizeof(url), (hasCustomRadioStation(carid) != 0) ? carid : -1);
				PlayAudioStreamForPlayer(playerid, url);
			}
			if(!IsACar(GetVehicleModel(carid))) {
				new engine, lights, alarm, doors, bonnet, boot, objective;
				GetVehicleParamsEx(carid, engine, lights, alarm, doors, bonnet, boot, objective);
				VehicleInfo[carid][EVEngine] = 1;
				SetVehicleParamsEx(carid, VehicleInfo[carid][EVEngine], lights, alarm, doors, bonnet, boot, objective);
			}
		}
		if(carid != 0 && (EAdminFlags:GetPVarInt(playerid, "AdminFlags") != EAdminFlags_None && !acdisabled)) {
			/*
			if(VehicleInfo[carid][EVLocked] == 1) {
				if(VehicleInfo[carid][EVType] == EVehicleType_Owned && VehicleInfo[carid][EVOwner] != playerid) {
					new msg[64];
					format(msg, sizeof(msg), "Hacking into locked vehicle %d",carid);
					hackKick(playerid, msg, "Car Hacking");
				}
			}
			*/
		}
		if(newstate == PLAYER_STATE_DRIVER) {
			if(GetPVarType(playerid, "AllowDriveBy") == PLAYER_VARTYPE_NONE) {
				SetPlayerArmedWeapon(playerid,0);
			}
		} else {
			if(GetPVarType(playerid, "AllowDriveBy") == PLAYER_VARTYPE_NONE) {
				for(new i=0;i<sizeof(BadDrivebyWeapons);i++) {
					if(GetPlayerWeaponEx(playerid) == BadDrivebyWeapons[i]) {
						SetPlayerArmedWeapon(playerid,0);
					}
				}
			}
		}
	}
	if(newstate == PLAYER_STATE_DRIVER) { /* Pizza Job */
		new vehicleid = GetPlayerVehicleID(playerid);
		if(VehicleInfo[vehicleid][EVType] == EVehicleType_JobCar) {
			if(VehicleInfo[vehicleid][EVOwner] == EJobType_PizzaMan) {
				initiatePizzaDelivery(playerid);
			}
		}
	}
	if(newstate == PLAYER_STATE_DRIVER) { /* Capture The Vehicle */
		new vehicleid = GetPlayerVehicleID(playerid);
		if(VehicleInfo[vehicleid][EVType] == EVehicleType_CaptureCar) {
			if(GetPVarInt(playerid, "Family") > 0) {
				initiateCaptureTheVehicle(playerid);
			}
		}
	}
	if(newstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER) { //Wipers - get on
		weatherInVehicleDecide(playerid);
		if(vehicleWipersOn(carid)) {
			SendClientMessage(playerid, X11_WHITE, "[INFO]: Wipers On");
		}
		if(isPlayerDying(playerid) || isPlayerWounded(playerid)) { //anim bug abuse "fix"
			putPlayerInHospital(playerid);
		}
	}
	if(oldstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_PASSENGER) { //Wipers - get off and CTV
		if(newstate == PLAYER_STATE_ONFOOT) {
			weatherInVehicleDecide(playerid);
			ctvOnStateChange(playerid, carid); //Only executes if the vehicle the player exited was the capture car... 
		}
	}
	if(newstate == PLAYER_STATE_DRIVER) {
		if(IsACar(GetVehicleModel(carid))) {
			HintMessage(playerid, COLOR_CUSTOMGOLD, "To start the engine press ~k~~TOGGLE_SUBMISSIONS~");
		}
		new msg[128];
		new newcar = GetPlayerVehicleID(playerid);
		new newmodel = GetVehicleModel(newcar);
		if(VehicleInfo[newcar][EVType] == EVehicleType_JobCar) {
			if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
				if(VehicleInfo[newcar][EVOwner] != GetPVarInt(playerid, "Job")) {
					format(msg,sizeof(msg), "You must be a %s to drive this!",GetJobName(VehicleInfo[newcar][EVOwner]));
					SendClientMessage(playerid, X11_RED3, msg);
					RemovePlayerFromVehicle(playerid);
				}
			}
			OnEnterJobCar(playerid, newcar, VehicleInfo[newcar][EVOwner]);
		}
		if(VehicleInfo[newcar][EVType] == EVehicleType_Faction) {
			if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
				new faction = GetPVarInt(playerid, "Faction");
				if(VehicleInfo[newcar][EVOwner] != faction) {
					if(getPlayerFactionType(playerid) != getFactionType(VehicleInfo[newcar][EVOwner]))
					{
						format(msg,sizeof(msg), "You don't have the keys to this vehicle!");
						SendClientMessage(playerid, X11_RED3, msg);
						RemovePlayerFromVehicle(playerid);
					}
				}
			}
		}
		if(IsACar(newmodel))
		{			
	        if(!IsABicycle(newmodel))
			{
				updateVehicleGUI(playerid);
			}
		}
		#if BIKE_RESTRICTION
		if(IsABicycle(newmodel)) {
			if(playerIsBikeRestricted(playerid)) {
				RemovePlayerFromVehicle(playerid);
				SendClientMessage(playerid, X11_RED3, "You currently don't have permissions to ride this bike, an admin must approve you.");
				return 1;
			}
		}
		#endif
	} else if(oldstate == PLAYER_STATE_DRIVER) {
		hideVehGUI(playerid);
		new jobcar = GetPVarInt(playerid, "JobCar");
		
		if(jobcar != 0 || GetPVarType(playerid, "TaxiPassenger") != PLAYER_VARTYPE_NONE 	
		|| GetPVarType(playerid, "CurrentTruckerRoute") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "IsOnPizzaRoute") != PLAYER_VARTYPE_NONE) {
			onJobAbort(playerid);
			onTaxiPassengerAbort(playerid);
			DestroyVehicle(jobcar);
			DeletePVar(playerid, "JobCar");
		}
	} else if(oldstate == PLAYER_STATE_PASSENGER) {
		if(GetPVarType(playerid, "TaxiDriver") != PLAYER_VARTYPE_NONE) {
			onJobAbort(GetPVarInt(playerid, "TaxiDriver"));
		}
	} else if(newstate == PLAYER_STATE_PASSENGER) {
		if(carid == INVALID_VEHICLE_ID) {
			return 0;
		}
		new driver = GetVehicleDriver(carid);
		if(driver != INVALID_PLAYER_ID) {
			if(GetPVarType(driver, "TaxiFare") != PLAYER_VARTYPE_NONE) {
				OnPlayerEnterTaxi(playerid, driver);
			}
		}
	}
	return 1;
}
YCMD:tempcar(playerid, params[], help) {
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates a temporary car");
		return 1;
	}
	new model, c1, c2;
	if(!sscanf(params,"iI(0)I(0)",  model, c1, c2)) {
		if(model < 400 || model > 611) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "Invalid Vehicle!");
			return 1;
		}
		if(isBannedVehicle(model) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "You can't spawn that vehicle!");
			return 1;
		}
		if(c1 < 0 || c1 > 255 || c2 < 0 || c2 > 255) {
			SendClientMessage(playerid, COLOR_LIGHTRED, "Invalid Model!");
			return 1;
		}
		new Float:X,Float:Y,Float:Z,Float:A;
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, A);
		new carid = CreateTempCar(model,X, Y,Z, A, c1, c2, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
		format(msg, sizeof(msg), "* Vehicle %d Spawned!",carid);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);

	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /tempcar [modelid] (c1) (c2)");
	}
	return 1;
}

YCMD:entercar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enters a car");
		return 1;
	}
	new carid, seat;
	if(!sscanf(params, "dD(0)", carid, seat)) {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle!");
			return 1;
		}
		new pseat = GetVehicleSeatID(carid, seat);
		if(pseat != -1) {
			RemovePlayerFromVehicle(playerid);
		}
		PutPlayerInVehicleEx(playerid, carid, seat);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /entercar [carid] (seatid)");
	}
	return 1;
}
YCMD:putincar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enters a car");
		return 1;
	}
	new target,carid, seat;
	if(!sscanf(params, "k<playerLookup_acc>dD(0)", target,carid, seat)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle!");
			return 1;
		}
		new pseat = GetVehicleSeatID(carid, seat);
		if(pseat != -1) {
			RemovePlayerFromVehicle(target);
		}
		PutPlayerInVehicleEx(target, carid, seat);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /entercar [carid] (seatid)");
	}
	return 1;
}
YCMD:closestcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Finds the closest car");
		return 1;
	}
	new msg[128];
	new carid = GetClosestVehicle(playerid);
	if(carid == -1) {
		format(msg, sizeof(msg), "* Closest Vehicle: None");
	} else {
		format(msg, sizeof(msg), "* Closest Vehicle: %d (Model: %d)",carid, GetVehicleModel(carid));
	}
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	return 1;
}
YCMD:setcarlock(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Manually set a cars lock");
	}
	new carid, lock;
	if(!sscanf(params, "dd", carid, lock)) {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle!");
			return 1;
		}
		VehicleInfo[carid][EVLockType] = ELockType:lock;
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Lock Set!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setcarlock [carid] [locktype]");
		SendClientMessage(playerid, X11_WHITE, "Lock types: 0 = Default, 1 = Simple, 2 = remote, 3 = remote, 4 = advanced, 5 = satelitte, 6 = titanium laser, 7 = bio");
	}
	return 1;
}
YCMD:pickcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for attempting to break into a car");
		return 1;
	}
	new numpicks = GetPVarInt(playerid, "VehLockpicks");
	if(numpicks < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have a vehicle lock pick, buy one at a 24/7");
		return 1;
	}
	new msg[128];
	new time = GetPVarInt(playerid, "LockpickCooldown");
	new timenow = gettime();
	if(LOCKPICK_COOLDOWN-(timenow-time) > 0) {
		format(msg, sizeof(msg), "You must wait %d seconds before you can use this",LOCKPICK_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	numpicks--;
	new carid = GetClosestVehicle(playerid);
	new Float:X, Float:Y, Float:Z;
	GetVehiclePos(carid, X, Y, Z);
	if(GetVehicleModel(carid) == 0 || !IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You are too far away from the vehicle");
		return 1;
	}
	if(IsPlayerBlocked(playerid) || GetPlayerState(playerid) != PLAYER_STATE_ONFOOT) {
	    SendClientMessage(playerid, X11_TOMATO_2, "You can't do that right now!");
	    return 1;
	}
	if(isVehicleStored(carid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "That vehicle is in storage so you can't do that!");
	    return 1;
	}
	if(GetPlayerSpecialAction(playerid) != SPECIAL_ACTION_DUCK) {
	    SendClientMessage(playerid, X11_TOMATO_2, "You need to be crouching to do this! Be sneaky!");
	    return 1;
	}
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned && VehicleInfo[carid][EVType] != EVehicleType_Family) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't pick this vehicle!");
		return 1;
	}
	//{"Lock Picking Skill","LockSkill",50,100,200,300,500,-1, "You need to jack %d more cars to reach the next level!"}
	//new picklevel = getSkillLevel(playerid, 3);
	new ELockType:lock = VehicleInfo[carid][EVLockType];
	SetPVarInt(playerid, "VehLockpicks", numpicks);
	new success = RandomEx(0,3);
	if(success == 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You have picked the car");
		VehicleInfo[carid][EVLocked] = 0;
		syncVehicleParams(carid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* Lock Picking failed");
		onVehicleLockPickFailure(carid);
	}
	switch(lock) {
		case ELockType_Remote: {
			if(VehicleInfo[carid][EVFlags] & EVehicleFlags_AlarmActivated) {
				SendClientMessage(playerid, X11_TOMATO_2, "* The vehicle's alarm went off, try deactivating it by disconnecting the wires under the dashboard.");
				setVehicleAlarm(carid, 1);
			}
		}
	}
	SetPVarInt(playerid, "LockpickCooldown",timenow);
	increaseSkillPoints(playerid, 3);
	return 1;
}
onVehicleLockPickFailure(carid) {
	new ELockType:lock = VehicleInfo[carid][EVLockType];
	new msg[128];
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned && VehicleInfo[carid][EVType] != EVehicleType_Family) {
		return 0;
	}
	new model = GetVehicleModel(carid);
	if(lock == ELockType_Default) {
		return 1;
	} else {
		format(msg, sizeof(msg), "SMS: Alert! Security system has been triggered in your %s!, Sender: %s security system", VehiclesName[model-400], VehiclesName[model-400]);
		if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
			SendClientMessage(VehicleInfo[carid][EVOwner], COLOR_YELLOW, msg);
		} else if(VehicleInfo[carid][EVType] == EVehicleType_Family) {
			FamilyMessage(VehicleInfo[carid][EVOwner], COLOR_YELLOW, msg);
		}
	}
	return 1;
}
soundCarAlarm(carid, play = 1) {
	new engine, lights, alarm, doors, bonnet, boot, objective;
	GetVehicleParamsEx(carid, engine, lights, alarm, doors, bonnet, boot, objective);
	SetVehicleParamsEx(carid, engine, lights, play == 1 ? 1 : 0, doors, bonnet, boot, objective);
	//SetTimerEx("DisableVehicleAlarm", 20000, false, "d", carid);
}

forward DisableVehicleAlarm(vehicleid);
public DisableVehicleAlarm(vehicleid)
{
    new engine, lights, alarm, doors, bonnet, boot, objective;
    GetVehicleParamsEx(vehicleid, engine, lights, alarm, doors, bonnet, boot, objective);
    SetVehicleParamsEx(vehicleid, engine, lights, false, doors, bonnet, boot, objective);
    return 1;
}
YCMD:carowner(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Finds the closest car");
		return 1;
	}
	new msg[128];
	new ownername[128];
	new carid;
	if(!sscanf(params, "d", carid)) {
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle");
			return 1;
		}
		switch(VehicleInfo[carid][EVType]) {
			case EVehicleType_Owned: {
				format(ownername,sizeof(ownername), "Player: %s (ID: %d)",GetPlayerNameEx(VehicleInfo[carid][EVOwner], ENameType_CharName),VehicleInfo[carid][EVOwner]);
			}
			case EVehicleType_Family: {
				new fid = FindFamilyBySQLID(VehicleInfo[carid][EVOwner]);
				format(ownername,sizeof(ownername), "Family: %s (ID: %d)",GetFamilyName(fid),fid);
			}
			case EVehicleType_JobCar: {
				format(ownername,sizeof(ownername), "Job: %s (ID: %d)",GetJobName(VehicleInfo[carid][EVOwner]),VehicleInfo[carid][EVOwner]);
			}
			case EVehicleType_PublicCar: {
				format(ownername,sizeof(ownername), "Public Car");
			}
			case EVehicleType_Faction: {
				format(ownername,sizeof(ownername), "Faction: %s (ID: %d)",GetFactionName(VehicleInfo[carid][EVOwner]),VehicleInfo[carid][EVOwner]);
			}
			case EVehicleType_RentCar: {
				format(ownername,sizeof(ownername), "Rental: %s (ID: %d)",GetPlayerNameEx(VehicleInfo[carid][EVOwner], ENameType_CharName),VehicleInfo[carid][EVOwner]);
			}
			case EVehicleType_CaptureCar: {
				format(ownername,sizeof(ownername), "Capture The Vehicle: %s (ID: %d)",GetPlayerNameEx(VehicleInfo[carid][EVOwner], ENameType_CharName),VehicleInfo[carid][EVOwner]);
			}
			case EVehicleType_None: {
				format(ownername,sizeof(ownername), "No-One");
			}
		}
		format(msg, sizeof(msg), "* Car Owner: %s",ownername);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /carowner [carid]");
	}
	return 1;
}
CreateTempCar(model, Float:X, Float:Y, Float:Z, Float:A, c1, c2, vw = 0, interior = 0) {
	new carid = CreateVehicle(model, X, Y, Z, A, c1, c2,900);
	SetVehicleVirtualWorld(carid, vw);
	LinkVehicleToInterior(carid, interior);
	if(!IsACar(GetVehicleModel(carid))) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVType] = EVehicleType_None;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	strmid(VehicleInfo[carid][EVPlate],"IWRP",0,4,32);
	SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVLockType] = ELockType_Impossible;
	VehicleInfo[carid][EVSQLID] = -1;
	clearTrunk(carid);
	return carid;
}
GetVehicleDriver(carid) {
	foreach(Player, i) {
		if(GetPlayerVehicleSeat(i) == 0) {
			if(GetPlayerVehicleID(i) == carid) {
				return i;
			}
		}
	}
	return INVALID_PLAYER_ID;
}
GetVehicleSeatID(carid, seat) {
	foreach(Player, i) {
		if(GetPlayerVehicleID(i) == carid) {
			if(GetPlayerVehicleSeat(i) == seat) {
				return i;
			}
		}
	}
	return -1;
}
public onPlayerLoadVehicles(playerid) {
	new rows,fields;
	new id_string[128],id;
	new modelid, c1, c2;
	new Float:X,Float:Y,Float:Z,Float:Angle;
	cache_get_data(rows,fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 1, id_string);//model
		modelid = strval(id_string);
		cache_get_row(i, 2, id_string);//c1
		c1 = strval(id_string);
		cache_get_row(i, 3, id_string);//c2
		c2 = strval(id_string);
		
		cache_get_row(i, 10, id_string);//x
		X = floatstr(id_string);
		cache_get_row(i, 11, id_string);//y
		Y = floatstr(id_string);
		cache_get_row(i, 12, id_string);//z
		Z = floatstr(id_string);
		cache_get_row(i, 13, id_string);//angle
		Angle = floatstr(id_string);
		new carid = CreateVehicle(modelid,X,Y,Z,Angle,c1,c2,-1);
		
		cache_get_row(i, 5, id_string);//lock
		id = strval(id_string);
		VehicleInfo[carid][EVLocked] = id;
		
		cache_get_row(i, 4, id_string);//locktype
		id = strval(id_string);
		
		VehicleInfo[carid][EVLockType] = ELockType:id;
		
		cache_get_row(i, 6, id_string);//fuel
		id = strval(id_string);
		VehicleInfo[carid][EVFuel] = id;
		
		
		new tirestatus, doorstatus, lightstatus, panelstatus, Float:health;
		
		cache_get_row(i, 16, id_string);//panel status
		panelstatus = strval(id_string);
		
		cache_get_row(i, 7, id_string);//tire status
		tirestatus = strval(id_string);
		
		cache_get_row(i, 8, id_string);//door status
		doorstatus = strval(id_string);
		
		cache_get_row(i, 9, id_string);//light status
		lightstatus = strval(id_string);
		
		cache_get_row(i, 15, id_string);//vehicle HP
		health = floatstr(id_string);
		//if(health < 250.0) health = 500.0;
		if(health > 250.0) {
			UpdateVehicleDamageStatus(carid, panelstatus, doorstatus, lightstatus, tirestatus);
			SetVehicleHealth(carid, health);		
		}
		cache_get_row(i, 17, id_string);//paintjob
		new paintjob = strval(id_string);
		if(vehicleHasPaintJob(carid)) {
			VehicleInfo[carid][EVPaintjob] = paintjob;
		}
		vehOnLoadPaintJob(carid);
		new componentid;
		for(new x=22;(x-22)<13;x++) {
			cache_get_row(i, x, id_string);
			componentid = strval(id_string);
			if(islegalcarmod(modelid, componentid))
				AddVehicleComponent(carid,componentid);
		}
		
		new Float:MileAge;
		cache_get_row(i, 19, id_string);//mileage
		MileAge = floatstr(id_string);
		VehicleInfo[carid][EVMileAge] = MileAge;
		
		cache_get_row(i, 20, id_string);//Impound Price
		VehicleInfo[carid][EVImpoundPrice] =  strval(id_string);
		
		cache_get_row(i, 21, id_string);//Alarm Settings
		VehicleInfo[i][EAlarmFlags] = AlarmFlags:strval(id_string);
		
		cache_get_row(i, 0, id_string);//SQL ID
		id = strval(id_string);
		
		
		VehicleInfo[carid][EVSQLID] = id;
		
		cache_get_row(i, 18, id_string);
		new EVehicleFlags:flags = EVehicleFlags:strval(id_string);
		
		VehicleInfo[carid][EVFlags] = flags;
		
		cache_get_row(i, 14, VehicleInfo[carid][EVPlate]);//vehicle HP
		
		if(strlen(VehicleInfo[carid][EVPlate]) > 1) {
			SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
		}
		VehicleInfo[carid][EVType] = EVehicleType_Owned;
		VehicleInfo[carid][EVOwner] = playerid;
		VehicleInfo[carid][EVColour][0] = c1;
		VehicleInfo[carid][EVColour][1] = c2;
		VehicleInfo[carid][EVRadioStation] = -1;
		if(!IsACar(GetVehicleModel(carid))) {
			VehicleInfo[carid][EVEngine] = 1;
		} else {
			VehicleInfo[carid][EVEngine] = 0;
		}
		new engine,lights,alarm,doors,bonnet,boot,objective;
		GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
		SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,VehicleInfo[carid][EVLocked],bonnet,boot,objective);
		loadTrunks(carid);
		if(flags & EVehicleFlags_LSPDImpounded) {
			putVehicleInImpound(carid,1,VehicleInfo[carid][EVImpoundPrice]);
		} else if((health < 250.0 && (playerHasCarInsurance(playerid) && IsACar(modelid))) || flags & EVehicleFlags_InsuranceImpound) {
			putVehicleInImpound(carid,0);
		} else if((health < 250.0 && (playerHasCarInsurance(playerid) && IsAFlyingVehicle(modelid))) || flags & EVehicleFlags_AIRPImpound) {
			putVehicleInImpound(carid,3); //3 because 2 is the LSPD one I believe
		} else if(isVehicleStored(carid)) {
			putVehicleInStorage(carid);
		}
		if(flags & EVehicleFlags_WindowsDown) {
			SetVehicleParamsCarWindows(carid, 0, 0, 0, 0);
		} else {
			SetVehicleParamsCarWindows(carid, 1, 1, 1, 1);
		}
		loadOwnedCarToys(carid);
	}
	new slot = GetPVarInt(playerid, "LastPlayerCar");
	if(slot != -1) {
		new carid = findPlayerCar(playerid, slot);
		if(carid != -1) {
			PutPlayerInVehicleEx(playerid, carid, 0);
		}
	}
}
putVehicleInImpound(carid, type, impoundprice = 0) {
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		return 0;
	}
	if(type == 0) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_InsuranceImpound;
		clearTrunk(carid);
	} else if(type == 1) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_LSPDImpounded;
		VehicleInfo[carid][EVImpoundPrice] = impoundprice;
		new msg[128];
		format(msg, sizeof(msg), "Your %s has been impounded for $%s",VehiclesName[GetVehicleModel(carid)-400],getNumberString(impoundprice));
		SendClientMessage(VehicleInfo[carid][EVOwner], COLOR_LIGHTBLUE, msg);
	} else if(type == 3) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_AIRPImpound;
		clearTrunk(carid);
	}
	HideCarToys(carid, 1);
	SetVehicleVirtualWorld(carid, VehicleInfo[carid][EVSQLID]);
	return 1;
}
putVehicleInStorage(carid) {
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		return 0;
	}
	VehicleInfo[carid][EVFlags] |= EVehicleFlags_Stored;
	SetVehicleVirtualWorld(carid, VehicleInfo[carid][EVSQLID]);
	return 1;
}
GetImpoundPrice(carid) {
	return VehicleInfo[carid][EVImpoundPrice];
}
removeVehicleFromImpound(carid) {
	setVehicleTotaled(carid, 0);
	RepairVehicle(carid);
	VehicleInfo[carid][EVEngine] = 0;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVLocked] = 1;
	syncVehicleParams(carid);
	if(VehicleInfo[carid][EVFlags] & EVehicleFlags_InsuranceImpound) {
		SetVehiclePos(carid, 2052.559082, -1904.250732, 13.421780);
		SetVehicleZAngle(carid, 179.776184);
		VehicleInfo[carid][EVFlags]  &= ~EVehicleFlags_InsuranceImpound;
	} else if(VehicleInfo[carid][EVFlags] & EVehicleFlags_LSPDImpounded) {
		SetVehiclePos(carid, 1521.606689, -1672.692504, 13.421872 + 1);
		SetVehicleZAngle(carid, 179.073165);
		VehicleInfo[carid][EVFlags]  &= ~EVehicleFlags_LSPDImpounded;
		VehicleInfo[carid][EVImpoundPrice] = 0;
	} else if(VehicleInfo[carid][EVFlags] & EVehicleFlags_AIRPImpound) {
		SetVehiclePos(carid, 1921.2538, -2342.8550, 13.5469 + 1);
		SetVehicleZAngle(carid, 187.9429);
		VehicleInfo[carid][EVFlags]  &= ~EVehicleFlags_AIRPImpound;
		VehicleInfo[carid][EVImpoundPrice] = 0;
	}
	HideCarToys(carid, 0);
	SetVehicleVirtualWorld(carid, 0);
}
public onPlayerVehicleCreate(playerid,model,c1,c2, Float:X, Float:Y, Float:Z, Float:Angle, ELockType:locktype, putincar, cantbesold) {
	new id = mysql_insert_id();
	new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,-1);
	VehicleInfo[carid][EVOwner] = playerid;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	VehicleInfo[carid][EVLocked] = 0;
	if(!IsACar(GetVehicleModel(carid))) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,VehicleInfo[carid][EVLocked],bonnet,boot,objective);
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVLockType] = locktype;
	VehicleInfo[carid][EVSQLID] = id;
	VehicleInfo[carid][EVType] = EVehicleType_Owned;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVPaintjob] = -1;
	VehicleInfo[carid][EVMileAge] = 0.0;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	if(cantbesold == 1) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_CantBeSold;
	}
	if(putincar == 1) {
		PutPlayerInVehicleEx(playerid, carid, 0);
	}
	new flags = GetPVarInt(playerid, "UserFlags");
	flags |= EUFHasCarInsurance;
	SetVehicleNumberPlate(carid, "IWRP");
	clearTrunk(carid);
}
saveVehicle(carid) {
	query[0] = 0;//[512];
	new Float:X,Float:Y,Float:Z,Float:A;
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, A);
	new Float:health;
	GetVehicleHealth(carid, health);
	new tirestatus, doorstatus, lightstatus, panelstatus;
	GetVehicleDamageStatus(carid,panelstatus, doorstatus, lightstatus, tirestatus);
	new components[13];
	for(new i=0;i<sizeof(components);i++) {
		components[i] = GetVehicleComponentInSlot(carid, i);
	}
	if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
		format(query,sizeof(query),"UPDATE `%s` SET `X` = %f,`Y` = %f, `Z` = %f, `Angle` = %f, `tirestatus` = %d,`doorstatus` = %d, `lightstatus` = %d, `panelstatus` = %d, `hp` = %f,`fuel` = %d,`locktype` = %d,`locked` = %d,`colour1` = %d,`colour2` = %d,`paintjob` = %d,`comp0` = %d,`comp1` = %d,`comp2` = %d,`comp3` = %d,`comp4` = %d,`comp5` = %d,`comp6` = %d,`comp7` = %d,`comp8` = %d,`comp9` = %d,`comp10` = %d,`comp11` = %d,`comp12` = %d,`flags` = %d,`mileage` = %f,`impoundprice` = %d WHERE `id` = %d",
		getVehicleTableName(carid), X, Y, Z, A, tirestatus, doorstatus, lightstatus, panelstatus, health,
		VehicleInfo[carid][EVFuel],_:VehicleInfo[carid][EVLockType],VehicleInfo[carid][EVLocked],
		VehicleInfo[carid][EVColour][0],VehicleInfo[carid][EVColour][1],VehicleInfo[carid][EVPaintjob],
		components[0],components[1],components[2],components[3],components[4],components[5],components[6],
		components[7],components[8],components[9],components[10],components[11],components[12],_:VehicleInfo[carid][EVFlags],VehicleInfo[carid][EVMileAge],VehicleInfo[carid][EVImpoundPrice],VehicleInfo[carid][EVSQLID]);
	} else {
		format(query,sizeof(query),"UPDATE `%s` SET `X` = %f,`Y` = %f, `Z` = %f, `Angle` = %f,`colour1` = %d,`colour2` = %d,`paintjob` = %d,`comp0` = %d,`comp1` = %d,`comp2` = %d,`comp3` = %d,`comp4` = %d,`comp5` = %d,`comp6` = %d,`comp7` = %d,`comp8` = %d,`comp9` = %d,`comp10` = %d,`comp11` = %d,`comp12` = %d WHERE `id` = %d",	getVehicleTableName(carid), X, Y, Z, A, VehicleInfo[carid][EVColour][0], VehicleInfo[carid][EVColour][1], VehicleInfo[carid][EVPaintjob], 
		components[0],components[1],components[2],components[3],components[4],components[5],components[6],
		components[7],components[8],components[9],components[10],components[11],components[12],VehicleInfo[carid][EVSQLID]);
	}
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	saveTrunks(carid);
}
DestroyOwnedVehicle(vehicleid) {
	query[0] = 0;//[128];
	format(query,sizeof(query),"DELETE FROM `%s` WHERE `id` = %d",getVehicleTableName(vehicleid),VehicleInfo[vehicleid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroyVehicleToys(vehicleid, 1);
	DestroyVehicle(vehicleid);
	setVehicleTotaled(vehicleid, 0);
	VehicleInfo[vehicleid][EVType] = EVehicleType_Uninit;
}
vehOnVehicleDeath(vehicleid, killerid) {
	#pragma unused killerid
	VehicleInfo[vehicleid][EVEngine] = 0;
	setVehicleTotaled(vehicleid, 0);
	//printf("vehOnVehicleDeath(%d, %d)", vehicleid, killerid);
	if(killerid != INVALID_PLAYER_ID) {
		if(isFloodingCarDeaths(killerid)) {
			SetVehicleToRespawn(vehicleid); //Just respawn the car
			return 1; //Don't do anything because the vehicle was destroyed by hacks / etc
		}
	}
	switch(VehicleInfo[vehicleid][EVType]) {
		case EVehicleType_Owned: {
			VehicleInfo[vehicleid][EVToDelete] = 1;
		}
		case EVehicleType_None: {
			DestroyVehicle(vehicleid);
		}
		case EVehicleType_RentCar: {
			DestroyRentCar(vehicleid);
		}
		case EVehicleType_CaptureCar: {
			DestroyCaptureCar(vehicleid);
			onVehicleCaptureFailure(vehicleid); //Send a message saying that the vehicle has blown up..
		}
	}
	if(GetVehicleModel(vehicleid) == 519) {
		foreach(Player, i) {
			if(IsPlayerInShamal(i) == vehicleid) {
				SendClientMessage(i, X11_TOMATO_2, "The shamal has crashed, and you have died.");
				putPlayerInHospital(i);
				return 1;
			}

		}
	}
	return 1;
}
stock returnAlarmFlagDesc(vehicleid) {
	new AlarmFlags:aflags = VehicleInfo[vehicleid][EAlarmFlags];
	tempstr[0] = 0;
	for(new i=0;i<sizeof(AlarmFlagDescription);i++) {
		if(aflags & AlarmFlagDescription[i][EAlarmFlagID]) {
			format(tempstr,sizeof(tempstr),"%s",AlarmFlagDescription[i][EAlarmFlagDesc]);
		}
	}
	return tempstr;
}
ShowAlarmFlagsMenu(playerid, vehicleid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new AlarmFlags:aflags = VehicleInfo[vehicleid][EAlarmFlags];
	new statustext[32]; 
	SetPVarInt(playerid, "VehicleAlarmEditing", vehicleid);
	for(new i=0;i<sizeof(AlarmFlagDescription);i++) {
		if(aflags & AlarmFlagDescription[i][EAlarmFlagID]) {
			statustext = "{FF0000}Disconnected";
		} else {
			statustext = "{00FF00}Connected";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",AlarmFlagDescription[i][EAlarmFlagDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EVehiclesDialog_EditAlarmFlags, DIALOG_STYLE_LIST, "Alarm Information:",dialogstr,"Toggle", "Done");
}
ShowStorageMenu(playerid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new statustext[32]; 
	new amount;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == playerid && VehicleInfo[i][EVType] == EVehicleType_Owned) {
			new model = GetVehicleModel(i);
			if(model != 0) {
				if(VehicleInfo[i][EVFlags] & EVehicleFlags_Stored) {
					statustext = "{00FF00}Stored";
				} else {
					statustext = "{FF0000}Not Stored";
				}
				format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",VehiclesName[model-400],statustext);
				strcat(dialogstr,tempstr,sizeof(dialogstr));
				amount++;
			}
		}
	}
	if(amount > 0) {
		ShowPlayerDialog(playerid, EVehiclesDialog_StorageMenu, DIALOG_STYLE_LIST, "Storage Information:",dialogstr,"(Un)Spawn", "Done");
	} else {
		ShowPlayerDialog(playerid, EAccountDialog_DoNothing, DIALOG_STYLE_MSGBOX, "{00BF00}Storage Information:","{FF0000}You don't have any cars you can actually store!","Ok", "");
	}
}
stock getVehicleTableName(carid) {
	new name[32];
	switch(VehicleInfo[carid][EVType]) {
		case EVehicleType_Owned: {
			format(name,sizeof(name),"%s","playercars");
		}
		case EVehicleType_Family: {
			format(name,sizeof(name),"%s","familycars");
		}
		case EVehicleType_Faction: {
			format(name,sizeof(name),"%s","factioncars");
		}
		case EVehicleType_JobCar: {
			format(name,sizeof(name),"%s","jobcars");
		}
	}
	return name;
}
vehOnEngineTry(playerid) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID) return 0;
	if(GetPlayerVehicleSeat(playerid) != 0) return 0;
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid, engine, lights, alarm, doors, bonnet, boot, objective);
	new msg[128];
	new time = GetPVarInt(playerid, "EngineCooldown");
	new timenow = gettime();
	if(ENGINE_COOLDOWN-(timenow-time) > 0) {
		return 1;
	}
	if(vehicleAlarmRunning(carid)) {
		if(VehicleInfo[carid][EVType] == EVehicleType_Owned) { //Only if it's an owned vehicle
			ShowAlarmFlagsMenu(playerid, carid);
			SendClientMessage(playerid, X11_RED2, "* Alarm running, vehicle won't start!");
			return 1;
		}
	}
	if(!IsACar(GetVehicleModel(carid))) {
		return 1;
	}
	if(VehicleInfo[carid][EVType] == EVehicleType_Uninit) {
		return 1;
	}
	if(VehicleInfo[carid][EVType] == EVehicleType_RentCar && VehicleInfo[carid][EVOwner] != playerid) {
		SendClientMessage(playerid, X11_RED2, "* You do not have the keys to this car!");
		SetPVarInt(playerid, "EngineCooldown", timenow);
		return 1;
	}else if(VehicleInfo[carid][EVFuel] < 1) {
		VehicleInfo[carid][EVEngine] = 0;
		SendClientMessage(playerid, X11_RED2, "* The vehicle is out of fuel!");
		format(msg, sizeof(msg), "* %s fails to start the engine.", GetPlayerNameEx(playerid,ENameType_RPName));
	} else if(vehicleIsTotaled(carid)) { 
		SendClientMessage(playerid, X11_RED2, "   Your vehicle is totaled, call a mechanic!");
		format(msg, sizeof(msg), "* %s fails to start the engine.", GetPlayerNameEx(playerid,ENameType_RPName));
	} else if(VehicleInfo[carid][EVEngine] == 0) {
		format(msg, sizeof(msg), "* %s starts the engine.", GetPlayerNameEx(playerid,ENameType_RPName));
		VehicleInfo[carid][EVEngine] = 1;
		bombOnVehicleEngine(carid);
	} else {
		format(msg, sizeof(msg), "* %s stops the engine.", GetPlayerNameEx(playerid,ENameType_RPName));
		VehicleInfo[carid][EVEngine] = 0;
	}
	SetPVarInt(playerid, "EngineCooldown", timenow);
	SetVehicleParamsEx(carid, VehicleInfo[carid][EVEngine], lights, false, doors, bonnet, boot, objective);
	ProxMessage(20.0, playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	return 1;
}
YCMD:uselock(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a car!");
		return 1;
	}
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this on this car!");
		return 1;
	}
	if(VehicleInfo[carid][EVOwner] != playerid && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		SendClientMessage(playerid, X11_TOMATO_2, "This isn't your vehicle!");
		return 1;
	}
	if(GetPVarType(playerid, "HoldingLock") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must buy a lock!");
		return 1;
	}
	new ELockType:locktype = ELockType:GetPVarInt(playerid, "HoldingLock");
	new ELockType:carlock = VehicleInfo[carid][EVLockType];
	if(carlock >= locktype) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot downgrade your lock!");
		return 1;
	}
	VehicleInfo[carid][EVLockType] = locktype;
	DeletePVar(playerid, "HoldingLock");
	query[0] = 0;//[128];
	format(query, sizeof(query), "UPDATE `playercars` SET `locktype` = %d WHERE `id` = %d",_:locktype,VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Vehicle Lock applied!");
	return 1;
}
stock LockName(ELockType:lockid)
{
	new name[16];
	switch(lockid)
	{
	    case ELockType_Default: format(name, sizeof(name), "Default");
	    case ELockType_Simple: format(name, sizeof(name), "Simple");
	    case ELockType_Remote: format(name, sizeof(name), "Remote");
	    case ELockType_Advanced: format(name, sizeof(name), "Advanced");
	    case ELockType_Satellite: format(name, sizeof(name), "Satellite");
	    case ELockType_HighTech: format(name, sizeof(name), "High-Tech");
	    case ELockType_TitaniumLaser: format(name, sizeof(name), "Titanium-Laser");
		case ELockType_BioMetric: format(name, sizeof(name), "Biometric");
		case ELockType_Impossible: format(name, sizeof(name), "Impossible");
	    default: format(name, sizeof(name), "Default");
	}
	return name;
}
public CheckGas() {
	for(new i=0;i<MAX_VEHICLES;i++) {
		new model = GetVehicleModel(i);
		if(model == 0 || !IsACar(model)) {
			continue;
		}
		if(VehicleInfo[i][EVEngine]) {
			new Float:X, Float:Y, Float:Z;
			GetVehicleVelocity(i, X, Y, Z);
			if(VehicleInfo[i][EVFuel] > 0) {
				if(X != 0.0 || Y != 0.0 || Z != 0.0) {
					VehicleInfo[i][EVFuel] -= 2;
				} else {
					VehicleInfo[i][EVFuel]--;
				}
			} else {
				VehicleInfo[i][EVFuel] = 0;
				VehicleInfo[i][EVEngine] = 0;
				syncVehicleParams(i);				
			}
		}
	}
}
public LockCar(carid)
{
	SetTimerEx("SetLights",800, false,"dd",carid,0);
	VehicleInfo[carid][EVLocked] = 1;
	if(VehicleInfo[carid][EVLockType] == ELockType_Remote) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_AlarmActivated;
	}
	syncVehicleParams(carid);	
	return 1;
}
public SetLights(carid, status) {
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,engine,status,alarm,doors,bonnet,boot,objective);
}

public UnlockCar(carid)
{
	SetTimerEx("SetLights",800, false,"dd",carid,0);
	VehicleInfo[carid][EVLocked] = 0;
	if(VehicleInfo[carid][EVLockType] == ELockType_Remote) {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_AlarmActivated;
	}
	syncVehicleParams(carid);
	return 1;
}
isValidModel(modelid) {
	return !(modelid < 400 || modelid > 611);
}

CreateFamilyCar(famid, model, c1, c2, ELockType:lock, Float:X, Float:Y, Float:Z, Float:Angle) {
	#pragma unused lock
	query[0] = 0;//[256];
	format(query, sizeof(query), "INSERT INTO `familycars` (`model`,`colour1`,`colour2`,`X`,`Y`,`Z`,`Angle`,`owner`) VALUES (%d,%d,%d,%f,%f,%f,%f,%d)",model,c1,c2,X,Y,Z,Angle,famid);
	mysql_function_query(g_mysql_handle, query, true, "onCreateFamilyVehicle", "ddddffff", famid, model, c1, c2, X, Y, Z, Angle);
}
DestroyFamilyCar(familyid, carid, byindex = 1) {
	if(byindex == 1)
		carid = getFamilyCar(familyid, carid);
	if(carid != -1) {
		query[0] = 0;//[256];
		format(query, sizeof(query), "DELETE FROM `familycars` WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		VehicleInfo[carid][EVOwner] = 0;
		VehicleInfo[carid][EVType] = EVehicleType_Uninit;
		DestroyVehicle(carid);
	}
}
getFamilyCar(familyid, carid) {
	new found;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_Family) {
			if(VehicleInfo[i][EVOwner] == familyid) {
				if(++found == carid) {
					return i;
				}
			}
		}
	}
	return -1;
}
public onCreateFamilyVehicle(famid, model, c1, c2, Float:X, Float:Y, Float:Z, Float:Angle) {
	new id;
	id = mysql_insert_id();
	new carid = CreateVehicle(model,X,Y,Z,Angle,c1,c2,DEF_RESPAWN_DELAY);
	if(!IsACar(GetVehicleModel(carid))) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVLocked] = 1;
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,VehicleInfo[carid][EVLocked],bonnet,boot,objective);
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVOwner] = famid;
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVLockType] = ELockType_Default;
	VehicleInfo[carid][EVSQLID] = id;
	VehicleInfo[carid][EVType] = EVehicleType_Family;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	clearTrunk(carid);
	format(VehicleInfo[carid][EVPlate],32,"IWRP");
	SetVehicleNumberPlate(carid, VehicleInfo[carid][EVPlate]);
	return 1;
}
setVehicleColour(carid, c1, c2) {
	ChangeVehicleColor(carid, c1, c2);
	VehicleInfo[carid][EVColour][0] = c1;
	VehicleInfo[carid][EVColour][1] = c2;
}
vehOnVehiclePaintjob(carid, playerid, paintjobid) {
	#pragma unused playerid
	VehicleInfo[carid][EVPaintjob] = paintjobid;
	if(paintjobid != -1) {
		setVehiclePaintJobFlag(carid, 1);
	} else {
		setVehiclePaintJobFlag(carid, 0);
	}
}
setVehiclePaintJobFlag(carid, enabled) {
	if(enabled == 1) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_PaintJob;
	} else {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_PaintJob;
	}
}
setVehicleTotaled(carid, enabled) {
	if(enabled == 1) {
		new engine, lights, alarm, doors, bonnet, boot, objective;
		GetVehicleParamsEx(carid, engine, lights, alarm, doors, bonnet, boot, objective);
		VehicleInfo[carid][EVEngine] = 0;
		SetVehicleParamsEx(carid, VehicleInfo[carid][EVEngine], lights, alarm, doors, bonnet, boot, objective);
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_Totaled;
		playVehicleHornForPlayers(carid);
	} else {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_Totaled;
	}
}
playVehicleHornForPlayers(carid) {
	new Float: X, Float: Y, Float: Z;
	GetVehiclePos(carid, X, Y, Z);
	foreach(Player, i) {
		if(IsPlayerInRangeOfPoint(i, 30.0, X, Y, Z)) {
			PlayerPlaySound(i, 1147, X, Y, Z);
			SetTimerEx("StopMusicForSinglePlayer", 10000, false, "d", i);
		}
	}
}
setVehicleAlarm(carid, enabled) { //This just tells us that the alarm for a certain vehicle is running
	if(enabled == 1) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_AlarmRunning;
	} else {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_AlarmRunning;
	}
	soundCarAlarm(carid, enabled);
}
setVehicleWiperFlag(carid, enabled) {
	if(enabled == 1) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_WipersOn;
	} else {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_WipersOn;
	}
	foreach(Player, i) {
		if(IsPlayerInVehicle(i, carid)) {
			weatherInVehicleDecide(i);
		}
	}
}
setVehiclePaintjob(carid,  paintjob) {
	ChangeVehiclePaintjob(carid, paintjob);
	VehicleInfo[carid][EVPaintjob] = paintjob;
}
getTrunkItem(carid, slot) {
	return VehicleTrunkInfo[carid][slot][EVehTrunkType];
}
getTrunkAmount(carid, slot) {
	return VehicleTrunkInfo[carid][slot][ETrunkAmount];
}
#pragma unused getTrunkAmount
#pragma unused getTrunkItem
YCMD:hood(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify a vehicles hood state");
		return 1;
	}
	new mode[32];
	new carid = GetClosestVehicle(playerid);
	if(carid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "No vehicles found");
		return 1;
	} else {
		if(DistanceBetweenPlayerAndVeh(playerid, carid) > 5.0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't near any vehicle!");
			return 1;
		}
		if(!IsACar(GetVehicleModel(carid)))
		{
		    SendClientMessage(playerid, COLOR_LIGHTRED, "   This vehicle doesn't have a hood!");
		    return 1;
		}
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine, lights, alarm, doors, bonnet, boot, objective);
	if(VehicleInfo[carid][EVLocked] == 1 && !(VehicleInfo[carid][EVLockType] == ELockType_BioMetric && VehicleInfo[carid][EVOwner] == playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This vehicle is locked!");
		return 1;
	}
	if(!sscanf(params,"s[32]",mode)) {
		if(!strcmp(mode,"open", true)) {
			if(bonnet == 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "The hood is already opened!");
				return 1;
			}
			SetVehicleParamsEx(carid,engine,lights,alarm,VehicleInfo[carid][EVLocked],1,boot,objective);
		} else if(!strcmp(mode,"close", true)) {
			if(bonnet != 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "The hood is already closed!");
				return 1;
			}			
			SetVehicleParamsEx(carid,engine,lights,alarm,VehicleInfo[carid][EVLocked],0,boot,objective);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /hood [mode]");
		SendClientMessage(playerid, X11_WHITE, "Modes: open, close");
	}
	return 1;
}
YCMD:lights(playerid, params[], help) {
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	
	}
	new carid = GetPlayerVehicleID(carid);
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine, lights, alarm, doors, bonnet, boot, objective);
	if(lights != 0) {
		lights = 1;
	} else {
		lights = 0;
	}
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Lights Toggled!");
	SetVehicleParamsEx(carid,engine, !lights, alarm, doors, bonnet, boot, objective);
	return 1;
}
YCMD:trunk(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify a vehicles trunk state");
		return 1;
	}
	if(IsOnDuty(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	new mode[32];
	new carid = !IsPlayerInAnyVehicle(playerid) ? GetClosestVehicle(playerid) : GetPlayerVehicleID(playerid);
	if(carid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "No vehicles found");
		return 1;
	} else {
		if(DistanceBetweenPlayerAndVeh(playerid, carid) > 5.0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't near any vehicle!");
			return 1;
		}
		if(!HasATrunk(GetVehicleModel(carid)))
		{
		    SendClientMessage(playerid, COLOR_LIGHTRED, "   This vehicle doesn't have a trunk!");
		    return 1;
		}
	}
	if(isVehicleStored(carid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "That vehicle is in storage so you can't do that!");
	    return 1;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine, lights, alarm, doors, bonnet, boot, objective);
	if(VehicleInfo[carid][EVLocked] == 1 && !(VehicleInfo[carid][EVLockType] == ELockType_BioMetric && VehicleInfo[carid][EVOwner] == playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This vehicle is locked!");
		return 1;
	}
	if(!sscanf(params,"s[32]",mode)) {
		if(!strcmp(mode,"open", true)) {
			if(boot == 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "The trunk is already opened!");
				return 1;
			}
			SetVehicleParamsEx(carid,engine,lights,alarm,VehicleInfo[carid][EVLocked],bonnet,1,objective);
		} else if(!strcmp(mode,"close", true)) {
			if(boot != 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "The trunk is already closed!");
				return 1;
			}			
			SetVehicleParamsEx(carid,engine,lights,alarm,VehicleInfo[carid][EVLocked],bonnet,0,objective);
		} else if(!strcmp(mode,"use", true)) {
			if(boot != 1 && !IsPlayerInAnyVehicle(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "The trunk isn't opened!");
				return 1;
			} else {
				if(GetPlayerState(playerid) != PLAYER_STATE_PASSENGER && IsPlayerInAnyVehicle(playerid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "Only passengers may use the trunk!");
					return 1;
				}
			}	
			showTrunkMenu(playerid, carid);
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /trunk [mode]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "modes: open, close, use");
	}
	return 1;
}
showTrunkMenu(playerid, carid) {
	SetPVarInt(playerid, "CarTrunk", carid);
	ShowPlayerDialog(playerid, EVehiclesDialog_TrunkMenu, DIALOG_STYLE_MSGBOX, "{00BF00}Take or Store?","{FFFFFF}Do you want to take an item or store an item in the trunk?","Take", "Store");
}
stock getTrunkSlotName(ETrunkType:type, amount) {
	new name[64];
	switch(type) {
		case ETrunkType_Empty: {
			name = "Empty";
		}
		case ETrunkType_Gun: {
			new gun, ammo;
			new gunname[32];
			decodeWeapon(amount, gun, ammo);
			amount = ammo;
			GetWeaponNameEx(gun, gunname, sizeof(gunname));
			format(name, sizeof(name), "%s - %s bullets",gunname,getNumberString(amount));
		}
		case ETrunkType_Money: {
			format(name, sizeof(name), "Money $%s", getNumberString(amount));
		}
		case ETrunkType_Pot: {
			format(name, sizeof(name), "Pot (%s grams)", getNumberString(amount));
		}
		case ETrunkType_Coke: {
			format(name, sizeof(name), "Coke (%s grams)", getNumberString(amount));
		}
		case ETrunkType_Meth: {
			format(name, sizeof(name), "Meth (%s grams)", getNumberString(amount));
		}
		case ETrunkType_MaterialsA: {
			format(name, sizeof(name), "Mats A (%s)", getNumberString(amount));
		}
		case ETrunkType_MaterialsB: {
			format(name, sizeof(name), "Mats B (%s)", getNumberString(amount));
		}
		case ETrunkType_MaterialsC: {
			format(name, sizeof(name), "Mats C (%s)", getNumberString(amount));
		}
		case ETrunkType_SpecialItem: {
			format(name, sizeof(name), "Special Item (%s)", GetCarryingItemName(amount));
		}
	}
	return name;
}
stock getTrunkTypePVarName(ETrunkType:type) {
	new name[64];
	switch(type) {
		case ETrunkType_Empty: {
			name = "Empty";
		}
		case ETrunkType_Gun: {
		}
		case ETrunkType_Money: {
			format(name, sizeof(name), "Money");
		}
		case ETrunkType_Pot: {
			format(name, sizeof(name), "Pot");
		}
		case ETrunkType_Coke: {
			format(name, sizeof(name), "Coke");
		}
		case ETrunkType_Meth: {
			format(name, sizeof(name), "Meth");
		}
		case ETrunkType_MaterialsA: {
			format(name, sizeof(name), "MatsA");
		}
		case ETrunkType_MaterialsB: {
			format(name, sizeof(name), "MatsB");
		}
		case ETrunkType_MaterialsC: {
			format(name, sizeof(name), "MatsC");
		}
		case ETrunkType_SpecialItem: {
			format(name, sizeof(name), "SpecialItem");
		}
	}
	return name;
}
stock getTrunkDisplayName(ETrunkType:type) {
	new name[64];
	switch(type) {
		case ETrunkType_Empty: {
			name = "Empty";
		}
		case ETrunkType_Gun: {
		}
		case ETrunkType_Money: {
			format(name, sizeof(name), "Money");
		}
		case ETrunkType_Pot: {
			format(name, sizeof(name), "Pot");
		}
		case ETrunkType_Coke: {
			format(name, sizeof(name), "Coke");
		}
		case ETrunkType_Meth: {
			format(name, sizeof(name), "Meth");
		}
		case ETrunkType_MaterialsA: {
			format(name, sizeof(name), "Materials A");
		}
		case ETrunkType_MaterialsB: {
			format(name, sizeof(name), "Materials B");
		}
		case ETrunkType_MaterialsC: {
			format(name, sizeof(name), "Materials C");
		}
		case ETrunkType_SpecialItem: {
			format(name, sizeof(name), "Special Item");
		}
	}
	return name;
}
showTrunkSlotMenu(playerid, take) {
	new car = GetPVarInt(playerid, "CarTrunk");
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<NUMBER_TRUNK_SLOTS;i++) {
		new amount = VehicleTrunkInfo[car][i][ETrunkAmount];
		if(VehicleTrunkInfo[car][i][EVehTrunkType] == ETrunkType_Gun) {
			new gun;
			decodeWeapon(amount,gun, amount);
		}
		format(tempstr, sizeof(tempstr), "%s\n",getTrunkSlotName(VehicleTrunkInfo[car][i][EVehTrunkType],VehicleTrunkInfo[car][i][ETrunkAmount]));
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	new title[32];
	if(take) {
		format(title, sizeof(title), "Take");
	} else {
		format(title, sizeof(title), "Store");
	}
	SetPVarInt(playerid, "TrunkTake", take);
	ShowPlayerDialog(playerid, EVehiclesDialog_TrunkSlotMenu, DIALOG_STYLE_LIST, title,dialogstr, "Use", "Cancel");
}
vehOnDialogResp(playerid, dialog, response, listitem, inputtext[]) {
	switch(dialog) {
		case EVehiclesDialog_TrunkMenu: {
			if(response) { //take
				showTrunkSlotMenu(playerid, 1);
			} else {
				showTrunkSlotMenu(playerid, 0);
			}
		}
		case EVehiclesDialog_TrunkSlotMenu: {
			if(!response) return 0;
			new take = GetPVarInt(playerid, "TrunkTake");
			new car = GetPVarInt(playerid, "CarTrunk");
			SetPVarInt(playerid, "TrunkSlot", listitem);
			if(take) {
				if(VehicleTrunkInfo[car][listitem][EVehTrunkType] == ETrunkType_Empty) {
					SendClientMessage(playerid, X11_TOMATO_2, "Theres nothing in here");
					return 1;
				}
				takeTrunkItem(playerid, car, listitem);
			} else {
				if(VehicleTrunkInfo[car][listitem][EVehTrunkType] != ETrunkType_Empty) {
					SendClientMessage(playerid, X11_TOMATO_2, "This slot is already occupied");
					return 1;
				}
				showStoreTrunkItemMenu(playerid, listitem);
			}
		}
		case EVehiclesDialog_TrunkPutMenu: {
			new carid, slot;
			slot = GetPVarInt(playerid, "TrunkSlot");
			carid = GetPVarInt(playerid, "CarTrunk");
			if(!response) return 0;
			switch(listitem) {
				case 0: {
					storeGunInTrunk(playerid,carid,slot);
				}
				case 1: {
					if(GetPVarInt(playerid, "Level") < 3) {
						SendClientMessage(playerid, X11_TOMATO_2, "You must be level 3 to store this!");
					} else {
						storeItemInTrunk(playerid, carid, slot, ETrunkType_Money);
					}
				}
				case 2: {
					storeItemInTrunk(playerid, carid, slot, ETrunkType_Pot);
				}
				case 3: {
					storeItemInTrunk(playerid, carid, slot, ETrunkType_Coke);
				}
				case 4: {
					storeItemInTrunk(playerid, carid, slot, ETrunkType_Meth);
				}
				case 5: {
					storeItemInTrunk(playerid, carid, slot, ETrunkType_MaterialsA);
				}
				case 6: {
					storeItemInTrunk(playerid, carid, slot,ETrunkType_MaterialsB);
				}
				case 7: {
					storeItemInTrunk(playerid, carid, slot, ETrunkType_MaterialsC);
				}
				case 8: {
					StoreSpecialItemInTrunk(playerid, carid, slot);
				}
			}
		}
		case EVehiclesDialog_TrunkAmount: {
			new pvarname[64],amount;
			amount = strval(inputtext);
			new ETrunkType:type = ETrunkType:GetPVarInt(playerid, "TrunkItemType");
			new carid, slot;
			slot = GetPVarInt(playerid, "TrunkSlot");
			carid = GetPVarInt(playerid, "CarTrunk");
			format(pvarname, sizeof(pvarname), "%s", getTrunkTypePVarName(type));
			new total = GetPVarInt(playerid, pvarname);
			if(amount < 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount");
				return 1;
			}
			if(total < amount) {
				format(pvarname, sizeof(pvarname), "You do not have enough! You are %d short",amount-total);
				SendClientMessage(playerid, X11_TOMATO_2, pvarname);
				return 1;
			}
			if(VehicleTrunkInfo[carid][slot][EVehTrunkType] != ETrunkType_Empty && VehicleTrunkInfo[carid][slot][EVehTrunkType] != type) {
				SendClientMessage(playerid, X11_TOMATO_2, "Something is already in here");
				return 1;
			}
			format(query, sizeof(query), "* %s puts some %s into the vehicle trunk",GetPlayerNameEx(playerid, ENameType_RPName),  getTrunkDisplayName(type));
			ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			total -= amount;
			SetPVarInt(playerid, pvarname, total);
			GiveMoneyEx(playerid, 0); //sync incase its money
			VehicleTrunkInfo[carid][slot][EVehTrunkType] = type;
			VehicleTrunkInfo[carid][slot][ETrunkAmount] += amount;
		}
		case EVehiclesDialog_SellCar: {
			new offerer = GetPVarInt(playerid, "CarSellOffer");
			new car = GetPVarInt(playerid, "CarSellCar");
			new price = GetPVarInt(playerid, "CarSellPrice");
			DeletePVar(playerid, "CarSellOffer");
			DeletePVar(playerid, "CarSellCar");
			DeletePVar(playerid, "CarSellPrice");
			new msg[128];
			if(!response) {
				SendClientMessage(offerer, X11_TOMATO_2, "* The user rejected the vehicle sell offer");
				return 0;
			} else {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
					SendClientMessage(offerer, X11_TOMATO_2, "The player didn't have enough money");
					return 1;
				}
				new Float:X, Float:Y, Float:Z;
				GetPlayerPos(playerid, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(offerer, 5.0, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
					return 1;
				}
				if(getNumOwnedCars(playerid) >= GetPVarInt(playerid, "MaxCars")) {
					SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more cars.");
					return 1;
				}
				GiveMoneyEx(playerid, -price);
				GiveMoneyEx(offerer, price);
				format(msg, sizeof(msg), "* %s takes out a pair of keys and hands it to %s",GetPlayerNameEx(offerer, ENameType_RPName),GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(20.0, playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
				setVehicleOwner(car, playerid);
			}
		}
		case EVehiclesDialog_EditAlarmFlags: {
			if(!response) {
				DeletePVar(playerid, "VehicleAlarmEditing");
				return 1;
			}
			new vehicleid = GetPVarInt(playerid, "VehicleAlarmEditing");
			new AlarmFlags:flag = AlarmFlagDescription[listitem][EAlarmFlagID];
			DeletePVar(playerid, "VehicleAlarmEditing");
			if(VehicleInfo[vehicleid][EAlarmFlags] & flag) {
				VehicleInfo[vehicleid][EAlarmFlags] &= ~flag;
			} else {
				VehicleInfo[vehicleid][EAlarmFlags] |= flag;
			}
			query[0] = 0;
			format(query, sizeof(query), "UPDATE `playercars` SET `alarmsettings` = %d WHERE `id` = %d",_:VehicleInfo[vehicleid][EAlarmFlags],VehicleInfo[vehicleid][EVSQLID]);
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			ShowAlarmFlagsMenu(playerid, vehicleid);
			tryDisableAlarm(playerid, vehicleid);
			return 1;
		}
		case EVehiclesDialog_StorageMenu: {
			if(!response) {
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: You closed the menu!");
				return 1;
			}
			new carid = findPlayerCar(playerid, listitem);
			if(carid != -1) {
				if(IsVehicleOccupied(carid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "[INFO]: The vehicle you're trying to store or spawn is occupied!");
					return 1;
				}
				if(isVehicleInImpound(carid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "[INFO]: The vehicle you're trying to store or spawn is impounded!");
					return 1;
				}
				if(VehicleInfo[carid][EVFlags] & EVehicleFlags_Stored) {
					VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_Stored;
					SetVehicleVirtualWorld(carid, 0);
					SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: Your vehicle has been spawned!");
				} else {
					VehicleInfo[carid][EVFlags] |= EVehicleFlags_Stored;
					SetVehicleVirtualWorld(carid, VehicleInfo[carid][EVSQLID]);
					SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: Your vehicle has been stored!");
				}
			}
			return 1;
		}
	}
	return 1;
}
tryDisableAlarm(playerid, vehicleid) {
	if(VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_WhiteWire && VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_RedWire && VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_MainPowerBox) {
		setVehicleAlarm(vehicleid, 0);
		UnlockCar(vehicleid);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: Alarm deactivated!");
	}
	return 1;
}
vehAlarmBroken(vehicleid) {
	if(~VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_WhiteWire && ~VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_RedWire && ~VehicleInfo[vehicleid][EAlarmFlags] & EAlarmFlags_MainPowerBox) {
		return 0;
	}
	return 1;
}
YCMD:sellcar(playerid, params[], help) {
	new user, price;
	if(!sscanf(params,"k<playerLookup>d", user, price)) {
		if(price < 0 || price > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		new msg[128];
		new carid = -1;
		if(!IsPlayerInAnyVehicle(playerid)) {
			carid = GetClosestVehicle(playerid);
		} else {
			carid = GetPlayerVehicleID(playerid);
		}
		if(carid == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Vehicle not found");
		} else {
			new Float:X, Float:Y, Float:Z;
			GetVehiclePos(carid, X, Y, Z);
			if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are too far");
				return 1;
			}
			if(user == playerid) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot sell a vehicle to yourself!");
				return 1;
			}
			new vw;
			GetPlayerPos(user, X, Y, Z);
			vw = GetPlayerVirtualWorld(user);
			if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z) || GetPlayerVirtualWorld(playerid) != vw) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are not near this person!");
				return 1;
			}
			if(VehicleInfo[carid][EVType] != EVehicleType_Owned || (VehicleInfo[carid][EVFlags] & EVehicleFlags_CantBeSold)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot sell this type of vehicle!");
				return 1;
			}
			if(VehicleInfo[carid][EVOwner] != playerid) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't own this car");
				return 1;
			}
		}
		SetPVarInt(user, "CarSellOffer", playerid);
		SetPVarInt(user, "CarSellCar", carid);
		SetPVarInt(user, "CarSellPrice", price);
		format(msg, sizeof(msg), "%s wants to sell you a %s for $%s",GetPlayerNameEx(playerid, ENameType_RPName), VehiclesName[GetVehicleModel(carid)-400], getNumberString(price));
		ShowPlayerDialog(user, EVehiclesDialog_SellCar, DIALOG_STYLE_MSGBOX, "{00BFFF}Car Sell Offer",msg,"Accept", "Decline");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Vehicle sell offer sent");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sellcar [playerid/name] [price]");
	}
	
	return 1;
}
YCMD:eject(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Eject a player from your car");
		return 1;
	}
	new user;
	if(!sscanf(params,"k<playerLookup>",user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be the driver!");
			return 1;
		}
		if(GetPlayerVehicleID(user) != GetPlayerVehicleID(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in the same vehicle!");
			return 1;
		}
		format(query, sizeof(query), "* You have thrown %s out of the car.", GetPlayerNameEx(user, ENameType_RPName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		format(query, sizeof(query), "* You have been thrown out the car by %s.", GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(user, COLOR_LIGHTBLUE, query);
		RemovePlayerFromVehicle(user);
		format(query, sizeof(query), "* %s has ejected %s from the vehicle.", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(user, ENameType_RPName));
		ProxMessage(30.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /eject [playerid/name]");
	}
	return 1;
}
takeTrunkItem(playerid, car, slot) {
	new ETrunkType:type = VehicleTrunkInfo[car][slot][EVehTrunkType];
	new msg[128];
	switch(type) {
		case ETrunkType_SpecialItem: {
			if(HasSpecialItem(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are already carrying a special item!");
				return 1;
			}
			new item = VehicleTrunkInfo[car][slot][ETrunkAmount];			
			VehicleTrunkInfo[car][slot][EVehTrunkType] = ETrunkType_Empty;
			VehicleTrunkInfo[car][slot][ETrunkAmount] = 0;
			GivePlayerItem(playerid, item);
			format(msg, sizeof(msg), "* %s takes a %s out of the trunk", GetPlayerNameEx(playerid, ENameType_RPName),GetCarryingItemName(item));
		}
		case ETrunkType_Gun: {
			new weapon, ammo;
			new hgun,hammo;
			decodeWeapon(VehicleTrunkInfo[car][slot][ETrunkAmount],weapon, ammo);
			GetPlayerWeaponDataEx(playerid, GetWeaponSlot(weapon), hgun, hammo);
			if(hgun != 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "You already have a gun in this slot!");
				return 1;
			}
			if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
				return 1;
			} else {
				GivePlayerWeaponEx(playerid, weapon, ammo);
			}
			format(msg, sizeof(msg), "* %s takes a %s out of the trunk", GetPlayerNameEx(playerid, ENameType_RPName),GunName[weapon]);
			VehicleTrunkInfo[car][slot][EVehTrunkType] = ETrunkType_Empty;
			VehicleTrunkInfo[car][slot][ETrunkAmount] = 0;
		}
		default: {
			new pvarname[64];
			format(pvarname, sizeof(pvarname), "%s", getTrunkTypePVarName(VehicleTrunkInfo[car][slot][EVehTrunkType]));
			VehicleTrunkInfo[car][slot][EVehTrunkType] = ETrunkType_Empty;
			new amount = GetPVarInt(playerid, pvarname);
			amount += VehicleTrunkInfo[car][slot][ETrunkAmount];
			SetPVarInt(playerid, pvarname, amount);
			GiveMoneyEx(playerid, 0);
			VehicleTrunkInfo[car][slot][ETrunkAmount] = 0;
			format(msg, sizeof(msg), "* %s takes a %s out of the vehicles trunk", GetPlayerNameEx(playerid, ENameType_RPName), getTrunkDisplayName(type));
		}
	}
	ProxMessage(25.0,playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	return 1;
}
storeItemInTrunk(playerid, carid, slot, ETrunkType:type) {
	#pragma unused carid
	#pragma unused slot
	SetPVarInt(playerid, "TrunkItemType", _:type);
	new msg[128];
	format(msg, sizeof(msg), "Enter how much of %s you would like to put in the trunk",getTrunkDisplayName(type));
	ShowPlayerDialog(playerid, EVehiclesDialog_TrunkAmount, DIALOG_STYLE_INPUT, "{00BF00}Enter Amount",msg,"Store", "Cancel");
	
}
StoreSpecialItemInTrunk(playerid, carid, slot) {
	new item = getPlayerCarryingItem(playerid);
	if(item == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not holding a special item!");
		return 1;
	}
	RemovePlayerItem(playerid);
	VehicleTrunkInfo[carid][slot][EVehTrunkType] = ETrunkType_SpecialItem;
	VehicleTrunkInfo[carid][slot][ETrunkAmount] = item;
	
	format(query, sizeof(query), "* %s put a %s into the vehicle trunk",GetPlayerNameEx(playerid, ENameType_RPName),GetCarryingItemName(item));
	ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 1;
}
storeGunInTrunk(playerid,carid,slot) {
	new weapon = GetPlayerWeaponEx(playerid);
	if(weapon == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be holding a weapon!");
		return 1;
	}
	new wepslot = GetWeaponSlot(weapon);
	new ammo;
	GetPlayerWeaponDataEx(playerid, wepslot, weapon, ammo);
	RemovePlayerWeapon(playerid, weapon);
	VehicleTrunkInfo[carid][slot][EVehTrunkType] = ETrunkType_Gun;
	VehicleTrunkInfo[carid][slot][ETrunkAmount] = encodeWeapon(weapon, ammo);
	format(query, sizeof(query), "* %s put a %s into the vehicle trunk",GetPlayerNameEx(playerid, ENameType_RPName),GunName[weapon]);
	ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 0;
}
showStoreTrunkItemMenu(playerid, slot) {
	SetPVarInt(playerid, "TrunkSlot", slot);
	ShowPlayerDialog(playerid, EVehiclesDialog_TrunkPutMenu, DIALOG_STYLE_LIST, "Trunk Store","Weapon\nCash\nPot\nCoke\nMeth\nMaterials A\nMaterials B\nMaterials C\nSpecial Item", "Use", "Cancel");
}
isInLEOVehicle(playerid) {
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		if(VehicleInfo[carid][EVType] == EVehicleType_Faction) {
			new owner = VehicleInfo[carid][EVOwner];
			if(IsLEOFaction(owner)) {
				return 1;
			}
		}
	}
	return 0;
}

isInMedicVehicle(playerid) {
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		if(VehicleInfo[carid][EVType] == EVehicleType_Faction) {
			new owner = VehicleInfo[carid][EVOwner];
			if(getFactionType(owner) == EFactionType_EMS) {
				return 1;
			}
		}
	}
	return 0;
}
isInGovernmentVehicle(playerid) {
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		if(VehicleInfo[carid][EVType] == EVehicleType_Faction) {
			new owner = VehicleInfo[carid][EVOwner];
			if(getFactionType(owner) == EFactionType_Government) {
				return 1;
			}
		}
	}
	return 0;
}
FactionType:getCarFactionType(carid) {
	if(VehicleInfo[carid][EVType] == EVehicleType_Faction) {
		return getFactionType(VehicleInfo[carid][EVOwner]);
	}
	return EFactionType_None;
}
#pragma unused getCarFactionType

loadTrunks(carid) {
	format(query, sizeof(query), "SELECT ");
	for(new i=0;i<NUMBER_TRUNK_SLOTS;i++) {
		format(tempstr, sizeof(tempstr), "`trunk%dtype`,`trunk%dnum`,",i, i);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), "FROM `%s` WHERE `id` = %d",getVehicleTableName(carid),VehicleInfo[carid][EVSQLID]);
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadTrunks", "d",carid);
}
forward OnLoadTrunks(carid);
public OnLoadTrunks(carid) {
	new rows, fields;
	new id_string[32];
	cache_get_data(rows, fields);
	for(new i,x;i<NUMBER_TRUNK_SLOTS;i++,x+=2) {
		cache_get_row(0, x, id_string);
		VehicleTrunkInfo[carid][i][EVehTrunkType] = ETrunkType:strval(id_string);
		cache_get_row(0, x+1, id_string);
		VehicleTrunkInfo[carid][i][ETrunkAmount] = strval(id_string);
	}
}
saveTrunks(carid) {
	format(query, sizeof(query), "UPDATE `%s` SET",getVehicleTableName(carid));
	for(new i=0;i<NUMBER_TRUNK_SLOTS;i++) {
		format(tempstr, sizeof(tempstr), "`trunk%dtype` = %d,`trunk%dnum` = %d,",i, _:VehicleTrunkInfo[carid][i][EVehTrunkType], i, VehicleTrunkInfo[carid][i][ETrunkAmount]);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " WHERE `id` = %d",VehicleInfo[carid][EVSQLID]);
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
setVehicleOwner(car, playerid) {
	if(VehicleInfo[car][EVType] != EVehicleType_Owned) {
		return 1;
	}
	VehicleInfo[car][EVOwner] = playerid;
	format(query, sizeof(query), "UPDATE `playercars` SET `owner` = %d WHERE `id` = %d",GetPVarInt(playerid, "CharID"), VehicleInfo[car][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 0;
}
Float:getLockDistance(ELockType:lock) {
	switch(lock) {
		case ELockType_Default, ELockType_Simple: {
			return 4.0;
		}
		case ELockType_Remote, ELockType_Advanced: {
			return 20.0;
		}
	}
	return 0.0;
}
vehOnVehicleRespray(playerid, vehicleid, color1, color2) {
	#pragma unused playerid
	VehicleInfo[vehicleid][EVColour][0] = color1;
	VehicleInfo[vehicleid][EVColour][1] = color2;
}
vehOnLoadPaintJob(vehicleid) {
	if(vehicleHasPaintJob(vehicleid)) { //Instead of repeating this twice where the vehicles create, I just place it here.
		ChangeVehiclePaintjob(vehicleid, VehicleInfo[vehicleid][EVPaintjob]);
	}
}
vehOnVehicleSpawn(vehicleid) {
	vehOnLoadPaintJob(vehicleid);
	VehicleInfo[vehicleid][EVFlags] &= ~EVehicleFlags_WindowsDown;
	VehicleInfo[vehicleid][EVFlags] &= ~EVehicleFlags_Totaled;
	if(VehicleInfo[vehicleid][EVType] == EVehicleType_Owned && VehicleInfo[vehicleid][EVToDelete] == 1) {
		if(playerHasCarInsurance(VehicleInfo[vehicleid][EVOwner])) {
			if(IsACar(GetVehicleModel(vehicleid))) {
				putVehicleInImpound(vehicleid,0);
				SendClientMessage(VehicleInfo[vehicleid][EVOwner], X11_LIGHTBLUE, "Your vehicle has been destroyed, you must reclaim it at the Insurance Company");	
			}
			else if(IsAFlyingVehicle(GetVehicleModel(vehicleid))) {
				putVehicleInImpound(vehicleid,3); //3 because 2 is the LSPD one I believe
				SendClientMessage(VehicleInfo[vehicleid][EVOwner], X11_LIGHTBLUE, "Your vehicle has been destroyed, you must reclaim it at the Airport Insurance Company");	
			}
		} else {
			DestroyOwnedVehicle(vehicleid);
			SendClientMessage(VehicleInfo[vehicleid][EVOwner], X11_LIGHTBLUE, "Your vehicle has been destroyed.");	
		}
		VehicleInfo[vehicleid][EVToDelete] = 0;
	} else if(VehicleInfo[vehicleid][EVType] == EVehicleType_Family) {
			VehicleInfo[vehicleid][EVLocked] = 1;//default to locked on load
			clearTrunk(vehicleid);
			syncVehicleParams(vehicleid);	
	} else {
		if(VehicleInfo[vehicleid][EVType] != EVehicleType_Owned) {
			VehicleInfo[vehicleid][EVFuel] = 100;
			VehicleInfo[vehicleid][EVLocked] = 0;
			VehicleInfo[vehicleid][EVEngine] = 0;
			clearTrunk(vehicleid);
		}
	}
	syncVehicleParams(vehicleid);
}
clearTrunk(carid) {
	for(new i=0;i<NUMBER_TRUNK_SLOTS;i++) {
		VehicleTrunkInfo[carid][i][EVehTrunkType] = ETrunkType_Empty;
		VehicleTrunkInfo[carid][i][ETrunkAmount] = 0;
	}
}
getCarRadio(carid) {
	return VehicleInfo[carid][EVRadioStation];
}
SetVehicleStation(carid, station) {
	new url[128];
	VehicleInfo[carid][EVRadioStation] = station;
	RadiosCustom[carid][ECustomRadio] = 0; //Set the custom radio to off
	getRadioURL(station, url, sizeof(url));
	foreach(Player, i) {
		if(IsPlayerInVehicle(i, carid)) {
			if(isValidRadionStation(station)) {
				PlayAudioStreamForPlayer(i, url);
			} else {
				StopAudioStreamForPlayer(i);
			}
		}
	}
}
SetCustomVehicleStation(carid, station[], active) {
	format(RadiosCustom[carid][ERadioURLCustom], 128-1, "%s", station);
	RadiosCustom[carid][ECustomRadio] = active;
	foreach(Player, i) {
		if(IsPlayerInVehicle(i, carid)) {
			if(active) {
				PlayAudioStreamForPlayer(i, RadiosCustom[carid][ERadioURLCustom]);
			} else {
				StopAudioStreamForPlayer(i);
			}
		}
	}
}
/*
YCMD:dropcar(playerid, params[], help) {
	if(!IsPlayerInRangeOfPoint(playerid, 15.0, -1733.27, 189.44, 3.55)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the drop point!");
		HintMessage(playerid, COLOR_CUSTOMGOLD, "Do /finddropcar to find it!");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can only do this with an owned vehicle!");
		return 1;
	}
	new msg[128];
	new value = floatround(getModelValue(GetVehicleModel(carid))*0.10);
	if(value == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't sell this!");
		return 1;
	}
	new time = GetPVarInt(playerid, "DropCarCooldown"); //todo: save this
	new timenow = gettime();
	if(DROPCAR_COOLDOWN-(timenow-time) > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait before doing this again!");
		return 1;
	}
	if(playerHasCarInsurance(VehicleInfo[carid][EVOwner])) {
		value = getVehicleReclaimPrice(carid);
		value += getInsurancePaydayPrice(playerid);
		value -= (250*getNumOwnedCars(playerid));
	}
	SetPVarInt(playerid, "DropCarCooldown", gettime());
	format(msg, sizeof(msg), "You sold this vehicle for $%s",getNumberString(value));
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	GiveMoneyEx(playerid, value);
	VehicleInfo[carid][EVToDelete] = 1;
	SetVehicleToRespawn(carid);
	return 1;	
}
*/
FindFamilyCar(famid, index, sqlid = 0) {
	new num;
	if(!sqlid) {
		famid = SQLIDFromFamily(famid);
	}
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_Family) {
			if(VehicleInfo[i][EVOwner] == famid) {
				if(num++ == index) {
					return i;
				}
			}
		}
	}
	return -1;
}
YCMD:finddropcar(playerid, params[], help) {
	SetPlayerCheckpoint(playerid, -1733.27, 189.44, 3.55,5.0);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
	return 1;
}
YCMD:respawnallcars(playerid, params[], help) {
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(GetVehicleModel(i) != 0) {
			if(VehicleInfo[i][EVType] != EVehicleType_Owned) {
				if(GetVehicleDriver(i) == INVALID_PLAYER_ID) {
					SetVehicleToRespawn(i);
				}
			}
		}
	}
	SendClientMessageToAll(COLOR_CUSTOMGOLD, "An admin has respawned all cars!");
	return 1;
}
destroyFamilyCars(id) {
	new carid, i;
	while((carid = FindFamilyCar(id,i++)) != -1) {
		DestroyFamilyCar(id, carid, 0);
	}
}
isVehicleInImpound(carid) {
	if(VehicleInfo[carid][EVFlags] & EVehicleFlags_InsuranceImpound) {
		return 1;
	}
	if(VehicleInfo[carid][EVFlags] & EVehicleFlags_LSPDImpounded) {
		return 2;
	}
	if(VehicleInfo[carid][EVFlags] & EVehicleFlags_AIRPImpound) {
		return 3;
	}
	return 0;
}
isVehicleStored(carid) {
	if(VehicleInfo[carid][EVFlags] & EVehicleFlags_Stored) {
		return 1;
	}
	return 0;
}
EVehicleFlags:getVehicleFlags(carid) {
	return VehicleInfo[carid][EVFlags];
}
YCMD:windows(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Control a vehicles windows");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a car!");
		return 1;
	}
	if(!carHasWindows(carid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This car doesn't have any windows!");
		return 1;
	}
	new msg[128];
	if(!vehicleWindowsDown(carid)) {
		VehicleInfo[carid][EVFlags] |= EVehicleFlags_WindowsDown;
		SetVehicleParamsCarWindows(carid, 0, 0, 0, 0);
		format(msg, sizeof(msg), "* %s rolls the window down.", GetPlayerNameEx(playerid,ENameType_RPName));
	} else {
		VehicleInfo[carid][EVFlags] &= ~EVehicleFlags_WindowsDown;
		SetVehicleParamsCarWindows(carid, 1, 1, 1, 1);
		format(msg, sizeof(msg), "* %s rolls the window up.", GetPlayerNameEx(playerid,ENameType_RPName));
	}
	ProxMessage(10.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 1;
}
vehicleWindowsDown(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_WindowsDown);
}
vehicleHasPaintJob(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_PaintJob);
}
vehicleIsTotaled(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_Totaled);
}
vehicleWipersOn(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_WipersOn);
}
vehicleAlarmRunning(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_AlarmRunning);
}
vehicleHasSirens(carid) {
	return _:(getVehicleFlags(carid) & EVehicleFlags_HasSirens);
}
syncVehicleParams(carid) {
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine, lights, alarm, doors, bonnet, boot, objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,VehicleInfo[carid][EVLocked],bonnet,boot,objective);
	if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
		if(VehicleInfo[carid][EVLockType] == ELockType_BioMetric) {
			SetTimerEx("UnlockCarForPlayer", 1000, false, "dd", VehicleInfo[carid][EVOwner],carid);
		}
	}
}
forward UnlockCarForPlayer(playerid, carid);
public UnlockCarForPlayer(playerid, carid) {
	SetVehicleParamsForPlayer(carid, playerid, 0, 0);
}
vehOnVehicleStreamIn(carid, forplayerid) {
	if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
		if(VehicleInfo[carid][EVLockType] == ELockType_BioMetric) {
			if(VehicleInfo[carid][EVOwner] == forplayerid) {
				SetVehicleParamsForPlayer(carid, forplayerid, 0, 0);
			}
		}
	}
}
IsVehicleOccupied(carid) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsPlayerInAnyVehicle(i)) {
				if(GetPlayerVehicleID(i) == carid) {
					return 1;
				}
			}
		}
	}
	return 0;
}
killVehiclePassengers(carid) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsPlayerInAnyVehicle(i)) {
				if(GetPlayerVehicleID(i) == carid) {
					SetPlayerHealthEx(i, 0.0);
				}
			}
		}
	}
}
isFactionCar(carid) {
	return VehicleInfo[carid][EVType] == EVehicleType_Faction;
}
isCaptureCar(carid) {
	return VehicleInfo[carid][EVType] == EVehicleType_CaptureCar;
}
isFlyCar(model) {
	return IsAFlyingVehicle(model);
}
vehOnPlayerEnterVehicle(playerid, vehicleid, ispassenger) {
	if(isFactionCar(vehicleid)) {
		if(!ispassenger) {
			if(GetVehicleDriver(vehicleid) != INVALID_PLAYER_ID) {
				new Float:X, Float:Y, Float:Z;
				GetPlayerPos(playerid, X, Y, Z);
				Z += 2.5;
				SetPlayerPos(playerid, X, Y, Z);
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot enter this vehicle!");
			}
		}
	} else {
		if(isPlayerDying(playerid) || isPlayerWounded(playerid)) { //anim bug abuse "fix"
			putPlayerInHospital(playerid);
		}
	}
}
IsFlyCar(carid) {
	return IsAFlyingVehicle(carid);
}
YCMD:removemods(playerid, params[], help) {
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(GetSpeed(playerid) > 0.0) {
		SendClientMessage(playerid, X11_TOMATO_2, "Stop the vehicle before doing this!");
		return 1;
	}
	if(VehicleInfo[carid][EVOwner] != playerid) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't own this car");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, Float:Angle, Float:vHealth;
	new panelstatus, doorstatus, lightstatus, tirestatus;
	GetVehicleDamageStatus(carid, panelstatus, doorstatus, lightstatus, tirestatus);
	new seat = GetPlayerVehicleSeat(playerid);
	GetVehicleHealth(carid, vHealth);
	removeCarComponents(carid);
	GetPlayerFacingAngle(playerid, Angle);
	GetPlayerPos(playerid, X, Y, Z);
	SetPlayerPos(playerid, X, Y, Z+1);
	SetPlayerFacingAngle(playerid, Angle);
	TogglePlayerControllableEx(playerid, 0);
	SetVehicleToRespawn(carid);
	SetVehiclePos(carid, X, Y, Z);
	SetVehicleZAngle(carid, Angle);
	PutPlayerInVehicleEx(playerid, carid, seat);
	TogglePlayerControllableEx(playerid, 1);
	SetVehicleHealth(carid, vHealth);
	UpdateVehicleDamageStatus(carid, panelstatus, doorstatus, lightstatus, tirestatus);
	ChangeVehiclePaintjob(playerid, 3);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Components Removed!");
	return 1;
}
YCMD:wipers(playerid, params[], help) {
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(!IsACar(GetVehicleModel(carid))) {
		SendClientMessage(playerid, X11_TOMATO_2, "This vehicle doesn't have any wipers!");
		return 1;
	}
	if(vehicleWipersOn(carid)) {
		SendClientMessage(playerid, X11_WHITE, "[INFO]: Wipers Off");
		setVehicleWiperFlag(carid, 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "[INFO]: Wipers On");
		setVehicleWiperFlag(carid, 1);
	}
	return 1;
}
YCMD:checktrunk(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays the contents of a vehicle trunk");
		return 1;
	}
	new carid;
	if(!sscanf(params, "d",carid)) {
		new model = GetVehicleModel(carid);
		if(model == 0) {
			SendClientMessage(playerid, X11_RED3, "Invalid Vehicle");
			return 1;
		}
		format(query, sizeof(query), "******* %s[%d] Trunk *******",VehiclesName[model-400], carid);
		SendClientMessage(playerid, X11_WHITE, query);
		for(new i=0;i<NUMBER_TRUNK_SLOTS;i++) {
			format(query, sizeof(query), "%d. %s",i+1,getTrunkSlotName(VehicleTrunkInfo[carid][i][EVehTrunkType],VehicleTrunkInfo[carid][i][ETrunkAmount]));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /checktrunk [carid]");
	}
	return 1;
}
YCMD:carid(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays yours/a persons current Car ID");
		return 1;
	}
	new target;
	if(!sscanf(params,"k<playerLookup_acc>",target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
	} else {
		target = playerid;
	}
	new carid = GetPlayerVehicleID(target);
	new seat = GetPlayerVehicleSeat(target);
	new lastcar = GetPVarInt(target, "LastCar");
	new numcars = GetNumVehiclesPlayerIn(target);
	format(query, sizeof(query), "* CarID: %d[%d]: LastCar: %d IsInCar: %d NumCars: %d",carid,seat,lastcar,IsPlayerInAnyVehicle(target),numcars);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	return 1;
}
getTotalVehiclesValue(playerid) {
	new value;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(VehicleInfo[i][EVOwner] == playerid && VehicleInfo[i][EVType] == EVehicleType_Owned) {
			value += getModelValue(GetVehicleModel(i));
			for(new j=0;j<NUMBER_TRUNK_SLOTS;j++) {
				if(VehicleTrunkInfo[i][j][EVehTrunkType] == ETrunkType_Money) {
					value += VehicleTrunkInfo[i][j][ETrunkAmount];
				}
			}
		}
	}
	return value;
}
YCMD:carprices(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Display a vehicle models value");
		return 1;
	}
	new id;
	if(!sscanf(params,"d",id)) {
		if(!(id >= 400 && id <= 611)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle ID.");
			return 1;
		}
		format(query, sizeof(query), "* %s : $%s", VehiclesName[id-400],getNumberString(getModelValue(id)));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, query);
	} else {
		new value;
		for(new i=400;i<=611;i++) {
			value = getModelValue(i);
			if(value == 0) {
				format(query, sizeof(query), "* %s - %d", VehiclesName[i-400],i);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, query);
			}
		}
	}
	return 1;
}
YCMD:cleartrunk(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Clears a cars trunk");
		return 1;
	}
	new id;
	if(!sscanf(params, "d", id)) {
		if(GetVehicleModel(id) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle!");
			return 1;
		}
		clearTrunk(id);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Trunk Cleared!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /cleartrunk [carid]");
	}
	return 1;
}
carHasTrunk(carid) {
	return HasATrunk(GetVehicleModel(carid));
}
carHasWindows(carid) {
	new model = GetVehicleModel(carid);
	return (HasATrunk(model) || IsACar(model)) && !IsABike(model) && !IsA1Seat(model);
}
removeCarComponents(carid) {
	new component;
	for(new i=0;i<13;i++) {
		component = GetVehicleComponentInSlot(carid, i);
		if(component != 0)
			RemoveVehicleComponent(carid,component);
	}
	setVehiclePaintJobFlag(carid, 0); //Removes the paintjob
}
PlayerIsInTheirCar(playerid) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid != INVALID_VEHICLE_ID) {
		if(VehicleInfo[carid][EVType] == EVehicleType_Owned) {
			if(VehicleInfo[carid][EVOwner] == playerid) {
				return 1;
			}
		}
	}
	return 0;
}
isNotADBWeapon(weaponid) {
	for(new i=0;i<sizeof(BadDrivebyWeapons);i++) {
		if(BadDrivebyWeapons[i] == weaponid) { 
			#if debug
			printf("%d is not a drive by weapon", BadDrivebyWeapons[i]);
			#endif
			return 1;
		}
	}
	return 0;
}

//shamal stuff

PutPlayerInShamal(playerid, vehicleid) {
	if(GetPlayerVehicleID(playerid) != INVALID_VEHICLE_ID) return 0;
	SetPlayerPos(playerid, 2.408077, 23.163587, 1199.593750);
	SetPlayerFacingAngle(playerid, 98.281707);
	SetPlayerInterior(playerid, 1);
	SetPlayerVirtualWorld(playerid, vehicleid);
	return 1;
}
ExitShamal(playerid) {
	new shamal = IsPlayerInShamal(playerid);
	if(shamal == 0) return 0;
	if(GetSpeed(shamal) > 0.0) {
		GivePlayerWeaponEx(playerid, 46, -1);
	}
	new Float:X, Float:Y, Float:Z;
	GetVehiclePos(shamal, X, Y, Z);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, 0);
	SetPlayerPos(playerid, X, Y, Z+10.0);
	return 1;
}
IsPlayerInShamal(playerid) {
	new vw = GetPlayerVirtualWorld(playerid);
	if(vw) {
		if(IsPlayerInRangeOfPoint(playerid, 55.0,2.408077, 23.163587, 1199.593750)) {
			return vw;
		}
	}
	return 0;
}

ShamalTryEnterExit(playerid) {
	new Float:X, Float:Y, Float:Z;
	if(IsPlayerInShamal(playerid)) {
		if(IsPlayerInRangeOfPoint(playerid, 5.0, 2.408077, 23.163587, 1199.593750)) {
			ExitShamal(playerid);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You must go to the shamal exit point (where you spawned).");
		}
		return 1;
	}
	for(new i=0;i<MAX_VEHICLES;i++) {
		GetVehiclePos(i, X, Y, Z);
		if(IsPlayerInRangeOfPoint(playerid, 25.0, X, Y, Z)) {
			if(GetVehicleModel(i) == 519) {
				//if(GetVehicleDriver(i) != INVALID_PLAYER_ID)
				if(VehicleInfo[i][EVLocked] == 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "* The shamal is locked");
					return 1;
				}
				PutPlayerInShamal(playerid, i);
			}
		}
	}
	return 0;
}


CreatePlayerRentCar(playerid, model, Float:X, Float:Y, Float:Z) {
	new carid = CreateVehicle(model,X,Y,Z,0.0,-1,-1,DEF_RESPAWN_DELAY);
	if(!IsACar(model)) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVOwner] = playerid;
	VehicleInfo[carid][EVColour][0] = -1;
	VehicleInfo[carid][EVColour][1] = -1;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVLockType] = ELockType_Default;
	VehicleInfo[carid][EVSQLID] = -1;
	VehicleInfo[carid][EVType] = EVehicleType_RentCar;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	PutPlayerInVehicleEx(playerid, carid, 0);
}
CreateCTVCar(model, Float:X, Float:Y, Float:Z, Float:VehHealth) {
	new carid = CreateVehicle(model,X,Y,Z,0.0,-1,-1,DEF_RESPAWN_DELAY);
	if(!IsACar(model)) {
		VehicleInfo[carid][EVEngine] = 1;
	} else {
		VehicleInfo[carid][EVEngine] = 0;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	SetVehicleParamsEx(carid,VehicleInfo[carid][EVEngine],lights,alarm,doors,bonnet,boot,objective);
	VehicleInfo[carid][EVToDelete] = 0;
	VehicleInfo[carid][EVOwner] = 0;
	VehicleInfo[carid][EVColour][0] = -1;
	VehicleInfo[carid][EVColour][1] = -1;
	VehicleInfo[carid][EVFuel] = 100;
	VehicleInfo[carid][EVRadioStation] = -1;
	VehicleInfo[carid][EVLockType] = ELockType_Default;
	VehicleInfo[carid][EVSQLID] = -1;
	VehicleInfo[carid][EVType] = EVehicleType_CaptureCar;
	VehicleInfo[carid][EVFlags] = EVehicleFlags:0;
	VehicleInfo[carid][EVCreationTime] = gettime();
	SetVehicleHealth(carid, VehHealth);
	return carid;
}
NumPlayerRentCars(playerid) {
	new x;
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				x++;
			}
		}
	}
	return x;
}

DestroyAllRentCars(playerid) {
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				DestroyRentCar(i);
			}
		}
	}
}
DestroyPlayerRentCar(playerid, caridx) {
	new x;
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				if(x++ == caridx) {
					DestroyRentCar(i);
				}
			}
		}
	}
}
DestroyRentCar(carid) {
	VehicleInfo[carid][EVOwner] = 0;
	VehicleInfo[carid][EVType] = EVehicleType_Uninit;
	DestroyVehicle(carid);
}
DestroyCaptureCar(carid) {
	VehicleInfo[carid][EVOwner] = 0;
	VehicleInfo[carid][EVType] = EVehicleType_Uninit;
	DestroyVehicle(carid);
}
DestroyAllCaptureCars(playerid) {
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_CaptureCar) {
			DestroyCaptureCar(i); //We don't really care about who's the owner here..
		}
	}
}
GetTotalRentCarPrice(playerid) {
	new price;
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				price += GetRentModelPrice(GetVehicleModel(i));
			}
		}
	}
	return price;
}
GetVehicleSQLID(carid) {
	if(carid < 0 || carid > MAX_VEHICLES) return 0;
	return VehicleInfo[carid][EVSQLID];
}
PlayerOwnsCar(carid, playerid) {
	if(carid < 0 || carid > MAX_VEHICLES) return 0;
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		return 0;			
	}
	if(VehicleInfo[carid][EVOwner] != playerid) {
		return 0;
	}
	return 1;
}
isBannedVehicle(modelid) {
	for(new i=0;i<sizeof(bannedvehicles);i++) {
		if(bannedvehicles[i] == modelid) { 
			#if debug
			printf("%d is a banned vehicle", bannedvehicles[i]);
			#endif
			return 1;
		}
	}
	return 0;
}
vehOnUpdateDamage(carid, playerid) {
	#pragma unused playerid
	new Float:vHealth;
	new msg[128];
	GetVehicleHealth(carid, vHealth);
	if(VehicleInfo[carid][EVToDelete] == 1) return 1; //Don't do anything in this case..
	if(vHealth >= 1 && vHealth <= 500.0) {
		if(!vehicleIsTotaled(carid)) {
			new carmodel = GetVehicleModel(carid);
			format(msg, sizeof(msg), "The ~r~%s ~w~appears to be ~r~totaled.", VehiclesName[carmodel-400]);
			sendScriptMessageToVehicle(carid, msg);
			setVehicleTotaled(carid, 1);
		}
		SetVehicleHealth(carid, 600.0);
	}
	return 1;
}
sendScriptMessageToVehicle(carid, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsPlayerInAnyVehicle(i)) {
				if(GetPlayerVehicleID(i) == carid) {
					ShowScriptMessage(i, msg);
				}
			}
		}
	}
}
vehDontExplode() {
	new Float:vHealth;
	for(new i=0;i<MAX_VEHICLES;i++) {
		GetVehicleHealth(i, vHealth);
		if(VehicleInfo[i][EVToDelete] == 1) return 1; //Don't do anything in this case..
		if(vHealth >= 1 && vHealth <= 500.0) {
			SetVehicleHealth(i, 600.0);
		}
	}
	return 1;
}
//Vehicle misc functions
task updateVehicles[500] () {
	vehDontExplode();
}
//Vehicle GUI functions
task UpdateVehGUI[100] () {
	foreach(Player,i) {
    	if(GetPlayerState(i) == PLAYER_STATE_DRIVER) {
    		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(i, "AccountFlags");
			if(aflags & EAccountFlags_MovieMode) { //If the movie mode is on, don't do anything
				hideVehGUI(i);
				return 1;
			} else {
				updateVehicleGUI(i);
			}
		}
	}
	return 1;
}
updateVehicleGUI(playerid) {
	new string[128], tmpstr[64];
	if(!IsPlayerInAnyVehicle(playerid)) {
		return 1;
	}
	new vehGUISpeedo = GetPVarInt(playerid, "vehGUISpeedo");
	PlayerTextDrawHide(playerid, PlayerText:vehGUISpeedo); //Hide it first so it doesn't overlap
	new newcar = GetPlayerVehicleID(playerid);
	new newmodel = GetVehicleModel(newcar);
	format(tmpstr, sizeof(tmpstr), "%.2f mi", getVehicleMileAge(newcar));
	format(string,sizeof(string),"%s ~l~/~w~ %d L ~l~/~w~ %d km/h ~l~/~w~ %s",VehiclesName[newmodel-400], VehicleInfo[newcar][EVFuel], GetSpeed(playerid), !getVehicleMileAge(newcar) ? ("N/A") : tmpstr);
	PlayerTextDrawSetString(playerid, PlayerText:vehGUISpeedo, string);
	PlayerTextDrawShow(playerid, PlayerText:vehGUISpeedo);
	return 1;
}
hideVehGUI(playerid) { //This gets called when a player exits a vehicle and hides all the TextDraws with the EType_VehGUI category
	new TextDrawIndex;
	for(new i=0; i < sizeof(TextDrawProperties); i++) {
		if(TextDrawProperties[i][ETDrawType] == EType_VehGUI) {
			TextDrawIndex = GetPVarInt(playerid, TextDrawProperties[i][TextDrawName]);
			PlayerTextDrawHide(playerid, PlayerText:TextDrawIndex); //Hide it
		}
	}
	return 1;
}
//End of Vehicle GUI