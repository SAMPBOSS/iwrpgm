
#define MAX_FACTION_RANKS 16
#define MAX_FACTION_NAME 32

#define FACTION_OOC_COLOUR 0x7BDDA5AA

enum EFactionInfo {
	EFactionName[MAX_FACTION_NAME],
	EFactionNumRanks,
	FactionType:EFactionType
};

enum {
	EFactionDialog_EquipChoose = EFactionsDialog_Base + 1,
	EFactionDialog_ArrestDialog,
	EFactionsDialog_CustomArrest,
	EFactionsDialog_BailPrice,
	EFactionsDialog_InviteDialog,
	EFactionDialog_LiveOffer,
	EFactionsDialog_CopPlace,
	EFactionsDialog_LEOCompOptions,
	EFactionsDialog_LEOReqBackups,
	EFactionsDialog_ShowBizList,
};


new Factions[][EFactionInfo] = {{"None",1, EFactionType_None},
								{"LAPD",16,EFactionType_LEO},
								{"FBI",8,EFactionType_LEO},
								{"Government",16,EFactionType_Government},
								{"Fox News", 6, EFactionType_SanNews},
								{"LAFD",11,EFactionType_EMS},
								{"EPHS",6,EFactionType_Education},
								{"LADOT",6,EFactionType_Transport},
								{"CADOC",8,EFactionType_LEO},
								{"Waste Management", 6, EFactionType_WasteMgmt}
								//{"SADPS",6,EFactionType_LEO},
								//{"LS City Transit Police",8,EFactionType_LEO}
								//Add new factions at the end, not in between other factions since it just messes up the id's
								};
						
new RankTitles[sizeof(Factions)][MAX_FACTION_RANKS][64] = {{"None","None","None","None","None","None","None","None","None","None","None","None","None","None","None","None"},
															{"Officer in Training", "Police Officer I", "Police Officer II", "Police Officer III", "Detective I", "Sergeant", "Detective II", "Staff Sergeant", "Detective III", "Lieutenant I", "Lieutenant II", "Captain I", "Captain II", "Commander", "Assistant Chief", "Chief Of Police"},
															{"Probie","Intern","Staff","Agent","Senior Agent","Special Agent","Deputy Director","Director","None","None","None","None","None","None","None","None"},
															{"Intern","Attorney","Judge","Chauffeur","Secretary","Secretary of Health","Secretary of Education","Secretary of Housing","Head Secretary","Guard","SS Agent","SS Special Agent","SS Director","Chief of Staff","Mayor's Assistant","Mayor"},
															{"Intern","Local Reporter","Local Editor","Network Anchor","Network Editor","Network Producer","None","None","None","None","None","None","None","None","None","None"},
															{"Trainee","Probationary","FF/EMT-B","FF/EMT-I","FF/EMT-A","Lieutenant","Captain","Battalion Commander","Battalion Chief","Deputy Fire Chief","Fire Chief","None","None","None","None","None"},
															{"Janitor","Substitute Teacher","Teacher","Campus Aid","Vice Principal","Principal","None","None","None","None","None","None","None","None","None","None"},
															{"Aspirer","Private Bus Driver","School Bus Driver","Commercial Bus Driver","Secretary","Director","None","None","None","None","None","None","None","None","None", "None"},
															{"Officer in Training","Intern","Staff","Police Officer I","Police Officer II","Police Officer III","Deputy Director","Director","None","None","None","None","None","None","None","None"},
															{"Cleaner","Assistant","Inspector","Supervisor","Secretary","Director","None","None","None","None","None","None","None","None","None", "None"}
															//Add new faction ranks at the end, not in between other factions since it just messes up the id's
														};
															//{"Trooper in Training", "Trooper", "Honorary Trooper", "Senior Trooper", "HP Trooper", "HP Sergeant" , "TET Trooper", "TET Sergeant", "Sergeant", "Staff Sergeant","Lieutenant", "Captain", "Major", "Commander","Assistant Commisioner","Commisioner"},
															//{"Police Officer","Field Training Officer","Sergeant","Lieutenant","Captain","Inspector","Deputy Chief","Chief","None","None","None","None","None","None","None","None"}};

new FactionOff[sizeof(Factions)] = 0;

enum E_PoliceComputers {
	E_DialogOptionText[128],
	E_Enabled, //Bool:1 - Enables the item. 0 - Disables it (Not being used at the moment)
	E_DoLEOCallBack[128],
}
new ComputerDialogs[][E_PoliceComputers] = {
	{"Request Backup", 1, "requestLEOBackup"},
	{"Answer Backup", 1, "answerLEOBackup"},
	{"Cancel Backup", 1, "cancelLEOBackup"},
	{"Find Business Address", 1, "displayLEOBizList"}
};

#include "factions\government.pwn"
#include "factions\leo.pwn"
#include "factions\news.pwn"
//#include "factions\hitman.pwn"
#include "factions\medic.pwn"
#include "factions\waste.pwn"

factionsOnGameModeInit() {
	for(new i=1;i<sizeof(Factions);i++) {
		loadFactionVehicles(i);
	}
	leoOnGameModeInit();
}
factionTryEnterExit(playerid) {
	leoTryEnterExit(playerid);
}
YCMD:makefactionleader(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone a faction leader");
		return 1;
	}
	new factionid, user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>d", user, factionid)) {
		if(factionid < 0 || factionid > sizeof(Factions)-1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Faction");
			return 1;
		}
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPVarInt(user, "Faction", factionid);
		SetPVarInt(user, "Rank", Factions[factionid][EFactionNumRanks]);
		SetPVarInt(user, "Family", 0);
		format(msg, sizeof(msg), "* Admin %s has given you control over %s",GetPlayerNameEx(playerid, ENameType_AccountName), Factions[factionid][EFactionName]);
		SendClientMessage(user, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* Admin %s has given %s control over %s",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName), Factions[factionid][EFactionName]);
		SendFactionMessage(factionid, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* You gave %s control over %s",GetPlayerNameEx(user, ENameType_CharName), Factions[factionid][EFactionName]);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makefactionleader [playerid/name] [faction]");
	}
	return 1;
}
YCMD:makefactionmember(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone a faction leader");
		return 1;
	}
	new faction, user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>d", user, faction)) {
		if(faction < 0 || faction > sizeof(Factions)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Faction");
			return 1;
		}
		SetPVarInt(user, "Faction", faction);
		SetPVarInt(user, "Family", 0);
		SetPVarInt(user, "Rank", 1);
		format(msg, sizeof(msg), "* Admin %s has forced you into %s",GetPlayerNameEx(playerid, ENameType_AccountName), Factions[faction][EFactionName]);
		SendClientMessage(user, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* Admin %s has forced %s into %s",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName), Factions[faction][EFactionName]);
		SendFactionMessage(faction, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* You forced %s into %s",GetPlayerNameEx(user, ENameType_CharName), Factions[faction][EFactionName]);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makefactionmember [playerid/name] [faction]");
	}
	return 1;
}
YCMD:finvite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Faction Invite");
		return 1;
	}
	new user;
	new msg[128];
	if(GetPVarInt(playerid, "Faction") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction");
		return 1;
	}
	if(GetPVarInt(playerid, "Rank") < 6) {
		if(GetPVarInt(playerid, "Faction") != 1 || GetPVarInt(playerid, "Rank") < 11) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a high enough rank");
			return 1;
		}
	}
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarInt(user, "Faction") != 0 || GetPVarInt(user, "Family") != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is in a faction or family!");
			return 1;
		}
		new arrests = GetPVarInt(user, "Arrests");
		if(arrests > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "The person you're trying to invite has a previous criminal record!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 5.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		sendFactionInvite(GetPVarInt(playerid, "Faction"), user);
		format(msg, sizeof(msg), "* You invited %s into the faction", GetPlayerNameEx(user, ENameType_RPName_NoMask));
		SendClientMessage(playerid, COLOR_LIGHTBLUE,msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /invite [playerid]");
	}
	return 1;
}
YCMD:funinvite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Uninvite someone from a faction");
		return 1;
	}
	new user,msg[128];
	if(GetPVarInt(playerid, "Faction") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction");
		return 1;
	}
	if(GetPVarInt(playerid, "Rank") < 6) {
		if(GetPVarInt(playerid, "Faction") != 1 || GetPVarInt(playerid, "Rank") < 11) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a high enough rank");
			return 1;
		}
	}
	new fid = GetPVarInt(playerid, "Faction");
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarInt(user, "Faction") != fid) {
			SendClientMessage(playerid, X11_TOMATO_2, "This member is not in your faction");
			return 1;
		}
		format(msg, sizeof(msg), "* %s has been kicked from the faction by %s",GetPlayerNameEx(user, ENameType_RPName_NoMask), GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
		SendFactionMessage(fid, COLOR_LIGHTBLUE, msg);
		SetPVarInt(user, "Faction", 0);
		SetPVarInt(user, "Rank", 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /uninvite [playerid]");
	}
	return 1;
}
YCMD:quitfaction(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for quitting your faction");
		return 1;
	}
	new msg[128];
	new fid = GetPVarInt(playerid, "Faction");
	if(fid == 0) {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "You aren't in a faction!");
		return 1;
	}
	format(msg, sizeof(msg), "%s quit %s.", GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetFactionName(fid));
	FamilyMessage(fid, COLOR_LIGHTBLUE, msg);
	SetPVarInt(playerid, "Faction", 0);
	SetPVarInt(playerid, "Rank", 0);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You have quit your faction");
	return 1;
}
sendFactionInvite(id, player) {
	dialogstr[0] = 0;
	format(dialogstr,sizeof(dialogstr),"You have been invited into \"%s\". Do you wish to join?",GetFactionName(id));
	SetPVarInt(player, "FactionInvite", id);
	ShowPlayerDialog(player, EFactionsDialog_InviteDialog, DIALOG_STYLE_MSGBOX, "{00BFFF}Faction Invite", dialogstr, "Accept", "Decline");
	return 1;
}
YCMD:radio(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a radio message to your faction");
		return 1;
	}
	if(isPlayerFrozen(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this while frozen!");
		return 1;
	}

	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	if(!IsValidFaction(faction) || faction == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction!");
		return 1;
	}	
	if(isInJail(playerid) != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this in jail!");
		return 1;
	}
	new string[128];
	new msg[128];
	if(!sscanf(params,"s[128]",msg)) {
		jailIfOOC(playerid, msg);
	    format(string, sizeof(string), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
		ProxMessage(20.0, playerid, string,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
		if(IsRadioJammerActiveInArea(playerid)) {
			format(string, sizeof(string), "* You hear static coming from the radio");
			ProxMessage(20.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			return 1;
		}
		format(string, sizeof(string), "** (Radio) %s %s: %s **", getFactionRankName(faction, rank), GetPlayerNameEx(playerid, ENameType_RPName_NoMask), msg);
		SendFactionMessage(faction, TEAM_BLUE_COLOR, string);
		SendRadioMsgToListeners(playerid, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /radio [message]");
	}
	return 1;
}
YCMD:faction(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a faction message");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	if(!IsValidFaction(faction) || faction == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction!");
		return 1;
	}	
	new string[128];
	new msg[128];
	if(isInJail(playerid) == 1 || FactionOff[faction] == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this right now!");
		return 1;
	}
	if(!sscanf(params,"s[128]",msg)) {
		format(string, sizeof(string), "** (( %s %s: %s )) **", getFactionRankName(faction, rank), GetPlayerNameEx(playerid, ENameType_RPName_NoMask), msg);
		SendFactionMessage(faction, FACTION_OOC_COLOUR, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /faction [message]");
	}
	return 1;
}
YCMD:factionoff(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enables / Disables \"/f\" in factions (Until server restart).");
		return 1;
	}
	new factionid;
	new msg[128];
	if(!sscanf(params, "d", factionid)) {
		if(factionid < 0 || factionid > sizeof(Factions)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid faction. Please use \"/factions\" to get the faction ID.");
			return 1;
		}
		if(FactionOff[factionid] != 1) {
			FactionOff[factionid] = 1;
		} else {
			FactionOff[factionid] = 0;
		}
		format(msg, sizeof(msg), "Notice: Faction chat %s for %s.", FactionOff[factionid] == 1 ? ("disabled") : ("enabled"), Factions[factionid][EFactionName]);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /factionoff [FactionID]");
	}
	return 1;
}
YCMD:factionlist(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all faction members");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "**** FACTION MEMBERS ***");
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_FactionAdmin) {
		sscanf(params, "d", faction);
		if(!IsValidFaction(faction)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Faction!");
			return 1;
		}
	}
	if(faction == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction");
		return 1;
	}
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Faction") == faction) {
				format(msg, sizeof(msg), "* %s Rank %d",GetPlayerNameEx(i, ENameType_CharName), GetPVarInt(i, "Rank"));
				SendClientMessage(playerid, X11_WHITE, msg);
			}
		}
	}
	return 1;
}
stock GetFactionName(faction) {
	new name[32];
	if(faction < 0 || faction > sizeof(Factions)) faction = 0;
	format(name, sizeof(name), "%s", Factions[faction][EFactionName]);
	return name;
}
SendFactionMessage(faction, color, const msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "BigFactionEars") == 1 || GetPVarInt(i, "Faction") == faction) {
			if(!IsRadioJammerActiveInArea(i) || color != TEAM_BLUE_COLOR)
				SendClientMessage(i, color, msg);
		}
	}
}
getFactionRankName(fid, rankid) {
	new name[MAX_FACTION_NAME];
	if(rankid < 1 || rankid > MAX_FACTION_RANKS || fid < 0 || fid > sizeof(Factions)) {
		strmid(name,"None",0,4, sizeof(name));
		return name;
	}
	strmid(name,RankTitles[fid][rankid-1],0,strlen(RankTitles[fid][rankid-1]),MAX_FACTION_NAME);
	if(strlen(name) < 1) {
		strmid(name,"None",0,4, sizeof(name));
	}
	return name;
}
FactionType:getPlayerFactionType(playerid) {
	return Factions[GetPVarInt(playerid, "Faction")][EFactionType];
}
factionsOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	switch(dialogid) {
		case EFactionDialog_EquipChoose: {
			if(response) {
				onEquipResponse(playerid, listitem);
			}
		}
		case EFactionDialog_ArrestDialog: {
			handleArrestChoose(playerid, listitem,response);
		}
		case EFactionsDialog_CustomArrest: {
			handleCustomArrest(playerid, inputtext,response);
		}
		case EFactionsDialog_BailPrice: {
			handleArrestBail(playerid, inputtext,response);
		}
		case EFactionsDialog_InviteDialog: {
			new msg[128];
			new fid;
			fid = GetPVarInt(playerid, "FactionInvite");
			if(response) {
				format(msg, sizeof(msg), "* %s has been invited into %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetFactionName(fid));
				SendFactionMessage(fid, COLOR_LIGHTBLUE, msg);
				SetPVarInt(playerid, "Faction", fid);
				SetPVarInt(playerid, "Rank", 1);
			}
			DeletePVar(playerid, "FactionInvite");
		}
		case EFactionDialog_LiveOffer: {
			handleLiveResponse(playerid, response);
		}
		case EFactionsDialog_CopPlace: {
			if(response) {
				handleCopPlace(playerid, listitem);
			}
		}
		case EFactionsDialog_LEOCompOptions: {
			handlePoliceComputer(playerid, listitem, response);
		}
		case EFactionsDialog_LEOReqBackups: {
			handlePoliceBackup(playerid, inputtext, response);
		}
		case EFactionsDialog_ShowBizList: {
			handlePoliceBizLocation(playerid, listitem, response);
		}
	}
	return 1;
}
IsValidFaction(faction) {
	return faction >= 0 && faction < sizeof(Factions);
}
IsLEOFaction(faction) {
	return Factions[faction][EFactionType] == EFactionType_LEO;
}
FactionType:getFactionType(faction) {
	return Factions[faction][EFactionType];
}
factionsOnLoadCharacter(playerid) {
	leoOnLoadCharacter(playerid);
}
/*
factionsOnPlayerGiveDamage(playerid, damagedid, Float:amount, weaponid) {
	leoOnPlayerGiveDamage(playerid, damagedid, amount, weaponid);
}
*/
factionsOnPlayerShootWeapon(playerid, weaponid, hittype, hitid, Float:fX, Float:fY, Float:fZ) {
	#pragma unused fX, fY, fZ
	leoOnPlayerShootWeapon(playerid, weaponid, hittype, hitid);
}
YCMD:fgiverank(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Give someone a rank in your faction");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	if(faction == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a faction!");
		return 1;
	}
	new urank = GetPVarInt(playerid, "Rank");
	if(faction == 1) {
		if(urank < 11) {
			SendClientMessage(playerid, X11_TOMATO_2, "Permission Denied!");
			return 1;			
		}
	} 
	else if(faction == 2) {
		if(urank < 7) {
			SendClientMessage(playerid, X11_TOMATO_2, "Permission Denied!");
			return 1;			
		}
	}
	else {
		if(urank < 5) {
			SendClientMessage(playerid, X11_TOMATO_2, "Permission Denied!");
			return 1;			
		}
	}
	new target, rank;
	if(!sscanf(params, "k<playerLookup_acc>d", target, rank)) {
		if(GetPVarInt(target, "Faction") != faction) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in your faction!");
			return 1;
		}
		if(rank < 1 || rank > urank) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't give someone this rank!");
			return 1;
		}
		SetPVarInt(target, "Rank", rank);
		new msg[128];
		format(msg, sizeof(msg), "* %s gave %s Rank %d",GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(target, ENameType_CharName),rank);
		SendFactionMessage(faction, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fgiverank [playerid/name] [rank]");
	}
	return 1;
}
YCMD:ranks(playerid, params[], help) {
	new faction = GetPVarInt(playerid, "Faction");
	new msg[128];
	new i;
	if(faction != 0) {
		for(i=1;i<Factions[faction][EFactionNumRanks]+1;i++) {
			format(msg, sizeof(msg), "%d: %s",i, getFactionRankName(faction, i));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	} else if((faction = GetPVarInt(playerid, "Family")) != 0) {
		new fam = FindFamilyBySQLID(faction);
		if(fam == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Internal Error!");
			return 1;
		}
		for(i=1;i<NUM_RANKS+1;i++) {
			format(msg, sizeof(msg), "%d: %s",i, getRankName(fam, i));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a faction or family!");
	}
	return 1;
}
SendBigFactionEarsMessage(color, const msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "BigFactionEars") == 1) {
				SendClientMessage(i, color, msg);
			}
		}
	}
}
getNumOnlineInFaction(faction) {
	new count = 0;
	foreach(Player, i) {
		if(GetPVarInt(i, "Faction") == faction) count ++;
	}
	return count;
}
YCMD:factions(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "List factions");
		return 1;
	}
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "*** Factions ***");
	for(new i=1;i<sizeof(Factions);i++) {
		format(msg, sizeof(msg), "* %d - %s Online: %d",i,Factions[i][EFactionName],getNumOnlineInFaction(i));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}