#define MAX_LOCKER_NAME 64

new LockerItems[][ESafeSingleItems] = {
	{"None","None",ESafeItemType:-1},
	{"Cash","Money",ESafeItemType_Money},
	{"Pot","Pot",ESafeItemType_Pot},
	{"Coke","Coke",ESafeItemType_Coke},
	{"Gun","Unused",ESafeItemType_Gun},
	{"Meth","Meth",ESafeItemType_Meth},
	{"Materials A","MatsA",ESafeItemType_MatsA},
	{"Materials B","MatsB",ESafeItemType_MatsB},
	{"Materials C","MatsC",ESafeItemType_MatsC},
	{"Special Item","SpecialItem",ESafeItemType_SpecialItem}
};

enum {
	ELockerDialog_GiveTake = ELockers_Base + 1,
	ELockerDialog_ModifySlot,
	ELockerDialog_Take,
	ELockerDialog_StoreChoose,
	ELockerDialog_TakeAmount,
	ELockerDialog_StoreAmount
}
enum ELockerIconInfo  {
	ELockerName[MAX_LOCKER_NAME],
	Float:ELockerPickupX,
	Float:ELockerPickupY,
	Float:ELockerPickupZ,
	ELockerPickupInt,
	ELockerPickupVW,
	ELockerPickupID,
	Text3D:ELockerText,
};
//todo: mechanic, lawyer, trucker?
new LockerIcons[][ELockerIconInfo] = { 
	//{"Unity Station Locker (( /locker ))",1743.28, -1864.23, 13.57, 0, 0, 0, Text3D:0},
	{"Santa Monica Beach Locker (( /locker ))",532.8861, -1812.9851, 6.578125, 0, 0, 0, Text3D:0},
	{"East LA Locker (( /locker ))",2368.7751,-1479.0050,23.9779, 0, 0, 0, Text3D:0}
};
#define NUM_LOCKER_SLOTS 7 //update in DB too
lockersOnGameModeInit() {
	for(new i=0;i<sizeof(LockerIcons);i++) {
		LockerIcons[i][ELockerText] = CreateDynamic3DTextLabel(LockerIcons[i][ELockerName], 0x2BFF00AA, LockerIcons[i][ELockerPickupX], LockerIcons[i][ELockerPickupY], LockerIcons[i][ELockerPickupZ]+1.0, 25.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, LockerIcons[i][ELockerPickupVW], LockerIcons[i][ELockerPickupInt]);
		LockerIcons[i][ELockerPickupID] =  CreateDynamicPickup(1239, 16,  LockerIcons[i][ELockerPickupX], LockerIcons[i][ELockerPickupY], LockerIcons[i][ELockerPickupZ], LockerIcons[i][ELockerPickupVW], LockerIcons[i][ELockerPickupInt]);
	}
}
lockersOnCharLogin(playerid) {
	checkLockers(playerid);
}
lockersOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	//saveLockers(playerid);
	deleteLockerPVars(playerid);
}
/*
YCMD:locker(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Access your public lockers");
		return 1;
	}
	if(!isAtLockerLocation(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the locker point");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	if(IsOnDuty(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
		return 1;
	}
	showTypeLockerMenu(playerid);
	return 1;
}
isAtLockerLocation(playerid) {
	for(new i=0;i<sizeof(LockerIcons);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 5.0,LockerIcons[i][ELockerPickupX], LockerIcons[i][ELockerPickupY], LockerIcons[i][ELockerPickupZ])) {
			return 1;
		}
	}
	return 0;
}
showTypeLockerMenu(playerid) {
	ShowPlayerDialog(playerid, ELockerDialog_GiveTake, DIALOG_STYLE_MSGBOX, "{00BFFF}Locker Menu","What would you like to do with your public locker?", "Store", "Take");
}
*/
lockersOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	new msg[128];
	new total;
	new pvarname[64];
	switch(dialogid) {
		case ELockerDialog_GiveTake: {
			showLockerTakeMenu(playerid,!response);
		}
		case ELockerDialog_Take: {
			SetPVarInt(playerid, "ItemIndex", listitem);
			if(GetPVarInt(playerid, "LockerTake") != 1) {
				if(response) {
					showStoreTypeChoose(playerid);
					return 1;
				} 
				return 1;
			}
			if(response) {
				format(pvarname, sizeof(pvarname), "Item%dType",listitem);
				new index = GetPVarInt(playerid, pvarname);
				format(pvarname, sizeof(pvarname), "Item%d",listitem);
				total = GetPVarInt(playerid, pvarname);
				if(index == 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "Theres nothing in this slot!");
					return 1;
				} else if(index == 4) {
					new gun, ammo, slot;
					decodeWeapon(total, gun, ammo);
					slot = GetWeaponSlot(gun);
					new curgun, curammo;
					GetPlayerWeaponDataEx(playerid, slot, curgun, curammo);
					if(curgun != 0) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are already holding a weapon in this slot!");
						return 1;
					}
					//GivePlayerWeaponEx(playerid, gun, ammo);
					if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
						return 1;
					} else {
						GivePlayerWeaponEx(playerid, gun, ammo);
					}
					SetPVarInt(playerid, pvarname, 0);
					format(pvarname, sizeof(pvarname), "Item%dType",listitem);	
					SetPVarInt(playerid, pvarname, 0);
					format(msg, sizeof(msg), "* %s takes something out %s locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully taken this item from your locker");
					saveLockers(playerid);
					return 1;
				} else if(index == 9) { //special item
					if(HasSpecialItem(playerid)) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are already holding a special item!");
						return 1;
					}
					GivePlayerItem(playerid, total);
					SetPVarInt(playerid, pvarname, 0);
					format(pvarname, sizeof(pvarname), "Item%dType",listitem);	
					SetPVarInt(playerid, pvarname, 0);
					saveLockers(playerid);
					return 1;
				}
				GetItemName(findItemType(index),pvarname, sizeof(pvarname));
				SetPVarInt(playerid, "ItemIndex", listitem);
				format(msg, sizeof(msg), "Enter how much %s you would like to remove\nYou currently have: %d in your locker and %d on hand.",pvarname,total,GetPVarInt(playerid, LockerItems[index][ESafeSItemPVar]));
				ShowPlayerDialog(playerid, ELockerDialog_TakeAmount, DIALOG_STYLE_INPUT, "{00BFFF}Locker Menu",msg, "Take", "Cancel");
			}
		}
		case ELockerDialog_TakeAmount: {
			if(response) {
				total = strval(inputtext);
				if(total <= 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
					return 1;
				}
				new index = GetPVarInt(playerid, "ItemIndex");
				format(pvarname, sizeof(pvarname), "Item%d",index);
				if(GetPVarInt(playerid, pvarname) < total) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
					return 1;
				} else {
					new lockertotal;
					format(pvarname, sizeof(pvarname), "Item%d", index);
					lockertotal = GetPVarInt(playerid, pvarname);
					format(pvarname, sizeof(pvarname), "Item%dType", index);
					new type = GetPVarInt(playerid, pvarname);
					if((lockertotal - total) < 1) {
						DeletePVar(playerid, pvarname);
						format(pvarname, sizeof(pvarname), "Item%d", index);
						DeletePVar(playerid, pvarname);
					} else {
						format(pvarname, sizeof(pvarname), "Item%d", index);
						SetPVarInt(playerid, pvarname, lockertotal - total);
					}
					new amount = GetPVarInt(playerid, LockerItems[type][ESafeSItemPVar]);
					amount += total;
					SetPVarInt(playerid, LockerItems[type][ESafeSItemPVar], amount);
					GiveMoneyEx(playerid, 0); //sync money
					format(msg, sizeof(msg), "* %s takes something out %s locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully taken this item from your locker");
					saveLockers(playerid);
				}
			}
			DeletePVar(playerid, "ItemIndex");
		}
		case ELockerDialog_StoreChoose: {
			if(!response) return 1;
			listitem++;
			format(pvarname, sizeof(pvarname), "Item%dType",GetPVarInt(playerid, "ItemIndex"));
			SetPVarInt(playerid, "StorageSlot", listitem);
			new index = GetPVarInt(playerid, pvarname);
			if((listitem != index && index != 0) || LockerItems[index][ESafeSType] == ESafeItemType_Gun || LockerItems[index][ESafeSType] == ESafeItemType_SpecialItem) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can't store this here!");
				return 1;
			}
			if(LockerItems[listitem][ESafeSType] == ESafeItemType_Gun) {
				new gun, ammo, gslot;
				gslot = GetWeaponSlot(GetPlayerWeaponEx(playerid));
				GetPlayerWeaponDataEx(playerid, gslot ,gun, ammo);
				if(gun < 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You aren't carrying any weapon on that slot");
					return 1;
				}
				gslot = encodeWeapon(gun, ammo);
				new itemindex = GetPVarInt(playerid, "ItemIndex");
				format(pvarname, sizeof(pvarname), "Item%d", itemindex);
				SetPVarInt(playerid, pvarname, gslot);
				format(pvarname, sizeof(pvarname), "Item%dType", itemindex);
				SetPVarInt(playerid, pvarname, listitem);
				RemovePlayerWeapon(playerid, gun);
				format(msg, sizeof(msg), "* %s puts something into %s locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully stored this item in your locker");
				saveLockers(playerid);
				return 1;
			} else if(LockerItems[listitem][ESafeSType] == ESafeItemType_SpecialItem) {
				if(!HasSpecialItem(playerid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not carrying a special item.");
					return 1;
				}
				new itemindex = GetPVarInt(playerid, "ItemIndex");
				new item = GetPVarInt(playerid, "SpecialItem");
				format(pvarname, sizeof(pvarname), "Item%dType", itemindex);
				SetPVarInt(playerid, pvarname, listitem);
				format(pvarname, sizeof(pvarname), "Item%d",itemindex);
				SetPVarInt(playerid, pvarname, item);
				format(msg, sizeof(msg), "* %s puts something into %s locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully stored this item in your locker");
				RemovePlayerItem(playerid);
				saveLockers(playerid);
				return 1;
			}
			GetItemName(findItemType(listitem),pvarname, sizeof(pvarname));
			format(msg, sizeof(msg), "Enter how much %s you would like to store\nYou currently have: %d in your locker and %d on hand.",pvarname,total,GetPVarInt(playerid, LockerItems[index][ESafeSItemPVar]));
			ShowPlayerDialog(playerid, ELockerDialog_StoreAmount, DIALOG_STYLE_INPUT, "{00BFFF}Locker Menu",msg, "Store", "Cancel");
		}
		case ELockerDialog_StoreAmount: {
			total = strval(inputtext);
			if(total <= 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
				return 1;
			}
			new slot = GetPVarInt(playerid, "StorageSlot");
			if(GetPVarInt(playerid, LockerItems[slot][ESafeSItemPVar]) < total) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
				return 1;
			}
			new lockertotal;
			new index = GetPVarInt(playerid, "ItemIndex");
			format(pvarname, sizeof(pvarname), "Item%d", index);
			lockertotal = GetPVarInt(playerid, pvarname);
			SetPVarInt(playerid, pvarname, lockertotal + total);
			format(pvarname, sizeof(pvarname), "Item%dType", index);
			new curslot = GetPVarInt(playerid, pvarname);
			new localtotal = GetPVarInt(playerid, LockerItems[slot][ESafeSItemPVar]);
			localtotal -= total;
			SetPVarInt(playerid, LockerItems[slot][ESafeSItemPVar], localtotal);
			GiveMoneyEx(playerid, 0); //sync money
			SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully stored this item from your locker");
			if(curslot == 0) {
				SetPVarInt(playerid, pvarname, slot);
			}
			format(msg, sizeof(msg), "* %s puts something into %s locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			DeletePVar(playerid, "StorageSlot");
			DeletePVar(playerid, "ItemIndex");
			saveLockers(playerid);
		}
	}
	return 1;
}
showStoreTypeChoose(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=1;i<sizeof(LockerItems);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",LockerItems[i][ESafeSItemName]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, ELockerDialog_StoreChoose, DIALOG_STYLE_LIST, "{00BFFF}Locker Menu",dialogstr, "Take", "Cancel");
}
showLockerTakeMenu(playerid,take) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new pvarname[64];
	for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
		new index,amount;
		format(pvarname, sizeof(pvarname), "Item%dType",i);
		index = GetPVarInt(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "Item%d",i);
		amount = GetPVarInt(playerid, pvarname);
		if(index == 0) { //none
			strcat(dialogstr, "None\n", sizeof(dialogstr));
		} else {
			new ESafeItemType:type = findItemType(index);
			if(type == ESafeItemType_Gun) {
				new gun, ammo;
				decodeWeapon(amount, gun, ammo);
				GetWeaponNameEx(gun, pvarname, sizeof(pvarname));
				amount = ammo;
			} else {
				GetItemName(type,pvarname, sizeof(pvarname));
			}
			if(type == ESafeItemType_SpecialItem) {
			format(tempstr, sizeof(tempstr), "%s - %s\n",pvarname,GetCarryingItemName(amount));
			} else {
				format(tempstr, sizeof(tempstr), "%s - %s\n",pvarname,getNumberString(amount));
			}
			strcat(dialogstr, tempstr, sizeof(dialogstr));
		}
	}
	SetPVarInt(playerid, "LockerTake", take);
	ShowPlayerDialog(playerid, ELockerDialog_Take, DIALOG_STYLE_LIST, "{00BFFF}Locker Menu",dialogstr, "Take", "Cancel");
}
checkLockers(playerid) {
	format(query, sizeof(query), "SELECT 1 FROM `lockers` WHERE `charid` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "OnCheckLockers", "d",playerid);
}
insertLockers(playerid) {
	format(query, sizeof(query), "INSERT INTO `lockers` SET `charid` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
forward OnCheckLockers(playerid);
public OnCheckLockers(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		loadLockers(playerid);
	} else {
		insertLockers(playerid);
	}
}
saveLockers(playerid) {
	new pvarname[64],pvarname2[64];
	format(query, sizeof(query), "UPDATE `lockers` SET ");
	for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Item%dType",i);
		format(pvarname2, sizeof(pvarname2), "Item%d",i);
		format(tempstr, sizeof(tempstr), "`item%dtype` = %d,`item%d` = %d,",i,GetPVarInt(playerid, pvarname), i, GetPVarInt(playerid, pvarname2));
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " WHERE `charid` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
deleteLockerPVars(playerid) {
	new pvarname[32];
	for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Item%dType",i);
		DeletePVar(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "Item%d",i);
		DeletePVar(playerid, pvarname);
	}
}

GetItemName(ESafeItemType:item, dst[], dstlen) {
	for(new i=0;i<sizeof(LockerItems);i++) {
		if(LockerItems[i][ESafeSType] == item) {
			format(dst, dstlen, "%s",LockerItems[i][ESafeSItemName]);
			return 1;
		}
	}
	return 0;
}
ESafeItemType:findItemType(index) {
	if(index < 0 || index > sizeof(LockerItems)) return ESafeItemType:-1;
	return LockerItems[index][ESafeSType];
}
loadLockers(playerid) {
	query[0] = 0;
	tempstr[0] = 0;
	format(query, sizeof(query), "SELECT ");
	for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
		format(tempstr, sizeof(tempstr), "`item%dtype`,`item%d`,",i, i);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " FROM `lockers` WHERE `charid` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadLockers", "d", playerid);
}
forward OnLoadLockers(playerid);
public OnLoadLockers(playerid) {
	new id_string[32];
	new pvarname[64];
	new x;
	for(new i=0;i<NUM_LOCKER_SLOTS*2;i+=2) {
		format(pvarname, sizeof(pvarname), "Item%dType",x);
		cache_get_row(0, i, id_string);
		//printf("PVarName: %s", pvarname);
		SetPVarInt(playerid, pvarname, strval(id_string));
		cache_get_row(0, i+1, id_string);
		format(pvarname, sizeof(pvarname), "Item%d",x);
		SetPVarInt(playerid, pvarname, strval(id_string));
		//printf("PVarName: %s", pvarname);
		x++;
	}
}
YCMD:checklocker(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays the contents of a players locker");
		return 1;
	}
	new pvarname[64];
	new target;
	if(!sscanf(params, "k<playerLookup_acc>",target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		format(query, sizeof(query), "******* %s's Locker *******",GetPlayerNameEx(target, ENameType_CharName));
		SendClientMessage(playerid, X11_WHITE, query);
		for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
			new index,amount;
			format(pvarname, sizeof(pvarname), "Item%dType",i);
			index = GetPVarInt(target, pvarname);
			format(pvarname, sizeof(pvarname), "Item%d",i);
			amount = GetPVarInt(target, pvarname);
			if(index == 0) { //none
				format(query, sizeof(query), "%d. None",i+1);
			} else {
				new ESafeItemType:type = findItemType(index);
				if(type == ESafeItemType_Gun) {
					new gun, ammo;
					decodeWeapon(amount, gun, ammo);
					GetWeaponNameEx(gun, pvarname, sizeof(pvarname));
					amount = ammo;
				} else {
					GetItemName(type,pvarname, sizeof(pvarname));
				}
				if(type != ESafeItemType_SpecialItem) {
					format(query, sizeof(query), "%d. %s - %s",i+1,pvarname,getNumberString(amount));
				} else {
					format(query, sizeof(query), "%d. %s - %s",i+1,pvarname,GetCarryingItemName(amount));
				}
			}
			SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /checklocker [playerid/name]");
	}
	return 1;
}
getPlayerLockerWealth(playerid) {
	new value;
	new pvarname[64];
	for(new i=0;i<NUM_LOCKER_SLOTS;i++) {
		new index,amount;
		format(pvarname, sizeof(pvarname), "Item%dType",i);
		index = GetPVarInt(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "Item%d",i);
		amount = GetPVarInt(playerid, pvarname);
		new ESafeItemType:type = findItemType(index);
		if(type == ESafeItemType_Money) {
			value += amount;
		}
	}
	return value;
}