enum EModelPreviewInfo {
	Float:EPreviewPosX,
	Float:EPreviewPosY,
	EPreviewSizeX,
	EPreviewSizeY,
	Float:EPreviewModelRotX,
	Float:EPreviewModelRotY,
	Float:EPreviewModelRotZ,
};

new PreviewBoxes[][EModelPreviewInfo] = {
	{150.0,50.0, 150, 150, 0.0, 0.0, 0.0},
	{150.0,210.0, 150, 150, -90.0, 0.0, 0.0},
	{310.0,50.0, 150, 150, 0.0, 0.0, -180.0},
	{310.0,210.0, 150, 150, 0.0, 0.0, 90.0}
};

enum ETextDrawAlignmentDefinition {
	ETextDrawAlignment_Left = 1,
	ETextDrawAlignment_Center,
	ETextDrawAlignment_Right
};

enum ETextDrawFonts {
	ETextDrawFont_Handwriting,
	ETextDrawFont_Stencil_Narrow,
	ETextDrawFont_Stencil_Long,
	ETextDrawFont_Stencil_Wide,
	ETextDrawFont_Sprite,
	ETextDrawFont_Model,
};

enum ETextDrawFlagsDefinition (<<= 1) {
	ETextDrawFlag_None = 0,
	ETextDrawFlag_TextShadow = 1,
	ETextDrawFlag_SetProportional,
	ETextDrawFlag_SetSelectable,
	ETextDrawFlag_UseBox,
	ETextDrawFlag_HiddenByDefault,

};

enum ETextDrawActionTypes {
	PLAYER_PREVIEW_BUTTON_NONE,
	PLAYER_PREVIEW_BUTTON_PREVIOuS,
	PLAYER_PREVIEW_BUTTON_CANCEL,
	PLAYER_PREVIEW_BUTTON_ACCEPT,
	PLAYER_PREVIEW_BUTTON_NEXT,
	PLAYER_PREVIEW_PREVIEW_BOX,
	PLAYER_PREVIEW_BACKGROUND,
	PLAYER_PREVIEW_TITLE,
	PLAYER_PREVIEW_FOOTER,
}

enum ETextDrawInfo {
	Float:ETextDrawPosX, 
	Float:ETextDrawPosY,
	Float:ETextDrawSizeX,
	Float:ETextDrawSizeY,
	Float:ETextDrawLetterSizeX,
	Float:ETextDrawLetterSizeY,
	ETextDrawTextColour,
	ETextDrawBackgroundColour,
	ETextDrawFonts:ETextDrawFont,
	ETextDrawContent[256],
	ETextDrawFlagsDefinition:ETextDrawFlags,

	ETextDrawShadowSize,
	ETextDrawOutlineSize,
	ETextDrawBoxColor,
	ETextDrawAlignmentDefinition:ETextDrawAlignment,
	ETextDrawActionTypes:ETextDrawActionType,

};

new PreviewUI[][ETextDrawInfo] = {
	{302.666503, 45.229701,  0.000000, 409.333496, 0.400000, 1.600000, 0, 0, ETextDrawFont_Stencil_Long, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~", ETextDrawFlag_UseBox|ETextDrawFlag_SetProportional, 0, 0, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BACKGROUND},
	//{185.0, 380.0, 35.0, 85.0, 0.150000, 1.000000, COLOR_RED,0, ETextDrawFont_Stencil_Long, "<< Back", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center,PLAYER_PREVIEW_BUTTON_PREVIOuS},
	{270.0, 380.0, 35.0, 75.0, 0.150000, 1.000000, X11_SKYBLUE,0, ETextDrawFont_Stencil_Long, "Cancel", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BUTTON_CANCEL},
	{360.0, 380.0, 35.0, 75.0, 0.150000, 1.000000, X11_SKYBLUE,0, ETextDrawFont_Stencil_Long, "Accept", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BUTTON_ACCEPT}
	//{450.0, 380.0, 35.0, 75.0, 0.350000, 1.200000, COLOR_RED,0, ETextDrawFont_Stencil_Long, "Next >>", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BUTTON_NEXT}
};

new SelectUI[][ETextDrawInfo] = {
	{302.666503, 25.229701,  0.000000, 409.333496, 0.400000, 1.600000, 0, 0, ETextDrawFont_Stencil_Long, "~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~~n~", ETextDrawFlag_UseBox|ETextDrawFlag_SetProportional, 0, 0, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BACKGROUND},
	{300.0, 25.0, 85.0, 185.0, 0.150000, 1.000000, X11_WHITE,0, ETextDrawFont_Stencil_Long, "Vehicle Menu", ETextDrawFlag_None, 0, 1, X11_BLACK, ETextDrawAlignment_Center,PLAYER_PREVIEW_TITLE},
	{300.0, 380.0, 35.0, 85.0, 0.150000, 1.000000, X11_WHITE,0, ETextDrawFont_Stencil_Long, "Page 1/1", ETextDrawFlag_None, 0, 1, X11_BLACK, ETextDrawAlignment_Center,PLAYER_PREVIEW_FOOTER},
	{185.0, 380.0, 35.0, 85.0, 0.150000, 1.000000, X11_SKYBLUE,0, ETextDrawFont_Stencil_Long, "<< Back", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center,PLAYER_PREVIEW_BUTTON_PREVIOuS},
	{450.0, 380.0, 35.0, 75.0, 0.150000, 1.000000, X11_SKYBLUE,0, ETextDrawFont_Stencil_Long, "Next >>", ETextDrawFlag_UseBox|ETextDrawFlag_SetSelectable, 0, 1, X11_BLACK, ETextDrawAlignment_Center, PLAYER_PREVIEW_BUTTON_NEXT}
};

enum EModelListInfo {
	EModelList_ModelID,
	EModelList_Title[32],
	EModelList_Footer[32],
	PlayerText:EModelList_TextDraw,
	PlayerText:EModelList_TitleTextDraw,
	PlayerText:EModelList_FooterTextDraw,
};
new ModelListInfo[][EModelListInfo] = {
	{411, "Infernus"},
	{405, "Sentinel"},
	{19, "Skin"}
};

#define MAX_MODEL_SELECT_COLS 4
#define MAX_MODEL_SELECT_ROWS 4

enum EModelPreviewCallbackAction {
	EModelPreviewAction_Exit,	
	EModelPreviewAction_Accept,
	EModelPreviewAction_Back,
	EModelPreviewAction_Next,
	EModelPreviewAction_LaunchPreview,
};

new mdlSelectCols[MAX_PLAYERS][MAX_MODEL_SELECT_ROWS*MAX_MODEL_SELECT_COLS][EModelListInfo];

#define NUM_PREVIEW_BOXES 4
#define NUM_PREVIEW_UI_ITEMS 3

#define NUM_MODEL_SELECT_UI_ITEMS 5

#define PREVIEWBOX_IDS_ARRAY_SIZE NUM_PREVIEW_BOXES+NUM_PREVIEW_UI_ITEMS


enum ETextDrawPreviewInfo {
	PlayerText:EPlayerPreviewTextDrawID,
	TextDrawActionTypes:EPlayerPreviewTextDrawType,
};
new PreviewBoxIDs[MAX_PLAYERS][PREVIEWBOX_IDS_ARRAY_SIZE][ETextDrawPreviewInfo];

new mdlPreviewCallback[MAX_PLAYERS][32];

getFreePreviewIndex(playerid) {
	for(new i=0;i<PREVIEWBOX_IDS_ARRAY_SIZE;i++) {
		if(PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID] == PlayerText:0) {
			return i;
		}
	}
	return -1;
}


getPreviewIndexFromTD(playerid, PlayerText:textdraw) {
	for(new i=0;i<PREVIEWBOX_IDS_ARRAY_SIZE;i++) {
		if(PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID] == textdraw) {
			return i;
		}
	}
	return -1;
}

destroyModelPreviewTDs(playerid) {
	for(new i=0;i<PREVIEWBOX_IDS_ARRAY_SIZE;i++) {
		if(PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID] != PlayerText:0) {
			PlayerTextDrawHide(playerid,PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID]);
			PlayerTextDrawDestroy(playerid,PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID]);
			PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID] = PlayerText:0;
			
		}
	}
}

changePreviewModel(playerid, modelid, col1 = -1, col2 = -1) {
	for(new i=0;i<PREVIEWBOX_IDS_ARRAY_SIZE;i++) {
		if(PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawType] == PLAYER_PREVIEW_PREVIEW_BOX) {
			new PlayerText:textdraw = PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID];
			PlayerTextDrawHide(playerid,PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID]);
	    	PlayerTextDrawSetPreviewModel(playerid, textdraw, modelid);
	    	if(col1 != -1 || col2 != -1) {
		    	PlayerTextDrawSetPreviewVehCol(playerid, textdraw, col1, col2);
	    	}
	    	PlayerTextDrawShow(playerid,PreviewBoxIDs[playerid][i][EPlayerPreviewTextDrawID]);
		}
	}
}

createpreviewUIElement(ETextDrawInfo:elementInfo[], playerid) {
	new PlayerText:textdraw = CreatePlayerTextDraw(playerid, elementInfo[ETextDrawPosX], elementInfo[ETextDrawPosY], elementInfo[ETextDrawContent]);

	if(elementInfo[ETextDrawFlags] & ETextDrawFlag_SetSelectable) {
		PlayerTextDrawSetSelectable(playerid, textdraw, 1);
	}
	if(elementInfo[ETextDrawFlags] & ETextDrawFlag_SetProportional) {
		PlayerTextDrawSetProportional(playerid, textdraw, 1);
	}

	if(elementInfo[ETextDrawFlags] & ETextDrawFlag_UseBox) {
		PlayerTextDrawUseBox(playerid, textdraw, 1);
	}
	PlayerTextDrawColor(playerid, textdraw, elementInfo[ETextDrawTextColour]);
	PlayerTextDrawBoxColor(playerid, textdraw, elementInfo[ETextDrawBoxColor]);
	PlayerTextDrawBackgroundColor(playerid, textdraw, elementInfo[ETextDrawBackgroundColour]);

	PlayerTextDrawFont(playerid, textdraw, elementInfo[ETextDrawFont]);

	PlayerTextDrawSetOutline(playerid, textdraw, elementInfo[ETextDrawOutlineSize]);

	PlayerTextDrawAlignment(playerid, textdraw, elementInfo[ETextDrawAlignment]);

	if(elementInfo[ETextDrawSizeX] != 0.0 || elementInfo[ETextDrawSizeY] != 0.0) {
		PlayerTextDrawTextSize(playerid, textdraw, elementInfo[ETextDrawSizeX], elementInfo[ETextDrawSizeY]);
	}

	if(elementInfo[ETextDrawLetterSizeX] != 0.0 || elementInfo[ETextDrawLetterSizeY] != 0.0) {
		PlayerTextDrawLetterSize(playerid, textdraw, elementInfo[ETextDrawLetterSizeX], elementInfo[ETextDrawLetterSizeY]);
	}

	PlayerTextDrawSetShadow(playerid, textdraw, elementInfo[ETextDrawShadowSize]);

	new idx = getFreePreviewIndex(playerid);
	if(idx == -1) {
		PlayerTextDrawDestroy(playerid, textdraw);
	} else {
		PreviewBoxIDs[playerid][idx][EPlayerPreviewTextDrawID] = textdraw;
		PreviewBoxIDs[playerid][idx][EPlayerPreviewTextDrawType] = elementInfo[ETextDrawActionType];
	}

	if(~elementInfo[ETextDrawFlags] & ETextDrawFlag_HiddenByDefault) {
		PlayerTextDrawShow(playerid, textdraw);
	}
}

launchModelPreviewMenu(playerid, modelid, col1 = -1, col2 = -1) {
	for(new i=0;i<NUM_PREVIEW_UI_ITEMS;i++) {
		createpreviewUIElement(PreviewUI[i], playerid);
	}
	for(new i=0;i<NUM_PREVIEW_BOXES;i++) {
		new PlayerText:textdraw = CreatePlayerTextDraw(playerid, PreviewBoxes[i][EPreviewPosX], PreviewBoxes[i][EPreviewPosY], "_");
	    PlayerTextDrawFont(playerid, textdraw, TEXT_DRAW_FONT_MODEL_PREVIEW);
	    PlayerTextDrawUseBox(playerid, textdraw, 1);
	    PlayerTextDrawBoxColor(playerid, textdraw, COLOR_RED);
	    PlayerTextDrawBackgroundColor(playerid, textdraw, COLOR_FADE3);
	    if(col1 != -1 || col2 != -1) {
	    	PlayerTextDrawSetPreviewVehCol(playerid, textdraw, col1, col2);
	    }
	    PlayerTextDrawTextSize(playerid, textdraw, PreviewBoxes[i][EPreviewSizeX], PreviewBoxes[i][EPreviewSizeY]);
	    PlayerTextDrawSetPreviewModel(playerid, textdraw, modelid);
	    PlayerTextDrawSetPreviewRot(playerid, textdraw, PreviewBoxes[i][EPreviewModelRotX], PreviewBoxes[i][EPreviewModelRotY], PreviewBoxes[i][EPreviewModelRotZ]);
	    PlayerTextDrawShow(playerid, textdraw);

	 	new idx = getFreePreviewIndex(playerid);
		if(idx == -1) {
			PlayerTextDrawDestroy(playerid, textdraw);
		} else {
			PreviewBoxIDs[playerid][idx][EPlayerPreviewTextDrawID] = textdraw;
			PreviewBoxIDs[playerid][idx][EPlayerPreviewTextDrawType] = PLAYER_PREVIEW_PREVIEW_BOX;
		}
	}
	new alpha = 0xAA;
	new colour = X11_SKYBLUE;
	colour &= 0xffffff00;
	colour |= alpha;
	SelectTextDraw(playerid, colour);
}

addItemToModelSelectMenu(playerid, modelid, title[], footer[]) {
	for(new r=0;r<MAX_MODEL_SELECT_ROWS;r++) {		
		for(new i=0;i<MAX_MODEL_SELECT_COLS;i++) {
			if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID] == 0) {
				mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID] = modelid;
				strmid(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Title], title,0,strlen(title), 32);
				strmid(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Footer], footer,0,strlen(footer), 32);
				return;
			}
		}
	}
}

forward launchModelSelectMenu(playerid, num_models, title[], footer[], callback[], Float:rotx, Float:roty, Float:rotz);
public launchModelSelectMenu(playerid, num_models, title[], footer[],callback[], Float:rotx, Float:roty, Float:rotz)  {
	strmid(mdlPreviewCallback[playerid], callback,0,strlen(callback), 32);
	for(new i=0;i<NUM_MODEL_SELECT_UI_ITEMS;i++) {
		if(SelectUI[i][ETextDrawActionType] == PLAYER_PREVIEW_TITLE) {
			strmid(SelectUI[i][ETextDrawContent], title,0, strlen(title), 256);
		} else if(SelectUI[i][ETextDrawActionType] == PLAYER_PREVIEW_FOOTER) {
			strmid(SelectUI[i][ETextDrawContent], footer,0, strlen(footer), 256);
		}
		createpreviewUIElement(SelectUI[i], playerid);
	}

	new Float:X, Float:Y;
	X = SelectUI[0][ETextDrawPosX] - 190.0;
	Y = SelectUI[0][ETextDrawPosY] + 15.0;

	new Float:tdSizeX = 100.0, Float:tdSizeY = 75.0;

	for(new r=0;r<MAX_MODEL_SELECT_ROWS;r++) {
		
		for(new i=0;i<MAX_MODEL_SELECT_COLS;i++) {
			if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID] != 0) {
				new PlayerText:textdraw = CreatePlayerTextDraw(playerid, X,Y, "_");
			    PlayerTextDrawFont(playerid, textdraw, TEXT_DRAW_FONT_MODEL_PREVIEW);
		    	PlayerTextDrawUseBox(playerid, textdraw, 1);
		    	PlayerTextDrawSetSelectable(playerid, textdraw, 1);
		    	PlayerTextDrawBoxColor(playerid, textdraw, COLOR_FADE3);
		    	PlayerTextDrawBackgroundColor(playerid, textdraw, COLOR_FADE3);
				PlayerTextDrawSetPreviewModel(playerid, textdraw, mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID]);
				PlayerTextDrawSetPreviewRot(playerid, textdraw, rotx, roty, rotz);
				PlayerTextDrawTextSize(playerid, textdraw, 75, 75);
				PlayerTextDrawShow(playerid, textdraw);
				mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TextDraw] = textdraw;

				//make title
				if(strlen(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Title]) > 0) {
					textdraw = CreatePlayerTextDraw(playerid, X+5.0,Y+5.0,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Title]);
					PlayerTextDrawFont(playerid, textdraw, 1);
					PlayerTextDrawLetterSize(playerid, textdraw, 0.150000, 1.200000);
					PlayerTextDrawColor(playerid, textdraw, -1);
					PlayerTextDrawSetOutline(playerid, textdraw, 1);
					PlayerTextDrawSetProportional(playerid, textdraw, 1);
					PlayerTextDrawAlignment(playerid, textdraw, 1);
					PlayerTextDrawShow(playerid, textdraw);
					mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TitleTextDraw] = textdraw;
				}

				//make footer
				if(strlen(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Footer]) > 0) {
					textdraw = CreatePlayerTextDraw(playerid, X+5.0,Y+55.0,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_Footer]);
					PlayerTextDrawFont(playerid, textdraw, 1);
					PlayerTextDrawLetterSize(playerid, textdraw, 0.150000, 1.200000);
					PlayerTextDrawColor(playerid, textdraw, -1);
					PlayerTextDrawSetOutline(playerid, textdraw, 1);
					PlayerTextDrawSetProportional(playerid, textdraw, 1);
					PlayerTextDrawAlignment(playerid, textdraw, 1);
					PlayerTextDrawShow(playerid, textdraw);
					mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_FooterTextDraw] = textdraw;
				}
			}
			X += tdSizeX + 5.0;
		}
		X = SelectUI[0][ETextDrawPosX] - 190.0;
		Y += tdSizeY + 5.0;
	}

	new alpha = 0xAA;
	new colour = X11_SKYBLUE;
	colour &= 0xffffff00;
	colour |= alpha;
	SelectTextDraw(playerid, colour);
}

clearModelSelectMenu(playerid) {
	for(new r=0;r<MAX_MODEL_SELECT_ROWS;r++) {		
		for(new i=0;i<MAX_MODEL_SELECT_COLS;i++) {
			if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID] != 0) {
				mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_ModelID] = 0;
				PlayerTextDrawHide(playerid,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TextDraw]);
				PlayerTextDrawDestroy(playerid,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TextDraw]);
				mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TextDraw] = PlayerText:0;

				if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TitleTextDraw] != PlayerText:0) {
					PlayerTextDrawHide(playerid,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TitleTextDraw]);
					PlayerTextDrawDestroy(playerid, mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TitleTextDraw]);
					mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TitleTextDraw] = PlayerText:0;
				}

				if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_FooterTextDraw] != PlayerText:0) {
					PlayerTextDrawHide(playerid,mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_FooterTextDraw]);
					PlayerTextDrawDestroy(playerid, mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_FooterTextDraw]);
					mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_FooterTextDraw] = PlayerText:0;
				}
			}
		}
	}
	destroyModelPreviewTDs(playerid);
}


mdlprevOnClickTD(playerid, Text:clickedid) { 
	if(clickedid == INVALID_TEXT_DRAW) {
		if(strlen(mdlPreviewCallback[playerid]) > 0) {
			CallRemoteFunction(mdlPreviewCallback[playerid], "ddds",playerid,EModelPreviewAction_Exit,-1,"_");
			return 1;
		}
	}
	return 0;
}

getModelSelectIndexFromTD(playerid, PlayerText:textdraw) {
	for(new r=0;r<MAX_MODEL_SELECT_ROWS;r++) {		
		for(new i=0;i<MAX_MODEL_SELECT_COLS;i++) {
			if(mdlSelectCols[playerid][(r*MAX_MODEL_SELECT_ROWS)+i][EModelList_TextDraw] == textdraw) {
				return (r*MAX_MODEL_SELECT_ROWS)+i;
			}
		}
	}
	return -1;
}


mdlprevOnClickPlayerTD(playerid, PlayerText:playertextid) {
	new id = getPreviewIndexFromTD(playerid,playertextid);
	if(id != -1) {
		if(PreviewBoxIDs[playerid][id][EPlayerPreviewTextDrawType] == PLAYER_PREVIEW_BUTTON_PREVIOuS) {
			changePreviewModel(playerid, 38);
		}
		new action;
		switch(PreviewBoxIDs[playerid][id][EPlayerPreviewTextDrawType]) {
			case PLAYER_PREVIEW_BUTTON_PREVIOuS:
				action = EModelPreviewAction_Back;
			case PLAYER_PREVIEW_BUTTON_NEXT:
				action = EModelPreviewAction_Next;
			case PLAYER_PREVIEW_BUTTON_ACCEPT:
				action = EModelPreviewAction_Accept;
			case PLAYER_PREVIEW_BUTTON_CANCEL:
				action = EModelPreviewAction_Exit;
		}
		CallRemoteFunction(mdlPreviewCallback[playerid], "ddds",playerid,action,-1,"_");
		return 1;
	} else {
		new idx = getModelSelectIndexFromTD(playerid, playertextid);
		if(idx != -1) {
			CallRemoteFunction(mdlPreviewCallback[playerid], "ddds",playerid,EModelPreviewAction_LaunchPreview,mdlSelectCols[playerid][idx][EModelList_ModelID],mdlSelectCols[playerid][idx][EModelList_Title]);
			return 1;
		}
	}
	return 0;
}