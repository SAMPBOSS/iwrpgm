/* 
	---------------------------------------------------------------------------------------------------
	Purpose: This will be used for functions among other things that need to be used by all the script.
	---------------------------------------------------------------------------------------------------
*/

/* Gun Classes */
enum EGunType {
	EWeaponType_Melee,
	EWeaponType_Pistol,
	EWeaponType_Shotgun,
	EWeaponType_SMG,
	EWeaponType_AssaultRifle,
	EWeaponType_Rifle,
	EWeaponType_Thrown,
	EWeaponType_Heavy,
	EWeaponType_Accessory,
};

enum WepClasses
{
	GunID,
	EGunType:EWeaponType,
}

new GunClasses[][WepClasses] = {
	{0, EWeaponType_Melee},
	{1, EWeaponType_Melee},
	{2, EWeaponType_Melee},
	{3, EWeaponType_Melee},
	{4, EWeaponType_Melee},
	{5, EWeaponType_Melee},
	{6, EWeaponType_Melee},
	{7, EWeaponType_Melee},
	{8, EWeaponType_Melee},
	{9, EWeaponType_Heavy},
	{10, EWeaponType_Melee},
	{11, EWeaponType_Melee},
	{12, EWeaponType_Melee},
	{13, EWeaponType_Melee},
	{14, EWeaponType_Melee},
	{15, EWeaponType_Melee},
	{16, EWeaponType_Thrown},
	{17, EWeaponType_Thrown},
	{18, EWeaponType_Thrown},
	{22, EWeaponType_Pistol},
	{23, EWeaponType_Pistol},
	{24, EWeaponType_Pistol},
	{25, EWeaponType_Shotgun},
	{26, EWeaponType_Shotgun},
	{27, EWeaponType_Shotgun},
	{28, EWeaponType_SMG},
	{29, EWeaponType_SMG},
	{30, EWeaponType_AssaultRifle},
	{31, EWeaponType_AssaultRifle},
	{32, EWeaponType_SMG},
	{33, EWeaponType_Rifle},
	{34, EWeaponType_Rifle},
	{35, EWeaponType_Heavy},
	{36, EWeaponType_Heavy},
	{37, EWeaponType_Heavy},
	{38, EWeaponType_Heavy},
	{39, EWeaponType_Thrown},
	{40, EWeaponType_Thrown},
	{41, EWeaponType_Accessory},
	{42, EWeaponType_Heavy},
	{43, EWeaponType_Accessory},
	{44, EWeaponType_Accessory},
	{45, EWeaponType_Accessory},
	{46, EWeaponType_Accessory}
	
};

getWeaponClassType(playerid, gunid) { 
	for(new i=0;i<sizeof(GunClasses);i++) {
		if(GunClasses[i][GunID] == gunid) {
			return GunClasses[i][EWeaponType];
		}
	}
	return 1;
}
/* End of Gun Classes */