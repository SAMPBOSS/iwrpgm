#define TRUNK_DISTANCE 		35.0
#define MAX_CUSTOM_RADIOS	2000
enum ERadioInfo {
	ERadioName[32],
	ERadioGenre[32],
	ERadioURL[128],
}
new Radios[][ERadioInfo] = {{"Comedy Club (Explicit Content)","Talk Radio","http://108.61.73.118:8026/listen.pls"},
							{"Deep & Soulful","R&B/Blues","http://ssradiouk.com/listen.pls"},
							{"DJ COOKI Radio","Other","http://78.158.137.69:8001/listen.pls"},
							{"UK Bass Bomber","Electronic","http://ukbassradio.com/live/128k/listen.pls"},
							{"Hot 108 Jamz","Hip-Hop","http://yp.shoutcast.com/sbin/tunein-station.pls?id=32999"},
							{"RadioUP.com","Pop", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=83836"},
							{"Activeradio.us", "Pop", "http://159.253.143.4:8322/listen.pls"},
							{"Defray Radio", "Hip-Hop", "http://www.defjay.com/listen.pls"},
							{"CultureFM Radio", "Old School Hip-Hop", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=106672"},
							{"107.3 KWCR","Hip-Hop / Pop", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=1357960"},
							{"Street Style Radio","Street", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=204517"},
							{"DTF Radio", "Hip-Hop/Pop", "http://149.255.33.69:8044/listen.pls"},
							{"Truehiphop.com", "Hip-Hop", "http://www.truehiphopfm.de/thhint/truehiphopfm.pls"},
							{"181.3 Kickin' Country", "Country", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=221956"},
							{"Highway 181", "Classic Country", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=297243"},
							{"Absolutely Country Hits Radio", "Country", "http://yp.shoutcast.com/sbin/tunein-station.pls?id=678106"},
							{"Hard Rock Radio", "Hard Rock", "https://www.reliastream.com/cast/tunein.php/niorozco/tunein.pls"},
							{"Hair Metal 101", "Metal", "http://www.streamlicensing.com/directory/index.cgi/playlist.pls?action=playlist&type=pls&sid=2483&stream_id=4223"},
							{"Radio Catarsis Metalera", "Metal", "http://46.8.37.137:7000/listen.pls"}
						};

enum ERadioCustomInfo {
	ERadioURLCustom[128],
	ECustomRadio,
}
new RadiosCustom[MAX_CUSTOM_RADIOS][ERadioCustomInfo];

YCMD:radiostation(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Change a vehicles radio station");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not driving a vehicle!");
		return 1;
	}
	new msg[128];
	new carid = GetPlayerVehicleID(playerid);
	new radio;
	if(!sscanf(params, "d", radio)) {
		radio--;
		if(getCarRadio(carid) == radio) {
			SendClientMessage(playerid, X11_TOMATO_2, "This already is the radio station!");
			return 1;
		}
		if(!isValidRadionStation(radio)) {
			format(msg, sizeof(msg), "* %s turns off the radio.",GetPlayerNameEx(playerid, ENameType_RPName));
		} else {
			format(msg, sizeof(msg), "* %s changes the radio station to %s",GetPlayerNameEx(playerid, ENameType_RPName),Radios[radio][ERadioName]);
		}
		SendVehicleMessage(carid, COLOR_PURPLE,msg);
		SetVehicleStation(carid, radio);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /radiostation [station]");
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Do /radiostations to see the stations.");
	}
	return 1;
}
YCMD:customradiotrack(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Change a vehicles radio station");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid) || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not driving a vehicle!");
		return 1;
	}
	new msg[128], customradiourl[128];
	new carid = GetPlayerVehicleID(playerid);
	new active;
	if(!sscanf(params, "s[127]", customradiourl)) { //Optional parameter, if customradiourl's length is 0 the radio will go off
		if(!strcmp(customradiourl, "off")) {
			format(msg, sizeof(msg), "* %s turns off the radio.",GetPlayerNameEx(playerid, ENameType_RPName));
		} else {
			active = 1;
			format(msg, sizeof(msg), "* %s changes the radio station.",GetPlayerNameEx(playerid, ENameType_RPName));
		}
		SendVehicleMessage(carid, COLOR_PURPLE, msg);
		SetCustomVehicleStation(carid, customradiourl, active);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /customradiotrack [customurl] (.mp3, .pls, .wav)");
		SendClientMessage(playerid, X11_WHITE, "USAGE: /customradiotrack off to turn the radio off.");
	}
	return 1;
}
YCMD:radiostations(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists radio stations");
		return 1;
	}
	new msg[128];
	for(new i=0;i<sizeof(Radios);i++) {
		format(msg, sizeof(msg), "Radio Station %d - %s - %s - %s",i+1,Radios[i][ERadioName],Radios[i][ERadioGenre],Radios[i][ERadioURL]);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	}
	return 1;
}
YCMD:hradio(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Activate the house radio");
		return 1;
	}
	new msg[128];
	new radio;
	new house = getStandingExit(playerid, 150.0);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a house");
		return 1;
	}
	if(!sscanf(params, "d", radio)) {
		radio--;
		if(getHouseRadio(house) == radio) {
			SendClientMessage(playerid, X11_TOMATO_2, "This already is the radio station!");
			return 1;
		}
		if(radio < 0 || radio >= sizeof(Radios)) {
			format(msg, sizeof(msg), "* %s turns off the radio.",GetPlayerNameEx(playerid, ENameType_RPName));
		} else {
			format(msg, sizeof(msg), "* %s changes the radio station to %s",GetPlayerNameEx(playerid, ENameType_RPName),Radios[radio][ERadioName]);
		}
		ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetHouseStation(house, radio);
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /hradio [station]");
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Do /radiostations to see the stations.");
	}
	return 1;
}
getRadioURL(radio, url[], dst, carid = -1) {
	printf("getRadioURL(%d, %s, %s, %d)", radio, url, dst, carid);
	if(carid == -1) {
		if(!isValidRadionStation(radio)) return 0;
		format(url,dst,"%s", Radios[radio][ERadioURL]);
	} else {
		if(RadiosCustom[carid][ECustomRadio]) {
			format(url,dst,"%s", RadiosCustom[carid][ERadioURLCustom]);
		}
	}
	return 1;
}
hasCustomRadioStation(carid) {
	return RadiosCustom[carid][ECustomRadio] == 1 ? 1 : 0;
}
isValidRadionStation(radio) {
	return !(radio < 0 || radio >= sizeof(Radios));
}
/*
setPlayerCustomRadioURL(playerid, radiourl[], share) {
	deleteRadioURLPVars(playerid);
	if(share > 0) {
		if(getPlayerRadioStreamingID(playerid) == -1) {
			onSetCustomRadioURL(playerid, radiourl);
		}
	} else {
		PlayAudioStreamForPlayer(playerid, radiourl);
	}
	if(!strcmp(radiourl, "0", true)) {
		StopAudioStreamForPlayer(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "Radio Stream Dropped");
	}
	return 1;
}
onSetCustomRadioURL(playerid, url[]) {
	SetPVarString(playerid, "CustomRadioURL", url);
	SetPVarInt(playerid, "RadioPlayerID", playerid);
}
deleteRadioURLPVars(playerid) {
	if(GetPVarType(playerid, "RadioPlayerID") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "CustomRadioURL");
		DeletePVar(playerid, "RadioPlayerID");
		DeletePVar(playerid, "PlayerReceivingAudio");
	}
	return 1;
}
getPlayerCustomRadioURL(playerid, url[], dst) {
	if(GetPVarType(playerid, "CustomRadioURL") != PLAYER_VARTYPE_NONE) {
		new radioval[128];
		GetPVarString(playerid, "CustomRadioURL", radioval, sizeof(radioval));
		format(url,dst,"%s", radioval);
	}
	return 1;
}
getPlayerRadioStreamingID(playerid) {
	if(GetPVarType(playerid, "RadioPlayerID") == PLAYER_VARTYPE_NONE) {
		return -1;
	}
	new returnplayerid = GetPVarInt(playerid, "RadioPlayerID");
	return returnplayerid;
}
*/
YCMD:mp3player(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggle an MP3 player");
		return 1;
	}
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasMP3Player) {
	    SendClientMessage(playerid, COLOR_LIGHTRED, "   You don't have an MP3 player!");
	    return 1;
	}
	new id;
	new url[128],msg[128];
	if(!sscanf(params, "d", id)) {
		id--;
		new curstation = GetPVarInt(playerid, "MP3Player");
		if(id == -1) {
			DeletePVar(playerid, "MP3Player");
			format(msg, sizeof(msg), "* %s turns %s MP3 Player off",GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			StopAudioStreamForPlayer(playerid);
			
		} else {
			getRadioURL(id, url, sizeof(url));
			SetPVarInt(playerid, "MP3Player", curstation);
			PlayAudioStreamForPlayer(playerid, url);
			format(msg, sizeof(msg), "* %s sets %s MP3 Player to %s",GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid),Radios[id][ERadioName]);
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /mp3player [station]");
	}
	return 1;
}
/*
checkCustomPrivateRadios() {
	//CHC Screwaround
	new url[128];
	new Float:X, Float:Y, Float:Z, vw;
	new radioplayerid;
	foreach(Player, i) {
		radioplayerid = getPlayerRadioStreamingID(i);
		if(IsPlayerConnectEx(radioplayerid)) {
			vw = GetPlayerVirtualWorld(radioplayerid);
			GetPlayerPos(radioplayerid, X, Y, Z);
			if(!IsPlayerInRangeOfPoint(i, TRUNK_DISTANCE, X, Y, Z) && GetPlayerVirtualWorld(i) != vw) {
				StopAudioStreamForPlayer(i);
				deleteRadioURLPVars(i);
			} else {
				if(GetPVarType(i, "PlayerReceivingAudio") == PLAYER_VARTYPE_NONE) {
					if(radioplayerid != -1) {
						getPlayerCustomRadioURL(radioplayerid, url, sizeof(url));
						PlayAudioStreamForPlayer(i, url);
						SetPVarInt(i, "PlayerReceivingAudio", 1);
					} else {
						deleteRadioURLPVars(i);
					}
				}
			}
		}
	}
	//end
}
*/
checkCarRadios() {
	new url[128];
	new Float:X, Float:Y, Float:Z, vw;
	new engine,lights,alarm,doors,bonnet,boot,objective;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "VehTrunkPlaying") != PLAYER_VARTYPE_NONE) {
				new carid = GetPVarInt(i, "VehTrunkPlaying");
				if(!IsVehicleStreamedIn(carid, i)) {
					DeletePVar(i, "VehTrunkPlaying");
					StopAudioStreamForPlayer(i);
				} else {
					GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
					GetVehiclePos(carid, X, Y, Z);
					if(!IsPlayerInRangeOfPoint(i, TRUNK_DISTANCE, X, Y, Z) || boot == 0) {
						StopAudioStreamForPlayer(i);
						DeletePVar(i, "VehTrunkPlaying");
					}
				}
				continue;
			}
			for(new v=0;v<MAX_VEHICLES;v++) {
				if(IsVehicleStreamedIn(v,i)) {
					new station = (hasCustomRadioStation(v) != 0) ? 1 : getCarRadio(v);
					if(station != -1) {
						GetVehicleParamsEx(v,engine,lights,alarm,doors,bonnet,boot,objective);
						if(boot == 1) {
							GetVehiclePos(v, X, Y, Z);
							vw = GetVehicleVirtualWorld(v);
							if(IsPlayerInRangeOfPoint(i, TRUNK_DISTANCE, X, Y, Z) && GetPlayerVirtualWorld(i) == vw) {
								if(isValidRadionStation(station)) {
									if(GetPVarInt(i, "VehTrunkPlaying") != v) {
										SetPVarInt(i, "VehTrunkPlaying", v);
										getRadioURL(station, url, sizeof(url), (hasCustomRadioStation(v)) ? v : -1);
										PlayAudioStreamForPlayer(i, url,X,Y,Z,TRUNK_DISTANCE,1);
										continue;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}