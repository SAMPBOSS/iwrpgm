new PlayerText:InfoBoxHeader[MAX_PLAYERS];
new PlayerText:InfoBoxContent[MAX_PLAYERS];

new PlayerText:MapTextDraw[MAX_PLAYERS];

new Text:ServerURLTD;

new InfoBoxShown[MAX_PLAYERS];

new PlayerText:ScriptMessageTD[MAX_PLAYERS]; //you are cuffed... anims, etc

#define INFO_DISAPPEAR_TIME 5000 //5000 ms

forward HideScriptMessage(playerid);

TextDrawsOnGameModeInit() {
	ServerURLTD = TextDrawCreate(42.000000, 429.000000, "IW-RP.com");
	TextDrawBackgroundColor(ServerURLTD, 255); //255
	TextDrawFont(ServerURLTD, 3);
	TextDrawLetterSize(ServerURLTD, 0.270000, 1.000000);
	TextDrawColor(ServerURLTD, -1724710657); //-1724710657
	TextDrawSetOutline(ServerURLTD, 1);
	TextDrawSetProportional(ServerURLTD, 1);
	
}


TextDrawOnPlayerConnect(playerid) {
	if(GetPVarInt(playerid, "PlayerTDsSetUP") == 1) return 0;
	InfoBoxHeader[playerid] = CreatePlayerTextDraw(playerid, 115.000000, 121.000000, "---");
	PlayerTextDrawAlignment(playerid, InfoBoxHeader[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid, InfoBoxHeader[playerid], 255);
	PlayerTextDrawFont(playerid, InfoBoxHeader[playerid], 2);
	PlayerTextDrawLetterSize(playerid, InfoBoxHeader[playerid], 0.470000, 1.000000);
	PlayerTextDrawColor(playerid, InfoBoxHeader[playerid], -1);
	PlayerTextDrawSetOutline(playerid, InfoBoxHeader[playerid], 1);
	PlayerTextDrawSetProportional(playerid, InfoBoxHeader[playerid], 1);
	PlayerTextDrawUseBox(playerid, InfoBoxHeader[playerid], 1);
	PlayerTextDrawBoxColor(playerid, InfoBoxHeader[playerid], 38550);
	PlayerTextDrawTextSize(playerid, InfoBoxHeader[playerid], 23.000000, 194.000000);
	

	InfoBoxContent[playerid] = CreatePlayerTextDraw(playerid, 115.000000, 133.000000, "---");
	PlayerTextDrawAlignment(playerid, InfoBoxContent[playerid], 2);
	PlayerTextDrawBackgroundColor(playerid, InfoBoxContent[playerid], 255);
	PlayerTextDrawFont(playerid, InfoBoxContent[playerid], 2);
	PlayerTextDrawLetterSize(playerid, InfoBoxContent[playerid], 0.239998, 1.099999);
	PlayerTextDrawColor(playerid, InfoBoxContent[playerid], -1);
	PlayerTextDrawSetOutline(playerid, InfoBoxContent[playerid], 1);
	PlayerTextDrawSetProportional(playerid, InfoBoxContent[playerid], 1);
	PlayerTextDrawUseBox(playerid, InfoBoxContent[playerid], 1);
	PlayerTextDrawBoxColor(playerid, InfoBoxContent[playerid], 150);
	PlayerTextDrawTextSize(playerid, InfoBoxContent[playerid], -19.000000, 194.000000);
	
	
	ScriptMessageTD[playerid] = CreatePlayerTextDraw(playerid,143.000000, 407.000000, "Default Message");
	PlayerTextDrawBackgroundColor(playerid,ScriptMessageTD[playerid], 255);
	PlayerTextDrawFont(playerid,ScriptMessageTD[playerid], 3);
	PlayerTextDrawLetterSize(playerid,ScriptMessageTD[playerid], 0.440000, 0.899999);
	PlayerTextDrawColor(playerid,ScriptMessageTD[playerid], -1);
	PlayerTextDrawSetOutline(playerid,ScriptMessageTD[playerid], 1);
	PlayerTextDrawSetProportional(playerid,ScriptMessageTD[playerid], 1);
	
	TextDrawShowForPlayer(playerid,ServerURLTD);
	SetPVarInt(playerid, "PlayerTDsSetUP", 1);
	if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_MovieMode) 
		SetMapTextDraw(playerid, 1);
	return 1;
}
ShowScriptMessage(playerid, msg[], timeout = -1) {
	PlayerTextDrawSetString(playerid, ScriptMessageTD[playerid], msg);
	PlayerTextDrawShow(playerid, ScriptMessageTD[playerid]);
	if(GetPVarType(playerid, "ScriptMsgTimer") != PLAYER_VARTYPE_NONE) {
		new timerid = GetPVarInt(playerid, "ScriptMsgTimer");
		KillTimer(timerid);
		DeletePVar(playerid, "ScriptMsgTimer");
	}
	if(timeout != -1) {
		new timerid = SetTimerEx("HideScriptMessage", timeout, false, "d", playerid);
		SetPVarInt(playerid, "ScriptMsgTimer", timerid);
	}
}
public HideScriptMessage(playerid) {
	if(GetPVarType(playerid, "ScriptMsgTimer") != PLAYER_VARTYPE_NONE) {
		new timerid = GetPVarInt(playerid, "ScriptMsgTimer");
		KillTimer(timerid);
		DeletePVar(playerid, "ScriptMsgTimer");
	}
	PlayerTextDrawHide(playerid, ScriptMessageTD[playerid]);
}
TextDrawOnPlayerDisconnect(playerid, reason) {
	if(reason == 3) return 0; //switchchar
	PlayerTextDrawDestroy(playerid, InfoBoxHeader[playerid]);
	PlayerTextDrawDestroy(playerid, InfoBoxContent[playerid]);
	if(MapTextDraw[playerid] != PlayerText:-1) {
		PlayerTextDrawHide(playerid, MapTextDraw[playerid]);
		PlayerTextDrawDestroy(playerid, MapTextDraw[playerid]);
	}
	MapTextDraw[playerid] = PlayerText:-1;
	PlayerTextDrawDestroy(playerid, ScriptMessageTD[playerid]);
	InfoBoxShown[playerid] = 0;
	return 1;
}
forward HideInfoDialog(playerid);
public HideInfoDialog(playerid) {
	PlayerTextDrawHide(playerid, InfoBoxHeader[playerid]);
	PlayerTextDrawHide(playerid, InfoBoxContent[playerid]);
	InfoBoxShown[playerid] = 0;
}
ShowInfoTextDraw(playerid, header[],msg[]) {

	if(InfoBoxShown[playerid] == 1) return 0;
	
	PlayerTextDrawSetString(playerid, InfoBoxHeader[playerid], header);
	PlayerTextDrawSetString(playerid, InfoBoxContent[playerid], msg);	
	
	PlayerTextDrawShow(playerid, InfoBoxHeader[playerid]);
	PlayerTextDrawShow(playerid, InfoBoxContent[playerid]);
	
	InfoBoxShown[playerid] = 1;
	
	SetTimerEx("HideInfoDialog", INFO_DISAPPEAR_TIME,false,"d",playerid);
	
	return 1;
	
}
updatePlayerMapLocations() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(~EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_MovieMode) {
				UpdateMapTextDraw(i);
			}
		}
	}
}
UpdateMapTextDraw(playerid) {
	if(GetPlayerInterior(playerid) != 0) return 0;
	if(MapTextDraw[playerid] != PlayerText:-1) {
		if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_MovieMode) { //movie mode must be set off
			PlayerTextDrawHide(playerid, MapTextDraw[playerid]);
			PlayerTextDrawDestroy(playerid, MapTextDraw[playerid]);
			MapTextDraw[playerid] = PlayerText:-1;
			return 1;
		}
		PlayerTextDrawSetString(playerid, MapTextDraw[playerid], GetPlayerArea(playerid));
	} else {
		if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_MovieMode) {
			if(MapTextDraw[playerid] == PlayerText:-1) {
				SetMapTextDraw(playerid, 1);
			}
		}
	}
	return 0;
}
TextDrawsOnPlayerEnterInterior(playerid, newint, oldint) {
	#pragma unused oldint
	if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_MovieMode) {
		if(newint == 0)
			SetMapTextDraw(playerid, 1);
		else
			SetMapTextDraw(playerid, 0);
	}
}
SetMapTextDraw(playerid, on) {

	if(on == 1) {
		MapTextDraw[playerid] = CreatePlayerTextDraw(playerid, 95.000000, 310.000000, "Los Santos");
		PlayerTextDrawAlignment(playerid, MapTextDraw[playerid], 2);
		PlayerTextDrawBackgroundColor(playerid, MapTextDraw[playerid], 255);
		PlayerTextDrawFont(playerid, MapTextDraw[playerid], 3);
		PlayerTextDrawLetterSize(playerid, MapTextDraw[playerid], 0.270000, 1.000000);
		PlayerTextDrawColor(playerid, MapTextDraw[playerid], -1);
		PlayerTextDrawSetOutline(playerid, MapTextDraw[playerid], 1);
		PlayerTextDrawSetProportional(playerid, MapTextDraw[playerid], 1);
		PlayerTextDrawShow(playerid, MapTextDraw[playerid]);
	} else {
		PlayerTextDrawHide(playerid, MapTextDraw[playerid]);
		PlayerTextDrawDestroy(playerid,MapTextDraw[playerid]);
		MapTextDraw[playerid] = PlayerText:-1;
	}
}