enum ETurfInfo {
	ETurfName[32],
	Float:ETurfMinX,
	Float:ETurfMinY,
	Float:ETurfMaxX,
	Float:ETurfMaxY,
	ETurfCaptureTime,
	ETurfSQLID,
	ETurfGangZoneID,
	ETurfOwner, //family ID of owner
	ETurfAreaID,
	TurfFlags:ETurfFlags,
}

#define MAX_TURFS 14
#define TURF_ROUNDS 10
#define TURF_COOLDOWN 10800
#define TURF_WAIT_TIME 10
#define MAX_TURF_EXTENSIONS 5

new Turfs[MAX_TURFS][ETurfInfo];
new TurfPoints[MAX_FAMILIES];//how many points a family has
new turfwartimer;
new turfwarfamily; //ID of family in the lead of the turf war
new turfwarturf;
new turfwartime;
new numturfextensions;

enum {
	ETurfDialog_ShowTurfs = ETurfDialog_Base + 1,
	ETurfDialog_ShowTurfsEdit,
	ETurfDialog_EditTurfFlags,
};

enum TurfFlags (<<= 1) {
	ETurfFlags_WeaponsOnly = 1,
	ETurfFlags_MeleeOnly,
	ETurfFlags_TurfDisabled,
};

enum ETurfFlagInfo {
	TurfFlags:ETurfFlagID, //flag ID
	ETurfFlagDesc[64],//description of the turf flag
};

new TurfFlagDescription[][ETurfFlagInfo] = 
{
	{ETurfFlags_WeaponsOnly, "Weapons Only"},
	{ETurfFlags_MeleeOnly, "Melee Only"},
	{ETurfFlags_TurfDisabled, "Turf Disabled (Not Available)"}
};

turfsOnGameModeInit() {
	loadTurfs();
	turfwarfamily = -1;
	turfwarturf = -1;
	turfwartime = -1;
	numturfextensions = 0;
}
loadTurfs() {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT `id`,`minx`,`miny`,`maxx`,`maxy`,`name`,`owner`,Unix_Timestamp(`claimtime`),`flags` FROM `turfs`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadTurfs", "");
}
forward OnLoadTurfs();
public OnLoadTurfs() {
	new rows, fields;
	cache_get_data(rows, fields);
	new id_string[128],id;
	for(new i=0;i<rows;i++) {
	
		cache_get_row(i, 0, id_string);
		id = strval(id_string);
		Turfs[i][ETurfSQLID] = id;
		
		cache_get_row(i, 1, id_string);
		Turfs[i][ETurfMinX] = floatstr(id_string);
		
		cache_get_row(i, 2, id_string);
		Turfs[i][ETurfMinY] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		Turfs[i][ETurfMaxX] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		Turfs[i][ETurfMaxY] = floatstr(id_string);
		
		cache_get_row(i, 5, Turfs[i][ETurfName]);
		
		cache_get_row(i, 6, id_string);
		Turfs[i][ETurfOwner] = FindFamilyBySQLID(strval(id_string));

		cache_get_row(i, 7, id_string);
		Turfs[i][ETurfCaptureTime] = strval(id_string);
		
		cache_get_row(i, 8, id_string);
		Turfs[i][ETurfFlags] = TurfFlags:strval(id_string);
		
		Turfs[i][ETurfGangZoneID] = GangZoneCreate(Turfs[i][ETurfMinX],Turfs[i][ETurfMinY],Turfs[i][ETurfMaxX],Turfs[i][ETurfMaxY]);
		Turfs[i][ETurfAreaID] = CreateDynamicRectangle(Turfs[i][ETurfMinX],Turfs[i][ETurfMinY],Turfs[i][ETurfMaxX],Turfs[i][ETurfMaxY], 0, 0);
	}
}
stock returnTurfFlagDesc(turfid) {
	new TurfFlags:aflags = Turfs[turfid][ETurfFlags];
	tempstr[0] = 0;
	for(new i=0;i<sizeof(TurfFlagDescription);i++) {
		if(aflags & TurfFlagDescription[i][ETurfFlagID]) {
			format(tempstr,sizeof(tempstr),"Type: %s",TurfFlagDescription[i][ETurfFlagDesc]);
		}
	}
	return tempstr;
}
ShowTurfFlagsMenu(playerid, turfid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new TurfFlags:aflags = Turfs[turfid][ETurfFlags];
	new statustext[32]; 
	SetPVarInt(playerid, "TurfEditing", turfid);
	for(new i=0;i<sizeof(TurfFlagDescription);i++) {
		if(aflags & TurfFlagDescription[i][ETurfFlagID]) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}NO";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",TurfFlagDescription[i][ETurfFlagDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, ETurfDialog_EditTurfFlags, DIALOG_STYLE_LIST, "Modify Turf Flags",dialogstr,"Toggle", "Done");
}
turfsOnPlayerSpawn(playerid) {
	for(new i=0;i<sizeof(Turfs);i++) {
		if(Turfs[i][ETurfSQLID] != 0) {
			GangZoneShowForPlayer(playerid, Turfs[i][ETurfGangZoneID],turfColour(i));
		}
	}
	if(turfwarturf > 0) {
		GangZoneFlashForPlayer(playerid, Turfs[turfwarturf][ETurfGangZoneID], convertTurfColour(FamilyColour(turfwarfamily)));
	}
}
turfsOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	query[0] = 0;//[256];
	switch(dialogid) {
		case ETurfDialog_ShowTurfsEdit: {
			if(!response) {
				return 1;
			}
			ShowTurfFlagsMenu(playerid, listitem);
		}
		case ETurfDialog_EditTurfFlags: {
			if(!response) {
				DeletePVar(playerid, "TurfEditing");
				return 1;
			}
			new turfid = GetPVarInt(playerid, "TurfEditing");
			new TurfFlags:flag = TurfFlagDescription[listitem][ETurfFlagID];
			DeletePVar(playerid, "TurfEditing");
			/*
			if(Turfs[turfid][ETurfFlags] & flag) {
				Turfs[turfid][ETurfFlags] &= ~flag;
			} else {
				Turfs[turfid][ETurfFlags] |= flag;
			}
			*/
			//Only set one flag at a time
			Turfs[turfid][ETurfFlags] = flag;
			query[0] = 0;
			format(query, sizeof(query), "UPDATE `turfs` SET `flags` = %d WHERE `id` = %d",_:Turfs[turfid][ETurfFlags],Turfs[turfid][ETurfSQLID]);
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			ShowTurfFlagsMenu(playerid, turfid);
			return 1;
		}
	}
	return 1;
}
turfColour(turf) {
	new colour = FamilyColour(Turfs[turf][ETurfOwner]);
	colour = convertTurfColour(colour);
	return colour;
}
convertTurfColour(colour) {
	new alpha = 0x90;
	colour &= 0xffffff00;
	colour |= alpha;
	return colour;
}
YCMD:takeover(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for attempting to take over a turf");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
		return 1;
	}
	if(gamesoff == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Turf Wars are disabled!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanTakeover) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	new turf = getPlayerTurf(playerid);
	if(turf == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a turf.");
		return 1;
	}
	if(FamilySafes[family][ESafeSQLID] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family needs a safe to do turf wars!");
		return 1;
	}
	if(Families[family][EFamilyColour] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need a family colour for this");
		return 1;
	}
	if(turfwarturf != -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There is already a turf war going on!");
		return 1;
	}
	new captime = remaingTurfCaptureTime(turf);
	if(captime != 0) {
		new msg[128];
		captime /= 60;
		format(msg, sizeof(msg), "This turf won't be capturable for %d minutes!",captime);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(numFamilyMembersInTurf(family, turf) < 3) {
		SendClientMessage(playerid, X11_TOMATO_2, "There must be at least 3 family members on the turf");
		#if !debug
		return 1;
		#endif
	}
	if(Turfs[turf][ETurfOwner] == family) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family already owns this turf!");
		return 1;
	}
	startTurfWar(family, turf);
	return 1;
}
getPlayerTurf(playerid) {
	for(new i=0;i<sizeof(Turfs);i++) {
		if(Turfs[i][ETurfSQLID] != 0) {
			if(IsPlayerInDynamicArea(playerid, Turfs[i][ETurfAreaID])) {
				return i;
			}
		}
	}
	return -1;
}
IsPlayerInTurfWar(playerid) {
	return GetPVarInt(playerid, "Family") != 0 && turfwarturf != -1 && getPlayerTurf(playerid) != -1;
}
numFamilyMembersInTurf(familyid, turfid) {
	new num;
	new sqlid = SQLIDFromFamily(familyid);
	foreach(Player, i) {
		if(GetPVarInt(i, "Family") == sqlid) {
			if(IsPlayerInDynamicArea(i, Turfs[turfid][ETurfAreaID])) {
				num++;
			}
		}
	}
	return num;
}
startTurfWar(familyid, turfid) {
	new msg[128];
	new colour = FamilyColour(familyid);
	colour = convertTurfColour(colour);
	format(msg, sizeof(msg), "%s started a turfwar at %s!",GetFamilyName(familyid), Turfs[turfid][ETurfName]);
	FamilyMessage(-1, COLOR_RED, msg);
	setTurfFlashColour(turfid, colour);
	turfwarturf = turfid;
	turfwarfamily = familyid;
	turfwartime = 0;
	turfwartimer = SetTimer("TurfTick",60000,true);
}
setTurfFlashColour(turf, colour) {
	foreach(Player, i) {
		GangZoneFlashForPlayer(i, Turfs[turf][ETurfGangZoneID], colour);
	}
}
setTurfColour(turf, colour) {
	foreach(Player, i) {
		if(IsPlayerConnected(i)) {
			GangZoneStopFlashForPlayer(i, Turfs[turf][ETurfGangZoneID]);
			GangZoneHideForPlayer(i, Turfs[turf][ETurfGangZoneID]);
			GangZoneShowForPlayer(i, Turfs[turf][ETurfGangZoneID], colour);
		}
	}
}
SendTurfMessage(turf, colour, msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "Family") != 0) {
			if(IsPlayerInDynamicArea(i, Turfs[turf][ETurfAreaID])) {
				SendClientMessage(i, colour, msg);
			}
		}
	}
}
turfsOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused playerid
	if(turfwarturf == -1) {
		return 1;
	}
	if(killerid != INVALID_PLAYER_ID) {
		turfDecide(playerid, killerid, reason);
	}
	return 1;
}
turfDecide(playerid, killerid, gunid) {
	new msg[128];
	new turf = getPlayerTurf(killerid);
	if(turf == -1) return 0;
	new fam = GetPVarInt(killerid, "Family");
	fam = FindFamilyBySQLID(fam);
	if(fam == -1 || Families[fam][EFamilyColour] == 0) return 0;
	
	if(Turfs[turf][ETurfFlags] & ETurfFlags_MeleeOnly) {
		if(getWeaponClassType(killerid, gunid) == _:EWeaponType_Melee) {
			GiveTurfWarPoint(fam, 1);
			format(msg, sizeof(msg), "* %s got 1 point for killing %s",GetFamilyName(fam),GetPlayerNameEx(playerid, ENameType_CharName));
		} else {
			GiveTurfWarPoint(fam, 0);
			format(msg, sizeof(msg), "* %s lost 1 point for killing %s with a weapon in a melee only turf.",GetFamilyName(fam),GetPlayerNameEx(playerid, ENameType_CharName));
		}
	} else if(Turfs[turf][ETurfFlags] & ETurfFlags_WeaponsOnly) {
		GiveTurfWarPoint(fam, 1);
		format(msg, sizeof(msg), "* %s got 1 point for killing %s",GetFamilyName(fam),GetPlayerNameEx(playerid, ENameType_CharName));
	}
	SendTurfMessage(turf, COLOR_RED, msg);
	return 1;
}
GetTurfWarLeader() {
	new maxscore,maxi;
	for(new i=0;i<sizeof(TurfPoints);i++) {
		if(TurfPoints[i] > maxscore) {
			maxscore = TurfPoints[i];
			maxi = i;
		}
	}
	return maxi;
}
GiveTurfWarPoint(family, positive) {
	new leader = GetTurfWarLeader();
	if(positive > 0) {
		TurfPoints[family]++;
	} else {
		TurfPoints[family]--;
	}
	new msg[128];
	if(GetTurfWarLeader() == family && leader != family && Families[family][EFamilyColour] != 0 && FamilySafes[family][ESafeSQLID] != 0) {
		turfwarfamily = family;
		if(turfwartime > 1)
			turfwartime--;
		format(msg, sizeof(msg), "* %s is now in the lead at %s",GetFamilyName(family),Turfs[turfwarturf][ETurfName]);
		FamilyMessage(-1, X11_RED, msg);
		setTurfFlashColour(turfwarturf, convertTurfColour(FamilyColour(family)));
	}
}
isTurfTied() {
	new leader = GetTurfWarLeader();
	new maxscore = TurfPoints[leader];
	for(new i=0;i<sizeof(TurfPoints);i++) {
		if(TurfPoints[i] >= maxscore && i != leader && maxscore != 0) {
			return 1;
		}
	}
	return 0;
}
resyncTurfs() {
	for(new i=0;i<MAX_TURFS;i++) {
		syncTurfColour(i);
	}
}
syncTurfColour(turf) {
	setTurfColour(turf, turfColour(turf));
}
forward TurfTick();
public TurfTick() {
	new string[128];
	if(++turfwartime == TURF_ROUNDS) {
		Turfs[turfwarturf][ETurfCaptureTime] = gettime();
		if(isTurfTied() && numturfextensions < MAX_TURF_EXTENSIONS) {
			FamilyMessage(-1, COLOR_RED, "� The turf is tied, extending the round for 1 minute");
			if(turfwartime > 1) {
				--turfwartime;
				numturfextensions++;
			}
			return 0;
		} else if(!numFamilyMembersInTurf(turfwarfamily,turfwarturf)) {
			Turfs[turfwarturf][ETurfOwner] = -1;
			format(string, sizeof(string), "� The family abandoned %s, so it was awarded to no one.",  Turfs[turfwarturf][ETurfName]);
			FamilyMessage(-1, COLOR_RED, string);
			setTurfColour(turfwarturf, turfColour(turfwarturf));
		} else {
			format(string, sizeof(string), "� %s took over the %s turf!",GetFamilyName(turfwarfamily),  Turfs[turfwarturf][ETurfName]);
			FamilyMessage(-1, COLOR_RED, string);
			Turfs[turfwarturf][ETurfOwner] = turfwarfamily;
			setTurfColour(turfwarturf, turfColour(turfwarturf));
		}
		for(new i=0;i<MAX_FAMILIES;i++) {
			TurfPoints[i] = 0;
		}
		saveTurf(turfwarturf);
		turfwarfamily = -1;
		turfwarturf = -1;
		turfwartime = -1;
		KillTimer(turfwartimer);
		turfwartimer = -1;
		numturfextensions = 0;
	} else {
		format(string, sizeof(string), "Turf: %s, Family: %s, Timer: %d:00", Turfs[turfwarturf][ETurfName],GetFamilyName(turfwarfamily), turfwartime);
		FamilyMessage(-1, COLOR_RED, string);
	}
	return 1;
}
YCMD:gototurf(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a turf");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	new index;
	if(!sscanf(params,"d",index)) {
		index--;
		if(index < 0 || index > sizeof(Turfs)-1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Turf ID");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gototurf [pointid]");
		return 1;
	}
	X = Turfs[index][ETurfMinX];
	Y = Turfs[index][ETurfMinY];
	Z = 14.0;
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		TPEntireCar(carid, 0, 0);
		LinkVehicleToInterior(carid, 0);
		SetVehicleVirtualWorld(carid, 0);
		SetVehiclePos(carid, X, Y, Z);
		
	} else {
		SetPlayerPosFindZ(playerid, X, Y, Z);
	}
	SetPlayerVirtualWorld(playerid, 0);
	SetPlayerInterior(playerid,0);
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}

YCMD:turfscore(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists the turf score");
		return 1;
	}
	if(turfwarturf == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There isn't a turf war going on!");
		return 1;
	}
	new string[128];
	SendClientMessage(playerid, X11_WHITE, "|***** Turf Scores *****|");
	for(new i; i < MAX_FAMILIES; i++)
	{
	   	if(IsValidFamily(i) && TurfPoints[i] != 0)
	   	{
			format(string, sizeof(string), "%s: %d", GetFamilyName(i), TurfPoints[i]);
			SendClientMessage(playerid, COLOR_RED, string);
		}
	}
	SendClientMessage(playerid, X11_WHITE, "|****************************|");
	return 1;
}
YCMD:turfpeople(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all the people in a turf war");
		return 1;
	}
	if(turfwarturf == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There isn't a turf war going on!");
		return 1;
	}
	if(!IsPlayerInDynamicArea(playerid, Turfs[turfwarturf][ETurfAreaID])) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't inside the turf!");
		return 1;
	}
	new user,msg[128];
	new sqlid;
	if(!sscanf(params,"k<playerLookup>", user)) {
		if(IsPlayerConnectEx(user)) {
			if(!IsPlayerInDynamicArea(user, Turfs[turfwarturf][ETurfAreaID]) || GetPVarType(user, "SpecPlayer") != PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not inside the turf!");
				return 1;
			}
			new Float:X, Float:Y, Float:Z;
			GetPlayerPos(user, X, Y, Z);
			SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
			SetTimerEx("RemoveCheckpoint", 25000, false, "d", playerid);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "|***** Turf People *****|");
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				sqlid = FindFamilyBySQLID(GetPVarInt(i, "Family"));
				if(sqlid != -1 && IsPlayerInDynamicArea(i, Turfs[turfwarturf][ETurfAreaID]) && GetPVarType(i, "SpecPlayer") == PLAYER_VARTYPE_NONE) {
					format(msg, sizeof(msg), "%s: %s",GetFamilyName(sqlid),GetPlayerNameEx(i, ENameType_RPName));
					SendClientMessage(playerid, FamilyColour(sqlid), msg);
				}
			}
		}
		SendClientMessage(playerid, X11_WHITE, "|****************************|");
	}
	return 1;
}
YCMD:turfs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all turfs and their owners");
		return 1;
	}
	new string[128];
	dialogstr[0] = 0;
	new colour;
	new owner[64];
	for(new i=0;i<sizeof(Turfs);i++) {
		new rcaptime = remaingTurfCaptureTime(i);
		if(Turfs[i][ETurfOwner] == -1) {
			colour = X11_WHITE;
		} else {
			colour = FamilyColour(Turfs[i][ETurfOwner]);
		}
		getTurfOwner(i, owner, sizeof(owner));
		rcaptime = floatround((rcaptime/60)/60);
		if(i == turfwarturf) {
			format(string, sizeof(string), "{%s}%d. %s Owner: %s, {FF0000}Being Claimed, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,returnTurfFlagDesc(i));
		} else if(remaingTurfCaptureTime(i) != 0) {
			if(rcaptime != 0) { //hours left
				format(string, sizeof(string), "{%s}%d. %s Owner: %s, Hours: %d, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,rcaptime,returnTurfFlagDesc(i));
			} else {
				rcaptime = floatround((remaingTurfCaptureTime(i)/60));
				format(string, sizeof(string), "{%s}%d. %s Owner: %s, Minutes: %d, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,rcaptime, returnTurfFlagDesc(i));
			}
		} else {
			format(string, sizeof(string), "{%s}%d. %s Owner: %s, Ready for capture, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,returnTurfFlagDesc(i));
		}
		strcat(dialogstr, string);
	}
	ShowPlayerDialog(playerid, ETurfDialog_ShowTurfs, DIALOG_STYLE_MSGBOX, "{00BFFF}Turfs List", dialogstr, "Done", "");
	return 1;
}
YCMD:turfflags(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Let's you edit turf flags");
		return 1;
	}
	new string[128];
	dialogstr[0] = 0;
	new colour;
	new owner[64];
	for(new i=0;i<sizeof(Turfs);i++) {
		new rcaptime = remaingTurfCaptureTime(i);
		if(Turfs[i][ETurfOwner] == -1) {
			colour = X11_WHITE;
		} else {
			colour = FamilyColour(Turfs[i][ETurfOwner]);
		}
		getTurfOwner(i, owner, sizeof(owner));
		rcaptime = floatround((rcaptime/60)/60);
		if(i == turfwarturf) {
			format(string, sizeof(string), "{%s}%d. %s Owner: %s, {FF0000}Being Claimed, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,returnTurfFlagDesc(i));
		} else if(remaingTurfCaptureTime(i) != 0) {
			if(rcaptime != 0) { //hours left
				format(string, sizeof(string), "{%s}%d. %s Owner: %s, Hours: %d, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,rcaptime,returnTurfFlagDesc(i));
			} else {
				rcaptime = floatround((remaingTurfCaptureTime(i)/60));
				format(string, sizeof(string), "{%s}%d. %s Owner: %s, Minutes: %d, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,rcaptime, returnTurfFlagDesc(i));
			}
		} else {
			format(string, sizeof(string), "{%s}%d. %s Owner: %s, Ready for capture, %s\n",getColourString(colour),i+1,Turfs[i][ETurfName],owner,returnTurfFlagDesc(i));
		}
		strcat(dialogstr, string);
	}
	ShowPlayerDialog(playerid, ETurfDialog_ShowTurfsEdit, DIALOG_STYLE_LIST, "{00BFFF}Turfs List - Edit Flags", dialogstr, "Edit", "Cancel");
	return 1;
}
saveTurf(turfid) {
	query[0] = 0;//[128];
	new sqlid = SQLIDFromFamily(Turfs[turfid][ETurfOwner]);
	format(query, sizeof(query), "UPDATE `turfs` SET `owner` = %d, `claimtime` = FROM_UNIXTIME(%d) WHERE `id` = %d",sqlid,Turfs[turfid][ETurfCaptureTime],Turfs[turfid][ETurfSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
getTurfOwner(turf, dst[], dstlen) {
	if(Turfs[turf][ETurfOwner] == -1) {
		format(dst, dstlen, "No-One");
	} else {
		format(dst, dstlen, "%s",GetFamilyName(Turfs[turf][ETurfOwner]));
	}
	return 0;
}
remaingTurfCaptureTime(turf) {
	new timenow = gettime();
	if(TURF_COOLDOWN-(timenow-Turfs[turf][ETurfCaptureTime]) > 0) {
		return TURF_COOLDOWN-(timenow-Turfs[turf][ETurfCaptureTime]);
	}
	return 0;
}
YCMD:agiveturf(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a turf to a family");
		return 1;
	}
	new turf, family;
	new msg[128];
	if(!sscanf(params,"dd", turf, family)) {
		turf--;
		if(turf < 0 || turf > sizeof(Turfs)-1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Turf");
			return 1;
		}
		if(!IsValidFamily(family)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
			return 1;
		}
		if(turfwarturf == turf) {
			SendClientMessage(playerid, X11_TOMATO_2, "There is a turf war going on at this turf!");
			return 1;
		}
		Turfs[turf][ETurfOwner] = family;
		setTurfColour(turf, turfColour(turf));
		format(msg, sizeof(msg), "* %s gave %s to %s",GetPlayerNameEx(playerid, ENameType_AccountName), Turfs[turf][ETurfName],GetFamilyName(family));
		FamilyMessage(-1, FamilyColour(family), msg);
		saveTurf(turf);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /agiveturf [turfid] [familyid]");
	}
	return 1;
}
turfsOnPayDay() {
	new familypayouts[MAX_FAMILIES];
	for(new i=0;i<sizeof(Turfs);i++) {
		if(Turfs[i][ETurfOwner] != -1) {
			new fam = Turfs[i][ETurfOwner];
			if(IsValidFamily(fam)) {
				 familypayouts[fam] += RandomEx(500, 2000);
			}
		}
	}
	for(new i=0;i<sizeof(familypayouts);i++) {
		if(familypayouts[i] != 0) {
			new safeid = FamilySafes[i][ESafeSQLID];
			if(safeid != 0) {
				addToItem(safeid, ESafeItemType_Money, familypayouts[i]);
			}
		}
	}
}
familyOwnsTurf(famid, turf) {
	if(turf < 0 || turf > sizeof(Turfs)-1) return 0;
	return Turfs[turf][ETurfOwner] == famid;
}
#pragma unused familyOwnsTurf