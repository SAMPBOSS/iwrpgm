enum {
	EPointDialog_ShowPoints = EPointDialog_Base + 1,
};

enum EPointType {
	EPointType_MatsA,
	EPointType_MatsB,
	EPointType_MatsC,
	EPointType_Pot,
	EPointType_Coke,
	EPointType_Meth,
	EPointType_Armoury
};
enum EPointData {
	EPointSQLID,
	Float:EPointDataX,
	Float:EPointDataY,
	Float:EPointDataZ,
	EPointDataName[32],
	EPointDataInt,
	EPointDataVW,
	EPointDataIconID,
	EPointType:EPointDataType,
	EPointDataDeliver, //1 if deliver place, otherwise 0, -1 if only claim
	EPointPickupModel,
	EPointPickupID,
	Text3D:EPointLabel,
	EPointCaptureTime,
	EPointOwnerFamily, //family index of owning family
	EPointMapSlotID,
	EPointLastVehicleID, //The last vehicle id created at a certain point. Used for cleanup..
	EPointCTVDelivery,
};
#define MAX_POINTS 11
new Points[MAX_POINTS][EPointData];

#define POINT_ROUNDS 10
#define POINT_DISTANCE 10.0
#define POINTWAR_COOLDOWN 5
#define CAPTURE_COOLDOWN 7200 //2 hours between captures
#define CAPTURE_WAIT_TIME 10
#define POINTTAG_COLOUR_YELLOW 0xFBFF00AA
#define POINTTAG_COLOUR_RED COLOR_BRIGHTRED
#define MAX_CAPTURE_TIME 1200 //The maximum allowed time to capture a van
#define MAX_ALLOWED_VANS 1 //Max allowed vans to drop, 1 for now
#define LOADOUT_TIME 60 //Time required to unload the van

forward onLoadPoints();

new pointwarpoint;
new timecount;

getPointColour(point, unowned_color = POINTTAG_COLOUR_YELLOW) {
	if(point == pointwarpoint) {
		return POINTTAG_COLOUR_RED;
	}
	if(Points[point][EPointOwnerFamily] == -1) {
		return unowned_color;
	} else {
		return FamilyColour(Points[point][EPointOwnerFamily]);
	}
}
givePlayerPointTip(point) {
	new string[128];
	switch(Points[point][EPointDataType]) {
		case EPointType_MatsA: {
			format(string, sizeof(string), "/mats");
		}
		case EPointType_MatsB: {
			format(string, sizeof(string), "/mats");
		}
		case EPointType_MatsC: {
			format(string, sizeof(string), "/mats");
		}
		case EPointType_Pot: {
			format(string, sizeof(string), "N/A");
		}
		case EPointType_Coke: {
			format(string, sizeof(string), "N/A");
		}
		case EPointType_Meth: {
			format(string, sizeof(string), "N/A");
		}
		/* case EPointType_Armoury: {
			return 1;
		} */
	}
	return string;
}
nearestPoint(playerid) {
	for(new i=0;i<sizeof(Points);i++) {
		if(IsPlayerInRangeOfPoint(playerid, POINT_DISTANCE, Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ])) {
			return i;
		}
	}
	return -1;
}
//0 = pickup, 1 = deliver, -1 = either
isNearPointType(playerid, EPointType:type, deliver = -1) {
	if(!IsPlayerConnectEx(playerid)) {
		return 0;
	}
	for(new i=0;i<sizeof(Points);i++) {
		if(IsPlayerInRangeOfPoint(playerid, POINT_DISTANCE, Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ])) {
			if(Points[i][EPointDataType] == type) {
				if(deliver == -1 || (Points[i][EPointDataDeliver] == deliver)) {
					return i;
				}
			}
		}
	}
	return -1;
}
remaingPointCaptureTime(point) {
	new timenow = gettime();
	if(CAPTURE_COOLDOWN-(timenow-Points[point][EPointCaptureTime]) > 0) {
		//printf("Capture Time: %d", Points[point][EPointCaptureTime]);
		return CAPTURE_COOLDOWN-(timenow-Points[point][EPointCaptureTime]);
	}
	return 0;
}
getNumDroppedVans() { //Gets the number of dropped vans
	new count;
	for(new i=0;i<sizeof(Points);i++) {
		if(getLastCTVCarID(i) > 0) { //It's obviously a van....
			count++;
		}
	}
	//printf("getNumDroppedVans: %d", count);
	return count;
}
checkCaptureTheVan() {
	for(new i=0;i<sizeof(Points);i++) {
		if(getPlayersAtXYZVW(30.0, Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ], Points[i][EPointDataVW]) < 1) { //Only drop the van if there isn't anybody at the point
			if(Points[i][EPointDataType] != EPointType_Meth) { //Don't even think about dropping the van inside the meth lab
				tryDropPointVan(i);
			}
		}
	}
	checkOldCTVCars();
	checkCapture();
	//updateForPlayers(); //Updates the checkpoint for the players, so they can find the van..
}
tryDropPointVan(pointid) {
	//printf("tryDropPointVan called with index %d", pointid);
	if(getNumDroppedVans() >= MAX_ALLOWED_VANS) {
		//printf("Couldn't drop van, it exceeds the maximum amount of vans dropped");
		return 1;
	}
	new rcaptime = remaingPointCaptureTime(pointid);
	rcaptime = (rcaptime/60)/60;
	//printf("rcaptime: %d", rcaptime);
	if(rcaptime != 0) {
		//printf("Couldn't drop van, it's not time yet");
		return 1;
	}
	if(getLastCTVCarID(pointid) == 0) { //Only drop another van if there isn't a van there..
		dropVanAtAvailablePoint(pointid); //This will only drop one van at the available point... (We don't want to throw like 4 vans)
	}
	return 1;
}
checkOldCTVCars() {
	for(new i=0;i<sizeof(Points);i++) {
		removeVanFromPointIfOld(i);
	}
}
removeVanFromPointIfOld(pointid) {
	new time = gettime();
	new carid = getLastCTVCarID(pointid);
	if(carid != 0) {
		if(time - VehicleInfo[carid][EVCreationTime] > MAX_CAPTURE_TIME) {
			DestroyCaptureCar(carid);
			onVehicleCaptureFailure(carid);
		}
	}
	return 1;
}
dropVanAtAvailablePoint(i) { //Code to drop the van goes inside here
	new carid = CreateCTVCar(498, Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ], 1000);
	sendVanMessageToPlayers(i);
	setLastCTVCarID(i, carid);
	setLastCTVRouteID(i, -1);
	//printf("Dropped a van, carid: %d", carid);
}
setLastCTVRouteID(carpointid, routeid) {
	Points[carpointid][EPointCTVDelivery] = routeid;
}
getLastCTVRouteID(carpointid) {
	if(carpointid != -1) {
		return Points[carpointid][EPointCTVDelivery];
	}
	return -1;
}
setLastCTVCarID(pointid, carid) {
	Points[pointid][EPointLastVehicleID] = carid;
}
getPointCarIDFromCarID(carid) {
	for(new i=0;i<sizeof(Points);i++) {
		if(getLastCTVCarID(i) == carid) {
			return getLastCTVCarID(i);
		}
	}
	return 0;
}
getCarPointOwnerFromCarID(carid) {
	for(new i=0;i<sizeof(Points);i++) {
		if(getLastCTVCarID(i) == carid) {
			return i;
		}
	}
	return 0;
}
getLastCTVCarID(pointid) {
	return Points[pointid][EPointLastVehicleID];
}
sendVanMessageToPlayers(pointinfo) {
	foreach(Player, i) {
		new family = FindFamilyBySQLID(GetPVarInt(i, "Family"));
		if(family != -1) {
			if(getFamilyType(family) != _:EFamilyType_None) {
				SendClientMessage(i, COLOR_LIGHTBLUE, "[INFO]: A van has become available for capture!");
				SetPlayerCheckpoint(i, Points[pointinfo][EPointDataX], Points[pointinfo][EPointDataY], Points[pointinfo][EPointDataZ], 5.0);
			} 
		} else if(IsAnLEO(i)) {
			SendClientMessage(i, TEAM_BLUE_COLOR, "[HQ]: A van has become available for capture!");
			SetPlayerCheckpoint(i, Points[pointinfo][EPointDataX], Points[pointinfo][EPointDataY], Points[pointinfo][EPointDataZ], 5.0);
		}
	}
	return 1;
}
initiateCaptureTheVehicle(playerid) {
	//Code goes inside here
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	if(family != -1) {
		if(getFamilyType(family) == _:EFamilyType_None) {
			SendClientMessage(playerid, X11_TOMATO_2, "Your family is not allowed to take the van since it's unofficial.");
			return 1;
		}
	}
	onInitiateCaptureTheVehicle(playerid);
	//SetTimerEx("onInitiateCaptureTheVehicle",2500,false,"d",playerid); //Wait 2.5 seconds so our blip doesn't get overridden..
	return 1;
}
ctvOnPlayerEnterVehicle(playerid, vehicleid, ispassenger) {
	#pragma unused ispassenger
	if(isCaptureCar(vehicleid)) {
		DisablePlayerCheckpoint(playerid); //Disable the checkpoint once we're about to get in..
	}
}
forward onInitiateCaptureTheVehicle(playerid);
public onInitiateCaptureTheVehicle(playerid) {
	printf("onInitiateCaptureTheVehicle was called");
	new carid = GetPlayerVehicleID(playerid);
	new carpointid = getCarPointOwnerFromCarID(carid);
	if(getLastCTVRouteID(carpointid) != -1) {
		setPlayerRouteID(playerid, carpointid);
		new actualrouteid = getLastCTVRouteID(carpointid);
		SetPlayerCheckpoint(playerid, Points[actualrouteid][EPointDataX], Points[actualrouteid][EPointDataY], Points[actualrouteid][EPointDataZ],10.0);
		return 1; //Don't do another route, the player already has a route or the vehicle already has one
	}
	new pointid = random(sizeof(Points));
	if(nearestPoint(playerid) == pointid || Points[pointid][EPointDataType] == EPointType_Meth) {
		//Try to get another route.. (Player's cannot go to the closest route, neither the Meth route)
		#if debug
		printf("ERROR: Got the same route");
		initiateCaptureTheVehicle(playerid);
		#endif
		return 1;
	}
	VehicleInfo[carid][EVOwner] = playerid;
	setPlayerRouteID(playerid, pointid);
	setLastCTVRouteID(carpointid, pointid);
	SetPlayerCheckpoint(playerid, Points[pointid][EPointDataX], Points[pointid][EPointDataY], Points[pointid][EPointDataZ],10.0);
	ShowScriptMessage(playerid, "Drive the vehicle to the destination, make sure you're not caught by the cops or that the van gets taken away!");
	return 1;
}
setPlayerRouteID(playerid, pointid) {
	SetPVarInt(playerid, "CTVRouteID", pointid);
}
getPlayerRouteID(playerid) {
	if(GetPVarType(playerid, "CTVRouteID") != PLAYER_VARTYPE_NONE) {
		return GetPVarInt(playerid, "CTVRouteID");
	}
	return -1;
}
onVehicleCaptureFailure(carid) {
	#pragma unused carid
	//Code should go inside here..
	foreach(Player, i) {
		new sqlid = GetPVarInt(i,"Family");
		if(sqlid > 0 || IsAnLEO(i)) {
			SendClientMessage(i, COLOR_LIGHTBLUE, "[INFO]: The vehicle has been lost and it couldn't be captured by anyone!");
		}
	}
	new pointid = getCarPointOwnerFromCarID(carid);
	setLastCTVCarID(pointid, 0); //Set it back to default
	VehicleInfo[carid][EVOwner] = 0; //Reset its owner
	ctvSyncEveryone(); //Sync Everyone... (Disables the checkpoint marker)
	Points[pointid][EPointCaptureTime] = gettime();
	setLastCTVRouteID(pointid, -1);
	timecount = 0;
	return 1;
}
onVehicleCaptureSuccess(playerid, pointid, carid) { //Make sure we're sending the last point id here
	#pragma unused carid
	//Code should go inside here..
	new string[128];
	new fid = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	format(string, sizeof(string), "† %s has successfully captured the cargo from %s.",GetFamilyName(fid),  Points[pointid][EPointDataName]);
	FamilyMessage(-1, COLOR_RED, string);
	rewardFamily(fid, pointid);
	setLastCTVCarID(pointid, 0); //Set it back to default
	VehicleInfo[carid][EVOwner] = 0; //Reset its owner
	DestroyCaptureCar(carid);
	ctvSyncEveryone();
	Points[pointid][EPointCaptureTime] = gettime();
	setLastCTVRouteID(pointid, -1);
	setPlayerRouteID(playerid, -1);
	timecount = 0;
	return 1;
}
ctvOnStateChange(playerid, vehicleid) { //Clear the ID if we exit the vehicle
	if(isCaptureCar(vehicleid)) {
		setPlayerRouteID(playerid, -1);
	}
	return 1;
}
checkCapture() {
	new string[128];
	foreach(Player, i) {
		if(GetPVarInt(i, "Family") != 0) {
			if(getPlayerRouteID(i) != -1) {
				if(IsPlayerInAnyVehicle(i)) {
					new carid = GetPlayerVehicleID(i);
					new oldpointid = getCarPointOwnerFromCarID(carid);
					if(isCaptureCar(carid)) {
						new actualpointid = nearestPoint(i);
						new famid = GetPVarInt(i, "Family");
						if(enemiesAreNear(actualpointid, famid)) {
							ShowScriptMessage(i, "You've to get rid of the enemies on your tail to be able to capture the van!", 5000);
							return 1;
						}
						if(getLastCTVRouteID(oldpointid) != actualpointid) { //We are not at the point yet
							return 1;
						}
						if(timecount >= LOADOUT_TIME) {
							onVehicleCaptureSuccess(i, oldpointid, carid);
						} else {
							timecount++;
							format(string, sizeof(string), "%d/%d seconds to go..", timecount, LOADOUT_TIME);
							ShowScriptMessage(i, string, 1000);
						}
					} else {
						new actualpointid = nearestPoint(i);
						new famid = GetPVarInt(i, "Family");
						if(!enemiesAreNear(actualpointid,famid)) {
							ShowScriptMessage(i, "Get back in the truck.", 5000);
						} else {
							ShowScriptMessage(i, "Get back in the truck after you're done with the enemies.", 5000);
						}
					}
				}
			}
		}
	}
	return 1;
}
/*
updateForPlayers() {
	new Float: X, Float: Y, Float: Z;
	new pointcarid, actualcarid;
	foreach(Player, i) {
		pointcarid = returnActiveCaptureCarID();
		actualcarid = GetPlayerVehicleID(i);
		if(GetPVarInt(i, "Family") == 0 && !IsAnLEO(i) || VehicleInfo[actualcarid][EVType] == EVehicleType_JobCar) {
			continue; //Just continue looping, skip this playerid, he is not in a family or he is in a job car doing a route
		}
		if(pointcarid != -1) {
			if(actualcarid != pointcarid && GetClosestVehicle(i) != pointcarid) {
				GetVehiclePos(pointcarid, X, Y, Z);
				SetPlayerCheckpoint(i, X, Y, Z,10.0);
			}
		}
	}
	return 1;
}
*/
forward updateForPlayer(playerid);
public updateForPlayer(playerid) {
	new Float: X, Float: Y, Float: Z;
	new pointcarid, actualcarid;
	pointcarid = returnActiveCaptureCarID();
	actualcarid = GetPlayerVehicleID(playerid);
	if(GetPVarInt(playerid, "Family") == 0 && !IsAnLEO(playerid) || VehicleInfo[actualcarid][EVType] == EVehicleType_JobCar) {
		return 1; //Don't do anything
	}
	if(pointcarid != -1) {
		if(actualcarid != pointcarid && GetClosestVehicle(playerid) != pointcarid) {
			GetVehiclePos(pointcarid, X, Y, Z);
			SetPlayerCheckpoint(playerid, X, Y, Z,10.0);
		}
	}
	return 1;
}
returnActiveCaptureCarID() {
	for(new i=0;i<sizeof(Points);i++) {
		if(Points[i][EPointSQLID] != 0) {
			new carid = getLastCTVCarID(i);
			if(carid > 0) { //If the last vehicle id is greater than 0 for that certain point
				return carid;
			}
		}
	}
	return -1;
}
ctvSyncEveryone() {
	foreach(Player, i) {
		if(IsAnLEO(i) || GetPVarInt(i,"Family") > 0) {
			DisablePlayerCheckpoint(i);
			if(GetPVarType(i, "CTVRouteID") != PLAYER_VARTYPE_NONE) {
				DeletePVar(i, "CTVRouteID");
			}
		}
	}
}
ctvOnPlayerEnterCheckpoint(playerid) {
	new carid = GetPlayerVehicleID(playerid);
	new oldpointid = getCarPointOwnerFromCarID(carid);
	new actualpointid = nearestPoint(playerid);
	if(getPlayerRouteID(playerid) == -1) {
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need to be in a vehicle in order to capture!");
		return 1;
	}
	if(getPointCarIDFromCarID(carid) != getLastCTVCarID(oldpointid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "That vehicle doesn't contain anything worthy!");
		return 1;
	}
	if(getLastCTVRouteID(oldpointid) != actualpointid) {
		SendClientMessage(playerid, X11_TOMATO_2, "You're not at the delivery point!");
		return 1;
	}
	return 1;
}
enemiesAreNear(point, sentfamid) {
	if(point != -1) {
		foreach(Player, i) {
			if(nearestPoint(i) == point) {
				if(GetPVarInt(i,"Family") != 0) {
					if(IsAnLEO(i) || sentfamid != GetPVarInt(i,"Family")) {
						return 1; //Enemies are near, return 1 and if it's a LEO, do so as well. LEO's are not allowed to capture
					}
				}
			}
		}
	}
	return 0;
}
rewardFamily(family, point) {
	new msg[128];
	new name[32];
	new reward, ESafeItemType:item;
	switch(Points[point][EPointDataType]) {
		case EPointType_MatsA: {
			format(name, sizeof(name), "Type A Materials");
			reward = RandomEx(500, 2500);
			item = ESafeItemType_MatsA;
		}
		case EPointType_MatsB: {
			format(name, sizeof(name), "Type B Materials");
			reward = RandomEx(500, 2500);
			item = ESafeItemType_MatsB;
		}
		case EPointType_MatsC: {
			format(name, sizeof(name), "Type C Materials");
			reward = RandomEx(500, 2500);
			item = ESafeItemType_MatsC;
		}
		case EPointType_Pot: {
			format(name, sizeof(name), "Pot");
			reward = RandomEx(10, 300);
			item = ESafeItemType_Pot;
		}
		case EPointType_Coke: {
			format(name, sizeof(name), "Coke");
			reward = RandomEx(10, 250);
			item = ESafeItemType_Coke;
		}
		case EPointType_Meth: {
			format(name, sizeof(name), "Meth");
			reward = RandomEx(10, 250);
			item = ESafeItemType_Meth;
		}
		case EPointType_Armoury: {
			format(name, sizeof(name), "Coke");
			reward = RandomEx(10, 250);
			item = ESafeItemType_Coke;
		}
	}
	format(msg, sizeof(msg), "The family got %s %s from the capture!", getNumberString(reward), name);
	FamilyMessage(family, X11_YELLOW, msg);
	addToItem(FamilySafes[family][ESafeSQLID], item, reward);
	return 1;
}
YCMD:points(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all points for a player");
		return 1;
	}
	dialogstr[0] = 0;
	tempstr[0] = 0;
	//new owner[MAX_FAMILY_NAME];
	for(new i=0;i<sizeof(Points);i++) {
		if(Points[i][EPointSQLID] != 0) {
			new rcaptime = remaingPointCaptureTime(i);
			new carid = getLastCTVCarID(i);
			if(carid > 0) { //If the last vehicle id is greater than 0 for that certain point
				if(!IsVehicleOccupied(carid)) { //It's not taken
					if(getLastCTVRouteID(i) != -1) { //It was taken but it was lost in the transition
						format(tempstr, sizeof(tempstr), "{%s}%d. %s: {FF0000}Cargo Lost!\n",getColourString(getPointColour(i,X11_WHITE)),i+1,Points[i][EPointDataName]);
					} else { //It's on location
						format(tempstr, sizeof(tempstr), "{%s}%d. %s: {FF0000}Cargo at Location!\n",getColourString(getPointColour(i,X11_WHITE)),i+1,Points[i][EPointDataName]);
					}
				} else { //Otherwise..
					format(tempstr, sizeof(tempstr), "{%s}%d. %s: {FF0000}Cargo Taken!\n",getColourString(getPointColour(i,X11_WHITE)),i+1,Points[i][EPointDataName]);
				}
			} else if(rcaptime != 0) {
				rcaptime = (rcaptime/60)/60;
				format(tempstr, sizeof(tempstr), "{%s}%d. %s Hours: %d\n",getColourString(getPointColour(i,X11_WHITE)),i+1,Points[i][EPointDataName],rcaptime);
			} else {
				format(tempstr, sizeof(tempstr), "{%s}%d. %s Ready for Capture\n",getColourString(getPointColour(i,X11_WHITE)),i+1,Points[i][EPointDataName]);
			}
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EPointDialog_ShowPoints, DIALOG_STYLE_MSGBOX, "{00BFFF}Point List", dialogstr, "Done", "");
	return 1;
}
getNearestPoint(playerid, deliver = -1) {
	new index = isNearPointType(playerid, EPointType_MatsA);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	index = isNearPointType(playerid, EPointType_MatsB);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	index = isNearPointType(playerid, EPointType_MatsC);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	index = isNearPointType(playerid, EPointType_Pot);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	index = isNearPointType(playerid, EPointType_Coke);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	index = isNearPointType(playerid, EPointType_Meth);
	if(index != -1 && (deliver == -1 || Points[index][EPointDataDeliver] == deliver)) {
		return index;
	}
	return -1;
}
getPointPVarName(index, dst[], len, pickup = 0) {
	switch(Points[index][EPointDataType]) {
		case EPointType_MatsA: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "MatAPacks", 0, 9, len);
			} else {
				strmid(dst, "MatsA", 0, 5, len);
			}
		}
		case EPointType_MatsB: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "MatBPacks", 0, 9, len);
			} else {
				strmid(dst, "MatsB", 0, 5, len);
			}
		}
		case EPointType_MatsC: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "MatCPacks", 0, 9, len);
			} else {
				strmid(dst, "MatsC", 0, 5, len);
			}
		}
		case EPointType_Pot: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "PotSeeds", 0, 8, len);
			} else {
				strmid(dst, "Pot", 0, 3, len);
			}
		}
		case EPointType_Coke: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "CokePlants", 0, 10, len);
			} else {
				strmid(dst, "Coke", 0, 4, len);
			}
		}
		case EPointType_Meth: {
			if(pickup || Points[index][EPointDataDeliver] != 1) {
				strmid(dst, "MethMaterials", 0, 13, len);
			} else {
				strmid(dst, "Meth", 0, 4, len);
			}
		}
		default: {
			dst[0] = 0;
		}
	}
}
getMatLetter(index) {
	switch(Points[index][EPointDataType]) {
		case EPointType_MatsA: {
			return 'A';
		}
		case EPointType_MatsB: {
			return 'B';
		}
		case EPointType_MatsC: {
			return 'C';
		}
	}
	return 0;
}
endVanSearch(playerid) {
	if(GetPVarType(playerid, "vansearchtimer") != PLAYER_VARTYPE_NONE) {
		new timer = GetPVarInt(playerid, "vansearchtimer");
		KillTimer(timer);
		DeletePVar(playerid, "vansearchtimer");
	}
}
/*
YCMD:drugs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "");
		return 1;
	}
	if(GetPVarInt(playerid, "Job") != EJobType_Drug_Dealer) {
		SendClientMessage(playerid, X11_RED3, "You are not a Drug Dealer");
		return 1;
	}
	new action[16],amount,price,index;
	new msg[128];
	if(!sscanf(params,"p< >s[16]D(-1)",action,amount)) {
		if((amount < 1 || amount > 99) && amount != -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		switch(tolower(action[0])) {
			case 'g': {
				index = getNearestPoint(playerid, 0);
				if(index == -1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not at the pickup point");
					return 1;
				}
				new name[32];
				if(Points[index][EPointDataType] == EPointType_Pot) {
					name = "Pot Seeds";
					price = 10;
				} else if(Points[index][EPointDataType] == EPointType_Coke) {
					name = "Coke Plants";
					price = 30;
				} else if(Points[index][EPointDataType] == EPointType_Meth) {
					name = "Meth Materials";
					price = 25;
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not at a drugs point");
					return 1;
				}
				new pvarname[32];
				getPointPVarName(index, pvarname, sizeof(pvarname),1);
				new maxdrugs = 7;
				new sindex = findJobSkillIndex(EJobType_Drug_Dealer);
				if(sindex != -1) {
					maxdrugs += (getSkillLevel(playerid, sindex)*2);
				}
				if(amount == -1) amount = maxdrugs;
				new drugs = GetPVarInt(playerid, pvarname);
				drugs += amount;
				if(drugs > maxdrugs) {
					SendClientMessage(playerid, X11_RED2, "You cannot carry this much");
					return 1;
				}
				price = floatround(amount * price);
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
					return 1;
				}
				if(Points[index][EPointDataType] == EPointType_Pot) {
					HintMessage(playerid, COLOR_CUSTOMGOLD, "Use /plantpot to plant the pot, and /harvest to get it later");
				}
				GiveMoneyEx(playerid, -price);
				SetPVarInt(playerid, pvarname, drugs);
				format(msg, sizeof(msg), "* You just bought %s %s",getNumberString(amount),name);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
			}
			case 'd': {
				index = getNearestPoint(playerid, 1);
				if(index == -1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not at the Deliver point");
					return 1;
				}
				new pvarname[32];
				getPointPVarName(index, pvarname, sizeof(pvarname),1);
				if(Points[index][EPointDataType] != EPointType_Pot && Points[index][EPointDataType] != EPointType_Coke && Points[index][EPointDataType] != EPointType_Meth) {
					SendClientMessage(playerid, X11_RED2, "You are not at a drug point");
					return 1;
				}
				new addamount = GetPVarInt(playerid, pvarname);
				if(addamount < 1) {
					SendClientMessage(playerid, X11_RED2, "You do not have any drug materials!");
					return 1;
				}
				getPointPVarName(index, pvarname, sizeof(pvarname));
				new ntotal = GetPVarInt(playerid, pvarname)+addamount;
				SetPVarInt(playerid, pvarname, ntotal);
				getPointPVarName(index, pvarname, sizeof(pvarname),1);
				SetPVarInt(playerid, pvarname, 0);
				new name[32];
				if(Points[index][EPointDataType] == EPointType_Pot) {
					name = "Pot";
				} else if(Points[index][EPointDataType] == EPointType_Coke) {
					name = "Coke";
				} else if(Points[index][EPointDataType] == EPointType_Meth) {
					name = "Meth";
				}
				format(msg, sizeof(msg), "* You now have %s %s", getNumberString(ntotal),name);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
				 
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /drugs [get/deliver] [amount]");
	}
	return 1;
}
*/
YCMD:mats(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Get/Deliver materials at points");
		return 1;
	}
	new action[16], amount;
	new msg[128];
	new index;
	if(GetPVarInt(playerid, "Job") != EJobType_Arms_Dealer && GetPVarInt(playerid, "Job") != EJobType_Crafter) {
		SendClientMessage(playerid, X11_RED3, "You are not a Arms Dealer, or a crafter");
		return 1;
	}
	if(!sscanf(params,"p< >s[16]D(-1)",action,amount)) {
		if((amount < 1 || amount > 99) && amount != -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		switch(tolower(action[0])) {
			case 'g': {
				index = getNearestPoint(playerid, 0);
				if(index == -1 || Points[index][EPointDataDeliver] != 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not at the pickup point");
					return 1;
				}
				new pvarname[6];
				getPointPVarName(index, pvarname, sizeof(pvarname));
				new price;
				if(Points[index][EPointDataType] == EPointType_MatsA) {
					price = 60;
				} else if(Points[index][EPointDataType] == EPointType_MatsB) {
					price = 70;
				} else if(Points[index][EPointDataType] == EPointType_MatsC) {
					price = 70;
				} else {
					SendClientMessage(playerid, X11_WHITE, "You are not at a materials point");
					return 1;
				}
				new total = GetPVarInt(playerid, pvarname);
				new maxmats = 7;
				new sindex = findJobSkillIndex(EJobType_Arms_Dealer);
				if(sindex != -1) {
					maxmats += getSkillLevel(playerid, sindex);
				}
				if(amount == -1) amount = maxmats;
				total += amount;
				if(total > maxmats) {
					SendClientMessage(playerid, X11_RED2, "You cannot hold this many materials");
					return 1;
				}
				price *= amount;
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
					return 1;
				}
				GiveMoneyEx(playerid, -price);
				SetPVarInt(playerid, pvarname, total);
				format(msg, sizeof(msg), "* You bought %s Mat %c Packs for $%s",getNumberString(amount),getMatLetter(index),getNumberString(price));
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
			}
			case 'd': {
				index = getNearestPoint(playerid, 1);
				if(index == -1 || Points[index][EPointDataDeliver] != 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not at the delivery point");
					return 1;
				}
				if(Points[index][EPointDataType] != EPointType_MatsA && Points[index][EPointDataType] != EPointType_MatsB && Points[index][EPointDataType] != EPointType_MatsC) {
					SendClientMessage(playerid, X11_WHITE, "You are not at a materials point");
					return 1;
				}
				new pvarname[6];
				getPointPVarName(index, pvarname, sizeof(pvarname),1);
				new packs = GetPVarInt(playerid, pvarname);
				if(packs == 0) {
					SendClientMessage(playerid, X11_WHITE, "You don't have any material packs");
					return 1;
				}
				new nummats = floatround(packs * 20);
				getPointPVarName(index, pvarname, sizeof(pvarname));
				packs = GetPVarInt(playerid, pvarname); //Gets the packs
				nummats += packs; //number of materials
				SetPVarInt(playerid, pvarname, nummats); //Sets the number of materials to what's done above
				//
				getPointPVarName(index, pvarname, sizeof(pvarname),1); //Gets the point pvarname for the packs
				SetPVarInt(playerid, pvarname, 0); //Sets the packs to 0
				format(msg, sizeof(msg), "* You now have %s materials!",getNumberString(nummats));
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
			}
			default: {
				SendClientMessage(playerid, X11_WHITE, "USAGE: /mats [get/deliver] [amount]");
				return 1;
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /mats [get/deliver] [amount]");
	}
	return 1;
}
YCMD:pickupspots(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Find a pickups location");
		return 1;
	}
	new index;
	if(!sscanf(params,"d",index)) {
		if(index < 0 || index > sizeof(Points)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Point ID");
			return 1;
		}
		index--;
		SetPlayerCheckpoint(playerid, Points[index][EPointDataX], Points[index][EPointDataY], Points[index][EPointDataZ], 3.0);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "The point is now marked on your radar");
		if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHints) {
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /pickupspots [pointid]");
	}
	return 1;
}
YCMD:findvan(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to find the van if you're in a faction.");
		return 1;
	}
	new actualcarid = GetPlayerVehicleID(playerid);
	if(GetPVarInt(playerid, "Family") == 0 && !IsAnLEO(playerid)) {
		if(VehicleInfo[actualcarid][EVType] == EVehicleType_JobCar) {
			SendClientMessage(playerid, X11_TOMATO_2, "You're currently inside a job car so you can't do this right now.");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This is meant to be used by faction members only.");
		}
		return 1;
	}
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	if(getFamilyType(family) == _:EFamilyType_None) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family cannot find the van since it's unofficial.");
		return 1;
	}
	if(GetPVarType(playerid, "vansearchtimer") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_WHITE, "You've stopped searching for the van, checkpoint removed...");
		DisablePlayerCheckpoint(playerid);
		endVanSearch(playerid);
	} else {
		new vansearchtimer = SetTimerEx("updateForPlayer", 1000, true,"d",playerid);
		SetPVarInt(playerid, "vansearchtimer", vansearchtimer);
		SendClientMessage(playerid, X11_WHITE, "You're now searching for the van, type '/findvan' again to stop searching.");
	}
	return 1;
}
YCMD:gotopickup(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a point");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	new index;
	if(!sscanf(params,"d",index)) {
		index--;
		if(index < 0 || index > sizeof(Points)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Point ID");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotopickup [pointid]");
		return 1;
	}
	X = Points[index][EPointDataX];
	Y = Points[index][EPointDataY];
	Z = Points[index][EPointDataZ];
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		TPEntireCar(carid, Points[index][EPointDataInt], Points[index][EPointDataVW]);
		LinkVehicleToInterior(carid, Points[index][EPointDataInt]);
		SetVehicleVirtualWorld(carid, Points[index][EPointDataVW]);
		SetVehiclePos(carid, X, Y, Z);
		
	} else {
		SetPlayerPos(playerid, X, Y, Z);
	}
	SetPlayerVirtualWorld(playerid, Points[index][EPointDataVW]);
	SetPlayerInterior(playerid, Points[index][EPointDataInt]);
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}
public onLoadPoints() {
	new rows, fields;
	cache_get_data(rows,fields);
	new id_string[32];
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,id_string);
		Points[i][EPointSQLID] = strval(id_string);
		cache_get_row(i,1,Points[i][EPointDataName]);
		cache_get_row(i,2,id_string);
		Points[i][EPointDataX] = floatstr(id_string);
		cache_get_row(i,3,id_string);
		Points[i][EPointDataY] = floatstr(id_string);
		cache_get_row(i,4,id_string);
		Points[i][EPointDataZ] = floatstr(id_string);
		cache_get_row(i,5,id_string);
		Points[i][EPointDataInt] = strval(id_string);
		cache_get_row(i,6,id_string);
		Points[i][EPointDataVW] = strval(id_string);
		cache_get_row(i,7,id_string);
		Points[i][EPointDataType] = EPointType:strval(id_string);
		cache_get_row(i,8,id_string);
		Points[i][EPointDataDeliver] = strval(id_string);
		cache_get_row(i,9,id_string);
		Points[i][EPointDataIconID] = strval(id_string);
		cache_get_row(i,10,id_string);
		Points[i][EPointPickupModel] = strval(id_string);
		cache_get_row(i,11,id_string);
		Points[i][EPointOwnerFamily] = FindFamilyBySQLID(strval(id_string));
		cache_get_row(i,12,id_string);
		Points[i][EPointCaptureTime] = strval(id_string);
		Points[i][EPointPickupID] = CreateDynamicPickup(Points[i][EPointPickupModel], 16, Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ],Points[i][EPointDataVW],Points[i][EPointDataInt]);
		//point tip
		new pointlabel[128];
		format(pointlabel, sizeof(pointlabel), "%s {FFFFFF}(( {FF0000} %s {FFFFFF} ))", Points[i][EPointDataName], givePlayerPointTip(i));
		Points[i][EPointLabel] = CreateDynamic3DTextLabel(pointlabel, getPointColour(i), Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ]+1.0, 10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, Points[i][EPointDataVW],Points[i][EPointDataInt]);
		if(Points[i][EPointDataIconID] != -1) {
			new mapid = allocMapID();		
			Points[i][EPointMapSlotID] = mapid;
		}
		setLastCTVCarID(i, 0); //Set the last car to 0 by default
	}
}
pointsOnGameModeInit() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`,`name`,`x`,`y`,`z`,`interior`,`virtualworld`,`type`,`deliver`,`iconid`,`model`,`owner`,Unix_Timestamp(`capturetime`) FROM `points`");
	mysql_function_query(g_mysql_handle, query, true, "onLoadPoints", "");
	pointwarpoint = -1;
}
pointsOnPlayerConnect(playerid) {
	loadPlayerPoints(playerid);
}
loadPlayerPoints(playerid) {
	for(new i=0;i<sizeof(Points);i++) {
		if(Points[i][EPointDataIconID] != -1 && Points[i][EPointSQLID] != 0) {
			SetPlayerMapIcon(playerid, Points[i][EPointMapSlotID], Points[i][EPointDataX], Points[i][EPointDataY], Points[i][EPointDataZ],Points[i][EPointDataIconID],0, MAPICON_LOCAL);
		}
	}
}
materialsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	endVanSearch(playerid);
}