
//only one special item can be held at a time, for two reasons, RP and cleaner code

//TODO: radio distortion effect

enum ESpecialItemInfo {
	ESpecialItemName[32],
	ESpecialItemShortName[32],
	ESpecialItemObjectID,
	ESpecialItemBone,
	Float:ESpecialItemX,
	Float:ESpecialItemY,
	Float:ESpecialItemZ,
	Float:ESpecialItemRotX,
	Float:ESpecialItemRotY,
	Float:ESpecialItemRotZ,
	Float:ESpecialItemScaleX,
	Float:ESpecialItemScaleY,
	Float:ESpecialItemScaleZ,
	ESpecialItemMatsAPrice,
	ESpecialItemMatsBPrice,
	ESpecialItemMatsCPrice,
	ESpecialItemRequiredLevel, //level to craft
	ESpecialItemHint[64],
	ESpecialItemID,
	ESpecialItemMaxLevel,
	
};
#define SPECIAL_ITEM_SLOT 7
#define RADIO_SNIFF_DIST 1000.0
#define RADIO_JAMMER_DIST 250.0
#define CELLPHONE_JAMMER_DIST 50.0
#define CELLPHONE_SNIFF_DIST 100.0
#define ILLEGAL_BROADCASTER_DIST 2500.0
#define CAMERA_DISABLE_DIST 150.0

new SpecialItems[][ESpecialItemInfo] = {
	{"Radio Tapper","rtapper", 19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 600,0,0,1, "", 0, 5}, //0
	{"Radio Jammer", "rjammer",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 900,0,0,1, "", 1, 5}, //1
	{"Camera Disabler", "cdisable",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 1500,0,0,2,"", 2, 5}, //2
	{"Cell Phone Jammer", "cjammer",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 0,2500,0,2,"", 3, 5}, //3
	{"Cell Phone Tapper", "ctapper",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 500,250,0,3, "", 4, 5}, //4
	{"Illegal Broadcaster", "ibroadcast",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 5000,2000,0,5, "Use /ibroadcast to use this item", 5, 5}, //5
	{"Megaphone", "megaphone",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 1250,100,0,4, "Use /megaphone to use this item.", 6, 5}, //6
	{"Gate Hacker", "gatehacker",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 1000,500,3500,5, "Use /gate to open a gate.", 7, 5}, //7
	{"Call Tracer", "ctracer",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 3500,250,0,3, "Use /tracecall to trace a call.", 8, 5} //8
	//{"Engine Totaller", "etotal",19513, BONE_RHAND,0.109999, 0.022999, -0.004999, -84.300018, -7.299924, 173.199920,0.891999, 1.000000, 1.168000, 2000,0,0,4,  "Use /breakcar to use this item."}, //9
};

stock getSpecialItemName(itemid) {
	new name[32];
	format(name, sizeof(name), "%s", SpecialItems[itemid][ESpecialItemName]);
	return name;
}

specialItemsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	new item = getPlayerCarryingItem(playerid);
	if(item != -1) {
		takeoutItem(playerid, -1);
	}
}

getItemIDByName(string[]){
	for(new i=0;i<sizeof(SpecialItems);i++) {
		if(!strcmp(SpecialItems[i][ESpecialItemShortName],string,true)) {
			return SpecialItems[i][ESpecialItemID];
		}
	}
	return -1;
}


canUseCraftCommand(playerid) {
	new lastcmd = GetPVarInt(playerid, "LastCraftCommand");
	new coolout = getGunCooldownTime(playerid);
	new time = gettime();
	if(lastcmd+coolout > time) {
		return lastcmd+coolout-time;
	}
	return 0;
}
getCraftCooldownTime(playerid) {
	new coolout =  15;
	new level = getJobLevel(playerid);
	switch(level) {
		case 1:  {
			coolout += 165;
		}
		case 2: {
			coolout += 105;
		}
		case 3: {
			coolout += 95;
		}
		case 4: {
			coolout += 55;
		}
	}
	return coolout;
}
HasSpecialItem(playerid) {
	return GetPVarInt(playerid, "SpecialItem") != -1;
}

stock GetPlayerCarryingItemName(playerid) {
	new item = GetPVarInt(playerid, "SpecialItem");
	new name[32];
	format(name, sizeof(name), "None");
	if(item < 0 || item > sizeof(SpecialItems)) return name;
	format(name, sizeof(name), "%s", SpecialItems[item][ESpecialItemName]);
	return name;
}
stock GetCarryingItemName(item) {
	new name[32];
	format(name, sizeof(name), "None");
	if(item < 0 || item > sizeof(SpecialItems)) return name;
	format(name, sizeof(name), "%s", SpecialItems[item][ESpecialItemName]);
	return name;
	
}
getPlayerCarryingItem(playerid) {
	new item = GetPVarInt(playerid, "SpecialItem");
	return GetPVarInt(playerid, "HoldingSpecialItem") == 0 ? -1 : item;
	
}
CanAffordSpecialItem(playerid, item) {
	new matsa = GetPVarInt(playerid, "MatsA");
	new matsb = GetPVarInt(playerid, "MatsB");
	new matsc = GetPVarInt(playerid, "MatsC");
	new clevel = getJobLevel(playerid);
	if(matsa < SpecialItems[item][ESpecialItemMatsAPrice]) return 0;
	if(matsb < SpecialItems[item][ESpecialItemMatsBPrice]) return 0;
	if(matsc < SpecialItems[item][ESpecialItemMatsCPrice]) return 0;
	if(clevel < SpecialItems[item][ESpecialItemRequiredLevel]) return 0;
	return 1;
}
canIncreaseFromItem(playerid,item) {
	new clevel = getJobLevel(playerid);

	return clevel <= SpecialItems[item][ESpecialItemMaxLevel];
}
ChargeSpecialItem(playerid, item) {
	new matsa = GetPVarInt(playerid, "MatsA");
	new matsb = GetPVarInt(playerid, "MatsB");
	new matsc = GetPVarInt(playerid, "MatsC");
	
	matsa -= SpecialItems[item][ESpecialItemMatsAPrice];
	matsb -= SpecialItems[item][ESpecialItemMatsBPrice];
	matsc -= SpecialItems[item][ESpecialItemMatsCPrice];

	if(canIncreaseFromItem(playerid, item)) {
		increaseJobPoints(playerid);
	}	
	
	SetPVarInt(playerid, "MatsA", matsa);
	SetPVarInt(playerid, "MatsB", matsb);
	SetPVarInt(playerid, "MatsC", matsc);
}
YCMD:craftitem(playerid, params[], help) {
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Crafter) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a crafter for this!");
		return 1;
	}
	new msg[128];
	new time = canUseCraftCommand(playerid);
	if(time != 0) {
		format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(getPlayerCarryingItem(playerid) != -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are already holding an item");
		return 1;
	}
	new item;
	if(!sscanf(params, "k<specialItemLookup>", item)) {
		if(item == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			goto list_items;
			return 1;
		}
		if(!CanAffordSpecialItem(playerid, item)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough materials/skill to make this item.");
			return 1;
		}
		
		ChargeSpecialItem(playerid, item);
		GivePlayerItem(playerid, item);
		SetPVarInt(playerid, "LastCraftCommand", gettime());
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /craftitem [itemname]");
		list_items:
		SendClientMessage(playerid, X11_WHITE, "*** Item Names ***");
		for(new i=0;i<sizeof(SpecialItems);i++) {
			format(msg, sizeof(msg), "* %s: %s MatsA:[%s] MatsB:[%s] MatsC:[%s] Level:[%s]",SpecialItems[i][ESpecialItemName],SpecialItems[i][ESpecialItemShortName],getNumberString(SpecialItems[i][ESpecialItemMatsAPrice]),getNumberString(SpecialItems[i][ESpecialItemMatsBPrice]),getNumberString(SpecialItems[i][ESpecialItemMatsCPrice]),getNumberString(SpecialItems[i][ESpecialItemRequiredLevel]));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	}
	return 1;
}
YCMD:itemids(playerid, params[], help) {
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "*** Special Item IDs ***");
	for(new i=0;i<sizeof(SpecialItems);i++) {
		format(msg, sizeof(msg), "* %s: %d",SpecialItems[i][ESpecialItemName],i);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}
YCMD:destroyitem(playerid, params[], help) {
	if(getPlayerCarryingItem(playerid) != -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not holding an item!");
		return 1;
	}
	SendClientMessage(playerid, COLOR_DARKGREEN, "Item Destroyed!");
	RemovePlayerItem(playerid);	
	return 1;
}
YCMD:agiveitem(playerid, params[], help) {
	new target,item,msg[128];
	if(!sscanf(params, "k<playerLookup_acc>k<specialItemLookup>", target, item)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(item < 0 || item > sizeof(SpecialItems)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			return 1;
		}
		if(getPlayerCarryingItem(target) != -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player is already holding an item!");
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s has given %s a %s", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_CharName), SpecialItems[item][ESpecialItemName]);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		format(msg, sizeof(msg), "* You have given %s a %s", GetPlayerNameEx(target, ENameType_CharName), SpecialItems[item][ESpecialItemName]);
		SendClientMessage(playerid, X11_YELLOW, msg);
		format(msg, sizeof(msg), "* You have been given a %s", SpecialItems[item][ESpecialItemName]);
		SendClientMessage(target, X11_YELLOW, msg);
		GivePlayerItem(target, item);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /agiveitem [playerid/name] [itemid]");
		SendClientMessage(playerid, X11_WHITE, "* Use /itemids to look up an items id");
	}
	return 1;
}
YCMD:atakeitem(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes an item away from a player");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarInt(playerid, "SpecialItem") == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player isn't holding an item");
			return 1;
		}
		RemovePlayerItem(target);
		SendClientMessage(playerid, X11_YELLOW, "Item Removed");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /atakeitem [playerid/name]");
	}
	return 1;
}
YCMD:giveitem(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a special item to someone");
		return 1;
	}
	new target;
	if(!sscanf(params,"k<playerLookup>",target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(playerid, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(target, 3.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		new item = getPlayerCarryingItem(playerid);
		
		if(item == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be holding an item!");
			return 1;
		}
		if(getPlayerCarryingItem(target) != -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player is already holding an item!");
			return 1;
		}
		RemovePlayerItem(playerid);
		GivePlayerItem(target, item);
		new msg[128];
		format(msg, sizeof(msg), "* %s hands a %s to %s",GetPlayerNameEx(playerid, ENameType_RPName),GetPlayerNameEx(target, ENameType_RPName),SpecialItems[item][ESpecialItemName]);
		ProxMessage(20.0, playerid, msg,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /giveitem [playerid/name]");
	}
	return 1;
}
GivePlayerItem(playerid, item) {
	if(strlen(SpecialItems[item][ESpecialItemHint]) < 1) {
		HintMessage(playerid, COLOR_DARKGREEN, "You can use /giveitem to give it to someone, and /holditem to take it out and use it");
	} else {
		HintMessage(playerid, COLOR_DARKGREEN, SpecialItems[item][ESpecialItemHint]);
	}
	SetPVarInt(playerid, "SpecialItem", item);
}
RemovePlayerItem(playerid) {
	new item = getPlayerCarryingItem(playerid);
	if(item != -1) {
		takeoutItem(playerid, -1);
	}
	SetPVarInt(playerid, "SpecialItem", -1);
}
takeoutItem(playerid, item) {
	new msg[128];
	if(item != -1) {
		SetPVarInt(playerid, "HoldingSpecialItem", 1);
		SetPlayerAttachedObject(playerid, SPECIAL_ITEM_SLOT, SpecialItems[item][ESpecialItemObjectID], SpecialItems[item][ESpecialItemBone], 
		SpecialItems[item][ESpecialItemX], SpecialItems[item][ESpecialItemY], SpecialItems[item][ESpecialItemZ],SpecialItems[item][ESpecialItemRotX], SpecialItems[item][ESpecialItemRotY], SpecialItems[item][ESpecialItemRotZ], SpecialItems[item][ESpecialItemScaleX], SpecialItems[item][ESpecialItemScaleY], SpecialItems[item][ESpecialItemScaleZ]);
		format(msg, sizeof(msg), "You have taken a %s out.", SpecialItems[item][ESpecialItemName]);
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	}
	else {
		SetPVarInt(playerid, "HoldingSpecialItem", 0);		
		RemovePlayerAttachedObject(playerid, SPECIAL_ITEM_SLOT);
		SendClientMessage(playerid, COLOR_DARKGREEN, "You have put your item away.");
	}
}
YCMD:holditem(playerid, params[], help) {
	new item = getPlayerCarryingItem(playerid);
	new holdingitem = GetPVarInt(playerid, "SpecialItem");
	if(holdingitem == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have an item");
		return 1;
	}
	if(item != -1) {
		takeoutItem(playerid, -1);
		
	} else {
		takeoutItem(playerid, holdingitem);
	}
	return 1;
}

IsSenderInRadioDistance(sender, target) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(sender, X, Y, Z);
	new Float:range = getPlayerCarryingItem(target)==5?ILLEGAL_BROADCASTER_DIST:CELLPHONE_SNIFF_DIST;
	return IsPlayerInRangeOfPoint(target, range, X, Y, Z);
}
SendRadioMsgToListeners(sender, msg[]) {
	new smsg[128];
	new fid = GetPVarInt(sender, "Faction");
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 0 || getPlayerCarryingItem(i) == 5) {
			if(IsSenderInRadioDistance(sender,i)) {
				format(smsg, sizeof(smsg), "** (Radio Sniffer) %s: [%s]: %s", GetPlayerNameEx(i, ENameType_RPName), GetFactionName(fid), msg);
				ProxMessage(10.0, i, smsg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
SendRawMsgToRadioSnoopers(sender, msg[]) {
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 0 || getPlayerCarryingItem(i) == 5) {
			if(IsSenderInRadioDistance(sender,i)) {
				ProxMessage(10.0, i, msg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
SendMsgToRadioSnoopers(sender, msg[]) {
	new smsg[128];
	new fid = GetPVarInt(sender, "Faction");
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 0 || getPlayerCarryingItem(i) == 5) {
			if(IsSenderInRadioDistance(sender,i)) {
				format(smsg, sizeof(smsg), "** (Radio Sniffer) %s: [%s]: %s", GetPlayerNameEx(i, ENameType_RPName), GetFactionName(fid), msg);
				ProxMessage(10.0, i, smsg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
SendDepartmentMsgToListeners(sender, msg[]) {
	new smsg[128];
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 0 || getPlayerCarryingItem(i) == 5) {
			if(IsSenderInRadioDistance(sender,i)) {
				format(smsg, sizeof(smsg), "** (Radio Sniffer) %s: [Department]: %s", GetPlayerNameEx(i, ENameType_RPName), msg);
				ProxMessage(10.0, i, smsg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
SendWTMsgToListeners(sender, msg[], channel) {
	new smsg[128];
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 0 || getPlayerCarryingItem(i) == 5) {
			if(IsSenderInRadioDistance(sender,i)) {
				format(smsg, sizeof(smsg), "** (Radio Sniffer) %s: [WTChan:%d]: %s", GetPlayerNameEx(i, ENameType_RPName),channel,msg);
				ProxMessage(10.0, i, smsg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
IsRadioJammerActiveInArea(playerid) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {			
			if(getPlayerCarryingItem(i) == 1) {
				if(IsPlayerInRangeOfPoint(i, RADIO_JAMMER_DIST, X, Y, Z)) return 1;
			}
		}
	}
	return 0;
}
IsInCellPhoneJammerArea(playerid) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {			
			if(getPlayerCarryingItem(i) == 3) {
				if(IsPlayerInRangeOfPoint(i, CELLPHONE_JAMMER_DIST, X, Y, Z)) return 1;
			}
		}
	}
	return 0;
}
SendCellPhoneToListeners(caller, target, msg[]) {
	new Float:X, Float:Y, Float:Z, Float:X2, Float:Y2, Float:Z2;
	GetPlayerPos(caller, X, Y, Z);
	GetPlayerPos(target, X2, Y2, Z2);
	new smsg[128];
	foreach(Player, i) {
		if(getPlayerCarryingItem(i) == 4 || getPlayerCarryingItem(i) == 5) {
			new Float:range = getPlayerCarryingItem(i)==5?ILLEGAL_BROADCASTER_DIST:CELLPHONE_SNIFF_DIST;
			if(IsPlayerInRangeOfPoint(i, range, X, Y, Z) || IsPlayerInRangeOfPoint(i, range, X2, Y2, Z2)) {
				format(smsg, sizeof(smsg), "** (Phone Sniffer) %s: [%d->%d]: %s", GetPlayerNameEx(i, ENameType_RPName),GetPVarInt(caller,"PhoneNumber"),target==INVALID_PLAYER_ID?911:GetPVarInt(target,"PhoneNumber"),msg);
				ProxMessage(10.0, i, smsg, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
IsPlayerHoldingMegaphone(playerid) {
	return getPlayerCarryingItem(playerid) == 6;
}
IsPlayerHoldingGateHacker(playerid) {
	return getPlayerCarryingItem(playerid) == 7;
}
IsInCameraDisablerArea(Float:X, Float:Y, Float:Z) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {			
			if(getPlayerCarryingItem(i) == 2) {
				if(IsPlayerInRangeOfPoint(i,CAMERA_DISABLE_DIST, X, Y, Z)) return 1;
			}
		}
	}
	return 0;
}
YCMD:ibroadcast(playerid, params[], help) {
	new broadcasttype[32];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Use your illegal broadcaster");
		return 1;
	}
	if(getPlayerCarryingItem(playerid) != 5) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have an illegal broadcaster!");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	new faction =0;
	new msg[120],smsg[128];
	if(!sscanf(params,"s[32]s[120]",broadcasttype,msg)) {
		if(!strcmp(broadcasttype,"LSPD",true)) {
			faction = 1;
		} else if(!strcmp(broadcasttype,"FBI",true)) {
			faction = 2;
		} else if(!strcmp(broadcasttype,"GOV",true)) {
			faction = 3;
		} else if(!strcmp(broadcasttype,"NEWS",true)) {
			faction = 4;
		} else if(!strcmp(broadcasttype,"HITMAN",true)) {
			faction = 5;
		} else if(!strcmp(broadcasttype,"EMS",true)) {
			faction = 6;
		} else if(!strcmp(broadcasttype,"SADPS",true)) {
			faction = 7;
		} else if(!strcmp(broadcasttype,"LSCTP",true)) {
			faction = 8;
		}  else if(!strcmp(broadcasttype,"DEPT",true)) {
			faction = -1;
		}   else if(!strcmp(broadcasttype,"CELL",true)) {
			faction = -2;
		} else if(IsNumeric(broadcasttype)) {
			faction = strval(broadcasttype);
			if(faction > 0 && faction < 9999) {
				SendWTMsgToListeners(playerid, msg, faction);
				format(smsg, sizeof(smsg), "* WalkiTalki: (Illegal Broadcaster): %s",msg);
				SendLocalWTMsg(faction, X,Y,Z, COLOR_MEDIUMAQUA, smsg);
				format(msg, sizeof(msg), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
				ProxMessage(20.0, playerid, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
				return 1;
			}
		}
		if(faction > 0) {
			format(smsg, sizeof(smsg), "** (Radio Sniffer) (Illegal Broadcaster): [%s]: %s", broadcasttype,msg);
			SendRawMsgToRadioSnoopers(playerid, smsg);
			format(smsg, sizeof(smsg), "** (Radio) (Illegal Broadcaster): %s **",msg);
			SendLocalFactionRadioMsg(faction, X,Y,Z,TEAM_BLUE_COLOR, smsg);
			format(msg, sizeof(msg), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
			ProxMessage(20.0, playerid, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			return 1;
		} else if(faction == -1) {		
			format(smsg, sizeof(smsg), "** (Radio Sniffer) (Illegal Broadcaster): [Department]: %s", msg);
			SendRawMsgToRadioSnoopers(playerid, smsg);
			format(smsg, sizeof(smsg), "** (Illegal Broadcaster): %s",msg);
			SendLocalDeptMsg(X,Y,Z, COLOR_ALLDEPT, smsg);
			format(msg, sizeof(msg), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
			ProxMessage(20.0, playerid, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			return 1;
		}  else if(faction == -2) {		
			format(smsg, sizeof(smsg), "** (Illegal Broadcaster): %s",msg);
			SendLocalCellMsg(X, Y, Z, COLOR_FADE1, smsg);
			format(msg, sizeof(msg), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
			ProxMessage(20.0, playerid, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			return 1;
		}
		//shouldn't get here unless invalid data
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Broadcast type");
		return 1;
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ibroadcast [type] [msg]");
		SendClientMessage(playerid, X11_WHITE, "Valid Types: LSPD, FBI, EMS, NEWS, SADPS, GOV, LSCTP, DEPT, CELL");
		SendClientMessage(playerid, X11_WHITE, "Type the walkie talkie channel to broadcast on a walkie talkie frequency");
	}
	return 1;
}
SendLocalCellMsg(Float:X, Float:Y, Float:Z, color, msg[]) {
	foreach(Player, i) {
		if(GetPVarType(i, "OnCall") != PLAYER_VARTYPE_NONE) {
			if(IsPlayerInRangeOfPoint(i, ILLEGAL_BROADCASTER_DIST, X, Y, Z)) {
				SendClientMessage(i, color, msg);
				ProxMessage(10.0, i, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
			}
		}
	}
}
SendLocalFactionRadioMsg(faction, Float:X, Float:Y, Float:Z, color, msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "Faction") == faction) {
			if(IsPlayerInRangeOfPoint(i, ILLEGAL_BROADCASTER_DIST, X, Y, Z)) {
				SendClientMessage(i, color, msg);
			}
		}
	}
	SendBigFactionEarsMessage(color, msg);
}
SendLocalDeptMsg(Float:X, Float:Y, Float:Z, color, msg[]) {
	foreach(Player, i) {
		if(IsAnLEO(i) || isGovernment(i) || isMedic(i)) {
			if(IsPlayerInRangeOfPoint(i, ILLEGAL_BROADCASTER_DIST, X, Y, Z)) {
				SendClientMessage(i, color, msg);
			}
		}
	}
	SendBigFactionEarsMessage(color, msg);
}
SendLocalWTMsg(channel, Float:X, Float:Y, Float:Z, color, msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "WTChannel") == channel) {
			if(IsPlayerInRangeOfPoint(i, ILLEGAL_BROADCASTER_DIST, X, Y, Z)) {
				SendClientMessage(i, color, msg);
			}
		}
	}
	
}

IsHoldingCallTracer(playerid) { 
	//we don't care if you are holding it
	return GetPVarInt(playerid, "SpecialItem") == 8;
}
YCMD:tracecall(playerid, params[], help) {
	if(GetPVarType(playerid, "OnCall") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be on a call");
		return 1;
	}
	if(!IsHoldingCallTracer(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have a call tracer!");
		return 1;
	}
	SetTimerEx("TraceCall", 60000, false, "d", playerid);
	SendClientMessage(playerid, X11_ORANGE, "The call is being traced. Please stay on the line for 1 minute");
	return 1;
}
forward TraceCall(playerid);
public TraceCall(playerid) {
	new msg[128];
	new caller = GetPVarInt(playerid, "OnCall");
	if(GetPVarType(playerid, "OnCall") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "The call ended before it could be traced.");
		return 1;
	}
	format(msg, sizeof(msg), "* Caller: %s[%d]",GetPlayerNameEx(caller, ENameType_RPName_NoMask), GetPVarInt(caller, "PhoneNumber"));
	SendClientMessage(playerid, X11_YELLOW, msg);
	if(GetPlayerVirtualWorld(caller) != 0 && GetPlayerInterior(caller) != 0) {
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(playerid, X, Y, Z);
		SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
		SendClientMessage(playerid, X11_YELLOW, "* A checkpoint has been marked on your radar");
		return 1;
	}
	return 1;
}