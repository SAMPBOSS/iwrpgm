enum EPayPhoneDynamicObj {
	NOT_DYNAMIC_OBJECT,
	DYNAMIC_OBJECT,
}
enum EPayPhoneInfo {
	EPayPhoneModelID,
	EPayPhoneDynamicObj:EPhoneType,
	Float:EPayPhoneX,
	Float:EPayPhoneY,
	Float:EPayPhoneZ,
	Float:EPayPhoneRotX,
	Float:EPayPhoneRotY,
	Float:EPayPhoneRotZ,
	EPayPhoneVW,
	EPayPhoneInt,
	EPayPhoneCost,
	EPayPhoneObjectID,
	Text3D:EPayPhoneText,
};
//Payphone obj id's 1216 - 1363
new PayPhones[][EPayPhoneInfo] = {
	{1216, NOT_DYNAMIC_OBJECT, 2069.4067, -1767.8309, 13.5622, 0.0, 0.0, 90.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 2362.9197, -1310.4407, 23.9983, 0.0, 0.0, 0.0, 0, 0, 5, 0},
	{1216, DYNAMIC_OBJECT, 393.8633,-2054.4646,7.8359, 0.0, 0.0, 270.0, 0, 0, 5, 0},
	{1216, DYNAMIC_OBJECT, 1185.7496,-1257.7440,15.1797, 0.0, 0.0, 270.0, 0, 0, 5, 0},
	{1216, DYNAMIC_OBJECT, 1558.7151,-1201.2233,19.9687, 0.0, 0.0, 355.66, 0, 0, 5, 0},
	{1216, DYNAMIC_OBJECT, 1607.2781,-1200.0945,19.7872, 0.0, 0.0, 1.62, 0, 0, 4, 0},
	{1216, DYNAMIC_OBJECT, 1675.3873,-1201.0298,19.8495, 0.0, 0.0, 360.0, 0, 0, 4, 0},
	{1216, DYNAMIC_OBJECT, 2232.8052,-1341.8912,23.9837, 0.0, 0.0, 270.0, 0, 0, 4, 0},
	{1216, DYNAMIC_OBJECT, 2054.4885,-1897.6880,13.5538, 0.0, 0.0, 360.0, 0, 0, 3, 0},
	{1216, DYNAMIC_OBJECT, 2131.3225,-1941.7701,13.5525, 0.0, 0.0, 180.0, 0, 0, 3, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1722.1556,-1720.8187,14.5000, 0.0, 0.0, 183.0, 0, 0, 3, 0},
	{1216, DYNAMIC_OBJECT, 1083.0184,-1383.5022,13.7813, 0.0, 0.0, 90.0, 0, 0, 2, 0},
	{1216, DYNAMIC_OBJECT, 814.8032,-1356.4996,13.5438, 0.0, 0.0, 178.0, 0, 0, 2, 0},
	{1216, DYNAMIC_OBJECT, 1416.5037,-1486.8480,20.4342, 0.0, 0.0, 358.0, 0, 0, 2, 0},
	{1216, DYNAMIC_OBJECT, 1701.5979,-1911.3341,13.5698, 0.0, 0.0, 90.0, 0, 0, 2, 0},
	{1216, NOT_DYNAMIC_OBJECT, 302.6993,-1592.7974,32.8336, 0.0, 0.0, 168.11, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 295.9433,-1573.1570,33.4921, 0.0, 0.0, 353.26, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 278.0959,-1630.5670,33.3104, 0.0, 0.0, 258.97, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 279.6600,-1630.8069,33.3106, 0.0, 0.0, 79.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1809.6760,-1597.4198,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1808.7030,-1598.2594,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1807.7208,-1599.1165,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1806.6851,-1599.9830,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1805.7251,-1600.8068,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1710.4633,-1604.8462,13.5469, 0.0, 0.0, 40.0, 0, 0, 5, 0},
	{1216, NOT_DYNAMIC_OBJECT, 1711.2843,-1605.5693,13.5469, 0.0, 0.0, 180.0, 0, 0, 5, 0},
	{1363, DYNAMIC_OBJECT, 1811.6613,-1738.6979,13.5469, 0.0, 0.0, 90.0, 0, 0, 2, 0}
};
payphonesOnGameModeInit() {
	new PayPhoneLabel[128];
	for(new i=0; i < sizeof(PayPhones); i++) {
		if(PayPhones[i][EPhoneType] == DYNAMIC_OBJECT) {
			PayPhones[i][EPayPhoneObjectID] = CreateDynamicObject(PayPhones[i][EPayPhoneModelID],PayPhones[i][EPayPhoneX],PayPhones[i][EPayPhoneY],PayPhones[i][EPayPhoneZ]-0.30, PayPhones[i][EPayPhoneRotX], PayPhones[i][EPayPhoneRotY], PayPhones[i][EPayPhoneRotZ],PayPhones[i][EPayPhoneVW],PayPhones[i][EPayPhoneInt]);
		}
		format(PayPhoneLabel, sizeof(PayPhoneLabel), "{%s} *** PAYPHONE ***", getColourString(TEAM_GROVE_COLOR)); //PayPhones[i][EPayPhoneCost] old
		PayPhones[i][EPayPhoneText] = CreateDynamic3DTextLabel(PayPhoneLabel, 0x89CEF3, PayPhones[i][EPayPhoneX], PayPhones[i][EPayPhoneY], PayPhones[i][EPayPhoneZ]+0.25, 10.0,INVALID_PLAYER_ID, INVALID_VEHICLE_ID,0,PayPhones[i][EPayPhoneVW], PayPhones[i][EPayPhoneInt]);
	}
}
isAtPayPhone(playerid) {
	for(new i=0;i<sizeof(PayPhones);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, PayPhones[i][EPayPhoneX], PayPhones[i][EPayPhoneY],PayPhones[i][EPayPhoneZ])) {
			return 1;
		}
	}
	return 0;
}
getClosestPayPhone(playerid) {
	for(new i=0;i<sizeof(PayPhones);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, PayPhones[i][EPayPhoneX], PayPhones[i][EPayPhoneY],PayPhones[i][EPayPhoneZ])) {
			return i;
		}
	}
	return -1;
}
endPayPhoneCall(playerid) {
	if(GetPVarType(playerid, "payphonetimer") != PLAYER_VARTYPE_NONE) {
		if(GetPVarType(playerid, "OldPhoneStatus") != PLAYER_VARTYPE_NONE) {
			new oldphonestatus = GetPVarInt(playerid, "OldPhoneStatus");
			SetPVarInt(playerid, "PhoneStatus", oldphonestatus);	
			DeletePVar(playerid, "OldPhoneStatus");	
		}
		new timer = GetPVarInt(playerid, "payphonetimer");
		KillTimer(timer);
		DeletePVar(playerid,"payphonetimer");
	}
}
isCallingFromPayPhone(playerid) {
	new phonestatus = GetPVarInt(playerid, "PhoneStatus");
	if(phonestatus == 3) {
		return 1;
	}
	return 0;
}
forward CheckPayPhoneCall(playerid);
public CheckPayPhoneCall(playerid) {
	if(GetPVarType(playerid, "payphonetimer") != PLAYER_VARTYPE_NONE) {
		new string[128];
		if(isAtPayPhone(playerid)) {
			if(GetMoneyEx(playerid) < 5) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have any more money, call hanged up");
				hangupCall(playerid, 1);
				endPayPhoneCall(playerid);
			}
			new index = getClosestPayPhone(playerid);
			format(string, sizeof(string), "~r~-$%d", PayPhones[index][EPayPhoneCost]);
			GiveMoneyEx(playerid, -PayPhones[index][EPayPhoneCost]);
			GameTextForPlayer(playerid, string, 5000, 1);
		} else {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "Left the PayPhone, call hanged up");
			hangupCall(playerid, 2);
			endPayPhoneCall(playerid);
			return 1;
		}
	}
	return 1;
}
YCMD:payphone(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Call someone from a PayPhone.");
		return 1;
	}
	new number;
	if(isPlayerFrozen(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this while frozen!");
		return 1;
	}
	if(!isAtPayPhone(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at a PayPhone!");
		return 1;
	}
	if(!sscanf(params,"d",number)) {
		new player = findPlayerFromPhoneNumber(number);
		if(GetPVarInt(player,"PhoneStatus") == 2 || getPlayerPhoneID(player) != -1 || GetPVarInt(player, "Calling") != PLAYER_VARTYPE_NONE || GetPVarInt(player, "CalledBy") != PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, COLOR_GREY, "You get a busy tone...");
			return 1;
		}
		if(GetPVarType(playerid, "Calling") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "CalledBy") != PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_WHITE, "You are already in a call");
			return 1;
		}
		if(number == 911) {
			SetPlayerAttachedObject(playerid, 9, 330, 6);
			SetPlayerSpecialAction(playerid,SPECIAL_ACTION_USECELLPHONE);
			start911Call(playerid);
			return 1;
		}
		if(player == -1 || number == 0 || isInJail(player)) {
			SendClientMessage(playerid, X11_RED2, "Phone number is not valid, or player is offline");
			return 1;
		}
		new oldphonestatus = GetPVarInt(playerid, "PhoneStatus");
		if(oldphonestatus != 3) {
			SetPVarInt(playerid, "OldPhoneStatus", oldphonestatus);
		}
		SetPVarInt(playerid, "PhoneStatus", 3);
		SetPVarInt(player, "CalledBy",playerid);
		SetPVarInt(playerid, "Calling",player);
		StartCalling(playerid, player);
		new payphonetimer = SetTimerEx("CheckPayPhoneCall", 5000, true,"d",playerid);
		SetPVarInt(playerid, "payphonetimer", payphonetimer);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "You are now calling from a PayPhone, charges may be added.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /payphone [number]");
	}
	return 1;
}
payphonesOnPayPhoneDisconnect(playerid, reason) {
	#pragma unused reason
	endPayPhoneCall(playerid);
}