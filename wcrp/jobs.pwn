/*
	All jobs(besides side-jobs for now at least) go here
	PVars here:
	JobTime(int) - Job contract time
	JobPickupCooldown(int) - time since last pickup
	TrashRoute(int) - Route ID of route you are on
	TrashRouteIndex(int) - Index of your last completed route
*/

#define MAX_JOB_NAME 32
#define JOB_EXPIRE_TIME 1800 //Was 3600 1 hour, it's now 30 minutes
#define JOB_PICKUP_COOLDOWN 15
#define TAXI_CHARGE_TIME 15
#define MECH_OFFER_COOLDOWN 30 //seconds you must wait before offering someone another refill
#define GOV_TAX_RATE 0.006

enum EJobInfo  {
	EJobName[MAX_JOB_NAME],
	Float:EJobPickupX,
	Float:EJobPickupY,
	Float:EJobPickupZ,
	EJobPickupInt,
	EJobPickupVW,
	EJobPickupID,
	Text3D:EJobText,
	Float:EJobSpotX,
	Float:EJobSpotY,
	Float:EJobSpotZ,
	EJob_Illegal,
	FamilyFlags:EJobRestriction,
};
//todo: mechanic, lawyer, trucker?

new JobPickups[][EJobInfo] = { 
	{"Trash Cleaner",359.68, 180.25, 1008.38, 3, 1,  0, Text3D:0,2322.636474, -2077.395263, 13.321226, 0, EFamilyType_Legal },
	{"Drug Dealer",2143.82, -1674.08, 15.09,0 ,0 ,0 , Text3D:0,2168.98, -1679.85, -15.09, 1, EFamilyType_Biker | EFamilyType_Mafia | EFamilyType_StreetGang },
	{"Arms Dealer",2666.66, -1108.06, 69.39,0 ,0 ,0 , Text3D:0,2666.66, -1108.06, 69.39, 1, EFamilyType_Biker | EFamilyType_Mafia },
	{"Taxi Driver", 359.47, 184.91, 1008.38, 3, 1, 0 ,Text3D:0, 1763.25, -1900.96, 13.56, 0, EFamilyType_Legal },
	{"Mercenary", 757.99, 5.37, 1000.70, 5,0, 0, Text3D:0,2232.48, -1726.69, 13.55, 1, EFamilyType_Biker | EFamilyType_Mafia | EFamilyType_StreetGang },
	{"Mechanic", 358.99, 167.42, 1008.38, 3, 1, 0, Text3D:0, 187.152008, -266.981597, 1.379108, 0, EFamilyType_Legal },
	{"Trucker",2196.027832, -2236.349121, 13.546875, 0, 0, 0, Text3D:0,2187.762695, -2273.814208, 13.546875,0, EFamilyType_Legal },
	{"Lawyer",358.99, 163.11, 1008.38, 3, 1, 0, Text3D:0,358.62, 163.11, 1008.38,0, EFamilyType_Legal },
	{"Pizza Man",2100.7856, -1804.1826, 13.5546, 0, 0, 0, Text3D:0,2100.7856, -1804.1826, 13.5546,0, EFamilyType_Legal },
	{"Crafter",2443.032714, -1964.974365, 13.546875, 0, 0, 0, Text3D:0,2443.032714, -1964.974365, 13.546875,1, EFamilyType_Biker | EFamilyType_Mafia }
};
/*
enum ETrashPathRouteInfo {
	Float:ETrashPointX,
	Float:ETrashPointY,
	Float:ETrashPointZ,
	ETrashPointName[32],
	ETrashRouteIndex,
};

enum ETrashRouteInfo {
	ETrashPathName[32],
	ETrashPathReward,
}

new TrashRoutePaths[][ETrashPathRouteInfo] = {
//ROUTE 1
{2691.063476,-2051.785400,13.184736,"WillowField",0},
{2870.521484,-1690.922363,10.587408,"East Beach",0},
{2659.175781,-1655.355590,10.403504,"LS Stadium",0},
{2470.913330,-1441.509521,24.324380,"East Los Angeles",0},
{2164.099365,-1382.546142,23.533029,"Jefferson",0},
{2109.733154,-1679.819946,13.078238,"Idlewood",0},
{2170.679443,-1895.969116,13.075672,"WillowField",0},
{2292.956298,-2095.010009,13.017915,"Route End",0},
//ROUTE 2
{2216.190185,-1988.616699,13.040861,"Willowfield",1},
{1926.863403,-1930.061645,13.088657,"El Sereno",1},
{1754.740112,-1818.431030,13.089556,"Union Station",1},
{1531.803222,-1699.604003,13.087725,"LSPD",1},
{1327.480468,-1570.967895,13.066282,"Commerce",1},
{1359.933349,-1160.671630,23.377698,"Market",1},
{1750.039672,-1166.117309,23.338724,"Downtown LS",1},
{1846.061157,-1419.971923,13.095695,"Echo Park",1},
{1819.081665,-1677.358276,13.086851,"Alhambra",1},
{1872.731811,-1934.315917,13.087196,"El Sereno",1},
{2083.479248,-1909.324951,13.085927,"Willowfield",1},
{2290.508300,-2092.346679,13.023062,"Route End",1},
//ROUTE 3
{2579.897705,-2380.190429,13.271441,"Ocean Docks",2},
{2759.141113,-2460.304199,13.230901,"Ocean Docks",2},
{2481.328369,-2578.465576,13.202222,"Ocean Docks",2},
{2227.451416,-2511.699218,13.073944,"LA international",2},
{1993.087402,-2670.601806,9.175144,"LA international",2},
{1425.037109,-2666.486572,13.081760,"LA international",2},
{1360.127563,-2498.104248,11.276537,"LA international",2},
{1156.496093,-2397.232666,10.565136,"Verdant Bluffs",2},
{970.303161,-1779.998046,13.803142,"Verona Beach",2},
{599.539733,-1722.524780,13.409934,"Santa Maria Beach",2},
{169.744522,-1575.317504,12.383356,"Rodeo",2},
{425.769714,-1344.217895,14.585289,"Rodeo",2},
{852.563293,-1023.404968,27.375007,"Vinewood",2},
{1389.450927,-944.808898,33.931022,"Vinewood",2},
{1879.583740,-1019.491210,35.827777,"Echo Park",2},
{2173.109375,-1203.408203,23.629276,"Jefferson",2},
{2199.435302,-1386.091918,23.535583,"Jefferson",2},
{2213.217285,-1720.550903,13.061816,"Inglewood",2},
{2291.020507,-2092.607421,13.025842,"Route End",2}
};

new TrashRoutes[][ETrashRouteInfo] = {
	{"Route 1", 300},
	{"Route 2", 435},
	{"Route 3", 350}
};
*/
enum ERouteInfo {
	Float:EIRX,
	Float:EIRY,
	Float:EIRZ,
};
new TruckerRoutes[][ERouteInfo] = {
	{1926.68579, 172.34605, 36.68267},
	{1352.02319, 355.39633, 19.71841},
	{-21.72571, -275.14276, 4.41887},
	{-1005.98621, -694.07623, 31.61895},
	{-1723.90417, -117.58652, 3.08411},
	{1021.64233, -320.58020, 73.49747},
	{698.91205, -467.29791, 15.84678},
	{314.70004, -234.56453, 1.04664},
	{2605.35986, -2210.45020, 12.97496},
	{2094.36084, -2095.32056, 13.14806},
	{1559.00952, 14.60896, 23.78474},
	{1313.19141, -866.94922, 39.17013},
	{1342.32495, -1751.32117, 12.96308},
	{2663.68750, -1593.99060, 12.89099}
};

enum EGunJobPrices { 
	EGunJobName[10],
	EGunJobWeaponID,
	EGunJobAMats,
	EGunJobBMats,
	EGunJobCMats,
	EGunJobLevel,
	EGunJobLevelUp, //0 Doesn't level up, 1 to level up per sale
};


enum ESkillInfo {
	ESkill_Name[32],
	ESkill_PVarName[32],
	ESkill_Level1Points,
	ESkill_Level2Points,
	ESkill_Level3Points,
	ESkill_Level4Points,
	ESkill_Level5Points,
	ESkill_Job, //if the skill is associated with a job
	ESkill_LevelUpStr[128],
};
new Skills[][ESkillInfo] = {{"Gun Skill","gunskill",50,100,200,300,500,EJobType_Arms_Dealer, "You have to sell %d more guns to reach the next level!"},
							{"Drug Skill","drugskill",50,100,200,300,500,EJobType_Drug_Dealer, "You have to sell %d more drugs to reach the next level!"},
							{"Mercenary Skill","mercskill",50,100,200,300,500,EJobType_Mercenary, "%d"},
							{"Lock Picking Skill","lockskill",50,100,200,300,500,-1, "You need to jack %d more cars to reach the next level!"},
							{"Mechanic Skill","mechskill",50,100,200,300,500,EJobType_Mechanic, "Service %d more cars"},
							//{"Fishing Skill","fishskill",50,100,200,300,500,-1, "Catch %d more fish"},
							{"Boxing Skill","boxskill",50,100,200,300,500,-1, "Win %d more fights"},
							{"Lawyer Skill","lawyerskill",50,100,200,300,500,EJobType_Lawyer, "Free %d more people"},
							{"Crafter Skill","crafterskill",50,100,200,300,500,EJobType_Crafter, "Craft %d more items"}
							};

new GunJobPrices[][EGunJobPrices] = {{"9mm",22,900,0,0,1,1},
									{"sdpistol",23,600,0,0,2,1},
									{"bat",5,100,0,0,0,1},
									//{"mace",41,300,200,0,2,1},
									//{"fire",42,400,200,0,2,1},
									{"knuckles",1,100,0,0,0,1},
									{"shovel",6,50,0,0,0,1},
									{"flowers",14,30,0,0,0,0},
									{"eagle",24,2000,0,750,3,1},
									{"uzi",28,0,0,800,3,1},
									{"tec9",32,0,0,800,3,1},
									{"mp5",29,500,500,500,3,1},
									{"shotgun",25,0,200,0,2,1},
									//{"spas12",27,3000,3000,3000,5,1},
									{"ak47",30,2000,0,2000,4,1},
									{"m4",31,1500,0,1000,3,1},
									{"rifle",33,2000,2000,2000,2,1},
									{"club",2,100,0,0,1,1},
									{"golf",2,100,0,0,1,1},
									{"cue",7,100,0,0,1,1},
									{"molotov",18,1000,1000,1000,5,1},
									//{"minigun",38,25000,25000,25000,5,1},
									{"rpg",35,40000,40000,40000,5,1}
									};

enum {
	EJobs_JobOffer = EJob_Base + 1,
	EJobs_TrashDialog = EJobs_JobOffer + 10,
	EJobs_GunSellDialog,
	EJobs_DrugSellDialog,
	EJobs_SkillsDialog,
	EJobs_SkillInfoDialog,
	EJobs_RefillOffer,
	EJobs_RepairOffer,
	EJobs_TuneMenu,
	EJobs_SpotMenu,
	EJobs_SetSkillLevelMenu,
	EJobs_SetSkillLevel,
	EJobs_SellVest,
	EJobs_LawyerFree,
	EJobs_FoodSellOptions,
	EJobs_FoodSellDialog,
};

enum { //Food types
	TYPE_ICECREAM,
	TYPE_HOTDOG,
}

enum ESellableFoodInfo {
	EFoodInfoDispName[64],
	EFoodPrice,
	EFoodType,
	EHungerIncrement,
}
new SellableFoodInfo[][ESellableFoodInfo] = {	{"Hot Dog with Ketchup",8,TYPE_HOTDOG,50}
												,{"Hot Dog with Mustard",10,TYPE_HOTDOG,50}
												,{"Hot Dog with Mayonnaise",10,TYPE_HOTDOG,50}
												,{"Hot Dog and Coca Cola",12,TYPE_HOTDOG,50}
												,{"Hot Dog and Sprite",12,TYPE_HOTDOG,50}
												,{"Vanilla Ice Cream",5,TYPE_ICECREAM,30}
												,{"Chocolate Ice Cream",7,TYPE_ICECREAM,30}
												,{"Black Walnut Ice Cream",7,TYPE_ICECREAM,30}
												,{"Blueberry Cheesecake Ice Cream",8,TYPE_ICECREAM,30}
												,{"Daiquiri 'Ice' Ice Cream",11,TYPE_ICECREAM,30}
												,{"German Chocolate Cake Ice Cream",10,TYPE_ICECREAM,30}
												,{"Lemon Custard Ice Cream",10,TYPE_ICECREAM,30}
												,{"Orange Sherbet Ice Cream",9,TYPE_ICECREAM,30}
};

enum EBuyableComponentInfo {
	ECompInfoDispName[64],
	ECompInfoID,
	ECompPrice,
}
new BuyableComponents[][EBuyableComponentInfo] = {//{"2x Nitro",1009,500} 
												//, {"5x Nitro",1008,1500}
												//, {"10x Nitro",1010,2000}
												  {"Shadow Rims",1073,500}
												, {"Mega Rims",1074,700}
												, {"Rimshine Rims",1075,800}
												, {"Wire Rims",1076,1000}
												, {"Classic Rims",1077,1200}
												, {"Twist Rims",1078,1400}
												, {"Cutter Rims",1079,1700}
												, {"Switch Rims",1080,1900}
												, {"Grove Rims",1081,2000}
												, {"Import Rims",1082,2800}
												, {"Dollar Rims",1083,3000}
												, {"Trance Rims",1084,3400}
												, {"Atomic Rims",1085,3800}
												, {"Hydraulics",1087,1000}
};

new lastpayday;
jobsOnGameModeInit() {
	new jobdesc[64];
	for(new i=0;i<sizeof(JobPickups);i++) {
		format(jobdesc, sizeof(jobdesc), "{%s}Job:[{%s}%s{%s}]",getColourString(X11_WHITE),getColourString(COLOR_BRIGHTRED),JobPickups[i][EJobName],getColourString(X11_WHITE));
		JobPickups[i][EJobText] = CreateDynamic3DTextLabel(jobdesc, 0x2BFF00AA, JobPickups[i][EJobPickupX], JobPickups[i][EJobPickupY], JobPickups[i][EJobPickupZ]+1.5, 30.0,INVALID_PLAYER_ID, INVALID_VEHICLE_ID,0,JobPickups[i][EJobPickupVW], JobPickups[i][EJobPickupInt]);
		JobPickups[i][EJobPickupID] =  CreateDynamicPickup(1314, 16,  JobPickups[i][EJobPickupX], JobPickups[i][EJobPickupY], JobPickups[i][EJobPickupZ], JobPickups[i][EJobPickupVW], JobPickups[i][EJobPickupInt]);
	}
	new Hour, Minute, Second;
	new timenow = gettime(Hour,Minute, Second);
	timenow -= (Minute*60)-Second;
	lastpayday = timenow;
	//new minsleft = 60-Minute; //minutes left until next pay day
	//new timeleft = ((minsleft * 60)-Second) * 1000;
	//printf("payday in: %d %d\n",minsleft,timeleft);
	//paydaytimer = SetTimerEx("Payday",timeleft,false,"d",1);
}
new payhour = -1;
checkPayday() {
	new Hour, Minute, Second;
	gettime(Hour,Minute, Second);
	new timenow = gettime();
	if(Hour != payhour) {
		if(timenow > lastpayday+3600 && (Minute > 0 && Minute < 5)) {
			payhour = Hour;
			lastpayday = gettime();
			Payday();
		}
	}
}
Payday() {
	new Hour, Minute, Second;
	gettime(Hour,Minute, Second);
	SetWorldTime(Hour);
	new string[128];
	
	turfsOnPayDay();
	plantsOnPayday();
	swapWeather();
	manageOverallOldBackPacks();
	loggingUpdateLogs(); //Update logs and create new folders if the date changes
	foreach(Player, i) {
		if(!IsPlayerConnectEx(i)) {
			continue;
		}
		new logintime = GetPVarInt(i, "CharLoginTime");
		if(gettime() - logintime < 600) {
			SendClientMessage(i, X11_TOMATO_2, "You haven't been playing long enough to earn a paycheck(10 minutes)!");
			continue;
		}
		new paycheck = GetPVarInt(i, "Payday");
		paycheck += 1000;
		new account = GetPVarInt(i, "Bank");
		//Everything is calculated, display it all
		SendClientMessage(i, COLOR_GREENISHGOLD, "|___ BANK STATEMENT ___|");
		format(string, sizeof(string), "  Paycheck: $%s", getNumberString(paycheck));
		SendClientMessage(i, X11_WHITE, string);
		format(string, sizeof(string), "  Savings: $%s", getNumberString(account));
		SendClientMessage(i, X11_WHITE, string);
		if(server_taxrate != 0) {
			new Float:tax = server_taxrate*GOV_TAX_RATE;
			new rtax = floatround(tax*paycheck);
			format(string, sizeof(string), "  Tax: %d percent ($%s)", server_taxrate,getNumberString(rtax));
			SendClientMessage(i, X11_WHITE, string);
			account -= rtax;
		}
		new ticketprice = GetPVarInt(i, "TicketTotal");
		if(ticketprice > 0) {
			new tprice = ticketprice/12;
			format(string, sizeof(string), "  Unpaid Ticket Fine: -$%s",getNumberString(tprice));
			account -= tprice;
		}
		new housetax = getHouseTax(i);
		if(housetax > 60000) housetax = 60000;
		format(string, sizeof(string), "  Property Tax: -$%s", getNumberString(housetax));
		SendClientMessage(i, X11_WHITE, string);
		new rprice = getRentPrice(i);
		if(rprice != 0) {
			format(string, sizeof(string), "  Rent: -$%s", getNumberString(rprice));
			SendClientMessage(i, X11_WHITE, string);
			account -= rprice;
		}
		new uflags = GetPVarInt(i, "UserFlags");
		if(uflags & EUFHasCarInsurance) {
			new insuranceprice = getInsurancePaydayPrice(i);
			format(string, sizeof(string), "  Vehicle Insurance: -$%s", getNumberString(insuranceprice));
			SendClientMessage(i, X11_WHITE, string);
			account -= insuranceprice;
		}
		if(uflags & EUFHasHealthInsurance) {
			new hprice = getHealthInsurancePrice(i);
			format(string, sizeof(string), "  Health Insurance: -$%s", getNumberString(hprice));
			SendClientMessage(i, X11_WHITE, string);
			account -= hprice;
		}
		new rentcarprice = GetTotalRentCarPrice(i);
		if(rentcarprice != 0) {
			format(string, sizeof(string), "  Car Rentals: -$%s",getNumberString(rentcarprice));
			SendClientMessage(i, X11_WHITE, string);
			account -= rentcarprice;
		}
		paycheck += getFactionPay(i);
		account -= housetax;
		SetPVarInt(i, "Bank", account);
		/* Double Bonus START */
		if(server_doublebonus != 0) {
			SetPVarInt(i, "RespectPoints", GetPVarInt(i, "RespectPoints")+2);
		} else {
			SetPVarInt(i, "RespectPoints", GetPVarInt(i, "RespectPoints")+1);
		}
		/* Double Bonus END */
		if(numPlayersOnServer() < 40) {
			new bonus = floatround(paycheck*0.25);
			format(string, sizeof(string), "((  Low Player Bonus: $%s ))", getNumberString(bonus));
			SendClientMessage(i, X11_WHITE, string);
			paycheck += bonus;
		}
		new ctime = GetPVarInt(i, "ConnectTime");
		if(++ctime == 50) {
			new dps = GetPVarInt(i, "DonatePoints");
			dps += 40;
			SetPVarInt(i, "DonatePoints",dps);
			GiveMoneyEx(i, 10000);
			dps = GetPVarInt(i, "Cookies");
			dps++;
			SetPVarInt(i, "Cookies", dps);
			format(string, sizeof(string), "* %s has reached 50 playing hours, and received $10,000, 40 donator points,and a cookie!",GetPlayerNameEx(i, ENameType_CharName));
			SendGlobalAdminMessage(X11_YELLOW,string);
			SendClientMessage(i, COLOR_CUSTOMGOLD, "* You have reached 50 playing hours, and have received $10,000, 40 donator points, and a cookie! Congratulations!");
			SetPlayerColor(i, getNameTagColour(i));
		}
		SetPVarInt(i, "ConnectTime", ctime);
		tryAddInterest(i, account); //Add the interest, this depends on how much they have on their bank account
		SendClientMessage(i, COLOR_GREENISHGOLD, "|---------------------------------|");
		/* Double Bonus START */
		if(server_doublebonus != 0) {
			SetPVarInt(i, "PayCheque", paycheck*2);
		} else {
			SetPVarInt(i, "PayCheque", paycheck);
		}
		/* Double Bonus END */
		tryLevelUp(i); //Check the player's exp and try to level him / her up
		DeletePVar(i, "Payday");
		attemptToCreateCheque(i);
		GameTextForPlayer(i, "~g~Pay Day", 5000, 1);
	}
	playLotto();
}
tryAddInterest(playerid, bank) {
	new string[128];
	if(isBankInNegative(playerid)) {
		format(string, sizeof(string), "  New Bank Balance: $%s", getNumberString(bank));	
	} else {
		new interest = calculateInterest(bank);
		format(string, sizeof(string), "  New Bank Balance: $%s ($%s) Interest Rate:(%d percent)", getNumberString(bank+interest), getNumberString(interest), server_interestrate);
		SetPVarInt(playerid, "Bank", bank+interest);
	}
	SendClientMessage(playerid, X11_WHITE, string);
}
calculateInterest(amount) {
	return floatround(float(amount) * (float(server_interestrate)/1000));
}
getFactionPay(playerid) 
{
	new amount;
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	new msg[128];
	if(faction == 1) {
		amount = rank*250;
		format(msg, sizeof(msg), "  LSPD Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);
	} else if(faction == 2) {
		amount = rank*550;
		format(msg, sizeof(msg), "  FBI Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);
	} else if(faction == 4) {
		amount = rank*550;
		format(msg, sizeof(msg), "  Fox News Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	} else if(faction == 3) {
		amount = rank*550;
		format(msg, sizeof(msg), "  Government Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	} else if(faction == 5) {
		amount = rank*550;
		format(msg, sizeof(msg), "  EMS Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	} else if(faction == 6) {
		amount = rank*200;
		format(msg, sizeof(msg), "  EPHS Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	} else if(faction == 7) {
		amount = rank*250;
		format(msg, sizeof(msg), "  LADOT Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	} else if(faction == 8) {
		amount = rank*550;
		format(msg, sizeof(msg), "  CADOC Paycheck: $%s",getNumberString(amount));
		SendClientMessage(playerid, X11_WHITE, msg);		
	}
	return amount;
}
/*
YCMD:signcheck(playerid, params[], help) {
	new check = GetPVarInt(playerid, "PayCheque");
	if(check == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have already signed your check!");
		return 1;
	}
	new msg[128];
	new account = GetPVarInt(playerid, "Bank");
	account += check;
	format(msg, sizeof(msg), "* You earned $%s",getNumberString(check));
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	SetPVarInt(playerid, "Bank", account);
	DeletePVar(playerid, "PayCheque");
	return 1;
}
*/
jobsOnPickupPickup(playerid, pickupid) {
	new title[128];
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(JobPickups);i++) {
		if(JobPickups[i][EJobPickupID] == pickupid) {
			if(JobPickups[i][EJob_Illegal] > 0) {
				new familyid = GetPVarInt(playerid, "Family");
				if (familyid == 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "* Only family members can get this job.");
					return 1;
				}
				#if debug
				new msg[128];
				format(msg, sizeof(msg), "[DEBUG]: FamTypeFlag: %d, JobRestrictionFlag: %d", getFamilyType(familyid, 1), _:JobPickups[i][EJobRestriction]);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
				#endif
				if(~_:JobPickups[i][EJobRestriction] & getFamilyType(familyid, 1) || getFamilyType(familyid, 1) == _:EFamilyType_None) {
					SendClientMessage(playerid, X11_TOMATO_2, "* Your faction can't take this job.");
					return 1;
				}
			}
			format(dialogstr,sizeof(dialogstr),"{FFFFFF}Would you like to join the %s Job? Press \"{FF0000}Accept{FFFFFF}\" to accept, or \"{FF0000}Reject{FFFFFF}\" to reject",JobPickups[i][EJobName]);
			format(title,sizeof(title),"{00BFFF}%s Job Offer",JobPickups[i][EJobName]);
			new remaining = CanTakeJob(playerid);
			if(remaining != 0) {
				format(title, sizeof(title), "You must wait {FF6347}%d{FFFFFF} minute(s) for your job contract to finish!", remaining/60);
				SendClientMessage(playerid, X11_WHITE, title);
				return 0;
			}
			SetPVarInt(playerid, "JobOffer", i);
			ShowPlayerDialog(playerid, _:EJobs_JobOffer, DIALOG_STYLE_MSGBOX, title, dialogstr, "Accept", "Reject");
			movePlayerBack(playerid, 3.0);
		}
	}
	return 1;
}
hasLegalJob(playerid) {
	new jobid = GetPVarInt(playerid, "Job");
	if(jobid != -1) {
		if(JobPickups[jobid][EJob_Illegal] != 0) {
			return 0;
		}
	}
	return 1;
}
jobsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	onJobAbort(playerid);
	if(GetPVarType(playerid, "TaxiDriver") != PLAYER_VARTYPE_NONE) {
		onTaxiPassengerAbort(playerid);
	}
}
OnPlayerEnterTaxi(playerid, driver) {
	SetPVarInt(playerid, "TaxiDriver", driver);
	SetPVarInt(driver, "TaxiPassenger", playerid);	
	//new timer = SetTimerEx("TaxiTimer",TAXI_CHARGE_TIME,true, "dd", driver, playerid);
	//SetPVarInt(playerid, "TaxiTimer", timer);
	//TaxiTimer(driver, playerid);
}
doTaxiCharges() {
	new msg[128];
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "TaxiDriver") != PLAYER_VARTYPE_NONE) {
				new driver = GetPVarInt(i, "TaxiDriver");
				if(!IsPlayerConnectEx(driver)) {
					DeletePVar(i, "TaxiDriver");
					continue;
				}
				if(!IsPlayerInAnyVehicle(i)) {
					DeletePVar(i, "TaxiDriver");
					continue;
				}
				new cost = GetPVarInt(driver, "TaxiFare");
				if(cost == 0) {
					DeletePVar(i, "TaxiDriver");
					continue;
				}
				if(GetPVarType(i, "TaxiDriver") != PLAYER_VARTYPE_NONE) {
					if(GetMoneyEx(i) >= cost) {
						new pay = GetPVarInt(driver, "Payday");
						pay += cost;
						SetPVarInt(driver, "Payday",pay);
						GiveMoneyEx(i, -cost);
						format(msg, sizeof(msg), "You paid $%s to the Taxi Driver",getNumberString(cost));
						SendClientMessage(i, TEAM_GROVE_COLOR, msg);
						format(msg, sizeof(msg), "You earned an additional $%s on your paycheck!",getNumberString(cost));
						SendClientMessage(driver, TEAM_GROVE_COLOR, msg);
					} else {
						SendClientMessage(i, X11_TOMATO_2, "You are being thrown out since you don't have enough money");
						RemovePlayerFromVehicle(i);
						DeletePVar(i, "TaxiDriver");
					}
				}
			}
		}
	}
}
/*
forward TaxiTimer(driver, passenger);
public TaxiTimer(driver, passenger) {
	new pay = GetPVarInt(driver, "Payday");
	new cost = GetPVarInt(driver, "TaxiFare");
	if(cost == 0) {
		if(GetPVarType(passenger, "TaxiTimer") != PLAYER_VARTYPE_NONE) {
			KillTimer(GetPVarInt(passenger, "TaxiTimer"));
			DeletePVar(passenger, "TaxiTimer");
		}
	}
	new msg[128];
	if(GetMoneyEx(passenger) >= cost) {
		pay += cost;
		SetPVarInt(driver, "Payday", pay);
		GiveMoneyEx(passenger, -cost);
		format(msg, sizeof(msg), "You paid $%d to the Taxi Driver",cost);
		SendClientMessage(passenger, TEAM_GROVE_COLOR, msg);
		format(msg, sizeof(msg), "You earned an additional $%d on your paycheck!",cost);
		SendClientMessage(driver, TEAM_GROVE_COLOR, msg);
	} else {
		SendClientMessage(passenger, X11_TOMATO_2, "You are being thrown out since you don't have enough money");
		RemovePlayerFromVehicle(passenger);
	}
	return 1;
}
*/
getMobileFoodIndexByType(playerid, type) {
	new charid = GetPVarInt(playerid, "CharID");
	for(new i=0;i<sizeof(WepCraftInfo);i++) {
		if(WepCraftInfo[index][pOwnerSQLID] != charid) {
			#if debug
			printf("index++: %d", index);
			#endif
			index++;
			continue;
		}
	}
	return -1;
}
getMobileMenuFoodIndex(index, type) {
	new x;
	for(new i=0;i<sizeof(SellableFoodInfo);i++) {
		if(SellableFoodInfo[i][EFoodType] == type) {
			if(SellableFoodInfo[i][EFoodPrice] != 0) {
				if(x++ == index) {
					return i;
				}
			}
		}
	}
	return -1;
}
jobOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	new msg[128];
	switch(dialogid) {
		case EJobs_JobOffer: {
			if(response) {
				new jobid = GetPVarInt(playerid, "JobOffer");
				SetPlayerJob(playerid, jobid/*, JobPickups[jobid][EJob_Illegal]==0?0:1*/);
			}
			DeletePVar(playerid, "JobOffer");
		}
		/*
		case EJobs_TrashDialog: {
			if(response) {
				startRoute(playerid, listitem);
			}
		}
		*/
		case EJobs_GunSellDialog: {
			new gunseller = GetPVarInt(playerid, "GunSeller");
			new gunprice = GetPVarInt(playerid, "GunPrice");
			new gunid = GetPVarInt(playerid, "GunSellID");
			if(response) {
				if(GetMoneyEx(playerid) < gunprice) {
					SendClientMessage(playerid, X11_RED3, "You don't have enough money for this weapon.");
					SendClientMessage(gunseller, X11_RED3, "This player doesn't have enough money for the gun!");
				} else {
					new Float:X,Float:Y,Float:Z;
					GetPlayerPos(gunseller, X, Y, Z);
					if(IsPlayerInRangeOfPoint(playerid,3.0,X, Y, Z)) {
						if(canAffordGun(gunseller, gunid)) {
							GiveMoneyEx(playerid, -gunprice);
							GiveMoneyEx(gunseller, gunprice);
							removeGunMaterials(gunseller, gunid);
							GivePlayerWeaponEx(playerid, gunid, -1);
							SendClientMessage(playerid, X11_RED3, "You have bought the weapon!");
							SendClientMessage(gunseller, COLOR_CUSTOMGOLD, "The player accepted the gun deal");
							new string[128];
							new weaponname[32];
							GetWeaponNameEx(gunid, weaponname, sizeof(weaponname));
							format(string, sizeof(string), "* %s makes a %s and hands it to %s.", GetPlayerNameEx(gunseller, ENameType_RPName), weaponname, GetPlayerNameEx(playerid, ENameType_RPName));
							ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
							doGunResolution(gunseller, gunid);
						}
					} else {
						SendClientMessage(playerid, X11_RED3, "You are too far away from the gun seller now.");
					}
				}
			} else {
				SendClientMessage(playerid, X11_RED3, "You rejected the gun deal");
				SendClientMessage(gunseller, X11_RED3, "Your gun offer has been rejected");
			}
			DeletePVar(playerid, "GunSeller");
			DeletePVar(playerid, "GunPrice");
			DeletePVar(playerid, "GunSellID");
		}
		case EJobs_DrugSellDialog: {
			new Float:X,Float:Y,Float:Z;
			new drugseller = GetPVarInt(playerid, "DrugSeller");
			new drugprice = GetPVarInt(playerid, "DrugPrice");
			new amount = GetPVarInt(playerid, "DrugAmount");
			
			new drugname[32];
			GetPVarString(playerid, "DrugType", drugname, sizeof(drugname));
			
			DeletePVar(playerid, "DrugSeller");
			DeletePVar(playerid, "DrugPrice");
			DeletePVar(playerid, "DrugAmount");
			
			DeletePVar(playerid, "DrugType");
			
			GetPlayerPos(drugseller, X, Y, Z);
			if(response) {
				if(IsPlayerInRangeOfPoint(playerid,3.0,X, Y, Z)) {
					if(GetMoneyEx(playerid) < drugprice) {
						SendClientMessage(playerid, X11_WHITE, "You don't have enough money!");
						SendClientMessage(drugseller, X11_RED_3, "The player doesn't have enough money");
						return 1;
					}
					
				} else {
					SendClientMessage(playerid, X11_RED3, "You are too far away from the drug seller now.");
					return 1;
				}
				
				GiveMoneyEx(playerid, -drugprice);
				GiveMoneyEx(drugseller, drugprice);
				
				new total_drugs = GetPVarInt(drugseller, drugname);
				if(total_drugs < amount) {
					SendClientMessage(playerid, X11_TOMATO_2, "* The dealer does not have enough drugs.");
					SendClientMessage(drugseller, X11_TOMATO_2, "* You do not have enough drugs.");
					return 1;
				}
				total_drugs -= amount;
				SetPVarInt(drugseller, drugname, total_drugs);
				
				total_drugs = GetPVarInt(playerid, drugname);
				total_drugs += amount;
				SetPVarInt(playerid, drugname, total_drugs);
				
				
				increaseJobPoints(drugseller);
				
				SendClientMessage(playerid, COLOR_DARKGREEN, "* Drug Transaction complete");
				SendClientMessage(drugseller, COLOR_DARKGREEN, "* Drug Transaction complete");
				
				format(tempstr, sizeof(tempstr), "* %s hands some %s to %s.",GetPlayerNameEx(drugseller, ENameType_RPName), drugname,GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);

			} else {
				SendClientMessage(playerid, X11_RED3, "The drug deal has been rejected.");
				SendClientMessage(drugseller, X11_RED3, "The drug deal has been rejected.");
			}
		}
		case EJobs_SkillsDialog: {
			if(!response) return 0;
			showJobSkills(playerid, listitem);
		}
		case EJobs_SkillInfoDialog: {
			
		}
		case EJobs_RefillOffer: {
			new mech = GetPVarInt(playerid, "RefillMech");
			new price = GetPVarInt(playerid, "RefillPrice");
			DeletePVar(playerid, "RefillMech");
			DeletePVar(playerid, "RefillPrice");
			new Float:X, Float:Y, Float:Z;
			GetPlayerPos(mech, X, Y, Z);
			if(!response) {
				SendClientMessage(mech, X11_TOMATO_2, "The player rejected the refill offer!");
			} else {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "Not enough money!");
					return 1;
				}
				if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "The mechanic is too far away");
					return 1;
				}
				new carid = GetPlayerVehicleID(playerid);
				if(carid == 0) {
					SendClientMessage(playerid, COLOR_CUSTOMGOLD, "You must be in a car");
					return 1;
				}
				format(tempstr, sizeof(tempstr), "* %s fuels the vehicle.", GetPlayerNameEx(mech, ENameType_RPName));
				ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SetPVarInt(mech, "GasCans", GetPVarInt(mech, "GasCans") -1);
				new oldfuel = VehicleInfo[carid][EVFuel];
				oldfuel += 15;
				if(oldfuel > 100) {
					format(tempstr, sizeof(tempstr), "* Some fuel spills down the side.");
					ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					oldfuel = 100;
				}
				VehicleInfo[carid][EVFuel] = oldfuel;
				//CheckGas();
				GiveMoneyEx(playerid, -price);
				GiveMoneyEx(mech, price);
				//GiveMoneyEx(mech, -(oldfuel*2));
				increaseJobPoints(mech);
				//format(msg, sizeof(msg), "* The fuel costed you $%s!",getNumberString((oldfuel*2)));
				//SendClientMessage(mech, COLOR_CUSTOMGOLD, msg);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Vehicle Refilled");
				SendClientMessage(mech, COLOR_CUSTOMGOLD, "* Vehicle Refilled");
			}
		}
		case EJobs_RepairOffer: {
			new mech = GetPVarInt(playerid, "RepairMech");
			new price = GetPVarInt(playerid, "RepairPrice");
			DeletePVar(playerid, "RepairMech");
			DeletePVar(playerid, "RepairPrice");
			new Float:X, Float:Y, Float:Z;
			GetPlayerPos(mech, X, Y, Z);
			if(!response) {
				SendClientMessage(mech, X11_TOMATO_2, "The player rejected the repair offer!");
			} else {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "Not enough money!");
					return 1;
				}
				if(!IsPlayerInRangeOfPoint(playerid, 15.0, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "The mechanic is too far away");
					return 1;
				}
				new carid = GetPlayerVehicleID(playerid);
				if(carid == 0) {
					SendClientMessage(playerid, COLOR_CUSTOMGOLD, "You must be in a car");
					return 1;
				}
				increaseJobPoints(mech);
				RepairVehicle(carid);
				GiveMoneyEx(playerid, -price);
				setVehicleTotaled(carid, 0);
				if(mech != playerid) {
					price -= getVehRepairCost(carid);
					GiveMoneyEx(mech, price);
				}
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Vehicle Repaired");
				SendClientMessage(mech, COLOR_CUSTOMGOLD, "* Vehicle Repaired");
			}
		}
		case EJobs_TuneMenu: {
			if(!response) return 0;
			if(GetMoneyEx(playerid) < BuyableComponents[listitem][ECompPrice]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money!");
				return 1;
			}
			new vehicleid = GetPlayerVehicleID(playerid);
			new vehicleide = GetVehicleModel(vehicleid);
			new componentid = BuyableComponents[listitem][ECompInfoID];
			new modok = islegalcarmod(vehicleide, componentid);
			if(!modok) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this mod on this car!");
			}
			AddVehicleComponent(vehicleid, componentid);
			GiveMoneyEx(playerid, -BuyableComponents[listitem][ECompPrice]);
			format(msg, sizeof(msg), "* You bought a %s for $%s",BuyableComponents[listitem][ECompInfoDispName], getNumberString(BuyableComponents[listitem][ECompPrice]));
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
			if(GetPVarInt(playerid, "Job") == EJobType_Mechanic) {
				increaseJobPoints(playerid);
			}
		}
		case EJobs_SpotMenu: {
			if(!response) return 0;
			SetPlayerCheckpoint(playerid, JobPickups[listitem][EJobSpotX], JobPickups[listitem][EJobSpotY], JobPickups[listitem][EJobSpotZ], 3.0);
			SetTimerEx("RemoveCheckpoint", 25000, false, "d", playerid);
			
		}
		case EJobs_SetSkillLevelMenu: {
			if(!response) return 0;
			SetPVarInt(playerid, "SetSkillIndex", listitem);
			tempstr[0] = 0;
			dialogstr[0] = 0;
			for(new i=0;i<5;i++) {
				format(tempstr, sizeof(tempstr), "Level %d\n",i+1);
				strcat(dialogstr, tempstr, sizeof(dialogstr));
			}
			ShowPlayerDialog(playerid, EJobs_SetSkillLevel, DIALOG_STYLE_LIST, "Set Skill Level",dialogstr,"Set", "Cancel");
		}
		case EJobs_SetSkillLevel: {
			if(!response) return 0;
			new skill = GetPVarInt(playerid, "SetSkillIndex");
			new target = GetPVarInt(playerid, "SetSkillTarget");
			DeletePVar(playerid, "SetSkillIndex");
			DeletePVar(playerid, "SetSkillTarget");
			format(msg, sizeof(msg), "* An admin set your %s level to %d",Skills[skill][ESkill_Name],listitem+1);
			SendClientMessage(target, X11_YELLOW, msg);
			format(msg, sizeof(msg), "* You set %s's %s level to %d",GetPlayerNameEx(target, ENameType_CharName),Skills[skill][ESkill_Name],listitem+1);
			SendClientMessage(playerid, X11_YELLOW, msg);
			new tagoffset = _:ESkill_Level1Points+listitem;
			new skilllevel = Skills[skill][ESkillInfo:tagoffset];
			if(listitem == 0) {
				skilllevel = 0;
			} else {
				skilllevel = Skills[skill][ESkillInfo:(tagoffset-1)]+1;
			}
			setSkillPoints(target, skill, skilllevel);
		}
		case EJobs_SellVest: {	
			new Float:amount = GetPVarFloat(playerid, "VestAmount");
			new user = GetPVarInt(playerid, "VestSeller");
			new price = GetPVarInt(playerid, "VestPrice");
			DeletePVar(playerid, "VestAmount");
			DeletePVar(playerid, "VestSeller");
			DeletePVar(playerid, "VestPrice");
			new isoffer = GetPVarInt(user, "VestOfferer") == playerid;
			if(isoffer) {
				DeletePVar(user, "VestOfferer");
			} else {
				if(GetPVarInt(user, "VestOfferer") != playerid) {
					SendClientMessage(playerid, X11_TOMATO_2, "This person is no longer offering you a vest");
					return 1;
				}
			}
			if(!response) {
				SendClientMessage(user, X11_TOMATO_2, "The vest offer has been rejected");
			} else {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
					SendClientMessage(user, X11_TOMATO_2, "The user didn't have enough money");
					return 1;
				}
				new matsprice = GetPVarInt(user, "VestMatsPrice");
				new matstotal = GetPVarInt(user, "MatsC");
				DeletePVar(user, "VestMatsPrice");
				if(matsprice > matstotal) {
					SendClientMessage(playerid, X11_TOMATO_2, "* The seller didn't have enough materials");
					SendClientMessage(user, X11_TOMATO_2, "* You don't have enough materials now!");
					return 1;
				}
				matstotal -= matsprice;
				SetPVarInt(user, "MatsC", matstotal);
				GiveMoneyEx(playerid, -price);
				GiveMoneyEx(user, price);
				new Float:total;
				GetPlayerArmourEx(playerid,total);
				amount += total;
				if(total > MAX_ARMOUR) {
					total = MAX_ARMOUR;
				}
				SetPlayerArmourEx(playerid, amount);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You have now bought a vest!");
				SendClientMessage(user, COLOR_LIGHTBLUE, "* The user bought a vest from you!");
				increaseJobPoints(user);
			}
		}
		case EJobs_LawyerFree: {
			new user = GetPVarInt(playerid, "LawyerID");
			new price = GetPVarInt(playerid, "LawyerPrice");
			new Float: X, Float: Y, Float: Z;
			DeletePVar(playerid, "LawyerID");
			DeletePVar(playerid, "LawyerPrice");
			new isoffer = GetPVarInt(user, "FreedomOfferer") == playerid;
			if(isoffer) {
				DeletePVar(user, "FreedomOfferer");
			} else {
					SendClientMessage(playerid, X11_TOMATO_2, "This person is no longer offering you a set out on bail.");
					return 1;
			}
			if(!response) {
				SendClientMessage(user, X11_TOMATO_2, "The bail offer has been rejected");
			} else {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
					SendClientMessage(user, X11_TOMATO_2, "The user didn't have enough money");
					return 1;
				}
				GetPlayerPos(user, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from the lawyer!");
					return 1;
				}
				if(GetPVarInt(playerid, "Bail") == PLAYER_VARTYPE_NONE) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have a bail.");
					SendClientMessage(user, X11_TOMATO_2, "The person you're trying to set free isn't on bail.");
					return 1;
				}
				if(GetPVarType(playerid, "ReleaseTime") != PLAYER_VARTYPE_NONE) {
					GetPlayerPos(user, X, Y, Z);
					SetPlayerPos(playerid, X, Y, Z);
					DeletePVar(playerid, "ReleaseTime");
					DeletePVar(playerid, "Bail");
					TogglePlayerControllableEx(playerid, 1);
					increaseJobPoints(user);
					GiveMoneyEx(playerid, -price);
					GiveMoneyEx(user, price);
					SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You are now free on bail, follow the lawyer to the way out!");
					SendClientMessage(user, COLOR_LIGHTBLUE, "* The person paid the bail!");
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "This person is not in jail.");
				}
			}
		}
		case EJobs_FoodSellOptions: {
			if(response) {
				new playa = GetPVarInt(playerid, "mobileSellFoodTo");
				new type = GetPVarInt(playerid, "mobileFoodType");
				DeletePVar(playerid, "mobileSellFoodTo");
				DeletePVar(playerid, "mobileFoodType");
				new foodid = getMobileMenuFoodIndex(listitem, type);
				if(foodid != -1) {
					sendFoodOffer(playerid, playa, foodid);
				}
			} else {
				SendClientMessage(playerid, X11_RED3, "You closed the menu.");
			}
		}
		case EJobs_FoodSellDialog: {
			new foodseller = GetPVarInt(playerid, "FoodSeller");
			new index = GetPVarInt(playerid, "FoodID");
			if(response) {
				if(GetMoneyEx(playerid) < SellableFoodInfo[index][EFoodPrice]) {
					SendClientMessage(playerid, X11_RED3, "You don't have enough money for this!");
					SendClientMessage(foodseller, X11_RED3, "This player doesn't have enough money for the food!");
				} else {
					new Float:X,Float:Y,Float:Z;
					GetPlayerPos(foodseller, X, Y, Z);
					if(IsPlayerInRangeOfPoint(playerid,3.0,X, Y, Z)) {
						GiveMoneyEx(playerid, -SellableFoodInfo[index][EFoodPrice]);
						GiveMoneyEx(foodseller, SellableFoodInfo[index][EFoodPrice]);
						new Float:HP;
						GetPlayerHealth(playerid, HP);
						HP += 20;
						SetPlayerHealthEx(playerid, HP);
						format(tempstr, sizeof(tempstr), "* %s takes $%s dollars from his pocket and buys a %s from %s.",GetPlayerNameEx(playerid, ENameType_RPName),getNumberString(SellableFoodInfo[index][EFoodPrice]),SellableFoodInfo[index][EFoodInfoDispName], GetPlayerNameEx(foodseller, ENameType_RPName));
						ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
						format(msg, sizeof(msg), "[INFO]: You bought the %s for $%s", SellableFoodInfo[index][EFoodInfoDispName], getNumberString(SellableFoodInfo[index][EFoodPrice]));
						new	boost = GetHungerLevel(playerid)+SellableFoodInfo[index][EHungerIncrement];
						if(boost > 100) boost = 100;
						SetPlayerHunger(playerid, boost);
						SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
					} else {
						SendClientMessage(playerid, X11_RED3, "You are too far away from the person who is selling the food now.");
					}
				}
			} else {
				SendClientMessage(playerid, X11_RED3, "You rejected the food");
				SendClientMessage(foodseller, X11_RED3, "Your food offer has been rejected");
			}
			DeletePVar(playerid, "FoodSeller");
			DeletePVar(playerid, "FoodID");
		}
	}
	return 1;
}
YCMD:payday(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Starts a payday");
		return 1;
	}
	Payday();
	return 1;
}
YCMD:jobids(playerid, params[], help) {
	new msg[128];
	for(new i=0;i<GetNumJobs();i++) {
		format(msg,sizeof(msg),"{FFFFFF}Job ID: {FF0000}%d %s",i,JobPickups[i][EJobName]);
		SendClientMessage(playerid, COLOR_GREY, msg);
	}
	return 1;
}
YCMD:refill(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for refilling your car");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mechanic) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
		return 1;
	}
	new user, price;
	new time = GetPVarInt(playerid, "MechanicCooldown");
	new timenow = gettime();
	new msg[128];
	if(MECH_OFFER_COOLDOWN-(timenow-time) > 0) {
		format(msg, sizeof(msg), "You must wait %d seconds before making another offer",MECH_OFFER_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(GetPVarInt(playerid, "GasCans") < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have any gas cans!");
		HintMessage(playerid, COLOR_CUSTOMGOLD, "You can buy one at any 24/7");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>d", user, price)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(!IsPlayerInAnyVehicle(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in a vehicle!");
			return 1;
		}
		if(price < 1 || price > 999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Price");
			return 1;
		}
		SetPVarInt(playerid, "MechanicCooldown", gettime());
		sendRefillOffer(playerid, user, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /refill [playerid/name] [price]");
	}
	return 1;
}
getVehRepairCost(carid) {
	new Float:health;
	GetVehicleHealth(carid,health);
	return floatround((1000-health)*2);
}
YCMD:repair(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for refilling your car");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mechanic) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
		return 1;
	}
	new user, price, engine, lights, alarm, doors, bonnet, boot, objective;
	new time = GetPVarInt(playerid, "MechanicCooldown");
	new timenow = gettime();
	new msg[128];
	if(MECH_OFFER_COOLDOWN-(timenow-time) > 0) {
		format(msg, sizeof(msg), "You must wait %d seconds before making another offer",MECH_OFFER_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>D(1500)", user, price)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(!IsPlayerInAnyVehicle(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in a vehicle!");
			return 1;
		}
		new rprice = getVehRepairCost(GetPlayerVehicleID(user));
		if(GetMoneyEx(playerid) < rprice) {
			format(msg, sizeof(msg), "You must have $%d to do this!",rprice);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		if(price < 500 || price > 10000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Price. The Price cannot either be below $500 or higher than $10000.");
			return 1;
		}
		new carid = GetPlayerVehicleID(user);
		GetVehicleParamsEx(carid, engine, lights, alarm, doors, bonnet, boot, objective);
		
		if(engine != 0 && IsACar(GetVehicleModel(carid))) {
			SendClientMessage(playerid, X11_TOMATO_2, "The vehicle engine must be off in order to repair it!");
			return 1;
		}
		SetPVarInt(playerid, "MechanicCooldown", gettime());
		sendRepairOffer(playerid, user, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /repair [playerid/name] [price]");
	}
	return 1;
}

YCMD:tow(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	new model = GetVehicleModel(carid);
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Tow a vehicle");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a car");
		return 1;
	}
	if(model != 525) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a tow truck!");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mechanic && !(IsAnLEO(playerid) || !IsOnDuty(playerid))) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
		return 1;
	} else {
		if(getJobLevel(playerid) < 3) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a level 3 mechanic!");
			return 1;
		}
	}
	new car = GetClosestVehicle(playerid);
	if(DistanceBetweenPlayerAndVeh(playerid, car) < 7.0 && GetVehicleModel(car) >= 400)
	{
		if(IsTrailerAttachedToVehicle(carid))
		{
			DetachTrailerFromVehicle(carid);
			SendClientMessage(playerid,COLOR_CUSTOMGOLD,"* Car untowed!");
		} else {
			AttachTrailerToVehicle(car, carid);
			SendClientMessage(playerid,COLOR_CUSTOMGOLD,"* Car towed!");
		}
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "No vehicle is close enough!");
	}
	return 1;
}
YCMD:tunecolour(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a vehicles colour");
		return 1;
	}
	new c1, c2;
	new job = GetPVarInt(playerid, "Job");
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_VehicleAdmin) {
		if(job != EJobType_Mechanic) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
			return 1;
		}
	}
	if(!sscanf(params, "dd", c1, c2)) {
		if(!IsPlayerInAnyVehicle(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
			return 1;
		}
		if(~aflags & EAdminFlags_VehicleAdmin) {
			if(GetMoneyEx(playerid) < 150) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have $150!");
				return 1;
			}
			if(getJobLevel(playerid) < 2) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a level 2 mechanic!");
				return 1;
			}
			GiveMoneyEx(playerid, -150);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "* The respray costed you $150");			
		}
		new carid = GetPlayerVehicleID(playerid);
		setVehicleColour(carid, c1, c2);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /tunecolour [colour1] [colour2]");
		SendClientMessage(playerid, X11_WHITE, "To see colour IDs go here: http://wiki.sa-mp.com/wiki/Color_ID");
	}
	return 1;
}
YCMD:tunepaintjob(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a vehicles colour");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_VehicleAdmin) {
		if(job != EJobType_Mechanic) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
			return 1;
		}
	}
	new paintjob;
	if(!sscanf(params, "d", paintjob)) {
		if(!IsPlayerInAnyVehicle(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
			return 1;
		}
		if(~aflags & EAdminFlags_VehicleAdmin) {
			if(GetMoneyEx(playerid) < 1000) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have $1000!");
				return 1;
			}
			if(getJobLevel(playerid) < 3) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a level 3 mechanic!");
				return 1;
			}
			GiveMoneyEx(playerid, -1000);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "* The paintjob costed you $1000");
		}
		new carid = GetPlayerVehicleID(playerid);
		setVehiclePaintjob(carid,  paintjob);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /tunecolour [colour1] [colour2]");
		SendClientMessage(playerid, X11_WHITE, "To see colour IDs go here: http://wiki.sa-mp.com/wiki/Color_ID");
	}
	return 1;
}
YCMD:tunecar(playerid, params[], help) {
	new job = GetPVarInt(playerid, "Job");
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_VehicleAdmin) {
		if(job != EJobType_Mechanic) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
			return 1;
		}
	}
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Used for modifying a vehicle");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	if(~aflags & EAdminFlags_VehicleAdmin) {
		if(getJobLevel(playerid) < 4) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a level 4 mechanic!");
			return 1;
		}
	}
	showTuneCarMenu(playerid);
	return 1;
}
showTuneCarMenu(playerid){ 
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(BuyableComponents);i++) {
		format(tempstr,sizeof(tempstr),"%s $%s\n",BuyableComponents[i][ECompInfoDispName],getNumberString(BuyableComponents[i][ECompPrice]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EJobs_TuneMenu, DIALOG_STYLE_LIST, "Tune Menu", dialogstr, "Buy", "Cancel");
}
sendRefillOffer(playerid, target, price) {
	new msg[128];
	format(msg, sizeof(msg), "{FFFFFF}%s wants to refill your vehicle for {FF0000}$%s",GetPlayerNameEx(playerid, ENameType_RPName), getNumberString(price));
	SetPVarInt(target, "RefillPrice",price);
	SetPVarInt(target, "RefillMech", playerid);
	ShowPlayerDialog(target, EJobs_RefillOffer, DIALOG_STYLE_MSGBOX, "{00BF00}Refill Offer", msg, "Accept", "Reject");
}
sendRepairOffer(playerid, target, price) {
	new msg[128];
	format(msg, sizeof(msg), "{FFFFFF}%s wants to repair your vehicle for {FF0000}$%s",GetPlayerNameEx(playerid, ENameType_RPName), getNumberString(price));
	SetPVarInt(target, "RepairPrice",price);
	SetPVarInt(target, "RepairMech", playerid);
	ShowPlayerDialog(target, EJobs_RepairOffer, DIALOG_STYLE_MSGBOX, "{00BFFF}Repair Offer", msg, "Accept", "Reject");
}
YCMD:jobspots(playerid, params[], help) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(JobPickups);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",JobPickups[i][EJobName]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EJobs_SpotMenu, DIALOG_STYLE_LIST, "{00BFFF}Job Spots", dialogstr, "Find", "Cancel");
	return 1;
}
YCMD:jobcar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Spawns a job car");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job < 0) {
		SendClientMessage(playerid, X11_RED3, "You must have a job to spawn a job car!");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, Float:Angle;
	new carid;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, Angle);
	if(IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_WHITE, "You must not be in a car for this.");
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 50.0, JobPickups[job][EJobSpotX],JobPickups[job][EJobSpotY],JobPickups[job][EJobSpotZ])) {
		SendClientMessage(playerid, X11_WHITE, "You must be near the job spot in order to spawn this. Do /jobspots to see all the job spots");
		return 1;
	}
	if(GetPVarType(playerid, "JobCar") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have a job car already.");
		return 1;
	}
	switch(job) {
		case EJobType_TrashCleaner: { //trash job
			carid = CreateJobCar(574, 1, 1, X, Y, Z, Angle, "", job);
			PutPlayerInVehicleEx(playerid, carid, 0);
		}
		case EJobType_TaxiDriver: {
			carid = CreateJobCar(420, 6, 6, X, Y, Z, Angle, "", job);
			PutPlayerInVehicleEx(playerid, carid, 0);
		}
		case EJobType_Mechanic: {
			carid = CreateJobCar(525, 1, 0, X, Y, Z, Angle, "", job);
			PutPlayerInVehicleEx(playerid, carid, 0);
		}
		case EJobType_Trucker: {
			carid = CreateJobCar(499, -1, -1, X, Y, Z, Angle, "", job);
			PutPlayerInVehicleEx(playerid, carid, 0);
		}
		default: {
			SendClientMessage(playerid, X11_RED3, "This job doesn't have a job car.");
		}		
	}
	if(carid != 0) {
		SetPVarInt(playerid, "JobCar", carid);
	}
	return 1;
}
YCMD:quitjob(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Quits your job");
		return 1;
	}
	new msg[128];
	new remaining = CanTakeJob(playerid);
	if(remaining != 0) {
			format(msg, sizeof(msg), "You must wait {FF6347}%d{FFFFFF} minute(s) for your job contract to finish!", remaining/60);
			SendClientMessage(playerid, X11_WHITE, msg);
			return 1;
	}
	SetPlayerJob(playerid, -1, 1);	
	SendClientMessage(playerid, X11_LIGHTBLUE, "* You fulfilled your time contract and quit your job.");
	return 1;
}

canAffordGun(playerid, gunid) { 
	new amats = GetPVarInt(playerid,"MatsA");
	new bmats = GetPVarInt(playerid,"MatsB");
	new cmats = GetPVarInt(playerid,"MatsC");
	for(new i=0;i<sizeof(GunJobPrices);i++) {
		if(GunJobPrices[i][EGunJobWeaponID] == gunid) {
			if(GunJobPrices[i][EGunJobAMats] > amats) return 0;
			if(GunJobPrices[i][EGunJobBMats] > bmats) return 0;
			if(GunJobPrices[i][EGunJobCMats] > cmats) return 0;
		}
	}
	return 1;
}
canArmsLevelUp(playerid, gunid) { 
	new index = findJobSkillIndex(EJobType_Arms_Dealer);
	for(new i=0;i<sizeof(GunJobPrices);i++) {
		if(GunJobPrices[i][EGunJobWeaponID] == gunid) {
			if(getWeaponClassType(playerid, gunid) == _:EWeaponType_Melee) {
				if(getSkillLevel(playerid, index) > 1) {
					return 0;
				} else {
					if(GunJobPrices[i][EGunJobLevelUp] == 0) { return 0; } else { return 1; }
				}
			} else {
				return 1;
			}
		}
	}
	return 1;
}
hasGunLevel(playerid, gunid) {
	new index = findJobSkillIndex(EJobType_Arms_Dealer);
	new glevel;
	if(index != -1) {
		glevel = getSkillLevel(playerid, index);
	}
	for(new i=0;i<sizeof(GunJobPrices);i++) {
		if(GunJobPrices[i][EGunJobWeaponID] == gunid) {
			if(glevel >= GunJobPrices[i][EGunJobLevel]) return 1;
		}
	}	
	return 0;
}
removeGunMaterials(playerid, gunid) {
	new amats = GetPVarInt(playerid,"MatsA");
	new bmats = GetPVarInt(playerid,"MatsB");
	new cmats = GetPVarInt(playerid,"MatsC");
	for(new i=0;i<sizeof(GunJobPrices);i++) {
		if(GunJobPrices[i][EGunJobWeaponID] == gunid) {
			amats -= GunJobPrices[i][EGunJobAMats];
			bmats -= GunJobPrices[i][EGunJobBMats];
			cmats -= GunJobPrices[i][EGunJobCMats];
		}
	}
	SetPVarInt(playerid, "MatsA", amats);
	SetPVarInt(playerid, "MatsB", bmats);
	SetPVarInt(playerid, "MatsC", cmats);
	return 1;
}
giveGunMaterials(playerid, gunid) {
	new amats = GetPVarInt(playerid,"MatsA");
	new bmats = GetPVarInt(playerid,"MatsB");
	new cmats = GetPVarInt(playerid,"MatsC");
	for(new i=0;i<sizeof(GunJobPrices);i++) {
		if(GunJobPrices[i][EGunJobWeaponID] == gunid) {
			amats += GunJobPrices[i][EGunJobAMats];
			bmats += GunJobPrices[i][EGunJobBMats];
			cmats += GunJobPrices[i][EGunJobCMats];
		}
	}
	SetPVarInt(playerid, "MatsA", amats);
	SetPVarInt(playerid, "MatsB", bmats);
	SetPVarInt(playerid, "MatsC", cmats);
	return 1;
}
YCMD:sellgun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offer a player a weapon to sell");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Arms_Dealer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Gun Dealer");
		return 1;
	}
	SendClientMessage(playerid, X11_RED3, "This command has been removed, go to the weapon factory at Ocean Docks to order weapons.");
	SendClientMessage(playerid, X11_RED3, "You can use '/give' to give another player a weapon.");
	return 1;
}
/*
YCMD:sellgun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offer a player a weapon to sell");
		return 1;
	}
	new playa, gun[32], price;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Arms_Dealer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Gun Dealer");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>s[32]d", playa, gun, price)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Error: User not found");
			return 1;
		}
		if(playa == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot sell guns yourself, use /makegun instead.");
			return 1;
		}
		new gunid = -1;
		for(new i=0;i<sizeof(GunJobPrices);i++) {
			if(!strcmp(GunJobPrices[i][EGunJobName], gun)) {
				gunid = GunJobPrices[i][EGunJobWeaponID];
				break;
			}
		}
		if(gunid == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid weapon name");
			return 1;
		}
		if(price < 1 || price >= 10000000) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Price");
			return 1;
		}
		if(!canAffordGun(playerid, gunid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough materials for this weapon.");
			return 1;
		}
		if(!hasGunLevel(playerid, gunid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have the gun level required to make this.");
			return 1;
		}
		new Float:X, Float:Y, Float:Z, vw;
		GetPlayerPos(playa, X, Y, Z);
		vw = GetPlayerVirtualWorld(playa);
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z) || GetPlayerVirtualWorld(playerid) != vw) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not near this person!");
			return 1;
		}
		new curgun,curammo;
		GetPlayerWeaponDataEx(playa, GetWeaponSlot(gunid), curgun, curammo);
		if(curgun != 0) {
			SendClientMessage(playerid, X11_WHITE, "This player is holding a weapon in this slot.");
			return 1;
		}
		new time = canUseGunCommand(playerid);
		if(time != 0) {
			new msg[128];
			format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		SetPVarInt(playerid, "LastGunCommand", gettime());
		sendGunOffer(playerid, playa, gunid, price);
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sellgun [playerid/name] [gunname] [price]");
		SendClientMessage(playerid, X11_WHITE, "To create a gun for yourself use /makegun");
		new msg[128];
		for(new i=0;i<sizeof(GunJobPrices);i++) {
			format(msg, sizeof(msg), "%d. Name:[%s] MatsA:[%d] MatsB:[%d] MatsC:[%d] Level:[%d]",i+1,GunJobPrices[i][EGunJobName],GunJobPrices[i][EGunJobAMats],GunJobPrices[i][EGunJobBMats],GunJobPrices[i][EGunJobCMats],GunJobPrices[i][EGunJobLevel]);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	}
	return 1;
}
*/
YCMD:sellfood(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offer a player an ice cream");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Error: User not found");
			return 1;
		}
		if(playa == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot sell food to yourself.");
			return 1;
		}
		new Float:X, Float:Y, Float:Z, vw;
		GetPlayerPos(playa, X, Y, Z);
		vw = GetPlayerVirtualWorld(playa);
		if(!IsPlayerInRangeOfPoint(playerid, 10.0, X, Y, Z) || GetPlayerVirtualWorld(playerid) != vw) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not near this person!");
			return 1;
		}
		new carid, model;
		if(!IsPlayerInAnyVehicle(playerid)) {
			carid = GetClosestVehicle(playerid);
		} else {
			carid = GetPlayerVehicleID(playerid);
		}
		model = GetVehicleModel(carid);
		if(!IsAMrWhoopee(model) && !IsAHotDog(model)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must at least have an Ice Cream or a Hot Dog vending vehicle!");
			return 1;
		}
		if(VehicleInfo[carid][EVOwner] != playerid && VehicleInfo[carid][EVType] != EVehicleType_Owned) {
			SendClientMessage(playerid, X11_TOMATO_2, "You're not the owner of that vehicle, therefore you can't sell any Ice Cream!");
			return 1;
		}
		SetPVarInt(playerid, "mobileSellFoodTo", playa);
		new type;
		if(IsAMrWhoopee(model)) {
			type = TYPE_ICECREAM;
			SetPVarInt(playerid, "mobileFoodType", type);
		} else if(IsAHotDog(model)) {
			type = TYPE_HOTDOG;
			SetPVarInt(playerid, "mobileFoodType", type);
		}
		showFoodOfferMenu(playerid, type);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sellfood [playerid/name]");
	}
	return 1;
}
showFoodOfferMenu(playerid, type){ 
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(SellableFoodInfo);i++) {
		if(SellableFoodInfo[i][EFoodType] == type) {
			format(tempstr,sizeof(tempstr),"%s $%s\n",SellableFoodInfo[i][EFoodInfoDispName],getNumberString(SellableFoodInfo[i][EFoodPrice]));
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EJobs_FoodSellOptions, DIALOG_STYLE_LIST, "Food Sell Menu", dialogstr, "Sell", "Cancel");
}
YCMD:selldrugs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offers a player drugs");
		return 1;
	}
	new playa, name[32], amount, price;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Drug_Dealer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Drug Dealer");
		return 1;
	}
	new time = canUseGunCommand(playerid);
	if(time != 0) {
		new msg[128];
		format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>s[32]dd",playa, name, amount, price)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(playa == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot sell drugs to yourself!");
			return 1;
		}
		if(price < 1 || price >= 10000000) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Price");
			return 1;
		}
		if(amount < 1 || amount > 9999) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Amount!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z, vw;
		GetPlayerPos(playa, X, Y, Z);
		vw = GetPlayerVirtualWorld(playa);
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z) || GetPlayerVirtualWorld(playerid) != vw) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not near this person!");
			return 1;
		}
		new type = tolower(name[0]);
		new pvarname[32];
		switch(type) {
			case 'p': {
				pvarname = "Pot";
			}
			case 'c': {
				pvarname = "Coke";
			}
			case 'm': {
				pvarname = "Meth";
			}
			default: {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Drug Type!");
				return 1;
			}
		}
		SetPVarInt(playerid, "LastDrugCommand", gettime());
		sendDrugOffer(playerid, playa, pvarname, amount, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /selldrugs [playerid/name] [drug] [amount] [price]");
		SendClientMessage(playerid, X11_LIGHTBLUE, "Drug Types: Pot, Coke, Meth");
	}
	return 1;
}
YCMD:makegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offer a player a weapon to sell");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Arms_Dealer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Gun Dealer");
		return 1;
	}
	SendClientMessage(playerid, X11_RED3, "This command has been removed, go to the weapon factory at Ocean Docks to order weapons.");
	SendClientMessage(playerid, X11_RED3, "You can use '/give' to give another player a weapon.");
	return 1;
}
/*
YCMD:makegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offer a player a weapon to sell");
		return 1;
	}
	new gun[32];
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Arms_Dealer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Gun Dealer");
		return 1;
	}
	if(!sscanf(params, "s[32]", gun)) {
		new gunid = -1;
		for(new i=0;i<sizeof(GunJobPrices);i++) {
			if(!strcmp(GunJobPrices[i][EGunJobName], gun)) {
				gunid = GunJobPrices[i][EGunJobWeaponID];
				break;
			}
		}
		if(gunid == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Weapon");
			return 1;
		}
		if(!canAffordGun(playerid, gunid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough materials for this weapon.");
			return 1;
		}
		if(!hasGunLevel(playerid, gunid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have the gun level required to make this.");
			return 1;
		}
		new string[128];
		new weaponname[32];
		new time = canUseGunCommand(playerid);
		if(time != 0) {
			new msg[128];
			format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		SetPVarInt(playerid, "LastGunCommand", gettime());
		GetWeaponNameEx(gunid, weaponname, sizeof(weaponname));
		format(string, sizeof(string), "* %s makes a %s and hands it to %sself.", GetPlayerNameEx(playerid, ENameType_RPName), weaponname, getThirdPersonPersonalPronoun(playerid));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		removeGunMaterials(playerid, gunid);
		GivePlayerWeaponEx(playerid, gunid, -1);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makegun [gunname]");
		new msg[128];
		for(new i=0;i<sizeof(GunJobPrices);i++) {
			format(msg, sizeof(msg), "%d. Name:[%s] MatsA:[%d] MatsB:[%d] MatsC:[%d] Level:[%d]",i+1,GunJobPrices[i][EGunJobName],GunJobPrices[i][EGunJobAMats],GunJobPrices[i][EGunJobBMats],GunJobPrices[i][EGunJobCMats],GunJobPrices[i][EGunJobLevel]);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	}
	return 1;
}
*/
YCMD:skills(playerid, params[], help) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shows your current job skill");
		return 1;
	}
	for(new i=0;i<sizeof(Skills);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",Skills[i][ESkill_Name]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EJobs_SkillsDialog, DIALOG_STYLE_LIST, "Skills",dialogstr,"Select", "Cancel");
	return 1;
}
YCMD:find(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mercenary command for finding a player");
		return 1;
	}
	new playa;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mercenary) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Mercenary");
		return 1;
	}
	new time = canUseMercCommand(playerid);
	if(time != 0) {
		new msg[128];
		format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPlayerState(playa) == PLAYER_STATE_SPECTATING || GetPlayerVirtualWorld(playa) != 0 || GetPlayerInterior(playa) != 0 || isTinFoiled(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Could not locate player.");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(playa, X, Y, Z);
		SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
		SetTimerEx("RemoveCheckpoint", getMercCheckpointTimeout(playerid), false, "d", playerid);
		SetPVarInt(playerid, "LastMercCommand", gettime());
		increaseJobPoints(playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /find [playerid/name]");
	}
	return 1;
}
YCMD:free(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lawyer command for setting a player out on bail");
		return 1;
	}
	new playa, price;
	new job = GetPVarInt(playerid, "Job");
	new Float:X, Float:Y, Float:Z;
	if(job != EJobType_Lawyer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Lawyer");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>d", playa, price)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(playerid == playa) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't free yourself!");
			return 1;
		}
		if(price < 500 || price > 999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "The price can't be below 500 or above 999999!");
			return 1;
		}
		GetPlayerPos(playa, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(GetPVarType(playa, "ReleaseTime") != PLAYER_VARTYPE_NONE) {
			sendLawyerOffer(playerid, playa, price);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in jail.");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /free [playerid/name] [price]");
	}
	return 1;
}
YCMD:tinfoil(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lawyer command for tinfoiling a player");
		return 1;
	}
	new playa;
	new job = GetPVarInt(playerid, "Job");
	new Float:X, Float:Y, Float:Z;
	if(job != EJobType_Lawyer) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Lawyer");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(playerid == playa) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't hide yourself!");
			return 1;
		}
		GetPlayerPos(playa, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(isTinFoiled(playa)) {
			toggleTinFoil(playa);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "This person is not tinfoiled anymore!");
			SendClientMessage(playa, COLOR_LIGHTBLUE, "You aren't being tinfoiled any longer!");
		} else {
			toggleTinFoil(playa);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "This person is being tinfoiled by you by now!");
			SendClientMessage(playa, COLOR_LIGHTBLUE, "You are being tinfoiled by a lawyer by now!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /tinfoil [playerid/name]");
	}
	return 1;
}
YCMD:sellvest(playerid, params[], help) {
	new user, price;
	new Float:amount;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mercenary) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Mercenary");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>dF(98.0)", user, price, amount)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(user == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't sell a vest to yourself!");
			return 1;
		}
		new time = canUseMercCommand(playerid);
		if(time != 0) {
			new msg[128];
			format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		if(price < 500 || price > 999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "The price can't be below 500 or above 999999!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(amount > 98.0 && amount <= 100.0) {
			amount = 98.0;
		}
		if(amount < 1.0 || amount > 98.0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vest size");
			return 1;
		}
		sendVestOffer(playerid, user, amount, price);
		SetPVarInt(playerid, "LastMercCommand", gettime());
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sellvest [playeird/name] [price] (amount)");
	}
	return 1;
}
toggleTinFoil(playerid) {
	if(isTinFoiled(playerid)) {
		DeletePVar(playerid, "TinFoil");
	} else {
		SetPVarInt(playerid, "TinFoil", 1);
	}
}
isTinFoiled(playerid) {
	if(GetPVarType(playerid, "TinFoil") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	return 0;
}
sendVestOffer(playerid, user, Float:amount, price) {
	new msg[128];
	new matCPrice = floatround(amount*40);
	new totalmats = GetPVarInt(playerid, "MatsC");
	if(matCPrice > totalmats) {
		format(msg, sizeof(msg), "You need %d more Mats C to make a vest!",matCPrice-totalmats);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	SetPVarFloat(user, "VestAmount", amount);
	SetPVarInt(user, "VestSeller", playerid);
	SetPVarInt(user, "VestPrice", price);
	SetPVarInt(playerid, "VestOfferer", user);
	SetPVarInt(playerid, "VestMatsPrice", matCPrice);
	format(msg, sizeof(msg), "* %s wants to sell you a amoured vest(%.3f armour) for $%s",GetPlayerNameEx(playerid, ENameType_RPName), amount, getNumberString(price));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Vest Offer sent");
	ShowPlayerDialog(user, EJobs_SellVest, DIALOG_STYLE_MSGBOX, "Vest Offer", msg, "Accept", "Reject");
	return 1;
}
sendLawyerOffer(playerid, user, price) {
	new msg[128];
	if(playerid == user) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot set yourself out on bail, call a lawyer. '/service lawyer'");
		return 1;
	}
	if(GetPVarInt(user, "Bail") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "This person doesn't have a bail, you cannot set them free.");
		return 1;
	}
	new bail = GetPVarInt(user, "Bail");
	new totalprice = bail+price;
	SetPVarInt(user, "LawyerID", playerid);
	SetPVarInt(user, "LawyerPrice", totalprice);
	SetPVarInt(playerid, "FreedomOfferer", user);
	format(msg, sizeof(msg), "* %s wants to set you free on bail for $%s",GetPlayerNameEx(playerid, ENameType_RPName), getNumberString(totalprice));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Lawyer Offer sent");
	ShowPlayerDialog(user, EJobs_LawyerFree, DIALOG_STYLE_MSGBOX, "Lawyer Offer", msg, "Accept", "Reject");
	return 1;
}
/*
canUseDrugCommand(playerid) {
	new lastcmd = GetPVarInt(playerid, "LastDrugCommand");
	new coolout = getDrugCooldownTime(playerid);
	new time = gettime();
	if(lastcmd+coolout > time) {
		return lastcmd+coolout-time;
	}
	return 0;
}
getDrugCooldownTime(playerid) {
	new coolout =  15;
	new level = getJobLevel(playerid);
	switch(level) {
		case 1:  {
			coolout += 165;
		}
		case 2: {
			coolout += 105;
		}
		case 3: {
			coolout += 95;
		}
		case 4: {
			coolout += 55;
		}
	}
	return coolout;
}
*/
canUseMercCommand(playerid) {
	new lastcmd = GetPVarInt(playerid, "LastMercCommand");
	new coolout = getMercCooldownTime(playerid);
	new time = gettime();
	if(lastcmd+coolout > time) {
		return lastcmd+coolout-time;
	}
	return 0;
}
getMercCooldownTime(playerid) {
	new coolout =  15;
	new level = getJobLevel(playerid);
	switch(level) {
		case 1:  {
			coolout += 165;
		}
		case 2: {
			coolout += 105;
		}
		case 3: {
			coolout += 95;
		}
		case 4: {
			coolout += 55;
		}
	}
	return coolout;
}

canUseGunCommand(playerid) {
	new lastcmd = GetPVarInt(playerid, "LastGunCommand");
	new coolout = getGunCooldownTime(playerid);
	new time = gettime();
	if(lastcmd+coolout > time) {
		return lastcmd+coolout-time;
	}
	return 0;
}
getGunCooldownTime(playerid) {
	new coolout =  15;
	new level = getJobLevel(playerid);
	switch(level) {
		case 1:  {
			coolout += 165;
		}
		case 2: {
			coolout += 105;
		}
		case 3: {
			coolout += 95;
		}
		case 4: {
			coolout += 55;
		}
	}
	return coolout;
}
getMercCheckpointTimeout(playerid) {
	new level = getJobLevel(playerid);
	new time = 60000;
	switch(level) {
		case 1:  {
			time -= 45000;
		}
		case 2: {
			time -= 35000;
		}
		case 3: {
			time -= 25000;
		}
		case 4: {
			time -= 15000;
		}
	}
	return time;
}
getMaxPizzaDeliveryTime(playerid, index) {
	new Float: pDistance = GetPlayerDistanceFromPoint(playerid, Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseZ]);
	return floatround(pDistance/10, floatround_round);
}
initiatePizzaDelivery(playerid) {
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_PizzaMan) {
		return 1;
	}
	new string[128];
	new houseid = getClosestPizzaDelivery(playerid);
	new deliverytime = getMaxPizzaDeliveryTime(playerid, houseid);
	if(houseid != -1) {
		SetPlayerCheckpoint(playerid, Houses[houseid][EHouseX],Houses[houseid][EHouseY],Houses[houseid][EHouseZ],5.0);
		SetPVarInt(playerid, "IsOnPizzaRoute", 1);
		SetPVarFloat(playerid, "PizzaDeliveryTime", deliverytime);
		format(string, sizeof(string), "You have %d seconds to deliver the pizza to the destination.", deliverytime);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
	} else {
		initiatePizzaDelivery(playerid); //try again
	}
	return 1;
}
getClosestPizzaDelivery(playerid) {
	new Float:pDistance;
	new index;
	index = getStandingHouse(playerid, RandomEx(200,1000));
	pDistance = GetPlayerDistanceFromPoint(playerid, Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseZ]);
	SetPVarFloat(playerid, "PizzaTravelDistance", pDistance);
	return index;
}
onPizzaDeliverySuccess(playerid) {
	if(GetPVarType(playerid, "PizzaDeliveryTime") != PLAYER_VARTYPE_NONE) {
		//new Float:totaldist = GetPVarFloat(playerid, "PizzaTravelDistance");
		new Float:takentime = GetPVarFloat(playerid, "PizzaDeliveryTime");
		new pay;
		new string[128];
		DeletePVar(playerid, "IsOnPizzaRoute");
		DeletePVar(playerid, "PizzaDeliveryTime");
		DeletePVar(playerid, "PizzaTravelDistance");
		SetPlayerPos(playerid, 2112.063964, -1778.667968, 13.390113);
		PlayerPlaySound(playerid, 1056, 0.0, 0.0, 0.0);
		if(takentime >= 10) {
			takentime = takentime*3;
			pay = floatround(takentime*2, floatround_round);
			format(string, sizeof(string), "Pizza delivered! You earned $%s from the delivery! Tip: $%s", getNumberString(pay), getNumberString(floatround(takentime)));
		} else {
			pay = floatround(takentime*3, floatround_round);
			format(string, sizeof(string), "Pizza delivered! You earned $%s from the delivery!", getNumberString(pay));
		}
		/*
		new paycheck = GetPVarInt(playerid, "Payday");
		SetPVarInt(playerid, "Payday", paycheck+pay);
		*/
		GiveMoneyEx(playerid, pay);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
		DisablePlayerCheckpoint(playerid);
		new vehicleid = GetPlayerVehicleID(playerid);
		SetVehicleToRespawn(vehicleid);
	}
	return 1;
}
onPizzaDeliveryFail(playerid) {
	if(GetPVarType(playerid, "PizzaDeliveryTime") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "IsOnPizzaRoute");
		DeletePVar(playerid, "PizzaDeliveryTime");
		new vehicleid = GetPVarInt(playerid, "LastCar");
		SetVehicleToRespawn(vehicleid);
		SetPlayerPos(playerid, 2112.063964, -1778.667968, 13.390113);
		SendClientMessage(playerid, X11_TOMATO_2, "You failed to deliver the pizza!");
		DisablePlayerCheckpoint(playerid);
	}
	return 1;
}
doPizzaTime(playerid) {
	new string[24];
	new Float:pizzadeliverytime;
	if(GetPVarType(playerid, "PizzaDeliveryTime") != PLAYER_VARTYPE_NONE) {
		pizzadeliverytime = GetPVarFloat(playerid, "PizzaDeliveryTime");
		format(string, sizeof(string), "%d", floatround(pizzadeliverytime-1, floatround_round));
		GameTextForPlayer(playerid, string, 1000, 6);
		SetPVarFloat(playerid, "PizzaDeliveryTime", pizzadeliverytime-1);
		if(pizzadeliverytime < 0) {
			onPizzaDeliveryFail(playerid);
		}
	}
}
checkJobsTime() {
	OnPlayerWasteThinkJob();
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Job") == EJobType_PizzaMan) {
				doPizzaTime(i);
			}
		}
	}
}
jobOnPlayerEnterCheckpoint(playerid) {
	if(GetPVarType(playerid, "CurrentTruckerRoute") != PLAYER_VARTYPE_NONE) {
		new p = GetPVarInt(playerid, "CurrentTruckerRoute");
		if(!IsPlayerInRangeOfPoint(playerid, 15.0, TruckerRoutes[p][EIRX], TruckerRoutes[p][EIRY], TruckerRoutes[p][EIRZ])) {
			//SendClientMessage(playerid, X11_TOMATO_2, "You must be in the Trucker point!");
			ShowScriptMessage(playerid, "You ~r~must ~w~be in the Trucker point!", 5000);
			SetPlayerCheckpoint(playerid, TruckerRoutes[p][EIRX], TruckerRoutes[p][EIRY], TruckerRoutes[p][EIRZ],5.0);
			return 1;
		}
		nextTruckerRoute(playerid);
	}
	if(GetPVarType(playerid, "IsOnPizzaRoute") != PLAYER_VARTYPE_NONE) {
		onPizzaDeliverySuccess(playerid);
	}
	return 1;
}
jobOnPlayerEnterDynamicCP(playerid, checkpointid) {
	if(GetPVarType(playerid, "TrashRoute") != PLAYER_VARTYPE_NONE) {
		wasteOnManageTrashJob(playerid, checkpointid);
	}
	return 1; 
}
//called when a player somehow aborts a job
onJobAbort(playerid) {
	new job = GetPVarInt(playerid, "Job");
	if(job != -1) {
		switch(job) {
			case EJobType_TaxiDriver: {
				if(GetPVarType(playerid, "TaxiPassenger") != PLAYER_VARTYPE_NONE) {
					new passenger = GetPVarInt(playerid, "TaxiPassenger");
					if(IsPlayerConnectEx(passenger)) {
						onTaxiPassengerAbort(passenger);
						DeletePVar(passenger, "TaxiDriver");
					}
					DeletePVar(playerid, "TaxiPassenger");
				}
			}
			case EJobType_Trucker: {
				endTruckerRoute(playerid);
			}
			case EJobType_PizzaMan: {
				onPizzaDeliveryFail(playerid);
			}
		}
	}
}
onTaxiPassengerAbort(playerid) {
	if(GetPVarType(playerid, "TaxiPassenger") != PLAYER_VARTYPE_NONE) {
		new driver = GetPVarInt(playerid, "TaxiDriver");
		SendClientMessage(driver, X11_RED, "Taxi round ended.");
		SendClientMessage(playerid, X11_RED,"Taxi charges aborted");
		DeletePVar(playerid, "TaxiPassenger");
		DeletePVar(playerid, "TaxiDriver");
	}
}
/*
startRoute(playerid, index) {
	new msg[128];
	new rindex = GetStartingRouteIndex(index);
	if(rindex == -1) {
		SendClientMessage(playerid, X11_RED3, "Script error: Failed to start route, report this to an admin");
		return 0;
	}
	SetPVarInt(playerid, "TrashRoute",index);
	SetPVarInt(playerid, "TrashRouteIndex",rindex);
	SetPlayerCheckpoint(playerid, TrashRoutePaths[rindex][ETrashPointX], TrashRoutePaths[rindex][ETrashPointY], TrashRoutePaths[rindex][ETrashPointZ], 5.0);
	format(msg, sizeof(msg), "~g~Next Stop: ~r~%s",TrashRoutePaths[rindex][ETrashPointName]);
	PlayerPlaySound(playerid, 1056, 0.0, 0.0, 0.0);
	GameTextForPlayer(playerid, msg, 10000, 3);
	return 1;
}
*/
nextTruckerRoute(playerid) {
	new r;
	if(GetPVarType(playerid, "CurrentTruckerRoute") == PLAYER_VARTYPE_NONE) {
		r = random(sizeof(TruckerRoutes));
		SetPlayerCheckpoint(playerid, TruckerRoutes[r][EIRX], TruckerRoutes[r][EIRY], TruckerRoutes[r][EIRZ], 5.0);
		SetPVarInt(playerid, "CurrentTruckerRoute", r);
		SetPVarInt(playerid, "TruckerDeliver", 0);
		ShowScriptMessage(playerid, "Drive to the destination on your mini-map to pickup the items", 8000);
	} else {
		new l = GetPVarInt(playerid, "CurrentTruckerRoute");
		new o = GetPVarInt(playerid, "LastTruckerRoute");
		if(GetPVarInt(playerid, "TruckerDeliver") == 0) {
			SetPVarInt(playerid, "TruckerDeliver", 1);
		} else {
			new Float:distance = GetPointDistance(TruckerRoutes[r][EIRX], TruckerRoutes[r][EIRY], TruckerRoutes[r][EIRZ], TruckerRoutes[o][EIRX], TruckerRoutes[o][EIRY], TruckerRoutes[o][EIRZ]);
			new money = floatround(distance/3);
			money += 100;
			SetPVarInt(playerid, "TruckerDeliver", 0);
			new paycheck = GetPVarInt(playerid, "Payday");
			SetPVarInt(playerid, "Payday", paycheck+money);
			format(query, sizeof(query), "$%d have been added to your check from this delivery!",money);
			ShowScriptMessage(playerid, query, 8000);
		}
		do {
			r = random(sizeof(TruckerRoutes));
		} while(IsPointInRangeOfPoint(TruckerRoutes[r][EIRX], TruckerRoutes[r][EIRY], TruckerRoutes[r][EIRZ],2000.0,TruckerRoutes[l][EIRX], TruckerRoutes[l][EIRY], TruckerRoutes[l][EIRZ]));
		SetPlayerCheckpoint(playerid, TruckerRoutes[r][EIRX], TruckerRoutes[r][EIRY], TruckerRoutes[r][EIRZ], 5.0);
		SetPVarInt(playerid, "CurrentTruckerRoute", r);
		SetPVarInt(playerid, "LastTruckerRoute",l);
	}
	return r;
}
endTruckerRoute(playerid) {
	if(GetPVarType(playerid, "CurrentTruckerRoute") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "CurrentTruckerRoute");
		DeletePVar(playerid, "LastTruckerRoute");
		DeletePVar(playerid, "TruckerDeliver");
		DisablePlayerCheckpoint(playerid);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* The job has been aborted");
	}
}
startTruckerRoute(playerid) {
	nextTruckerRoute(playerid);
}
YCMD:startclean(playerid, params[], help) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new carid = GetPlayerVehicleID(playerid);
	if(carid != 0) {
		if(VehicleInfo[carid][EVType] != EVehicleType_JobCar) {
			//SendClientMessage(playerid, X11_RED3, "You must be in a Trash Cleaner vehicle");
			ShowScriptMessage(playerid, "~r~You ~w~must be in a ~r~Trash Cleaner vehicle~w~.", 5000);
			return 1;
		}
	} else {
		//SendClientMessage(playerid, X11_RED3, "You must be in a Trash Cleaner vehicle, do /jobcar near the vehicle point to spawn a trash car if none are available.");
		ShowScriptMessage(playerid, "You must be in a ~r~Trash Cleaner vehicle~w~, do ~r~/jobcar ~w~near the vehicle point to spawn a trash car if none are available.", 5000);
		return 1;
	}
	if(GetPlayerState(playerid) != PLAYER_STATE_DRIVER){
		//SendClientMessage(playerid, X11_TOMATO_2, "You must be the driver!");
		ShowScriptMessage(playerid, "~r~You must be the driver!", 5000);
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_TrashCleaner) {
		//SendClientMessage(playerid, X11_TOMATO_2,"You must be a trash cleaner");
		ShowScriptMessage(playerid, "You must be a ~r~trash cleaner~w~.", 5000);
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 50.0, JobPickups[job][EJobSpotX],JobPickups[job][EJobSpotY],JobPickups[job][EJobSpotZ])) {
		//SendClientMessage(playerid, X11_WHITE, "You must be near the job spot in order to do this. Do /jobspots to see all the job spots");
		ShowScriptMessage(playerid, "You must be near the ~r~job spot ~w~in order to do this. Do ~r~/jobspots ~w~to see all the job spots.", 5000);
		return 1;
	}
	/*
	for(new i=0;i<sizeof(TrashRoutes);i++) {
		format(tempstr,sizeof(tempstr),"Route: %s Number of stops: %d Pay: $%s\n",TrashRoutes[i][ETrashPathName],GetNumTrashPaths(i), getNumberString(TrashRoutes[i][ETrashPathReward]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EJobs_TrashDialog, DIALOG_STYLE_LIST, "Choose your garbage route",dialogstr,"Select", "Cancel");
	*/
	startClean(playerid);
	return 1;
}
YCMD:fare(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players taxi fare");
		return 1;
	}
	new price;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_TaxiDriver) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a Taxi Driver!");
		return 1;
	}
	new time,timenow;
	time = GetPVarInt(playerid, "FareCooldown");
	timenow = gettime();
	if(JOB_PICKUP_COOLDOWN-(timenow-time) > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait before using this command again!");
		return 1;
	}
	if(!sscanf(params, "d", price)) {
		if(price < 1 || price > 500) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Price!");
			return 1;
		}
		new carid = GetPlayerVehicleID(playerid);
		if(carid != 0) {
			if(VehicleInfo[carid][EVType] != EVehicleType_JobCar) {
				SendClientMessage(playerid, X11_RED3, "You must be in a Taxi Driver vehicle");
				return 1;
			}
		} else {
			SendClientMessage(playerid, X11_RED3, "You must be in a Taxi Driver vehicle, do /jobcar near the vehicle point to spawn a taxi if none are available.");
			return 1;
		}
		if(GetPVarType(playerid, "TaxiFare") == PLAYER_VARTYPE_NONE) {
			new msg[128];
			SetPVarInt(playerid, "TaxiFare", price);
			format(msg, sizeof(msg), "Taxi Driver %s is now on duty for $%s, do /service taxi to call for a taxi",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),getNumberString(price));
			SendClientMessageToAll(TEAM_GROVE_COLOR, msg);
		} else {
			SendClientMessage(playerid, X11_WHITE, "You are now off duty.");
			DeletePVar(playerid, "TaxiFare");
		}
		SetPVarInt(playerid, "FareCooldown", gettime());
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fare [price]");
	}
	return 1;
}
YCMD:tie(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Ties or unties a player");
		return 1;
	}
	new target;
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mercenary) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Mercenary");
		return 1;
	}
	new time = canUseMercCommand(playerid);
	if(time != 0) {
		new msg[128];
		format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(IsPlayerInAnyVehicle(target) == 0 || IsPlayerInAnyVehicle(playerid) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You both must be in a car");
			return 1;
		}
		if(GetPlayerVehicleID(target) != GetPlayerVehicleID(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in the same vehicle!");
			return 1;
		}
		new string[128];
		if(GetPVarType(target,"Tied") == PLAYER_VARTYPE_NONE) {
			format(string, sizeof(string), "* You were tied by %s.", GetPlayerNameEx(playerid, ENameType_RPName));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* You tied %s.", GetPlayerNameEx(target, ENameType_RPName));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			GameTextForPlayer(target, "~r~Tied", 2500, 3);
			TogglePlayerControllableEx(target, 0);
			SetPVarInt(target, "Tied", 1);
		} else {
			format(string, sizeof(string), "* You were untied by %s.", GetPlayerNameEx(playerid, ENameType_RPName));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* You untied %s.", GetPlayerNameEx(target, ENameType_RPName));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			GameTextForPlayer(target, "~g~Untied", 2500, 3);
			TogglePlayerControllableEx(target, 1);
			DeletePVar(target, "Tied");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /(un)tie [playerid/name]");
	}
	return 1;
}
YCMD:frisk(playerid, params[], help) {
	new target;
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 3.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "|__________ Items with %s __________|", GetPlayerNameEx(target, ENameType_RPName));
		SendClientMessage(playerid, COLOR_WHITE, msg);
		if(GetPVarInt(target, "MatsA") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type A Materials");
		}
		if(GetPVarInt(target, "MatsB") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type B Materials");
		}
		if(GetPVarInt(target, "MatsC") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type C Materials");
		}
		if(GetPVarInt(target, "Pot") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Pot");
		}
		if(GetPVarInt(target, "Coke") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Coke");
		}
		if(GetPVarInt(target, "Meth") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Meth");
		}
		if(GetMoneyEx(target) > 0) {
			format(msg, sizeof(msg), "| Money $%s", GetMoneyEx(target) >= 5000 ? getNumberString(5000) : getNumberString(GetMoneyEx(target)));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Money");
		}
		if(HasSpecialItem(target)) {
			format(msg, sizeof(msg), "| %s", GetPlayerCarryingItemName(target));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
		drugsSendStatsMessage(playerid, target);
		new gun, ammo, gunname[32];
		for(new i=0;i<12;i++) {
			GetPlayerWeaponData(target, i, gun, ammo);
			if(gun != 0) {
				GetWeaponNameEx(gun, gunname, sizeof(gunname));
				format(msg, sizeof(msg), "| Weapon: %s",gunname);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
			}
		}
		format(msg, sizeof(msg), "* %s has frisked %s.", GetPlayerNameEx(playerid, ENameType_RPName) ,GetPlayerNameEx(target, ENameType_RPName));
		ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /frisk [playerid/name]");
	}
	return 1;
}
YCMD:trunkplayer(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Puts a player in your trunk");
		return 1;
	}
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mercenary) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be a Mercenary");
		return 1;
	}
	new msg[128];
	new time = canUseMercCommand(playerid);
	if(time != 0) {
		format(msg, sizeof(msg), "You must wait %d seconds before continuing",time);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	new carid = GetClosestVehicle(playerid);
	if(carid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There are no cars!");
		return 1;
	}
	new target;
	if(!sscanf(params,"k<playerLookup>",target)) {
		if(!IsPlayerConnectEx(target) || target == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetVehiclePos(carid, X, Y, Z);
		if(GetPVarType(target, "TrunkedCar") == PLAYER_VARTYPE_NONE && !IsPlayerInRangeOfPoint(target, 10.0, X, Y, Z) ) {
			SendClientMessage(playerid, X11_TOMATO_2, "The player is too far away!");
			return 1;
		}
		GetPlayerPos(target, X, Y, Z);
		if(GetPVarType(target, "TrunkedCar") == PLAYER_VARTYPE_NONE && !IsPlayerInRangeOfPoint(playerid, 10.0, X, Y, Z) && !IsPlayerInAnyVehicle(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;			
		}
		new engine,lights,alarm,doors,bonnet,boot,objective;
		GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
		if(boot == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "The trunk must be opened");
			return 1;
		}							
		if(GetPVarType(target, "TrunkedCar") == PLAYER_VARTYPE_NONE) {
			SetPVarInt(target, "TrunkedCar", carid);			
			saveSpecDetails(target);
			TogglePlayerSpectating(target, 1);
			PlayerSpectateVehicle(target, carid);
			format(msg, sizeof(msg), "* %s throws %s in the vehicles trunk.", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(target, ENameType_RPName));
			SetPVarInt(playerid, "LastMercCommand", gettime());
		} else {
			if(GetPVarInt(target, "TrunkedCar") != carid) {
				SendClientMessage(playerid, X11_TOMATO_2, "This player is not in this car!");
				return 1;
			}
			format(msg, sizeof(msg), "* %s pulls %s out of the vehicles trunk.", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(target, ENameType_RPName));
			DeletePVar(target, "TrunkedCar");
			GetPlayerPos(playerid, X, Y, Z);
			TogglePlayerSpectating(target, 0);
			loadSpecDetails(target, 0);
			SetPlayerPos(target, X, Y, Z);
		}
		ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /(un)trunkplayer [playerid/name]");
	}
	return 1;
}
/*
	Have you finished your job?
*/

CanTakeJob(playerid) {
	new time = gettime();
	new jobtime = GetPVarInt(playerid, "JobTime");
	if(JOB_EXPIRE_TIME-(time-jobtime) > 0) {
		return JOB_EXPIRE_TIME-(time-jobtime);
	}
	return 0;
}
SetPlayerJob(playerid, jobid, skipcontract = 0) {
	new msg[128];
	SetPVarInt(playerid, "Job", jobid);
	if(skipcontract == 0) {
		format(msg, sizeof(msg), "You are now on a %d minutes contract. When it is complete, you can use {FF6347}/quitjob", JOB_EXPIRE_TIME/60);
		SendClientMessage(playerid, X11_WHITE, msg);
		SetPVarInt(playerid, "JobTime",gettime());
	}
	return 1;
}
stock GetJobName(jobid) {
	new name[MAX_JOB_NAME];
	if(jobid < 0 || jobid > sizeof(JobPickups)-1) {
		strmid(name,"None",0, 4, sizeof(name));
	} else {
		strmid(name,JobPickups[jobid][EJobName],0, strlen(JobPickups[jobid][EJobName]), sizeof(name));
	}
	return name;
 }
GetNumJobs() {
	return sizeof(JobPickups);
}
GetNumTrashPaths(index) {
	new count;
	for(new i=0;i<sizeof(TrashRoutePaths);i++) {
		if(TrashRoutePaths[i][ETrashRouteIndex] == index) {
			count++;
		}
	}
	return count;
}
GetStartingRouteIndex(route) {
	for(new i=0;i<sizeof(TrashRoutePaths);i++) {
		if(TrashRoutePaths[i][ETrashRouteIndex] == route) {
			return i;
		}
	}
	return -1;
}
sendFoodOffer(playerid, target, index) {
	new msg[128];
	new foodname[32];
	format(foodname, sizeof(foodname), "%s", SellableFoodInfo[index][EFoodInfoDispName]);
	format(msg, sizeof(msg), "%s wants to sell you a %s for $%s", GetPlayerNameEx(playerid, ENameType_RPName), foodname, getNumberString(SellableFoodInfo[index][EFoodPrice]));
	SetPVarInt(target, "FoodSeller", playerid);
	SetPVarInt(target, "FoodID", index);
	SendClientMessage(playerid, X11_WHITE, "The offer has been sent to the player, waiting to see if he or she does accept.");
	ShowPlayerDialog(target, EJobs_FoodSellDialog, DIALOG_STYLE_MSGBOX, "Food:", msg, "Accept", "Reject");
}
sendGunOffer(playerid, target, gunid, price) {
	new msg[128];
	new gunname[32];
	GetWeaponNameEx(gunid, gunname, sizeof(gunname));
	format(msg, sizeof(msg), "%s wants to sell you a %s for $%s", GetPlayerNameEx(playerid, ENameType_RPName),gunname, getNumberString(price));
	SetPVarInt(target, "GunSeller", playerid);
	SetPVarInt(target, "GunPrice", price);
	SetPVarInt(target, "GunSellID", gunid);
	SendClientMessage(playerid, X11_WHITE, "Offered gun to player, waiting to see if he will accept");
	ShowPlayerDialog(target, EJobs_GunSellDialog, DIALOG_STYLE_MSGBOX, "Gun deal", msg, "Accept", "Reject");
}
sendDrugOffer(playerid, target, drugname[], amount, price) {
	new total = GetPVarInt(playerid, drugname);
	if(total < amount) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough drugs!");
		return 1;
	}
	new msg[128];
	format(msg, sizeof(msg), "%s wants to sell you %sgs of %s for $%s", GetPlayerNameEx(playerid, ENameType_RPName),getNumberString(amount), drugname, getNumberString(price));
	SetPVarInt(target, "DrugSeller", playerid);
	SetPVarInt(target, "DrugBuyer", target);
	SetPVarInt(target, "DrugPrice", price);
	SetPVarInt(target, "DrugAmount", amount);
	SetPVarString(target, "DrugType", drugname);
	SendClientMessage(playerid, X11_WHITE, "Offered drugs to player, waiting to see if he will accept");
	ShowPlayerDialog(target, EJobs_DrugSellDialog, DIALOG_STYLE_MSGBOX, "Drug Deal", msg, "Accept", "Reject");
	return 0;
}

findJobSkillIndex(job) {
	for(new i=0;i<sizeof(Skills);i++) {
		if(Skills[i][ESkill_Job] == job) {
			return i;
		}
	}
	return -1;
}
increaseJobPoints(playerid) {
	new pvarname[32];
	getJobPointsPVarName(playerid, pvarname, sizeof(pvarname));
	new level = GetPVarInt(playerid, pvarname);
	setJobPoints(playerid, level+1);
}
doGunResolution(gun_seller, gun_id) {
	if(canArmsLevelUp(gun_seller, gun_id)) {
		increaseJobPoints(gun_seller);
	}
}
setJobPoints(playerid, level) {
	new index = findJobSkillIndex(GetPVarInt(playerid, "Job"));
	setSkillPoints(playerid, index, level);
}
setSkillPoints(playerid, skillindex, points) {
	query[0] = 0;
	SetPVarInt(playerid, Skills[skillindex][ESkill_PVarName], points);
	format(query, sizeof(query), "UPDATE `characters` SET `%s` = %d WHERE `id` = %d", Skills[skillindex][ESkill_PVarName], points, GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
increaseSkillPoints(playerid, skillindex) {
	new points = getSkillPoints(playerid, skillindex);
	points++;
	setSkillPoints(playerid, skillindex, points);
}
getJobPoints(playerid) {
	new points;
	new pvarname[32];
	getJobPointsPVarName(playerid, pvarname, sizeof(pvarname));
	points = GetPVarInt(playerid, pvarname);
	return points;
}
#pragma unused getJobPoints
getJobPointsPVarName(playerid, dst[], dstlen) {
	new job = GetPVarInt(playerid, "Job");
	new index = findJobSkillIndex(job);
	if(index != -1) {
		strmid(dst, Skills[index][ESkill_PVarName], 0, strlen(Skills[index][ESkill_PVarName]), dstlen);
	}
	return 0;
}
getJobLevel(playerid) {
	new job = GetPVarInt(playerid, "Job");
	new index = findJobSkillIndex(job);
	if(index != -1) {
		return getSkillLevel(playerid, index);
	}
	return 0;
}
getSkillLevel(playerid, skillindex) {
	if(skillindex < 0 || skillindex > sizeof(Skills)) {
		return 0;
	}
	new points = GetPVarInt(playerid, Skills[skillindex][ESkill_PVarName]);
	if(points <= Skills[skillindex][ESkill_Level1Points]) {
		return 1;
	}
	if(points <= Skills[skillindex][ESkill_Level2Points]) {
		return 2;
	}
	if(points <= Skills[skillindex][ESkill_Level3Points]) {
		return 3;
	}
	if(points <= Skills[skillindex][ESkill_Level4Points]) {
		return 4;
	} else {
	//if(points <= Skills[skillindex][ESkill_Level5Points]) {
		return 5;
	}
}
getSkillPoints(playerid, skillindex) {
	if(skillindex < 0 || skillindex > sizeof(Skills)) {
		return 0;
	}
	return GetPVarInt(playerid, Skills[skillindex][ESkill_PVarName]);
}

showJobSkills(playerid, index) {
	new msg[256];
	new levelupstr[128];
	new level = getSkillLevel(playerid, index);
	new remainingpoints;
	switch(level) {
		case 1: {
			remainingpoints = Skills[index][ESkill_Level1Points];
		}
		case 2: {
			remainingpoints = Skills[index][ESkill_Level2Points];
		}
		case 3: {
			remainingpoints = Skills[index][ESkill_Level3Points];
		}
		case 4: {
			remainingpoints = Skills[index][ESkill_Level4Points];
		}
		case 5: {
			remainingpoints = Skills[index][ESkill_Level5Points];
		}
	}
	remainingpoints -= getSkillPoints(playerid, index);
	if(remainingpoints < 0) remainingpoints = 0;
	format(levelupstr, sizeof(levelupstr), Skills[index][ESkill_LevelUpStr], remainingpoints);
	format(msg, sizeof(msg), "{FF0000}%s\n{FFFF00}%s\nYou are level %d!",Skills[index][ESkill_Name],levelupstr,getSkillLevel(playerid, index));
	ShowPlayerDialog(playerid, EJobs_SkillInfoDialog, DIALOG_STYLE_MSGBOX, "Skills", msg, "Ok", "");
	return 1;
}
loadSkills(playerid) {
	format(query, sizeof(query), "SELECT ");
	for(new i=0;i<sizeof(Skills);i++) {
		format(tempstr, sizeof(tempstr), "`%s`,",Skills[i][ESkill_PVarName]);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " FROM `characters` WHERE `id` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadSkills", "d",playerid);
}
forward OnLoadSkills(playerid);
public OnLoadSkills(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows < 0 ){
		return 1;
	}
	new fieldname[32],data[32];
	for(new i=0;i<fields;i++) {
		cache_get_field(i, fieldname);
		cache_get_row(0, i, data);
		SetPVarInt(playerid, fieldname, strval(data));
	}
	return 0;
}
saveSkills(playerid) {
	format(query, sizeof(query), "UPDATE `characters` SET ");
	for(new i=0;i<sizeof(Skills);i++) {
		format(tempstr, sizeof(tempstr), "`%s` = %d,",Skills[i][ESkill_PVarName],GetPVarInt(playerid,Skills[i][ESkill_PVarName]) );
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " WHERE `id` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
YCMD:service(playerid, params[], help) {
	new type[32];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Request a service");
		return 1;
	}
	if(!sscanf(params, "s[32]", type)) {
		new time,timenow;
		time = GetPVarInt(playerid, "ServiceCooldown");
		timenow = gettime();
		if(JOB_PICKUP_COOLDOWN-(timenow-time) > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must wait before using this command again!");
			return 1;
		}
		SetPVarInt(playerid, "ServiceCooldown", gettime());
		new msg[128];
		if(!strcmp(type, "Mechanic", true)) {
			format(msg, sizeof(msg), "* %s is requesting a mechanic {FFFFFF}(( /accept mechanic %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),playerid);
			sendJobMessage(EJobType_Mechanic, COLOR_LIGHTBLUE, msg);
			SetPVarInt(playerid, "MechRequest", gettime());
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Your request has been sent to all on duty mechanics");
		} else if(!strcmp(type, "Taxi", true)) {
			format(msg, sizeof(msg), "* %s is requesting a taxi {FFFFFF}(( /accept taxi %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),playerid);
			sendJobMessage(EJobType_TaxiDriver, COLOR_LIGHTBLUE, msg);
			SetPVarInt(playerid, "TaxiRequest", gettime());
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Your request has been sent to all on duty taxi drivers");
		} else if(!strcmp(type, "Lawyer", true)) {
			format(msg, sizeof(msg), "* %s is requesting a lawyer {FFFFFF}(( /accept lawyer %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),playerid);
			sendJobMessage(EJobType_Lawyer, COLOR_LIGHTBLUE, msg);
			SetPVarInt(playerid, "LawyerRequest", gettime());
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Your request has been sent to all lawyers");
		} else if(!strcmp(type, "EMS", true)) {			
			format(msg, sizeof(msg), "* %s is requesting a paramedic {FFFFFF}(( /accept EMS %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),playerid);
			SendMedicMessage(COLOR_LIGHTBLUE, 1, msg);
			SetPVarInt(playerid, "MedicRequest", gettime());
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Your request has been sent to all on duty EMS members");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Type!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /service [type]");
		SendClientMessage(playerid, X11_WHITE, "Types: Mechanic, Taxi, EMS, Lawyer");
	}
	return 1;
}
YCMD:accept(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Accept something from someone");
		return 1;
	}
	new type[32], user;
	new Float:X, Float:Y, Float:Z;
	if(!sscanf(params, "s[32]U(-1)", type, user)) {
		if(!strcmp(type, "Mechanic", true)) {
			new job = GetPVarInt(playerid, "Job");
			if(job != EJobType_Mechanic) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
				return 1;
			}
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			if(GetPVarInt(user, "MechRequest") != PLAYER_VARTYPE_NONE) {
				new msg[128];
				format(msg, sizeof(msg), "* %s has accepted your mechanic request. Stay where you are and wait.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
				SendClientMessage(user, X11_YELLOW, msg);
				format(msg, sizeof(msg), "* %s has accepted the mechanic request of %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(user, ENameType_RPName_NoMask));
				sendJobMessage(EJobType_Mechanic, COLOR_LIGHTBLUE, msg);
				DeletePVar(user, "MechRequest");
				if(GetPlayerInterior(user) != 0 || GetPlayerVirtualWorld(user) != 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "This user is inside a building now!");
					return 1;
				}
				GetPlayerPos(user, X, Y, Z);
				SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not requesting a mechanic!");
			}
		} else if(!strcmp(type, "Taxi", true)) {
			new job = GetPVarInt(playerid, "Job");
			if(job != EJobType_TaxiDriver) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a taxi driver for this!");
				return 1;
			}
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			if(GetPVarInt(user, "TaxiRequest") != PLAYER_VARTYPE_NONE) {
				new msg[128];
				format(msg, sizeof(msg), "* %s has accepted your taxi request. Stay where you are and wait.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
				SendClientMessage(user, X11_YELLOW, msg);
				format(msg, sizeof(msg), "* %s has accepted the taxi request of %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(user, ENameType_RPName_NoMask));
				sendJobMessage(EJobType_TaxiDriver, COLOR_LIGHTBLUE, msg);
				DeletePVar(user, "TaxiRequest");
				if(GetPlayerInterior(user) != 0 || GetPlayerVirtualWorld(user) != 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "This user is inside a building now!");
					return 1;
				}
				GetPlayerPos(user, X, Y, Z);
				SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not requesting a taxi!");
			}
		} else if(!strcmp(type, "Lawyer", true)) {
			new job = GetPVarInt(playerid, "Job");
			if(job != EJobType_Lawyer) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a lawyer for this!");
				return 1;
			}
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			if(GetPVarInt(user, "LawyerRequest") != PLAYER_VARTYPE_NONE) {
				new msg[128];
				format(msg, sizeof(msg), "* %s has accepted your lawyer request. Stay where you are and wait.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
				SendClientMessage(user, X11_YELLOW, msg);
				format(msg, sizeof(msg), "* %s has accepted the lawyer request of %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(user, ENameType_RPName_NoMask));
				sendJobMessage(EJobType_TaxiDriver, COLOR_LIGHTBLUE, msg);
				DeletePVar(user, "LawyerRequest");
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not requesting a lawyer!");
			}
		} else if(!strcmp(type, "EMS", true)) {
			new faction = GetPVarInt(playerid, "Faction");
			if(getFactionType(faction) != EFactionType_EMS) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be a medic");
				return 1;
			}
			if(GetPVarInt(user, "MedicRequest") != 0) {
				new msg[128];
				format(msg, sizeof(msg), "* %s has accepted the medic request of %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(user, ENameType_RPName_NoMask));
				SendMedicMessage(COLOR_LIGHTBLUE, 1, msg);
				DeletePVar(user, "MedicRequest");
				if(GetPlayerInterior(user) != 0 || GetPlayerVirtualWorld(user) != 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "This person is inside an interior!");
					return 1;
				}
				GetPlayerPos(user, X, Y, Z);
				SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not requesting a medic.");
			}
		}else if(!strcmp(type, "Death", true)) {
			if(!isPlayerDying(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are not dying!");
				return 1;
			}
			finishDying(playerid);
		} else if(!strcmp(type, "Anim", true)) {
			if(IsPlayerBlocked(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this right now!");
				return 1;
			}
			if(GetPVarType(playerid, "OfferingAnim") != PLAYER_VARTYPE_NONE) {
				new animidx = GetPVarInt(playerid, "OfferingAnim");
				new offerer = GetPVarInt(playerid, "AnimOfferer");
				new subanim = GetPVarInt(playerid, "AnimSubAnim");
				SendClientMessage(offerer, COLOR_LIGHTBLUE, "* Animation Accepted!");
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Animation Accepted!");
				DeletePVar(playerid, "OfferingAnim");
				DeletePVar(playerid, "AnimOfferer");
				DeletePVar(playerid, "AnimSubAnim");
				GetPlayerPos(offerer, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(playerid, 1.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(offerer)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are too far!");
					return 1;
				}
				if(subanim) {
					printf("subanim: %s %s %s\n",offerer,animidx,playerid);
					StartPlayerSubAnim(offerer, animidx, playerid);
				} else {
					printf("anim: %s %s %s\n",offerer,animidx,playerid);
					StartPlayerAnimation(offerer, animidx,playerid);
				}
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "You are not being offered an animation!");
				return 1;
			}
			return 1;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Type!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /accept [type]");
		SendClientMessage(playerid, X11_WHITE, "Types: mechanic, taxi, medic");
	}
	return 1;
}
sendJobMessage(job, color, const msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Job") == job) {
				if(~EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoJobMessages) {
					SendClientMessage(i, color, msg);
				}
			}
		}
	}
}

forward ShowSetSkillMenu(playerid,targetid);
public ShowSetSkillMenu(playerid,targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	SetPVarInt(playerid, "SetSkillTarget", targetid);
	for(new i=0;i<sizeof(Skills);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",Skills[i][ESkill_Name]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EJobs_SetSkillLevelMenu, DIALOG_STYLE_LIST, "Set Job",dialogstr,"Set", "Cancel");
}
OnEnterJobCar(playerid, carid, job) {
	#pragma unused carid
	if(job == EJobType_Trucker && GetPVarInt(playerid, "Job") == EJobType_Trucker) {
		startTruckerRoute(playerid);
	}
}