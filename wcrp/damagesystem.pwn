
//Body parts
#define BODY_PART_TORSO 3
#define BODY_PART_GROIN 4
#define BODY_PART_LEFT_ARM 5
#define BODY_PART_RIGHT_ARM 6
#define BODY_PART_LEFT_LEG 7
#define BODY_PART_RIGHT_LEG 8
#define BODY_PART_HEAD 9
//Player states
#define PLAYER_STATE_ALIVE 1
#define PLAYER_STATE_DEAD 0
//Cooldown
#define DAMAGE_JUMP_COOLDOWN 3

enum EWepDamageType {
	EDamageType_Melee,
	EDamageType_Weapon,
	EDamageType_Rifle,
};
enum eWepDamage {
	weapon,
	Float:damage,
	EWepDamageType:EDamageWeaponType,
	Float:PushVelocity,
	MaxTimes,
	TimeFreeze,
};
new WeaponDamage[][eWepDamage] = {
	{3, 15.0, EDamageType_Melee, 0.0, -1, -1}, //3 (damage was 30)
	{4, 40.0, EDamageType_Melee, 0.0, -1, -1}, //4
	{5, 10.0, EDamageType_Melee, 0.0, -1, -1}, //5 (damage was 30)
	{8, 40.0, EDamageType_Melee, 0.0, -1, -1}, //8
	{22, 25.0, EDamageType_Weapon, 0.025, 3, 250}, //22 (damage was 40)
	{23, 30.0, EDamageType_Weapon, 0.025, 3, 250}, //23
	{24, 30.0, EDamageType_Weapon, 0.025, 2, 550}, //24 (damage was 96)
	{25, 30.0, EDamageType_Weapon, 0.25, 2, 550}, //25 (max rounds was 2)
	{27, 30.0, EDamageType_Weapon, 0.25, 3, 550}, //27
	{28, 16.0, EDamageType_Weapon, 0.025, 5, 250}, //28
	{29, 26.0, EDamageType_Weapon, 0.025, 3, 250}, //29 (max rounds was 4)
	{30, 30.0, EDamageType_Weapon, 0.025, 3, 550}, //30
	{31, 30.0, EDamageType_Weapon, 0.025, 4, 250}, //31 (max rounds was 5)
	{33, 35.0, EDamageType_Rifle, 0.025, 1, 550}, //33
	{34, 35.0, EDamageType_Rifle, 0.025, 1, 550} //34
};

enum eWepSounds {
	weapon,
	link[128],
};
new WeaponSounds[][eWepSounds] = {
	{3, "-1"}, //22
	{4, "-1"}, //22
	{5, "-1"}, //22
	{22, "http://www.wc-rp.net/audio/gunsounds/p99_fire1.wav"}, //22
	{23, "http://www.wc-rp.net/audio/gunsounds/p99_fire_sil1.wav"}, //23
	{24, "http://www.wc-rp.net/audio/gunsounds/raptor_fire1.wav"}, //24
	{25, "http://www.wc-rp.net/audio/gunsounds/frinesi_fire.wav"}, //25
	{27, "http://www.wc-rp.net/audio/gunsounds/frinesi_fire.wav"}, //27
	{28, "http://www.wc-rp.net/audio/gunsounds/mp9_fire1.wav"}, //28
	{29, "http://www.wc-rp.net/audio/gunsounds/p90_fire2.wav"}, //29
	{30, "http://www.wc-rp.net/audio/gunsounds/sig552_fire3.wav"}, //30
	{31, "http://www.wc-rp.net/audio/gunsounds/sig552_fire3.wav"}, //31
	{33, "http://www.wc-rp.net/audio/gunsounds/l96_fire1.wav"}, //33
	{34, "http://www.wc-rp.net/audio/gunsounds/sniper_fire.wav"} //34
};
new Text3D:WoundedLabels[MAX_PLAYERS];

damageOnGameModeInit() {
	for(new i=0;i<MAX_PLAYERS;i++) {
		WoundedLabels[i] = Text3D:0;
	}
}

damageSystemOnPlayerTakeDamage(playershot, shooter, Float:amount, wep, bodypart) {
	printf("damageSystemOnPlayerTakeDamage(%d, %d, %f, %d, %d)", playershot, shooter, Float:amount, wep, bodypart);
	if(isInPaintball(playershot)) return 1;
	if(!isPlayerDying(shooter) || GetPVarType(shooter, "HasTaser") == PLAYER_VARTYPE_NONE) {
		for(new wepid; wepid < sizeof(WeaponDamage); wepid++) {
			if (WeaponDamage[wepid][weapon] == wep) {
				decideWepAssign(WeaponDamage[wepid][EDamageWeaponType], playershot, shooter, amount, wepid, bodypart);
			}
		}
	}	
	return 1;
}
Float:factorBulletPower(playerid, shooterid) {
	new Float: X, Float: Y, Float: Z;
	GetPlayerPos(shooterid, X, Y, Z);
	new Float: BPDistance = GetPlayerDistanceFromPoint(playerid, X, Y, Z);
	return BPDistance;
}
Float:factorBulletSpeed(playerid, shooterid) {
	new Float: X, Float: Y, Float: Z;
	GetPlayerPos(shooterid, X, Y, Z);
	new Float: BPDistance = GetPlayerDistanceFromPoint(playerid, X, Y, Z);
	return BPDistance/9; 
}
decideWepAssign(EWepDamageType:wepType, playershotid, shooterid, Float:dmgamount, gunid, bodypart) {
	switch(wepType) {
		case EDamageType_Weapon: {
			if(isValidShootingDistance(playershotid, shooterid)) {
				authorizeWepDamage(playershotid, shooterid, dmgamount, gunid, bodypart);
			}						
		}
		case EDamageType_Rifle: {
			if(isValidShootingDistance(playershotid, shooterid)) {
				authorizeWepDamage(playershotid, shooterid, dmgamount, gunid, bodypart);
			}						
		}
		case EDamageType_Melee: {
			authorizeWepDamage(playershotid, shooterid, dmgamount, gunid, bodypart);
		}
	}
	return 1;
}
stock gunDamagePushPlayer(playerid, shooterid, Float:distance) {
	new Float:X,Float:Y,Float:Z,Float:Angle;
	GetPlayerVelocity(playerid,X,Y,Z);
	GetPlayerFacingAngle(playerid,Angle);
	X += ( distance * floatsin( Angle, degrees ) );
	Y += ( distance * floatcos( Angle, degrees ) );
	if(isValidShootingDistance(playerid, shooterid)) {
		if(Angle > 180) {
			SetPlayerVelocity(playerid,-X,-Y,Z+distance);
		} else {
			SetPlayerVelocity(playerid,X,Y,Z+distance);
		}
	}
	return 1;
}
authorizeWepDamage(player, shooterid, Float:damageamount, wepid, bodypart) {
	new Float:bulletpower, Float:bulletspeed, Float:bulletpowercalc, Float:bulletdamagecalc;
	//new EAccountFlags:aflags = EAccountFlags:GetPVarInt(player, "AccountFlags");
	bulletpower = factorBulletPower(player, shooterid);
	bulletspeed = factorBulletSpeed(player, shooterid);
	bulletpowercalc = WeaponDamage[wepid][PushVelocity]/bulletpower;
	switch(WeaponDamage[wepid][EDamageWeaponType]) {
		case EDamageType_Weapon: {
			bulletdamagecalc = WeaponDamage[wepid][damage]/bulletspeed;
		}
		case EDamageType_Rifle: {
			bulletdamagecalc = WeaponDamage[wepid][damage];
		}
		case EDamageType_Melee: {
			bulletdamagecalc = WeaponDamage[wepid][damage];
		}
	}
	revertWepDamageBeforeDamage(player, damageamount);
	/* Small Error Handling */
	if(bulletdamagecalc > WeaponDamage[wepid][damage]) { //Sometimes the divisions turn positive due to dividing a greater number by a lower one like 0.0...
		bulletdamagecalc = WeaponDamage[wepid][damage]; //So we just set it to the default damage for that single bullet in-case that happens
	}
	doWepDamage(player, bulletdamagecalc, wepid, bodypart);
	gunDamagePushPlayer(player, shooterid, bulletpowercalc);
	setAttackerID(player, shooterid);
	sendGunSoundToInteriors(shooterid, wepid);
	/*
	if(aflags & EAccountFlags_ShowRedScreen) {
		colorScreen(player, 1, 4278190267);
		SetTimerEx("colorScreen",1000, false, "ddd",player,0,0);
	}
	*/
	//printf("BulletPowerCalc: %f", bulletpowercalc);
	//printf("BulletDamageCalc: %f", bulletdamagecalc);
}
setAttackerID(playerid, shooterid) {
	SetPVarInt(playerid, "AttackerID", shooterid);
}
getAttackerID(playerid) {
	if(GetPVarType(playerid, "AttackerID") != PLAYER_VARTYPE_NONE) {
		new returnattackerid = GetPVarInt(playerid, "AttackerID");
		return returnattackerid;
	}
	return -1;
}
dropPlayerFromBike(damagedid, wepid) {
	new Float:X, Float:Y, Float:Z;
	if(!IsPlayerBlocked(damagedid)) {
		if(WeaponDamage[wepid][EDamageWeaponType] != EDamageType_Melee) {
			if(IsPlayerInAnyVehicle(damagedid)) {
				new vehid = GetPlayerVehicleID(damagedid);
				new model = GetVehicleModel(vehid);
				if(IsABike(model) || IsABicycle(model)) {
					GetPlayerPos(damagedid,X,Y,Z);
					SetPlayerPos(damagedid, X, Y, Z+0.5);
				}
			}
		}
	}
}
revertWepDamageBeforeDamage(damagedid, Float:amountreset) {
	new Float:health;
	new Float:armour;
	GetPlayerHealth(damagedid, health);
	GetPlayerArmourEx(damagedid, armour);
	if(armour > 1 && armour <= MAX_ARMOUR) {
		SetPlayerArmourEx(damagedid, armour+amountreset);
	} else {
		if(health <= MAX_HEALTH) {
			SetPlayerHealthEx(damagedid, health+amountreset);
		}
	}
}
doWepDamage(damagedid, Float:amount, wepid, bodypart) {
	new Float:health;
	new Float:armour;
	GetPlayerHealth(damagedid, health);
	GetPlayerArmourEx(damagedid, armour);
	dropPlayerFromBike(damagedid, wepid);
	wobbleScreenForPlayer(damagedid, 500, 64000); //clientid, time in ms, drunkLevelamount
	if(armour > 1 && armour <= MAX_ARMOUR) {
		SetPlayerArmourEx(damagedid, armour-amount);
	} else {
		SetPlayerArmourEx(damagedid, 0);
		new shouldreducedmg = random(2);
		//if((getPlayerDisease(damagedid) == DISEASE_METH || getPlayerDisease(damagedid) == DISEASE_COCAINE) && shouldreducedmg) {
		new DrugFlags:aflags = DrugFlags:GetPVarInt(damagedid, "drug_effects");
		if(aflags & EDrugEffect_Resistance && shouldreducedmg) {
			amount = amount/2;
			SetPlayerHealthEx(damagedid, health-amount);
		} else {
			SetPlayerHealthEx(damagedid, health-amount);
		}
	}
	tryToApplyHurt(damagedid, wepid, bodypart);
	return 1;
}
tryToApplyHurt(damagedid, wepid, bodypart) {
	if(WeaponDamage[wepid][EDamageWeaponType] != EDamageType_Melee) {
		setPlayerHurt(damagedid, wepid, bodypart);
	}
	return 1;
}
setPlayerHurt(damagedid, wep, bodypart) {
	SetPVarInt(damagedid, "ShotReason", WeaponDamage[wep][weapon]);
	switch(bodypart) {
		//Disabled head shots, under request
		/*
		case BODY_PART_HEAD: {
			makePlayerWounded(damagedid, 1, PLAYER_STATE_DEAD);
		}
		*/
		case BODY_PART_LEFT_LEG, BODY_PART_RIGHT_LEG: {
			makePlayerUnableToRun(damagedid, 1);
		}
	}
	checkDamageCount(damagedid, wep);
}
checkDamageCount(damagedid, wep) {
	new Float:health;
	new Float:armour;
	new Float: X, Float: Y, Float: Z;
	GetPlayerHealth(damagedid, health);
	GetPlayerArmourEx(damagedid, armour);
	GetPlayerPos(damagedid, X, Y, Z);

	if(armour < 34.0) {
		setPlayerTimesShot(damagedid, 1);
		if(getWeaponClassType(damagedid, WeaponDamage[wep][weapon]) == _:EWeaponType_Shotgun) {
			if(IsPlayerInRangeOfPoint(getAttackerID(damagedid), 15.0, X, Y, Z)) {
				makePlayerWounded(damagedid, 1); //At this stage they're pretty much done with a gun of that type..
				return 1;
			}
		}
		if(getPlayerTimesShot(damagedid) >= getMaxRoundsToHurt(wep)) {
			makePlayerWounded(damagedid, 1);
		}
	}
	return 1;
}
makePlayerUnableToRun(damagedid, makeunable) {
	if(makeunable == 1) {
		ShowScriptMessage(damagedid, "You've been shot at one of your legs so now you can't jump any longer, unless you get treatment.",6000);
		SetPVarInt(damagedid, "PlayerCantRun", 1);
	} else {
		DeletePVar(damagedid, "PlayerCantRun");
	}
	return 1;
}
playerIsNotAbleToRun(damagedid) {
	if(GetPVarType(damagedid, "PlayerCantRun") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	return 0;
}
allowedToJump(damagedid) {
	new time = GetPVarInt(damagedid, "TimeJumped");
	new timenow = gettime();
	if(DAMAGE_JUMP_COOLDOWN-(timenow-time) > 0) {
		return 0;
	}
	return 1;
}
tryMakePlayerFall(damagedid) {
	if(!allowedToJump(damagedid) && getMovingSpeed(damagedid) > 0.10) {
		if(!IsPlayerInAnyVehicle(damagedid)) {
			makePlayerFall(damagedid);
		}
	}
	SetPVarInt(damagedid, "TimeJumped", gettime());
}
makePlayerFall(damagedid) {
	ShowScriptMessage(damagedid, "You've tripped over because you tried to jump while being shot in the leg.");
	ApplyAnimation(damagedid, "PED", "BIKE_fall_off", 4.1, 0, 1, 1, 1, 0, 1);
	DeletePVar(damagedid, "TimeJumped");
	SetTimerEx("getPlayerUpAnim",1500,false,"d",damagedid);
}
forward getPlayerUpAnim(damagedid);
public getPlayerUpAnim(damagedid) {
	ApplyAnimation(damagedid,"PED","getup",4.0,0,0,0,0,0,1);
	return 1;
}
damageSystemOnPlayerKeyState(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	#if debug
	printf("damageSystemOnPlayerKeyState(%d, %d, %d)", playerid, newkeys, oldkeys);
	#endif 
	if(playerIsNotAbleToRun(playerid)) {
		if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT) {
			if(newkeys & KEY_JUMP) {
				tryMakePlayerFall(playerid);
			}
		}
	} else if(isPlayerWounded(playerid) || isPlayerDying(playerid)) {
		if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT) {
			if(newkeys & KEY_SECONDARY_ATTACK || newkeys & KEY_JUMP) {
				applyAppropiateWoundedAnim(playerid, isPlayerDying(playerid) == 1 ? 0 : 1); //playerid, alivestate
			}
		}
	}
	return 1;
}
wobbleScreenForPlayer(damagedid, mstime, amount) {
	new dizzylevel = GetPlayerDrunkLevel(damagedid);
	if(GetPVarType(damagedid, "OldDrunkLevel") != PLAYER_VARTYPE_NONE) {
		SetPVarInt(damagedid, "OldDrunkLevel", dizzylevel);
	}
	SetPlayerDrunkLevel(damagedid, dizzylevel+amount);
	SetTimerEx("stopScreenWobble",mstime, false, "d",damagedid);
}
forward stopScreenWobble(damagedid);
public stopScreenWobble(damagedid) {
	if(GetPVarType(damagedid, "OldDrunkLevel") != PLAYER_VARTYPE_NONE) {
		new OldDLevel = GetPVarInt(damagedid, "OldDrunkLevel");
		SetPlayerDrunkLevel(damagedid, OldDLevel);
		DeletePVar(damagedid, "OldDrunkLevel");
	} else {
		SetPlayerDrunkLevel(damagedid, 0);
	}
	return 1;
}
YCMD:togwounded(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggle wounded on a person");
		return 1;
	}
	new target, status, alivestate;
	if(!sscanf(params, "k<playerLookup>D(-1)D(1)", target,status,alivestate)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if((status == -1 && isPlayerWounded(target)) || status == 0) {
			makePlayerWounded(target, 0);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Wounded Status removed");
		} else {
			setPlayerTimesShot(target, 1);
			makePlayerWounded(target, 1, alivestate);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Wounded Status set");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /togwounded [playerid]");
	}
	return 1;
	
}
//forward makePlayerWounded(damagedid, hurt, playerstate = PLAYER_STATE_ALIVE);
//public
makePlayerWounded(damagedid, hurt, playerstate = PLAYER_STATE_ALIVE) { //The player is always alive, remember that the player is wounded and in some cases ... dead ... depending on the affected area
	if(hurt == 1) {
		if(!isPlayerWounded(damagedid)) //Only do this if they're not wounded
			SetPlayerHealthEx(damagedid, 35.0);

		if(!IsPlayerInAnyVehicle(damagedid)) {
			if(playerstate == PLAYER_STATE_DEAD) {
				ShowScriptMessage(damagedid, "You've been brutally shot and now you're dead ~r~'/accept death'~w~...",3000);
				SetTimerEx("SetControllable",3000,false,"dd",damagedid,0);
				makePlayerDying(damagedid, 0); //0 means without anim and the message
			} else {
				SetPVarInt(damagedid, "DownOnGround", 1);
				ShowScriptMessage(damagedid, "You're ~r~ wounded~w~. Someone must help you up.",3000);
			}
			setWounded3dTextLabel(damagedid, 1, playerstate);
			applyAppropiateWoundedAnim(damagedid, playerstate);
		}
		doEvidenceDropFromDamage(damagedid);
	} else {
		if(isPlayerDying(damagedid)) {
			ShowScriptMessage(damagedid, "You're very weak right now, you ~r~can't~w~ stand back up!.",3000);
			return 1;
		}
		clearTimesShot(damagedid);
		TogglePlayerControllableEx(damagedid, 1);
		destroyWounded3dTextLabel(damagedid);
		if(!IsPlayerInAnyVehicle(damagedid)) {
			getPlayerUpAnim(damagedid); //This just calls the animation, it shouldn't be used to unfreeze the character, it doesn't do that.
		}
	}
	return 1;
}
applyAppropiateWoundedAnim(damagedid, playerstate = PLAYER_STATE_ALIVE) {
	if(playerstate == PLAYER_STATE_DEAD) {
		ApplyAnimation(damagedid, "PED", "KO_shot_face", 4.0, 0, 1, 1, 1, 1, 1);
		SetTimerEx("SetControllable",3000,false,"dd",damagedid,0);
	} else {
		ApplyAnimation(damagedid, "WUZI", "CS_DEAD_GUY", 4.0, 1, 1, 1, 1, 0, 1);
	}
	SetPlayerArmedWeapon(damagedid, 0);
}
doEvidenceDropFromDamage(damagedid) {
	new attackerid = getAttackerID(damagedid);
	new gunid = GetPVarInt(damagedid, "ShotReason");
	new amount = getPlayerTimesShot(damagedid);
	evidenceOnClothes(attackerid, damagedid, Evidence_Blood);
	dropEvidence(damagedid, Evidence_Blood);
	dropEvidence(attackerid, Evidence_Ammo, amount, gunid); /* I highly dislike this repetitive statement */
	sendGunShotToHAndBizzes(attackerid);
}
sendGunShotToHAndBizzes(attackerid) {
	new string[128];
	format(string, sizeof(string), "** You hear gun shots coming from outside **");
	sendMessageToHouses(attackerid, 60.0, string, COLOR_PURPLE);
	sendMessageToBusinesses(attackerid, 60.0, string, COLOR_PURPLE);
}
sendGunSoundToInteriors(shooterid, wepid) {
	if(WeaponSounds[wepid][weapon] >= 22) {
		sendAreaSound(shooterid, 60.0, WeaponSounds[wepid][link], 1);
	}
}
sendAreaSound(playerid, Float:radi = 30.0, url[], interiors = 1) {
	if(interiors == 1) {
		for(new i=0;i<sizeof(Business);i++) {
			if(IsPlayerInRangeOfPoint(playerid, radi, Business[i][EBusinessEntranceX],Business[i][EBusinessEntranceY],Business[i][EBusinessEntranceZ])) {
				sendAreaSoundXYZ(url, Business[i][EBusinessExitX],Business[i][EBusinessExitY],Business[i][EBusinessExitZ], Business[i][EBusinessVW], radi);
			}
		}
		for(new i=0;i<sizeof(Houses);i++) {
			if(IsPlayerInRangeOfPoint(playerid, radi, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ])) {
				sendAreaSoundXYZ(url, Houses[i][EHouseX], Houses[i][EHouseY], Houses[i][EHouseZ], houseGetVirtualWorld(i), radi);
			}
		}
	} else {
		new Float: X, Float: Y, Float: Z;
		new VW = GetPlayerVirtualWorld(playerid);
		GetPlayerPos(playerid, X, Y, Z);
		foreach(Player, i) {
			if(IsPlayerInRangeOfPoint(i, radi, X, Y, Z)) {
				if(GetPlayerVirtualWorld(i) == VW) {
					PlayAudioStreamForPlayer(i, url,X,Y,Z,radi,1);
				}
			}
		}
	}
	return 1;
}
isPlayerWounded(damagedid) {
	if(GetPVarType(damagedid, "DownOnGround") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	return 0;
}
setPlayerTimesShot(damagedid, times) {
	new amount = getPlayerTimesShot(damagedid)+times;
	SetPVarInt(damagedid, "TimesShot", amount);
}
getMaxRoundsToHurt(wepid) {
	return WeaponDamage[wepid][MaxTimes];
}
getPlayerTimesShot(damagedid) {
	if(GetPVarType(damagedid, "TimesShot") != PLAYER_VARTYPE_NONE) {
		return GetPVarInt(damagedid, "TimesShot");
	}
	return 0;
}
clearTimesShot(damagedid) {
	destroyWounded3dTextLabel(damagedid);
	DeletePVar(damagedid, "TimesShot");
	DeletePVar(damagedid, "ShotReason");
	DeletePVar(damagedid, "DownOnGround");
	DeletePVar(damagedid, "AttackerID");
	DeletePVar(damagedid, "PlayerCantRun");
	DeletePVar(damagedid, "TimeJumped");
}
isValidShootingDistance(damagedid, shooterid) {
	new Float: X, Float: Y, Float: Z;
	GetPlayerPos(damagedid, X, Y, Z);
	new Float: SDistance = GetPlayerDistanceFromPoint(shooterid, X, Y, Z);
	//printf("%f",SDistance);
	if(SDistance > 1.0) {
		return 1;
	}
	return 0;
}

YCMD:helpup(playerid, params[], help) {
	new Float:X, Float:Y, Float:Z, target;
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(target == playerid && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot stand up by yourself wait for someone to help you up or ask for help!");
			return 1;
		}
		if(isPlayerWounded(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot help someone stand up while you're injured!");
			return 1;
		}
		if(!isPlayerWounded(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not wounded!");
			return 1;
		}
		if(isPlayerDying(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is dying, you cannot help them up.");
			return 1;
		}
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be around this person!");
			return 1;
		}
		if(IsPlayerInAnyVehicle(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "The person you are trying to help is inside a vehicle!");
			return 1;
		}
		new str[128];
		format(query, sizeof(query), "* You helped %s stand up.",GetPlayerNameEx(target, ENameType_RPName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		format(query, sizeof(query), "* %s helped you stand up.",GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(target, COLOR_LIGHTBLUE, query);
		format(str, sizeof (str), "%s gets close to %s and helps %s stand up", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(target, ENameType_RPName), GetPlayerNameEx(target, ENameType_RPName));
		ProxMessage(30.0,playerid, str, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
		makePlayerWounded(target, 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /helpup [playerid]");
	}
	return 1;
}
/*
getPlayerWounded3dLabelText(playerid) {
	new ret[128];
	new gunid = GetPVarInt(playerid, "ShotReason");
	format(ret, sizeof(ret), "Critically Wounded! (( /helpup [%d] ))", GetPVarInt(playerid,"MaskOn")?GetPVarInt(playerid, "MaskID"):playerid);
	return ret;
}
*/
getPlayerWounded3dLabelText(playerid, playerstate = PLAYER_STATE_ALIVE) {
	new ret[128];
	new gunid = GetPVarInt(playerid, "ShotReason");
	new timesShot = getPlayerTimesShot(playerid);
	if(playerstate == PLAYER_STATE_ALIVE) {
		format(ret, sizeof(ret), "Critically Wounded! (( /helpup [%d] ))\n %s %s rounds.", playerid, timesShot ? getNumberString(timesShot) : "Unknown amount of", GunName[gunid]);
	} else {
		format(ret, sizeof(ret), "Deceased\n %s %s rounds.", timesShot ? getNumberString(timesShot) : "Unknown amount of", GunName[gunid]);
	}
	return ret;
}
setWounded3dTextLabel(playerid, status, playerstate) {
	destroyWounded3dTextLabel(playerid);
	if(status == 1) {
		if(WoundedLabels[playerid] == Text3D:0) {
			WoundedLabels[playerid] = CreateDynamic3DTextLabel(getPlayerWounded3dLabelText(playerid, playerstate),X11_WHITE, 0.0, 0.0, 0.4, NAMETAG_DRAW_DISTANCE,playerid,.testlos=1);
		}
	} else {
		destroyWounded3dTextLabel(playerid);
	}
	SetPVarInt(playerid, "WoundedLabel", status);
}
destroyWounded3dTextLabel(playerid) {
	if(WoundedLabels[playerid] != Text3D:0) {
		DestroyDynamic3DTextLabel(WoundedLabels[playerid]);
		WoundedLabels[playerid] = Text3D:0;
		DeletePVar(playerid, "WoundedLabel");
	}
}
damageSystemOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused reason
	#pragma unused killerid
	new Float: fX, Float: fY, Float: fZ;
	if(!isInPaintball(playerid)) {
		if(!getPlayerNumEvidenceAtPosByType(playerid, Evidence_Body)) { //Feel free to create a body here then
			GetPlayerPos(playerid, fX, fY, fZ);
			dropEvidence(playerid, Evidence_Body, 1, 0, fX, fY, fZ);
		}
	}
	clearTimesShot(playerid);
	destroyWounded3dTextLabel(playerid);
}
damageSystemOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	destroyWounded3dTextLabel(playerid);
}