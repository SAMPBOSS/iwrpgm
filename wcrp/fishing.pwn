enum EFishInfo {
	EFishName[32],
	EFishGunID, //if its not 0, give them this gun when they get it
	EFishValue, //value for selling, multplied by weight
	EFishMinWeight,
	EFishMaxWeight,
};

new FishInfo[][EFishInfo] = {
	{"Salmon",0,25,1,15},
	{"Tuna",0,25,1,15},
	{"Crab",0,10,1,5},
	{"Trout",0,30,1,15},
	{"Sea Bass",0,45,1,8},
	{"Shark",0,35,1,15},
	{"Turtle",0,25,1,25},
	{"Dolphin",0,20,1,25},
	{"Cat Fish",0,25,1,5},
	{"Blue Marlin",0,30,1,5},
	//junk
	{"Can",0,0,0,0},
	{"Pants",0,0,0,0},
	{"Shoes",0,0,0,0},
	{"Garbage",0,0,0,0},
	{"Baby Diaper",0,0,0,0},
	{"Tire",0,0,0,0},
	//{"Human Finger",0,0,0,0},
	//{"Severed Penis",0,0,0,0},
	//{"Dead Baby",0,0,0,0},
	{"Car Battery",0,0,0,0},
	{"Horse Hoove",0,0,0,0},
	{"Log",0,0,0,0}
	//guns
	/*
	{"AK47",30,0,0,0},
	{"Desert Eagle",24,0,0,0},
	{"M4",31,0,0,0},
	{"Shotgun",25,0,0,0},
	{"MP5",29,0,0,0},
	{"Nite Stick",3,0,0,0}
	*/
};

#define MAX_HOLDABLE_FISH 5
#define FISH_CMD_TIME 60

new PlayerFishing[MAX_PLAYERS];
new FishTimer[MAX_PLAYERS];
new CanCastPoll[MAX_PLAYERS];

enum EHoldFishInfo {
	EHoldingFishIdx,
	EHoldingFishWeight,
	EHoldingFightCaughtTime,
}
new HoldingFish[MAX_PLAYERS][MAX_HOLDABLE_FISH][EHoldFishInfo];

fishOnGameModeInit() {

	CreateDynamicPickup(1239, 16, 362.2244,-2088.7981,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 362.2244,-2088.7981,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 354.5382,-2088.7979,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 354.5382,-2088.7979,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 369.8107,-2088.7927,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 369.8107,-2088.7927,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 367.3637,-2088.7925,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 367.3637,-2088.7925,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 383.4157,-2088.7849,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 383.4157,-2088.7849,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 374.9598,-2088.7979,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 374.9598,-2088.7979,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 396.2197,-2088.6692,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 396.2197,-2088.6692,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 391.1094,-2088.7976,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 391.1094,-2088.7976,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 398.7553,-2088.7490,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 398.7553,-2088.7490,7.8359+1, 50.0);
	
	CreateDynamicPickup(1239, 16, 403.8266,-2088.7598,7.8359);
	CreateDynamic3DTextLabel("(( /fish ))", 0x2BB00AA, 403.8266,-2088.7598,7.8359+1, 50.0);
	
	
	
	
}
fishOnPlayerLogin(playerid) {
	for(new i=0;i<MAX_HOLDABLE_FISH;i++) {
		HoldingFish[playerid][i][EHoldingFishIdx] = -1;
		HoldingFish[playerid][i][EHoldingFishWeight] = 0;
		HoldingFish[playerid][i][EHoldingFightCaughtTime] = 0;
		
	}
	PlayerFishing[playerid] = 0;
}
tryHoldFish(playerid, fish, weight) {
	for(new i=0;i<MAX_HOLDABLE_FISH;i++) {
		if(HoldingFish[playerid][i][EHoldingFishIdx] == -1) {
			HoldingFish[playerid][i][EHoldingFishIdx] = fish;
			HoldingFish[playerid][i][EHoldingFishWeight] = weight;
			HoldingFish[playerid][i][EHoldingFightCaughtTime] = gettime();
			return 1;
		}
		
	}
	return 0;
}
forward RandomFish(playerid);
public RandomFish(playerid) {
	new shouldCatchFish = RandomEx(0, 5);
	new msg[128];
	new foundfish = 0;
	if(!IsAtFishPlace(playerid) || !IsNearFishVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need to be near a fish boat or at the fish place in order to fish.");
		return 1;
	}
	if(shouldCatchFish <= 2) {
		new idx = RandomEx(0,sizeof(FishInfo));		
		new weight = RandomEx(FishInfo[idx][EFishMinWeight],FishInfo[idx][EFishMaxWeight]);
		new shouldGetGun = RandomEx(0,100)==5;
		if(FishInfo[idx][EFishGunID] != 0 && shouldGetGun == 1) {
			foundfish = 1;
			format(msg, sizeof(msg), "~g~Congratulations!, ~w~You found a ~r~%s~w~!",FishInfo[idx][EFishName]);
			ShowScriptMessage(playerid, msg, 3000);
			format(msg, sizeof(msg), "{%s}Congratulations!, {%s}You found a {%s}%s{%s}!",getColourString(COLOR_GREEN),getColourString(COLOR_WHITE),getColourString(COLOR_GREEN),FishInfo[idx][EFishName],getColourString(COLOR_WHITE));
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
			new gun, ammo, slot = GetWeaponSlot(FishInfo[idx][EFishGunID]);
			GetPlayerWeaponDataEx(playerid, slot, gun, ammo);
			if(gun != 0 && gun != FishInfo[idx][EFishGunID]) {
				SendClientMessage(playerid, X11_TOMATO_2, "The weapon has been discarded since you are holding a weapon in its slot!");
				//return 1;
			} else {
				//GivePlayerWeaponEx(playerid, FishInfo[idx][EFishGunID], -1);
				if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
					return 1;
				} else {
					GivePlayerWeaponEx(playerid, FishInfo[idx][EFishGunID], -1);
				}
			}
			
			//return 1;
		} else if(FishInfo[idx][EFishMaxWeight] == 0 && FishInfo[idx][EFishGunID] == 0) {
			foundfish = 1;
			format(msg, sizeof(msg), "~r~Sorry!, ~w~You found a ~g~%s~w~, try again!",FishInfo[idx][EFishName]);
			ShowScriptMessage(playerid, msg, 3000);
			format(msg, sizeof(msg), "{%s}Sorry!, {%s}You found a {%s}%s{%s}, try again!",getColourString(COLOR_RED),getColourString(COLOR_WHITE),getColourString(COLOR_GREEN),FishInfo[idx][EFishName],getColourString(COLOR_WHITE));
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
			//return 1;
		} else if(FishInfo[idx][EFishGunID] == 0) {
			foundfish = 1;
			format(msg, sizeof(msg), "~g~Congratulations!, ~w~You found a ~r~%s~w~, weighing %d pounds!",FishInfo[idx][EFishName],weight);
			ShowScriptMessage(playerid, msg, 6000);
			format(msg, sizeof(msg), "{%s}Congratulations!, {%s}You found a {%s}%s{%s}, weighing %d pounds!",getColourString(COLOR_GREEN),getColourString(COLOR_WHITE),getColourString(COLOR_RED),FishInfo[idx][EFishName],getColourString(COLOR_WHITE),weight);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
			if(!tryHoldFish(playerid, idx, weight)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are holding too many fish already!");
				HintMessage(playerid, COLOR_DARKGREEN, "Use /dropfish to drop a fish");
				//return 0;
			}
		}
		if(foundfish == 1) {
			format(msg, sizeof(msg), "* %s reels a %s in with %s fishing poll.",GetPlayerNameEx(playerid, ENameType_RPName),FishInfo[idx][EFishName],getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			return 1;
		}
	}
	format(msg, sizeof(msg), "* %s reels nothing in with %s fishing poll.",GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
	ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	ShowScriptMessage(playerid, "~r~Sorry! ~w~You didn't find anything, try again!", 6000);	
	return 0;
}
YCMD:dropfish(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a fish on hand");
		return 1;
	}
	new msg[128],fishidx;
	if(!sscanf(params, "d",fishidx)) {
		fishidx--;
		if(fishidx < 0 || fishidx > MAX_HOLDABLE_FISH) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Fish Index!");
			return 1;
		}
		if(HoldingFish[playerid][fishidx][EHoldingFishIdx] == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not holding a fish in this slot!");
			return 1;
		}
		format(msg, sizeof(msg), "* %s takes a %s out and throws it on the ground.", GetPlayerNameEx(playerid, ENameType_RPName), FishInfo[HoldingFish[playerid][fishidx][EHoldingFishIdx]][EFishName]);
		ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		HoldingFish[playerid][fishidx][EHoldingFishIdx] = -1;
		HoldingFish[playerid][fishidx][EHoldingFishWeight] = 0;
		HoldingFish[playerid][fishidx][EHoldingFightCaughtTime] = 0;
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /dropfish [id]");
		SendClientMessage(playerid, X11_WHITE, "Use /myfish to see your fish id");
	}
	return 1;
}
YCMD:myfish(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists your fish");
		return 1;
	}
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "**** Your fish ****");
	for(new i=0;i<MAX_HOLDABLE_FISH;i++) {
		if(HoldingFish[playerid][i][EHoldingFishIdx] != -1) {
			new fidx = HoldingFish[playerid][i][EHoldingFishIdx];
			format(msg, sizeof(msg), "%d. %s - Weight: %d",i+1,FishInfo[fidx][EFishName],HoldingFish[playerid][i][EHoldingFishWeight]);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	}
	return 1;
}
StartFishing(playerid) {
	PlayerFishing[playerid] = 1;
	CanCastPoll[playerid] = 1;
	FishTimer[playerid] = -1;
	ShowScriptMessage(playerid,  "~w~Press ~r~~k~~PED_SPRINT~~w~ to cast the fishing poll.", 10000);
}
StopFishing(playerid) {
	if(FishTimer[playerid] != -1)
		KillTimer(FishTimer[playerid]);
	FishTimer[playerid] = -1;
	PlayerFishing[playerid] = 0;
	HintMessage(playerid, COLOR_DARKGREEN, "You can see your fish with /myfish, and sell them at the fish store.");
}
forward FishingReadyTimer(playerid);
public FishingReadyTimer(playerid) {
	ShowScriptMessage(playerid, "~w~Press ~r~~k~~PED_SPRINT~~w~ to cast the fishing poll.", 16000);
	CanCastPoll[playerid] = 1;
}
GetFishInfoAt(playerid, name[], namesize, slot, &weight, &value) {
	if(slot < 0 || slot > MAX_HOLDABLE_FISH) return 0;
	if(HoldingFish[playerid][slot][EHoldingFishIdx] != -1) {
		new fidx = HoldingFish[playerid][slot][EHoldingFishIdx];
		format(name, namesize, "%s",FishInfo[fidx][EFishName]);
		weight = HoldingFish[playerid][slot][EHoldingFishWeight];
		value = weight * FishInfo[fidx][EFishValue];
		value = floatround(value / 2.0);
	} else {
		value = 0;
		weight = 0;
	}
	return 1;
}
RemoveFishAt(playerid, idx) {
	HoldingFish[playerid][idx][EHoldingFishIdx] = -1;
	HoldingFish[playerid][idx][EHoldingFishWeight] = 0;
	HoldingFish[playerid][idx][EHoldingFightCaughtTime] = 0;
}
YCMD:catchfish(playerid, params[], help) {
	RandomFish(playerid);
	return 1;
}
fishingOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(FishTimer[playerid] != -1) {
		KillTimer(FishTimer[playerid]);
		FishTimer[playerid] = -1;
	}
}
YCMD:fish(playerid, params[], help) {	
	if(!IsWearingAccessory(playerid, 18632)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not holding a fishing poll!");
		HintMessage(playerid, COLOR_DARKGREEN, "You can buy a fishing poll at bincos. Use /accessories to take it out."); //, or the fish store
		return 1;
	}
	if(!IsAtFishPlace(playerid) || !IsNearFishVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need to be near a fish boat or at the fish place in order to fish.");
		return 1;
	}
	if(PlayerFishing[playerid] != 1) {
		if(!IsNearFishVehicle(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on a boat, or at the fishing point!");
			return 1;
		}
		if(IsAtFishPlace(playerid)) {
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			if(~lflags & ELicense_Fishing) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can't fish here since you don't have a fishing license");
				HintMessage(playerid, COLOR_DARKGREEN, "You can illegally fish if you buy a boat.");
				return 1;
			}
		}
		new msg[128];
		new timenow = gettime();
		new time = GetPVarInt(playerid, "LastFishCmd");
		if(FISH_CMD_TIME-(timenow-time) > 0) {
			format(msg, sizeof(msg), "You must wait %d seconds before you can use this command",FISH_CMD_TIME-(timenow-time));
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		
		SetPVarInt(playerid, "LastFishCmd", gettime());
		StartFishing(playerid);
	} else {
		StopFishing(playerid);
	}
	return 1;
}
IsAtFishPlace(playerid)
{
    //FuncLog("IsAtFishPlace");
	if(IsPlayerConnected(playerid))
	{
		if(IsPlayerInRangeOfPoint(playerid,1.0,403.8266,-2088.7598,7.8359) || IsPlayerInRangeOfPoint(playerid, 1.0,398.7553,-2088.7490,7.8359))
		{//Fishplace at the bigwheel
			return 1;
		}
		else if(IsPlayerInRangeOfPoint(playerid, 1.0,396.2197,-2088.6692,7.8359) || IsPlayerInRangeOfPoint(playerid, 1.0,391.1094,-2088.7976,7.8359))
		{//Fishplace at the bigwheel
			return 1;
		}
		else if(IsPlayerInRangeOfPoint(playerid, 1.0, 383.4157,-2088.7849,7.8359) || IsPlayerInRangeOfPoint(playerid, 1.0,374.9598,-2088.7979,7.8359))
		{//Fishplace at the bigwheel
			return 1;
		}
		else if(IsPlayerInRangeOfPoint(playerid, 1.0,369.8107,-2088.7927,7.8359) || IsPlayerInRangeOfPoint(playerid, 1.0,367.3637,-2088.7925,7.8359))
		{//Fishplace at the bigwheel
			return 1;
		}
		else if(IsPlayerInRangeOfPoint(playerid, 1.0,362.2244,-2088.7981,7.8359) || IsPlayerInRangeOfPoint(playerid, 1.0,354.5382,-2088.7979,7.8359))
		{//Fishplace at the bigwheel
			return 1;
		}
	}
	return 0;
}
IsNearFishVehicle(playerid) {
	new carid = GetClosestVehicle(playerid);
	new model;
	model = GetVehicleModel(carid);
	if(IsAtFishPlace(playerid) || DistanceBetweenPlayerAndVeh(playerid, carid) < 10.0 && IsABoat(model))
	{
		return 1;
	} else {
		carid = GetPlayerSurfingVehicleID(playerid);
		model = GetVehicleModel(carid);
		if(carid != INVALID_VEHICLE_ID) {
			if(DistanceBetweenPlayerAndVeh(playerid, carid) < 10.0 && IsABoat(model)) {
				return 1;
			}
		}
	}
	return 0;
	
}
fishOnPlayerKeyState(playerid, oldkeys, newkeys) {
	#pragma unused oldkeys
	if(newkeys & KEY_SPRINT) {
		if(PlayerFishing[playerid] == 1) {
			if(!IsNearFishVehicle(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "Since you left the fishing point, you stopped fishing.");
				StopFishing(playerid);
				return 0;
			}
			if(FishTimer[playerid] == -1) {
				FishTimer[playerid] = SetTimerEx("FishingReadyTimer", 15000, true, "i", playerid);
			}
			if(CanCastPoll[playerid] == 1) {
				SetTimerEx("RandomFish", 3000, false, "d", playerid);
				ApplyAnimation(playerid,"SWORD","sword_block",50.0,0,1,0,1,1);
				ShowScriptMessage(playerid, "Casting poll... Lets see if you catch anything!", 8000);	
				CanCastPoll[playerid] = 0;
			} else {
				ShowScriptMessage(playerid, "You must wait 15 seconds before you can cast again", 10000);	
			}
		}
	}
	return 0;
}
YCMD:eatfish(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Eat your fish");
		return 1;
	}
	new index, msg[128];
	if(!sscanf(params, "d", index)) {
		index--;
		if(index < 0 || index > MAX_HOLDABLE_FISH) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid fish index, use /myfish");
			return 1;
		}
		new fish = HoldingFish[playerid][index][EHoldingFishIdx];
		if(fish != -1) {			
			format(msg, sizeof(msg), "* %s eats a %s.", GetPlayerNameEx(playerid, ENameType_RPName), FishInfo[fish][EFishName]);
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			new boost = HoldingFish[playerid][index][EHoldingFishWeight]*2;
			boost = GetHungerLevel(playerid)+boost;
			if(boost > 100) boost = 100;
			SetPlayerHunger(playerid, boost);
			RemoveFishAt(playerid, index);
			return 1;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not holding a fish at this slot!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /eatfish [fishidx]");
	}
	return 1;
}