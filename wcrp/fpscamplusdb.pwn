//Key Defines
#define KEY_AIM     (128)
#define HOLDING(%0) \
	((newkeys & (%0)) == (%0))
#define RELEASED(%0) \
	(((newkeys & (%0)) != (%0)) && ((oldkeys & (%0)) == (%0)))
//End of Key Defines

YCMD:fpscamera(playerid, params[], help) {
	new totalfpscams;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "AttachFpsCamera") != PLAYER_VARTYPE_NONE) {
				totalfpscams++;
			}
		}
	}
	if(totalfpscams > 34) {
		ShowScriptMessage(playerid, "Too ~r~ many~w~ people are using the FPS camera right now, please wait a bit.",3000);
		return 1;
	}
	ToggleFPSMode(playerid);
    return 1;
}
ToggleFPSMode(playerid) {
	if(GetPVarType(playerid, "FpsCameraActive") == PLAYER_VARTYPE_NONE) {
	    SetPVarInt(playerid, "AttachFpsCamera", CreateObject(19300, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
        AttachObjectToPlayer(GetPVarInt(playerid, "AttachFpsCamera"), playerid, 0.0, 0.25, 0.73, 0.0, 0.0, 0.0);
		AttachCameraToObject(playerid, GetPVarInt(playerid, "AttachFpsCamera"));
        SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO] FPS Mode On!");
        SetPVarInt(playerid, "FpsCameraActive", 1);
	} else{
        SetCameraBehindPlayer(playerid);
		DestroyObject(GetPVarInt(playerid, "AttachFpsCamera"));
		SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO] FPS Mode Off!");
		deleteFPSPVars(playerid);
    }
}
fpsCamOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(GetPVarType(playerid, "FpsCameraActive") != PLAYER_VARTYPE_NONE) {
		ToggleFPSMode(playerid);
	}
}
dbkeysOnPlayerKeyState(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	new gunid = GetPlayerWeapon(playerid);
	if(newkeys & KEY_CROUCH && IsPlayerInAnyVehicle(playerid) && GetPlayerState(playerid) != PLAYER_STATE_DRIVER && gunid >= 22) {
		if(GetPVarInt(playerid, "WasDrivingBy") == 0) {
      		setPlayerDriveBy(playerid, 1);
        } else {
        	setPlayerDriveBy(playerid, 0);
		}
	}
	return 1;
}
setPlayerDriveBy(playerid, toggle) {
	if(toggle == 1) {
		new weaponid = GetPlayerWeapon(playerid);
		if(isNotADBWeapon(weaponid)) {
			SetPlayerArmedWeapon(playerid, 0);
			return 1;
		} else {
			SetPVarInt(playerid, "WasDrivingBy", 1);
      		SetPVarInt(playerid, "WeaponDbID", GetPlayerWeaponEx(playerid));
			ShowScriptMessage(playerid, "You're now driving by, press ~r~~k~~GROUP_CONTROL_BWD~~w~ again to pull the gun in.",3000);
		}
	} else {
		SetPlayerArmedWeapon(playerid, 0);
		SetTimerEx("BringWeaponBack",350,0,"d",playerid);
		ApplyAnimation(playerid,"PED","CAR_GETIN_RHS",4.1,0,0,0,0,1,1);
	}
	return 1;
}
drivebyOnPlayerStateChange(playerid, newstate, oldstate) {
	#pragma unused newstate
	if(oldstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_PASSENGER) {
		if(GetPVarInt(playerid, "WasDrivingBy") > 0) {
			//reset the drive by stuff
			deleteDByPVars(playerid);
		}
	}
}
drivebyOnPlayerExitVehicle(playerid, vehicleid) {
	#pragma unused vehicleid
    if(GetPVarInt(playerid, "WasDrivingBy") > 0) {
    	//reset the drive by stuff
		deleteDByPVars(playerid);
    }
    return 1;
}
deleteDByPVars(playerid) {
	DeletePVar(playerid, "WeaponDbID");
	DeletePVar(playerid, "WasDrivingBy");
}
deleteFPSPVars(playerid) {
	DeletePVar(playerid, "FpsCameraActive");
	DeletePVar(playerid, "AttachFpsCamera");
}
forward CheckForCamera(playerid);
public CheckForCamera(playerid) {
    if(GetPVarInt(playerid, "FpsCameraActive") == 1) {
	    DestroyObject(GetPVarInt(playerid, "AttachFpsCamera"));
        SetPVarInt(playerid, "AttachFpsCamera", CreateObject(19300, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
        AttachObjectToPlayer(GetPVarInt(playerid, "AttachFpsCamera"), playerid, 0.0, 0.25, 0.73, 0.0, 0.0, 0.0);
		AttachCameraToObject(playerid, GetPVarInt(playerid, "AttachFpsCamera"));
		SetPVarInt(playerid, "FpsCameraActive", 1);
    } else {
        SetCameraBehindPlayer(playerid);
    }
}
forward SwitchFpsCameraView(playerid, camview);
public SwitchFpsCameraView(playerid, camview) {
	if(GetPlayerWeapon(playerid) < 32) { //If it's not anything like a rocket launcher or sniper
		DestroyObject(GetPVarInt(playerid, "AttachFpsCamera"));
		SetPVarInt(playerid, "AttachFpsCamera", CreateObject(19300, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0));
		if(camview == 2) { //Aiming Camera View
			if(GetPlayerWeapon(playerid) > 28 && GetPlayerWeapon(playerid) < 32) { //If it's a rifle or something like that
				AttachObjectToPlayer(GetPVarInt(playerid, "AttachFpsCamera"), playerid, -0.05, -0.20, 0.80, 0.0, 0.0, 0.0); //+right -left, +front -back, height
			} else { //Pistols and others
				AttachObjectToPlayer(GetPVarInt(playerid, "AttachFpsCamera"), playerid, 0.0, 0.04, 0.80, 0.0, 0.0, 0.0);
			}
		}
		if(camview == 3) { //Normal Camera View
			AttachObjectToPlayer(GetPVarInt(playerid, "AttachFpsCamera"), playerid, 0.0, 0.25, 0.73, 0.0, 0.0, 0.0);
		}
		AttachCameraToObject(playerid, GetPVarInt(playerid, "AttachFpsCamera"));
		SetPVarInt(playerid, "FpsCameraActive", 1);
	}
    return 1;
}
forward BringWeaponBack(playerid);
public BringWeaponBack(playerid) {
    SetPlayerArmedWeapon(playerid, GetPVarInt(playerid, "WeaponDbID"));
	deleteDByPVars(playerid);
}
