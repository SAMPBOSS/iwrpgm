#define LOTTO_MIN 25000
#define LOTTO_MAX 100000
#define LOTTO_CAP 120000

#define LOTTO_NUMBER_MAX 100
		
new lottoFunds;

//player pvar LottoNumber is the ticket number they bought, lost on quit

setLottoFunds(funds) {
	lottoFunds = funds;
}
getLottoFunds() {
	return lottoFunds;
}
adjustLottoFunds(offset) {
	if(lottoFunds+offset < LOTTO_CAP)
		lottoFunds += offset;
}
saveLottoFunds() {
	format(query, sizeof(query), "UPDATE `misc` SET `jackpot` = %d",getLottoFunds());
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
lottoOnGameModeExit() {
	saveLottoFunds();
}
YCMD:playlotto(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Force the lottery to run");
		return 1;
	}
	playLotto();
	return 1;
}
YCMD:setjackpot(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Override the lotto funds");
		return 1;
	}
	new funds;
	if(!sscanf(params, "d", funds)) {
		setLottoFunds(funds);
		SendClientMessage(playerid, COLOR_DARKGREEN, "[Notice]: Funds Adjusted!");
	} else {
		format(query, sizeof(query), "Lotto Jackpot: $%s",getNumberString(getLottoFunds()));
		SendClientMessage(playerid, COLOR_DARKGREEN, query);
	}
	return 1;
}
playLotto() {
	new lottoNumber = RandomEx(1,LOTTO_NUMBER_MAX);
	new playerisWinner[MAX_PLAYERS],numWinners,string[128];
	format(string, sizeof(string), "* Lotto: The winning number is: %d, with a total jackpot of $%s!",lottoNumber,getNumberString(getLottoFunds()));
	NewsMessage(X11_YELLOW,string);
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(getPlayerLottoTicket(i) == lottoNumber) {
				playerisWinner[i] = 1;
				numWinners++;
			}
			deleteLottoTicket(i);
		}
	}
	if(numWinners != 0) {
		new perPlayerJackpot = floatround(getLottoFunds()/numWinners);
		for(new i=0;i<MAX_PLAYERS;i++) {
			if(playerisWinner[i] == 1) {
				GiveMoneyEx(i, perPlayerJackpot);
				format(string, sizeof(string), "* Lotto: %s has won $%s from the lottery!",GetPlayerNameEx(i, ENameType_RPName_NoMask), getNumberString(perPlayerJackpot));
				NewsMessage(X11_YELLOW,string);
				adjustLottoFunds(-perPlayerJackpot);
			}
		}
		adjustLottoFunds(LOTTO_MIN); //reset the lotto!
	} else {
		new raise = RandomEx(5000, 25000);
		adjustLottoFunds(raise);
		format(string, sizeof(string), "* Lotto: There are no winners for this lotto! The jackpot has now raised to: $%s",getNumberString(getLottoFunds()));
		NewsMessage(X11_YELLOW,string);
	}
	saveLottoFunds();
	MassHintMessage(COLOR_LIGHTCYAN, "You can buy a lottery ticket at any 24/7");
}
YCMD:lottoinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays lottery info");
		return 1;
	}
	new number;
	if((number = getPlayerLottoTicket(playerid)) != 0) {
		format(query, sizeof(query), "* Your ticket number: %d",number);
		SendClientMessage(playerid, X11_YELLOW, query);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* You don't have a lotto ticket!");
	}
	format(query, sizeof(query), "* Jackpot: $%s",getNumberString(getLottoFunds()));
	SendClientMessage(playerid, X11_YELLOW, query);
	format(query, sizeof(query), "* Number of participants: %d",GetNumLottoPlayers());
	SendClientMessage(playerid, X11_YELLOW, query);
	return 1;
}
YCMD:trashlotto(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Trash your lottery ticket");
		return 1;
	}
	if(getPlayerLottoTicket(playerid) != 0) {
		deleteLottoTicket(playerid);
		SendClientMessage(playerid, X11_YELLOW, "* Ticket Trashed!");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* You don't have a ticket!");
	}
	return 1;
}
deleteLottoTicket(playerid) {
	DeletePVar(playerid, "LottoNumber");
}
GetNumLottoPlayers() {
	new num;
	foreach(Player, i) {
		if(getPlayerLottoTicket(i) != 0)
			num++;
	}
	return num;
}
giveRandomLottoNumber(playerid) {
	new onumber = getPlayerLottoTicket(playerid);
	if(onumber != 0) {
		return onumber;
	}
	onumber = RandomEx(1,LOTTO_NUMBER_MAX);
	setPlayerLottoTicket(playerid, onumber);
	return onumber;
}
setPlayerLottoTicket(playerid, number) {
	SetPVarInt(playerid, "LottoNumber", number);
}
getPlayerLottoTicket(playerid) {
	return GetPVarInt(playerid, "LottoNumber");
}
isLottoNumberAcceptable(number) {
	return number < LOTTO_NUMBER_MAX && number > 0;
}
getLottoMax() {
	return LOTTO_NUMBER_MAX;
}