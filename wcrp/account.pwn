/*
	All account related code goes here, including commands and mysql code
*/
/*
	PVars used by this script:
		RegistrationPass(string) - password
		RegistrationEmail(string) - email
		AccountID(int) - MySQL account ID
		AccountName(string) - account name
		CharIDN(int) - nth character ID corresponding to the account
		CharUsernameN(string) N = char index - stores character username from character selection dialog for displaying in selection menu
		NumCharacters(int) - Number of characters(calculated from number of rows in OnLoadCharacters)
		LoginAttempts(int) - Number of login attempts(for used to kick)
		LoggedIn(int) - if 1 the player has authenticated with their account
		CharID(int) - logged in character ID
		CharUsername(string) - Current character name
		AdminFlags(int) - admin flags
		Money(int) - Cash on hand
		Bank(int) - Cash in bank
		Sex(int) - 0 if male, 1 if female, otherwise tranny
		AdminEdit(int) - Admin you are editing(for dialogs)
		MatsA/B/C(int) - Number of materials you have on hand
		Pot(int) - Amount of pot you have on had
		Coke(int) - Amount of coke you have on hand
		Meth(int) - Amount of meth you have on hand
		Payday(int) - payday bonus
		CorrectingTest(int) - Used for the Dynamic 3DTextLabels admins have
		TestStep(int) - Used for checking where on the test the user is
		
*/


#define MAX_EMAIL_LEN 128
#define MAX_PASSWORD_LEN 64 //maximum enterable password lenggth
#define MAX_PASS_HASH_LEN 32 //password hash
#define MAX_CHARACTERS 5
#define MAX_LOGIN_ATTEMPTS 5
#define MAX_IPCHECK_TIME 3
#define CAM_TRANSITION_TIME 60000 //ms

enum {
	EAccountDialog_Registration = EAccountDialog_Base + 1,
	EAccountDialog_Question,
	EAccountDialog_Question_1 = EAccountDialog_Question,
	EAccountDialog_Question_2,
	EAccountDialog_Question_3,
	EAccountDialog_Question_4,
	EAccountDialog_Question_5,
	EAccountDialog_Question_6,
	EAccountDialog_Question_7,
	EAccountDialog_Question_8,
	EAccountDialog_Question_9,
	EAccountDialog_Question_10,
	EAccountDialog_Question_11,
	EAccountDialog_Question_Last = EAccountDialog_Question_11,
	EAccountDialog_CreatePassword,
	EAccountDialog_EnterEmail,
	//END REGISTRATION PROCESS
	EAccountDialog_DoNothing,
	EAccountDialog_EnterPassword,
	EAccountDialog_CreateCharacter,
	EAccountDialog_ChooseCharacter,
	EAccountDialog_SetChar_Sex,
	EAccountDialog_SetChar_Age,
	EAccountDialog_WelcomeMessage,
	EAccountDialog_SpawnDialog,
	EAccountDialog_ChangePass,
	EAccountDialog_ChangePass_S1,
	EAccountDialog_ChangePass_S2,
	EAccountDialog_Settings,
	EAccountDialog_Num_Questions = EAccountDialog_Question_Last - EAccountDialog_Question
};

new Text3D:RPTestLabels[MAX_PLAYERS];
/*
#define NUM_REG_ANSWERS 4
enum eRegistrationQuestions {
	eRegQuestion[128],
	eRegAnswer1[128],
	eRegAnswer2[128],
	eRegAnswer3[128],
	eRegAnswer4[128],
	eRegProperAnswer
};
new RegistrationQuestions[][eRegistrationQuestions] = {
	{"What is RP?","Stealing peoples identify who play on the server","Creating and taking the role, of a real person","Killing people for fun","Doing stunt tricks",1},
	{"What is OOC?","Out of Cactus","Out of Cars","Out of Character","Out of Context",2},
	{"What is MG?","Looking at someones name and taking it","Using information you recieved OOcly IC","Using IC information OOC","Mixing the chat commands up",1}
};
*/

enum eRegistrationQuestions {
	eRegQuestion[128],
	eRegAnswer1[128],
	eRegAnswer2[128],
	eRegAnswer3[128],
	eRegAnswer4[128],
	eRegProperAnswer
};
new RegistrationQuestions[][eRegistrationQuestions] = {
	{"What is DM?"},
	{"What is Role Play?"},
	{"Briefly explain what revenge killing is."},
	{"What is OOC?"},
	{"What is MG? Give an Example of it."},
	{"What is PG? Give an Example of it."},
	{"What is CK?"},
	{"What do you do if you find a bug?"},
	{"What is an example of an RP name?"},
	{"What do you do if you see someone breaking a rule?"},
	{"Where are you allowed to drive?"},
	{"What is our policy on robbing newbies?"},
	{"You are being arrested, what do you do?"},
	{"How should you use the '/me' command?"}
};

enum E_PlayerSpawnMenu {
	E_DialogOptionText[128],
	E_SpawnClassType,
	Float: pSpawnX,
	Float: pSpawnY,
	Float: pSpawnZ,
	Float: pSpawnAngle,
}

enum {
	Type_NextToPlayer,
	Type_Civilian,
	Type_Criminal,
}

new PlayerSpawnMenu[][E_PlayerSpawnMenu] = {
	{"Inglewood West", Type_Civilian, 1864.1890,-1599.3417,14.0189,177.28},
	{"Inglewood East", Type_Civilian, 2127.2605,-1796.8536,13.5539,3.0},
	{"East Los Angeles", Type_Criminal, 2377.9407,-1474.5808,23.8112,91.7195},
	{"Airport", Type_Civilian, 1682.68, -2326.80, 13.55,0.0},
	{"Union Station", Type_Civilian, 1714.67, -1890.58, 13.57,0.0},
	{"Verona Mall", Type_Civilian, 1129.9287,-1486.3196,22.7612,360.0},
	{"Next to someone", Type_NextToPlayer, 0.0,0.0,0.0,0.0}
};

//End of Registration Questions
enum EAccountLoginFlags (<<= 1) {
	EAccountLoginFlags_None = 0, 
	EAccountLoginFlags_Whitelist = 1,
	EAccountLoginFlags_Blacklist,
	EAccountLoginFlags_DenyAdmin,
	EAccountLoginFlags_Undercover,
	EAccountLoginFlags_AnyPassword,
	EAdminFlags_All = -1,
};

forward isAccountRegistered(const name[]);
forward onAccountRetrieve(playerid, handle);
forward OnLoadIPFlags(playerid,prelogin);
forward accountOnPlayerConnect(playerid);
forward OnQuizPass(playerid);
forward onAccountCreate(playerid, last_insert_id);
forward OnRegistrationComplete(playerid);
forward OnLoadCharacters(playerid);
forward OnLoginSuccess(playerid);
forward OnLoginAttempt(playerid);
forward onCharacterCreate(playerid, charname[], last_insert_id);
forward OnLoadCharacter(playerid, charid);
forward accountOnPlayerDisconnect(playerid, reason);
forward rpTestOnPlayerDisconnect(playerid, reason);
forward IsPlayerConnectEx(playerid);
forward showRegistrationDialog(playerid);
forward showSpawnMenu(playerid);

//new IPCheckTimer[MAX_PLAYERS];

accountOnGameModeInit() {
	/*
	for(new s = 1; s < 300; s++)//Bad Skins can NOT be bigger than the amount of actual skins listed
	{
		if(IsValidSkin(s))
		{
			AddPlayerClass(s, 1682.68,-2326.80, 13.55, 0.0, 0, 0, 0, 0, 0, 0);
		}
	}
	*/
	repeat SaveTimer();
}
public isAccountRegistered(const name[]) {
}
/*
	This call back is called when OnPlayerConnect looks for if the account is registered, basically just for retrieving ID
*/

public showSpawnMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(PlayerSpawnMenu);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",PlayerSpawnMenu[i][E_DialogOptionText]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccountDialog_SpawnDialog, DIALOG_STYLE_LIST, "Where does your character's life begin?",dialogstr,"Ok", "Cancel");
}

public onAccountRetrieve(playerid) {
	new id_string[MAX_PLAYER_NAME];
	new id;
	new rows,fields;
	cache_get_data(rows,fields);
	SetRandomCamera(playerid);
	//PlayAudioStreamForPlayer(playerid, "http://www.wc-rp.net/audio/DUST.mp3");
	new reason[128],timestr[64],banner[24],msg[128],banexpire[64];
	if(GetPVarType(playerid, "Banner") != PLAYER_VARTYPE_NONE) {
		GetPVarString(playerid, "BanReason", reason, sizeof(reason));
		GetPVarString(playerid, "BanTime", timestr, sizeof(timestr));
		GetPVarString(playerid, "Banner", banner, sizeof(banner));
		GetPVarString(playerid, "BanExpire", banexpire, sizeof(banexpire));
		SendClientMessage(playerid, X11_TOMATO_2, "You are banned.");
		format(msg,sizeof(msg),"Reason: %s by %s on %s BanID: %d",reason,banner,timestr,GetPVarInt(playerid, "BanID"));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		if(banexpire[0] != '0')  {
			format(msg, sizeof(msg), "Ban Expire Time: %s",banexpire);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
		}
		SendClientMessage(playerid, X11_TOMATO_2, "Make a ban appeal on the forums if you feel this is an error.");
		SetTimerEx("TimedKick",1000, false, "d",playerid);
		return 1;
	}
	if(GetPVarType(playerid, "RangeBanID") != PLAYER_VARTYPE_NONE) { //range ban check
		new host[17],mask[17];
		id = GetPVarInt(playerid,"RangeBanID");
		GetPVarString(playerid, "RangeBanReason", reason, sizeof(reason));
		GetPVarString(playerid, "RangeBanBanner", banner, sizeof(banner));
		GetPVarString(playerid, "RangeBanHost", host, sizeof(host));
		GetPVarString(playerid, "RangeBanMask", mask, sizeof(mask));
		SendClientMessage(playerid, X11_TOMATO_2, "You are range banned.");
		format(msg,sizeof(msg),"Reason: %s by %s on %s BanID: %d",reason,banner,timestr,id);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		SendClientMessage(playerid, X11_TOMATO_2, "Make a ban appeal on the forums if you feel this is an error.");
		SetTimerEx("TimedKick",1000, false, "d",playerid);
		return 1;
	}
	if(rows > 0) {
		cache_get_row(0, 0, id_string);
		id = strval(id_string);
		cache_get_row(0, 1, id_string);
		SetPVarString(playerid,"AccountName",id_string);
		if(rows > 1) {
			cache_get_row(1, 0, id_string);
			id = strval(id_string);
			SendClientMessage(playerid, X11_TOMATO_2, "Warning: A character with your account name exists, if you can't login use your account name instead");
		}
		SetPVarInt(playerid, "AccountID", id);
		ShowLoginDialog(playerid);
	} else {
		GetPlayerName(playerid, id_string, sizeof(id_string));
		SetPVarString(playerid,"AccountName",id_string);
		if(!server_rptest) {
			OnQuizPass(playerid);
		} else {
			if(NumHelpersOnline() >= 2) {
				showRegistrationDialog(playerid);
			} else {
				OnQuizPass(playerid);
			}
		}
	}
	return 0;
}

/*
	This callback is used to validate if an account was created or not, and to retrieve its ID if it was
*/
public onAccountCreate(playerid, last_insert_id) {
	
	new id = mysql_insert_id();
	if(id == 0 || id == last_insert_id) {
		SendClientMessage(playerid, X11_RED, "There was an error creating your account.");
		//TODO: LOG ERRORS
		Kick(playerid);
		return 0;
	}
	SetPVarInt(playerid,"AccountID",id);
	#if debug
	new dbgmsg[128];
	format(dbgmsg,sizeof(dbgmsg),"[DBG]: Account ID: %d",id);
	SendClientMessage(playerid, X11_RED,dbgmsg);
	#endif
	SetPVarInt(playerid, "AccountLoginTime", gettime()); //Login time for the account, not the character
	ShowCharacterDialog(playerid);
	return 1;
}

/*
	This callback is called when a character is created, and is used to validate if it was created or not, and get its ID if it was
*/

public onCharacterCreate(playerid, charname[], last_insert_id) {
	new rows,fields,id;
	cache_get_data(rows,fields);
	id = mysql_insert_id();
	if(id == 0 || id == last_insert_id) {
		SendClientMessage(playerid, X11_RED, "There was an error creating your account.");
	}
	#if debug
	new dbgmsg[128];
	format(dbgmsg,sizeof(dbgmsg),"[DBG]: Character ID: %d",id);
	SendClientMessage(playerid, X11_RED,dbgmsg);
	#endif
	SetPVarInt(playerid, "CreateCharID", id);
	new pvarname[32];
	new index = 0;
	do {
		format(pvarname,sizeof(pvarname),"CharID%d",index++);
	} while(GetPVarType(playerid, pvarname) == PLAYER_VARTYPE_INT);
	SetPVarInt(playerid, pvarname, id);
	format(pvarname,sizeof(pvarname),"CharUsername%d",index-1);
	SetPVarString(playerid, pvarname, charname);
	SetPVarInt(playerid, "NumCharacters", GetPVarInt(playerid,"NumCharacters")+1);
	ShowCharacterSetupDialog(playerid);
}
ShowCharacterSetupDialog(playerid) {
	ShowPlayerDialog(playerid, _:EAccountDialog_SetChar_Sex, DIALOG_STYLE_LIST, "Choose your sex","Male\nFemale","Select", "Quit");	
}
public accountOnPlayerConnect(playerid) {
	/*
	#if !debug
	checkNeedImport(playerid);
	#else
	*/
	new name[(MAX_PLAYER_NAME*2)+1];
	new szQuery[256];
	//DON'T use GetPlayerNameEx here!
	GetPlayerName(playerid,name,MAX_PLAYER_NAME);
	SetPVarString(playerid,"AccountName",name);
	mysql_real_escape_string(name,name);
	//This query used INNER JOIN before, causing everything to bug out if they crashed upon registration DUH!!!!!
	format(szQuery, sizeof(szQuery), "SELECT `accounts`.`id`,`accounts`.`username` FROM `characters` RIGHT JOIN `accounts` ON `characters`.`accountid` = `accounts`.`id` WHERE `accounts`.`username` = \"%s\" OR `characters`.`username` = \"%s\" LIMIT 0,1", name, name);
	mysql_function_query(g_mysql_handle, szQuery, true, "onAccountRetrieve", "d", playerid);
	SetPlayerColor(playerid, getNameTagColour(playerid));
	
	sendServerInfo(playerid);
	//#endif
	return 1;
}
/*
checkNeedImport(playerid) {
//
	new name[(MAX_PLAYER_NAME*2)+1];
	new szQuery[512];
	//DON'T use GetPlayerNameEx here!
	GetPlayerName(playerid,name,MAX_PLAYER_NAME);
	mysql_real_escape_string(name,name);
	format(szQuery, sizeof(szQuery), "SELECT `accounts`.`username`,`accounts`.`password`,`accounts`.`email`,`accounts`.`ConnectTime`,`accounts`.`donatepoints`,`accounts`.`donaterank`,`accounts`.`id` FROM `wcrpbeta`.`accounts` INNER JOIN `wcrpbeta`.`userinfo` ON `userinfo`.`accountid` = `accounts`.`id`  WHERE (`userinfo`.`username` = \"%s\" OR `accounts`.`username` = \"%s\") AND `accounts`.`imported` = 0 LIMIT 0, 1",name,name);
	mysql_function_query(g_mysql_handle, szQuery, true, "OnNeedImportCheck", "d",playerid);
}
forward OnNeedImportCheck(playerid);
public OnNeedImportCheck(playerid) {
	new id_string[128];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		cache_get_row(0, 0, id_string);
		importAccount(id_string);
		SendClientMessage(playerid, X11_TOMATO_2, "Your account is now being imported, please reconnect!");
		SetTimerEx("TimedKick",2500, false, "d",playerid);
	} else {
		new name[(MAX_PLAYER_NAME*2)+1];
		new szQuery[256];
		//DON'T use GetPlayerNameEx here!
		GetPlayerName(playerid,name,MAX_PLAYER_NAME);
		SetPVarString(playerid,"AccountName",name);
		mysql_real_escape_string(name,name);
		format(szQuery, sizeof(szQuery), "SELECT `id` FROM `accounts` WHERE `username` = \"%s\" UNION SELECT `accountid` FROM `characters` WHERE `username` = \"%s\"", name, name);
		mysql_function_query(g_mysql_handle, szQuery, true, "onAccountRetrieve", "d", playerid);
		SetPlayerColor(playerid, getNameTagColour(playerid));
	
		sendServerInfo(playerid);
	}
	return 1;
}
importAccount(name[]) {
	new request[256];
	format(request, sizeof(request), "www.wc-rp.com/blahblahimport.php?username=%s", name);
	HTTP(0, HTTP_GET, request, "", "OnTryImport");
}
forward OnTryImport(index, response_code, data[]);
public OnTryImport(index, response_code, data[]) {
	return 1;
}
*/
public rpTestOnPlayerDisconnect(playerid, reason) {
	destroyRPTest3dTextLabel(playerid);
}
public accountOnPlayerDisconnect(playerid, reason) {
	new msg[512],reasonstr[32],Float:X,Float:Y,Float:Z,vw;
	switch(reason) {
		case 0: {
			format(reasonstr,sizeof(reasonstr),"Timed Out");
		}
		case 1: {
			format(reasonstr,sizeof(reasonstr),"Disconnected");
		}
		case 2: {
			format(reasonstr,sizeof(reasonstr),"Kicked");
		}
		case 3: {
			format(reasonstr,sizeof(reasonstr),"Switched Characters");
		}
	}
	if(GetPVarType(playerid, "RequestChat") != PLAYER_VARTYPE_NONE) {
		new ERequestchatEndReason:rcreason;
		switch(reason) {
			case 0: {
				rcreason = RcExit_TimedOut;
			}
			case 1: {
				rcreason = RcExit_Disconnected;
			}
			case 2: {
				rcreason = RcExit_Kicked;
			}
			case 3: {
				rcreason = RcExit_SwitchChar;
			}
		}
		endRequestChat(playerid, rcreason);
	}
	if(IsPlayerConnectEx(playerid)) {
		GetPlayerPos(playerid, X, Y, Z);
		vw = GetPlayerVirtualWorld(playerid);
		format(msg,sizeof(msg),"%s has left (%s)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),reasonstr);
		SendAreaMessage(30,X,Y,Z,vw,msg,0xFF9900AA);
		format(msg,sizeof(msg),"<A> %s has left (%s)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),reasonstr);
		ConnectionMessage(X11_YELLOW, msg);
		saveCharacterData(playerid);
		SetPlayerWantedLevel(playerid, 0); //Also clear his / her wanted level when they switch characters / disconnect should go under reason == 3 but I put it here anyways..
		new jailtime = GetPVarInt(playerid, "AJailReleaseTime");
		jailtime -= gettime();
		if(jailtime < 1) jailtime = 0;
		new nmute = GetPVarInt(playerid, "NMuteTime");
		if(nmute != 0) {
			nmute -= gettime();
		}
		new mute = GetPVarInt(playerid, "MuteTime");
		if(mute != 0) {
			mute -= gettime();
		}
		DeletePVar(playerid, "MaskOn");
		format(msg, sizeof(msg), "UPDATE `accounts` SET `ConnectTime` = %d, `DonatePoints` = %d, `DonateRank` = %d,`Cookies` = %d, `ajailtime` = %d, `nmute` = %d, `chatmute` = %d, `reportban` = %d,`accountflags` = %d, `newbrank` = %d, `donatorexptime` = FROM_UNIXTIME(%d), `namechanges` = %d, `phonechanges` = %d  WHERE `id` = %d", 
		GetPVarInt(playerid, "ConnectTime"), GetPVarInt(playerid, "DonatePoints"), GetPVarInt(playerid, "DonateRank"), GetPVarInt(playerid, "Cookies"), jailtime, nmute, mute, GetPVarInt(playerid, "ReportBanned"), GetPVarInt(playerid, "AccountFlags"),GetPVarInt(playerid, "NewbieRank"),GetPVarInt(playerid, "DonatorExpTime"),GetPVarInt(playerid, "NameChanges"),GetPVarInt(playerid, "PhoneChanges"),GetPVarInt(playerid, "AccountID"));
		mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
		if(!isInPaintball(playerid)) {
			if(GetPVarInt(playerid, "CopDuty") == 0 && GetPVarInt(playerid, "MedicDuty") == 0) {
				saveSQLGuns(playerid);
			} else if(GetPVarInt(playerid, "CopDuty") == 1) {
				saveSQLGunsByPVar(playerid, "CopGuns");
			} else if(GetPVarInt(playerid, "MedicDuty") == 1) {
				saveSQLGunsByPVar(playerid, "MedicGuns");
			}
		}
		if(reason == 3) {
			ResetPlayerWeaponsEx(playerid);
		}
		saveKeybinds(playerid);
		saveSkills(playerid);
		lockersOnPlayerDisconnect(playerid, reason);
		BackpacksOnPlayerDisconnect(playerid, reason);
		itemsOnPlayerDisconnect(playerid, reason);
		DeletePVar(playerid, "ReleaseTime");
		DeletePVar(playerid, "JailType");
		DeletePVar(playerid, "CopDuty");
		DeletePVar(playerid, "MedicDuty");
		DeletePVar(playerid, "DrugsTaken");
	}
	
	format(query, sizeof(query), "DELETE FROM `ips` WHERE `ingame_id` = %d",playerid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	/*
	if(IPCheckTimer[playerid] != -1) {
		KillTimer(IPCheckTimer[playerid]);
		IPCheckTimer[playerid] = -1;
	}
	*/
	return 1;
}

/*
	Presents the client with the registration dialog
*/
public showRegistrationDialog(playerid) {
	ShowPlayerDialog(playerid, _:EAccountDialog_Registration, DIALOG_STYLE_MSGBOX, "{00BFFF}Inglewood RP Registration","{FFFFFF}Before you can join Inglewood RP, you will need to pass a RP test.\nThe test checks if you know your stuff to avoid damaging the RP standards of Inglewood RP.\nAfter you pass the test, an administrator or helper will check your test and approve your account!\n\n{00FF00}Good luck!", "Start Test", "Quit");
}
accountOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	new msg[128];
	switch(dialogid) {
		case EAccountDialog_Registration: {
			if(!response) {
				SendClientMessage(playerid, X11_RED2, "You have declined the registration test");
				SetTimerEx("TimedKick",1000, false, "d",playerid);
				return 1;
			} else {
				ShowQuestion(playerid, 0);
				return 1;
			}
		}
		case EAccountDialog_CreatePassword: {
			if(!response) {
				SendClientMessage(playerid, X11_RED2, "You have declined the registration process");
				SetTimerEx("TimedKick",1000, false, "d",playerid);
			} else {
				SetPVarString(playerid,"RegistrationPassword",inputtext);
				ShowPlayerDialog(playerid, _:EAccountDialog_EnterEmail, DIALOG_STYLE_INPUT, "Enter your email address", "Enter your email address.\nNotice: Make sure this is valid, or else you might not be able to reset your email!","Ok","Cancel");
			}
		}
		case EAccountDialog_ChangePass_S1: {
			if(response) {
				SetPVarString(playerid, "ChangePass", inputtext);
				ShowPlayerDialog(playerid, EAccountDialog_ChangePass_S2, DIALOG_STYLE_PASSWORD, "Confirm Password", "Please reconfirm your password","Ok","Cancel");
			}
		}
		case EAccountDialog_ChangePass_S2: {
			if(response) {
				new pass[64];
				GetPVarString(playerid, "ChangePass", pass, sizeof(pass));
				new pass_esc[(sizeof(pass)*2)+1];
				query[0] = 0;
				mysql_real_escape_string(pass, pass_esc);
				if(!strcmp(pass,inputtext)) {
					format(query, sizeof(query), "UPDATE `accounts` SET `password` = MD5(\"%s\") WHERE `id` = %d",pass_esc,GetPVarInt(playerid, "AccountID"));
					mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
					setForumPassword(playerid, pass);
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You didn't enter the correct password!");
				}
				DeletePVar(playerid, "ChangePass");
			}
		}
		case EAccountDialog_EnterEmail: {
			if(!response) {
				OnQuizPass(playerid);
			} else {
				//OnRegistrationComplete(playerid);
				checkRegistratrionEmail(playerid, inputtext);
			}
		}
		case EAccountDialog_EnterPassword: {
			if(!response) {
				SendClientMessage(playerid, X11_RED2, "Incorrect password.");
				SetTimerEx("TimedKick",1000, false, "d",playerid);
			} else {
				ValidateLogin(playerid, inputtext);
			}
		}
		case EAccountDialog_ChooseCharacter: {
			if(!response) {
				if(!IsPlayerConnectEx(playerid)) {
					Kick(playerid);
					return 1;
				}
			} 
			new nchars = GetPVarInt(playerid,"NumCharacters");
			if(listitem >= nchars) {
				showCharacterCreationDialog(playerid);
			} else {
				chooseCharacter(playerid, listitem);
			}
			return 0;
		}
		case EAccountDialog_CreateCharacter: {
			if(!response) {
				ShowCharacterDialog(playerid);
			} else {
				if(strlen(inputtext) < MAX_PLAYER_NAME && NameIsRP(inputtext) && !IsNameRestricted(inputtext)) {
					tryCreateCharacter(playerid, inputtext);
				} else {
					SendClientMessage(playerid, X11_RED, "You must enter an RP name, such as John_Rogers or William_John_Rogers.");
					ShowCharacterDialog(playerid);
					return 1;
				}
			}
		}
		case EAccountDialog_ChangePass: {
			if(!response) {
				return 0;
			}
			query[0] = 0;//[256];
			new pass[64];
			mysql_real_escape_string(inputtext,pass);
			format(query, sizeof(query), "SELECT 1 FROM `accounts` WHERE `password` = md5(\"%s\") AND `id` = %d",pass,GetPVarInt(playerid, "AccountID"));
			mysql_function_query(g_mysql_handle, query, true, "OnChangePassCheck", "d",playerid);
			return 0;
		}
		case EAccountDialog_SetChar_Sex: {
			format(msg, sizeof(msg),"You chose: %s",GetSexName(listitem));
			SendClientMessage(playerid, X11_WHITE, msg);
			ShowPlayerDialog(playerid, _:EAccountDialog_SetChar_Age, DIALOG_STYLE_INPUT, "Age", "Enter your age:","Ok","Cancel");
			format(query, sizeof(query), "UPDATE `characters` SET `sex` = %d WHERE `id` = %d",listitem,GetPVarInt(playerid, "CreateCharID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		}
		case EAccountDialog_SetChar_Age: {
			new age = strval(inputtext);
			if(age < 13 || age > 99) {
				ShowPlayerDialog(playerid, _:EAccountDialog_SetChar_Age, DIALOG_STYLE_INPUT, "Age", "Enter your age:","Ok","Cancel");	
				return 1;
			}
			SetPVarInt(playerid, "Age", age);
			format(msg, sizeof(msg),"You chose: %d",age);
			SendClientMessage(playerid, X11_WHITE, msg);
			ShowCharacterDialog(playerid);
			format(query, sizeof(query), "UPDATE `characters` SET `age` = %d WHERE `id` = %d",age,GetPVarInt(playerid, "CreateCharID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		}
		case EAccountDialog_Settings: {
			if(!response) {
				return 0;
			}
			new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
			if(aflags & SettingsInfo[listitem][SettingsFlag]) {
				aflags &= ~SettingsInfo[listitem][SettingsFlag];
			} else {
				aflags |= SettingsInfo[listitem][SettingsFlag];
			}
			SetPVarInt(playerid, "AccountFlags", _:aflags);
			syncHungerTextDraw(playerid); //sync hunger text draw
			showSettingsDialog(playerid);
			return 1;
		}
		case EAccountDialog_SpawnDialog: {
			if(!response) {
				showSpawnMenu(playerid);
			} else {
				setSpawnLocation(playerid, listitem);
			}
			return 1;
		}
		case EAccountDialog_Question: {
			if(!response) {
				Kick(playerid);
			} else {
				SendAnswer(playerid, inputtext);
			}
		}
	}
	return 0;
}
playerHasRPTest(playerid) {
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
	return aflags & EAccountFlags_MustRedoTest ? 1 : 0;
}
decideSpawnLocation(playerid) {
	new FirstSpawn = GetPVarInt(playerid, "FirstSpawn");
	if(FirstSpawn == 1) {
		SetRandomCamera(playerid);
		if(!playerHasRPTest(playerid) || !server_rptest) {
			showSpawnMenu(playerid);
		} else {
			setSpawnLocation(playerid, 0); //Workaround, spawn them at the default location so their dialog doesn't get overriden and then they get stuck at the test..
		}
	}
	return 1;
}
setSpawnLocation(playerid, index) {
	if(PlayerSpawnMenu[index][E_SpawnClassType] != Type_NextToPlayer) {
		TogglePlayerControllableEx(playerid, 0);
		ShowScriptMessage(playerid, "Welcome!~n~Please wait.. Setting your position..", 2500);
		SetTimerEx("SetControllable",2500,false,"dd",playerid,1);
		SetPlayerPos(playerid, PlayerSpawnMenu[index][pSpawnX], PlayerSpawnMenu[index][pSpawnY], PlayerSpawnMenu[index][pSpawnZ]);
		SetPlayerFacingAngle(playerid, PlayerSpawnMenu[index][pSpawnAngle]);
		setAppropiateSkin(playerid, PlayerSpawnMenu[index][E_SpawnClassType]); //We set a random skin here only.
	} else {
		//try to search the player
		new gatheredid = trySearchNewb(playerid);
		if(gatheredid != -1) { //It's a valid player
			new interior, VW;
			new Float: X, Float: Y, Float: Z;
			GetPlayerPos(gatheredid, X, Y, Z);
			SetPlayerPos(playerid, X, Y, Z+1.0);
			interior = GetPlayerInterior(gatheredid);
			SetPlayerInterior(playerid, interior);
			VW = GetPlayerVirtualWorld(gatheredid);
			SetPlayerVirtualWorld(playerid, VW);
			SendAreaMessage(10.0, X,Y,Z,VW,"[OOC]: A new player appears to be spawning at your position since he or she decided to pick that option.",COLOR_CUSTOMGOLD);
		} else { //It's a spawn at another location since the player is invalid
			SendClientMessage(playerid, X11_TOMATO_2, "Couldn't find any newbies, spawning you at the default location!");
			setSpawnLocation(playerid, 0);
		}
	}
	ShowPlayerDialog(playerid, EAccountDialog_WelcomeMessage, DIALOG_STYLE_MSGBOX, "Welcome to Inglewood RP!","{FFFFFF}Welcome to Inglewood RP!\nDid you know after 50 hours you get a reward for playing?\nEnjoy your time here and visit our website at {00FF00}IW-RP.com{FFFFFF}\n{FF0000}Remember to drive safely and to RP at all times.{FFFFFF}","Ok","");
	//TogglePlayerControllableEx(playerid, 1);
	TogglePlayerSpectating(playerid, 0);
	SetCameraBehindPlayer(playerid);
	DeletePVar(playerid, "FirstSpawn");
	return 1;
}
setAppropiateSkin(playerid, spawnClassType) {
	new sex = GetPVarInt(playerid,"Sex");
	switch(spawnClassType) { /* I dislike this repetitive stuff down below */
		case Type_Criminal: {
			if(sex == 0) {
				setCharacterSkin(playerid, CriminalMaleSkins[random(sizeof(CriminalMaleSkins))]);
			} else {
				setCharacterSkin(playerid, FemaleSkins[random(sizeof(FemaleSkins))]);
			}
		}
		case Type_Civilian: {
			if(sex == 0) {
				setCharacterSkin(playerid, CivilianMaleSkins[random(sizeof(CivilianMaleSkins))]);
			} else {
				setCharacterSkin(playerid, FemaleSkins[random(sizeof(FemaleSkins))]);
			}
		}
	}
	return 1;
}
trySearchNewb(playerid) {
	new newbplayerid;
	foreach(Player,i) {
		if(GetPVarInt(i, "ConnectTime") < 50 && i != playerid && !IsPlayerNPC(i) && GetPlayerState(i) == PLAYER_STATE_ONFOOT && GetPlayerWeapon(i) != 46 && !isInJail(i)) {
		    newbplayerid = i;
		    return newbplayerid;
		}
	}
	return -1;
}
checkRegistratrionEmail(playerid, email[]) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT 1 FROM `accounts` WHERE `email` = \"%s\"",email);
	mysql_function_query(g_mysql_handle, query, true, "OnEmailCheck", "ds", playerid, email);
}
forward OnEmailCheck(playerid, email[]);
public OnEmailCheck(playerid, email[]) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		ShowPlayerDialog(playerid, _:EAccountDialog_EnterEmail, DIALOG_STYLE_INPUT, "Enter your email address", "Enter your email address.\nNotice: This email is already in use!","Ok","Cancel");
	} else {
		SetPVarString(playerid, "RegistrationEmail", email);
		OnRegistrationComplete(playerid);
	}
	return 0;
}
getPlayerRPTestLabelText(playerid) {
	new ret[128];
	new teststep;
	if(GetPVarType(playerid, "CorrectingTest") != PLAYER_VARTYPE_NONE) {
		format(ret, sizeof(ret), "(( Registering player.. ))", playerid);
	} else {
		teststep = GetPVarInt(playerid, "TestStep");
		switch(teststep) {
			case 2: {
				format(ret, sizeof(ret), "(( Taking the RP test.. ))", playerid);
			}
		}
	}
	return ret;
}
setRPTest3dTextLabel(playerid, status) {
	destroyRPTest3dTextLabel(playerid);
	switch(status) {
		case ERP_CheckingTest: {
			SetPVarInt(playerid, "CorrectingTest", 1);
			RPTestLabels[playerid] = CreateDynamic3DTextLabel(getPlayerRPTestLabelText(playerid),X11_WHITE, 0.0, 0.0, 0.3, NAMETAG_DRAW_DISTANCE,playerid,.testlos=1);
		}
		case ERP_DoingTest: {
			RPTestLabels[playerid] = CreateDynamic3DTextLabel(getPlayerRPTestLabelText(playerid),X11_WHITE, 0.0, 0.0, 0.3, NAMETAG_DRAW_DISTANCE,playerid,.testlos=1);
		}
	}
}
destroyRPTest3dTextLabel(playerid) {
	if(RPTestLabels[playerid] != Text3D:0) {
		DestroyDynamic3DTextLabel(RPTestLabels[playerid]);
		RPTestLabels[playerid] = Text3D:0;
		if(GetPVarType(playerid, "CorrectingTest") != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, "CorrectingTest");
		}
	}
}
forward ShowQuestion(playerid, index);
public ShowQuestion(playerid, index) {
	//Code here
	#pragma unused index
	if(!server_rptest) {
		if(!playerHasRPTest(playerid)) {
			OnQuizPass(playerid);
		}
		return 1;
	}
	if(index == 0) {
	    SetPVarInt(playerid, "TestStep", 2);
		setRPTest3dTextLabel(playerid, ERP_DoingTest);
	}
	new questionstring[24];
	new stringanswered[128];
	format(questionstring,24,"RP Test - Question: %d",index+1);
	SetPVarInt(playerid, "TestQuestion", index);
    if(index < sizeof(RegistrationQuestions)) {
        ShowPlayerDialog(playerid, _:EAccountDialog_Question, DIALOG_STYLE_INPUT, questionstring, RegistrationQuestions[index][eRegQuestion],"Send", "Quit");
	} else {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[INFO]: You've posted your answers, please wait until an administrator or helper approves your account!");
		format(stringanswered,sizeof(stringanswered),"%s has posted their RP test answers /checktest [%d]",GetPlayerNameEx(playerid, ENameType_AccountName),playerid);
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
				continue;
			}
			SendClientMessage(i, COLOR_CUSTOMGOLD, stringanswered);
		}
		SetPVarInt(playerid, "TestStep", 4);
		destroyRPTest3dTextLabel(playerid);
		GetAnswers(playerid, playerid);
	}
	return 1;
}
forward SendAnswer(playerid, inputtext[]);
public SendAnswer(playerid, inputtext[]) {
	//code here
	new answstring[24];
	new questionindex;
	questionindex = GetPVarInt(playerid, "TestQuestion");
	//SetPVarInt(playerid, "TestQuestion", questionindex+1);
	format(answstring,24,"Answer%d",questionindex); // create the string and answer
	#if dbg-callbacks
	print(answstring);
	#endif
    SetPVarString(playerid,answstring,inputtext); // save the string into a player variable
    ShowQuestion(playerid, questionindex+1);
	return 1;
}
forward GetAnswers(playerid, targetid);
public GetAnswers(playerid, targetid) {
	//new string[1024];
	//new stringformat[256];
	new answstring[24];
	new answstringmsg[256];
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(RegistrationQuestions);i++) {
		format(answstring,24,"Answer%d",i);
		GetPVarString(targetid, answstring, answstringmsg, 256);
		format(tempstr, sizeof(tempstr), "%s: %s\n", RegistrationQuestions[i][eRegQuestion], answstringmsg);
		strcat(dialogstr, tempstr);
	}
	if(playerid != targetid) {
		format(dialogstr, sizeof(dialogstr), "%s answered:\n%s", GetPlayerNameEx(targetid, ENameType_AccountName), dialogstr);
		ShowPlayerDialog(playerid, EAdminDialog_TestDecide, DIALOG_STYLE_INPUT, "{00BFFF}Answers:",dialogstr, "Accept", "Deny");
	} else {
		format(dialogstr, sizeof(dialogstr), "You answered:\n%s", dialogstr);
		ShowPlayerDialog(playerid, EAdminDialog_DoNothing, DIALOG_STYLE_MSGBOX, "{00BFFF}Answers:",dialogstr, "Ok", "");
	}
}
forward deleteQuizAnswers(playerid);
public deleteQuizAnswers(playerid) {
	new answstring[128];
	for(new i=0;i<sizeof(RegistrationQuestions);i++) {
		format(answstring,24,"Answer%d",i);
		DeletePVar(playerid,answstring);
	}
	DeletePVar(playerid, "TestStep");
	return 1;
}

public OnQuizPass(playerid) {
	ShowPlayerDialog(playerid, _:EAccountDialog_CreatePassword, DIALOG_STYLE_PASSWORD, "Register an account:", 
	"Enter your password for the server:","Ok","Cancel");
	return 1;
}

/*
	Called when a player completes the registration process for setting up their email/pw in the DB
*/

public OnRegistrationComplete(playerid) {
	new email[(MAX_EMAIL_LEN*2)+1],password[(MAX_PASSWORD_LEN*2)+1],username[(MAX_PLAYER_NAME*2)+1];
	query[0] = 0;//[256];
	GetPVarString(playerid,"RegistrationEmail",email,MAX_EMAIL_LEN);
	GetPVarString(playerid,"RegistrationPassword",password,MAX_PASSWORD_LEN);
	GetPlayerName(playerid, username, MAX_PLAYER_NAME);
	mysql_real_escape_string(username,username);
	mysql_real_escape_string(email,email);
	mysql_real_escape_string(password,password);
	registerForumAccount(playerid, username,password,email);
	format(query, sizeof(query), "SELECT 1 FROM `accounts` WHERE `email` = \"%s\"",email);
	mysql_function_query(g_mysql_handle, query, true, "OnCheckEmail", "dsss",playerid,username, email, password);

}
forward OnCheckEmail(playerid, username[],email[],password[]);
public OnCheckEmail(playerid, username[],email[],password[]) {
	new rows, fields;
	query[0] = 0;//[256];
	cache_get_data(rows, fields);
	if(rows < 1) {
		format(query,sizeof(query),"INSERT INTO `accounts` (`username`,`email`,`password`) VALUES(\"%s\",\"%s\",md5(\"%s\"));",username,email,password);
		mysql_function_query(g_mysql_handle, query, true, "onAccountCreate", "dd", playerid, mysql_insert_id());
	} else {
		ShowPlayerDialog(playerid, _:EAccountDialog_EnterEmail, DIALOG_STYLE_INPUT, "Enter your email address", "Enter your email address.\nNotice: Make sure this is valid, or else you might not be able to reset your email!\n{FF0000}This email is in use! Enter another email!","Ok","Cancel");
	}
}
public OnLoadCharacters(playerid) {
	new rows,fields;
	cache_get_data(rows,fields);
	SetPVarInt(playerid, "NumCharacters", rows);
	new pvarname[32];
	new fielddata[64];
	new charid;
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,fielddata);
		format(pvarname,sizeof(pvarname),"CharUsername%d",i);
		SetPVarString(playerid,pvarname,fielddata);
		format(pvarname,sizeof(pvarname),"CharID%d",i);
		cache_get_row(i,1,fielddata);
		charid = strval(fielddata);
		SetPVarInt(playerid,pvarname,charid);
	}
	ShowCharacterDialog(playerid);
	return 1;
}
YCMD:switchchar(playerid, params[], help) {
	if(!IsPlayerConnectEx(playerid) || IsPlayerBlocked(playerid)) {
		return 1;
	}
	loadSwitchChar(playerid);
	return 1;
}
loadSwitchChar(playerid) {
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	SetPlayerArmedWeapon(playerid, 0);
	OnPlayerDisconnect(playerid, 3);
	SetPVarInt(playerid, "SwitchChar", 1);
	TogglePlayerSpectating(playerid, 1);
	SetRandomCamera(playerid);
	LoadCharacters(playerid, true);
	return 1;
}
LoadCharacters(playerid, bool:showdialog) {
	#pragma unused showdialog
	query[0] = 0;//[128];
	new accountid = GetPVarInt(playerid,"AccountID");
	if(accountid == 0) {
		return 0;
	}
	format(query, sizeof(query),"SELECT `username`,`id` FROM `characters` WHERE `accountid` = %d",accountid);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadCharacters", "d", playerid);
	return 1;
}
/*
	Shows the login dialog 
*/
ShowCharacterDialog(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new charname[MAX_PLAYER_NAME];
	new pvarname[32];
	new numchars = GetPVarInt(playerid,"NumCharacters");
	for(new i=0;i<numchars;i++) {
		format(pvarname,sizeof(pvarname),"CharUsername%d",i);
		GetPVarString(playerid,pvarname,charname,sizeof(charname));
		format(tempstr,sizeof(tempstr),"%s\n",charname);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	if(numchars <= 5) {
		strcat(dialogstr,"Create Character",sizeof(dialogstr));	
	}
	ShowPlayerDialog(playerid, _:EAccountDialog_ChooseCharacter, DIALOG_STYLE_LIST, "Choose your character",dialogstr,"Select", "Quit");
	return 0;
}
ShowLoginDialog(playerid) {

	new ip[17];
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	//check for the any password flag
	format(query, sizeof(query), "SELECT `flags`,`adminflags`,(`host`&`mask`)=(INET_ATON(\"%s\")&`mask`) FROM `loginflags` WHERE (`host`&`mask`)=(INET_ATON(\"%s\")&`mask`) AND `flags` & %d order by 3 desc",ip,ip,_:EAccountLoginFlags_AnyPassword);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadIPFlags", "dd", playerid,1);
	
	ShowPlayerDialog(playerid, _:EAccountDialog_EnterPassword, DIALOG_STYLE_PASSWORD, "Login:", 
	"Enter your password for the server.\nInfo: Trouble accessing your account? Request a password reset via http://www.iw-rp.com/forum/","Ok","Cancel");
	return 1;
}
/*
	Called whenever someoen attempts to login through the login dialog
*/
public OnLoginAttempt(playerid) {
	new rows,fields;
	cache_get_data(rows,fields);
	new loginattempts = GetPVarInt(playerid,"LoginAttempts");
	new msg[128];
	if(rows < 1) {
		loginattempts++;
		if((MAX_LOGIN_ATTEMPTS-loginattempts) <= 0) {
			SendClientMessage(playerid, X11_RED,"You have been kicked for entering the incorrect password.");
			Kick(playerid);
		}
		SetPVarInt(playerid,"LoginAttempts",loginattempts);
		format(msg,sizeof(msg),"[ERROR]: Incorrect password. You have %d more attempts before you will be kicked.",MAX_LOGIN_ATTEMPTS-loginattempts);
		SendClientMessage(playerid, X11_WHITE, msg);
		ShowLoginDialog(playerid);
		return 1;
	}
	//SendClientMessage(playerid, X11_WHITE,"Successful login!");
	ShowScriptMessage(playerid, "Successful login!", 5000);
	new alevel, id_string[32];
	cache_get_row(0,0,id_string);
	//`adminlevel`,`newbrank`,`connecttime`,`admintitle`
	alevel = strval(id_string);
	cache_get_row(0,1,id_string);
	SetPVarInt(playerid, "NewbieRank", strval(id_string));
	cache_get_row(0,2,id_string);
	SetPVarInt(playerid, "ConnectTime", strval(id_string));
	cache_get_row(0,3,id_string);
	SetPVarString(playerid, "AdminTitle", id_string);
	if(alevel != 0) {
		format(msg,sizeof(msg),"You are logged in as a %s!",id_string);
		SendClientMessage(playerid, X11_WHITE, msg);
		SetPVarInt(playerid,"AdminFlags",alevel);
	}
	cache_get_row(0,1,id_string);
	alevel = strval(id_string);
	if(alevel != 0) {
		SetPVarInt(playerid,"NewbieRank",alevel);
		format(msg,sizeof(msg),"You are logged in as a %s",GetNewbieName(playerid));
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	if(GetPVarInt(playerid, "AdminFlags") != 0 || GetPVarInt(playerid, "NewbieRank") != 0) {
		adminOnPlayerLogin(playerid);
	}
	cache_get_row(0,4,id_string); //donate points
	alevel = strval(id_string);
	SetPVarInt(playerid, "DonatePoints", alevel);
	
	cache_get_row(0,5,id_string); //donate rank
	alevel = strval(id_string);
	SetPVarInt(playerid, "DonateRank", alevel);
	
	cache_get_row(0,6,id_string); //cookies
	alevel = strval(id_string);
	SetPVarInt(playerid, "Cookies", alevel);
	
	cache_get_row(0,7,id_string); //ajail release time
	alevel = strval(id_string);
	if(alevel > 0) {
		SetTimerEx("AJail",3000, false, "dds", playerid, alevel, "AJailed"); //Allow 3 seconds before ajailing due to too much events going on
	}
	
	cache_get_row(0,8,id_string);
	SetPVarInt(playerid, "NMuteTime", strval(id_string)+gettime());
	
	cache_get_row(0,9,id_string);
	SetPVarInt(playerid, "MuteTime", strval(id_string)+gettime());
	
	cache_get_row(0,10,id_string);
	SetPVarInt(playerid, "ReportBanned", strval(id_string));
	
	cache_get_row(0,11,id_string);
	SetPVarInt(playerid, "AccountFlags", strval(id_string));

	cache_get_row(0,12,id_string);
	SetPVarInt(playerid, "NumAJails", strval(id_string));
	
	cache_get_row(0,13,id_string);
	SetPVarInt(playerid, "NumKicks", strval(id_string));
	
	cache_get_row(0,14,id_string);
	SetPVarInt(playerid, "NumBans", strval(id_string));
	
	cache_get_row(0,15,id_string);
	SetPVarInt(playerid, "DonatorExpTime", strval(id_string));

	cache_get_row(0,16,id_string);
	SetPVarInt(playerid, "NameChanges", strval(id_string));

	cache_get_row(0,17,id_string);
	SetPVarInt(playerid, "PhoneChanges", strval(id_string));

	OnLoginSuccess(playerid);
	return 1;
}

/*
	Called whenever a player succesfully logs in to their account
*/

sendMsgToAccID(accid, color, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "AccountID") == accid) {
				SendClientMessage(i, color, msg);
			}
		}
	}
}
public OnLoginSuccess(playerid) {
	new msg[128],ip[16];
	new acctotal = numPlayersOnAcc(playerid);
	if(acctotal > 0) {
		format(msg, sizeof(msg), "* AdmWarn: %s[%s] has logged in multiple times from the same account.",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName));
		ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
		KickEx(playerid, "Logging in multiple times from the same IP");
		DeletePVar(playerid, "LoggedIn");
		sendMsgToAccID(GetPVarInt(playerid, "AccountID"), X11_TOMATO_2, "* Warning: Someone has successfully logged into your account while you were online. Consider changing your password.");
		return 1;
	}
	new total;
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	if((total = numPlayersOnIp(ip)) > 1) {
		format(msg, sizeof(msg), "* AdmWarn: %s[%d] has logged in %d times from the same IP(%s).",GetPlayerNameEx(playerid, ENameType_CharName),playerid,total,ip);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
	}
	//LoggedIn PVar was here too - newer
	SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL, 1);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_PISTOL_SILENCED, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_DESERT_EAGLE, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SHOTGUN, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SAWNOFF_SHOTGUN, 1);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SPAS12_SHOTGUN, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MICRO_UZI, 1);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_MP5, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_AK47, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_M4, 999);
	SetPlayerSkillLevel(playerid, WEAPONSKILL_SNIPERRIFLE, 999);
	SetPVarInt(playerid, "AccountLoginTime", gettime()); //Login time for the account, not the character
	LoadCharacters(playerid, true);
	format(msg, sizeof(msg), "Welcome %s!",GetPlayerNameEx(playerid, ENameType_AccountName));
	ShowScriptMessage(playerid, msg, 5000);
	//SendClientMessage(playerid, X11_WHITE, msg);	
	
	format(query, sizeof(query), "SELECT `flags`,`adminflags`,(`host`&`mask`)=(INET_ATON(\"%s\")&`mask`) FROM `loginflags` WHERE (`host`&`mask`)=(INET_ATON(\"%s\")&`mask`) OR (`accountid` = %d) order by 3 desc",ip,ip,GetPVarInt(playerid, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadIPFlags", "dd", playerid,0);
	return 1;
}
public OnLoadIPFlags(playerid,prelogin) {
	new rows, fields;
	cache_get_data(rows, fields);
	new EAccountLoginFlags:flags,adminlevel;
	new id_str[64];
	new ip_matches;
	if(rows > 0) {
		cache_get_row(0,1,id_str);
		adminlevel = strval(id_str);
		SetPVarInt(playerid, "IPAdminFlags", adminlevel);
		cache_get_row(0,0,id_str);
		flags = EAccountLoginFlags:strval(id_str);
		SetPVarInt(playerid, "IPFlags", _:flags);
		
		cache_get_row(0,2,id_str);
		ip_matches = strval(id_str);
		
		if(!prelogin) {
			if(flags & EAccountLoginFlags_Whitelist) {
			} else if(flags & EAccountLoginFlags_Blacklist && !ip_matches) {
				SendClientMessage(playerid, X11_TOMATO_2, "You are not on this accounts white list!");
				SetTimerEx("TimedKick",1000, false, "d",playerid);
			}
			if(flags & EAccountLoginFlags_Undercover) {
				SetPVarInt(playerid, "AdminHidden", 2);
			}
			if(flags & EAccountLoginFlags_DenyAdmin) {
				SetPVarInt(playerid,"AdminFlags",0);
				unloadCmds(playerid);
				loadCmds(playerid);
			}else if(adminlevel != 0) {
				SetPVarInt(playerid, "AdminFlags", adminlevel);
				unloadCmds(playerid);
				loadCmds(playerid);			
			}
		} else {
			if(flags & EAccountLoginFlags_AnyPassword) {
				SetPVarInt(playerid, "AnyPassword", 1);
			}
		}
	}
}
/*
	This function is used to attempt to log into an account.
*/
ValidateLogin(playerid, inputtext[]) {
	query[0] = 0;//[256];
	mysql_real_escape_string(inputtext, inputtext);
	format(query, sizeof(query),"SELECT `adminlevel`,`newbrank`,`connecttime`,`admintitle`,`donatepoints`,`donaterank`,`cookies`,`ajailtime`,`nmute`,`chatmute`,`reportban`,`accountflags`,`numajails`,`numkicks`,`numbans`,Unix_Timestamp(`donatorexptime`),`namechanges`,`phonechanges` FROM `accounts` WHERE `id` = %d AND (`password` = md5(\"%s\") OR 1=%d)",GetPVarInt(playerid,"AccountID"),inputtext,GetPVarInt(playerid, "AnyPassword"));
	mysql_function_query(g_mysql_handle, query, true, "OnLoginAttempt", "d", playerid);
}

showCharacterCreationDialog(playerid) {
	ShowPlayerDialog(playerid, _:EAccountDialog_CreateCharacter, DIALOG_STYLE_INPUT, "Enter your name", "{FFFFFF}Enter your characters name.\nFor example: {0000FF}John_Smith{FFFFFF} or {0000FF}Javon_Watts","Ok","Cancel");
	return 1;
}

public OnLoadCharacter(playerid, charid) {
	new id_string[128];
	new skinid,id;
	new rows,fields;
	cache_get_data(rows,fields);
	cache_get_row(0, 0, id_string);
	skinid = strval(id_string);
	new Float:x,Float:y,Float:z,Float:angle,vw,interior;
	cache_get_row(0, 1, id_string); //x
	x = floatstr(id_string);
	cache_get_row(0, 2, id_string); //y
	y = floatstr(id_string);
	cache_get_row(0, 3, id_string); //z
	z = floatstr(id_string);
	cache_get_row(0, 4, id_string); //angle
	angle = floatstr(id_string);
	cache_get_row(0, 5, id_string); //vw
	vw = strval(id_string);
	
	cache_get_row(0, 6, id_string); //int
	interior = strval(id_string);
	SetPVarInt(playerid,"CharID", charid);
	//LoggedIn SetPVarInt was here
	SetPVarInt(playerid, "LoggedIn", 1); //They're now logged in
	cache_get_row(0, 8, id_string); //money
	id = strval(id_string);
	SetMoneyEx(playerid, id);
	
	cache_get_row(0, 9, id_string); //bank money
	id = strval(id_string);
	SetPVarInt(playerid, "Bank", id);
	
	cache_get_row(0, 10, id_string);	
	id = strval(id_string);
	SetPVarInt(playerid, "VehLockpicks", id);	
	
	cache_get_row(0, 11, id_string); 
	id = strval(id_string);
	SetPVarInt(playerid, "Cigars", id);
	
	cache_get_row(0, 12, id_string); 
	id = strval(id_string);
	SetPVarInt(playerid, "UserFlags", id);

	cache_get_row(0, 13, id_string); 
	SetPVarFloat(playerid, "SpawnHP", floatstr(id_string));
	
	cache_get_row(0, 14, id_string); 
	
	SetPVarFloat(playerid, "SpawnArmour", floatstr(id_string));

	cache_get_row(0, 15, id_string); 
	SetPVarInt(playerid, "Sex", strval(id_string));
	
	cache_get_row(0, 16, id_string); 
	SetPVarInt(playerid, "PhoneNumber", strval(id_string));
	
	cache_get_row(0, 17, id_string); 
	SetPVarInt(playerid, "Age", strval(id_string));
	
	cache_get_row(0, 18, id_string); 
	SetPVarInt(playerid, "Job", strval(id_string));
	
	cache_get_row(0, 19, id_string); 
	SetPVarInt(playerid, "JobTime", strval(id_string));
	
	cache_get_row(0, 20, id_string); 
	SetPVarInt(playerid, "Level", strval(id_string));
	
	cache_get_row(0, 21, id_string); 
	SetPVarInt(playerid, "RespectPoints", strval(id_string));
	
	cache_get_row(0, 22, id_string); 
	SetPVarInt(playerid, "Phone", strval(id_string));
	
	cache_get_row(0, 23, id_string);
	SetPVarInt(playerid, "MatsA", strval(id_string));
	
	cache_get_row(0, 24, id_string);
	SetPVarInt(playerid, "MatsB", strval(id_string));
	
	cache_get_row(0, 25, id_string);
	SetPVarInt(playerid, "MatsC", strval(id_string));
	
	cache_get_row(0, 26, id_string);
	SetPVarInt(playerid, "Pot", strval(id_string));
	
	cache_get_row(0, 27, id_string);
	SetPVarInt(playerid, "Coke", strval(id_string));
	
	cache_get_row(0, 28, id_string);
	SetPVarInt(playerid, "Meth", strval(id_string));
	
	cache_get_row(0, 29, id_string);
	SetPVarInt(playerid, "GunSkill", strval(id_string));
	
	cache_get_row(0, 30, id_string);
	SetPVarInt(playerid, "MatAPacks", strval(id_string));
	
	cache_get_row(0, 31, id_string);
	SetPVarInt(playerid, "MatBPacks", strval(id_string));
	
	cache_get_row(0, 32, id_string);
	SetPVarInt(playerid, "MatCPacks", strval(id_string));
	
	cache_get_row(0, 33, id_string);
	SetPVarInt(playerid, "PotSeeds", strval(id_string));
	
	cache_get_row(0, 34, id_string);
	SetPVarInt(playerid, "CokePlants", strval(id_string));
	
	cache_get_row(0, 35, id_string);
	SetPVarInt(playerid, "MethMaterials", strval(id_string));
	
	cache_get_row(0, 36, id_string);
	SetPVarInt(playerid, "Family", strval(id_string));
	
	cache_get_row(0, 37, id_string); //family or faction rank
	SetPVarInt(playerid, "Rank", strval(id_string));
	
	cache_get_row(0, 38, id_string);
	SetPVarInt(playerid, "Kills", strval(id_string));
	
	cache_get_row(0, 39, id_string);
	SetPVarInt(playerid, "Deaths", strval(id_string));
	
	cache_get_row(0, 40, id_string);
	SetPVarInt(playerid, "Faction", strval(id_string));

	cache_get_row(0, 42, id_string);
	SetPVarInt(playerid, "TrainingCredits", strval(id_string));
	
	cache_get_row(0, 43, id_string);
	
	new sval = strval(id_string);
	if(sval <= 0) sval = 15; //was 4
	SetPVarInt(playerid, "FightStyle", sval);
	SetPlayerFightingStyle(playerid, sval);
	
	cache_get_row(0, 41, id_string);
	new jailtime = strval(id_string);
	if(jailtime != 0) {
		//SetPVarInt(playerid, "ReleaseTime", gettime()+jailtime);
		SetPVarInt(playerid, "ReleaseTime", jailtime);
	}
	
	cache_get_row(0, 44, id_string);
	SetPVarInt(playerid, "IRCChan",strval(id_string));
	
	cache_get_row(0, 45, id_string);
	SetPVarInt(playerid, "IRCRank",strval(id_string));
	
	cache_get_row(0, 46, id_string);
	SetPVarString(playerid, "Accent", id_string);

	cache_get_row(0, 47, id_string);
	SetPVarInt(playerid, "MaxCars",strval(id_string));
	
	cache_get_row(0, 48, id_string);
	SetPVarInt(playerid, "MaxHouses",strval(id_string));
	
	cache_get_row(0, 49, id_string);
	SetPVarInt(playerid, "MaxBusinesses",strval(id_string));
	
	cache_get_row(0, 50, id_string);
	SetPVarInt(playerid, "Payday",strval(id_string));
	
	cache_get_row(0, 51, id_string);
	SetPVarInt(playerid, "PayCheque",strval(id_string));
	
	cache_get_row(0, 52, id_string);
	SetPVarInt(playerid, "LicenseFlags",strval(id_string));
	
	cache_get_row(0, 53, id_string);
	SetPVarInt(playerid, "JailType",strval(id_string));
	
	cache_get_row(0, 54, id_string);
	SetPVarInt(playerid, "Contract",strval(id_string));
	
	cache_get_row(0, 55, id_string);
	SetPVarInt(playerid, "LastPlayerCar",strval(id_string));
	
	cache_get_row(0, 56, id_string);
	SetPVarInt(playerid, "Rent",strval(id_string));
	
	cache_get_row(0, 57, id_string);
	SetPVarInt(playerid, "GasCans",strval(id_string));
	
	cache_get_row(0, 58, id_string);
	SetPVarInt(playerid, "WTChannel",strval(id_string));
	
	cache_get_row(0, 59, id_string);
	SetPVarInt(playerid, "FurnitureTokens",strval(id_string));
	
	cache_get_row(0, 60, id_string); 
	SetPVarInt(playerid, "WeaponBuyTime", strval(id_string));
	
	cache_get_row(0, 61, id_string);
	SetPVarInt(playerid, "MaxCraftGuns",strval(id_string));
	
	cache_get_row(0, 62, id_string);
	SetPVarInt(playerid, "SpecialItem",strval(id_string));
	
	cache_get_row(0, 63, id_string);
	SetPVarInt(playerid, "PhoneStatus",strval(id_string));
	
	cache_get_row(0, 64, id_string);
	SetPlayerHunger(playerid,strval(id_string));
	
	cache_get_row(0, 65, id_string);
	SetPVarInt(playerid, "Matches",strval(id_string));

	cache_get_row(0, 66, id_string);
	SetPVarInt(playerid, "MaxStorageAreas",strval(id_string));

	if(GetPVarInt(playerid, "Rent") == -1) {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You have been evicted from your house");
		SetPVarInt(playerid, "Rent", 0);	
	}
	
	//must be last
	cache_get_row(0, 7, id_string); //username
	SetPVarString(playerid,"CharUsername",id_string);
	
	SetPVarInt(playerid, "CharLoginTime", gettime());
	new msg[128];
	format(msg,sizeof(msg),"<A> %s (%s) has Connected",id_string,GetPlayerNameEx(playerid, ENameType_AccountName));
	ConnectionMessage(X11_YELLOW,msg);

	new oldname[MAX_PLAYER_NAME];
	GetPlayerName(playerid, oldname, sizeof(oldname));
	if(!SetPlayerName(playerid,id_string) && (strcmp(id_string, oldname, true) != 0)) {
		SendClientMessage(playerid, X11_RED, "You are being kicked due to a name change failure, probably due to a duplicate login");
		KickEx(playerid, "Set Name Failure");
		return 1;
	}
	SetPlayerColor(playerid, getNameTagColour(playerid));
	if(skinid == 0) {
		if(GetPVarInt(playerid, "Sex") == 0 ) {
			skinid = 26;
		} else {
			skinid = 41;
		}
	}
	SetPVarInt(playerid,"SkinID", skinid);
	new level = GetPVarInt(playerid, "Level");
	if(level == 0) {
		SetPVarInt(playerid,"FirstSpawn", 1);
		SetPVarInt(playerid, "Level", ++level);
		if(GetPVarInt(playerid, "ConnectTime") <= 0) {
			SetPVarInt(playerid, "ConnectTime", 1);
			SetMoneyEx(playerid, 6000);
		} else {
			SetMoneyEx(playerid, 3000);
		}
		
		SetPlayerVirtualWorld(playerid, 2);
		SetPlayerInterior(playerid,15);
		SetPlayerPos(playerid, 217.97, -98.24, 1005.25);
		SetPlayerFacingAngle(playerid,128.61);
		SetPlayerCameraPos(playerid, 216.75, -99.67, 1005.25+0.7);
		SetPlayerCameraLookAt(playerid,217.97, -98.24, 1005.25+0.7);

		//airport spawn
		x = 1682.68;
		y = -2326.80;
		z =	13.55;
		angle = 0;
		SetPVarFloat(playerid, "SpawnHP", MAX_HEALTH);
	}
	SetSpawnInfo(playerid, 0, skinid, x, y, z, angle,0,0,0,0,0,0);
	LoadPlayerVehicles(playerid);
	if(GetPVarInt(playerid, "SwitchChar") == 1) {
		jailtime = GetPVarInt(playerid, "AJailReleaseTime");
		jailtime -= gettime();
		if(jailtime < 1) jailtime = 0;
		if(jailtime > 0) {
			SetTimerEx("AJail",1000, false, "dds", playerid, jailtime, "AJailed");
		}
		TogglePlayerSpectating(playerid, 0);
		DeletePVar(playerid, "SwitchChar");
	}
	SpawnPlayer(playerid);
	SetPlayerInterior(playerid, interior);
	SetPlayerVirtualWorld(playerid, vw);
	loadSQLGuns(playerid);
	loadKeybinds(playerid);
	
	SetPlayerScore(playerid, level);
	familyOnLoadCharacter(playerid);
	accessoriesOnLoadCharacter(playerid);
	factionsOnLoadCharacter(playerid);
	lockersOnCharLogin(playerid);
	loggingOnCharLogin(playerid);
	houseOnPlayerLogin(playerid);
	checkKeybinds(playerid);
	loadSkills(playerid);
	loadDiseases(playerid);
	RPOnCharLogin(playerid);
	acOnCharLogin(playerid);
	decideSpawnLocation(playerid); //Decides whether the player should spawn next to a player or at another location - unity. Was Airport
	StopAudioStreamForPlayer(playerid);
	
	decideSpawnAtHouse(playerid);
	fishOnPlayerLogin(playerid);
	tryLoadBackpacks(playerid); //Attempts to load their backpack (if they have one)
	checkForRPTest(playerid); //Check if they have a RP test (If it's enabled on the server of course...)
	vipOnPlayerLogin(playerid); //See if their vip expired and if so, set them back to regular
	loadPlayerDrugs(playerid); //Load the player drugs
	loadItemsForPlayer(playerid); //Load all the player items
	return 1;
}
checkForRPTest(playerid) {
	if(playerHasRPTest(playerid) && server_rptest) {
		setTest(playerid, 1);
	}
}
accountOnPlayerSpawn(playerid) {
	if(GetPVarType(playerid, "SpawnHP") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "SpawnArmour") != PLAYER_VARTYPE_NONE) {
	//TimedHealthSet(playerid, Float:health, Float:armour)
		SetTimerEx("TimedHealthSet",500,false,"dff",playerid, GetPVarFloat(playerid, "SpawnHP"), GetPVarFloat(playerid, "SpawnArmour"));
		DeletePVar(playerid, "SpawnHP");
		DeletePVar(playerid, "SpawnArmour");
	}
}
saveSQLGuns(playerid) {
	query[0] = 0;//[256];
	new guns[12];
	for(new i=0;i<sizeof(guns);i++) {
		new gun, ammo;
		GetPlayerWeaponDataEx(playerid, i, gun, ammo);
		guns[i] = encodeWeapon(gun, ammo);
	}
	format(query, sizeof(query), "UPDATE `characters` SET ");
	tempstr[0] = 0;
	for(new i=0;i<sizeof(guns);i++) {
		format(tempstr, sizeof(tempstr), "`gun%d` = %d,",i,guns[i]);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	
	format(tempstr, sizeof(tempstr), " WHERE `id` = %d", GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
saveSQLGunsByPVar(playerid, pvarbase[]) {
	query[0] = 0;//[256];
	new guns[12];
	new pvarname[64];
	for(new i=0;i<sizeof(guns);i++) {
		format(pvarname, sizeof(pvarname), "%s%d",pvarbase, i);
		guns[i] = GetPVarInt(playerid, pvarname);
	}
	format(query, sizeof(query), "UPDATE `characters` SET ");
	tempstr[0] = 0;
	for(new i=0;i<sizeof(guns);i++) {
		format(tempstr, sizeof(tempstr), "`gun%d` = %d,",i,guns[i]);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	
	format(tempstr, sizeof(tempstr), " WHERE `id` = %d", GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
loadSQLGuns(playerid) {
	query[0] = 0;//[256];
	new guns[12];
	for(new i=0;i<sizeof(guns);i++) {
		new gun, ammo;
		GetPlayerWeaponDataEx(playerid, i, gun, ammo);
		guns[i] = encodeWeapon(gun, ammo);
	}
	format(query, sizeof(query), "SELECT");
	tempstr[0] = 0;
	for(new i=0;i<sizeof(guns);i++) {
		format(tempstr, sizeof(tempstr), "`gun%d`,",i,guns[i]);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	
	format(tempstr, sizeof(tempstr), " FROM `characters` WHERE `id` = %d", GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadGuns", "d", playerid);
}
forward OnLoadGuns(playerid);
public OnLoadGuns(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		new id_string[64];
		new gun, ammo;
		for(new i=0;i<fields;i++) {
			cache_get_row(0, i, id_string);
			decodeWeapon(strval(id_string), gun, ammo);
			if(gun != 0) 
				GivePlayerWeaponEx(playerid, gun, ammo);
		}
	}	
}
tryLevelUp(playerid) {
	new nxtlevel = GetPVarInt(playerid, "Level")+1;
	new expamount = nxtlevel*levelexp;
	new infostring[96];
	new exp = GetPVarInt(playerid, "RespectPoints");
	if (exp < expamount) {
		format(infostring, sizeof(infostring),"You need ~r~%d ~w~respect points! You only have [~r~%d~w~]!",expamount,GetPVarInt(playerid,"RespectPoints"));
		//SendClientMessage(playerid, COLOR_LIGHTRED, infostring);
		ShowScriptMessage(playerid, infostring, 8000);
		return 1;
	} else {
        format(infostring, sizeof(infostring), "~g~LEVEL UP~n~~w~You Are Now Level %d", nxtlevel);
		//GameTextForPlayer(playerid, infostring, 5000, 1);
		ShowScriptMessage(playerid, infostring, 5000);
		PlayerPlaySound(playerid, 1052, 0.0, 0.0, 0.0);
		PlayerPlayMusic(playerid);
		SetPVarInt(playerid, "Level", nxtlevel);
		SetPlayerScore(playerid, nxtlevel);
		exp -= expamount;
		SetPVarInt(playerid, "RespectPoints", exp);
	}	
	return 1;
}
forward OnLoadSpouse(playerid, charid);
public OnLoadSpouse(playerid, charid) {
	new id_string[MAX_PLAYER_NAME+1];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		cache_get_row(0, 0, id_string);
		new id = strval(id_string);
		SetPVarInt(playerid, "SpouseID", id);
		cache_get_row(0, 1, id_string);
		if(id == 0) { 
			SetPVarString(playerid, "SpouseName", "No-One");
		} else {
			SetPVarString(playerid, "SpouseName", id_string);
		}
	}
}
forward LoadCharacterInformation(playerid, index);
public LoadCharacterInformation(playerid, index) {
	query[0] = 0;//[1024];
	new charid;
	format(query,sizeof(query),"CharID%d",index);
	charid = GetPVarInt(playerid, query);
	format(query,sizeof(query),"SELECT `skin`,`x`,`y`,`z`,`angle`,`virtualworld`,`interior`,`username`,`money`,`bank`,`vehlocks`,`cigars`,`userflags`,%s%s%d",
	" `health`,`armour`,`sex`,`phonenumber`,`age`,`job`,Unix_Timestamp(`jobtime`),`level`,`respectpoints`,`phone`,`matsa`,`matsb`,`matsc`,`pot`,`coke`,`meth`,`gunskill`,`matapacks`,`matbpacks`,`matcpacks`,`potseeds`,`cokeplants`,`methmaterials`,`FMember`,`Rank`,`Kills`,`Deaths`,`Member`,`jailtime`,`trainingcredits`,`fightstyle`,`irc`,`ircrank`,`accent`,`maxcars`,`maxhouses`,`maxbusinesses`,`payday`,`paycheque`,`licenseflags`,`jailtype`,`contract`",
	",`lastcar`,`renthouse`,`gascan`,`wtchannel`,`furnituretokens`,Unix_Timestamp(`weaponbuytime`),`maxcraftguns`,`specialitem`,`phonestatus`,`hunger`,`matches`,`maxstorageareas` FROM `characters` WHERE `id` = ",charid);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadCharacter", "dd", playerid,charid);	
	
	format(query, sizeof(query), "SELECT `s`.`id`,`s`.`username` FROM `characters` AS `c` LEFT JOIN `characters` AS `s` ON `s`.`id` = `c`.`spouse` WHERE `c`.`id` = %d",charid);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadSpouse", "dd", playerid,charid);	
}
chooseCharacter(playerid, index) {
	stopSetRandomCamera(playerid); //Stop rotating the camera
	LoadCharacterInformation(playerid, index);
	return 1;	
}

tryCreateCharacter(playerid, name[]) {
	query[0] = 0;//[128];
	mysql_real_escape_string(name,name);
	format(query, sizeof(query),"SELECT 1 FROM `characters` WHERE `username` = \"%s\"",name);
	mysql_function_query(g_mysql_handle, query, true, "onCreateChar", "ds", playerid,name);
}
forward onCreateChar(playerid, name[]);
public onCreateChar(playerid, name[]) {
	query[0] = 0;//[128];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		ShowPlayerDialog(playerid, _:EAccountDialog_CreateCharacter, DIALOG_STYLE_INPUT, "Enter your name", "{FFFFFF}Enter your characters name.\nFor example: {0000FF}John_Smith{FFFFFF} or {0000FF}Javon_Watts\n{FF0000}This name is in use! Enter another one!","Ok","Cancel");
	} else {
		format(query, sizeof(query), "INSERT INTO `characters` (`username`,`accountid`) VALUES (\"%s\", %d)",name,GetPVarInt(playerid,"AccountID"));
		mysql_function_query(g_mysql_handle, query, true, "onCharacterCreate", "ds", playerid,name);
	}
}
forward SetRandomCamera(playerid);
public SetRandomCamera(playerid) {
	new LastCamCount = RandomEx(0, 5);
	switch(LastCamCount) {
		case 0: {
			InterpolateCameraPos(playerid, 1549.44, -1674.01, 104.86, 1748.60, -1863.11, 100.99, CAM_TRANSITION_TIME, CAMERA_MOVE);
			InterpolateCameraLookAt(playerid, 1449.44, -1574.01, 104.86, 1748.60, -1863.11, 11.99, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
		case 1: {
			InterpolateCameraPos(playerid, 1748.60, -1863.11, 100.99, 1930.16, -1414.90, 28.46, CAM_TRANSITION_TIME, CAMERA_MOVE);	
			InterpolateCameraLookAt(playerid, 1648.60, -1763.11, 100.99, 1748.60, -1863.11, 11.99, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
		case 2: {
			InterpolateCameraPos(playerid, 1930.16, -1414.90, 28.46, 2456.0452, -1631.4119, 50.2718, CAM_TRANSITION_TIME, CAMERA_MOVE);	
			InterpolateCameraLookAt(playerid, 1830.16, -1314.90, 28.46, 1930.16, -1414.90, 18.46, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
		case 3: {
			InterpolateCameraPos(playerid, 2456.0452, -1631.4119, 50.2718, 1779.6514, -2224.4497, 123.1702, CAM_TRANSITION_TIME, CAMERA_MOVE);
			InterpolateCameraLookAt(playerid, 2356.0452, -1531.4119, 50.2718, 2456.0452, -1631.4119, 50.2718, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
		case 4: {
			InterpolateCameraPos(playerid, 1779.6514, -2224.4497, 123.1702, 1591.3127, -2137.5685, 109.1709, CAM_TRANSITION_TIME, CAMERA_MOVE);
			InterpolateCameraLookAt(playerid, 1679.6514, -2124.4497, 123.1702, 1291.3127, -2037.5685, 113.1709, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
		case 5: {
			InterpolateCameraPos(playerid, 1291.3127, -2037.5685, 109.1709, 1549.44, -1674.01, 30.86, CAM_TRANSITION_TIME, CAMERA_MOVE);
			InterpolateCameraLookAt(playerid, 1191.3127, -1674.5685, 30.86, 1291.3127, -2037.5685, 113.1709, CAM_TRANSITION_TIME, CAMERA_MOVE);
		}
	}
	return 1;
}
stopSetRandomCamera(playerid) {
	TogglePlayerSpectating(playerid, 0);
}
getNameTagColour(playerid) {
	new aduty = GetPVarInt(playerid, "AdminDuty");
	if(!IsPlayerConnectEx(playerid)) {
		return X11_BLACK;
	}
	if(aduty == 1) {
		return COLOR_RED;
	} else if(aduty == 2) {
		return COLOR_BRIGHTRED;
	}
	if(GetPVarInt(playerid, "CopDuty") > 0) {
		return TEAM_BLUE_COLOR;
	}
	return X11_WHITE;
}

saveCharacterData(playerid) {
	query[0] = 0;//[2048];
	new accent[(32*2)+1];
	new Float:X,Float:Y,Float:Z,Float:A;
	new interior, vw;
	if(isInPaintball(playerid)) {
		X = GetPVarFloat(playerid, "XPaintBall");
		Y = GetPVarFloat(playerid, "YPaintBall");
		Z = GetPVarFloat(playerid, "ZPaintBall");
		interior = GetPVarInt(playerid, "IntPaintBall");
		vw = GetPVarInt(playerid, "VWPaintBall");
		GetPlayerFacingAngle(playerid, A);
	} else {
		GetPlayerPos(playerid, X, Y, Z);
		GetPlayerFacingAngle(playerid, A);
		interior = GetPlayerInterior(playerid);
		vw = GetPlayerVirtualWorld(playerid);
	}
	new Float:health,Float:armour;
	GetPlayerHealth(playerid, health);
	GetPlayerArmour(playerid, armour);
	if(GetPlayerState(playerid) == PLAYER_STATE_WASTED || isPlayerDying(playerid)) { // /q to avoid fix
		health = 0.0;
		armour = 0.0;
		ResetPlayerWeaponsEx(playerid);
	}
	format(query, sizeof(query),"UPDATE `characters` SET `x` = %f, `y` = %f, `z` = %f, `angle` = %f, `interior` = %d, `virtualworld` = %d, `skin` = %d, `money` = %d, `bank` = %d,`userflags` = %d, `phonenumber` = %d,`phone` = %d,`sex` = %d, `age` = %d,`vehlocks` = %d,`cigars` = %d,`health` = %f,`armour` = %f, `job` = %d, `jobtime` = FROM_UNIXTIME(%d), `weaponbuytime` = FROM_UNIXTIME(%d) WHERE `id` = %d",
	X, Y, Z, A, interior, vw, GetPVarInt(playerid,"SkinID"), GetPVarInt(playerid,"Money"), GetPVarInt(playerid,"Bank"), GetPVarInt(playerid, "UserFlags"), GetPVarInt(playerid, "PhoneNumber"), GetPVarInt(playerid, "Phone"),GetPVarInt(playerid, "Sex"),GetPVarInt(playerid, "Age"),GetPVarInt(playerid,"VehLockpicks"),GetPVarInt(playerid,"Cigars"),health,armour,GetPVarInt(playerid, "Job"),GetPVarInt(playerid, "JobTime"),GetPVarInt(playerid, "WeaponBuyTime"),GetPVarInt(playerid,"CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	new jailtime = GetPVarInt(playerid, "ReleaseTime");
	//jailtime -= gettime();
	//if(jailtime < 1) jailtime = 0;
	GetPVarString(playerid, "Accent", accent, sizeof(accent));
	mysql_real_escape_string(accent, accent);
	format(query, sizeof(query),"UPDATE `characters` SET `level` = %d, `respectpoints` = %d, `matsa` = %d,`matsb` = %d, `matsc` = %d,`pot` = %d, `coke` = %d, `meth` = %d,`gunskill` = %d, `matapacks` = %d, `matbpacks` = %d, `matcpacks` = %d, `potseeds` = %d, `cokeplants` = %d, `methmaterials` = %d, `FMember` = %d, `Rank` = %d, `Kills` = %d, `Deaths` = %d,`Member` = %d,`jailtime` = %d, `trainingcredits` = %d, `fightstyle` = %d, `payday` = %d, `paycheque` = %d, `licenseflags` = %d WHERE `id` = %d",
	GetPVarInt(playerid, "Level"), GetPVarInt(playerid,"RespectPoints"),GetPVarInt(playerid, "MatsA"),
	GetPVarInt(playerid, "MatsB"),GetPVarInt(playerid, "MatsC"),GetPVarInt(playerid, "Pot"),GetPVarInt(playerid, "Coke"),
	GetPVarInt(playerid, "Meth"),GetPVarInt(playerid, "GunSkill"), GetPVarInt(playerid, "MatAPacks"),
	GetPVarInt(playerid, "MatBPacks"),GetPVarInt(playerid, "MatCPacks"),GetPVarInt(playerid, "PotSeeds"),
	GetPVarInt(playerid, "CokePlants"),GetPVarInt(playerid, "MethMaterials"),
	GetPVarInt(playerid, "Family"),GetPVarInt(playerid, "Rank"),
	GetPVarInt(playerid, "Kills"),GetPVarInt(playerid, "Deaths"),GetPVarInt(playerid, "Faction"),
	jailtime, GetPVarInt(playerid, "TrainingCredits"), GetPVarInt(playerid, "FightStyle"),
	GetPVarInt(playerid, "Payday"),GetPVarInt(playerid, "PayCheque"),GetPVarInt(playerid, "LicenseFlags"),
	GetPVarInt(playerid,"CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query),"UPDATE `characters` SET `irc` = %d, `ircrank` = %d,`accent` = \"%s\",`maxcars` = %d,`maxhouses` = %d, `maxbusinesses` = %d,`jailtype` = %d,`contract` = %d,`renthouse` = %d,`gascan` = %d,`wtchannel` = %d,`spouse` = %d,`furnituretokens` = %d, `maxcraftguns` = %d WHERE `id` = %d",
	GetPVarInt(playerid, "IRCChan"), GetPVarInt(playerid,"IRCRank"),accent, GetPVarInt(playerid, "MaxCars"),GetPVarInt(playerid, "MaxHouses"),GetPVarInt(playerid, "MaxBusinesses"),GetPVarInt(playerid, "JailType"),GetPVarInt(playerid, "Contract"),GetPVarInt(playerid, "Rent"),GetPVarInt(playerid, "GasCans"),GetPVarInt(playerid, "WTChannel"),GetPVarInt(playerid, "SpouseID"),GetPVarInt(playerid, "FurnitureTokens"),GetPVarInt(playerid, "MaxCraftGuns"), GetPVarInt(playerid,"CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query), "UPDATE `characters` set `specialitem` = %d,`phonestatus` = %d,`hunger` = %d, `matches` = %d where `id` = %d",GetPVarInt(playerid, "SpecialItem"), GetPVarInt(playerid, "PhoneStatus"),GetHungerLevel(playerid),GetPVarInt(playerid, "Matches"),GetPVarInt(playerid,"CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	saveDiseases(playerid);
	return 1;
}

public IsPlayerConnectEx(playerid) {
	if(IsPlayerConnected(playerid)) {
		return GetPVarInt(playerid, "LoggedIn");
	}
	return 0;
}
IsNameTaken(name[], callback[], playerid) {
	query[0] = 0;//[128];
	new name_esc[(MAX_PLAYER_NAME*2)+1];
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	mysql_real_escape_string(name, name_esc);
	format(query, sizeof(query),"SELECT `id` FROM `characters` WHERE `username` = \"%s\"", name_esc);
	mysql_function_query(g_mysql_handle, query, true, callback, "ds", playerid, name);
	return 1;
}
IsAccountTaken(name[], callback[], playerid) {
	query[0] = 0;//[128];
	new name_esc[(MAX_PLAYER_NAME*2)+1];
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	mysql_real_escape_string(name, name_esc);
	format(query, sizeof(query),"SELECT `id` FROM `accounts` WHERE `username` = \"%s\"", name_esc);
	mysql_function_query(g_mysql_handle, query, true, callback, "ds", playerid, name);
	return 1;
}
IsPhoneNumberTaken(number, callback[], playerid) {
	query[0] = 0;//[128];
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	format(query, sizeof(query),"SELECT `id` FROM `characters` WHERE `phonenumber` = %d", number);
	mysql_function_query(g_mysql_handle, query, true, callback, "dd", playerid, number);
	return 1;
}
changeName(playerid, name[]) {
	query[0] = 0;//[128];
	new name_esc[(MAX_PLAYER_NAME*2)+1];
	mysql_real_escape_string(name, name_esc);
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	format(query, sizeof(query),"UPDATE `characters` SET `username` = \"%s\" WHERE `id` = %d", name_esc,GetPVarInt(playerid, "CharID"));
	//at this point its assumed the name change WILL be successful
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	SetPlayerName(playerid, name);
	SetPVarString(playerid, "CharUsername", name);
	return 1;
}
changeAccountName(playerid, name[]) { 
	query[0] = 0;//[128];
	new name_esc[(MAX_PLAYER_NAME*2)+1];
	mysql_real_escape_string(name, name_esc);
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	setForumAccName(playerid, GetPlayerNameEx(playerid, ENameType_AccountName), name);
	format(query, sizeof(query),"UPDATE `accounts` SET `username` = \"%s\" WHERE `id` = %d", name_esc,GetPVarInt(playerid, "AccountID"));
	SetPVarString(playerid, "AccountName", name);
	//at this point its assumed the name change WILL be successful
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	return 1;
}
setPhoneNumber(playerid, number) {
	query[0] = 0;//[128];
	if(!IsPlayerConnectEx(playerid)) {
		return 1;
	}
	format(query, sizeof(query),"UPDATE `characters` SET `phonenumber` = %d WHERE `id` = %d",number, GetPVarInt(playerid, "CharID"));
	SetPVarInt(playerid, "PhoneNumber", number);
	//at this point its assumed the name change WILL be successful
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	return 1;
}

YCMD:fixscreen(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Resets someones screen and virtual world/interior");
		return 1;
	}
	new msg[32];
	SetCameraBehindPlayer(playerid);
	if(!sscanf(params,"s[32]",msg)) {
		if(!strcmp(msg, "yes", true)) {
			if(isInJail(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this command right now!");
				return 1;
			}
			SetPlayerInterior(playerid, 0);
			SetPlayerVirtualWorld(playerid, 0);
			return 1;
		}
	}
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "(( If you can't see any, or most textures, or people try /fixscreen yes ))");
	return 1;
}
YCMD:changepass(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Change your password");
		return 1;
	}
	ShowPlayerDialog(playerid, EAccountDialog_ChangePass, DIALOG_STYLE_PASSWORD, "Change Password", "Notice: In order to confirm your indentity, please enter your current password","Ok","Cancel");
	return 1;
}
forward OnChangePassCheck(playerid);
public OnChangePassCheck(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		ShowPlayerDialog(playerid, EAccountDialog_ChangePass_S1, DIALOG_STYLE_PASSWORD, "Confirm Password", "Enter your new password","Ok","Cancel");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "Incorrect Password!");
	}
}
YCMD:settings(playerid, params[], help) {
	showSettingsDialog(playerid);
	return 1;
}
showSettingsDialog(playerid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
	new statustext[32]; 
	strcat(dialogstr, "Setting\tSet\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(SettingsInfo);i++) {
		if(aflags&SettingsInfo[i][SettingsFlag]) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}NO";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s\t%s\n",SettingsInfo[i][SettingsName],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccountDialog_Settings, DIALOG_STYLE_TABLIST_HEADERS, "Modify Settings",dialogstr,"Toggle", "Done");
}
timer SaveTimer[300000]() {
	new msg[256];
	for(new playerid=0;playerid<MAX_PLAYERS;playerid++) {
		if(IsPlayerConnectEx(playerid)) {
			saveCharacterData(playerid);
			new jailtime = GetPVarInt(playerid, "AJailReleaseTime");
			jailtime -= gettime();
			if(jailtime < 1) jailtime = 0;
			new nmute = GetPVarInt(playerid, "NMuteTime");
			if(nmute != 0) {
				nmute -= gettime();
				if(nmute <= 0) nmute = 0;
			}
			new mute = GetPVarInt(playerid, "MuteTime");
			if(mute != 0) {
				mute -= gettime();
				if(mute <= 0) mute = 0;
			}
			format(msg, sizeof(msg), "UPDATE `accounts` SET `ConnectTime` = %d, `DonatePoints` = %d, `DonateRank` = %d,`Cookies` = %d, `ajailtime` = %d, `nmute` = %d, `chatmute` = %d, `reportban` = %d,`accountflags` = %d, `newbrank` = %d  WHERE `id` = %d", 
			GetPVarInt(playerid, "ConnectTime"), GetPVarInt(playerid, "DonatePoints"), GetPVarInt(playerid, "DonateRank"), GetPVarInt(playerid, "Cookies"), jailtime, nmute, mute, GetPVarInt(playerid, "ReportBanned"), GetPVarInt(playerid, "AccountFlags"),GetPVarInt(playerid, "NewbieRank"),GetPVarInt(playerid, "AccountID"));
			mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
		}
	}
}
IsNameRestricted(name[]) {
	new restrictedLastnames[][MAX_PLAYER_NAME] = {{"Thugz"}, {"Dogg"}, {"Tapio"}, {"Gardel"}, {"Rider"}};
	new len = strlen(name);
	for(new x=0;x<sizeof(restrictedLastnames);x++) {
		for(new i=len;i>0;i--) {
			if(name[i] == '_') {
				if(strcmp(name[i+1], restrictedLastnames[x], true) == 0) {
					return 1;
				}
			}
		}
	}
	return 0;
}
findCharBySQLID(id) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "CharID") == id) {
				return i;
			}
		}
	}
	return INVALID_PLAYER_ID;
}
getTotalWealth(playerid) {
	new wealth = 0;
	wealth += getTotalVehiclesValue(playerid);
	wealth += getPlayerPropertiesValue(playerid);
	wealth += GetMoneyEx(playerid);
	wealth += GetPVarInt(playerid, "Bank");
	wealth += GetTotalBusinessValue(playerid);
	wealth += getPlayerLockerWealth(playerid);
	return wealth;
}
playerIsBikeRestricted(playerid) {
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
	if(aflags & EAccountFlags_IsBikeRestricted) {
		return 1;
	}
	return 0;
}
playerIsMaskRestricted(playerid) {
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
	if(aflags & EAccountFlags_IsMaskRestricted) {
		return 1;
	}
	return 0;
}
YCMD:ipflags(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all IP flags");
	}
	mysql_function_query(g_mysql_handle,"SELECT `l`.`id`,`a1`.`username`,`a2`.`username`,`l`.`adminflags`,INET_NTOA(`l`.`host`), INET_NTOA(`l`.`mask`),`l`.`flags` FROM `loginflags` AS `l` LEFT JOIN `accounts` AS `a1` ON `a1`.`id` = `l`.`accountid` LEFT JOIN `accounts` AS `a2` ON `a2`.`id` = `l`.`setby`",true,"OnIPFlagsLookup","d",playerid);
	return 1;
}
forward OnIPFlagsLookup(playerid);
public OnIPFlagsLookup(playerid) {
	new rows, fields;
	new id_string[128];
	cache_get_data(rows, fields);
	new id, EAccountLoginFlags:flags,adminflags;
	
	new setfor[MAX_PLAYER_NAME+1],setby[MAX_PLAYER_NAME+1];
	
	new host[17],mask[17];

	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "*** IP Account Flags");
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,id_string);
		id = strval(id_string);
		cache_get_row(i,1,setfor);
		cache_get_row(i,2,setby);
		cache_get_row(i,3,id_string);
		adminflags = strval(id_string);

		cache_get_row(i,4,host);
		cache_get_row(i,5,mask);
		cache_get_row(i,6,id_string);
		flags = EAccountLoginFlags:strval(id_string);
		
		format(msg, sizeof(msg), "* ID: %d Set For: %s Set By: %s Admin Flags: %d Host: %s Mask: %s ",id,setfor,setby,adminflags,host,mask);
		if(flags & EAccountLoginFlags_Whitelist)
			strcat(msg,"WL ",sizeof(msg));
			
		if(flags & EAccountLoginFlags_Blacklist)
			strcat(msg,"BL ",sizeof(msg));
			
		if(flags & EAccountLoginFlags_DenyAdmin)
			strcat(msg,"DA ",sizeof(msg));
			
		if(flags & EAccountLoginFlags_Undercover)
			strcat(msg,"UC ",sizeof(msg));
			
		if(flags & EAccountLoginFlags_AnyPassword)
			strcat(msg,"AP ",sizeof(msg));
			
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
}
YCMD:delipflag(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes an IP flag");
		return 1;
	}
	new id;
	if(!sscanf(params, "d", id)) {
		format(query, sizeof(query), "DELETE FROM `loginflags` WHERE `id` = %d",id);
		mysql_function_query(g_mysql_handle,query,true,"OnDelIPFlag","d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /delipflag [sqlid]");
	}
	return 1;
}
forward OnDelIPFlag(playerid);
public OnDelIPFlag(playerid) {
	new numrows = mysql_affected_rows();
	if(numrows > 0) {
		SendClientMessage(playerid, X11_YELLOW, "* IP Flag deleted!");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* IP Flag not found!");
	}
	return 1;
}
YCMD:addipflag(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Adds an IP flag");
		return 1;
	}
	new host[(17*2)+1],mask[(17*2)+1],whitelist,blacklist,denyadmin,undercover,anypw;
	new target;
	new aflags;
	new EAccountLoginFlags:flags;
	if(!sscanf(params, "s[16]s[16]dlllllK<playerLookup_acc>(INVALID_PLAYER_ID)",host,mask,aflags,whitelist,blacklist,denyadmin,undercover,anypw,target)) {
		new accid = 0;
		if(target != INVALID_PLAYER_ID) {
			accid = GetPVarInt(target, "AccountID");
		}
		if(whitelist)
			flags |= EAccountLoginFlags_Whitelist;
		if(blacklist) 
			flags |= EAccountLoginFlags_Blacklist;
		if(denyadmin)
			flags |= EAccountLoginFlags_DenyAdmin;
		if(undercover)
			flags |= EAccountLoginFlags_Undercover;
		if(anypw)
			flags |= EAccountLoginFlags_AnyPassword;
		mysql_real_escape_string(host, host);
		mysql_real_escape_string(mask, mask);
		format(query, sizeof(query), "INSERT INTO `loginflags` SET `accountid` = %d, `setby` = %d,`seton` = CURRENT_TIMESTAMP,`flags` = %d,`adminflags` = %d,`host` = INET_ATON(\"%s\"),`mask` = INET_ATON(\"%s\")",accid,GetPVarInt(playerid, "AccountID"),_:flags,aflags,host,mask);
		mysql_function_query(g_mysql_handle,query,true,"OnAddIPFlag","d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /addipflag [host] [mask] [adminflags] [whitelist] [blacklist] [denyadmin] [undercover] [anypw] (accountname)");
	}
	return 1;
}
forward OnAddIPFlag(playerid);
public OnAddIPFlag(playerid) {
	new id = mysql_insert_id();
	format(query, sizeof(query), "* IP Flag ID: %d",id);
	SendClientMessage(playerid, X11_YELLOW, query);
}
LookupPlayerIp(playerid) {
	new ip_cookie = RandomEx(1000, 999999999);
	format(query, sizeof(query), "http://www.iw-rp.com/info_check.php?id=%d&cookie=%d",playerid,ip_cookie);
	SetPVarInt(playerid, "IPCookie", ip_cookie);
	PlayAudioStreamForPlayer(playerid,query);
	ShowScriptMessage(playerid, "* Please wait while we validate your information.", 8000);
	SetTimerEx("CheckIP", 2000, false, "d", playerid);
}
forward CheckIP(playerid);
public CheckIP(playerid) {
	format(query, sizeof(query), "SELECT INET_NTOA(`ip`) FROM `ips` WHERE `ingame_id` = %d AND `cookie` = %d" ,playerid,GetPVarInt(playerid, "IPCookie"));
	mysql_function_query(g_mysql_handle, query, true, "OnTryCheckIP", "d", playerid);
	return 1;
}
forward OnTryCheckIP(playerid);
public OnTryCheckIP(playerid) {
	new rows, columns;
	cache_get_data(rows, columns);
	new ip_str[32];
	if(rows > 0) {
		cache_get_row(0, 0, ip_str);
		SetPVarString(playerid, "IPAddress", ip_str);
		OnPlayerConnectEx(playerid);
		format(query, sizeof(query), "DELETE FROM `ips` WHERE `ingame_id` = %d",playerid);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		DeletePVar(playerid, "IPCookie");
		return 1;
	}
	//Retry if the above fails
	SetTimerEx("RetryIPLookup", 5000, false, "d", playerid);
	return 1;
}
forward RetryIPLookup(playerid);
public RetryIPLookup(playerid) {
	if(GetPVarType(playerid, "IPAddress") == PLAYER_VARTYPE_NONE) {
		ShowScriptMessage(playerid, "* Failed to receive information... Retrying..", 5000);
		SendClientMessage(playerid, X11_WHITE, "* Please note: Audio streams must be enabled.");
		SendClientMessage(playerid, X11_WHITE, "* If you cannot login, go into \"Options\" -> \"Audio Settings\", and then turn your radio on to at least one bar or more. ");
		SendClientMessage(playerid, X11_WHITE, "* Please check if /audiomsg is on, audio messages must be enabled! If everything fails delete your gta_sa.set file ... ");
		SendClientMessage(playerid, X11_WHITE, "* which is located at Documents/GTA San Andreas User Files");
		format(query, sizeof(query), "DELETE FROM `ips` WHERE `ingame_id` = %d",playerid); //clear any entries which may exist
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		LookupPlayerIp(playerid); //Keep trying
	}
	return 1;
}