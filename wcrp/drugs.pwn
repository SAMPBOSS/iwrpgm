/*
	- Temporary PVars Used By This Script
	- DrugName[24] - The name of the drug
	- DrugType(int) - The drug type
	- DrugColorIndex(int) - The color index
	- DrugOwner(int) - The one that requested..
	- DrugFlags(int) - The drug flags..
	- OwnerType(int) - Is this drug being created in a factory or in a house? (1 = House, 2 = Business)
	- PlayerChecking(int) - The player you're checking
	- EditDIndex(int) - The drug index the player is editing (For admins, 1 edits, 2 deletes)
*/
/* Defines */
#define MAX_DRUGS				500 //500 is the maximum drug limit for now
#define MAX_DRUG_TYPES 			6	//How many drug types do we have? Increase if we ever add more
#define MAX_DRUG_SLOTS 			3   //Used for reading off the database
#define CHEMICALS_BASE_AMOUNT	5   //The minimum amount of chemicals required to make a certain drug
#define DRUGDOSE_BASE_AMOUNT	10   //The minimum amount of drug dose required to check a player's drug level
#define MINIMUM_OVERDOSE		90 	 //The minimum amount for an overdose	
#define DIALOG_LIST_OPTS		4   //4 for now
#define DEFAULT_DRUG_AMOUNT		25 //The default amount they get when they create the drug
#define DRUG_ACTIVETIME			1800 //The amount of time any drug will remain active for in the bloodstream (seconds)
#define DRUG_MAX_PRODUCTION		25 //The maximum amount a player can produce without a lab
/* Forwards */
forward OnLoadDrugs();
forward chooseDrugColor(playerid);
forward onChooseDrugColor(playerid, index);
forward OnAddDrugToDatabase(owner, ownertype, colorindex, DrugFlags:flags, type, drugname[]);
forward showDrugPropsDialog(playerid);
forward viewDrugProductionWrapper(playerid);
forward showDrugCreateDialog(playerid);
forward OnLoadPlayerDrugs(playerid);
forward savePlayerDrugs(playerid);
forward onCheckDrugNameExists(playerid, name[]);

/* Enums & Arrays */
enum DrugPVarStates {
	STATE_HAS_NOTHING,
	STATE_HAS_DRUGS,
	STATE_ANYTHING,
};
enum {
	EDrugs_Mode_Add,
	EDrugs_Mode_Take,
	EDrugs_Mode_Produce,
	EDrugs_Mode_EditPPG,
	//Things that are not in the edit dialog (viewDrugProduction) must always be last under buy
	EDrugs_Mode_Buy,
}
enum {
	EDrugs_Type_Decrease,
	EDrugs_Type_Increase,
}
enum {
	DATA_DRUGTYPE, //This is not the drug type but the SQLID of the drug itself
	DATA_DRUGAMOUNT,
}
enum {
	EDrugsDialog_CreateDrug = EDrugs_Base + 1,
	EDrugsDialog_EnterName,
	EDrugsDialog_EnterType,
	EDrugsDialog_SetColor,
	EDrugsDialog_SetEffects,
	EDrugsDialog_AcceptDecline,
	EDrugsDialog_ShowMenu,
	EDrugsDialog_ViewDrugOpts,
	EDrugsDialog_Mode_Take,
	EDrugsDialog_Mode_Add,
	EDrugsDialog_Mode_Produce,
	EDrugsDialog_PickOption,
	EDrugsDialog_ShowGiveDrug,
	EDrugsDialog_PharmacyBuyMenu,
	EDrugsDialog_Mode_Buy,
	EDrugsDialog_Mode_EditPPG,
	EDrugsDialog_OnEditPPG,
	EDrugsDialog_ShowDropDrug,
	EDrugsDialog_Delete,
	EDrugsDialog_DoNothing
}

new DrugColours[] =	{0xFFFFFFFF,0xE3C519FF,0xFF0000FF,0xFF80FFFF,0xFF6317FF,0xC1C100FF,0x800080FF,
						 0xFFB0FFFF,0x804040FF,0xFFD7EBFF,0xBBBB00FF,0x00D200FF,0x660033FF,0xFFFF80FF,0xFFA346FF,
						 0xFFD7FFFF,0xFFFF91FF,0xC6C6FFFF,0xD76B00FF,0xC10061FF,0xF4F400FF,0xFF9664FF,
						 0x00FF80FF,0xBF0000FF,0x008000FF,0x00ECECFF,0xC0C0C0FF,0xAA3333FF,0xC0C0C0FF,
						 0x003C00FF,0xE1E100FF,0x00AAFFFF,0x0000FFFF};

enum DrugFlags (<<= 1) {
	EDrugEffect_None = 0,
	EDrugEffect_Health = 1, //Health..
	EDrugEffect_Armour, //Gives armour
	EDrugEffect_Resistance, //Better resistance against bullets
	EDrugEffect_Narcolepsy, //Gives you narcolepsy effects
	EDrugEffect_Hallucinations, //Produces hallucinations
	EDrugEffect_Hearing, //Improves hearing
	EDrugEffect_HearingLoss, //Produces hearing impairment
	EDrugEffect_DestroyBody, //Systematically destroys the body (Lowers health)
	EDrugEffect_Hungry, //Makes you become hungry
	EDrugEffect_LessHunger, //Makes you eat less
	EDrugEffect_Drowsiness, //Makes you become drowsy
	EDrugEffect_Immobilize, //Makes you unable to move for a while
	EDrugEffect_Euphoria, //Gives you an Euphoria effect
	EDrugEffect_Vomit, //Makes you vomit randomly
	EDrugEffect_HairLoss, //Makes you lose hair
	EDrugEffect_Amaurosis, //Amaurosis (Blindness)
	EDrugEffect_Smokeable, //Can be smoked
	EDrugEffect_EyesRed, //Turns eyes red
	EDrugEffect_TreatDiseases, //Treats and prevents diseases
	EDrugEffect_NoDrowsiness, //Stops Drowsiness
	EDrugEffect_FasterHealing, //Faster healing at hospital
	EDrugEffect_Drinkable, //Can be drunk
};

enum EDrugFlagsInfo {
	DrugFlags:EDrugFlag, //drug flag
	EDrugFlagDesc[64], //description of the drug flag
	EDrugFlagDescAlt[32], //Alternate description of the drug flag (1-2  words only)
};
new DrugFlagDesc[][EDrugFlagsInfo] = 
{
	{EDrugEffect_Health, "Gives you health", "Health Boost"}, //Health..
	{EDrugEffect_Armour, "Gives you armour", "Armour Boost"}, //Gives armour
	{EDrugEffect_Resistance, "Better resistance against bullets", "Resistance"}, //Better resistance against bullets
	{EDrugEffect_Narcolepsy, "Gives you narcolepsy effects", "Narcolepsy"}, //Gives you narcolepsy effects
	{EDrugEffect_Hallucinations, "Produces hallucinations", "Hallucinations"}, //Produces hallucinations
	{EDrugEffect_Hearing, "Improves hearing", "Improved Hearing"}, //Improves hearing
	{EDrugEffect_HearingLoss, "Produces hearing impairment", "Hearing Impairment"}, //Produces hearing impairment
	{EDrugEffect_DestroyBody, "Destroys the body (Lowers health)", "Lowers Health"}, //Systematically destroys the body (Lowers health)
	{EDrugEffect_Hungry, "Makes you become hungry", "Hunger"}, //Makes you become hungry
	{EDrugEffect_LessHunger, "Makes you eat less", "Less Apetite"}, //Makes you eat less
	{EDrugEffect_Drowsiness, "Makes you become drowsy", "Drowsiness"}, //Makes you become drowsy
	{EDrugEffect_Immobilize, "Makes you unable to move for a while", "Immobilizes"}, //Makes you unable to move for a while
	{EDrugEffect_Euphoria, "Gives you an Euphoria effect", "Euphoria"}, //Gives you an Euphoria effect
	{EDrugEffect_Vomit, "Makes you vomit randomly", "Vomiting"}, //Makes you vomit randomly
	{EDrugEffect_HairLoss, "Makes you lose hair", "Hair Loss"}, //Makes you lose hair
	{EDrugEffect_Amaurosis, "Amaurosis (Blindness)", "Amaurosis (Blindness)"}, //Amaurosis (Blindness)
	{EDrugEffect_Smokeable, "Can be smoked", "Smokeable"}, //Can be smoked
	{EDrugEffect_EyesRed, "Turns eyes red", "Eyes Red"}, //Turns eyes red
	{EDrugEffect_TreatDiseases, "Treats and prevents diseases", "Treats Diseases"}, //Treats and prevents cold
	{EDrugEffect_NoDrowsiness, "Stops Drowsiness", "Stops Drowsiness"}, //Stops the drowsiness
	{EDrugEffect_FasterHealing, "Faster healing at hospital", "Faster Healing"}, //Faster healing at hospital
	{EDrugEffect_Drinkable, "Can be drunk", "Drinkable"} //Can be drunk
};

enum EDrugInfo {
	EDrugSQLID,
	EDrugOwner, //Stores the SQLID of the drug owner
	EDrugOwnerType,
	DrugFlags:EDFlag,
	EDrugName[64],
	EDrugAmount,
	EDrugModel,
	EDrugColorIndex,
	EnumDrugType:EDrugType,
	EDrugPPG, //Price Per Gram (Pharmacies only (Businesses))
};
new Drugs[MAX_DRUGS][EDrugInfo];

enum EnumDrugType {
	DrugType_Pill,
	DrugType_Injectable,
	DrugType_Solid,
	DrugType_Paper,
	DrugType_Plant,
	DrugType_Liquid,
}
new DrugTypes[MAX_DRUG_TYPES][] = {
	{"Pill"},
	{"Injectable"},
	{"Solid"},
	{"Paper"},
	{"Plant"}, //Plants can produce seeds
	{"Liquid"}
};

enum E_DrugDialog {
	E_DialogOptionText[128],
	E_DialogDrCallBack[128],
}
/* New's */
new DrugDialog[][E_DrugDialog] = {
	{"View Produced Drugs", "viewDrugProductionWrapper"},
	{"Add A Drug", "showDrugCreateDialog"}
};

enum E_DrugPVars {
	E_DrugType[32],
	E_DrugAmount[32],
	E_DrugType_SQLSave[32],
	E_DrugAmount_SQLSave[32],
}
/* New's */
new DrugPVars[1][E_DrugPVars] = { //The maximum for this array structure is 1
	{"drug_type_%d", "drug_amount_%d", "`drug_type_%d` = %d", "`drug_amount_%d` = %d"}
};


enum E_DrugDialogList {
	E_DrugDList_Type[32],
	E_DrugDList_Option,
}
new DrugDialogList[DIALOG_LIST_OPTS][E_DrugDialogList] = {
	{"Add (From Inventory)", EDrugs_Mode_Add},
	{"Take (From business / house)", EDrugs_Mode_Take},
	{"Produce", EDrugs_Mode_Produce},
	{"Change Price Per %s", EDrugs_Mode_EditPPG}
};
/* General Functions */
drugsOnGameModeInit() {
	loadDrugs();
}
loadDrugs() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`, `drug_name`, `drug_flags`, `amount`, `owner`, `owner_type`, `model`, `drug_color`, `drug_type`, `drug_ppg` FROM `drugs`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadDrugs", "");
}

public OnLoadDrugs() {
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	for(new index=0;index<rows;index++) {
		//if(Drugs[i][EDrugSQLID] != 0) continue; //This won't be used for reloading drugs for now
		new i = findFreeDrugSpot();
		if(i == -1) {
			printf("Drugs Array IS FULL!\n");
			continue;
		}
		cache_get_row(index, 0, id_string);
		Drugs[i][EDrugSQLID] = strval(id_string);
		
		cache_get_row(index, 1, Drugs[i][EDrugName]);
		
		cache_get_row(index, 2, id_string);
		Drugs[i][EDFlag] = DrugFlags:strval(id_string);
		
		cache_get_row(index, 3, id_string);
		Drugs[i][EDrugAmount] = strval(id_string);
		
		cache_get_row(index, 4, id_string);
		Drugs[i][EDrugOwner] = strval(id_string);
		
		cache_get_row(index, 5, id_string);
		Drugs[i][EDrugOwnerType] = strval(id_string);
		
		cache_get_row(index, 6, id_string);
		Drugs[i][EDrugModel] = strval(id_string);
		
		cache_get_row(index, 7, id_string);
		Drugs[i][EDrugColorIndex] = strval(id_string);
		
		cache_get_row(index, 8, id_string);
		Drugs[i][EDrugType] = EnumDrugType:strval(id_string);
		
		cache_get_row(index, 9, id_string);
		Drugs[i][EDrugPPG] = strval(id_string);

		#if debug
		printf("Loaded Drug: SQLID: %d, ModelID: %d, Color(INT) %d",Drugs[i][EDrugSQLID], Drugs[i][EDrugModel], Drugs[i][EDrugColorIndex]);
		#endif
	}
	return 1;
}
reloadDrug(index) {
	query[0] = 0;
	Drugs[index][EDrugSQLID] = 0;
	format(query, sizeof(query), "SELECT `id`, `drug_name`, `drug_flags`, `amount`, `owner`, `owner_type`, `model`, `drug_color`, `drug_type`, `drug_ppg` FROM `drugs` where id = %d", Drugs[index][EDrugSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadDrugs", "");
}
saveDrug(id) {
	query[0] = 0;
	format(query, sizeof(query), "UPDATE `drugs` set `drug_flags` = %d, `amount` = %d, `drug_ppg` = %d where id = %d", _:Drugs[id][EDFlag], Drugs[id][EDrugAmount], Drugs[id][EDrugPPG], Drugs[id][EDrugSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
DrugColour(index) {
	return RGBA_To_ARBG(DrugColours[index]);
}
checkDrugNameExists(playerid, drugname[]) {
	query[0] = 0;//[128];
	mysql_real_escape_string(drugname,drugname);
	format(query, sizeof(query),"SELECT 1 FROM `drugs` WHERE `drug_name` = \"%s\"",drugname);
	mysql_function_query(g_mysql_handle, query, true, "onCheckDrugNameExists", "ds", playerid,drugname);
}
public onCheckDrugNameExists(playerid, name[]) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "* The drug name you're trying to use is already being used, please enter a different name.");
		showDrugCreateDialog(playerid);
	} else {
		SetPVarString(playerid, "DrugName", name);
		dialogstr[0] = 0;
		tempstr[0] = 0;
		for(new i=0;i<sizeof(DrugTypes);i++) {
			format(tempstr, sizeof(tempstr), "%s\n",DrugTypes[i]);
			strcat(dialogstr, tempstr, sizeof(dialogstr));
		}
		ShowPlayerDialog(playerid, EDrugsDialog_EnterType, DIALOG_STYLE_LIST, "What type of drug is this?", dialogstr, "Next","Cancel");
	}
}
DrugsOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	new msg[128];
	switch(dialogid) {
		case EDrugsDialog_EnterName: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug creation menu.");
				showDrugPropsDialog(playerid); //Show them the main menu
				return 1;
			}
			checkDrugNameExists(playerid, inputtext);
		}
		case EDrugsDialog_EnterType: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug creation menu.");
				deleteDrugTempPVars(playerid);
				return 1;
			}
			SetPVarInt(playerid, "DrugType", listitem);
			showSideEffectsDialog(playerid);
		}
		case EDrugsDialog_SetEffects: {
			new index = -1;
			if(!response) {
				if(GetPVarType(playerid, "EditDIndex") != PLAYER_VARTYPE_NONE) {
					SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug editing menu.");
					DeletePVar(playerid, "EditDIndex");
				} else {
					SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug creation menu.");
					chooseDrugColor(playerid);
				}
				return 1;
			}
			if(GetPVarType(playerid, "EditDIndex") != PLAYER_VARTYPE_NONE) {
				index = GetPVarInt(playerid, "EditDIndex");
			}
			new DrugFlags:aflags = index != -1  ? Drugs[index][EDFlag] : DrugFlags:GetPVarInt(playerid, "DrugFlags");
			if(aflags & DrugFlagDesc[listitem][EDrugFlag]) {
				aflags &= ~DrugFlagDesc[listitem][EDrugFlag];
			} else {
				aflags |= DrugFlagDesc[listitem][EDrugFlag];
			}
			if(index != -1) {
				Drugs[index][EDFlag] = aflags;
				saveDrug(index);
			} else {
				SetPVarInt(playerid, "DrugFlags", _:aflags);
			}
			showSideEffectsDialog(playerid, index != -1 ? index : -1); //Keep updating the flags, etc
			return 1;
		}
		case EDrugsDialog_SetColor: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug creation menu.");
				deleteDrugTempPVars(playerid);
				return 1;
			}
			onChooseDrugColor(playerid, listitem);
		}
		case EDrugsDialog_AcceptDecline: {
			new player;
			player = GetPVarInt(playerid, "PlayerChecking");
			if(!response) {
				DeletePVar(playerid, "PlayerChecking");
				deleteDrugTempPVars(player);
				SendClientMessage(player, X11_LIGHTBLUE, "* An admin has declined your drug creation.");
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You declined the drug creation.");
				return 1;
			}
			if(GetPVarType(player, "DrugName") == PLAYER_VARTYPE_NONE || GetPVarType(player, "Owner") == PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_TOMATO_2, "This player isn't requesting to create a drug!");
				return 1;
			}
			//Drug info
			setupDrugToDBParams(playerid, player);
			DeletePVar(playerid, "PlayerChecking");
		}
		case EDrugsDialog_ShowMenu: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				return 1;
			}
			CallLocalFunction(DrugDialog[listitem][E_DialogDrCallBack],"d", playerid);
		}
		case EDrugsDialog_ViewDrugOpts: {
			if(!response) {
				DeletePVar(playerid, "EditDIndex");
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				return 1;
			}
			tempstr[0] = 0;
			dialogstr[0] = 0;
			new bizid = getBusinessInside(playerid);
			SetPVarInt(playerid, "CurrentTempIndex", getDrugIDByDialogIndex(listitem, bizid != -1 ? 2 : 1, bizid != -1 ? Business[bizid][EBusinessID] : GetPVarInt(playerid, "CharID"))); //If we need an owner, that's how we specify it

			if(GetPVarType(playerid, "EditDIndex") != PLAYER_VARTYPE_NONE) { //To edit a specific drug that exists on the server, as an admin (Needs givedrugs perms)
				if(GetPVarInt(playerid, "EditDIndex") == 1) {
					showSideEffectsDialog(playerid, getDrugIDByDialogIndex(listitem));
				} else if(GetPVarInt(playerid, "EditDIndex") == 2) {
					promptDeleteDrug(playerid, getDrugIDByDialogIndex(listitem));
				}
				return 1;
			}
			for(new i=0;i<sizeof(DrugDialogList);i++) {
				if(DrugDialogList[i][E_DrugDList_Option] != EDrugs_Mode_EditPPG) { //This is repetitive, would like to find a way to write it without repeating, ignore for now
					format(tempstr, sizeof(tempstr), "%s\n", DrugDialogList[i][E_DrugDList_Type]);
					strcat(dialogstr,tempstr,sizeof(dialogstr));
				} else {
					if(IsDrugFactory(bizid) && Business[bizid][EBusinessOwner] == GetPVarInt(playerid,"CharID")) {
						format(tempstr, sizeof(tempstr), DrugDialogList[i][E_DrugDList_Type], drugsGetMeasureUnit(GetPVarInt(playerid, "CurrentTempIndex"), 1));
						strcat(tempstr,"\n",sizeof(tempstr));
						strcat(dialogstr,tempstr,sizeof(tempstr));
					}
				}
			}
			ShowPlayerDialog(playerid, EDrugsDialog_PickOption, DIALOG_STYLE_LIST, "What would you like to do with this drug?",dialogstr,"Continue", "Exit");
		}
		case EDrugsDialog_Mode_Add: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				return 1;
			}
			new index, pvarid, amount;
			index = GetPVarInt(playerid, "CurrentTempIndex");
			DeletePVar(playerid, "CurrentTempIndex");
			if(strval(inputtext) <= 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "* Invalid amount!");
				return 1;
			}
			pvarid = getUsableDrugSpot(playerid, Drugs[index][EDrugSQLID], STATE_HAS_DRUGS); //Find a drug spot for this drug we're storing in the inventory..
			if(pvarid != -1) {
				amount = drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT);
				if(strval(inputtext) > amount) {
					SendClientMessage(playerid, X11_TOMATO_2, "* You don't have that much!");
					return 1;
				}
				Drugs[index][EDrugAmount] += strval(inputtext);
				format(msg, sizeof(msg), "%d %s of %s have been added to the drug slot.", strval(inputtext), drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
				SendClientMessage(playerid, X11_WHITE, msg);
				setDrugPVar(playerid, index, pvarid, strval(inputtext), EDrugs_Type_Decrease); //The drug index and the pvarid (0, 1, 2) that we got from the getUsableDrugSpot function
				saveDrug(index);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "* You don't seem to have the drug you're trying to store!");
				return 1;
			}
		}
		case EDrugsDialog_Mode_Take: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				return 1;
			}
			new index, pvarid;
			index = GetPVarInt(playerid, "CurrentTempIndex");
			DeletePVar(playerid, "CurrentTempIndex");
			if(strval(inputtext) <= 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "* Invalid amount!");
				return 1;
			}
			if(strval(inputtext) > Drugs[index][EDrugAmount]) {
				SendClientMessage(playerid, X11_TOMATO_2, "* You don't have that much!");
				return 1;
			}
			pvarid = getUsableDrugSpot(playerid, Drugs[index][EDrugSQLID], STATE_ANYTHING);
			if(pvarid != -1) {
				format(msg, sizeof(msg), "%d %s of %s have been added to your inventory.", strval(inputtext), drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
				SendClientMessage(playerid, X11_WHITE, msg);
				Drugs[index][EDrugAmount] -= strval(inputtext);
				setDrugPVar(playerid, index, pvarid, strval(inputtext), EDrugs_Type_Increase); //The drug index and the pvarid (0, 1, 2) that we got from the getUsableDrugSpot function
				saveDrug(index);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have any slots to store the drugs on, get rid of some of them.");
			}
		}
		case EDrugsDialog_PickOption: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				return 1;
			}
			new index;
			index = GetPVarInt(playerid, "CurrentTempIndex");
			showDrugInput(playerid, index, listitem);
		}
		case EDrugsDialog_Mode_Produce: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				DeletePVar(playerid, "CurrentTempIndex");
				return 1;
			}
			new index, chemicals, amountprod;
			new bizid = getBusinessInside(playerid);
			new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
			amountprod = strval(inputtext);
			if(bizid != -1) {
				if((IsDrugFactory(bizid) && Business[bizid][EBusinessOwner] == GetPVarInt(playerid,"CharID")) || (getFamilyType(family) == _:EFamilyType_StreetGang)) {
					if(amountprod < 0 || amountprod > 5000) {
						SendClientMessage(playerid, X11_TOMATO_2, "Invalid amount, please enter an amount between 0 and 5000.");
						return 1;
					}
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You either don't own a drug factory or you're not in a street gang.");
					return 1;
				}
			} else {
				if(amountprod < 0 || amountprod > DRUG_MAX_PRODUCTION) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invalid amount, please enter an amount between 0 and "#DRUG_MAX_PRODUCTION".");
					return 1;
				}
			}
			dialogstr[0] = 0;
			index = GetPVarInt(playerid, "CurrentTempIndex");
			DeletePVar(playerid, "CurrentTempIndex");

			if((Drugs[index][EDrugAmount] + amountprod) > DRUG_MAX_PRODUCTION) {
				SendClientMessage(playerid, X11_TOMATO_2, "The maximum you can hold for that drug is "#DRUG_MAX_PRODUCTION", buy a drug factory or join a street gang to produce more.");
				return 1;
			}
			new DrugFlags:aflags = DrugFlags:Drugs[index][EDFlag];
			for(new i=0;i<sizeof(DrugFlagDesc);i++) {
				if(aflags & DrugFlagDesc[i][EDrugFlag]) {
					chemicals += CHEMICALS_BASE_AMOUNT;
					chemicals += floatround(amountprod / 2);
				}
			}
			if(GetPVarInt(playerid, "chemicals") < chemicals) {
				format(dialogstr, sizeof(dialogstr), "You don't have enough chemicals to make this drug, you need at least %d.", chemicals);
				SendClientMessage(playerid, X11_TOMATO_2, dialogstr);
				return 1;
			}
			SetPVarInt(playerid, "chemicals", GetPVarInt(playerid, "chemicals") - chemicals);
			format(dialogstr, sizeof(dialogstr), "You've produced %d %s of %s and it has been put in your drug business safe.", amountprod, drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
			SendClientMessage(playerid, X11_WHITE, dialogstr);
			Drugs[index][EDrugAmount] += amountprod;
			saveDrug(index);
		}
		case EDrugsDialog_ShowGiveDrug: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				deleteDrugTempPVars(playerid);
				return 1;
			}
			new sqlid, pvarid, pvaridtarget, target, amount;
			target = GetPVarInt(playerid, "DrugTempTarget");
			amount = GetPVarInt(playerid, "DrugTempAmount");
			deleteDrugTempPVars(playerid);
			sqlid = getDrugSQLIDByListItem(playerid, listitem, pvarid);
			if(sqlid != -1) {
				if(amount > drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT) || amount <= 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
					return 1;
				}
				pvarid = getUsableDrugSpot(playerid, sqlid, STATE_ANYTHING);
				pvaridtarget = getUsableDrugSpot(target, sqlid, STATE_ANYTHING);
				if(pvarid != -1 && pvaridtarget != -1) {
					format(msg, sizeof(msg), "You've given %d %s of %s to %s.", amount, drugsGetMeasureUnit(getDrugIDBySQLID(sqlid)), drugsGetNameFromSQLID(sqlid), GetPlayerNameEx(target, ENameType_RPName));
					SendClientMessage(playerid, X11_WHITE, msg);
					format(msg, sizeof(msg), "%d %s of %s have been added to your inventory.", amount, drugsGetMeasureUnit(getDrugIDBySQLID(sqlid)), drugsGetNameFromSQLID(sqlid));
					SendClientMessage(target, X11_WHITE, msg);
					setDrugPVar(target, getDrugIDBySQLID(sqlid), pvaridtarget, amount, EDrugs_Type_Increase); //The drug index and the pvarid (0, 1, 2) that we got from the getUsableDrugSpot function
					setDrugPVar(playerid, getDrugIDBySQLID(sqlid), pvarid, amount, EDrugs_Type_Decrease);
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "Either you or the other player don't have any slots available for that type of drug.");
					SendClientMessage(target, X11_TOMATO_2, "You don't have any slots available.");
				}
			}
		}
		case EDrugsDialog_ShowDropDrug: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug-drop menu.");
				deleteDrugTempPVars(playerid);
				return 1;
			}
			new sqlid, pvarid, amount, Float: X, Float: Y, Float: Z;
			amount = GetPVarInt(playerid, "DrugTempAmount");
			deleteDrugTempPVars(playerid);
			sqlid = getDrugSQLIDByListItem(playerid, listitem, pvarid);
			if(sqlid != -1) {
				if(amount > drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT) || amount <= 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
					return 1;
				}
				GetPlayerPos(playerid, X, Y, Z);
				dropItem(playerid, EType_Drug, amount, X, Y, Z, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid), sqlid);
				setDrugPVar(playerid, getDrugIDBySQLID(sqlid), pvarid, amount, EDrugs_Type_Decrease); //EDrugs_Type_Decrease
			}
		}
		case EDrugsDialog_PharmacyBuyMenu: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the buy menu.");
				DeletePVar(playerid, "CurrentTempIndex");
				return 1;
			}
			new cindex, x, index;
			new bizid = getBusinessInside(playerid);
			while(((index = getDrugIDByDialogIndex(x++, 2, Business[bizid][EBusinessID])) != -1)) {
				if(Drugs[index][EDrugPPG] != 0) {
					if(cindex++ == listitem) break;
				}
			}
			//new index = getDrugIDByDialogIndex(listitem, 2, Business[bizid][EBusinessID]); //If we need an owner, that's how we specify it
			if(index != -1) {
				SetPVarInt(playerid, "CurrentTempIndex", index); 
				showDrugInput(playerid, index, EDrugs_Mode_Buy);
			} else {
				format(msg, sizeof(msg), "[DBGMSG]: Error, invalid drug id for Drug SQLID %d, contact an admin.", index);
				SendClientMessage(playerid, X11_TOMATO_2, msg);
			}
		}
		case EDrugsDialog_Mode_Buy: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the buy menu.");
				DeletePVar(playerid, "CurrentTempIndex");
				DeletePVar(playerid, "BuyingBiz");
				return 1;
			}
			new index, pvarid, amount, biz;
			index = GetPVarInt(playerid, "CurrentTempIndex");
			biz = GetPVarInt(playerid, "BuyingBiz");
			DeletePVar(playerid, "CurrentTempIndex");
			DeletePVar(playerid, "BuyingBiz");
			pvarid = getUsableDrugSpot(playerid, Drugs[index][EDrugSQLID], STATE_ANYTHING);
			amount = strval(inputtext);
			if(amount > Drugs[index][EDrugAmount] || amount < 1) {
				showDrugInput(playerid, index, EDrugs_Mode_Buy);
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid amount!");
				return 1;
			}
			if(Drugs[index][EDrugAmount] < 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "The pharmacy ran out of stock for such drug!");
				return 1;
			}
			if(pvarid != -1) {
				format(msg, sizeof(msg), "You've bought %d %s of %s for $%d.", amount, drugsGetMeasureUnit(index), drugsGetNameFromSQLID(Drugs[index][EDrugSQLID]), amount * Drugs[index][EDrugPPG]);
				SendClientMessage(playerid, X11_WHITE, msg);
				format(msg, sizeof(msg), "%d %s of %s have been added to your inventory.", amount, drugsGetMeasureUnit(index), drugsGetNameFromSQLID(Drugs[index][EDrugSQLID]));
				SendClientMessage(playerid, X11_WHITE, msg);
				setDrugPVar(playerid, index, pvarid, amount, EDrugs_Type_Increase); //The drug index and the pvarid (0, 1, 2) that we got from the getUsableDrugSpot function
				GiveMoneyEx(playerid, -(amount * Drugs[index][EDrugPPG]));
				addToBusinessTill(biz, amount * Drugs[index][EDrugPPG]);
				Drugs[index][EDrugAmount] -= amount;
				saveDrug(index);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have any slots available.");
			}
		}
		case EDrugsDialog_Mode_EditPPG: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				DeletePVar(playerid, "CurrentTempIndex");
				return 1;
			}
			new bizid = getBusinessInside(playerid);
			new index = GetPVarInt(playerid, "CurrentTempIndex"); //If we need an owner, that's how we specify it
			if(IsDrugFactory(bizid) && Business[bizid][EBusinessOwner] == GetPVarInt(playerid,"CharID")) {
				showDrugInput(playerid, index, EDrugs_Mode_EditPPG);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't own a Pharmacy so this option is not available to you.");
			}
		}
		case EDrugsDialog_OnEditPPG: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the drug menu.");
				DeletePVar(playerid, "CurrentTempIndex");
				return 1;
			}
			new cost = strval(inputtext);
			if(cost > 500 || cost < 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "You may only pick a price between 0 and 500");
				showDrugInput(playerid, GetPVarInt(playerid, "CurrentTempIndex"), EDrugs_Mode_EditPPG);
				return 1;
			}
			new index = GetPVarInt(playerid, "CurrentTempIndex"); //If we need an owner, that's how we specify it
			DeletePVar(playerid, "CurrentTempIndex");
			format(msg, sizeof(msg), "The cost per %s for this drug has been changed to $%d", drugsGetMeasureUnit(index, 1), cost);
			SendClientMessage(playerid, X11_WHITE, msg);
			Drugs[index][EDrugPPG] = cost;
			saveDrug(index);
		}
		case EDrugsDialog_Delete: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* Drug deletion canceled!");
				DeletePVar(playerid, "EditDIndex");
				return 1;
			}
			new index = GetPVarInt(playerid, "EditDIndex");
			DeletePVar(playerid, "EditDIndex");
			format(msg, sizeof(msg), "AdmWarn: %s has deleted a drug named %s SQLID: %d.", GetPlayerNameEx(playerid, ENameType_AccountName), Drugs[index][EDrugName], Drugs[index][EDrugSQLID]);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
			deleteDrug(index);
		}
	}
	return 1;
}
deleteDrug(id) {
	query[0] = 0;
	format(query, sizeof(query), "DELETE FROM `drugs` WHERE `id` = %d",Drugs[id][EDrugSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	
	format(query, sizeof(query), "DELETE FROM `droppeditems` WHERE `type` = %d AND `extra_sqlid` = %d", _:EType_Drug, Drugs[id][EDrugSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			new slot, deleted;
			for(new x=0; x<sizeof(DrugPVars); x++) {
				for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) { //Loop through the pvars
					if(drugsGetPVarInfo(i, slot, DATA_DRUGTYPE) == Drugs[id][EDrugSQLID]) {
						SendClientMessage(i, X11_LIGHTBLUE, "Your drug has been deleted.");
						deleteSpecificPlayerDrugVar(i, slot);
						deleted = 1;
					}
					slot++;
				}
			}
			if(deleted != 0) {
				reloadItemsForPlayer(i);
			}
		}
	}

	DestroyAllPlantsByDrugSQLID(Drugs[id][EDrugSQLID]);

	Drugs[id][EDrugOwner] = 0;
	Drugs[id][EDrugSQLID] = 0;
	Drugs[id][EDrugAmount] = 0;
}
promptDeleteDrug(playerid, index) {
	new msg[128];
	if(index != -1) {
		SetPVarInt(playerid, "EditDIndex", index);
		format(msg, sizeof(msg), "Are you sure you want to delete %s with SQL ID %d? It will affect all players that own it.", Drugs[index][EDrugName], Drugs[index][EDrugSQLID]);
		ShowPlayerDialog(playerid, EDrugsDialog_Delete, DIALOG_STYLE_MSGBOX, "Drug Prompt:", msg, "Yes", "No");
	}
	return 1;
}
hasAnyDrugs(playerid) { //Simply checks if the player has any drugs in any of the slots
	new j;
	for(new i=0; i<sizeof(DrugPVars); i++) {
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) { //Loop through the pvars
			if(getDrugIDBySQLID(drugsGetPVarInfo(playerid, j, DATA_DRUGTYPE)) != -1)
				return 1;
			j++;
		}
	}
	return 0;
}
stock drugsGetPVarInfo(playerid, pvarid, datatype) {
	new j, retval;
	tempstr[0] = 0;
	new pvardrugtype[32];
	for(new i=0; i<sizeof(DrugPVars); i++) {
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) { //Loop through the pvars
			if(j == pvarid) {
				switch(datatype) {
					case DATA_DRUGTYPE: {
						format(pvardrugtype, sizeof(pvardrugtype), DrugPVars[i][E_DrugType], j);
						retval = GetPVarInt(playerid, pvardrugtype);
					}
					case DATA_DRUGAMOUNT: {
						format(pvardrugtype, sizeof(pvardrugtype), DrugPVars[i][E_DrugAmount], j);
						retval = GetPVarInt(playerid, pvardrugtype);
					}
				}
			}
			j++;
		}
	}
	return retval;
}
stock drugsGetMeasureUnit(index, singleunit = -1) {
	new retval[12];
	switch(Drugs[index][EDrugType]) {
		case DrugType_Liquid, DrugType_Injectable: {
			strcpy(retval, "fl oz");
		}
		default: {
			strcpy(retval, singleunit != -1 ? ("gram") : ("gram(s)"));
		}
	}
	return strlen(retval) != 0 ? (retval) : ("gram(s)"); //Return gram(s) if it's wrong anyways
}
getDrugIDBySQLID(sqlid) {
	for(new i=0;i<sizeof(Drugs);i++) {
		if(Drugs[i][EDrugSQLID] == sqlid && sqlid != 0) { //A sqlid can't be 0 anywhere, this func is wrong all over the place since it expects to have a value :/
			return i;
		}
	}
	return -1;
}
getUsableDrugSpot(playerid, drugtype, DrugPVarStates:statex) {
	new slot;
	for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) {
		if(drugsGetPVarInfo(playerid, slot, DATA_DRUGTYPE) == drugtype && statex == STATE_HAS_DRUGS) {
			if(drugsGetPVarInfo(playerid, slot, DATA_DRUGAMOUNT) != 0)
				return slot;
		} else {
			if(getDrugIDBySQLID(drugsGetPVarInfo(playerid, slot, DATA_DRUGTYPE)) == -1 || drugsGetPVarInfo(playerid, slot, DATA_DRUGTYPE) == drugtype)
				return slot;
		}
		slot++;
	}
	return -1; //If all of this gets skipped we for sure know we couldn't find anything
}
setDrugPVar(playerid, index, pvarid, amount, type) { //Type: Type of operation, add up or subtract?
	new qstring[128], damount;
	if(pvarid != -1) {
		for(new i=0; i<sizeof(DrugPVars); i++) {
			damount = drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT);
			format(qstring, sizeof(qstring), "%s", DrugPVars[i][E_DrugAmount]);
			format(qstring, sizeof(qstring), qstring, pvarid);
			SetPVarInt(playerid, qstring, (type == EDrugs_Type_Increase) ? (damount + amount) : (damount - amount)); //Set the amount
			
			format(qstring, sizeof(qstring), "%s", DrugPVars[i][E_DrugType]);
			format(qstring, sizeof(qstring), qstring, pvarid);
			SetPVarInt(playerid, qstring, (drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT) > 0) ? Drugs[index][EDrugSQLID] : 0); //Set the amount based on that drug (The SQL id this is), we've to precalculate the next one, that's why we do the amount - 1
		}
	}
	return 1;
}
showDrugInput(playerid, index, mode) {
	dialogstr[0] = 0;
	if(index != -1) {
		SetPVarInt(playerid, "CurrentTempIndex", index);
		switch(mode) {
			case EDrugs_Mode_Add: {
				format(dialogstr, sizeof(dialogstr), "You currently have %d %s of %s, how much would you like to add?", Drugs[index][EDrugAmount], drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
				ShowPlayerDialog(playerid, EDrugsDialog_Mode_Add, DIALOG_STYLE_INPUT, "Drugs Menu:", dialogstr, "Add", "Cancel");
			}
			case EDrugs_Mode_Take: {
				format(dialogstr, sizeof(dialogstr), "You currently have %d %s of %s, how much would you like to take?", Drugs[index][EDrugAmount], drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
				ShowPlayerDialog(playerid, EDrugsDialog_Mode_Take, DIALOG_STYLE_INPUT, "Drugs Menu:", dialogstr, "Take", "Cancel");
			}
			case EDrugs_Mode_Produce: {
				if(Drugs[index][EDrugType] != DrugType_Plant) {
					new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
					new bizid = getBusinessInside(playerid);
					if(!IsDrugFactory(bizid) || getFamilyType(family) != _:EFamilyType_StreetGang) {
						if(Drugs[index][EDrugAmount] >= DRUG_MAX_PRODUCTION) {
							ShowPlayerDialog(playerid, EDrugsDialog_DoNothing, DIALOG_STYLE_MSGBOX, "Drugs Menu:", "You can't produce any more of that drug, join a street gang or buy a factory to do so.", "Ok", "");
							return 1;
						}
					}
					format(dialogstr, sizeof(dialogstr), "You currently have %d %s of %s, how much would you like to produce?", Drugs[index][EDrugAmount], drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
					ShowPlayerDialog(playerid, EDrugsDialog_Mode_Produce, DIALOG_STYLE_INPUT, "Drugs Menu:", dialogstr, "Produce", "Cancel");
				} else {
					ShowPlayerDialog(playerid, EDrugsDialog_DoNothing, DIALOG_STYLE_MSGBOX, "Drugs Menu:", "This drug cannot be produced since it is a plant, in order to produce this drug, you've to plant it by using \"/plantdrug\".", "Ok", "");
				}
			}
			case EDrugs_Mode_EditPPG: {
				format(dialogstr, sizeof(dialogstr), "The actual price per %s for this drug is $%d to how much should it be changed?", drugsGetMeasureUnit(index, 1), Drugs[index][EDrugPPG]);
				ShowPlayerDialog(playerid, EDrugsDialog_OnEditPPG, DIALOG_STYLE_INPUT, "Drugs Menu:", dialogstr, "Change", "Cancel");
			}
			case EDrugs_Mode_Buy: { //This that are not in the edit dialog must always be before this, prepare for mess otherwise
				format(dialogstr, sizeof(dialogstr), "This drug costs $%d per %s, how much would you like to buy?", Drugs[index][EDrugPPG], drugsGetMeasureUnit(index, 1));
				ShowPlayerDialog(playerid, EDrugsDialog_Mode_Buy, DIALOG_STYLE_INPUT, "Drugs Menu:", dialogstr, "Buy", "Cancel");
			}
		}
	}
	return 1;
}
setupDrugToDBParams(playerid, player) {
	new type, colorindex, ownertype, owner;
	new drugname[24];
	new msg[128];
	GetPVarString(player, "DrugName", drugname, sizeof(drugname));
	type = GetPVarInt(player, "DrugType");
	new DrugFlags:aflags = DrugFlags:GetPVarInt(player, "DrugFlags");
	colorindex = GetPVarInt(player, "DrugColorIndex");
	ownertype = GetPVarInt(player, "OwnerType");
	owner = GetPVarInt(player, "Owner");
	deleteDrugTempPVars(player);

	format(query, sizeof(query), "INSERT INTO `drugs` (`drug_name`,`drug_flags`,`owner`,`owner_type`,`drug_color`,`drug_type`) VALUES (\"%s\",%d,%d,%d,%d,%d)",drugname,_:aflags,owner,ownertype,colorindex,type);
	mysql_function_query(g_mysql_handle, query, true, "OnAddDrugToDatabase", "ddddds", owner, ownertype, colorindex, _:aflags, type, drugname);

	//Messages
	SendClientMessage(playerid, X11_YELLOW, "* Drug Approved!");
	SendClientMessage(player, X11_YELLOW, "Your drug has been approved!");
	format(msg, sizeof(msg), "* %s has approved %s's drug(%s)",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(player, ENameType_RPName_NoMask),drugname);
	ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
}
public OnAddDrugToDatabase(owner, ownertype, colorindex, DrugFlags:flags, type, drugname[]) {
	new index = findFreeDrugSpot();
	if(index == -1) {
		ABroadcast(X11_RED,"Can't create drug right now. Drug array is full.",EAdminFlags_BasicAdmin); //We should destroy drugs here but destroying already existing drugs would create massive issues since players won't be able to sync from the tables
		return 0;
	}
	new sqlid = mysql_insert_id();
	Drugs[index][EDrugSQLID] = sqlid;
	Drugs[index][EDrugOwner] = owner;
	Drugs[index][EDrugOwnerType] = ownertype;
	Drugs[index][EDrugColorIndex] = colorindex;
	Drugs[index][EDFlag] = flags;
	Drugs[index][EDrugType] = EnumDrugType:type;
	Drugs[index][EDrugAmount] = DEFAULT_DRUG_AMOUNT;
	format(Drugs[index][EDrugName], 32, "%s", drugname);
	return 1;
}
deleteDrugTempPVars(playerid) {
	DeletePVar(playerid, "DrugName");
	DeletePVar(playerid, "DrugType");
	DeletePVar(playerid, "DrugFlags");
	DeletePVar(playerid, "DrugColorIndex");
	DeletePVar(playerid, "OwnerType");
	DeletePVar(playerid, "Owner");
	DeletePVar(playerid, "DrugTempTarget");
	DeletePVar(playerid, "DrugTempAmount");
}
findFreeDrugSpot() {
	for(new i=0;i<sizeof(Drugs);i++) {
		if(Drugs[i][EDrugSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
public viewDrugProductionWrapper(playerid) {
	viewDrugProduction(playerid);
	return 1;
}
viewDrugProduction(playerid, aedit = 0) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new j;
	new bizid = getBusinessInside(playerid);
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	for(new i=0;i<sizeof(Drugs);i++) {
		if(Drugs[i][EDrugOwner] == GetPVarInt(playerid, "CharID") && bizid == -1 || (IsDrugFactory(bizid) && Business[bizid][EBusinessOwner] == GetPVarInt(playerid,"CharID") && Drugs[i][EDrugOwnerType] == 2 && Drugs[i][EDrugOwner] == Business[bizid][EBusinessID]) || (aflags & EAdminFlags_GiveDrugs && aedit != 0)) { //Only show the drugs they've at the biz if they're inside it, otherwise it'll be a mess of indexes
			if(Drugs[i][EDrugSQLID] != 0) {
				format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %d %s\n",Drugs[i][EDrugName],Drugs[i][EDrugAmount], drugsGetMeasureUnit(i));
				strcat(dialogstr,tempstr,sizeof(dialogstr));
				j++;
			}
		}
	}
	if(j != 0) {
		if(aedit != 0) {
			switch(aedit) {
				case 1: { //Edit
					ShowPlayerDialog(playerid, EDrugsDialog_ViewDrugOpts, DIALOG_STYLE_LIST, "Select the drug you want to edit:",dialogstr,"Edit", "Exit");
				}
				case 2: { //Delete
					ShowPlayerDialog(playerid, EDrugsDialog_ViewDrugOpts, DIALOG_STYLE_LIST, "Select the drug you want to delete:",dialogstr,"Delete", "Exit");
				}
			}
			SetPVarInt(playerid, "EditDIndex", aedit); //This is not a valid index, I just don't wanna make any more pvars it just means we're editing
		} else {
			ShowPlayerDialog(playerid, EDrugsDialog_ViewDrugOpts, DIALOG_STYLE_LIST, "Select the drug you want to check on:",dialogstr,"Continue", "Exit");
		}
	} else {
		showDrugPropsDialog(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "No drugs were found here!");
	}
}
public showDrugCreateDialog(playerid) {
	ShowPlayerDialog(playerid, EDrugsDialog_EnterName, DIALOG_STYLE_INPUT, "Enter a name for this drug:","{FFFFFF}Drug Name\n{FF0000}Note:{FFFFFF} Admins have to approve drugs so make sure you enter a proper name for it.","Next", "Cancel");
}
showSideEffectsDialog(playerid, index = -1) { //Allows you to edit a drug as well, if you're an admin
	tempstr[0] = 0;
	dialogstr[0] = 0;
	if(index != -1) {
		SetPVarInt(playerid, "EditDIndex", index);
	}
	new DrugFlags:aflags = index != -1  ? Drugs[index][EDFlag] : DrugFlags:GetPVarInt(playerid, "DrugFlags");
	new statustext[32]; 
	for(new i=0;i<sizeof(DrugFlagDesc);i++) {
		if(aflags & DrugFlagDesc[i][EDrugFlag]) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}NO";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",DrugFlagDesc[i][EDrugFlagDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EDrugsDialog_SetEffects, DIALOG_STYLE_LIST, "What are the effects of this drug?",dialogstr,"Toggle", "Done");
}
public chooseDrugColor(playerid) {
	dialogstr[0] = 0;
	new temptxt[256];
	for(new i=0;i<sizeof(DrugColours);i++) {
		format(temptxt, sizeof(temptxt), "{%s}Sample %d\n",getColourString(DrugColours[i]),i);
		strcat(dialogstr,temptxt,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EDrugsDialog_SetColor, DIALOG_STYLE_LIST, "{00BFFF}Choose a drug color",dialogstr, "Ok", "Cancel");
}
public onChooseDrugColor(playerid, index) {
	SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO]: You've chosen a drug color!");
	new bizid = getBusinessInside(playerid);
	new houseid = getStandingExit(playerid, 55.0);
	if(bizid != -1 || houseid != -1) {
		if(IsDrugFactory(bizid)) {
			SetPVarInt(playerid, "OwnerType", 2);
			SetPVarInt(playerid, "Owner", Business[bizid][EBusinessID]);
		} else {
			if(Houses[houseid][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
				SetPVarInt(playerid, "OwnerType", 1);
				SetPVarInt(playerid, "Owner", GetPVarInt(playerid, "CharID"));
			}
		}
	}
	SetPVarInt(playerid, "DrugColorIndex", index);
	sendDrugRequest(playerid);
	return 1;
}
sendDrugRequest(playerid) {
	new msg[128];
	new drugname[24];
	GetPVarString(playerid, "DrugName", drugname, sizeof(drugname));
	SendClientMessage(playerid, COLOR_DARKGREEN, "Your request has been sent, please wait until an admin reviews it!");
	format(msg, sizeof(msg), "* %s wants to create the %s drug. Do /reviewdrug [playerid] to accept it or decline it.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), drugname);
	ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
}
public showDrugPropsDialog(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(DrugDialog);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",DrugDialog[i][E_DialogOptionText]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EDrugsDialog_ShowMenu, DIALOG_STYLE_LIST, "Drugs Menu",dialogstr,"Ok", "Cancel");
}
getDrugIDByDialogIndex(index, ownertype = -1, ownerid = -1) {
	new x;
	for(new i=0;i<sizeof(Drugs);i++) {
		if(Drugs[i][EDrugOwnerType] == ownertype && Drugs[i][EDrugOwner] == ownerid || ownertype == -1 || ownerid == -1) {
			if(Drugs[i][EDrugSQLID] != 0) {
				if(x++ == index) {
					return i;
				}
			}
		}
	}
	return -1;
}
getDrugSQLIDByListItem(playerid, index, &pvarid) {
	new x;
	for(new i=0;i<MAX_DRUG_SLOTS;i++) {
		if(drugsGetPVarInfo(playerid, i, DATA_DRUGTYPE) != 0) {
			if(x++ == index) {
				pvarid = i;
				return drugsGetPVarInfo(playerid, i, DATA_DRUGTYPE);
			}
		}
	}
	return -1;
}
/* Player Related - Saving / Loading */
loadPlayerDrugs(playerid) {
	new qstring[128];
	format(query, sizeof(query), "SELECT ");
	for(new i=0;i<sizeof(DrugPVars);i++) {
		for(new j=0; j<3; j++) {
			format(qstring, sizeof(qstring), "`%s`,`%s`",DrugPVars[i][E_DrugType], DrugPVars[i][E_DrugAmount]);
			format(qstring, sizeof(qstring), qstring, j, j);
			strcat(query, qstring, sizeof(query));
			strcat(query, ",", sizeof(query));
		}
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), ",`drug_effects`, Unix_Timestamp(`lastdrugtaken`), `chemicals` FROM `characters` WHERE `id` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadPlayerDrugs", "d",playerid);
}
public OnLoadPlayerDrugs(playerid) {
	new rows, fields, nrow, xvalue;
	cache_get_data(rows, fields);
	if(rows < 0 ){
		return 1;
	}
	new fieldname[32], j;
	tempstr[0] = 0;
	for(new i=0;i<MAX_DRUG_SLOTS*2;i+=2) {
		format(tempstr, sizeof(tempstr), "drug_type_%d",j);
		cache_get_row(0, i, fieldname);
		SetPVarInt(playerid, tempstr, strval(fieldname));
		cache_get_row(0, i+1, fieldname);
		format(tempstr, sizeof(tempstr), "drug_amount_%d",j);
		SetPVarInt(playerid, tempstr, strval(fieldname));
		j++;
	}
	nrow = j*2; //Because the loop above increments the fields by 2 and we end up at the 3rd field.
	cache_get_row(0, nrow++, fieldname);
	xvalue = strval(fieldname);
	SetPVarInt(playerid, "drug_effects", xvalue);
	cache_get_row(0, nrow++, fieldname);
	xvalue = strval(fieldname);
	SetPVarInt(playerid, "lastdrugtaken", xvalue);
	cache_get_row(0, nrow++, fieldname);
	xvalue = strval(fieldname);
	SetPVarInt(playerid, "chemicals", xvalue);
	#if debug
	printf("OnLoadPlayerDrugs::LastRow->%d", nrow);
	#endif
	checkIfDrugsDeleted(playerid);
	return 0;
}
/*
	Documentation on public savePlayerDrugs(playerid)
		- pvarid is the id of the pvar which is incremented once we hit the loop to format the query string.
		- E_DrugType_SQLSave and E_DrugAmount_SQLSave are the SQL formatted drug types at the top, I made this so we can update everything easily.
*/
public savePlayerDrugs(playerid) {
	new pvarid, qstring[128];
	tempstr[0] = 0;
	format(query, sizeof(query), "UPDATE `characters` SET ");
	for(new i=0;i<sizeof(DrugPVars);i++) {
		format(qstring, sizeof(qstring), "%s,%s",DrugPVars[i][E_DrugType_SQLSave], DrugPVars[i][E_DrugAmount_SQLSave]);
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) {
			//Setting String
			format(tempstr, sizeof(tempstr), qstring, pvarid, drugsGetPVarInfo(playerid, pvarid, DATA_DRUGTYPE), pvarid, drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT));
			strcat(query, tempstr, sizeof(query));
			strcat(query, ",", sizeof(query));
			pvarid++;
		}
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), ",`drug_effects` = %d, `lastdrugtaken` = FROM_UNIXTIME(%d), `chemicals` = %d WHERE `id` = %d", GetPVarInt(playerid, "drug_effects"), GetPVarInt(playerid, "lastdrugtaken"), GetPVarInt(playerid, "chemicals"), GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	#if debug
	printf("savePlayerDrugs::DEBUG QUERY: %s", query);
	#endif
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
checkIfDrugsDeleted(playerid) {
	if(IsPlayerConnectEx(playerid)) {
		new slot;
		for(new i=0; i<sizeof(DrugPVars); i++) {
			for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) { //Loop through the pvars
				if(drugsGetPVarInfo(playerid, slot, DATA_DRUGTYPE) != 0) {
					if(getDrugIDBySQLID(drugsGetPVarInfo(playerid, slot, DATA_DRUGTYPE)) == -1) {
						SendClientMessage(playerid, X11_LIGHTBLUE, "Your drug has been deleted on a drug wipe.");
						deleteSpecificPlayerDrugVar(playerid, slot);
					}
				}
				slot++;
			}
		}
	}
	return 1;
}
drugsSendStatsMessage(playerid, targetid) {
	new pvarid;
	tempstr[0] = 0;
	format(query, sizeof(query), "{%s}[Drugs]:{%s} ", getColourString(COLOR_LIGHTBLUE), getColourString(COLOR_LIGHTCYAN));
	for(new i=0;i<sizeof(DrugPVars);i++) {
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) {
			//Setting String
			format(tempstr, sizeof(tempstr), "Type %d: [%s]", pvarid+1, drugsGetNameFromSQLID(drugsGetPVarInfo(targetid, pvarid, DATA_DRUGTYPE)));
			strcat(query, tempstr, sizeof(query));
			strcat(query, " ", sizeof(query)); //Leave intentional space, to separate each type
			if(getDrugIDBySQLID(drugsGetPVarInfo(targetid, pvarid, DATA_DRUGTYPE)) != -1) {
				format(tempstr, sizeof(tempstr), "Amount: [%s]", getNumberString(drugsGetPVarInfo(targetid, pvarid, DATA_DRUGAMOUNT)));
				strcat(query, tempstr, sizeof(query));
				strcat(query, " ", sizeof(query)); //Leave intentional space, to separate each type
			}
			pvarid++;
		}
	}
	if(!isnull(query)) {
		SendClientMessage(playerid, COLOR_GREY, query);
	}
	return 1;
}
stock drugsGetNameFromSQLID(sqlid) {
	new index, xret[32];
	index = getDrugIDBySQLID(sqlid);
	if(index != -1) {
		strcpy(xret, Drugs[index][EDrugName]);
	}
	return strlen(xret) != 0 ? (xret) : ("None");
}
drugsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	deleteDrugTempPVars(playerid);
	savePlayerDrugs(playerid);
	deletePlayerDrugVars(playerid);
}
deletePlayerDrugVars(playerid, onlydrugs = 0) { //Because this can be used for lapd as well
	new j;
	tempstr[0] = 0;
	for(new i=0;i<MAX_DRUG_SLOTS*2;i+=2) {
		format(tempstr, sizeof(tempstr), "drug_type_%d",j);
		DeletePVar(playerid, tempstr);
		format(tempstr, sizeof(tempstr), "drug_amount_%d",j);
		DeletePVar(playerid, tempstr);
		j++;
	}
	if(!onlydrugs) {
		DeletePVar(playerid, "drug_effects");
		DeletePVar(playerid, "lastdrugtaken");
		DeletePVar(playerid, "chemicals");
	}
}
deleteSpecificPlayerDrugVar(playerid, pvarid) {
	if(pvarid != -1) {
		tempstr[0] = 0;
		format(tempstr, sizeof(tempstr), "drug_type_%d",pvarid);
		DeletePVar(playerid, tempstr);
		format(tempstr, sizeof(tempstr), "drug_amount_%d",pvarid);
		DeletePVar(playerid, tempstr);
	}
}
drugsFindDrugIDByName(drugname[]) { //This function returns the drug index from the server itself, not the player pvars.
	for(new i=0;i<sizeof(Drugs);i++) {
		if(strfind(Drugs[i][EDrugName], drugname, true) != -1) {
			return i;
		}
	}
	return -1;
}
findDrugIndexByName(playerid, drugname[], &pvarid) { //This function returns the drug index as well as the pvar id just from the name, right from the player's inventory.
	new index;
	for(new i=0;i<sizeof(DrugPVars);i++) {
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) {
			index = getDrugIDBySQLID(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGTYPE));
			if(index != -1) {
				if(strfind(Drugs[index][EDrugName], drugname, true) != -1) {
					if(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT) > 0) {
						return index;
					}
				}
			}
			pvarid++;
		}
	}
	pvarid = -1;
	return -1;
}
setDrugEffects(playerid, DrugFlags:aflags) {
	SetPVarInt(playerid, "drug_effects", _:aflags);
	SetPVarInt(playerid, "lastdrugtaken", (_:aflags != _:EDrugEffect_None) ? gettime() : _:EDrugEffect_None);
}
checkDrugEffects() { //Gets called from IWRP.pwn
	foreach(Player, i) { //This is left to do.
		new time = GetPVarInt(i, "lastdrugtaken");
		new timenow = gettime();
		new DrugFlags:aflags = DrugFlags:GetPVarInt(i, "drug_effects");
		if(aflags != EDrugEffect_None) {
			if(DRUG_ACTIVETIME-(timenow-time) > 0) { //If the time on the drug is still greater than 0
				if(aflags & EDrugEffect_Hallucinations) {
					//Hallucinations will be here, for now I'm just thinking on making it sound based
				} if(aflags & EDrugEffect_Hungry) {
					SetPlayerHunger(i, GetHungerLevel(i)-2); //Make them hungry faster
				} if(aflags & EDrugEffect_LessHunger) {
					SetPlayerHunger(i, GetHungerLevel(i)+2); //Take their hunger away
				} if(aflags & EDrugEffect_Immobilize) {
					new randimmob = RandomEx(0, 20);
					if(randimmob % RandomEx(9, 12) == 0) { //Do it if any of the random numbers is divisible by a number between 9 and 12
						if(GetPlayerState(i) == PLAYER_STATE_ONFOOT && IsPlayerBlocked(i) == 0 && !isInPaintball(i)) {
							ShowScriptMessage(i, "The drug ~r~immobilized ~w~you.");
							ApplyAnimation(i, "CRACK", "crckdeth2", 4.1, 0, 1, 1, 1, 0, 1);
							SetTimerEx("getPlayerUpAnim",RandomEx(5000, 10000),false,"d",i);
						}
					}
				} if(aflags & EDrugEffect_TreatDiseases) {
					if(playerHasUncureableDisease(i)) return 1;
					curePlayerDisease(i);
				} if(aflags & EDrugEffect_Drowsiness) {
					if(~aflags & EDrugEffect_NoDrowsiness) {
						SetPlayerDrunkLevel(i, 5000); //Make them drowsy since the other flag isn't set
					}
				} if(aflags & EDrugEffect_NoDrowsiness) {
					SetPlayerDrunkLevel(i, 0);
				}
				killPlayerOnOverdose(i);
			} else {
				setDrugEffects(i, EDrugEffect_None); //Drug effects all gone after this
			}
		}
	}
	return 1;
}
killPlayerOnOverdose(playerid) {
	if(getDrugLevel(playerid) >= MINIMUM_OVERDOSE) {
		ShowScriptMessage(playerid, "Warning, you've ~r~overdosed ~w~and you will probably ~r~die ~w~from it.");
		new randkill = RandomEx(0, 20);
		if(randkill % RandomEx(9, 12) == 0) { //Do it if any of the random numbers is divisible by a number between 9 and 12
			ShowScriptMessage(playerid, "You ~r~died ~w~from an ~r~overdose~w~.");
			setDrugEffects(playerid, EDrugEffect_None); //Drug effects all gone after this
			SetPlayerHealthEx(playerid, 0);
		}
	}
	return 1;
}
getDrugLevel(playerid) {
	new dlevel;
	new DrugFlags:aflags = DrugFlags:GetPVarInt(playerid, "drug_effects");
	for(new i=0;i<sizeof(DrugFlagDesc);i++) {
		if(aflags & DrugFlagDesc[i][EDrugFlag]) {
			dlevel += DRUGDOSE_BASE_AMOUNT;
		}
	}
	return dlevel;
}
getTimesEffectUsed(DrugFlags:effect) {
	new times;
	for(new i=0;i<sizeof(Drugs);i++) {
		if(effect & Drugs[i][EDFlag]) {
			times++;
		}
	}
	return times;
}
produceDrugEffect(playerid, index) { //The index of the drug we took
	if(index != -1) {
		new Float:health;
		if(Drugs[index][EDFlag] & EDrugEffect_Health) {
        	GetPlayerHealth(playerid, health);
			SetPlayerHealthEx(playerid, health + 5.0);
		}
		if(Drugs[index][EDFlag] & EDrugEffect_Armour) {
			GetPlayerArmourEx(playerid, health);
			SetPlayerArmourEx(playerid, health + 5.0);
		}
		if(Drugs[index][EDFlag] & EDrugEffect_Smokeable) {
			SetPlayerSpecialAction(playerid, SPECIAL_ACTION_SMOKE_CIGGY);
			tryMakeOthersInVehHigh(playerid, index);
		}
		if(Drugs[index][EDFlag] & EDrugEffect_Drinkable) {
			SetPlayerSpecialAction(playerid, SPECIAL_ACTION_DRINK_SPRUNK);
		}
	}
	return 1;
}
tryMakeOthersInVehHigh(playerid, index) {
	new msg[128];
	new carid = GetPlayerVehicleID(playerid);
	if(!vehicleWindowsDown(carid)) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				if(IsPlayerInAnyVehicle(i)) {
					if(GetPlayerVehicleID(i) == carid) {
						if(i != playerid) {
							format(msg, sizeof(msg), "You're now ~r~smoking ~w~passive ~r~%s ~w~smoke.", drugsGetNameFromSQLID(Drugs[index][EDrugSQLID]));
							ShowScriptMessage(i, msg);
							setDrugEffects(i, Drugs[index][EDFlag]);
						}
					}
				}
			}
		}
	}
	return 1;
}
drugsShowGiveDrugMenu(playerid, target, amount, drop = 0) { //0 = default (This can be used to give or drop a drug)
	SetPVarInt(playerid, "DrugTempTarget", target);
	SetPVarInt(playerid, "DrugTempAmount", amount);
	new pvarid;
	dialogstr[0] = 0;
	tempstr[0] = 0;
	if(!hasAnyDrugs(playerid)) {
		deleteDrugTempPVars(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You're not carrying any drugs with you!");
		return 1;
	}
	for(new i=0;i<sizeof(DrugPVars);i++) {
		for(new k=0;k<MAX_DRUG_SLOTS*2;k+=2) {
			//Setting String
			if(getDrugIDBySQLID(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGTYPE)) != -1) {
				format(tempstr, sizeof(tempstr), "%s: %s %s\n", drugsGetNameFromSQLID(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGTYPE)), getNumberString(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGAMOUNT)), drugsGetMeasureUnit(getDrugIDBySQLID(drugsGetPVarInfo(playerid, pvarid, DATA_DRUGTYPE))));
				strcat(dialogstr, tempstr, sizeof(dialogstr));
			}
			pvarid++;
		}
	}
	ShowPlayerDialog(playerid, drop == 0 ? EDrugsDialog_ShowGiveDrug : EDrugsDialog_ShowDropDrug, DIALOG_STYLE_LIST, drop == 0 ? ("Which drug would you like to give?") : ("Which drug would you like to drop") ,dialogstr,drop == 0 ? ("Give") : ("Drop"), "Cancel");
	return 1;
}
drugsPharmacyMenu(playerid, bizid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new j;
	for(new i=0;i<sizeof(Drugs);i++) {
		if(Drugs[i][EDrugSQLID] != 0) {
			if(Drugs[i][EDrugOwnerType] == 2 && Drugs[i][EDrugOwner] == Business[bizid][EBusinessID]) { //Only show the drugs they've at the biz if they're inside it, otherwise it'll be a mess of indexes
				if(Drugs[i][EDrugPPG] > 0) { //The drug can be sold
					format(tempstr,sizeof(tempstr),"{FFFFFF}%s - $%d per %s\n",Drugs[i][EDrugName],Drugs[i][EDrugPPG],drugsGetMeasureUnit(i));
					strcat(dialogstr,tempstr,sizeof(dialogstr));
					j++;
				}
			}
		}
	}
	ShowPlayerDialog(playerid, j == 0 ? EDrugsDialog_DoNothing : EDrugsDialog_PharmacyBuyMenu, j == 0 ? DIALOG_STYLE_MSGBOX : DIALOG_STYLE_LIST, "Pharmacy Menu",j == 0 ? ("The business doesn't have anything for sale.") : dialogstr,j == 0 ? ("Ok") : ("Select"), "Cancel");
	SetPVarInt(playerid, "BuyingBiz", bizid);
}
/* Commands */
YCMD:businessdrugs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to view your drugs.");
		return 1;
	}
	new bizid = getBusinessInside(playerid);
	new houseid = getStandingExit(playerid, 55.0);
	if(bizid != -1) { //I got tired of trying to come up with less lines of code here since the commands stopped working when I did so I've to use this recursive thing which is horrible..
		if(IsDrugFactory(bizid) && Business[bizid][EBusinessOwner] == GetPVarInt(playerid,"CharID")) {
			showDrugPropsDialog(playerid);
			return 1;
		}
	}
	if(houseid != -1) {
		if(Houses[houseid][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
			showDrugPropsDialog(playerid);
			return 1;
		}
	}
	SendClientMessage(playerid, X11_TOMATO_2, "You need to be at either a drug factory or at your own house to do this!");
	return 1;
}
YCMD:reviewdrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to review a drug that is pending.");
		return 1;
	}
	new player;
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarType(player, "DrugName") == PLAYER_VARTYPE_NONE || GetPVarType(player, "Owner") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player isn't requesting to create a drug!");
			return 1;
		}
		dialogstr[0] = 0;
		tempstr[0] = 0;
		new DrugFlags:aflags = DrugFlags:GetPVarInt(player, "DrugFlags");
		strcat(dialogstr, "Effects may include:\n");
		for(new i=0;i<sizeof(DrugFlagDesc);i++) {
			if(aflags & DrugFlagDesc[i][EDrugFlag]) {
				format(tempstr, sizeof(tempstr), "- %s (%d)\n",DrugFlagDesc[i][EDrugFlagDescAlt], getTimesEffectUsed(DrugFlagDesc[i][EDrugFlag]));
				strcat(dialogstr, tempstr, sizeof(dialogstr));
			}
		}
		SetPVarInt(playerid, "PlayerChecking", player);
		ShowPlayerDialog(playerid, EDrugsDialog_AcceptDecline, DIALOG_STYLE_MSGBOX, "{00BFFF}Review", dialogstr, "Accept", "Decline");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /reviewdrug [playerid]");
		return 1;
	}
	return 1;
}
YCMD:usedrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to use a specific drug");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	new type[32], msg[128], index, pvarid;
	if(!sscanf(params, "s[32]", type)) {
		index = findDrugIndexByName(playerid, type, pvarid);
		if(index != -1) {
			if(Drugs[index][EDFlag] & EDrugEffect_Smokeable) {
				new matches = GetPVarInt(playerid, "Matches");
				if(matches > 0) {
					SetPVarInt(playerid, "Matches", --matches);
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have any matches with you, buy some at a 24/7!");
					return 1;
				}
			}
			format(msg, sizeof (msg), "** %s consumes some %s.", GetPlayerNameEx(playerid, ENameType_RPName), Drugs[index][EDrugName]);
			ProxMessage(30.0,playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
			SetPlayerChatBubble(playerid, msg, COLOR_PURPLE, 30.0, 5000);
			setDrugPVar(playerid, index, pvarid, 1, EDrugs_Type_Decrease);
			setDrugEffects(playerid, Drugs[index][EDFlag]);
			produceDrugEffect(playerid, index);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have any more of that drug!");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "[USAGE]: /usedrug <part of drug name>");
		return 1;
	}
	return 1;
}
YCMD:givedrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to give a player a specific drug");
		return 1;
	}
	new type[32], msg[128], index, pvarid, num, target;
	if(!sscanf(params, "k<playerLookup_acc>s[32]d", target, type, num)) {
		index = drugsFindDrugIDByName(type);
		if(index != -1) {
			pvarid = getUsableDrugSpot(target, Drugs[index][EDrugSQLID], STATE_ANYTHING);
			if(pvarid != -1) {
				format(msg, sizeof(msg),"* %sg %s sent!", getNumberString(num), Drugs[index][EDrugName]);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD,msg);
				format(msg, sizeof(msg),"* An admin sent you %s%s of %s!", getNumberString(num), drugsGetMeasureUnit(index), Drugs[index][EDrugName]);
				SendClientMessage(target, COLOR_LIGHTBLUE, msg);
				setDrugPVar(target, index, pvarid, num, EDrugs_Type_Increase);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "The player's drug slots are occupied.");
				SendClientMessage(target, X11_TOMATO_2, "The admin isn't able to send you drugs since all your drug slots are occupied.");
			}
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Drug not found, make sure you spelled it right.");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "[USAGE]: /givedrug <playerid> <part of drug name> <amount>");
		return 1;
	}
	return 1;
}
YCMD:editdrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to edit a drug.");
		return 1;
	}
	viewDrugProduction(playerid, 1);
	return 1;
}
YCMD:drugtest(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to drug test a player.");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && getFactionType(faction) != EFactionType_EMS) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop or a medic!");
		return 1;
	}
	if(!IsOnDuty(playerid) && !isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	new player;
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(playerid, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(player, 3.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		if(getDrugLevel(player) != 0) {
			SendClientMessage(playerid, X11_WHITE, "This person has apparently taken some drugs in the last 30 minutes.");
			return 1;
		} else {
			SendClientMessage(playerid, X11_WHITE, "This person is clean, no drugs have shown up.");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /drugtest [playerid]");
	}
	return 1;
}
YCMD:reloaddrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Reloads a drug from the database by SQLID. Could cause massive mess if used / edited improperly!");
		return 1;
	}
	new id;
	if (!sscanf(params, "d", id))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Reloading drug");
		for(new i=0;i<sizeof(Drugs);i++) {
			if(Drugs[i][EDrugSQLID] == id) {
				reloadDrug(i);
				return 1;
			}
		}
		SendClientMessage(playerid, X11_WHITE, "Failed to reload drug");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /reloaddrug [sqlid]");
		SendClientMessage(playerid, X11_WHITE,"Use with caution! Could cause massive mess that would impact all the players if used / edited improperly!");
	}
	return 1;
}
YCMD:deletedrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Deletes a drug from the Database. Affects all players that own it, use carefully!");
		return 1;
	}
	viewDrugProduction(playerid, 2);
	SendClientMessage(playerid, X11_TOMATO_2,"Use with caution! Could cause massive mess that would impact all the players if used / edited improperly!");
	return 1;
}