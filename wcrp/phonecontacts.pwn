/*
-------
	PVars Used By This Script:
-------
*/

#define MAX_CONTACTS 	30
#define TYPE_DESC		1
#define TYPE_NUMBER 	2
/* Modes */
#define MODE_VIEW		1
#define MODE_ADD		2
#define MODE_DELETE 	3
#define MODE_EDIT		4


/* Forwards */
forward OnReceiveContacts(playerid, checkMode);
forward sendPlayerContacts(playerid, checkMode);
forward showContactMenu(playerid);
forward populateContactArray(playerid);
forward OnPopulateArray(playerid);
forward addPlayerContact(playerid, checkMode);
forward deletePlayerContact(playerid, checkMode);
/* Enums */
enum {
	EContactSystem_AddContact = EContactSystem_Base + 1,
	EContactSystem_ShowMenu,
	EContactSystem_ShowContacts,
	EContactSystem_Advanced,
	EContactSystem_AddNumber,
	EContactSystem_DeleteContact,
	EContactSystem_SendSMS,
	EContactSystem_DoNothing,
};
enum E_ContactSystem {
	E_DialogOptionText[128],
	E_DoContactCallBack[128],
	E_Mode,
}
/* New's */
new PhoneContacts[][E_ContactSystem] = {
	{"My Contacts", "sendPlayerContacts", MODE_VIEW},
	{"Add A Contact", "addPlayerContact", MODE_ADD},
	{"Delete A Contact", "deletePlayerContact", MODE_DELETE}
	//{"Edit A Contact", "editPlayerContact", MODE_EDIT}
};

enum EPhoneContacts {
	EMySQLID,
	EPhoneNumber,
};
new PhoneContactsArray[MAX_PLAYERS][MAX_CONTACTS][EPhoneContacts];
/* Functions */
public showContactMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(PhoneContacts);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",PhoneContacts[i][E_DialogOptionText]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EContactSystem_ShowMenu, DIALOG_STYLE_LIST, "Contact Menu",dialogstr,"Ok", "Cancel");
}
public sendPlayerContacts(playerid, checkMode) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT `phonenr`, `desc` FROM `phonecontacts` WHERE `charid` = %d LIMIT %d",GetPVarInt(playerid, "CharID"), MAX_CONTACTS);
	mysql_function_query(g_mysql_handle, query, true, "OnReceiveContacts", "dd",playerid,checkMode);
}
public OnReceiveContacts(playerid,checkMode) {
	new rows,fields;
	dialogstr[0] = 0;
	query[0] = 0;
	cache_get_data(rows,fields);
	if(rows < 1) {
		format(query, sizeof(query), "**** Contacts for: %s ****\n",GetPlayerNameEx(playerid, ENameType_CharName));
		strcat(dialogstr,query,sizeof(dialogstr));
		strcat(dialogstr,"No Contacts Found!",sizeof(dialogstr));
		ShowPlayerDialog(playerid, EContactSystem_DoNothing, DIALOG_STYLE_MSGBOX, "Contacts:", dialogstr, "Close", "");
		return 1;
	} else {
		new number, desc[32], field_data[32];
		for(new i=0;i<rows;i++) {
			//ID is useless here, we do that on populateContactArray..
			cache_get_row(i, 0, field_data);
			number = strval(field_data);
			cache_get_row(i, 1, desc);

			format(query, sizeof(query), "%s - %d\n", desc, number);
			strcat(dialogstr,query,sizeof(dialogstr));
		}
	}
	forwardCheckMode(playerid, checkMode, dialogstr);
	return 0;
}
forwardCheckMode(playerid, checkMode, dstring[]) { //This is where we do the call to update our contact array
	populateContactArray(playerid);
	switch(checkMode) {
		case MODE_VIEW: {
			ShowPlayerDialog(playerid, EContactSystem_ShowContacts, DIALOG_STYLE_LIST, "Contacts:", dstring, "Call", "SMS");
		}
		case MODE_DELETE: {
			ShowPlayerDialog(playerid, EContactSystem_DeleteContact, DIALOG_STYLE_LIST, "Contacts:", dstring, "Delete", "Cancel");
		}
	}
}
public populateContactArray(playerid) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT `id`, `phonenr` FROM `phonecontacts` WHERE `charid` = %d LIMIT %d",GetPVarInt(playerid, "CharID"), MAX_CONTACTS);
	mysql_function_query(g_mysql_handle, query, true, "OnPopulateArray", "d",playerid);
}
public OnPopulateArray(playerid) {
	new rows,fields;
	cache_get_data(rows,fields);
	if(rows < 1) {
		//Nothing to populate..
		return 1;
	} else {
		new field_data[32];
		for(new i=0;i<rows;i++) {
			cache_get_row(i,0,field_data);
			PhoneContactsArray[playerid][i][EMySQLID] = strval(field_data); //Populate / Update the player array everytime this gets called
			cache_get_row(i,1,field_data);
			PhoneContactsArray[playerid][i][EPhoneNumber] = strval(field_data); //Populate / Update the player array everytime this gets called
		}
	}
	return 1;
}
phoneCOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused response
	#pragma unused listitem
	#pragma unused inputtext
	phonecHandleDialog(playerid, dialogid, response, listitem, inputtext);
	return 1;
}
phonecHandleDialog(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	#pragma unused listitem
	switch(dialogid) {
		case EContactSystem_ShowMenu: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the contact menu.");
				return 1;
			}
			CallLocalFunction(PhoneContacts[listitem][E_DoContactCallBack],"dd", playerid, PhoneContacts[listitem][E_Mode]);
		}
		case EContactSystem_ShowContacts: {
			if(!response) {
				startSendSMS(playerid, listitem); //Edit, call, sms, etc..
				return 1;
			}
			tryCallContact(playerid, listitem);
		}
		case EChequeSystem_DoNothing: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the contact menu.");
				return 1;
			}
		}
		case EContactSystem_AddContact: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the contact menu.");
				return 1;
			}
			if(strlen(inputtext) < 2) {
				ShowScriptMessage(playerid, "Please enter a valid description for this phone number..", 5000);
				addPlayerContact(playerid, MODE_ADD);
				return 1;
			}
			contactsCreateTempPVar(playerid, "TempContactDesc", inputtext, TYPE_DESC);
			addPlayerNumber(playerid);
		}
		case EContactSystem_AddNumber: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the contact menu.");
				return 1;
			}
			contactsCreateTempPVar(playerid, "TempContactNumber", inputtext, TYPE_NUMBER);
			tryToCreateContact(playerid);
		}
		case EContactSystem_DeleteContact: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the contact menu.");
				return 1;
			}
			DeleteContact(playerid, listitem);
		}
		case EContactSystem_SendSMS: {
			if(!response) {
				showContactMenu(playerid);
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You canceled the sms.");
				return 1;
			}
			trySendMessage(playerid, inputtext);
		}
	}
	return 1;
}
contactsCreateTempPVar(playerid, pvarname[], text[], type) {
	switch(type) {
		case TYPE_NUMBER: {
			SetPVarInt(playerid, pvarname, strval(text));
		}
		case TYPE_DESC: {
			SetPVarString(playerid, pvarname, text);
		}
	}
}
getPhoneNumberByCharID(charid) {
	foreach(Player, i) {
		if(GetPVarInt(i, "CharID") == charid) {
			if(IsPlayerConnectEx(i))
				return GetPVarInt(i, "PhoneNumber");
		}
	}
	return -1;
}
tryCallContact(playerid, listitem) {
	new phonenumber = PhoneContactsArray[playerid][listitem][EPhoneNumber];
	new player = findPlayerFromPhoneNumber(phonenumber);
	if(player != -1) {
		tryCallNumber(playerid, phonenumber);
	} else {
		ShowScriptMessage(playerid, "There was an error when trying to call, try again later..", 5000);
		showContactMenu(playerid);
	}
}
startSendSMS(playerid, listitem) {
	SetPVarInt(playerid, "TempDialogIndex", listitem);
	ShowPlayerDialog(playerid, EContactSystem_SendSMS, DIALOG_STYLE_INPUT, "Send SMS:", "Enter a message", "Send", "Cancel");
}
trySendMessage(playerid, message[]) {
	new savedindex = GetPVarInt(playerid, "TempDialogIndex");
	new phonenumber = PhoneContactsArray[playerid][savedindex][EPhoneNumber];
	new player = findPlayerFromPhoneNumber(phonenumber);
	deleteContactPVars(playerid);
	if(strlen(message) < 1) {
		startSendSMS(playerid, savedindex);
		ShowScriptMessage(playerid, "Invalid message. Try again..", 5000);
		return 1;
	}
	if(player != -1) {
		trySendSMS(playerid, phonenumber, message);
	} else {
		ShowScriptMessage(playerid, "There was an error when trying to deliver your message, try again later..", 5000);
		showContactMenu(playerid);
	}
	return 1;
}
public addPlayerContact(playerid, checkMode) {
	ShowPlayerDialog(playerid, EContactSystem_AddContact, DIALOG_STYLE_INPUT, "Add A Contact:", "Enter a description for this contact", "Next", "Cancel");
}
public deletePlayerContact(playerid, checkMode) {
	sendPlayerContacts(playerid, checkMode);
}
addPlayerNumber(playerid) {
	ShowPlayerDialog(playerid, EContactSystem_AddNumber, DIALOG_STYLE_INPUT, "Add A Contact:", "Enter a number for this contact", "Add", "Cancel");
}
FindFreeContact(playerid) {
	for(new i=0;i<MAX_CONTACTS;i++) {
		if(PhoneContactsArray[playerid][i][EMySQLID] == 0) {
			return i;
		}
	}
	return -1;
}
tryToCreateContact(playerid) {
	new phonenumber = GetPVarInt(playerid, "TempContactNumber");
	new contactdesc[32];
	if(FindFreeContact(playerid) == -1) {
		ShowScriptMessage(playerid, "You can't store any more contacts, try deleting some of them.", 5000);
		showContactMenu(playerid);
		return 1;
	}
	GetPVarString(playerid, "TempContactDesc", contactdesc, sizeof(contactdesc));
	deleteContactPVars(playerid);
	createContact(playerid, contactdesc, phonenumber);
	return 1;
}
createContact(playerid, contactdesc[], number) { //Use carefully, not to play around
	query[0] = 0;//[128];
	new charid = GetPVarInt(playerid, "CharID");
	mysql_real_escape_string(contactdesc,contactdesc);
	ShowScriptMessage(playerid, "You've successfully added a contact to your contact list!", 5000);
	format(query, sizeof(query),"INSERT INTO `phonecontacts` (`charid`, `phonenr`, `desc`, `charidphonenr`) values (%d, %d, '%s', %d)",charid,number,contactdesc,getPhoneNumberByCharID(charid));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	showContactMenu(playerid);
}
DeleteContact(playerid, listitem) {
	query[0] = 0;//[128];
	format(query, sizeof(query),"DELETE FROM `phonecontacts` where id = %d",PhoneContactsArray[playerid][listitem][EMySQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	ShowScriptMessage(playerid, "You've successfully deleted a contact from your contact list!", 5000);
	showContactMenu(playerid);
}
deleteContactPVars(playerid) {
	DeletePVar(playerid, "TempContactDesc");
	DeletePVar(playerid, "TempContactNumber");
	DeletePVar(playerid, "TempDialogIndex");
}
deleteContactVariables(playerid) {
	for(new i=0; i<MAX_CONTACTS; i++) {
		PhoneContactsArray[playerid][i][EMySQLID] = 0;
		PhoneContactsArray[playerid][i][EPhoneNumber] = 0;
	}
}
contactsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	deleteContactPVars(playerid);
	deleteContactVariables(playerid);
}
/* Commands */
YCMD:contacts(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to view your contacts, do a call or send an sms.");
		return 1;
	}
	new status = GetPVarInt(playerid, "PhoneStatus");
	if(getPhoneType(playerid) == 0 || GetPVarInt(playerid, "PhoneNumber") == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	if(isJailed(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this here!");
		return 1;
	}
	if(status == 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your phone is off!");
		return 1;
	}
	showContactMenu(playerid);
	return 1;
}