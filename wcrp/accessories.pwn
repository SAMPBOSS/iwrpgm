/*
	PVars used by this
	
	Accessory0-9(int) - index of accessory at this slot
	
	AccessoryX/Y/ZINDEX(float) offset on X/Y/Z 
	AccessoryRotX/Y/ZINDEX
	AccessoryScaleX/Y/ZINDEX
	AccessoryBoneINDEX
	AccessoryEnabled0-9
	
	hasAccessoryN(int) - 1 if player owns this accessory(index of Accessories array)
	userAccessorySQLIDN(int) - SQL ID of accessory
*/

//slow 0-6 = accessories
//7 = specialitems
//8 = backpack?
//9 = cop item

#define BONE_SPINE 1
#define BONE_HEAD 2
#define BONE_LARM 3
#define BONE_RARM 4
#define BONE_LHAND 5
#define BONE_RHAND 6
#define BONE_LTHIGH 7
#define BONE_RTHIGH 8
#define BONE_LFOOT 9
#define BONE_RFOOT 10
#define BONE_RCALF 11
#define BONE_LCALF 12
#define BONE_LFOREARM 13
#define BONE_RFOREARM 14
#define BONE_LCLAVICLE 15
#define BONE_RCLAVICLE 16
#define BONE_NECK 17
#define BONE_JAW 18

#define NUM_ACCESSORY_SLOTS 6

enum {
	EAccessories_MainMenu = EAccessoriesDialog_Base+1,
	EAccessories_EditAccessory,
	EAccessories_SetSlot,
	EAccessories_EditSlot,
	EAccessories_ToggleSlot,
	EAccessories_SelectEditItem,
	EAccessories_ModifyItem,
	EAccessories_SetBone,
	EAccessories_SetValue,
	EAccessories_DeleteAccessory,
	EAccessories_ItemBoneChange,
};

new AccessoryCategories[][] = {{"Glasses"}, {"Masks"}, {"Hand Held Items"}, {"Hats"}, {"Helmets"}, {"Bandanas"}, {"Cellphones"}, {"Bags"}, {"Watches"}, {"Misc"}};

enum EAccessoryInfo {
	EAccessoryName[32],
	EAccessoryModelID,
	EAccessoryBone, //default bone
	EMenuID,
	EAccessoryPrice,
	EAccessoryDPPrice,
};

new Accessories[][EAccessoryInfo] = {
	{"Glasses",19006,BONE_HEAD, 0, 100, 1},
	{"Briefcase",1210,BONE_LHAND, 2, 500, 1},
	{"Flashlight",18641,BONE_LHAND, 2, 250, 1},
	{"Fishing Rod",18632,BONE_LHAND, 2, 250, 1},
	{"Wrench",18633,BONE_LHAND, 2, 250, 1},
	{"Crowbar",18634,BONE_LHAND, 2, 250, 1},
	{"Hammer",18635,BONE_LHAND, 2, 250, 1},
	{"Screwdriver",18644,BONE_LHAND, 2, 250, 1},
	{"Hard Hat",18638,BONE_HEAD, 3, 250, 1},
	{"Black Hat",18639,BONE_HEAD, 3, 500, 1},
	{"Red Laser Pointer",18643,BONE_LHAND, 2, 0, 50},
	{"Purple Laser Pointer",19080,BONE_LHAND, 2, 0, 50},
	{"Pink Laser Pointer",19081,BONE_LHAND, 2, 0, 50},
	{"Orange Laser Pointer",19082,BONE_LHAND, 2, 0, 50},
	{"Green Laser Pointer",19083,BONE_LHAND, 2, 0, 50},
	{"Yellow Laser Pointer",19084,BONE_LHAND, 2, 0, 50},
	{"Boater Hat 1",18944,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 2",18945,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 3",18946,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 4",18947,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 5",18948,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 6",18949,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 7",18940,BONE_HEAD, 3, 500, 1},
	{"Boater Hat 8",18941,BONE_HEAD, 3, 500, 1},
	{"Boxing Helmet",18942,BONE_HEAD, 3, 500, 1},
	{"Hat Man 1",18967,BONE_HEAD, 3, 500, 1},
	{"Hat Man 2",18968,BONE_HEAD, 3, 500, 1},
	{"Hat Man 3",18969,BONE_HEAD, 3, 500, 1},
	{"Hat Tiger",18970,BONE_HEAD, 3, 500, 1},
	{"Hat Cool 1",18971,BONE_HEAD, 3, 500, 1},
	{"Hat Cool 2",18972,BONE_HEAD, 3, 500, 1},
	{"Hat Cool 3",18973,BONE_HEAD, 3, 500, 1},
	{"Santa Hat 1",19064,BONE_HEAD, 3, 500, 1},
	{"Santa Hat 2",19065,BONE_HEAD, 3, 500, 1},
	{"Santa Hat 3",19066,BONE_HEAD, 3, 500, 1},
	{"Hoody Hat 1",19067,BONE_HEAD, 3, 500, 1},
	{"Hoody Hat 2",19068,BONE_HEAD, 3, 500, 1},
	{"Hoody Hat 3",19069,BONE_HEAD, 3, 500, 1},
/*
	{"Parrot 1",19078,BONE_LCLAVICLE, 3, 500, 1},
	{"Parrot 2",19079,BONE_LCLAVICLE, 3, 500, 1},
*/
	{"Guitar",19317, BONE_LHAND, 2, 1500, 5},
	{"Glasses 1",19007,BONE_HEAD, 0, 500, 1},
	{"Glasses 2",19008,BONE_HEAD, 0, 500, 1},
	{"Glasses 3",19009,BONE_HEAD, 0, 500, 1},
	{"Glasses 4",19010,BONE_HEAD, 0, 500, 1},
	{"Glasses 5",19011,BONE_HEAD, 0, 500, 1},
	{"Glasses 6",19012,BONE_HEAD, 0, 500, 1},
	{"Glasses 7",19013,BONE_HEAD, 0, 500, 1},
	{"Glasses 8",19014,BONE_HEAD, 0, 500, 1},
	{"Glasses 9",19015,BONE_HEAD, 0, 500, 1},
	{"Glasses 10",19016,BONE_HEAD, 0, 500, 1},
	{"Glasses 11",19017,BONE_HEAD, 0, 500, 1},
	{"Glasses 12",19018,BONE_HEAD, 0, 500, 1},
	{"Glasses 13",19019,BONE_HEAD, 0, 500, 1},
	{"Glasses 14",19020,BONE_HEAD, 0, 500, 1},
	{"Glasses 15",19021,BONE_HEAD, 0, 500, 1},
	{"Glasses 16",19022,BONE_HEAD, 0, 500, 1},
	{"Glasses 17",19022,BONE_HEAD, 0, 500, 1},
	{"Glasses 18",19023,BONE_HEAD, 0, 500, 1},
	{"Glasses 19",19024,BONE_HEAD, 0, 500, 1},
	{"Glasses 20",19025,BONE_HEAD, 0, 500, 1},
	{"Glasses 21",19026,BONE_HEAD, 0, 500, 1},
	{"Glasses 22",19027,BONE_HEAD, 0, 500, 1},
	{"Glasses 23",19028,BONE_HEAD, 0, 500, 1},
	{"Glasses 24",19029,BONE_HEAD, 0, 500, 1},
	{"Glasses 25",19030,BONE_HEAD, 0, 500, 1},
	{"Glasses 26",19031,BONE_HEAD, 0, 500, 1},
	{"Glasses 27",19032,BONE_HEAD, 0, 500, 1},
	{"Glasses 28",19033,BONE_HEAD, 0, 500, 1},
	{"Glasses 29",19034,BONE_HEAD, 0, 500, 1},
	{"Glasses 30",19035,BONE_HEAD, 0, 500, 1},
	{"Hockey Mask 1",19036,BONE_HEAD, 1, 500, 1},
	{"Hockey Mask 2",19037,BONE_HEAD, 1, 2500, 1},
	{"Hockey Mask 3",19038,BONE_HEAD, 1, 4500, 1},
	{"Monocole",19349,BONE_HEAD, 1, 5500, 1},
	{"Top Hat",19352,BONE_HEAD, 3, 5500, 1},
	{"Cane",19348,BONE_LHAND, 2, 5500, 1},
	{"Cowboy hat 1",19095,BONE_HEAD, 3, 500, 1},
	{"Cowboy hat 2",19096,BONE_HEAD, 3, 500, 1},
	{"Cowboy hat 3",19097,BONE_HEAD, 3, 500, 1},
	{"Cowboy hat 4",19098,BONE_HEAD, 3, 500, 1},
	{"Cowboy hat 5",19096,BONE_HEAD, 3, 500, 1},
	{"Burger Hat",19094,BONE_HEAD, 3, 500, 1},
	{"\"Dude\" Hat",19093,BONE_HEAD, 3, 500, 1},
	{"\"100 Percent Gangster\" Hat",19067,BONE_HEAD, 3, 500, 1},
	{"Yellow/Black Hat",18973,BONE_HEAD, 3, 500, 1},
	{"Mosiac Hat",18966,BONE_HEAD, 3, 500, 1},
	{"Water Hat",18939,BONE_HEAD, 3, 500, 1},
	{"Blue/Dark Blue Hat",18940,BONE_HEAD, 3, 500, 1},
	{"Army Beret",18924,BONE_HEAD, 3, 500, 1},
	{"Skulls Bandana",18911,BONE_HEAD, 5, 500, 1},
	{"Black Bandana",18912,BONE_HEAD, 5, 500, 1},
	{"Camouflage Bandana",18914,BONE_HEAD, 5, 500, 1},
	{"Pink/Orange Bandana",18915,BONE_HEAD, 5, 500, 1},
	{"Yellow/Red Triangular Bandana",18916,BONE_HEAD, 5, 500, 1},
	{"Water Bandana",18917,BONE_HEAD, 5, 500, 1},
	{"Grey/White Bandana",18918,BONE_HEAD, 5, 500, 1},
	{"White/Black Bandana",18919,BONE_HEAD, 5, 500, 1},
	{"Brown/Yellow Bandana",18920,BONE_HEAD, 5, 500, 1},
	{"CJ Elvis Head",18963,BONE_HEAD, 25, 15000, 1},
	{"Black Winter Hat",18964,BONE_HEAD, 5, 500, 1},
	
	//Helmets--------------------------------------------
	{"White Full Helmet", 18978, BONE_HEAD, 4, 250, 1},
	{"Red Full Helmet", 18977, BONE_HEAD, 4, 250, 1},
	{"Pink Full Helmet", 18979, BONE_HEAD, 4, 250, 1},
	{"Flame Full Helmet", 18645, BONE_HEAD, 4, 250, 1},
	{"Electric Blue Full Helmet", 18978, BONE_HEAD, 4, 250, 1},
	{"Red Cheetah Full Helmet", 18977, BONE_HEAD, 4, 250, 1},
	{"Green Full Helmet", 18977, BONE_HEAD, 4, 250, 1},
	{"Blue MotoCross Helmet", 18976, BONE_HEAD, 4, 250, 1},
	{"White Biker Helmet", 18936, BONE_HEAD, 4, 250, 1},
	{"Red Biker Helmet", 18937, BONE_HEAD, 4, 250, 1},
	{"Blue Biker Helmet", 18938, BONE_HEAD, 4, 250, 1},
	
	//Mask-Bandanas-----------------------------------------
	{"White Hockey Mask" , 19036 , BONE_HEAD,  5, 500, 10} , 	
	{"Red Hockey Mask" , 19037 , BONE_HEAD ,  5 , 500, 10} , 	
	{"Green Hockey Mask" , 19037 , BONE_HEAD ,  5 ,500, 10} , 	
	{"Zorro Mask" , 18974 , BONE_HEAD ,  5 , 500, 10} , 	
	{"Gas Mask" , 19472 , BONE_HEAD ,  5 , 200, 1} , 
	{"Eye Patch" , 18905 , BONE_HEAD , 5 , 1000, 1} ,
	{"Black Bandana", 18912 , BONE_HEAD, 5, 100, 1},
	{"Green Bandana", 18913, BONE_HEAD, 5, 100, 1},
	{"Black Balaclava", 19801, BONE_HEAD, 5, 100, 1},
	
	//CellPhones-----------------------------------------------------
	
	{ "Gold Cell Phone" , 18865 , BONE_LHAND , 6 , 0 , 2} , 
	{ "Blue Cell Phone" , 18866 , BONE_LHAND , 6 , 75, 1} , 
	{ "Orange Cell Phone" , 18867 , BONE_LHAND , 6 , 75 ,0 } , 
	{ "Black Cell Phone" , 18868 , BONE_LHAND , 6 , 75, 1} , 
	{ "Pink Cell Phone" , 18869 , BONE_LHAND , 6 , 0, 2 } , 
	{ "Red Cell Phone" , 18870 , BONE_LHAND , 6 , 0, 2} , 
	{ "Green Cell Phone" , 18871 , BONE_LHAND , 6 , 0, 2 } , 
	{ "Dark Blue Cell Phone" , 18872 , BONE_LHAND , 6 , 75 , 1} , 
	{ "Yellow Cell Phone" , 18873 , BONE_LHAND , 6 , 75 , 1} , 
	{ "White Cell Phone" , 18874 , BONE_LHAND , 6 , 75, 2} ,
	
	//Bags-------------------------------------------------------------------
	{ "Hiker Backpack" , 19559 , BONE_LHAND , 7 , 250, 1} , 
	{ "Parachute Backpack" , 371 , BONE_LHAND , 7 , 250, 1} , 
	{ "Suitcase" , 19624 , BONE_LHAND , 7 , 100 , 1} , 
	{ "Blue Backpack" , 3026 , BONE_LHAND , 7 , 0 , 5} , 
	{ "Briefcase" , 1210 , BONE_LHAND , 7 , 150, 1} , 
	
	//Watches-----------------------------------------------------------
	
	{ "Gold/White Watch" , 19039 , BONE_LFOREARM , 8 , 0, 1} ,
	{ "Silver/Black Watch" , 19040 , BONE_LFOREARM , 8 , 0, 1} ,
	{ "Bronze/Black Watch" , 19041 , BONE_LFOREARM , 8 , 1, 1} ,
	{ "Gold/Black Watch" , 19032 , BONE_LFOREARM , 8 , 0, 1} ,
	{ "Silver/White Watch" , 19043 , BONE_LFOREARM , 8 , 0, 1} ,
	{ "Pink/Black Watch" , 19044 , BONE_LFOREARM , 8 , 1500, 1} ,
	{ "Red/Black Watch" , 19045 , BONE_LFOREARM , 8 , 1500, 1} ,
	{ "Green/Black Watch" , 19046 , BONE_LFOREARM , 8 , 1250 , 1} ,
	{ "Blue/Black Watch" , 19049 , BONE_LFOREARM , 8 , 1250, 1} ,
	
	//Misc--------------------------------------------------------------
/*
	{ "Parrot 1" , 19098 , BONE_LCLAVICLE , 9 , 25, 1} ,
	{ "Parrot 2" , 19099 , BONE_LCLAVICLE , 13 , 25, 1} ,
*/
	{ "Electric Guitar" , 19319 , BONE_LHAND , 13 , 0, 2} ,
	{ "V Guitar" , 19318 , BONE_LHAND , 9 , 0, 2} ,
	{ "Death Metal Guitar" , 19319 , BONE_LHAND , 9 , 0, 2} ,
	{ "Cane" , 19348 , BONE_RHAND , 9 , 5500 , 1 } ,
	{ "Pager" , 18895 , BONE_LHAND , 9 , 25 , 1} ,
	{ "Cigarette" , 19625 , BONE_LHAND , 9 , 5 , 1} ,
	{ "Camera" , 369 , BONE_LHAND , 9 , 15 , 1} ,
	{ "Camera with Flash" , 19623 , BONE_LHAND , 13 , 20 , 1} ,
	{ "Microphone" , 19610 , BONE_LHAND , 9 , 15 , 1} ,
	{ "White Headphones" , 19421 , BONE_HEAD , 9 , 0 , 50 } ,
	{ "Black Headphones" , 19422 , BONE_HEAD , 9 , 0 , 50 } ,
	{ "Pink Headphones" , 19423 , BONE_HEAD , 9 , 0 , 50 } ,
	{ "Blue Headphones" , 19424 , BONE_HEAD , 9 , 0 , 50 }
	
};




enum ESkinMenuState {
	ESSMS_CurPage,
	ESSMS_RemainingCount,
	ESSMS_PageCount,
	ESSMS_InPreviewMode,
};
new SkinMenuState[MAX_PLAYERS][ESkinMenuState];

enum EAccessoriesMenuState {
	EASMS_CurPage,
	EASMS_RemainingCount,
	EASMS_PageCount,
	EASMS_InPreviewMode,
	EASMS_VIPStore,
	EASMS_MenuIndex,
};
new AccessoriesMenuState[MAX_PLAYERS][EAccessoriesMenuState];

new BincosAccessoriesPickup;
new BincosClothesPickup;
new CarAccessoriesPickup;
#pragma unused CarAccessoriesPickup
// The non-rp skins go from 33 to 230
new BadSkins[] = { 0, 33, 134, 137, 160, 162, 230, 74, 71, 211, 217, 265, 266, 267, 274, 275, 276, 277, 278, 279, 163, 164, 165, 166, 280, 281, 282, 283, 284, 285, 286, 287, 288, 99, 300,301, 302,303,304,305,306,307,308,309,310,311 };

enum ESkinRaces {
	ERace_White,
	ERace_Black,
	ERace_Hispanic,
	ERace_Asian,
	ERace_Any

};

enum ESkinFlags (<<= 1) {
	ESkinFlag_Mechanic, //good skin for messes, grease spills etc, public works
	ESkinFlag_Murderer, //a good skin for dealing with bloody crimes
	ESkinFlag_Agent,
	ESkinFlag_Hooker,
	ESkin_Old,
	ESkin_Rollerblade,
	ESkin_Gangster,
	ESkin_Biker,
	ESkin_Mafia,
	ESkin_FoodService,
	ESkin_Farmer,
	ESkin_Boxing,
	ESkin_Elvis,
	ESkin_Beach,
	ESKin_Karate,
	ESkin_Homeless,
	ESkin_PublicWorks
};

enum ESkinInfo {
	ESkinID,
	ESkinModelName[32],
	ESkinName[64],
	ESkinLocationString[64],
	ESkinGender, //0 = male, 1 = female, 2 = all
	ESkinCategory,
	ESkinRace,
	ESkinPrice,
	ESkinDPS
};



new SkinData[][ESkinInfo] = {
	{0,"cj","Carl \"CJ\" Johnson (Main Character)","Anywhere",0},
	{1,"truth","The Truth","San Fierro and Las Venturas",0},
	{2,"maccer","Maccer","Las Venturas and Los Santos",0},
	{3,"andre","Andre","Unknown",0},
	{4,"bbthin","Barry \"Big Bear\" Thorne [Thin]","Los Santos",0},
	{5,"bb","Barry \"Big Bear\" Thorne [Big]","Los Santos",0},
	{6,"emmet","Emmet","Los Santos",0},
	{7,"taxiguy","Taxi Driver/Train Driver","San Andreas",0},
	{8,"janitor","Janitor","4 Dragons Casino",0},
	{9,"bfori","Normal Ped","San Andreas",1},
	{10,"bfost","Old Woman","San Andreas",1},
	{11,"vbfycrp","Casino croupier","Las Venturas Casinos",1},
	{12,"bfyri","Rich Woman","San Andreas",1},
	{13,"bfyst","Street Girl","San Andreas",1},
	{14,"bmori","Normal Ped","San Andreas",0},
	{15,"bmost","Mr.Whittaker (RS Haul Owner)","San Andreas",0},
	{16,"bmyap","Airport Ground Worker","Airports",0},
	{17,"bmybu","Businessman","San Andreas",0},
	{18,"bmybe","Beach Visitor","Beaches of SA",0},
	{19,"bmydj","DJ","San Andreas",0},
	{20,"bmyri","Rich Guy (Madd Dogg\'s Manager)","San Andreas",0},
	{21,"bmycr","Normal Ped","San Andreas",0},
	{22,"bmyst","Normal Ped","San Andreas",0},
	{23,"wmybmx","BMXer","Los Santos",0},
	{24,"wbdyg1","Madd Dogg Bodyguard","Madd Dogg\'s Mansion",0},
	{25,"wbdyg2","Madd Dogg Bodyguard","Madd Dogg\'s Mansion",0},
	{26,"wmybp","Backpacker","San Andreas",0},
	{27,"wmycon","Construction Worker","Doherty",0},
	{28,"bmydrug","Drug Dealer","Los Santos",0},
	{29,"wmydrug","Drug Dealer","San Fierro",0},
	{30,"hmydrug","Drug Dealer","Las Venturas",0},
	{31,"dwfolc","Farm-Town inhabitant","San Andreas",1},
	{32,"dwmolc1","Farm-Town inhabitant","San Andreas",0},
	{33,"dwmolc2","Farm-Town inhabitant","San Andreas",0},
	{34,"dwmylc1","Farm-Town inhabitant","San Andreas",0},
	{35,"hmogar","Gardener","San Andreas",0},
	{36,"wmygol1","Golfer","San Andreas",0},
	{37,"wmygol2","Golfer","San Andreas",0},
	{38,"hfori","Normal Ped","San Andreas",0},
	{39,"hfost","Normal Ped","San Andreas",1},
	{40,"hfyri","Normal Ped","San Andreas",1},
	{41,"hfyst","Normal Ped","San Andreas",1},
	{42,"jethro","Jethro","San Fierro",0},
	{43,"hmori","Normal Ped","San Andreas",0},
	{44,"hmost","Normal Ped","San Andreas",0},
	{45,"hmybe","Beach Visitor","Beaches of SA",0},
	{46,"hmyri","Normal Ped","San Andreas",0},
	{47,"hmycr","Normal Ped","San Andreas",0},
	{48,"hmyst","Normal Ped","San Andreas",0},
	{49,"omokung","Snakehead (Da Nang)","San Andreas",0},
	{50,"wmymech","Mechanic","San Andreas",0},
	{51,"bmymoun","Mountain Biker","San Andreas",0},
	{52,"wmymoun","Mountain Biker","San Andreas",0},
	{53,"Unknown","Unknown","San Andreas",1},
	{54,"ofost","Normal Ped","San Andreas",1},
	{55,"ofyri","Normal Ped","San Andreas",1},
	{56,"ofyst","Normal Ped","San Andreas",1},
	{57,"omori","Oriental Ped","San Andreas",0},
	{58,"omost","Oriental Ped","San Andreas",0},
	{59,"omyri","Normal Ped","San Andreas",0},
	{60,"omyst","Normal Ped","San Andreas",0},
	{61,"wmyplt","Pilot","San Andreas",0},
	{62,"wmopj","Colonel Fuhrberger","San Andreas",0},
	{63,"bfypro","Prostitute","San Andreas",1},
	{64,"hfypro","Prostitute","San Andreas",1},
	{65,"kendl","Kendl Johnson","Los Santos and San Fierro",1},
	{66,"bmypol1","Pool Player","Bars",0},
	{67,"bmypol2","Pool Player","Bars, Works at Dillimore Gas Station",0},
	{68,"wmoprea","Priest/Preacher","Las Venturas",0},
	{69,"sbfyst","Normal Ped","San Andreas",1},
	{70,"wmosci","Scientist","Area 69",0},
	{71,"wmysgrd","Security Guard","Airport Guard Boxes",0},
	{72,"swmyhp1","Hippy","San Andreas",0},
	{73,"swmyhp2","Hippy","San Fierro",0},
	{75,"swfopro","Prostitute","San Andreas",1},
	{76,"wfystew","Stewardess","San Andreas",1},
	{77,"swmotr1","Homeless","San Andreas",1},
	{78,"wmotr1","Homeless","San Andreas",0},
	{79,"bmotr1","Homeless","San Andreas",0},
	{80,"vbmybox","Boxer","San Andreas Gyms",0},
	{81,"vwmybox","Boxer","San Andreas Gyms",0},
	{82,"vhmyelv","Black Elvis","Las Venturas",0},
	{83,"vbmyelv","White Elvis","Las Venturas",0},
	{84,"vimyelv","Blue Elvis","Las Venturas",0},
	{85,"vwfypro","Prostitute","San Andreas",1},
	{86,"ryder3","Ryder with robbery mask","Los Santos",0},
	{87,"vwfyst1","Stripper","San Andreas",1},
	{88,"wfori","Normal Ped","San Andreas",1},
	{89,"wfost","Normal Ped","San Andreas",1},
	{90,"wfyjg","Jogger","San Andreas",1},
	{91,"wfyri","Rich Woman","San Andreas",1},
	{92,"wfyro","Rollerskater","Beaches of SA",1},
	{93,"wfyst","Normal Ped","San Andreas",1},
	{94,"wmori","Normal Ped","San Andreas",0},
	{95,"wmost","Normal Ped, Works at or owns Dillimore Gas Station","San Andreas",0},
	{96,"wmyjg","Jogger","San Andreas",0},
	{97,"wmylg","Lifeguard","Beaches",0},
	{98,"wmyri","Normal Ped","San Andreas",0},
	{99,"wmyro","Rollerskater","San Andreas",0},
	{100,"wmycr","Biker","San Andreas",0},
	{101,"wmyst","Normal Ped","San Andreas",0},
	{102,"ballas1","Balla","Los Santos",0},
	{103,"ballas2","Balla","Los Santos",0},
	{104,"ballas3","Balla","Los Santos",0},
	{105,"fam1","Grove Street Families","Los Santos",0},
	{106,"fam2","Grove Street Families","Los Santos",0},
	{107,"fam3","Grove Street Families","Los Santos",0},
	{108,"lsv1","Los Santos Vagos","Los Santos",0},
	{109,"lsv2","Los Santos Vagos","Los Santos",0},
	{110,"lsv3","Los Santos Vagos","Los Santos",0},
	{111,"maffa","The Russian Mafia","Around SA",0},
	{112,"maffb","The Russian Mafia","Around SA",0},
	{113,"mafboss","The Russian Mafia","Around SA",0},
	{114,"vla1","Varios Los Aztecas","Los Santos",0},
	{115,"vla2","Varios Los Aztecas","Los Santos",0},
	{116,"vla3","Varios Los Aztecas","Los Santos",0},
	{117,"triada","Triad","San Fierro",0},
	{118,"triadb","Triad","San Fierro",0},
	{119,"sindaco","Johhny Sindacco","Las Venturas",0},
	{120,"triboss","Triad Boss","San Fierro",0},
	{121,"dnb1","Da Nang Boy","San Fierro",0},
	{122,"dnb2","Da Nang Boy","San Fierro",0},
	{123,"dnb3","Da Nang Boy","San Fierro",0},
	{124,"vmaff1","The Mafia","Las Venturas",0},
	{125,"vmaff2","The Mafia","Las Venturas",0},
	{126,"vmaff3","The Mafia","Las Venturas",0},
	{127,"vmaff4","The Mafia","Las Venturas",0},
	{128,"dnmylc","Farm Inhabitant","San Andreas",0},
	{129,"dnfolc1","Farm Inhabitant","San Andreas",1},
	{130,"dnfolc2","Farm Inhabitant","San Andreas",1},
	{131,"dnfylc","Farm Inhabitant","San Andreas",1},
	{132,"dnmolc1","Farm Inhabitant","San Andreas",0},
	{133,"dnmolc2","Farm Inhabitant","San Andreas",0},
	{134,"sbmotr2","Homeless","San Andreas",0},
	{135,"swmotr2","Homeless","San Andreas",0},
	{136,"sbmytr3","Normal Ped","San Andreas",0},
	{137,"swmotr3","Homeless","San Andreas",0},
	{138,"wfybe","Beach Visitor","Beaches of SA",1},
	{139,"bfybe","Beach Visitor","Beaches of SA",1},
	{140,"hfybe","Beach Visitor","Beaches of SA",1},
	{141,"sofybu","Businesswoman","San Andreas",1},
	{142,"sbmyst","Taxi Driver","San Fierro",0},
	{143,"sbmycr","Crack Maker","San Andreas",0},
	{144,"bmycg","Crack Maker","San Andreas",0},
	{145,"wfycrk","Crack Maker","Beaches of SA",1},
	{146,"hmycm","Crack Maker","Beaches of SA",0},
	{147,"wmybu","Businessman","San Andreas",0},
	{148,"bfybu","Businesswoman","San Andreas",1},
	{149,"smokev","Big Smoke Armored","Los Santos",0},
	{150,"wfybu","Businesswoman","San Andreas",1},
	{151,"dwfylc1","Normal Ped","San Andreas",1},
	{152,"wfypro","Prostitute","San Andreas",1},
	{153,"wmyconb","Construction Worker","San Andreas",0},
	{154,"wmybe","Beach Visitor","Beaches of SA",0},
	{155,"wmypizz","Well Stacked Pizza Worker","Well Stacked Pizza co.",0},
	{156,"bmobar","Barber","San Andreas",0},
	{157,"cwfyhb","Hillbilly","San Andreas",1},
	{158,"cwmofr","Farmer","San Andreas",0},
	{159,"cwmohb1","Hillbilly","San Andreas",0},
	{160,"cwmohb2","Hillbilly","San Andreas",0},
	{161,"cwmyfr","Farmer","San Andreas",0},
	{162,"cwmyhb1","Hillbilly","San Andreas",0},
	{163,"bmyboun","Black Bouncer","San Andreas",0},
	{164,"wmyboun","White Bouncer","San Andreas",0},
	{165,"wmomib","White MIB agent","San Andreas",0},
	{166,"bmymib","Black MIB agent","San Andreas",0},
	{167,"wmybell","Cluckin\' Bell Worker","Cluckin\' Bell",0},
	{168,"bmochil","Hotdog/Chilli Dog Vendor","Hotdog vans and chilli dog carts",0},
	{169,"sofyri","Normal Ped","San Andreas",1},
	{170,"somyst","Normal Ped","San Andreas",0},
	{171,"vwmybjd","Blackjack Dealer","Casinos",0},
	{172,"vwfycrp","Casino croupier","Casinos",1},
	{173,"sfr1","San Fierro Rifa","San Fierro",0},
	{174,"sfr2","San Fierro Rifa","San Fierro",0},
	{175,"sfr3","San Fierro Rifa","San Fierro",0},
	{176,"bmybar","Barber","San Andreas",0},
	{177,"wmybar","Barber","San Andreas",0},
	{178,"wfysex","Whore","San Andreas",1},
	{179,"wmyammo","Ammunation Salesman","Ammunation",0},
	{180,"bmytatt","Tattoo Artist","Tattoo Shops",0},
	{181,"vwmycr","Punk","San Andreas",0},
	{182,"vbmocd","Cab Driver","San Andreas",0},
	{183,"vbmycr","Normal Ped","San Andreas",0},
	{184,"vhmycr","Normal Ped","San Andreas",0},
	{185,"sbmyri","Normal Ped","San Andreas",0},
	{186,"somyri","Normal Ped","San Andreas",0},
	{187,"somybu","Businessman","San Andreas",0},
	{188,"swmyst","Normal Ped","San Andreas",0},
	{189,"wmyva","Valet","San Fierro",0},
	{190,"copgrl3","Barbara Schternvart","El Quebrados",1},
	{191,"gungrl3","Helena Wankstein","Blueberry",1},
	{192,"mecgrl3","Michelle Cannes","San Fierro",1},
	{193,"nurgrl3","Katie Zhan","San Fierro",1},
	{194,"crogrl3","Millie Perkins","Las Venturas",1},
	{195,"gangrl3","Denise Robinson","Los Santos",1},
	{196,"cwfofr","Farm-Town inhabitant","San Andreas",1},
	{197,"cwfohb","Hillbilly","San Andreas",1},
	{198,"cwfyfr1","Farm-Town inhabitant","San Andreas",1},
	{199,"cwfyfr2","Farm-Town inhabitant","San Andreas",1},
	{200,"cwmyhb2","Hillbilly","San Andreas",0},
	{201,"dwfylc2","Farmer","San Andreas",1},
	{202,"dwmylc2","Farmer","San Andreas",0},
	{203,"omykara","Karate Teacher","San Fierro Gym",0},
	{204,"wmykara","Karate Teacher","San Fierro Gym",0},
	{205,"wfyburg","Burger Shot Cashier","Burger Shot",1},
	{206,"vwmycd","Cab Driver","San Andreas",0},
	{207,"vhfypro","Prostitute","San Andreas",1},
	{208,"suzie","Su Xi Mu (Suzie)","San Fierro",0},
	{209,"omonood","Oriental Noodle stand vendor","Noodle stands",0},
	{210,"omoboat","Oriental Boating School Instructor","Boating School",0},
	{211,"wfyclot","Clothes shop staff","Clothes shops",1},
	{212,"vwmotr1","Homeless","San Andreas",0},
	{213,"vwmotr2","Weird old man","San Andreas",0},
	{214,"vwfywai","Waitress (Maria Latore)","Caligulas Casino (cutscene only)",1},
	{215,"sbfori","Normal Ped","San Andreas",1},
	{216,"swfyri","Normal Ped","San Andreas",1},
	{217,"wmyclot","Clothes shop staff","Clothes shops",0},
	{218,"sbfost","Normal Ped","San Andreas",1},
	{219,"sbfyri","Rich Woman","San Andreas",1},
	{220,"sbmocd","Cab Driver","San Fierro",0},
	{221,"sbmori","Normal Ped","San Andreas",0},
	{222,"sbmost","Normal Ped","San Andreas",0},
	{223,"shmycr","Normal Ped","San Andreas",0},
	{224,"sofori","Normal Ped","San Andreas",1},
	{225,"sofost","Normal Ped","San Andreas",1},
	{226,"sofyst","Normal Ped","San Andreas",1},
	{227,"somobu","Oriental Businessman","San Andreas",0},
	{228,"somori","Oriental Ped","San Andreas",0},
	{229,"somost","Oriental Ped","San Andreas",0},
	{230,"swmotr5","Homeless","San Andreas",0},
	{231,"swfori","Normal Ped","San Andreas",1},
	{232,"swfost","Normal Ped","San Andreas",1},
	{233,"swfyst","Normal Ped","San Andreas",1},
	{234,"swmocd","Cab Driver","San Andreas",0},
	{235,"swmori","Normal Ped","San Andreas",0},
	{236,"swmost","Normal Ped","San Andreas",0},
	{237,"shfypro","Prostitute","San Andreas",1},
	{238,"sbfypro","Prostitute","San Andreas",1},
	{239,"swmotr4","Homeless","San Andreas",0},
	{240,"swmyri","The D.A","San Fierro",0},
	{241,"smyst","Afro-American","San Andreas",0},
	{242,"smyst2","Mexican","San Andreas",0},
	{243,"sfypro","Prostitute","San Andreas",1},
	{244,"vbfyst2","Stripper","San Andreas",1},
	{245,"vbfypro","Prostitute","San Andreas",1},
	{246,"vhfyst3","Stripper","San Andreas",1},
	{247,"bikera","Biker","San Andreas",0},
	{248,"bikerb","Biker","San Andreas",0},
	{249,"bmypimp","Pimp","San Fierro",0},
	{250,"swmycr","Normal Ped","San Andreas",0},
	{251,"wfylg","Lifeguard","Beaches",1},
	{252,"wmyva2","Naked Valet","San Fierro (mission only)",0},
	{253,"bmosec","Bus Driver","San Fierro",0},
	{254,"bikdrug","Biker Drug Dealer","San Andreas",0},
	{255,"wmych","Chauffeur (Limo Driver)","San Andreas",0},
	{256,"sbfystr","Stripper","San Andreas",1},
	{257,"swfystr","Stripper","San Andreas",1},
	{258,"heck1","Heckler","San Andreas",0},
	{259,"heck2","Heckler","San Andreas",0},
	{260,"bmycon","Construction Worker","San Fierro",0},
	{261,"wmycd1","Cab driver","San Andreas",0},
	{262,"bmocd","Cab driver","San Andreas",0},
	{263,"vwfywa2","Normal Ped","San Andreas",1},
	{264,"wmoice","Clown (Ice-cream Van Driver)","San Andreas (Driving ice-cream vans (Mr.Whoopee))",0},
	{265,"tenpen","Officer Frank Tenpenny (Corrupt Cop)","Missions",0},
	{266,"pulaski","Officer Eddie Pulaski (Corrupt Cop)","Missions",0},
	{267,"Hernandez","Officer Jimmy Hernandez","Missions",0},
	{268,"dwayne","Dwaine/Dwayne","San Fierro (Missions)",0},
	{269,"smoke","Melvin \"Big Smoke\" Harris (Mission)","Los Santos",0},
	{270,"sweet","Sean \'Sweet\' Johnson","Los Santos/San Fierro (Missions)",0},
	{271,"ryder","Lance \'Ryder\' Wilson","Los Santos/San Fierro (Missions)",0},
	{272,"forelli","Mafia Boss","Los Santos",0},
	{273,"tbone","T-Bone Mendez","San Fierro",0},
	{274,"laemt1","Paramedic (Emergency Medical Technician)","Los Santos",0},
	{275,"lvemt1","Paramedic (Emergency Medical Technician)","Las Venturas",0},
	{276,"sfemt1","Paramedic (Emergency Medical Technician)","San Fierro",0},
	{277,"lafd1","Firefighter","Los Santos",0},
	{278,"lvfd1","Firefighter","Las Venturas",0},
	{279,"sffd1","Firefighter","San Fierro",0},
	{280,"lapd1","Los Santos Police Officer","Los Santos",0},
	{281,"sfpd1","San Fierro Police Officer","San Fierro",0},
	{282,"lvpd1","Las Venturas Police Officer","Las Venturas",0},
	{283,"csher","County Sheriff","Countryside",0},
	{284,"lapdm1","LSPD Motorbike Cop","San Andreas",0},
	{285,"swat","S.W.A.T Special Forces","San Andreas",0},
	{286,"fbi","Federal Agent","San Andreas",0},
	{287,"army","San Andreas Army","San Andreas",0},
	{288,"dsher","Desert Sheriff","Desert",0},
	{289,"zero","Zero","San Fierro",0},
	{290,"rose","Ken Rosenberg","Las-Venturas Casino",0},
	{291,"paul","Kent Paul","LV/LS",0},
	{292,"cesar","Cesar Vialpando","LS/SF",0},
	{293,"ogloc","Jeffery \"OG Loc\" Martin/Cross","Los-Santos, Burger Shot",0},
	{294,"wuzimu","Wu Zi Mu (Woozie)","San Fierro and Las Venturas",0},
	{295,"torino","Michael Toreno","Las Venturas and San Fierro",0},
	{296,"jizzy","Jizzy B.","San Fierro, The Pleasure Domes",0},
	{297,"maddogg","Madd Dogg","Madd Dogg\'s Mansion, Los Santos",0},
	{298,"cat","Catalina","Fern Ridge",1},
	{299,"claude","Claude Speed","Cutscene Only",0},
	{300,"lapdna","Los Santos Police Officer (Without gun holster)","-",0},
	{301,"sfpdna","San Fierro Police Officer (Without gun holster)","-",0},
	{302,"lvpdna","Las Venturas Police Officer (Without gun holster)","-",0},
	{303,"lapdpc","Los Santos Police Officer (Without uniform)","-",0},
	{304,"lapdpd","Los Santos Police Officer (Without uniform)","-",0},
	{305,"lvpdpc","Las Venturas Police Officer (Without uniform)","-",0},
	{306,"WFYCLPD","Los Santos Police Officer","-",1},
	{307,"VBFYCPD","San Fierro Police Officer","-",1},
	{308,"WFYCLEM","San Fierro Paramedic (Emergency Medical Technician)","-",1},
	{309,"WFYCLLV","Las Venturas Police Officer","-",1},
	{310,"csherna","Country Sheriff (Without hat)","-",0},
	{311,"dsherna","Desert Sheriff (Without hat)","-",0}
};

getSkinArrayIndex(skinid) {
	for(new i=0;i<sizeof(SkinData);i++) {
		if(SkinData[i][ESkinID] == skinid) {
			return i;
		}
	}
	return -1;
}

//Some skins defined on utils.pwn just search "skin"

forward onAccessoryBuy(playerid, index);
forward onAccessoriesLoad(playerid);
forward accessoriesOnPlayerDisconnect(playerid, reason);

accessoriesOnGameModeInit() {
	BincosAccessoriesPickup = CreateDynamicPickup(1239, 16, 205.01, -101.37, 1005.26); //bincos accessories pickup
	CreateDynamic3DTextLabel("(( /accessories ))", 0x2BB00AA, 205.01, -101.37, 1005.26+1, 50.0);//Bincos accessories label
	
	BincosClothesPickup = CreateDynamicPickup(1239, 16, 210.047988, -102.005409, 1005.257812);
	CreateDynamic3DTextLabel("(( /skin ))", 0x2BB00AA, 210.047988, -102.005409, 1005.257812+1, 50.0);//Bincos accessories label
	
	//CarAccessoriesPickup = CreateDynamicPickup(1239, 16, 1264.86, -1669.75, 13.55); //VIP car modding
	//CreateDynamic3DTextLabel("(( /buyaccessory ))", 0x2BB00AA, 1264.86, -1669.75, 13.55+1, 50.0);//car modding
}
accessoriesOnPlayerPickupPickup(playerid, pickup) {
	if(pickup == BincosAccessoriesPickup) {
		showAccessoriesDialog(playerid);
		movePlayerBack(playerid, 3.0);
	} else if(pickup == BincosClothesPickup) {
		SendClientMessage(playerid, COLOR_DARKGREEN, "(( Use /skin to change your skin ))");
		movePlayerBack(playerid, 3.0);
	}
}


setupSkinMenuState(playerid, curpage = 1) {
	new remaining_amount, pages, numskins;
	new start_index = 0;
	new temptxt[128];

	new vehstr[32],vehprice[32];

	for(new i=0,c=0,j=1;i<sizeof(SkinData);i++) {
		if(IsSkinAllowed(playerid, SkinData[i][ESkinID])) {
			if(c++ >= (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS)*j) {
				if(++j == curpage)
					start_index = i;
			}
			numskins++;
		}
	}

	clearModelSelectMenu(playerid);
	if(curpage < 1) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	for(new i=start_index,x=0;x<numskins&&i<sizeof(SkinData);i++) {
		if(IsSkinAllowed(playerid, SkinData[i][ESkinID])) {
			format(vehstr,sizeof(vehstr),"%s",SkinData[i][ESkinName]);
			format(vehprice,sizeof(vehprice), "~g~ID: %d",SkinData[i][ESkinID]);
			addItemToModelSelectMenu(playerid, SkinData[i][ESkinID], vehprice,vehstr);
			x++;
		}
	}

	remaining_amount = numskins % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numskins) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	SkinMenuState[playerid][ESSMS_CurPage] = curpage;
	SkinMenuState[playerid][ESSMS_PageCount] = pages;
	SkinMenuState[playerid][ESSMS_RemainingCount] = remaining_amount;
	SkinMenuState[playerid][ESSMS_InPreviewMode] = 0;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numskins, "Skin Menu", page_str, "OnSkinMenuSelect", 0.0, 0.0, 0.0);
}
forward OnSkinMenuSelect(playerid, action, modelid, title[]);
public OnSkinMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 3);
			SkinMenuState[playerid][ESSMS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		new skinidx = getSkinArrayIndex(SkinMenuState[playerid][ESSMS_InPreviewMode]);
		if(skinidx != -1) {
			new msg[128];
			format(msg, sizeof(msg), "* Skin set to %d - %s",SkinMenuState[playerid][ESSMS_InPreviewMode],SkinData[skinidx][ESkinName]);
			SendClientMessage(playerid, COLOR_LIGHTGREEN, msg);
			setCharacterSkin(playerid, SkinMenuState[playerid][ESSMS_InPreviewMode]);
			destroyModelPreviewTDs(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	} else if(action == EModelPreviewAction_Next) {
		setupSkinMenuState(playerid, SkinMenuState[playerid][ESSMS_CurPage] + 1);
	} else if(action == EModelPreviewAction_Back) {
		setupSkinMenuState(playerid, SkinMenuState[playerid][ESSMS_CurPage] - 1);
	} else if(action == EModelPreviewAction_Exit) {
		if(SkinMenuState[playerid][ESSMS_InPreviewMode] != 0) {
			setupSkinMenuState(playerid, SkinMenuState[playerid][ESSMS_CurPage]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
}
YCMD:skin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for changing a players skin");
		return 1;
	}
	new skinid;
	if(!IsPlayerInRangeOfPoint(playerid, 15.0, 210.047988, -102.005409, 1005.257812)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at Bincos!");
		return 1;
	}
	setupSkinMenuState(playerid);
	return 1;
}
IsValidSkin(skin) {
	return getSkinArrayIndex(skin) != -1;
}
IsSkinAllowed(playerid, skinid) {
	#pragma unused playerid
	if(!IsValidSkin(skinid)) {
		return 0;
	}
	new sex = GetPVarInt(playerid,"Sex");
	new skinidx = getSkinArrayIndex(skinid);
	if(skinidx != -1) {
		if(SkinData[skinidx][ESkinGender] != sex && sex != 2)
			return 0;
	} else {
		return 0;
	}
	for(new i=0;i<sizeof(BadSkins);i++) {
		if(BadSkins[i] == skinid) {
			return 0;
		}
	}
	return 1;
}
YCMD:buyaccessories(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying accessories");
		return 1;
	}
	showAccessoriesDialog(playerid);
	return 1;
}
YCMD:accessories(playerid, params[], help) {
	if(help) {
		return 1;
	}
	showAccessoriesModifyDialog(playerid);
	return 1;
}
showAccessoriesDialog(playerid, VIPMode = 0) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	if(VIPMode) {
		SetPVarInt(playerid, "VIPAccessoryBuy", 1);
	}
	for(new i=0;i<sizeof(AccessoryCategories);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",AccessoryCategories[i]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccessories_MainMenu, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Buy", "Cancel");
	return 1;
}
accessoriesOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	if(!response) {
		return 1;
	}
	switch(dialogid) {
		case EAccessories_MainMenu: {
			if(GetPVarInt(playerid, "VIPAccessoryBuy") == 1) {
				showVIPAccessoriesBuyMenu(playerid, listitem);
			} else {
				showAccessoryBuyMenu(playerid, listitem);
			}
		}
		case EAccessories_EditAccessory: {
			switch(listitem) {
				case 0: { //set slot
					showSetAccessorySlot(playerid);
				}
				case 1: {
					showEnableDisableSlot(playerid);
				}
				case 2: {
					if(GetPVarType(playerid, "ManualEdit") != PLAYER_VARTYPE_NONE) {
						DeletePVar(playerid, "ManualEdit");
					}
					showEditAccessoryMenu(playerid);
				}
				case 3: {
					showDeleteAccessoryMenu(playerid);
				}
				case 4: {
					showSetAccessoryBone(playerid);
				}
				case 5: {
					SetPVarInt(playerid, "ManualEdit", 1);
					showEditAccessoryMenu(playerid);
				}
			}
		}
		case EAccessories_SetSlot: {
			showEditSlotMenu(playerid, listitem);
		}
		case EAccessories_EditSlot: {
			new object = getPlayerListedAccessory(playerid, listitem);
			new slot = GetPVarInt(playerid, "AccessMenu");
			setItemSlot(playerid, object, slot);
		}
		case EAccessories_ToggleSlot: {
			toggleAccessorySlot(playerid, listitem);
		}
		case EAccessories_SelectEditItem: {
			if(GetPVarInt(playerid, "ManualEdit") == 1) {
				new accessory = getPlayerListedAccessory(playerid, listitem);
				if(accessory != -1) {
					showEditItemsMenu(playerid, accessory);
				} else {
					//format(msg, sizeof(msg), "Tell CHC This: %d %d %d",playerid,accessory, listitem);
					SendClientMessage(playerid, X11_TOMATO_2, "Please close the accessories menu and re-open it");
					//ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
				}
			} else {
				EditAttachedObject(playerid, listitem);
			}
		}
		case EAccessories_ModifyItem: {
			handleModifyItem(playerid, listitem);
		}
		case EAccessories_SetBone: {
			setItemBone(playerid, GetPVarInt(playerid, "AccessoryItemEdit"), listitem+1);
		}
		case EAccessories_SetValue: {
			handleEditItem(playerid, GetPVarInt(playerid, "AccessoryItemEdit"), GetPVarInt(playerid, "AccessoryEditType"), floatstr(inputtext));
			showEditItemsMenu(playerid,GetPVarInt(playerid, "AccessoryItemEdit"));
		}
		case EAccessories_DeleteAccessory: {
			handleDeleteAccessory(playerid, getPlayerListedAccessory(playerid, listitem));
			showDeleteAccessoryMenu(playerid);
		}
		case EAccessories_ItemBoneChange: {
			SetPVarInt(playerid, "AccessoryItemEdit", getAccessoryAtSlot(playerid, listitem));
			ShowPlayerDialog(playerid, EAccessories_SetBone, DIALOG_STYLE_LIST, "Edit Bone","Spine\nHead\nLeft Arm\nRight Arm\nLeft Hand\nRight Hand\nLeft Thigh\nRight Thigh\nLeft Foot\nRight Foot\nLeft Calf\nRight Calf\nLeft Forearm\nRight Forearm\nLeft Clavicle\nRight Clavicle\nNeck\nJaw","Select", "Cancel");
		}
	}
	return 1;
}
handleDeleteAccessory(playerid, index) {
	new pvarname[64];
	if(playerHasAccessory(playerid, index)) {
		new slot = getAccessorySlot(playerid, index);
		if(slot != -1) {
			RemovePlayerAttachedObject(playerid, slot);
		}
		format(pvarname, sizeof(pvarname), "hasAccessory%d", index);
		DeletePVar(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",index);
		query[0] = 0;//[128];
		format(query, sizeof(query), "DELETE FROM `useraccessories` WHERE `id` = %d",GetPVarInt(playerid, pvarname));
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	}
	return 1;
}
showDeleteAccessoryMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(Accessories);i++) {
		if(playerHasAccessory(playerid, i)) {
			format(tempstr, sizeof(tempstr), "%s\n",Accessories[i][EAccessoryName]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EAccessories_DeleteAccessory, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Delete", "Cancel");
}

setupAccessoriesMenuState(playerid, curpage, index, isVIP = 0) {
	new remaining_amount, pages, numaccessories;
	new start_index = 0;
	new temptxt[128];

	new vehstr[32],vehprice[32];

	AccessoriesMenuState[playerid][EASMS_VIPStore] = isVIP;

	for(new i=0,c=0,j=1;i<sizeof(Accessories);i++) {
		if(Accessories[i][EMenuID] == index && ((AccessoriesMenuState[playerid][EASMS_VIPStore] == 0 && Accessories[i][EAccessoryPrice] != 0) || (AccessoriesMenuState[playerid][EASMS_VIPStore] == 1 && Accessories[i][EAccessoryDPPrice] != 0))) {
			if(c++ >= (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS)*j) {
				if(++j == curpage)
					start_index = i;
			}
			numaccessories++;
		}
	}

	clearModelSelectMenu(playerid);
	if(curpage < 0) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}

	for(new i=start_index,x=0;x<numaccessories&&i<sizeof(Accessories);i++) {
		if(Accessories[i][EMenuID] == index && ((AccessoriesMenuState[playerid][EASMS_VIPStore] == 0 && Accessories[i][EAccessoryPrice] != 0) || (AccessoriesMenuState[playerid][EASMS_VIPStore] == 1 && Accessories[i][EAccessoryDPPrice] != 0))) {
			format(vehstr,sizeof(vehstr),"%s",Accessories[i][EAccessoryName]);
			if(AccessoriesMenuState[playerid][EASMS_VIPStore] == 0) {
				format(vehprice,sizeof(vehprice), "~g~$%s",getNumberString(Accessories[i][EAccessoryPrice]));
			} else {
				format(vehprice,sizeof(vehprice), "~g~%s DPs",getNumberString(Accessories[i][EAccessoryDPPrice]));		
			}
			
			addItemToModelSelectMenu(playerid, Accessories[i][EAccessoryModelID], vehprice,vehstr);
			x++;
		}
	}

	remaining_amount = numaccessories % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numaccessories) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	AccessoriesMenuState[playerid][EASMS_CurPage] = curpage;
	AccessoriesMenuState[playerid][EASMS_PageCount] = pages;
	AccessoriesMenuState[playerid][EASMS_RemainingCount] = remaining_amount;
	AccessoriesMenuState[playerid][EASMS_InPreviewMode] = 0;
	AccessoriesMenuState[playerid][EASMS_MenuIndex] = index;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numaccessories, "Accessories Menu", page_str, "OnAccessoriesMenuSelect", 0.0, 0.0, 0.0);
}
forward OnAccessoriesMenuSelect(playerid, action, modelid, title[]);
public OnAccessoriesMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			AccessoriesMenuState[playerid][EASMS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		new option = getMenuOption(GetPVarInt(playerid, "AccessMenu"), AccessoriesMenuState[playerid][EASMS_InPreviewMode], AccessoriesMenuState[playerid][EASMS_VIPStore]);
		if(option != -1) {
			buyAccessory(playerid, option, AccessoriesMenuState[playerid][EASMS_VIPStore]);
		}
	} else if(action == EModelPreviewAction_Next) {
		setupAccessoriesMenuState(playerid, AccessoriesMenuState[playerid][EASMS_CurPage] + 1, AccessoriesMenuState[playerid][EASMS_MenuIndex], AccessoriesMenuState[playerid][EASMS_VIPStore]);
	} else if(action == EModelPreviewAction_Back) {
		setupAccessoriesMenuState(playerid, AccessoriesMenuState[playerid][EASMS_CurPage] - 1, AccessoriesMenuState[playerid][EASMS_MenuIndex], AccessoriesMenuState[playerid][EASMS_VIPStore]);
	} else if(action == EModelPreviewAction_Exit) {
		if(AccessoriesMenuState[playerid][EASMS_InPreviewMode] != 0) {
			setupAccessoriesMenuState(playerid, AccessoriesMenuState[playerid][EASMS_CurPage], AccessoriesMenuState[playerid][EASMS_MenuIndex], AccessoriesMenuState[playerid][EASMS_VIPStore]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
}
showAccessoryBuyMenu(playerid, index) {
	SetPVarInt(playerid, "AccessMenu", index);
	setupAccessoriesMenuState(playerid, 1, index, 0);
	return 1;
}
showVIPAccessoriesBuyMenu(playerid, index) {
	SetPVarInt(playerid, "AccessMenu", index);
	setupAccessoriesMenuState(playerid, 1, index, 1);
	return 1;
}
showAccessoriesModifyDialog(playerid) {
	ShowPlayerDialog(playerid, EAccessories_EditAccessory, DIALOG_STYLE_LIST, "Accessories Menu","Set Slots\nEnable\\Disable Slot\nEdit Item\nDelete Item\nSet Item Bone\nManual Edit","Select", "Cancel");
	return 1;
}
showSetAccessorySlot(playerid) {
	new pvarname[64];
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Accessory%d", i);
		new index = GetPVarInt(playerid, pvarname);
		new itemname[32];
		if(index == -1 || GetPVarType(playerid, pvarname) == PLAYER_VARTYPE_NONE) {
			format(itemname,sizeof(itemname), "None");
		} else {
			format(itemname, sizeof(itemname), "%s",Accessories[index][EAccessoryName]);
		}
		format(tempstr, sizeof(tempstr), "Slot %d (%s)\n",i,itemname);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccessories_SetSlot, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Select", "Cancel");
	return 1;
}
showEditSlotMenu(playerid, index) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	SetPVarInt(playerid, "AccessMenu", index);
	for(new i=0;i<sizeof(Accessories);i++) {
		if(playerHasAccessory(playerid, i)) {
			format(tempstr, sizeof(tempstr), "%s\n",Accessories[i][EAccessoryName]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EAccessories_EditSlot, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Select", "Cancel");
	return 1;
}
playerHasAccessory(playerid, index) {
	new pvarname[64];
	format(pvarname, sizeof(pvarname), "hasAccessory%d", index);
	return GetPVarType(playerid, pvarname) != PLAYER_VARTYPE_NONE;
}
//for use in the slot menu only right now
getPlayerListedAccessory(playerid, index) {
	new x;
	for(new i=0;i<sizeof(Accessories);i++) {
		if(playerHasAccessory(playerid, i)) {
			if(x++ == index) {
				return i;
			}
		}
	}
	return -1;
}
getMenuOption(menu, modelid, vipshop = 0) {
	for(new i=0;i<sizeof(Accessories);i++) {
		if(Accessories[i][EMenuID] == menu) {
			if(Accessories[i][EAccessoryPrice] != 0 || (vipshop == 1 && Accessories[i][EAccessoryDPPrice] != 0)) {
				if(Accessories[i][EAccessoryModelID] == modelid) {
					return i;
				}
			}
		}
	}
	return -1;
}
numBuyableMenus(menu) {
	new x;
	for(new i=0;i<sizeof(Accessories);i++) {
		if(Accessories[i][EMenuID] == menu) {
			x++;
		}
	}
	return x;
}
buyAccessory(playerid, index, VIP = 0) {
	new msg[128];
	new price;
	if(VIP) {
		price = Accessories[index][EAccessoryDPPrice];
		new dps = GetPVarInt(playerid, "DonatePoints");
		DeletePVar(playerid, "VIPAccessoryBuy");
		if(dps < price) {
			format(msg, sizeof(msg), "You do not have enough donator points for this. You need %d more donator points",price-dps);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		} else {
			dps -= price;
			SetPVarInt(playerid, "DonatePoints", dps);
			format(msg, sizeof(msg), "Congratulations, You now own a %s!",Accessories[index][EAccessoryName]);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		}
	} else {
		price = Accessories[index][EAccessoryPrice];
		new money = GetMoneyEx(playerid);
		if(money < price) {
			format(msg, sizeof(msg), "You do not have enough money for this. You need $%s more dollars",getNumberString(price-money));
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		} else {
			format(msg, sizeof(msg), "Congratulations, You now own a %s!",Accessories[index][EAccessoryName]);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
			SendClientMessage(playerid, COLOR_DARKGREEN, "(( Use /accessories to use your item ))");
			GiveMoneyEx(playerid, -price);
		}
	}
	format(msg, sizeof(msg), "hasAccessory%d",index);
	SetPVarInt(playerid, msg, 1);
	format(msg, sizeof(msg), "INSERT INTO `useraccessories` (`accessoryid`,`owner`) VALUES (%d,%d)",index,GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, msg, true, "onAccessoryBuy", "dd", playerid, index);
	return 0;
}
accessoriesOnLoadCharacter(playerid) {
	query[0] = 0;//[256];
	format(query, sizeof(query), "SELECT `id`,`accessoryid`,`offsetx`,`offsety`,`offsetz`,`rotx`,`roty`,`rotz`,`scalex`,`scaley`,`scalez`,`slot`,`bone`,`enabled` FROM `useraccessories` WHERE `owner` = %d", GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "onAccessoriesLoad", "d", playerid);
}
public accessoriesOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	new pvarname[64];
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname),"Accessory%d",i);
		DeletePVar(playerid, pvarname);
	}
}

public onAccessoriesLoad(playerid) {
	new id_string[128];
	new pvarname[64];
	
	new index;
	new slot;
	new rows,fields;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
	
		cache_get_row(i, 1, id_string);
		index = strval(id_string);
	
		format(pvarname, sizeof(pvarname), "hasAccessory%d",index);
		SetPVarInt(playerid, pvarname, 1);
		cache_get_row(i, 0, id_string);
		format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",index);
		SetPVarInt(playerid, pvarname, strval(id_string));
	
		format(pvarname, sizeof(pvarname), "AccessoryX%d",index);
		cache_get_row(i, 2, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	
		format(pvarname, sizeof(pvarname), "AccessoryY%d",index);
		cache_get_row(i, 3, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	
		format(pvarname, sizeof(pvarname), "AccessoryZ%d",index);
		cache_get_row(i, 4, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
		
		format(pvarname, sizeof(pvarname), "AccessoryRotX%d",index);
		cache_get_row(i, 5, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
		
		format(pvarname, sizeof(pvarname), "AccessoryRotY%d",index);
		cache_get_row(i, 6, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
		
		format(pvarname, sizeof(pvarname), "AccessoryRotZ%d",index);
		cache_get_row(i, 7, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	
		format(pvarname, sizeof(pvarname), "AccessoryScaleX%d",index);
		cache_get_row(i, 8, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	
		format(pvarname, sizeof(pvarname), "AccessoryScaleY%d",index);
		cache_get_row(i, 9, id_string);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	
		cache_get_row(i, 10, id_string);
		format(pvarname, sizeof(pvarname), "AccessoryScaleZ%d",index);
		SetPVarFloat(playerid, pvarname, floatstr(id_string));
	

		cache_get_row(i, 12, id_string);
		format(pvarname, sizeof(pvarname), "AccessoryBone%d",index);
		SetPVarInt(playerid, pvarname, strval(id_string));
			
		cache_get_row(i, 11, id_string);
		slot = strval(id_string);
		if(slot >= 0) {
			format(pvarname, sizeof(pvarname), "Accessory%d",slot);
			SetPVarInt(playerid, pvarname, index);
			
			cache_get_row(i, 13, id_string);
			new enabled = strval(id_string);
			format(pvarname, sizeof(pvarname), "AccessoryEnabled%d",slot);
			SetPVarInt(playerid, pvarname, enabled);
			if(enabled == 1) {
				SetTimerEx("applyPlayerObject", 2500, false, "dd", playerid, slot);
			}
		}
	}
}
public onAccessoryBuy(playerid, index) {
	new pvarname[64];
	new id = mysql_insert_id();
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",index);
	SetPVarInt(playerid, pvarname, id);
	return 1;
}
getAccessoryAtSlot(playerid, slot) {
	new pvarname[64];
	format(pvarname, sizeof(pvarname), "Accessory%d", slot);
	if(GetPVarType(playerid, pvarname) == PLAYER_VARTYPE_NONE) {
		return -1;
	}
	return GetPVarInt(playerid, pvarname);
}
getAccessorySlot(playerid, accessory) {
	new pvarname[64];
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Accessory%d", i);
		if(GetPVarType(playerid, pvarname) != PLAYER_VARTYPE_NONE) {
			if(GetPVarInt(playerid, pvarname) == accessory) {
				return i;
			}
		}
	}
	return -1;
}
showEnableDisableSlot(playerid) {
	new pvarname[64];
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new enabled[] = "Enabled";
	new disabled[] = "Disabled";
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "AccessoryEnabled%d", i);
		new index = GetPVarInt(playerid, pvarname);
		format(tempstr, sizeof(tempstr), "Slot %d (%s)\n",i,index!=0?enabled:disabled);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccessories_ToggleSlot, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Toggle", "Cancel");
	return 1;
}


toggleAccessorySlot(playerid, index) {
	new pvarname[64];
	query[0] = 0;//[128];
	format(pvarname, sizeof(pvarname), "AccessoryEnabled%d", index);
	new on = GetPVarInt(playerid, pvarname);
	if(on == 1) {
		on = 0;
	} else {
		on = 1;
	}
	SetPVarInt(playerid, pvarname, on);
	new item = getAccessoryAtSlot(playerid, index);
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",item);
	format(query, sizeof(query), "UPDATE `useraccessories` SET `enabled` = %d WHERE `id` = %d",on, GetPVarInt(playerid,pvarname));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	new slot = getAccessoryAtSlot(playerid, index);
	if(slot != -1 && on == 1) {
		applyPlayerObject(playerid, index);
	} else if(on == 0 && slot != -1) {
		RemovePlayerAttachedObject(playerid, index);
	}
	return 1;
}
forward applyPlayerObject(playerid, index);
public applyPlayerObject(playerid, index) {
	new pvarname[64];
	query[0] = 0;//[128];
	new Float:x,Float:y, Float:z, Float:rx, Float:ry, Float:rz, Float:sx, Float:sy, Float:sz;
	new item = getAccessoryAtSlot(playerid, index);
	if(item == -1) return 0;
	new model = Accessories[item][EAccessoryModelID];
	format(pvarname, sizeof(pvarname), "AccessoryBone%d", item);
	new bone = GetPVarInt(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryEnabled%d", index);
	new on = GetPVarInt(playerid, pvarname);
	if(!on) return 0;
	if(bone == 0) {
		bone = Accessories[item][EAccessoryBone];
		format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",index);
		format(query, sizeof(query), "UPDATE `useraccessories` SET `bone` = %d WHERE `id` = %d",bone, GetPVarInt(playerid, pvarname));
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		SetPVarInt(playerid, pvarname, bone);
	}
	format(pvarname, sizeof(pvarname), "AccessoryX%d", item);
	x = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryY%d", item);
	y = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryZ%d", item);
	z = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotX%d", item);
	rx = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotY%d", item);
	ry = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotZ%d", item);
	rz = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryScaleX%d", item);
	sx = GetPVarFloat(playerid, pvarname);
	if(sx == 0) {
		sx = 1;
	}
	format(pvarname, sizeof(pvarname), "AccessoryScaleY%d", item);
	sy = GetPVarFloat(playerid, pvarname);
	if(sy == 0) {
		sy = 1;
	}
	format(pvarname, sizeof(pvarname), "AccessoryScaleZ%d", item);
	sz = GetPVarFloat(playerid, pvarname);
	if(sz == 0) {
		sz = 1;
	}
	SetPlayerAttachedObject(playerid, index, model, bone, x, y, z, rx, ry, rz, sx, sy, sz);
	return 1;
}
setItemBone(playerid, item, bone) {
	new pvarname[64];
	query[0] = 0;//[128];
	format(pvarname, sizeof(pvarname), "AccessoryBone%d", item);
	SetPVarInt(playerid, pvarname, bone);
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d", item);
	format(query, sizeof(query), "UPDATE `useraccessories` SET `bone` = %d WHERE `id` = %d",bone, GetPVarInt(playerid, pvarname));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	reloadPlayerItem(playerid, item);
}
reloadPlayerItem(playerid, item) {
	new slot = getAccessorySlot(playerid, item);
	if(slot != -1) {
		RemovePlayerAttachedObject(playerid, slot);
		applyPlayerObject(playerid, slot);
	}
}
showEditAccessoryMenu(playerid) {
	new pvarname[64];
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Accessory%d", i);
		new index = GetPVarInt(playerid, pvarname);
		new itemname[32];
		if(index == -1 || GetPVarType(playerid, pvarname) == PLAYER_VARTYPE_NONE) {
			format(itemname,sizeof(itemname), "None");
		} else {
			format(itemname, sizeof(itemname), "%s",Accessories[index][EAccessoryName]);
		}
		format(tempstr, sizeof(tempstr), "Slot %d (%s)\n",i,itemname);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccessories_SelectEditItem, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Modify", "Cancel");
}
showSetAccessoryBone(playerid) {
	new pvarname[64];
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "Accessory%d", i);
		new index = GetPVarInt(playerid, pvarname);
		new itemname[32];
		if(index == -1 || GetPVarType(playerid, pvarname) == PLAYER_VARTYPE_NONE) {
			format(itemname,sizeof(itemname), "None");
		} else {
			format(itemname, sizeof(itemname), "%s",Accessories[index][EAccessoryName]);
		}
		format(tempstr, sizeof(tempstr), "Slot %d (%s)\n",i,itemname);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAccessories_ItemBoneChange, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Modify", "Cancel");
}
showEditItemsMenu(playerid, item) {
	new pvarname[64];
	dialogstr[0] = 0;
	new bonename[32];
	new Float:x,Float:y, Float:z, Float:rx, Float:ry, Float:rz, Float:sx, Float:sy, Float:sz;
	format(pvarname, sizeof(pvarname), "AccessoryBone%d", item);
	new bone = GetPVarInt(playerid, pvarname);
	if(bone == 0) {
		bone = Accessories[item][EAccessoryBone];
		SetPVarInt(playerid, pvarname, bone);
	}
	getBoneName(bone, bonename, sizeof(bonename));
	format(pvarname, sizeof(pvarname), "AccessoryX%d", item);
	x = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryY%d", item);
	y = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryZ%d", item);
	z = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotX%d", item);
	rx = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotY%d", item);
	ry = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryRotZ%d", item);
	rz = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryScaleX%d", item);
	sx = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryScaleY%d", item);
	sy = GetPVarFloat(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "AccessoryScaleZ%d", item);
	sz = GetPVarFloat(playerid, pvarname);
	SetPVarInt(playerid, "AccessoryItemEdit", item);
	format(dialogstr, sizeof(dialogstr), "Bone: %s\nX: %f\nY: %f\nZ: %f\nRotation X: %f\nRotation Y: %f\nRotation Z: %f\nScale X: %f\nScale Y: %f\nScale Z: %f\n",bonename,x,y,z,rx,ry,rz,sx,sy,sz);
	ShowPlayerDialog(playerid, EAccessories_ModifyItem, DIALOG_STYLE_LIST, "Accessories Menu",dialogstr,"Select", "Cancel");
	return 1;
}
accessoriesOnPlayerEditObject(playerid, response, index, modelid, boneid, Float:fOffsetX, Float:fOffsetY, Float:fOffsetZ, Float:fRotX, Float:fRotY, Float:fRotZ, Float:fScaleX, Float:fScaleY, Float:fScaleZ) {
	#pragma unused boneid
	#pragma unused modelid
	if(!response) return 0;
	if(fScaleX <= -1.5 || fScaleX >= 1.5) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your scale on the X axis is too large");
		return 1;
	}
	if(fScaleY <= -1.5 || fScaleY >= 1.5) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your scale on the Y axis is too large");
		return 1;
	}
	if(fScaleZ <= -1.5 || fScaleZ >= 1.5) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your scale on the Z axis is too large");
		return 1;
	}
	new item = getAccessoryAtSlot(playerid, index);
	new pvarname[64];
	query[0] = 0;
	format(pvarname, sizeof(pvarname), "AccessoryX%d", item);
	SetPVarFloat(playerid, pvarname, fOffsetX);
	format(pvarname, sizeof(pvarname), "AccessoryY%d", item);
	SetPVarFloat(playerid, pvarname, fOffsetY);
	format(pvarname, sizeof(pvarname), "AccessoryZ%d", item);
	SetPVarFloat(playerid, pvarname, fOffsetZ);
	format(pvarname, sizeof(pvarname), "AccessoryRotX%d", item);
	SetPVarFloat(playerid, pvarname, fRotX);
	format(pvarname, sizeof(pvarname), "AccessoryRotY%d", item);
	SetPVarFloat(playerid, pvarname, fRotY);
	format(pvarname, sizeof(pvarname), "AccessoryRotZ%d", item);
	SetPVarFloat(playerid, pvarname, fRotZ);
	format(pvarname, sizeof(pvarname), "AccessoryScaleX%d", item);
	SetPVarFloat(playerid, pvarname, fScaleX);
	format(pvarname, sizeof(pvarname), "AccessoryScaleY%d", item);
	SetPVarFloat(playerid, pvarname, fScaleY);
	format(pvarname, sizeof(pvarname), "AccessoryScaleZ%d", item);
	SetPVarFloat(playerid, pvarname, fScaleZ);
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",item);
	format(query, sizeof(query), "UPDATE `useraccessories` SET `offsetx` = %f, `offsety`= %f, `offsetz` = %f, `rotx` = %f, `roty` = %f, `rotz` = %f, `scalex` = %f, `scaley` = %f, `scalez` = %f WHERE `id` = %d",fOffsetX, fOffsetY, fOffsetZ, fRotX, fRotY, fRotZ, fScaleX, fScaleY, fScaleZ,GetPVarInt(playerid,pvarname));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	reloadPlayerItem(playerid, item);
	return 1;
}
handleEditItem(playerid, item, type, Float:value) {
	new pvarname[32];
	query[0] = 0;//[256];
	new column[32];
	switch(type) {
		case 1: {
			format(pvarname, sizeof(pvarname), "AccessoryX%d", item);
			format(column, sizeof(column), "`offsetx`");
		}
		case 2: {
			format(pvarname, sizeof(pvarname), "AccessoryY%d", item);
			format(column, sizeof(column), "`offsety`");
		}
		case 3: {
			format(pvarname, sizeof(pvarname), "AccessoryZ%d", item);
			format(column, sizeof(column), "`offsetz`");
		}
		case 4: {
			format(pvarname, sizeof(pvarname), "AccessoryRotX%d", item);
			format(column, sizeof(column), "`rotx`");
		}
		case 5: {
			format(pvarname, sizeof(pvarname), "AccessoryRotY%d", item);
			format(column, sizeof(column), "`roty`");
		}
		case 6: {
			format(pvarname, sizeof(pvarname), "AccessoryRotZ%d", item);
			format(column, sizeof(column), "`rotz`");
		}
		case 7: {
			format(pvarname, sizeof(pvarname), "AccessoryScaleX%d", item);
			format(column, sizeof(column), "`scalex`");
		}
		case 8: {
			format(pvarname, sizeof(pvarname), "AccessoryScaleY%d", item);
			format(column, sizeof(column), "`scaley`");
		}
		case 9: {
			format(pvarname, sizeof(pvarname), "AccessoryScaleZ%d", item);
			format(column, sizeof(column), "`scalez`");
		}
		default: {
			return 1;
		}
	}
	if(type >= 7 && type <= 9) {
		if(value <= -1.5 || value >= 1.5) {
			SendClientMessage(playerid, X11_TOMATO_2, "Your scale is too large");
			return 1;
		}
	}
	SetPVarFloat(playerid, pvarname, value);
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",item);
	format(query, sizeof(query), "UPDATE `useraccessories` SET %s = %f WHERE `id` = %d",column,value, GetPVarInt(playerid,pvarname));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	reloadPlayerItem(playerid, item);
	return 1;
}
handleModifyItem(playerid, item) {
	if(item == 0){ 
		ShowPlayerDialog(playerid, EAccessories_SetBone, DIALOG_STYLE_LIST, "Edit Bone","Spine\nHead\nLeft Arm\nRight Arm\nLeft Hand\nRight Hand\nLeft Thigh\nRight Thigh\nLeft Foot\nRight Foot\nLeft Calf\nRight Calf\nLeft Forearm\nRight Forearm\nLeft Clavicle\nRight Clavicle\nNeck\nJaw","Select", "Cancel");
	} else {
		SetPVarInt(playerid, "AccessoryEditType", item);
		ShowPlayerDialog(playerid, EAccessories_SetValue, DIALOG_STYLE_INPUT, "Edit Value","Enter the new value you would like","Ok", "Cancel");
	}
}
getBoneName(bone, dst[], dstlen) {
	switch(bone) {
		case BONE_SPINE: {
			strcpy(dst, "Spine", dstlen);
		}
		case BONE_HEAD: {
			strcpy(dst, "Head", dstlen);
		}
		case BONE_LARM: {
			strcpy(dst, "Left Arm", dstlen);
		}
		case BONE_RARM: {
			strcpy(dst, "Right Arm", dstlen);
		}
		case BONE_LHAND: {
			strcpy(dst, "Left Hand", dstlen);
		}
		case BONE_RHAND: {
			strcpy(dst, "Right Hand", dstlen);
		}
		case BONE_LTHIGH: {
			strcpy(dst, "Left Thigh", dstlen);
		}
		case BONE_RTHIGH: {
			strcpy(dst, "Right Thigh", dstlen);
		}
		case BONE_LFOOT: {
			strcpy(dst, "Left Foot", dstlen);
		}
		case BONE_RFOOT: {
			strcpy(dst, "Right Foot", dstlen);
		}
		case BONE_LCALF: {
			strcpy(dst, "Left Calf", dstlen);
		}
		case BONE_RCALF: {
			strcpy(dst, "Right Calf", dstlen);
		}
		case BONE_LFOREARM: {
			strcpy(dst, "Left Forearm", dstlen);
		}
		case BONE_RFOREARM: {
			strcpy(dst, "Right Forearm", dstlen);
		}
		case BONE_LCLAVICLE: {
			strcpy(dst, "Left Clavicle", dstlen);
		}
		case BONE_RCLAVICLE: {
			strcpy(dst, "Right Clavicle", dstlen);
		}
		case BONE_NECK: {
			strcpy(dst, "Neck", dstlen);
		}
		case BONE_JAW: {
			strcpy(dst, "Jaw", dstlen);
		}
	}
	return 0;
}
setItemSlot(playerid, object, slot) {
	new msg[128], pvarname[64];
	format(msg, sizeof(msg), "Accessory%d", slot);
	SetPVarInt(playerid, msg, object);
	format(pvarname, sizeof(pvarname), "userAccessorySQLID%d",object);
	format(query, sizeof(query), "UPDATE `useraccessories` SET `slot` = %d WHERE `id` = %d",slot, GetPVarInt(playerid, pvarname));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	reloadPlayerItem(playerid, slot);
}
restoreAccessories(playerid) {
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		new slot = getAccessoryAtSlot(playerid, i);
		if(slot != -1) {
			applyPlayerObject(playerid, i);
		}
	}
}
IsWearingAccessory(playerid, objectid) {
	new pvarname[64];
	for(new i=0;i<NUM_ACCESSORY_SLOTS;i++) {
		format(pvarname, sizeof(pvarname), "AccessoryEnabled%d", i);
		new index = GetPVarInt(playerid, pvarname);
		if(index == 1) {
			index = getAccessoryAtSlot(playerid, i);
			if(index != -1 && Accessories[index][EAccessoryModelID] == objectid) return 1;
		}
	}
	return 0;
}
stock getSkinName(skinid) {
	new skinname[64];
	new skinidx = getSkinArrayIndex(skinid);
	if(skinidx != -1) {
		strmid(skinname,SkinData[skinidx][ESkinName],0,strlen(SkinData[skinidx][ESkinName]),64);
	}
	return skinname;
}