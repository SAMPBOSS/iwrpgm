
enum ESafeTypeInfo {
	ESafeTypeName[32],
	ESafeTypeMaxCash,
	ESafeTypeMaxPot,
	ESafeTypeMaxCoke,
	ESafeTypeMaxMeth,
	ESafeTypeMaxGuns,
	ESafeTypeMaxMatsA,
	ESafeTypeMaxMatsB,
	ESafeTypeMaxMatsC,
	ESafeTypePrice
}

new SafeTypes[][ESafeTypeInfo] = {{"Simple Safe",25000,300,200,100,5,50000,45000,25000, 15000},
									{"Medium Safe",250000,2000,2000,2000,10,100000,100000,100000, 35000},
									{"Large Safe",1500000,3000,3000,3000,25,100000,100000,100000, 55000},
									{"Very Large Safe",99500000,999000,999000,999000,50,100000,1000000,1000000, 250000}};

enum ESafeOwnerType {
	ESafeOwner_House,
	ESafeOwner_Family,
};

enum ESafeInfo {
	ESafeType,
	Float:ESafeX,
	Float:ESafeY,
	Float:ESafeZ,
	Float:ESafeAngle,
	ESafeObjectID,
	ESafeSQLID
};

enum ESafeItemType {
	ESafeItemType_Money,
	ESafeItemType_Pot,
	ESafeItemType_Coke,
	ESafeItemType_Meth,
	ESafeItemType_Gun,
	ESafeItemType_MatsA,
	ESafeItemType_MatsB,
	ESafeItemType_MatsC,
	ESafeItemType_SpecialItem,
}
//family safe object ids
new FamilySafes[MAX_FAMILIES][ESafeInfo];
new HouseSafes[MAX_HOUSES][ESafeInfo];

enum ESafeSingleItems {
	ESafeSItemName[32],
	ESafeSItemPVar[32],
	ESafeItemType:ESafeSType,
};

new SafeSingleItems[][ESafeSingleItems] = {
											{"Cash","Money",ESafeItemType_Money},
											{"Pot","Pot",ESafeItemType_Pot},
											{"Coke","Coke",ESafeItemType_Coke},
											{"Gun","Unused",ESafeItemType_Gun},
											{"Meth","Meth",ESafeItemType_Meth},
											{"Materials A","MatsA",ESafeItemType_MatsA},
											{"Materials B","MatsB",ESafeItemType_MatsB},
											{"Materials C","MatsC",ESafeItemType_MatsC}
										  };

stock CreateSafe(ESafeOwnerType:ownertype, safetype, owner, Float:X, Float:Y, Float:Z, Float:A, callback[]) {
	query[0] = 0;
	format(query, sizeof(query), "INSERT INTO `safes` (`X`,`Y`,`Z`,`Angle`,`type`,`safetype`,`owner`) VALUES (%f,%f,%f,%f,%d,%d,%d)",X, Y, Z, A, _:ownertype, safetype, owner);
	mysql_function_query(g_mysql_handle, query, true, "OnCreateSafe","dffffsd",owner,X,Y,Z,A, callback, safetype);
}
forward OnCreateSafe(owner, Float:X, Float:Y, Float:Z, Float:A, callback[], type);
public OnCreateSafe(owner, Float:X, Float:Y, Float:Z, Float:A, callback[], type) {
	query[0] = 0;
	new id = mysql_insert_id();
	for(new i=0;i<sizeof(SafeSingleItems);i++) {
		if(i == _:ESafeItemType_Gun) continue;
		format(query, sizeof(query), "INSERT INTO `safeitems` (`safeid`,`type`) VALUES (%d,%d)",id, i);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	}
	CallLocalFunction(callback,"ddffff", owner, type, X, Y, Z, A);
}

stock DeleteSafe(ESafeOwnerType:ownertype, id) {
	query[0] = 0;//[256];
	format(query, sizeof(query), "DELETE FROM `safes` WHERE `id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query), "DELETE FROM `safeitems` WHERE `safeid` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroySafeObject(ownertype,id);
}
DestroySafeObject(ESafeOwnerType:ownertype,id) {
	if(ownertype == ESafeOwner_Family) {
		for(new i=0;i<sizeof(FamilySafes);i++) {
			if(FamilySafes[i][ESafeSQLID] == id) {
				DestroyDynamicObject(FamilySafes[i][ESafeObjectID]);
				FamilySafes[i][ESafeObjectID] = 0;
				FamilySafes[i][ESafeSQLID] = 0;
			}
		}
	} else if(ownertype == ESafeOwner_House) {
		for(new i=0;i<sizeof(HouseSafes);i++) {
			if(HouseSafes[i][ESafeSQLID] == id) {
				DestroyDynamicObject(HouseSafes[i][ESafeObjectID]);
				HouseSafes[i][ESafeObjectID] = 0;
				HouseSafes[i][ESafeSQLID] = 0;
			}
		}
	}
	return 1;
}


//either family ID of family, or house ID(non-sql for family)
setupSafe(ESafeOwnerType:ownertype, Float:X, Float:Y, Float:Z, Float:A, owner) {
	switch(ownertype) {
		case ESafeOwner_Family: {
			FamilySafes[owner][ESafeX] = X;
			FamilySafes[owner][ESafeY] = Y;
			FamilySafes[owner][ESafeZ] = Z;
			FamilySafes[owner][ESafeAngle] = A;
			new vw = GetFamilyHQVW(owner);
			new hqid = Families[owner][EFamilyHQID];
			new interior = FamilyHQs[hqid][EFamilyHQInfoInt];
			FamilySafes[owner][ESafeObjectID] = CreateDynamicObject(1829, X, Y,Z, 0, 0, A, vw, interior);
		}
		case ESafeOwner_House: {
			HouseSafes[owner][ESafeX] = X;
			HouseSafes[owner][ESafeY] = Y;
			HouseSafes[owner][ESafeZ] = Z;
			HouseSafes[owner][ESafeAngle] = A;
			new vw =  houseGetVirtualWorld(owner);
			new interior = houseGetInterior(owner);
			HouseSafes[owner][ESafeObjectID] = CreateDynamicObject(1829, X, Y,Z, 0, 0, A, vw, interior);
		}
	}
}
YCMD:gotofamsafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Goto a family safe");
		return 1;
	}
	new family;
	if(!sscanf(params, "d", family)) {	
		if(family < 0 || family > sizeof(Families)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family!");
			return 1;
		}
		if(family == -1 || FamilySafes[family][ESafeObjectID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Your family doesn't have a safe!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z, interior;
		GetDynamicObjectPos(FamilySafes[family][ESafeObjectID], X, Y, Z);
		SetPlayerVirtualWorld(playerid, GetFamilyHQVW(family));
		SetPlayerPos(playerid, X, Y, Z);
		interior = Families[family][EFamilyHQID];
		SetPlayerInterior(playerid, FamilyHQs[interior][EFamilyHQInfoInt]);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotofamsafe [familyid]");
	}
	return 1;
}
YCMD:buyfamilysafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying a family safe");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(getFamilyType(family) == _:EFamilyType_None) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family cannot purchase a safe since it's unofficial.");
		return 1;
	}
	if(FamilyHasSafe(family)) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family already has a safe!");
		return 1;
	}
	if(~rankperms & EFamilyPerms_CanAdjust) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	if(GetFamilyHQVW(family) != GetPlayerVirtualWorld(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in your family HQ!");
		return 1;
	}
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(SafeTypes);i++) {
		format(tempstr, sizeof(tempstr), "%s - %s\n",SafeTypes[i][ESafeTypeName],getNumberString(SafeTypes[i][ESafeTypePrice]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFamilyDialog_BuySafe, DIALOG_STYLE_LIST, "{00BFFF}Buying family safe",dialogstr, "Buy", "Cancel");
	return 1;
}
YCMD:buyhousesafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Buys a family safe");
		return 1;
	}
	new house = getStandingExit(playerid, 150.0);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be inside a house!");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must own this house!");
		return 1;
	}
	if(HouseHasSafe(house)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You already have a safe!");
		return 1;
	}
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(SafeTypes);i++) {
		format(tempstr, sizeof(tempstr), "%s - %s\n",SafeTypes[i][ESafeTypeName],getNumberString(SafeTypes[i][ESafeTypePrice]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFamilyDialog_BuyHouseSafe, DIALOG_STYLE_LIST, "{00BFFF}Buying house safe",dialogstr, "Buy", "Cancel");
	return 1;
}
handleBuyFamilySafe(playerid, listitem) {
	new price = SafeTypes[listitem][ESafeTypePrice];
	if(GetMoneyEx(playerid) < price) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money for this");
		return 1;
	}
	GiveMoneyEx(playerid, -price);
	new sqlid = GetPVarInt(playerid,"Family");
	new Float:X,Float:Y,Float:Z,Float:A;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	Z -= 0.55;
	CreateSafe(ESafeOwner_Family, listitem, sqlid, X, Y, Z, A, "OnFamilySafeCreate");
	return 1;
}
handleBuyHouseSafe(playerid, listitem) {
	new price = SafeTypes[listitem][ESafeTypePrice];
	if(GetMoneyEx(playerid) < price) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money for this");
		return 1;
	}
	GiveMoneyEx(playerid, -price);
	new sqlid = getStandingExit(playerid, 150.0);
	sqlid = getHouseSQLID(sqlid);
	new Float:X,Float:Y,Float:Z,Float:A;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	Z -= 0.55;
	CreateSafe(ESafeOwner_House, listitem, sqlid, X, Y, Z, A, "OnHouseSafeCreate");
	return 1;
}
YCMD:movesafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Used for moving a safe");
	}
	SetPVarInt(playerid, "MovingSafe", 1);
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Click on the safe to move it!");
	return 1;
}
YCMD:deletesafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Used for moving a safe");
	}
	SetPVarInt(playerid, "MovingSafe", 2);
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Click on the safe to delete it!");
	return 1;
}
YCMD:upgradesafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Used for moving a safe");
	}
	SetPVarInt(playerid, "MovingSafe", 3);
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Click on the safe to upgrade it!");
	return 1;
}
forward OnFamilySafeCreate(familyid, type, Float:X,Float:Y,Float:Z,Float:A);
public OnFamilySafeCreate(familyid, type, Float:X,Float:Y,Float:Z,Float:A) {
	new fid = FindFamilyBySQLID(familyid);
	new id = mysql_insert_id();
	FamilySafes[fid][ESafeSQLID] = id;
	FamilySafes[fid][ESafeType] = type;
	setupSafe(ESafeOwner_Family, X, Y, Z, A, fid);
}
forward OnHouseSafeCreate(houseid, type, Float:X,Float:Y,Float:Z,Float:A);
public OnHouseSafeCreate(houseid, type, Float:X,Float:Y,Float:Z,Float:A) {
	new id = mysql_insert_id();
	houseid = houseIDFromSQLID(houseid);
	if(houseid == -1) return 0;
	HouseSafes[houseid][ESafeSQLID] = id;
	HouseSafes[houseid][ESafeType] = type;
	setupSafe(ESafeOwner_House, X, Y, Z, A, houseid);
	return 1;
}
loadFamilySafes() {
	mysql_function_query(g_mysql_handle,"SELECT `safes`.`id`,`X`,`Y`,`Z`,`Angle`,`safetype`,`safes`.`owner` FROM `families` INNER JOIN `safes` ON `families`.`id` = `safes`.`owner` WHERE `safes`.`type` = 1", true, "OnLoadFamilySafes","");
}
forward OnLoadFamilySafes();
public OnLoadFamilySafes() {
	new rows, fields;
	new id_string[128];
	cache_get_data(rows,fields);
	new id, Float:X, Float:Y, Float:Z, Float:A, type,owner;
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,id_string);
		id = strval(id_string);
		cache_get_row(i,1,id_string);
		X = floatstr(id_string);
		cache_get_row(i,2,id_string);
		Y = floatstr(id_string);
		cache_get_row(i,3,id_string);
		Z = floatstr(id_string);
		cache_get_row(i,4,id_string);
		A = floatstr(id_string);
		cache_get_row(i,5,id_string);
		type = strval(id_string);
		cache_get_row(i,6,id_string);
		owner = strval(id_string);
		new fid = FindFamilyBySQLID(owner);
		if(fid != -1) {
			
			setupSafe(ESafeOwner_Family, X, Y, Z, A, fid);
			FamilySafes[fid][ESafeSQLID] = id;
			FamilySafes[fid][ESafeType] = type;
		}
	}
}
loadHouseSafes() {
	mysql_function_query(g_mysql_handle,"SELECT `safes`.`id`,`safes`.`X`,`safes`.`Y`,`safes`.`Z`,`safes`.`Angle`,`safetype`,`safes`.`owner` FROM `houses` INNER JOIN `safes` ON `houses`.`id` = `safes`.`owner` WHERE `safes`.`type` = 0", true, "OnLoadHouseSafes","");
}
forward OnLoadHouseSafes();
public OnLoadHouseSafes() {
	new rows, fields;
	new id_string[128];
	cache_get_data(rows,fields);
	new id, Float:X, Float:Y, Float:Z, Float:A, type,owner;
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,id_string);
		id = strval(id_string);
		cache_get_row(i,1,id_string);
		X = floatstr(id_string);
		cache_get_row(i,2,id_string);
		Y = floatstr(id_string);
		cache_get_row(i,3,id_string);
		Z = floatstr(id_string);
		cache_get_row(i,4,id_string);
		A = floatstr(id_string);
		cache_get_row(i,5,id_string);
		type = strval(id_string);
		cache_get_row(i,6,id_string);
		owner = strval(id_string);
		new fid = houseIDFromSQLID(owner);
		if(fid != -1) {
			setupSafe(ESafeOwner_House, X, Y, Z, A, fid);
			HouseSafes[fid][ESafeSQLID] = id;
			HouseSafes[fid][ESafeType] = type;
		}
	}
}
YCMD:fsafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for controlling the family safe");
		return 1;
	}
	if(IsOnDuty(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	if(FamilySafes[family][ESafeObjectID] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family doesn't have a safe!");
		return 1;
	}
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, FamilySafes[family][ESafeX], FamilySafes[family][ESafeY], FamilySafes[family][ESafeZ])) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near the safe!");
		return 1;
	}
	if(GetFamilyHQVW(family) != GetPlayerVirtualWorld(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in your family HQ!");
		return 1;
	}
	new type[32],item[32],amount;
	if(!sscanf(params,"s[32]S()[32]D(0)",type,item,amount)) {
		if(amount > 999999 || amount < 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(!strcmp(type,"show", true)) {
			sendSafeStats(playerid, FamilySafes[family][ESafeSQLID]);
		} else if(!strcmp(type, "store", true)) {
			if(~rankperms & EFamilyPerms_CanGiveSafe) {
				SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
				return 1;
			}
			new found = 0;
			for(new i=0;i<sizeof(SafeSingleItems);i++) {
				if(!strcmp(item, SafeSingleItems[i][ESafeSItemName], true) || !strcmp(item,SafeSingleItems[i][ESafeSItemPVar], true)) {
					found = 1;
					tryAddItem(playerid, FamilySafes[family][ESafeSQLID], SafeSingleItems[i][ESafeSType], amount);
				}
			}
			if(!found) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			}
		} else if(!strcmp(type, "take", true) && strlen(item) > 0) {
			if(~rankperms & EFamilyPerms_CanTakeSafe) {
				SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
				return 1;
			}
			if(!strcmp(item, "Gun", true, 3)) {
				new index = strval(item[3]);
				if(index < 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invaild Weapon ID");
					return 1;
				}
				tryTakeSafeGun(playerid, FamilySafes[family][ESafeSQLID], index);
				return 1;
			}
			new found = 0;
			for(new i=0;i<sizeof(SafeSingleItems);i++) {
				if(!strcmp(item, SafeSingleItems[i][ESafeSItemName], true) || !strcmp(item,SafeSingleItems[i][ESafeSItemPVar], true)) {
					found = 1;
					tryTakeSafeItem(playerid, FamilySafes[family][ESafeSQLID], SafeSingleItems[i][ESafeSType], amount);
				}
			}
			if(!found) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			}
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Mode!");
		}
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fsafe [mode] [item] [amount]");
		SendClientMessage(playerid, X11_WHITE, "Modes: show, store, take");
		SendClientMessage(playerid, X11_WHITE, "Item Names: cash, pot, coke, meth, gun0 - gunX, MatsA, MatsB, MatsC");
	}
	return 1;
}
YCMD:hsafe(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for controlling the house safe");
		return 1;
	}
	if(IsOnDuty(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	new family = getStandingExit(playerid, 150.0);
	if(family == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be inside a house!");
		return 1;
	}
	if(Houses[family][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must own this house!");
		return 1;
	}
	if(HouseSafes[family][ESafeObjectID] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your house doesn't have a safe!");
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 5.0, HouseSafes[family][ESafeX], HouseSafes[family][ESafeY], HouseSafes[family][ESafeZ])) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near the safe!");
		return 1;
	}
	if(houseGetVirtualWorld(family) != GetPlayerVirtualWorld(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in your family HQ!");
		return 1;
	}
	new type[32],item[32],amount;
	if(!sscanf(params,"s[32]S()[32]D(0)",type,item,amount)) {
		if(amount > 999999 || amount < 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(!strcmp(type,"show", true)) {
			sendSafeStats(playerid, HouseSafes[family][ESafeSQLID]);
		} else if(!strcmp(type, "store", true) && strlen(item) > 0) {
			new found = 0;
			for(new i=0;i<sizeof(SafeSingleItems);i++) {
				if(!strcmp(item, SafeSingleItems[i][ESafeSItemName], true) || !strcmp(item,SafeSingleItems[i][ESafeSItemPVar], true)) {
					found = 1;
					tryAddItem(playerid, HouseSafes[family][ESafeSQLID], SafeSingleItems[i][ESafeSType], amount);
				}
			}
			if(!found) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			}
		} else if(!strcmp(type, "take", true) && strlen(item) > 0) {
			if(!strcmp(item, "Gun", true, 3)) {
				new index = strval(item[3]);
				if(index < 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invaild Weapon ID");
					return 1;
				}
				tryTakeSafeGun(playerid, HouseSafes[family][ESafeSQLID], index);
				return 1;
			}
			new found = 0;
			for(new i=0;i<sizeof(SafeSingleItems);i++) {
				if(!strcmp(item, SafeSingleItems[i][ESafeSItemName], true) || !strcmp(item,SafeSingleItems[i][ESafeSItemPVar], true)) {
					found = 1;
					tryTakeSafeItem(playerid, HouseSafes[family][ESafeSQLID], SafeSingleItems[i][ESafeSType], amount);
				}
			}
			if(!found) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			}
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Mode!");
		}
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /hsafe [mode] [item] [amount]");
		SendClientMessage(playerid, X11_WHITE, "Modes: show, store, take");
		SendClientMessage(playerid, X11_WHITE, "Item Names: cash, pot, coke, meth, gun0 - gunX, MatsA, MatsB, MatsC");
	}
	return 1;
}
sendSafeStats(playerid, id) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `type`,`value` FROM `safeitems` WHERE `safeid` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "OnShowSafe","dd", playerid, id);
}
forward OnShowSafe(playerid, safeid);
public OnShowSafe(playerid, safeid) {
	new rows, fields;
	cache_get_data(rows, fields);
	new msg[128];
	new itemname[64],itemvalue;
	new type;
	new id_string[128];
	new weapnum;
	SendClientMessage(playerid, X11_WHITE, "Safe Items:");
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		type = strval(id_string);
		cache_get_row(i, 1, id_string);
		itemvalue = strval(id_string);
		getItemName(itemname, sizeof(itemname), type);
		if(ESafeItemType:type == ESafeItemType_Gun) {
			weapnum++;
			new gun, ammo;
			new gunname[32];
			decodeWeapon(itemvalue, gun, ammo);
			GetWeaponNameEx(gun, gunname, sizeof(gunname));
			format(itemname, sizeof(itemname), "%s(%s)", gunname, getNumberString(weapnum));
			itemvalue = ammo;
		}
		format(msg, sizeof(msg), "%s: %s",itemname, getNumberString(itemvalue));
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
}
getItemName(dst[], dstlen, type) {
	switch(type) {
		case ESafeItemType_Money: {
			format(dst, dstlen, "Money");
		}
		case ESafeItemType_Pot: {
			format(dst, dstlen, "Pot");
		}
		case ESafeItemType_Coke: {
			format(dst, dstlen, "Coke");
		}
		case ESafeItemType_Meth: {
			format(dst, dstlen, "Meth");
		}
		case ESafeItemType_Gun: {
			format(dst, dstlen, "Gun");
		}
		case ESafeItemType_MatsA: {
			format(dst, dstlen, "Materials A");
		}
		case ESafeItemType_MatsB: {
			format(dst, dstlen, "Materials B");
		}
		case ESafeItemType_MatsC: {
			format(dst, dstlen, "Materials C");
		}
	}
	return 0;
}
tryAddItem(playerid, safeid, ESafeItemType:type, amount) {
	query[0] = 0;//[256];
	if(type != ESafeItemType_Gun) {
		new safetype = getSafeType(safeid);
		new maxitems = getSafeTypeMax(safetype, type);
		format(query, sizeof(query), "SELECT 1 FROM `safeitems` WHERE `safeid` = %d AND `type` = %d AND `value`+%d < %d",safeid, _:type,amount,maxitems);
	} else {
		format(query, sizeof(query), "SELECT 1 FROM `safeitems` WHERE `safeid` = %d AND `type` = %d",safeid, _:type);
	}
	mysql_function_query(g_mysql_handle, query, true, "OnSafeAddItemCheck", "dddd",playerid,safeid,_:type,amount);
}
forward OnSafeAddItemCheck(playerid, safeid, ESafeItemType:type, amount);
public OnSafeAddItemCheck(playerid, safeid, ESafeItemType:type, amount) {
	new rows, fields;
	cache_get_data(rows, fields);
	new msg[128];
	if(rows < 1 && type != ESafeItemType_Gun) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't fit this in the safe, its probably full!");
	} else if(rows >= getSafeTypeMax(getSafeType(safeid), type) && type == ESafeItemType_Gun) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't fit this in the safe, its probably full!");
	} else {
		new i = getSafeTypeIndex(type);
		if(i == -1) return 0;
		if(type != ESafeItemType_Gun) {
			if(amount < 1 || amount > 9999999) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
				return 1;
			}
			new num = GetPVarInt(playerid, SafeSingleItems[i][ESafeSItemPVar]);
			if(num < amount) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
				return 1;
			}
			num -= amount;
			SetPVarInt(playerid, SafeSingleItems[i][ESafeSItemPVar], num);
			GiveMoneyEx(playerid, 0);//sync their money
			format(msg, sizeof(msg), "* %s has put %s %s into the family safe",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),getNumberString(amount),SafeSingleItems[i][ESafeSItemName]);
		} else if(SafeSingleItems[i][ESafeSType] == ESafeItemType_Gun) {
			new weapon, ammo;
			weapon = GetPlayerWeaponEx(playerid);
			if(weapon == 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "You aren't holding a weapon");
				return 1;
			}
			new slot = GetWeaponSlot(weapon);
			GetPlayerWeaponDataEx(playerid, slot, weapon, ammo);
			amount = encodeWeapon(weapon, ammo);
			RemovePlayerWeapon(playerid, weapon);
			new gunname[32];
			GetWeaponNameEx(weapon, gunname, sizeof(gunname));
			format(msg, sizeof(msg), "* %s has put a %s into the family safe",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),gunname);
		}
		new ESafeOwnerType:ownertype = getSafeOwnerType(safeid);
		if(ownertype == ESafeOwner_Family) {
			new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
			if(family != -1) 
				FamilyMessage(family, X11_YELLOW, msg);
		} else {
			SendClientMessage(playerid, X11_YELLOW, "* Safe Item Added!");
		}
		addToItem(safeid, type, amount);
	}
	return 0;
}
getSafeTypeIndex(ESafeItemType:type) {
	for(new i=0;i<sizeof(SafeSingleItems);i++) {
		if(SafeSingleItems[i][ESafeSType] == type) {
			return i;
		}
	}
	return -1;
}
getSafeTypeMax(safe, ESafeItemType:type) {
	new maxitems;
	if(safe == -1) return 0;
	switch(type) {
		case ESafeItemType_Money: {
			maxitems = SafeTypes[safe][ESafeTypeMaxCash];
		}
		case ESafeItemType_Pot: {
			maxitems = SafeTypes[safe][ESafeTypeMaxPot];
		}
		case ESafeItemType_Coke: {
			maxitems = SafeTypes[safe][ESafeTypeMaxCoke];
		}
		case ESafeItemType_Meth: {
			maxitems = SafeTypes[safe][ESafeTypeMaxMeth];
		}
		case ESafeItemType_MatsA: {
			maxitems = SafeTypes[safe][ESafeTypeMaxMatsA];
		}
		case ESafeItemType_MatsB: {
			maxitems = SafeTypes[safe][ESafeTypeMaxMatsB];
		}
		case ESafeItemType_MatsC: {
			maxitems = SafeTypes[safe][ESafeTypeMaxMatsC];
		}
		case ESafeItemType_Gun: {
			maxitems = SafeTypes[safe][ESafeTypeMaxGuns];
		}
	}
	return maxitems;
}
addToItem(safeid, ESafeItemType:type, value) {
	query[0] = 0;//[128];
	if(type != ESafeItemType_Gun) {
		new safe = getSafeType(safeid);
		if(safe == -1) {
			return 0;
		}
		new maxitems = getSafeTypeMax(safe, type);
		format(query, sizeof(query), "UPDATE `safeitems` SET `value` = `value`+%d WHERE `safeid` = %d AND `type` = %d AND `value`+%d < %d",value,safeid, _:type,value,maxitems);
	} else {
		format(query, sizeof(query), "INSERT INTO `safeitems` (`safeid`,`type`,`value`) VALUES (%d,%d,%d)",safeid,_:type,value);
	}
	mysql_function_query(g_mysql_handle, query, true, "OnEditSafeItem","d",safeid);
	return 1;
}

forward OnEditSafeItem(safeid);
public OnEditSafeItem(safeid) {
	new numaffected =  mysql_affected_rows();
	for(new i=0;i<sizeof(FamilySafes);i++) {
		if(FamilySafes[i][ESafeSQLID] == safeid && numaffected == 0) {
			FamilyMessage(i, X11_YELLOW, "Failed to add items to safe, its probably full, or would have been over capacity if those items were added");
		}
	}
}
tryTakeSafeItem(playerid, safeid, ESafeItemType:type, amount) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT `value` FROM `safeitems` WHERE `safeid` = %d AND `type` = %d",safeid, _:type);
	mysql_function_query(g_mysql_handle, query, true, "OnSafeItemCheck","dddd", playerid, safeid, _:type, amount);
}
forward OnSafeItemCheck(playerid, safeid, ESafeItemType:type, amount);
public OnSafeItemCheck(playerid, safeid, ESafeItemType:type, amount) {
	new total,id_string[128];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		cache_get_row(0, 0, id_string);
		total = strval(id_string);
	} else return 0;
	if(total-amount < 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "There is not enough of this item to take!");
		return 1;
	}
	takeFromSafe(playerid, safeid, type, amount);
	return 0;
}
takeFromSafe(playerid, safeid, ESafeItemType:type, amount) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "UPDATE `safeitems` SET `value` = `value`-%d WHERE `safeid` = %d AND `type` = %d AND `value`-%d >= 0",amount,safeid, _:type,amount);
	mysql_function_query(g_mysql_handle, query, true, "OnSafeUpdate","dddd",playerid,safeid,_:type,amount);
}
forward OnSafeUpdate(playerid, safeid, ESafeItemType:type, amount);
public OnSafeUpdate(playerid, safeid, ESafeItemType:type, amount) {
	new numrows = mysql_affected_rows();
	if(numrows < 1) return 0;
	switch(type) {
		case ESafeItemType_Money: {
			GiveMoneyEx(playerid, amount);
		}
		case ESafeItemType_Pot: {
			new pot = GetPVarInt(playerid, "Pot");
			pot += amount;
			SetPVarInt(playerid, "Pot", pot);
		}
		case ESafeItemType_Coke: {
			new pot = GetPVarInt(playerid, "Coke");
			pot += amount;
			SetPVarInt(playerid, "Coke", pot);
		}
		case ESafeItemType_Meth: {
			new pot = GetPVarInt(playerid, "Meth");
			pot += amount;
			SetPVarInt(playerid, "Meth", pot);
		}
		case ESafeItemType_MatsA: {
			new pot = GetPVarInt(playerid, "MatsA");
			pot += amount;
			SetPVarInt(playerid, "MatsA", pot);
		}
		case ESafeItemType_MatsB: {
			new pot = GetPVarInt(playerid, "MatsB");
			pot += amount;
			SetPVarInt(playerid, "MatsB", pot);
		}
		case ESafeItemType_MatsC: {
			new pot = GetPVarInt(playerid, "MatsC");
			pot += amount;
			SetPVarInt(playerid, "MatsC", pot);
		}
	}
	new house, id;
	findSafeBySQLID(safeid, house, id);
	if(house == 0) {
		new i = getSafeTypeIndex(type);
		format(query, sizeof(query), "* %s has taken %s %s from the family safe",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),getNumberString(amount),SafeSingleItems[i][ESafeSItemName]);
		FamilyMessage(id, X11_YELLOW, query);
	}
	return 1;
}
tryTakeSafeGun(playerid, safeid, index) {
	query[0] = 0;//[128];
	format(query, sizeof(query), "SELECT `id`,`value` FROM `safeitems` WHERE `safeid` = %d AND `type` = %d LIMIT %d, 1",safeid, _:ESafeItemType_Gun, index-1);
	mysql_function_query(g_mysql_handle, query, true, "OnSafeGunCheck","ddd", playerid, safeid, index);
}
forward OnSafeGunCheck(playerid, safeid, index);
public OnSafeGunCheck(playerid, safeid, index) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Gun Index!");
		return 1;
	}
	new gun, ammo;
	new id_string[128];
	cache_get_row(0, 1, id_string);
	decodeWeapon(strval(id_string), gun, ammo);
	
	if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
		return 1;
	} else {
		GivePlayerWeaponEx(playerid, gun, ammo);
	}
	new gunname[32];
	GetWeaponNameEx(gun, gunname, sizeof(gunname));
	new house, id;
	findSafeBySQLID(safeid, house, id);
	if(house == 0) {
		format(id_string, sizeof(id_string), "* %s has taken a %s from the family safe",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),gunname);
		FamilyMessage(id, X11_YELLOW, id_string);
	} else {
		format(id_string, sizeof(id_string), "* %s has taken a %s from the house safe",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),gunname);
		SendClientMessage(playerid, X11_YELLOW, id_string);
	}
	
	cache_get_row(0, 0, id_string);
	format(id_string, sizeof(id_string), "DELETE FROM `safeitems` WHERE `id` = %d",strval(id_string));
	mysql_function_query(g_mysql_handle, id_string, false, "EmptyCallback","");
	return 1;
}
getSafeType(safeid) {
	for(new i=0;i<sizeof(FamilySafes);i++) {
		if(FamilySafes[i][ESafeSQLID] == safeid) {
			return FamilySafes[i][ESafeType];
		}
	}
	for(new i=0;i<sizeof(HouseSafes);i++) {
		if(HouseSafes[i][ESafeSQLID] == safeid) {
			return HouseSafes[i][ESafeType];
		}
	}
	return -1;
}
ESafeOwnerType:getSafeOwnerType(safeid) {
	for(new i=0;i<sizeof(FamilySafes);i++) {
		if(FamilySafes[i][ESafeSQLID] == safeid) {
			return ESafeOwner_Family;
		}
	}
	for(new i=0;i<sizeof(HouseSafes);i++) {
		if(HouseSafes[i][ESafeSQLID] == safeid) {
			return ESafeOwner_House;
		}
	}
	return ESafeOwnerType:-1;
}
FamilyHasSafe(id) {
	return FamilySafes[id][ESafeSQLID] != 0;
}
HouseHasSafe(id) {
	return HouseSafes[id][ESafeSQLID] != 0;
}
DeleteHouseSafe(id) {
	DeleteSafe(ESafeOwner_House,HouseSafes[id][ESafeSQLID]);
}
DeleteFamilySafe(id) {
	DeleteSafe(ESafeOwner_Family,FamilySafes[id][ESafeSQLID]);
}
safesOnPlayerEditObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	#pragma unused rx
	#pragma unused ry
	query[0] = 0;//[128];
	new house, safeid;
	if(response != EDIT_RESPONSE_FINAL) {
		return 0;
	}
	new index;
	if((index = findSafeByObjID(objectid, house, safeid)) != -1) {
		//printf("found safe: %d %d %d %d || %d %d\n",index,objectid,house,safeid,playerid,GetPVarInt(playerid, "Faction"));
		if(house == 0) {
			new rank = GetPVarInt(playerid, "Rank")-1;
			new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
			if(family != -1) {
				new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
				if(~rankperms & EFamilyPerms_CanAdjust || (safeid != FamilySafes[family][ESafeSQLID])) {
					SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
					return 1;
				}
				if(!IsPlayerInRangeOfPoint(playerid, 5.0, FamilySafes[family][ESafeX], FamilySafes[family][ESafeY], FamilySafes[family][ESafeZ])) {
					SendClientMessage(playerid, X11_TOMATO_2, "You aren't near the safe!");
					return 1;
				}
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This isn't your safe!");
				return 1;
			}
			FamilySafes[index][ESafeX] = x;
			FamilySafes[index][ESafeY] = y;
			FamilySafes[index][ESafeZ] = z;
			FamilySafes[index][ESafeAngle] = rz;
			SendClientMessage(playerid, X11_TOMATO_2, "Safe position saved!");
			FamilyMessage(index, X11_YELLOW, "The family safe has been moved");
			format(query, sizeof(query), "UPDATE `safes` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f WHERE `id` = %d",x,y,z,rz,FamilySafes[safeid][ESafeSQLID]);
		} else {
			if(!playerOwnsHouse(index, playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "This isn't your house safe!");
				return 1;
			}
			HouseSafes[index][ESafeX] = x;
			HouseSafes[index][ESafeY] = y;
			HouseSafes[index][ESafeZ] = z;
			HouseSafes[index][ESafeAngle] = rz;
			SendClientMessage(playerid, X11_TOMATO_2, "Safe position saved!");
			format(query, sizeof(query), "UPDATE `safes` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f WHERE `id` = %d",x,y,z,rz,HouseSafes[safeid][ESafeSQLID]);
		}
		SetDynamicObjectPos(objectid, x, y, z);
		SetDynamicObjectRot(objectid, 0.0, 0.0, rz);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	}
	return 1;
}
safesOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#if debug
		format(query, sizeof(query), "DBG: safesOnPlayerSelectObject(%d,%d,%d,%f,%f,%f)", playerid, objectid, modelid, x, y, z);
		printf(query);
	#else
		#pragma unused x
		#pragma unused y
		#pragma unused z
		#pragma unused modelid
	#endif
	new house, safeid;
	new family = -1;
	new movingsafe = GetPVarInt(playerid, "MovingSafe");
	DeletePVar(playerid, "MovingSafe");
	if(findSafeByObjID(objectid, house, safeid) != -1) {
		if(house == 0) {
			new rank = GetPVarInt(playerid, "Rank")-1;
			family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
			if(family != -1) {
				new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
				if(~rankperms & EFamilyPerms_CanAdjust || (safeid != FamilySafes[family][ESafeSQLID])) {
					SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
					return 1;
				}
				if(!IsPlayerInRangeOfPoint(playerid, 5.0, FamilySafes[family][ESafeX], FamilySafes[family][ESafeY], FamilySafes[family][ESafeZ])) {
					SendClientMessage(playerid, X11_TOMATO_2, "You aren't near the safe!");
					return 1;
				}
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This isn't your safe!");
				return 1;
			}
		}
		if(movingsafe == 1) {
			EditDynamicObject(playerid, objectid);
		} else if(movingsafe == 2) {
			if(house == 0) {
				DeleteSafe(ESafeOwner_Family,safeid);
				FamilyMessage(family, X11_YELLOW, "The family safe has been deleted");
			} else {
				new houseid = getStandingHouse(playerid);
				if(!playerOwnsHouse(houseid, playerid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "This isn't your safe!");
					return 1;
				}
				SendClientMessage(playerid, X11_TOMATO_2, "Your safe has been deleted.");
				DeleteSafe(ESafeOwner_House,safeid);
			}
			CancelEdit(playerid);
		} else if(movingsafe == 3) {
			showUpgradeSafeMenu(playerid, safeid, house);
			CancelEdit(playerid);
		}
	}
	return 0;	
}
showUpgradeSafeMenu(playerid, safeid, house) {
	dialogstr[0] = 0;
	new type = getSafeType(safeid);
	for(new i=0;i<sizeof(SafeTypes);i++) {
		format(tempstr, sizeof(tempstr), "{%s}%s - $%s\n",getColourString(type==i?COLOR_LIGHTRED:COLOR_LIGHTGREEN),SafeTypes[i][ESafeTypeName],getNumberString(SafeTypes[i][ESafeTypePrice]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	SetPVarInt(playerid, "SafeUpgradeID",safeid);
	SetPVarInt(playerid, "IsHouseSafe", house);
	ShowPlayerDialog(playerid, EFamilyDialog_UpgradeSafe, DIALOG_STYLE_LIST, "{00BFFF}Safe Upgrade",dialogstr, "Buy", "Cancel");
}
handleUpgradeSafe(playerid, listitem) {
	new safe = GetPVarInt(playerid, "SafeUpgradeID");
	new type = getSafeType(safe);
	if(listitem <= type) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot downgrade your safe!");
		return 1;
	}
	new price = SafeTypes[listitem][ESafeTypePrice];
	if(GetMoneyEx(playerid) < price) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
		return 1;
	}
	GiveMoneyEx(playerid, -price);
	setSafeType(safe, type);
	DeletePVar(playerid, "SafeUpgradeID");
	DeletePVar(playerid, "IsHouseSafe");
	return 0;
}
setSafeType(safe, type) {
	new house, safeid;
	findSafeBySQLID(safe, house, safeid);
	format(query, sizeof(query), "UPDATE `safes` SET `safetype` = %d WHERE `id` = %d",type,safe);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	if(house) {
	} else {
		FamilySafes[safeid][ESafeType] = type;
	}
}
findSafeByObjID(objid, &house, &safeid) {
	for(new i=0;i<sizeof(FamilySafes);i++) {
		if(FamilySafes[i][ESafeObjectID] == objid) {
			house = 0;
			safeid = FamilySafes[i][ESafeSQLID];
			return i;
		}
	}
	for(new i=0;i<sizeof(HouseSafes);i++) {
		if(HouseSafes[i][ESafeObjectID] == objid) {
			house = 1;
			safeid = HouseSafes[i][ESafeSQLID];
			return i;
		}
	}
	safeid = -1;
	house = -1;
	return -1;
}
findSafeBySQLID(sqlid, &house, &safeid) {
	for(new i=0;i<sizeof(FamilySafes);i++) {
		if(FamilySafes[i][ESafeSQLID] == sqlid) {
			house = 0;
			safeid = i;
			return i;
		}
	}
	for(new i=0;i<sizeof(HouseSafes);i++) {
		if(HouseSafes[i][ESafeSQLID] == sqlid) {
			house = 1;
			safeid = i;
			return i;
		}
	}
	safeid = -1;
	house = -1;
	return -1;
	
}