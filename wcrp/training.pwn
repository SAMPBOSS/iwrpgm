#define GYM_INTERIOR 5
new PlayerTraining[MAX_PLAYERS];
new PlayerLap[MAX_PLAYERS];
new BikeID[MAX_PLAYERS];
trainingOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	PlayerTraining[playerid] = 0;
	PlayerLap[playerid] = 0;
	if(BikeID[playerid] != 0) {
		DestroyVehicle(BikeID[playerid]);
		BikeID[playerid] = 0;
	}
}
isAtGym(playerid) {
	if(IsPlayerInRangeOfPoint(playerid, 30.0, 772.174987,-2.430534,1000.729064) && GetPlayerInterior(playerid) == GYM_INTERIOR) {
        return 1;
    }
	return 0;
}
trainingOnPlayerEnterRacePoint(playerid) {
	PlayerPlaySound(playerid,1057,0.0,0.0,0.0);
	new training = GetPVarInt(playerid, "TrainingCredits");
	if(IsPlayerInAnyVehicle(playerid) && BikeID[playerid] == 0) {
		DisablePlayerRaceCheckpoint(playerid);
	}
	if(PlayerLap[playerid] > 0)
	{
		if(PlayerLap[playerid] == 1){ PlayerLap[playerid] = 2; SetPlayerRaceCheckpoint(playerid, 0, 2380.18, -1724.08, 13.55, 2480.43, -1723.69, 13.55, 4.0); }
		else if(PlayerLap[playerid] == 2){ PlayerLap[playerid] = 3; SetPlayerRaceCheckpoint(playerid, 0, 2480.43, -1723.69, 13.55, 2464.66, -1667.43, 13.47, 4.0); }
		else if(PlayerLap[playerid] == 3){ PlayerLap[playerid] = 4; SetPlayerRaceCheckpoint(playerid, 0, 2464.66, -1667.43, 13.47, 2324.14, -1666.98, 13.98, 4.0); }
		else if(PlayerLap[playerid] == 4){ PlayerLap[playerid] = 5; SetPlayerRaceCheckpoint(playerid, 0, 2324.14, -1666.98, 13.98, 2235.06, -1662.78, 15.47, 4.0); }
		else if(PlayerLap[playerid] == 5){ PlayerLap[playerid] = 6; SetPlayerRaceCheckpoint(playerid, 0, 2235.06, -1662.78, 15.47, 2225.18, -1724.03, 13.56, 4.0); }
		else if(PlayerLap[playerid] == 6){ PlayerLap[playerid] = 7; SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0); }
		else if(PlayerLap[playerid] == 7)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 9) { PlayerLap[playerid] = 10; SetPlayerRaceCheckpoint(playerid, 0, 2176.17, -1744.61, 13.15, 2102.80, -1744.19, 13.16, 4.0); }
		else if(PlayerLap[playerid] == 10){ PlayerLap[playerid] = 11; SetPlayerRaceCheckpoint(playerid, 0, 2102.80, -1744.19, 13.16, 2089.29, -1886.46, 13.15, 4.0); }
		else if(PlayerLap[playerid] == 11){ PlayerLap[playerid] = 12; SetPlayerRaceCheckpoint(playerid, 0, 2089.29, -1886.46, 13.15, 2225.96, -1885.86, 13.15, 4.0); }
		else if(PlayerLap[playerid] == 12){ PlayerLap[playerid] = 13; SetPlayerRaceCheckpoint(playerid, 0, 2225.96, -1885.86, 13.15, 2225.18, -1724.03, 13.56, 4.0); }
		else if(PlayerLap[playerid] == 13){ PlayerLap[playerid] = 14; SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0); }
		else if(PlayerLap[playerid] == 14)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 20){ PlayerLap[playerid] = 21; SetPlayerRaceCheckpoint(playerid, 0, 2405.63, -1756.79, 13.15, 2405.49, -1964.77, 13.15, 4.0);}
		else if(PlayerLap[playerid] == 21){ PlayerLap[playerid] = 22; SetPlayerRaceCheckpoint(playerid, 0, 2405.49, -1964.77, 13.15, 2321.54, -1964.95, 13.17, 4.0);}
		else if(PlayerLap[playerid] == 22){ PlayerLap[playerid] = 23; SetPlayerRaceCheckpoint(playerid, 0, 2321.54, -1964.95, 13.17, 2305.55, -1902.61, 13.22, 4.0);}
		else if(PlayerLap[playerid] == 23){ PlayerLap[playerid] = 24; SetPlayerRaceCheckpoint(playerid, 0, 2305.55, -1902.61, 13.22, 2226.35, -1885.74, 13.15, 4.0);}
		else if(PlayerLap[playerid] == 24){ PlayerLap[playerid] = 25; SetPlayerRaceCheckpoint(playerid, 0, 2226.35, -1885.74, 13.15, 2225.18, -1724.03, 13.56, 4.0);}
		else if(PlayerLap[playerid] == 25){ PlayerLap[playerid] = 26, SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 26)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 30){ PlayerLap[playerid] = 31; SetPlayerRaceCheckpoint(playerid, 0, 2208.01, -1636.53, 15.05, 2219.06, -1492.17, 23.60, 4.0);}
		else if(PlayerLap[playerid] == 31){ PlayerLap[playerid] = 32; SetPlayerRaceCheckpoint(playerid, 0, 2219.06, -1492.17, 23.60, 2334.56, -1492.51, 23.61, 4.0);}
		else if(PlayerLap[playerid] == 32){ PlayerLap[playerid] = 33; SetPlayerRaceCheckpoint(playerid, 0, 2334.56, -1492.51, 23.61, 2334.15, -1555.94, 23.60, 4.0);}
		else if(PlayerLap[playerid] == 33){ PlayerLap[playerid] = 34; SetPlayerRaceCheckpoint(playerid, 0, 2334.15, -1555.94, 23.60, 2336.79, -1723.33, 13.11, 4.0);}
		else if(PlayerLap[playerid] == 34){ PlayerLap[playerid] = 35; SetPlayerRaceCheckpoint(playerid, 0, 2336.79, -1723.33, 13.11, 2225.18, -1724.03, 13.56, 4.0);}
		else if(PlayerLap[playerid] == 35){ PlayerLap[playerid] = 36; SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 36)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 40){ PlayerLap[playerid] = 41; SetPlayerRaceCheckpoint(playerid, 0, 2177.95, -1743.07, 13.15, 2088.77, -1738.14, 13.15, 4.0);}
		else if(PlayerLap[playerid] == 41){ PlayerLap[playerid] = 42; SetPlayerRaceCheckpoint(playerid, 0, 2088.77, -1738.14, 13.15, 2009.94, -1664.04, 13.14, 4.0);}
		else if(PlayerLap[playerid] == 42){ PlayerLap[playerid] = 43; SetPlayerRaceCheckpoint(playerid, 0, 2009.94, -1664.04, 13.14, 2009.88, -1620.78, 13.14, 4.0);}
		else if(PlayerLap[playerid] == 43){ PlayerLap[playerid] = 44; SetPlayerRaceCheckpoint(playerid, 0, 2009.88, -1620.78, 13.14, 2107.99, -1622.23, 13.16, 4.0);}
		else if(PlayerLap[playerid] == 44){ PlayerLap[playerid] = 45; SetPlayerRaceCheckpoint(playerid, 0, 2107.99, -1622.23, 13.16, 2225.18, -1724.03, 13.56, 4.0);}
		else if(PlayerLap[playerid] == 45){ PlayerLap[playerid] = 46; SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 46)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}//SWIMMING
		else if(PlayerLap[playerid] == 50){ PlayerLap[playerid] = 51; SetPlayerRaceCheckpoint(playerid, 0, 723.68, -1581.30, -0.38, 723.55, -1758.44, -0.63, 4.0);}
		else if(PlayerLap[playerid] == 51){ PlayerLap[playerid] = 52; SetPlayerRaceCheckpoint(playerid, 0, 723.55, -1758.44, -0.63, 722.97, -1923.62, -0.25, 4.0);}
		else if(PlayerLap[playerid] == 52){ PlayerLap[playerid] = 53; SetPlayerRaceCheckpoint(playerid, 0, 722.97, -1923.62, -0.25, 654.34, -2006.30, -0.70, 4.0);}
		else if(PlayerLap[playerid] == 53){ PlayerLap[playerid] = 54; SetPlayerRaceCheckpoint(playerid, 0, 654.34, -2006.30, -0.70, 510.43, -1991.50, -0.65, 4.0);}
		else if(PlayerLap[playerid] == 54){ PlayerLap[playerid] = 55; SetPlayerRaceCheckpoint(playerid, 0, 510.43, -1991.50, -0.65, 406.07, -1988.61, -0.63, 4.0);}
		else if(PlayerLap[playerid] == 55){ PlayerLap[playerid] = 56; SetPlayerRaceCheckpoint(playerid, 0, 406.07, -1988.61, -0.63, 163.42, -1994.19, -0.67, 4.0);}
		else if(PlayerLap[playerid] == 56){ PlayerLap[playerid] = 57; SetPlayerRaceCheckpoint(playerid, 0, 163.42, -1994.19, -0.67, -41.54, -1929.31, -0.53, 4.0);}
		else if(PlayerLap[playerid] == 57){ PlayerLap[playerid] = 58; SetPlayerRaceCheckpoint(playerid, 0, -41.54, -1929.31, -0.53, -248.25, -1881.23, -0.71, 4.0);}
		else if(PlayerLap[playerid] == 58){ PlayerLap[playerid] = 59; SetPlayerRaceCheckpoint(playerid, 0, -248.25, -1881.23, -0.71, -387.96, -1843.28, 2.01, 4.0);}
		else if(PlayerLap[playerid] == 59){ PlayerLap[playerid] = 60; SetPlayerRaceCheckpoint(playerid, 1, -387.96, -1843.28, 2.01, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 60)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid,5);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 70){ PlayerLap[playerid] = 71; SetPlayerRaceCheckpoint(playerid, 0, 725.97, -1665.36, -0.71, 723.33, -1773.81, -0.38, 4.0);}
		else if(PlayerLap[playerid] == 71){ PlayerLap[playerid] = 72; SetPlayerRaceCheckpoint(playerid, 0, 723.33, -1773.81, -0.38, 725.53, -1911.77, -0.60, 4.0);}
		else if(PlayerLap[playerid] == 72){ PlayerLap[playerid] = 73; SetPlayerRaceCheckpoint(playerid, 0, 725.53, -1911.77, -0.60, 800.43, -2046.08, -0.27, 4.0);}
		else if(PlayerLap[playerid] == 73){ PlayerLap[playerid] = 74; SetPlayerRaceCheckpoint(playerid, 0, 800.43, -2046.08, -0.27, 891.69, -2166.94, -0.68, 4.0);}
		else if(PlayerLap[playerid] == 74){ PlayerLap[playerid] = 75; SetPlayerRaceCheckpoint(playerid, 0, 891.69, -2166.94, -0.68, 972.17, -2302.19, -0.77, 4.0);}
		else if(PlayerLap[playerid] == 75){ PlayerLap[playerid] = 76; SetPlayerRaceCheckpoint(playerid, 0, 972.17, -2302.19, -0.77, 1120.29, -2449.89, -0.46, 4.0);}
		else if(PlayerLap[playerid] == 76){ PlayerLap[playerid] = 77; SetPlayerRaceCheckpoint(playerid, 0, 1120.29, -2449.89, -0.46, 1244.42, -2628.34, -0.58, 4.0);}
		else if(PlayerLap[playerid] == 77){ PlayerLap[playerid] = 78; SetPlayerRaceCheckpoint(playerid, 1, 1244.42, -2628.34, -0.58, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 78)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid,5);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 90){ PlayerLap[playerid] = 91; SetPlayerRaceCheckpoint(playerid, 0, -527.55, 1635.27, -0.54, -625.71, 1308.54, -0.53, 4.0);}
		else if(PlayerLap[playerid] == 91){ PlayerLap[playerid] = 92; SetPlayerRaceCheckpoint(playerid, 0, -625.71, 1308.54, -0.53, -429.27, 1163.16, -0.47, 4.0);}
		else if(PlayerLap[playerid] == 92){ PlayerLap[playerid] = 93; SetPlayerRaceCheckpoint(playerid, 0, -429.27, 1163.16, -0.47, -661.67, 876.49, -0.58, 4.0);}
		else if(PlayerLap[playerid] == 93){ PlayerLap[playerid] = 94; SetPlayerRaceCheckpoint(playerid, 1, -661.67, 876.49, -0.58, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 94)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid,5);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 120){ PlayerLap[playerid] = 121; SetPlayerRaceCheckpoint(playerid, 0, 724.84, -1886.89, -0.30, 735.10, -2050.66, -0.15, 4.0);}
		else if(PlayerLap[playerid] == 121){ PlayerLap[playerid] = 122; SetPlayerRaceCheckpoint(playerid, 0, 735.10, -2050.66, -0.15, 696.32, -2240.57, -0.18, 4.0);}
		else if(PlayerLap[playerid] == 122){ PlayerLap[playerid] = 123; SetPlayerRaceCheckpoint(playerid, 0, 696.32, -2240.57, -0.18, 600.09, -2315.52, -0.17, 4.0);}
		else if(PlayerLap[playerid] == 123){ PlayerLap[playerid] = 124; SetPlayerRaceCheckpoint(playerid, 0, 600.09, -2315.52, -0.17, 540.19, -2270.03, -0.46, 4.0);}
		else if(PlayerLap[playerid] == 124){ PlayerLap[playerid] = 125; SetPlayerRaceCheckpoint(playerid, 0, 540.19, -2270.03, -0.46, 604.89, -2119.15, -0.40, 4.0);}
		else if(PlayerLap[playerid] == 125){ PlayerLap[playerid] = 126; SetPlayerRaceCheckpoint(playerid, 0, 604.89, -2119.15, -0.40, 701.19, -1974.65, 0.61, 4.0);}
		else if(PlayerLap[playerid] == 126){ PlayerLap[playerid] = 127; SetPlayerRaceCheckpoint(playerid, 0, 701.19, -1974.65, 0.61, 732.22, -1800.14, -0.34, 4.0);}
		else if(PlayerLap[playerid] == 127){ PlayerLap[playerid] = 128; SetPlayerRaceCheckpoint(playerid, 0, 732.22, -1800.14, -0.34, 723.75, -1690.14, -0.56, 4.0);}
		else if(PlayerLap[playerid] == 128){ PlayerLap[playerid] = 129; SetPlayerRaceCheckpoint(playerid, 0, 723.75, -1690.14, -0.56, 717.50, -1498.87, -0.40, 4.0);}
		else if(PlayerLap[playerid] == 129){ PlayerLap[playerid] = 130; SetPlayerRaceCheckpoint(playerid, 1, 717.50, -1498.87, -0.40, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 130)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid,5);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
		else if(PlayerLap[playerid] == 140){ PlayerLap[playerid] = 141; SetPlayerRaceCheckpoint(playerid, 0, 2627.02, -1732.55, 10.67, 2773.88, -1657.55, 11.08, 4.0);}
		else if(PlayerLap[playerid] == 141){ PlayerLap[playerid] = 142; SetPlayerRaceCheckpoint(playerid, 0, 2773.88, -1657.55, 11.08, 2944.32, -1419.97, 10.30, 4.0);}
		else if(PlayerLap[playerid] == 142){ PlayerLap[playerid] = 143; SetPlayerRaceCheckpoint(playerid, 0, 2944.32, -1419.97, 10.30, 2852.65, -1131.13, 24.50, 4.0);}
		else if(PlayerLap[playerid] == 143){ PlayerLap[playerid] = 144; SetPlayerRaceCheckpoint(playerid, 0, 2852.65, -1131.13, 24.50, 2737.12, -1043.30, 52.85, 4.0);}
		else if(PlayerLap[playerid] == 144){ PlayerLap[playerid] = 145; SetPlayerRaceCheckpoint(playerid, 0, 2737.12, -1043.30, 52.85, 2593.85, -987.18, 79.89, 4.0);}
		else if(PlayerLap[playerid] == 145){ PlayerLap[playerid] = 146; SetPlayerRaceCheckpoint(playerid, 0, 2593.85, -987.18, 79.89, 2391.22, -977.11, 71.93, 4.0);}
		else if(PlayerLap[playerid] == 146){ PlayerLap[playerid] = 147; SetPlayerRaceCheckpoint(playerid, 0, 2391.22, -977.11, 71.93, 2252.46, -1042.76, 52.69, 4.0);}
		else if(PlayerLap[playerid] == 147){ PlayerLap[playerid] = 148; SetPlayerRaceCheckpoint(playerid, 0, 2252.46, -1042.76, 52.69, 2049.59, -985.40, 44.71, 4.0);}
		else if(PlayerLap[playerid] == 148){ PlayerLap[playerid] = 149; SetPlayerRaceCheckpoint(playerid, 0, 2049.59, -985.40, 44.71, 1973.44, -1213.45, 24.86, 4.0);}
		else if(PlayerLap[playerid] == 149){ PlayerLap[playerid] = 150; SetPlayerRaceCheckpoint(playerid, 0, 1973.44, -1213.45, 24.86, 1970.36, -1282.82, 23.63, 4.0);}
		else if(PlayerLap[playerid] == 150){ PlayerLap[playerid] = 151; SetPlayerRaceCheckpoint(playerid, 0, 1970.36, -1282.82, 23.63, 1845.89, -1451.75, 13.01, 4.0);}
		else if(PlayerLap[playerid] == 151){ PlayerLap[playerid] = 152; SetPlayerRaceCheckpoint(playerid, 0, 1845.89, -1451.75, 13.01, 1822.63, -1721.37, 12.99, 4.0);}
		else if(PlayerLap[playerid] == 152){ PlayerLap[playerid] = 153; SetPlayerRaceCheckpoint(playerid, 0, 1822.63, -1721.37, 12.99, 1911.28, -1773.67, 12.98, 4.0);}
		else if(PlayerLap[playerid] == 153){ PlayerLap[playerid] = 154; SetPlayerRaceCheckpoint(playerid, 0, 1911.28, -1773.67, 12.98, 2075.23, -1813.79, 12.99, 4.0);}
		else if(PlayerLap[playerid] == 154){ PlayerLap[playerid] = 155; SetPlayerRaceCheckpoint(playerid, 0, 2075.23, -1813.79, 12.99, 2171.02, -1752.79, 12.98, 4.0);}
		else if(PlayerLap[playerid] == 155){ PlayerLap[playerid] = 156; SetPlayerRaceCheckpoint(playerid, 0, 2171.02, -1752.79, 12.98, 2225.18, -1724.03, 13.56, 4.0);}
		else if(PlayerLap[playerid] == 156){ PlayerLap[playerid] = 157; SetPlayerRaceCheckpoint(playerid, 1, 2225.18, -1724.03, 13.56, 0.0, 0.0, 0.0, 4.0);}
		else if(PlayerLap[playerid] == 157)
		{
		    PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
			RemovePlayerFromVehicle(GetPlayerVehicleID(playerid));
			DestroyVehicle(BikeID[playerid]);
			BikeID[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
			SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid,5);
			SendClientMessage(playerid, COLOR_LIGHTBLUE,"Training Completed! You earned 1 Training Credit.");
			training += 1;
		}
	}
	SetPVarInt(playerid, "TrainingCredits", training);
}
YCMD:training(playerid, cmdtext[], help)
{
	new training = GetPVarInt(playerid, "TrainingCredits");
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Training commands");
		return 1;
	}
    if(IsPlayerInRangeOfPoint(playerid,20.0,765.9343,0.2761,1000.7173))
	{
	    new idx;
		new x_nr[96];
		new string[128];
		x_nr = strtok(cmdtext, idx);
		if(isnull(x_nr))
		{
			SendClientMessage(playerid, COLOR_DARKGREEN, "|__________________ Training __________________|");
			SendClientMessage(playerid, COLOR_DARKGREEN, "USAGE: /Training [name]");
			SendClientMessage(playerid, COLOR_DARKGREEN, "Available Sports: Running, Biking, Climbing, Swimming");
			SendClientMessage(playerid, COLOR_DARKGREEN, "Available Names: Stats, Style, Stop, Help");
			SendClientMessage(playerid, COLOR_DARKGREEN, "|___________________________________________|");
			return 1;
		}
		if(strcmp(x_nr,"help",true) == 0)
		{
			SendClientMessage(playerid, X11_ORANGE, "With Training you can gain Trainingcredits.");
			SendClientMessage(playerid, X11_ORANGE, "With these Credits you can buy upgrades.");
			SendClientMessage(playerid, X11_ORANGE, "Cheating is not allowed!");
			SendClientMessage(playerid, X11_ORANGE, "While jogging/climbing/swimming don't enter any vehicles!");
			SendClientMessage(playerid, X11_ORANGE, "If you leave your Bike while biking for too long, you will be canceled!");
		}
		else if(strcmp(x_nr,"running", true) == 0)
		{
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "Run the marked Route.");
			new route;
			route = random(6);
			PlayerTraining[playerid] = 1;
			SetPlayerPos(playerid, 2225.18, -1724.03, 13.56);
			SetCameraBehindPlayer(playerid);
			SetPlayerInterior(playerid, 0);
			if(route <= 1)
			{
				SetPlayerRaceCheckpoint(playerid, 0, 2299.34, -1723.95, 13.53, 2380.18, -1724.08, 13.55, 4.0);
				PlayerLap[playerid] = 1;
			}
			else if(route == 2)
			{
				SetPlayerRaceCheckpoint(playerid, 0, 2176.17, -1744.61, 13.15, 2102.80, -1744.19, 13.16, 4.0);
				PlayerLap[playerid] = 10;
			}
			else if(route == 3)
			{
				SetPlayerRaceCheckpoint(playerid, 0, 2405.63, -1756.79, 13.15, 2405.49, -1964.77, 13.15, 4.0);
				PlayerLap[playerid] = 20;
			}
			else if(route == 4)
			{
				SetPlayerRaceCheckpoint(playerid, 0, 2208.01, -1636.53, 15.05, 2219.06, -1492.17, 23.60, 4.0);
				PlayerLap[playerid] = 30;
			}
			else if(route >= 5)
			{
				SetPlayerRaceCheckpoint(playerid, 0, 2177.95, -1743.07, 13.15, 2088.77, -1738.14, 13.15, 4.0);
				PlayerLap[playerid] = 40;
			}
		}
		else if(strcmp(x_nr,"swimming", true) == 0)
		{
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "Swim the marked Route.");
			new route;
			route = random(4);
			PlayerTraining[playerid] = 1;
			SetPlayerInterior(playerid, 0);
			if(route <= 1)
			{
				SetPlayerPos(playerid, 723.39, -1495.00, 1.93);
				SetCameraBehindPlayer(playerid);
				SetPlayerRaceCheckpoint(playerid, 0, 723.68, -1581.30, -0.38, 723.55, -1758.44, -0.63, 4.0);
				PlayerLap[playerid] = 50;
			}
			else if(route == 2)
			{
				SetPlayerPos(playerid, 716.08, -1628.88, 2.42);
				SetCameraBehindPlayer(playerid);
				SetPlayerRaceCheckpoint(playerid, 0, 725.97, -1665.36, -0.71, 723.33, -1773.81, -0.38, 4.0);
				PlayerLap[playerid] = 70;
			}
			else if(route == 3)
			{
				SetPlayerPos(playerid, -794.89, 1812.47, -0.57);
				SetCameraBehindPlayer(playerid);
				SetPlayerRaceCheckpoint(playerid, 0, -527.55, 1635.27, -0.54, -625.71, 1308.54, -0.53, 4.0);
				PlayerLap[playerid] = 90;
			}
			else if(route >= 4)
			{
				SetPlayerPos(playerid, 715.25, -1694.83, 2.42);
				SetCameraBehindPlayer(playerid);
				SetPlayerRaceCheckpoint(playerid, 0, 715.25, -1694.83, 2.42, 724.84, -1886.89, -0.30, 4.0);
				PlayerLap[playerid] = 120;
			}
		}
		else if(strcmp(x_nr,"biking", true) == 0)
		{
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "Bike the marked Route.");
			//new route;
			//route = random(2);
			PlayerTraining[playerid] = 1;
			SetPlayerPos(playerid, 2225.18, -1724.03, 13.56);
			SetPlayerInterior(playerid, 0);
			new bikeid;
			bikeid = CreateVehicle(510, 2225.18, -1724.03, 13.56, -90.0, -1, -1,  900);
			PutPlayerInVehicleEx(playerid, bikeid, 0);
			BikeID[playerid] = bikeid;
			SetCameraBehindPlayer(playerid);
			//if(route <= 1)
			//{
			SetPlayerRaceCheckpoint(playerid, 0, 2421.71, -1724.57, 13.39, 2627.02, -1732.55, 10.67, 4.0);
			PlayerLap[playerid] = 140;
		}
		else if(strcmp(x_nr,"stats", true) == 0)
		{
			format(string,sizeof(string),"Current Training Credits: %d ", GetPVarInt(playerid, "TrainingCredits"));
			SendClientMessage(playerid, COLOR_LIGHTBLUE,string);
		}
		else if(strcmp(x_nr,"stop", true) == 0)
		{
			if(PlayerLap[playerid] > 50)
			{
				if(BikeID[playerid] > 0) { DestroyVehicle(BikeID[playerid]); BikeID[playerid] = 0; }
				DisablePlayerRaceCheckpoint(playerid);
				//SetPlayerPos(playerid,772.174987,-2.430534,1000.729064);
				//SetCameraBehindPlayer(playerid);
				//SetPlayerInterior(playerid,5);
				//PlayerInfo[playerid][pInt] = 5;
				//SendClientMessage(playerid, COLOR_YELLOW," You take a Taxi to get back to the Gym.");
				return 1;
			}
			DisablePlayerCheckpoint(playerid);
			SendClientMessage(playerid, COLOR_LIGHTRED, "Training Canceled! Reason: Stopped");
			PlayerTraining[playerid] = 0;
			PlayerLap[playerid] = 0;
		}
		else if(strcmp(x_nr,"style", true) == 0)
		{
			new x_item[256];
			x_item = strtok(cmdtext, idx);
			if(isnull(x_item))
			{
				SendClientMessage(playerid, COLOR_DARKGREEN, "{00FF00}USAGE: /Training Style {FF0000}[Name]");
				SendClientMessage(playerid, COLOR_DARKGREEN, "{00FF00}You can get:");
				SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}Normal{FFFFFF} | {00FF00}Cost: {FF0000}0 {00FF00}Training Credits.");
				SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}Boxing{FFFFFF} | {00FF00}Cost: {FF0000}3 {00FF00}Training Credits.");
				SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}KungFu{FFFFFF} | {00FF00}Cost: {FF0000}4 {00FF00}Training Credits.");
				//SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}Kneehead{FFFFFF} | {00FF00}Cost: {FF0000}5 {00FF00}Training Credits.");
				//SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}Grabkick{FFFFFF} | {00FF00}Cost: {FF0000}6 {00FF00}Training Credits."); 
				SendClientMessage(playerid, COLOR_DARKGREEN, "{FF0000}Elbow{FFFFFF} | {00FF00}Cost: {FF0000}7 {00FF00}Training Credits.");
				return 1;
			}
			if(strcmp(x_item,"normal", true) ==0) { SetPlayerFightingStyle(playerid, 4); SetPVarInt(playerid, "FightStyle", 4); }
			if(strcmp(x_item,"boxing", true) ==0)
			{
				if(training >= 3)
				{
					SetPlayerFightingStyle(playerid, 5);
					SetPVarInt(playerid, "FightStyle", 5);
					training -= 3;
					SendClientMessage(playerid, COLOR_DARKGREEN, "You are now using the 'Boxing' Style");
				}
				else { SendClientMessage(playerid, COLOR_LIGHTRED, "	You don't have enough Traning Credits."); }
			}
			else if(strcmp(x_item,"kungfu", true) ==0)
			{
				if(training >= 4)
				{
					SetPlayerFightingStyle(playerid, 6);
					SetPVarInt(playerid, "FightStyle", 6);
					training -= 4;
					SendClientMessage(playerid, COLOR_DARKGREEN, "You are now using the 'KungFu' Style");
				}
				else { SendClientMessage(playerid, COLOR_LIGHTRED, "	You don't have enough Traning Credits."); }
			}
			/*
			else if(strcmp(x_item,"kneehead", true) ==0)
			{
				if(training >= 5)
				{
					SetPlayerFightingStyle(playerid, 7);
					SetPVarInt(playerid, "FightStyle", 7);
					training -= 5;
					SendClientMessage(playerid, COLOR_DARKGREEN, "You are now using the 'KneeHead' Style");
				}
				else { SendClientMessage(playerid, COLOR_LIGHTRED, "	You don't have enough Traning Credits."); }
			}
			else if(strcmp(x_item,"grabkick", true) ==0)
			{
				if(training >= 6)
				{
					SetPlayerFightingStyle(playerid, 15);
					SetPVarInt(playerid, "FightStyle", 15);
					training -= 6;
					SendClientMessage(playerid, COLOR_DARKGREEN, "You are now using the 'GrabKick' Style");
				}
				else { SendClientMessage(playerid, COLOR_LIGHTRED, "	You don't have enough Traning Credits."); }
			}
			*/
			else if(strcmp(x_item,"elbow", true) ==0)
			{
				if(training >= 7)
				{
					SetPlayerFightingStyle(playerid, 26);
					SetPVarInt(playerid, "FightStyle", 26);
					training -= 7;
					SendClientMessage(playerid, COLOR_DARKGREEN, "You are now using the 'Elbow' Style");
				}
				else { SendClientMessage(playerid, COLOR_LIGHTRED, "	You don't have enough Traning Credits."); }
			}
		}
	}
	else
	{
		SendClientMessage(playerid, COLOR_LIGHTRED,"	You have to be at Ganton Gym!");
	}
	SetPVarInt(playerid, "TrainingCredits", training);
	return 1;
}
trainingOnPlayerDeath(playerid) {
	if(BikeID[playerid] > 0)
	{
	    DestroyVehicle(BikeID[playerid]);
		BikeID[playerid] = 0;
	}
}
trainingOnPlayerStateChange(playerid, newstate, oldstate) {
	#pragma unused newstate
	if(oldstate == PLAYER_STATE_DRIVER)
	{
		if(PlayerTraining[playerid] > 0)
		{
			SendClientMessage(playerid, COLOR_LIGHTRED, "	Training Canceled! Reason: Loss of bike!");
			DestroyVehicle(BikeID[playerid]);
			BikeID[playerid] = 0;
			PlayerTraining[playerid] = 0;
			DisablePlayerRaceCheckpoint(playerid);
		}
	}
}