new FamilyColours[] =	{0xFFFFFFFF,0xE3C519FF,0xFF0000FF,0xFF80FFFF,0xFF6317FF,0xC1C100FF,0x800080FF,
						 0xFFB0FFFF,0x804040FF,0xFFD7EBFF,0xBBBB00FF,0x00D200FF,0x660033FF,0xFFFF80FF,0xFFA346FF,
						 0xFFD7FFFF,0xFFFF91FF,0xC6C6FFFF,0xD76B00FF,0xC10061FF,0xF4F400FF,0xFF9664FF,
						 0x00FF80FF,0xBF0000FF,0x008000FF,0x00ECECFF,0xC0C0C0FF,0xAA3333FF,0xC0C0C0FF,
						 0x003C00FF,0xE1E100FF,0x00AAFFFF,0x0000FFFF};
#define MAX_FAMILY_MOTD 128
#define MAX_FAMILY_NAME 32
#define NUM_RANKS 6

#define FamilyType_Legal 		1
#define FamilyType_Biker 		2
#define FamilyType_Mafia		3
#define FamilyType_StreetGang 	4

enum EFamilyPermissions (<<= 1) {
	EFamilyPerms_CanTakeSafe = 1,
	EFamilyPerms_CanGiveSafe,
	EFamilyPerms_CanDriveCar,
	EFamilyPerms_CanUninvite,
	EFamilyPerms_CanInvite,
	EFamilyPerms_CanAdjust,
	EFamilyPerms_CanSetRank,
	EFamilyPerms_CanSetRankEqual, //can set a rank equal or above theirs
	EFamilyPerms_CanParkCar,
	EFamilyPerms_CanClaim,
	EFamilyPerms_CanTakeover,
	EFamilyPerms_CanControlIBiz,
	EFamilyPerms_CanTag,
	EFamilyPerms_CanEditWallTags,
	EFamilyPerms_CanDeleteWallTags,
	EFamilyPerms_CanUseFamilyChat,
};
enum EFamilyPermFlagInfo {
	EFamilyPermissions:EFFIFlags, //flag ID
	EFFIDesc[64],//description of admin flag
};
new FamilyFlagDescription[][EFamilyPermFlagInfo] = 
{
	{EFamilyPerms_CanTakeSafe, "Can take from the safe"},
	{EFamilyPerms_CanGiveSafe, "Can put items in the safe"},
	{EFamilyPerms_CanDriveCar, "Can drive family cars(and lock/unlock them)"},
	{EFamilyPerms_CanUninvite, "Can uninvite from the family"},
	{EFamilyPerms_CanInvite, "Can invite to the family"},
	{EFamilyPerms_CanAdjust, "Can use /adjust"},
	{EFamilyPerms_CanSetRank, "Can Set rank on people"},
	{EFamilyPerms_CanSetRankEqual, "Can Uninvite higher ranks, and set ranks to equal/higher"},
	{EFamilyPerms_CanParkCar, "Can use /parkfamilycar"},
	{EFamilyPerms_CanClaim, "Can use /claim"},
	{EFamilyPerms_CanTakeover, "Can use /takeover"},
	{EFamilyPerms_CanControlIBiz, "Can control WareHouses"},
	{EFamilyPerms_CanTag, "Can Tag On Walls"},
	{EFamilyPerms_CanEditWallTags, "Can Edit Wall Tags"},
	{EFamilyPerms_CanDeleteWallTags, "Can Delete Wall Tags"},
	{EFamilyPerms_CanUseFamilyChat, "Can use /f"}
};
enum FamilyFlags (<<= 1) {
	EFamilyType_None = 0,
	EFamilyType_Legal = 1,
	EFamilyType_Biker,
	EFamilyType_Mafia,
	EFamilyType_StreetGang,
};

enum EFamilyFlagsInfo {
	FamilyFlags:EFamilyFlag, //family flag
	EFamilyFlagDesc[64],//description of the family flag
};

new FamilyFlagTypesDesc[][EFamilyFlagsInfo] = 
{
	{EFamilyType_None, "Unofficial - The family can't do anything official factions do."},
	{EFamilyType_Legal, "Legal - Gives the family some amount of cash"},
	{EFamilyType_Biker, "Biker - Allowed to take the Drug/Gun job."},
	{EFamilyType_Mafia, "Mafia - Allowed to take the Drug/Gun job."},
	{EFamilyType_StreetGang, "Streetgang - Allowed to take the drug job with no /fsafe."}
};

enum EFamiliyInfo {
	EFamilySQLID,
	EFamilyName[MAX_FAMILY_NAME],
	EFamilyMOTD[MAX_FAMILY_MOTD],
	EFamilyPermissions:EFamilyRankPerms[NUM_RANKS],
	EFamilyOwner[MAX_PLAYER_NAME],
	EFamilyColour,
	EFamilyHQID,
	Float:EFamilyHQX,
	Float:EFamilyHQY,
	Float:EFamilyHQZ,
	FamilyFlags:EFFlags,
};
#define MAX_FAMILIES 15
new FamilyRanks[MAX_FAMILIES][NUM_RANKS][MAX_FAMILY_NAME];
new Families[MAX_FAMILIES][EFamiliyInfo];

enum EFamilyHQInfo {
	EFamilyHQInfoName[32],
	Float:EFamilyHQInfoX,
	Float:EFamilyHQInfoY,
	Float:EFamilyHQInfoZ,
	EFamilyHQInfoInt,
	EFamilyHQInfoPrice,
	EFamilyHQInfoFreeze, //It's a dynamic interior, freeze the player so he / she doesn't fall
};

new FamilyHQs[][EFamilyHQInfo] = {
	{"Small House",2268.38,-1210.44,1047.74,10, 200000, 0},
	{"Small Strip Club",1212.02,-28.66,1000.95,3, 400000, 0},
	{"Medium House",2324.41,-1147.54,1050.71,12, 600000, 0},
	{"Medium Strip Club",2324.41,-1147.54,1050.71,12, 800000, 0},
	{"Large Strip Club",965.84,-53.10,1001.12,3, 1000000, 0},
	{"Big House",2317.82,-1024.75,1050.21,9, 1200000, 0},
	{"Huge Strip Club",-2637.44,1404.63,906.46,3, 1400000, 0},
	{"Penthouse",2523.83,-1285.92,1054.64,2, 1600000, 0},
	{"Biker HQ",1116.8680,1361.7433,501.0859,50, 200000, 1},
	{"Two Level HQ",310.8482,313.7412,1003.3047,4, 200000, 1},
	{"Mechanic Garage HQ",-1986.9423,178.0188,85.3039,50, 200000, 1}
};

enum {
	EFamilyDialog_SetFamColour = EFamilyDialog_Base + 1,
	EFamilyDialog_InviteDialog,
	EFamilyDialog_PermissionsEdit,
	EFamilyDialog_HQBuy,
	EFamilyDialog_BuySafe,
	EFamilyDialog_UpgradeSafe,
	EFamilyDialog_BuyHouseSafe,
	EFamilyDialog_ToggleFamilyFlags,
};

forward OnLoadFamilies();
forward OnFamilyCreate(playerid,name[],familytype);

familiesOnGameModeInit() {
	Command_AddAltNamed("familycars","fcars");
	Command_AddAltNamed("flock","familylock");
	Command_AddAltNamed("funlock","familyunlock");
	Command_AddAltNamed( "buyfamilycolour", "buyfamilycolor");
	Command_AddAltNamed( "buyfamilyhq", "buyfamilyhouse");
	loadFamilies();
}
public OnLoadFamilies() {
	new rows, fields;
	cache_get_data(rows,fields);
	
	new id_string[128];

	for(new i=0;i<rows;i++) {	
		
		cache_get_row(i,0,id_string);		
		
		Families[i][EFamilySQLID] = strval(id_string);
		
		cache_get_row(i,1,Families[i][EFamilyName]);
		
		cache_get_row(i,2, id_string);
		Families[i][EFamilyColour] = strval(id_string);
		
		cache_get_row(i,3,Families[i][EFamilyMOTD]);
		
		cache_get_row(i,4,Families[i][EFamilyOwner]);
		
		GiveNameSpace(Families[i][EFamilyOwner]);

		cache_get_row(i,5,FamilyRanks[i][0]); //rank1text 
		cache_get_row(i,6,id_string); //rank1perms
		Families[i][EFamilyRankPerms][0] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,7,FamilyRanks[i][1]); //rank2text
		cache_get_row(i,8,id_string); //rank2perms
		Families[i][EFamilyRankPerms][1] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,9,FamilyRanks[i][2]); //rank3text
		cache_get_row(i,10,id_string); //rank3perms
		Families[i][EFamilyRankPerms][2] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,11,FamilyRanks[i][3]);	//rank4text
		cache_get_row(i,12,id_string); //rank4perms
		Families[i][EFamilyRankPerms][3] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,13,FamilyRanks[i][4]); //rank5perms
		cache_get_row(i,14,id_string); //rank5perms
		Families[i][EFamilyRankPerms][4] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,15,FamilyRanks[i][5]); //rank5perms
		cache_get_row(i,16,id_string); //rank5perms
		Families[i][EFamilyRankPerms][5] = EFamilyPermissions:strval(id_string);
		
		cache_get_row(i,17,id_string); //hq
		Families[i][EFamilyHQID] = strval(id_string);
		
		cache_get_row(i,18,id_string); //hqx
		Families[i][EFamilyHQX] = floatstr(id_string);
		
		cache_get_row(i,19,id_string); //hqy
		Families[i][EFamilyHQY] = floatstr(id_string);
		
		cache_get_row(i,20,id_string); //hqz
		Families[i][EFamilyHQZ] = floatstr(id_string);
		
		cache_get_row(i,21,id_string); //Family Flags
		Families[i][EFFlags] = FamilyFlags:strval(id_string);

		LoadFamilyCars(Families[i][EFamilySQLID]);
		
	}
	loadFamilySafes();
	pointsOnGameModeInit();
}
familyOnLoadCharacter(playerid) {
	new fid = GetPVarInt(playerid, "Family");
	if(fid == 0) return 1;
	fid = FindFamilyBySQLID(fid);
	new msg[128];
	if(fid == -1) {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Your family has been deleted!");
		SetPVarInt(playerid, "Family", 0);
		SetPVarInt(playerid, "Rank", 0);
		return 1;
	} else {
		format(msg, sizeof(msg), "Family MOTD: %s",Families[fid][EFamilyMOTD]);
		SendClientMessage(playerid, FamilyColour(fid), msg);
	}
	return 1;
}
public OnFamilyCreate(playerid,name[],familytype) {
	new id = mysql_insert_id();
	new i;
	new msg[128];
	for(i=0;i<sizeof(Families);i++) {
		if(Families[i][EFamilySQLID] == 0) {
			Families[i][EFamilySQLID] = id;
			Families[i][EFamilyColour] = 0;
			Families[i][EFamilyHQID] = -1;
			Families[i][EFamilyRankPerms][5] = EFamilyPermissions:-1;
			toggleFamilyType(i, familytype, 1);
			format(Families[i][EFamilyName],32,"%s",name);
			format(Families[i][EFamilyOwner],MAX_PLAYER_NAME,"%s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			GiveNameSpace(Families[i][EFamilyOwner]);
			format(msg, sizeof(msg), "* %s Created a new family: {FF0000}%s",Families[i][EFamilyOwner],name);
			Broadcast(X11_WHITE, msg);
			format(msg, sizeof(msg), "* New Family SQL ID: %d, slot: %d",id, i);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_FamilyAdmin);
			SetPVarInt(playerid, "Family", id);
			SetPVarInt(playerid, "Rank", 6);
			return 1;
		}
	}
	ABroadcast(X11_TOMATO_2, "Error: Could not find a free family slot, increase MAX_FAMILIES", EAdminFlags_FamilyAdmin);
	return 1;
}
toggleFamilyType(idx, familytype, set) {
	query[0] = 0;
	switch(familytype) {
		case EFamilyType_None: {
			if(set == 1) Families[idx][EFFlags] = FamilyFlags:EFamilyType_None;
		}
		case FamilyType_Legal: {
			if(set == 1) Families[idx][EFFlags] |= FamilyFlags:EFamilyType_Legal; else Families[idx][EFFlags] &= ~FamilyFlags:EFamilyType_Legal;
		}
		case FamilyType_Biker: {
			if(set == 1) Families[idx][EFFlags] |= FamilyFlags:EFamilyType_Biker; else Families[idx][EFFlags] &= ~FamilyFlags:EFamilyType_Biker;
		}
		case FamilyType_Mafia: {
			if(set == 1) Families[idx][EFFlags] |= FamilyFlags:EFamilyType_Mafia; else Families[idx][EFFlags] &= ~FamilyFlags:EFamilyType_Mafia;
		}
		case FamilyType_StreetGang: {
			if(set == 1) Families[idx][EFFlags] |= FamilyFlags:EFamilyType_StreetGang; else Families[idx][EFFlags] &= ~FamilyFlags:EFamilyType_StreetGang;
		}
	}
	format(query, sizeof(query), "UPDATE `families` SET `familyflags` = %d WHERE `id` = %d",_:Families[idx][EFFlags],Families[idx][EFamilySQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 1;
}
getFamilyType(famid, sqlid = 0) {
	if(sqlid) {
		for(new i=0;i<sizeof(Families);i++) {
			if(Families[i][EFamilySQLID] == famid) {
				return _:Families[i][EFFlags];
			}
		}
	} else {
		return _:Families[famid][EFFlags];
	}
	return -1;
}
unloadFamily(index) {
	Families[index][EFamilySQLID] = 0;
}
#pragma unused unloadFamily
loadFamily(sqlid) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `families`.`id`, `name`, `colour`,`motd`,`characters`.`username`,`rank1text`,`rank1perms`,`rank2text`,`rank2perms`,`rank3text`,`rank3perms`,`rank4text`,`rank4perms`,`rank5text`,`rank5perms`,`rank6text`,`rank6perms`,`hq`,`hqx`,`hqy`,`hqz`,`familyflags` FROM `families` INNER JOIN `characters` ON `owner` = `characters`.`id` WHERE `family`.`id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadFamilies", "");
}
#pragma unused loadFamily
loadFamilies() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `families`.`id`, `name`, `colour`,`motd`,`characters`.`username`,`rank1text`,`rank1perms`,`rank2text`,`rank2perms`,`rank3text`,`rank3perms`,`rank4text`,`rank4perms`,`rank5text`,`rank5perms`,`rank6text`,`rank6perms`,`hq`,`hqx`,`hqy`,`hqz`,`familyflags` FROM `families` INNER JOIN `characters` ON `owner` = `characters`.`id`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadFamilies", "");
}
CreateFamily(playerid, name[], familytype) {
	query[0] = 0;
	format(query, sizeof(query), "INSERT INTO `families` (`name`,`owner`) VALUES (\"%s\",%d)",name,GetPVarInt(playerid, "CharID"));
	//if(GetMoneyEx(playerid) < 1000000) {
		//format(query, sizeof(query), "* %s's family couldn't be created because the user didn't have enough money on hand",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
		//ABroadcast(X11_YELLOW, query, EAdminFlags_FamilyAdmin);
	//} else {
		//GiveMoneyEx(playerid, -1000000);
	mysql_function_query(g_mysql_handle, query, true, "OnFamilyCreate", "dsd", playerid,name,familytype);
	//}
}
YCMD:families(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all official families");
		return 1;
	}
	if (EAdminFlags:GetPVarInt(playerid, "AdminLevel") & EAdminFlags_None) {
		if(GetPVarType(playerid, "Family") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "Only family members or admins may use this");
			return 1;
		}
	}
	new msg[128],numfound;
	SendClientMessage(playerid, X11_WHITE,"|___Families___|");
	for(new i=0;i<sizeof(Families);i++) {
		if(Families[i][EFamilySQLID] != 0) { 
			new FamilyFlags:aflags = Families[i][EFFlags];
			if (aflags != EFamilyType_None) {
	        	format(msg, sizeof(msg), "ID: %d | Name: %s | Owner: %s", i, GetFamilyName(i), Families[i][EFamilyOwner]);
	        	SendClientMessage(playerid, FamilyColour(i), msg);
				numfound++;
			}
		}
	}
	format(msg, sizeof(msg), "Number of official families found: %d",numfound);
	SendClientMessage(playerid, X11_WHITE, msg);
	SendClientMessage(playerid, X11_WHITE,"-----------------------");
	return 1;
}
YCMD:ufamilies(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all unofficial families");
		return 1;
	}
	if (EAdminFlags:GetPVarInt(playerid, "AdminLevel") & EAdminFlags_None) {
		if(GetPVarType(playerid, "Family") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "Only family members or admins may use this");
			return 1;
		}
	}
	new msg[128],numfound;
	SendClientMessage(playerid, X11_WHITE,"|___Families___|");
	for(new i=0;i<sizeof(Families);i++) {
		if(Families[i][EFamilySQLID] != 0) { 
			new FamilyFlags:aflags = Families[i][EFFlags];
			if (aflags == EFamilyType_None) {
	        	format(msg, sizeof(msg), "ID: %d | Name: %s | Owner: %s", i, GetFamilyName(i), Families[i][EFamilyOwner]);
	        	SendClientMessage(playerid, FamilyColour(i), msg);
				numfound++;
			}
		}
	}
	format(msg, sizeof(msg), "Number of unofficial families found: %d",numfound);
	SendClientMessage(playerid, X11_WHITE, msg);
	SendClientMessage(playerid, X11_WHITE,"-----------------------");
	return 1;
}
YCMD:createfamily(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Attempt to create a family");
		return 1;
	}
	/*
	if(GetPVarInt(playerid, "Level") < 5) {
		SendClientMessage(playerid, X11_TOMATO_2 , "You must be level 5 to create a family!");
		return 1;
	}
	*/
	if(GetPVarType(playerid, "FamilyName") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "Please wait, you already requested to create a family!");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") != 0) {
		SendClientMessage(playerid, X11_WHITE, "You are in a family. You must /quitfamily first, before you can create a family");
		return 1;
	}
	new name[32];
	new msg[128];
	new type;
	if(!sscanf(params, "s[32]d",name, type)) {
		if(type < 0 || type > 4) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid range. Pick a range between 0 and 4.");
			return 1;
		}
		SetPVarString(playerid, "FamilyName", name);
		SetPVarInt(playerid, "FamilyType", type);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Waiting for an admin to approve the family");
		//SendClientMessage(playerid, X11_WHITE, "Notice: Hold on to your $1,000,000, if you don't have it when the family is approved you will not be able to make the family");
		format(msg, sizeof(msg), "* %s wants to create the %s (%s) family. Do /acceptfamily or /declinefamily",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),name,type == 0 ? ("Unofficial") : ("Official"));
		ABroadcast(X11_YELLOW, msg, EAdminFlags_FamilyAdmin);
	} else {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Family Types:");
		for(new i=0; i<sizeof(FamilyFlagTypesDesc); i++) {
			format(msg, sizeof(msg), "%d. %s", i, FamilyFlagTypesDesc[i][EFamilyFlagDesc]);
			SendClientMessage(playerid, X11_WHITE, msg);
		}
		SendClientMessage(playerid, X11_WHITE, "USAGE: /createfamily [name] [Type]");
		return 1;
	}
	return 1;
}
YCMD:destroyfamily(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a family");
		return 1;
	}
	new id;
	new msg[128];
	if(!sscanf(params, "d",id)) {
		if(id < 0 || id > sizeof(Families)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family!");
			return 1;
		}
		if(IsValidFamily(id)) {
			format(msg, sizeof(msg), "* %s deleted family %s",GetPlayerNameEx(playerid, ENameType_AccountName),Families[id][EFamilyName]);
			Broadcast(X11_LIGHTBLUE, msg);
			DestroyFamily(id);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This family doesn't exist");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /destroyfamily [id]");
	}
	return 1;
}

YCMD:family(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a family message");
		return 1;
	}
	new string[128];
	if(isInJail(playerid) == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this in jail!");
		return 1;
	}
	if(GetPVarInt(playerid,"Faction") != 0) {
		format(string, sizeof(string), "/faction %s",params);
		Command_ReProcess(playerid, string, false);
		return 1;
	} else if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family.");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank");
	new fid = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[fid][EFamilyRankPerms][rank-1];
	if(~rankperms & EFamilyPerms_CanUseFamilyChat) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	new msg[128];
	if(!sscanf(params, "s[128]", msg)) {
		format(string, sizeof(string), "** (( %s %s: %s )) **",getRankName(fid, rank),GetPlayerNameEx(playerid, ENameType_RPName_NoMask), msg);
		FamilyMessage(fid, FamilyColour(fid), string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /family [message]");
	}
	return 1;
}
YCMD:buyfamilyhq(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying a family HQ");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	if(getFamilyType(family) == _:EFamilyType_None) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your family cannot purchase an HQ since it's unofficial.");
		return 1;
	}
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanAdjust) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	if(Families[family][EFamilyHQID] != -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You already have a family HQ!");
		return 1;
	}
	new temptxt[256];
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(FamilyHQs);i++) {
		format(temptxt, sizeof(temptxt), "%s - $%s\n",FamilyHQs[i][EFamilyHQInfoName],getNumberString(FamilyHQs[i][EFamilyHQInfoPrice]));
		strcat(dialogstr,temptxt,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFamilyDialog_HQBuy, DIALOG_STYLE_LIST, "{00BFFF}Choose a HQ",dialogstr, "Ok", "Cancel");
	return 1;
}
YCMD:acceptfamily(playerid, params[], help) {
	new player;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Approves a family");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		new name[32];
		new type;
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarType(player, "FamilyName") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_WHITE, "This player isn't requesting a family");
			return 1;
		}
		GetPVarString(player, "FamilyName", name, sizeof(name));
		type = GetPVarInt(player, "FamilyType");
		SendClientMessage(playerid, X11_YELLOW, "* Family Approved!");
		DeletePVar(player, "FamilyName");
		DeletePVar(player, "FamilyType");
		CreateFamily(player, name, type);
		SendClientMessage(playerid, X11_YELLOW, "The family has been approved!");
		format(msg, sizeof(msg), "* %s has approved %s's family(%s)",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(player, ENameType_RPName_NoMask),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_FamilyAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /approvefamily [playerid]");
		return 1;
	}
	return 1;
}
YCMD:declinefamily(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Declines a family");
		return 1;
	}
	new player,msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		new name[32];
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarType(player, "FamilyName") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_WHITE, "This player isn't requesting a family");
			return 1;
		}
		GetPVarString(player, "FamilyName", name, sizeof(name));
		DeletePVar(player, "FamilyName");
		SendClientMessage(player, X11_YELLOW, "The family has been Declined!");
		format(msg, sizeof(msg), "* %s has declined %s's family(%s)",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(player, ENameType_RPName_NoMask),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_FamilyAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /approvefamily [playerid]");
	}
	return 1;
}
YCMD:quitfamily(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for quitting your family");
		return 1;
	}
	new msg[128];
	if(GetPVarInt(playerid,"Faction") != 0) {
		format(msg, sizeof(msg), "/quitfaction %s",params);
		Command_ReProcess(playerid, msg, false);
		return 1;
	}
	new fid = GetPVarInt(playerid, "Family");
	if(fid == 0) {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "You aren't in a family!");
		return 1;
	}
	fid = FindFamilyBySQLID(fid);
	format(msg, sizeof(msg), "%s quit %s.", GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetFamilyName(fid));
	FamilyMessage(fid, COLOR_LIGHTBLUE, msg);
	SetPVarInt(playerid, "Family", 0);
	SetPVarInt(playerid, "Rank", 0);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You have quit your family");
	return 1;
}
YCMD:makefamilymember(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone a member of a family");
		return 1;
	}
	new playa,msg[128],familyid;
	if(!sscanf(params, "k<playerLookup_acc>d", playa,familyid)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!IsValidFamily(familyid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
			return 1;
		}
		format(msg, sizeof(msg), "Admin %s made %s member of %s.", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_RPName_NoMask), GetFamilyName(familyid));
		FamilyMessage(familyid, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "You have made %s member of %s.", GetPlayerNameEx(playa, ENameType_RPName_NoMask),GetFamilyName(familyid));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		SetPVarInt(playa, "Faction", 0);
		SetPVarInt(playa, "Family", SQLIDFromFamily(familyid));
		SetPVarInt(playa, "Rank", 1);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makefamilymember [playerid/name] [familyid]");
	}
	return 1;
}
YCMD:makefamilyleader(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone a member of a family");
		return 1;
	}
	new playa,msg[128],familyid;
	if(!sscanf(params, "k<playerLookup_acc>d", playa,familyid)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!IsValidFamily(familyid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
			return 1;
		}
		format(msg, sizeof(msg), "Admin %s made %s leader of %s.", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_RPName_NoMask), GetFamilyName(familyid));
		FamilyMessage(familyid, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "You have made %s leader of %s.", GetPlayerNameEx(playa, ENameType_RPName_NoMask),GetFamilyName(familyid));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		SetPVarInt(playa, "Family", SQLIDFromFamily(familyid));
		SetPVarInt(playa, "Faction", 0);
		SetPVarInt(playa, "Rank", 6);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makefamilyleader [playerid/name] [familyid]");
	}
	return 1;
}
YCMD:gotohq(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a families HQ");
		return 1;
	}
	new fid;
	if(!sscanf(params, "d", fid)) {
		if(!IsValidFamily(fid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
			return 1;
		}
		SetPlayerPos(playerid, Families[fid][EFamilyHQX], Families[fid][EFamilyHQY], Families[fid][EFamilyHQZ]);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerInterior(playerid, 0);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotohq [familyid]");
	}
	return 1;
}
YCMD:movehq(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a Families HQ");
		return 1;
	}
	new fid;
	if(!sscanf(params, "d", fid)) {
		if(!IsValidFamily(fid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
			return 1;
		}
		GetPlayerPos(playerid, Families[fid][EFamilyHQX], Families[fid][EFamilyHQY], Families[fid][EFamilyHQZ]);
		saveFamily(fid);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Family HQ Moved");
	}
	return 1;
}
YCMD:agiverank(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Assigns someones rank in a family or faction");
		return 1;
	}
	new playa,msg[128],rank;
	if(!sscanf(params, "k<playerLookup_acc>d", playa,rank)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(rank < 1 || rank > 16) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Rank");
			return 1;
		}
		SetPVarInt(playa, "Rank", rank);
		format(msg, sizeof(msg), "** You have been given Rank %d by Admin %s", rank, GetPlayerNameEx(playerid, ENameType_AccountName));
		SendClientMessage(playa, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "** You have given %s Rank %d.", GetPlayerNameEx(playa, ENameType_RPName_NoMask),rank);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /agiverank [playerid/name] [rankid]");
	}
	return 1;
}
YCMD:giverank(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Assigns someones rank in a family or faction");
		return 1;
	}
	new playa,msg[128],rank;
	new currank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	if(GetPVarInt(playerid, "Faction") != 0) {
		format(msg, sizeof(msg), "/fgiverank %s",params);
		Command_ReProcess(playerid, msg, false);
		return 1;
	}
	/*
	if(family == 0)  {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
		return 1;
	}
	*/
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][currank];
	if(~rankperms & EFamilyPerms_CanSetRank) {
		SendClientMessage(playerid, X11_TOMATO_2, "Permission denied.");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>d", playa,rank)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(rank < 1 || rank > 6) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Rank");
			return 1;
		}
		if(GetPVarInt(playa, "Family") != GetPVarInt(playerid, "Family")) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not in your family");
			return 1;
		}
		new userrank = GetPVarInt(playa, "Rank");
		if(userrank >= currank) {
			if(~rankperms & EFamilyPerms_CanSetRankEqual) {
				SendClientMessage(playerid, X11_TOMATO_2, "Permission Denied.");
				return 1;
			}
		}
		SetPVarInt(playa, "Rank", rank);
		format(msg, sizeof(msg), "** %s has been given rank %d by %s", GetPlayerNameEx(playa, ENameType_CharName),rank, GetPlayerNameEx(playerid, ENameType_CharName));
		FamilyMessage(family, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "** You have given %s Rank %d.", GetPlayerNameEx(playa, ENameType_RPName_NoMask),rank);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /giverank [playerid/name] [rankid]");
	}
	return 1;
}
YCMD:adjust(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Adjust family properties");
		return 1;
	}
	new item[32],param[128];
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanAdjust) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	if(!sscanf(params, "s[32]S()[128]",item,param)) {
		if(!strcmp(item, "MOTD", true)) {
			setFamilyMOTD(FindFamilyBySQLID(GetPVarInt(playerid, "Family")),FormatColourString(param));
		} else if(!strcmp(item, "RankPerms", true, 9)) {
				new arank = strval(item[9]);
				if(arank < 1 || arank > NUM_RANKS) {
					SendClientMessage(playerid, X11_WHITE, "Invalid Rank");
					return 1;
				}
				showFamilyPermissionsMenu(playerid, FindFamilyBySQLID(GetPVarInt(playerid, "Family")), arank);
		} else if(!strcmp(item, "Rank", true, 4)) {
				new arank = strval(item[4]);
				if(arank < 1 || arank > NUM_RANKS) {
					SendClientMessage(playerid, X11_WHITE, "Invalid Rank");
					return 1;
				}
				setRankText(FindFamilyBySQLID(GetPVarInt(playerid, "Family")),arank,param);
				SendClientMessage(playerid, X11_WHITE, "Rank Text Updated!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /adjust [item]");
		SendClientMessage(playerid, X11_WHITE, "Items: Motd, Rank(1-6), RankPerms(1-6)");
	}
	return 1;
}
YCMD:buyfamilycolour(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying family colours");
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2,"You must be in a family for this!");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(Families[family][EFamilyColour] != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You already have a family colour!");
		return 1;
	}
	if(~rankperms & EFamilyPerms_CanAdjust) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	dialogstr[0] = 0;
	new temptxt[256];
	for(new i=1;i<sizeof(FamilyColours);i++) {
		format(temptxt, sizeof(temptxt), "{%s}Sample %d\n",getColourString(FamilyColours[i]),i);
		strcat(dialogstr,temptxt,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFamilyDialog_SetFamColour, DIALOG_STYLE_LIST, "{00BFFF}Choose a Family Colour",dialogstr, "Ok", "Cancel");
	return 1;
}
YCMD:parkfamilycar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Update a family cars position");
		return 1;
	}
	if(GetMoneyEx(playerid) < 5000) {
		SendClientMessage(playerid, X11_RED_2, "You need $5,000 to do this!");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_RED_2, "You must be in the family car");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(VehicleInfo[carid][EVType] != EVehicleType_Family) {
		SendClientMessage(playerid, X11_RED_2, "You must be in a family car");
		return 1;
	} else if(VehicleInfo[carid][EVOwner] != GetPVarInt(playerid, "Family")) {
		SendClientMessage(playerid, X11_RED_2, "This is not your familys car.");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanParkCar) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	new Float:X,Float:Y,Float:Z,Float:A;
	new plate[(32*2)+1];
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, A);
	mysql_real_escape_string(VehicleInfo[carid][EVPlate],plate);
	query[0] = 0;
	format(query, sizeof(query), "UPDATE `familycars` SET `X` = %f, `Y` = %f, `Z` = %f, `Angle` = %f, `plate` = \"%s\" WHERE `id` = %d",X,Y,Z,A,plate,VehicleInfo[carid][EVSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "ReloadFamilyCar", "d",carid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Vehicle Position Saved!");
	GiveMoneyEx(playerid, -5000);
	return 1;
}
YCMD:reloadfamilycar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Reloads a family car");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_RED_2, "You must be in the family car");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(VehicleInfo[carid][EVType] != EVehicleType_Family) {
		SendClientMessage(playerid, X11_RED_2, "You must be in a family car");
		return 1;
	}
	ReloadFamilyCar(VehicleInfo[carid][EVSQLID]);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Vehicle Reloaded");
	return 1;
}
YCMD:invite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for inviting someone into a family");
		return 1;
	}
	new player, msg[128];
	if(GetPVarInt(playerid,"Faction") != 0) {
		format(msg, sizeof(msg), "/finvite %s",params);
		Command_ReProcess(playerid, msg, false);
		return 1;
	}
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanInvite) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(player, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(GetPVarInt(player, "Faction") != 0 || GetPVarInt(player, "Family") != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player is in a faction, or family!");
			return 1;
		}
		new sqlid = GetPVarInt(playerid,"Family");
		new fid = FindFamilyBySQLID(sqlid);
		sendFamilyInvite(fid, player);
		format(msg, sizeof(msg), "* %s has been invited to join the family by %s",GetPlayerNameEx(player, ENameType_RPName_NoMask), GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
		FamilyMessage(fid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /invite [playerid/name]");
	}
	return 1;
}
YCMD:uninvite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for inviting someone into a family");
		return 1;
	}
	new player, msg[128];
	if(GetPVarInt(playerid, "Family") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
		return 1;
	}
	if(GetPVarInt(playerid,"Faction") != 0) {
		format(msg, sizeof(msg), "/funinvite %s",params);
		Command_ReProcess(playerid, msg, false);
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank")-1;
	new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
	new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
	if(~rankperms & EFamilyPerms_CanUninvite) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>", player)) {
		if(!IsPlayerConnectEx(player)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarInt(player, "Family") != GetPVarInt(playerid, "Family")) {
			SendClientMessage(playerid, X11_WHITE, "This player is not in your family");
			return 1;
		}
		format(msg, sizeof(msg), "* %s has been kicked from the family by %s",GetPlayerNameEx(player, ENameType_RPName_NoMask),GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
		FamilyMessage(family, COLOR_LIGHTBLUE, msg);
		SetPVarInt(player, "Family", 0);
		SetPVarInt(player, "Rank", 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /uninvite [playerid/name]");
	}
	return 1;
}
YCMD:makefamilycar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes an owned car a family car");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle do this.");
		return 1;
	}
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned || VehicleInfo[carid][EVOwner] != playerid) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not own this vehicle.");
		return 1;
	}
	new sqlfam = GetPVarInt(playerid, "Family");
	if(sqlfam == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family!");
		return 1;
	}
	new model,c1,c2,ELockType:lock,famid;
	model = GetVehicleModel(carid);
	c1 = VehicleInfo[carid][EVColour][0];
	c2 = VehicleInfo[carid][EVColour][1];
	lock = VehicleInfo[carid][EVLockType];
	new Float:X, Float:Y, Float:Z, Float:Angle;
	GetVehiclePos(carid, X, Y, Z);
	GetVehicleZAngle(carid, Angle);
	DestroyOwnedVehicle(carid);
	famid = FindFamilyBySQLID(sqlfam);
	if(getFamilyCars(famid) > 10) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot give this family any more cars.");
		return 1;
	}
	CreateFamilyCar(sqlfam, model, c1, c2, ELockType:lock, X, Y, Z, Angle);
	new msg[128];
	format(msg, sizeof(msg), "* %s gave a %s to the family",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), VehiclesName[model-400]);
	FamilyMessage(famid, X11_YELLOW, msg);
	return 1;
}
YCMD:displayfamilycars(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists a familys cars");
		return 1;
	}
	new familyid;
	new locked[] = "{FF6347}Locked";
	new unlocked[] = "{ADD8E6}Unlocked";
	new numcars;
	new msg[128];
	if(!sscanf(params, "d", familyid)) {
		familyid = SQLIDFromFamily(familyid);
		for(new i=0;i<sizeof(VehicleInfo);i++ ){
			if(VehicleInfo[i][EVType] == EVehicleType_Family) {
				if(VehicleInfo[i][EVOwner] == familyid) {
					format(msg, sizeof(msg), "%d. %s (%s{FFFFFF}) {FF0000}Car ID: %d Model: %d SQLID: %d", ++numcars,VehiclesName[GetVehicleModel(i)-400],VehicleInfo[i][EVLocked]?locked:unlocked,i,GetVehicleModel(i), VehicleInfo[i][EVSQLID]);
					SendClientMessage(playerid, X11_WHITE, msg);
				}
			}
		}
		format(msg, sizeof(msg), "Number of vehicles: {ADD8E6}%d",numcars);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /displayfamilycars [familyid]");
	}
	return 1;
}
YCMD:familycars(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Lists your families cars.");
		return 1;
	}
	new msg[128];
	new famid = GetPVarInt(playerid, "Family");
	if(famid == 0) {
		SendClientMessage(playerid, X11_RED_2, "You must be in a family");
		return 1;
	}
	new locked[] = "{FF6347}Locked";
	new unlocked[] = "{ADD8E6}Unlocked";
	new numcars;
	for(new i=0;i<sizeof(VehicleInfo);i++) {
		if(GetVehicleModel(i) != 0) {
			if(VehicleInfo[i][EVType] == EVehicleType_Family) {
				if(VehicleInfo[i][EVOwner] == famid) {
					format(msg, sizeof(msg), "%d. %s (%s{FFFFFF})", ++numcars,VehiclesName[GetVehicleModel(i)-400],VehicleInfo[i][EVLocked]?locked:unlocked);
					SendClientMessage(playerid, X11_WHITE, msg);
				}
			}
		}
	}
	format(msg, sizeof(msg), "Number of vehicles: {ADD8E6}%d",numcars);
	SendClientMessage(playerid, X11_WHITE, msg);
	return 1;
}
YCMD:setfamilyname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modifys a families name");
		return 1;
	}
	new famid, name[MAX_FAMILY_NAME];
	if(!sscanf(params, "ds["#MAX_FAMILY_NAME"]",famid, name)) {
		if(IsValidFamily(famid)) {
			setFamilyName(famid, name);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Family Updated!");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family ID!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setfamilyname [famid] [name]");
	}
	return 1;
}
YCMD:setfamilycolour(playerid, params[], help) {
	new colour, famid, override;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a family colour");
		return 1;
	}
	if(!sscanf(params, "ddD(0)", famid, colour, override)) {
		if(!IsValidFamily(famid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid family!");
			return 1;
		}
		if(colour < 0 || colour >= sizeof(FamilyColours)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid colour index!");
			return 1;
		}
		if(IsFamilyColourUsed(colour) && !override && colour != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "A family is already using this colour!");
			return 1;
		}
		setFamilyColour(famid, colour);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setfamilycolour [famid] [colourid] (override)");
	}
	return 1;
}
YCMD:clearfamilycolour(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes a families colour");
		return 1;
	}
	new famid;
	if(!sscanf(params, "d", famid)) {
		if(IsValidFamily(famid)) {
			setFamilyColour(famid, 0);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Family colour removed!");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /clearfamilycolour [famid]");
	}
	return 1;
}
YCMD:togglefamilytype(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to change the family flags");
		return 1;
	}
	new famid;
	if(!sscanf(params, "d", famid)) {
		if(!IsValidFamily(famid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid family!");
			return 1;
		}
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[INFO]: At the moment, multiple faction types are not supported, set one at a time please..");
		ShowFamilyFlags(playerid, famid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /togglefamilytype [famid]");
	}
	return 1;
}
ShowFamilyFlags(playerid, fid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new FamilyFlags:aflags = Families[fid][EFFlags];
	new statustext[32]; 
	SetPVarInt(playerid, "FamilyEditing", fid);
	for(new i=0;i<sizeof(FamilyFlagTypesDesc);i++) {
		if(aflags & FamilyFlagTypesDesc[i][EFamilyFlag] || (FamilyFlagTypesDesc[i][EFamilyFlag] == EFamilyType_None && aflags == EFamilyType_None)) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}No"; //Else show no
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",FamilyFlagTypesDesc[i][EFamilyFlagDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFamilyDialog_ToggleFamilyFlags, DIALOG_STYLE_LIST, "Modify Family Flags",dialogstr,"Toggle", "Done");
}
setFamilyColour(famid, colourindex) {
	new sqlid = SQLIDFromFamily(famid);
	if(sqlid == -1) return 0;
	Families[famid][EFamilyColour] = colourindex;
	resyncFamily(famid);
	format(query, sizeof(query), "UPDATE `families` SET `colour` = %d WHERE `id` = %d", colourindex, sqlid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	
	return 0;
}
setFamilyName(famid, name[]) {
	new name_esc[(MAX_FAMILY_NAME*2)+1];
	new sqlid = SQLIDFromFamily(famid);
	if(sqlid == -1) return 0;
	mysql_real_escape_string(name,name_esc);
	format(Families[famid][EFamilyName], MAX_FAMILY_NAME, "%s",name);
	format(query, sizeof(query), "UPDATE `families` SET `name` = \"%s\" WHERE `id` = %d",name_esc,sqlid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 1;
}
FamilyMessage(index, color, msg[]) {
	new fam;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			fam = GetPVarInt(i, "Family");
			if(GetPVarInt(i, "BigFamilyEars") == 1 || (index == -1 && fam != 0) || (FindFamilyBySQLID(fam) == index && index != -1)) {
				if(~EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoFamilyChat)  {
					SendClientMessage(i, color, msg);
				}
			}
		}
	}
}
FindFamilyBySQLID(id) {
	for(new i=0;i<sizeof(Families);i++) {
		if(Families[i][EFamilySQLID] == id && id != 0) {
			return i;
		}
	}
	return -1;
}
SQLIDFromFamily(index) {
	if(index < 0 || index > sizeof(Families)) return 0;
	return Families[index][EFamilySQLID];
}
IsValidFamily(index) {
	if(index < 0 || index > sizeof(Families)) return 0;
	if(Families[index][EFamilySQLID] != 0)
		return 1;
	return 0;
}
stock GetFamilyName(id) {
	new name[32];
	if(IsValidFamily(id)) {
		strmid(name,Families[id][EFamilyName],0,strlen(Families[id][EFamilyName]), MAX_FAMILY_NAME);
	} else {
		strmid(name,"None",0,4, sizeof(name));
	}
	return name;
}
DestroyFamily(id) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Family") == Families[id][EFamilySQLID]) {
				SetPVarInt(i, "Rank", 0);
				SetPVarInt(i, "Family", 0);
				SendClientMessage(i, COLOR_LIGHTBLUE, "* Your family has been deleted!");
			}
		}
	}
	query[0] = 0;
	format(query, sizeof(query), "DELETE FROM `families` WHERE `id` = %d",Families[id][EFamilySQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	destroyFamilyCars(id);
	deleteFamilyNotes(id);
	if(FamilyHasSafe(id)) {
		DeleteFamilySafe(id);
	}
	
	deleteFamilyWallTags(Families[id][EFamilySQLID]);
	
	resyncTurfs();
	
	Families[id][EFamilySQLID] = 0;
	Families[id][EFamilyHQID] = -1;
}
FamilyColour(index) {
	if(!IsValidFamily(index)) {
		return FamilyColours[0];
	}
	return FamilyColours[Families[index][EFamilyColour]];
}
setFamilyMOTD(fid,motd[]) {
	new msg[128];
	new sqlid = SQLIDFromFamily(fid);
	strmid(Families[fid][EFamilyMOTD],motd,0,strlen(motd), MAX_FAMILY_MOTD);
	mysql_real_escape_string(motd, motd);
	format(msg, sizeof(msg), "UPDATE `families` SET `MOTD` = \"%s\" WHERE `id` = %d",motd,sqlid);
	mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
	format(msg, sizeof(msg), "Family MOTD Set to: %s", motd);
	FamilyMessage(fid, FamilyColour(fid), msg);
	return 1;
}
setRankText(fid, rankid, text[]) {
	new msg[128];
	new sqlid = SQLIDFromFamily(fid);
	strmid(FamilyRanks[fid][rankid-1],text,0,strlen(text), MAX_FAMILY_NAME);
	mysql_real_escape_string(text, text);
	format(msg, sizeof(msg), "UPDATE `families` SET `rank%dtext` = \"%s\" WHERE `id` = %d",rankid,text,sqlid);
	mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");
}
getRankName(fid, rankid) {
	new name[MAX_FAMILY_NAME];
	if(rankid < 1 || rankid > NUM_RANKS) {
		strmid(name,"None",0,4, sizeof(name));
		return name;
	}
	strmid(name,FamilyRanks[fid][rankid-1],0,strlen(FamilyRanks[fid][rankid-1]), MAX_FAMILY_NAME);
	if(strlen(name) < 1) {
		strmid(name,"None",0,4, sizeof(name));
	}
	return name;
}
familyOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	dialogstr[0] = 0;
	switch(dialogid) {
		case EFamilyDialog_SetFamColour: {
			if(!response) {
				return 1;
			}
			if(IsFamilyColourUsed(listitem+1)) {
				SendClientMessage(playerid, X11_TOMATO_2, "A family is already using this colour!");
				return 1;
			}
			new sqlid = GetPVarInt(playerid,"Family");
			new fid = FindFamilyBySQLID(sqlid);
			SendClientMessage(playerid, FamilyColour(fid), "Family Colour Updated!");
			setFamilyColour(fid, listitem+1);
		}
		case EFamilyDialog_InviteDialog: {
			if(GetPVarType(playerid, "FamilyInvite") == PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_TOMATO_2, "You aren't invited to a family");
				return 1;
			}
			new msg[128];
			if(!response) {
				format(msg, sizeof(msg), "* %s has declined the family invitation",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				return 1;
			}
			new fid = GetPVarInt(playerid, "FamilyInvite");
			SetPVarInt(playerid, "Family",SQLIDFromFamily(fid));
			SetPVarInt(playerid, "Rank", 1);
			format(msg, sizeof(msg), "* %s has joined the family",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			FamilyMessage(fid, COLOR_LIGHTBLUE, msg);
		}
		case EFamilyDialog_PermissionsEdit: {
			if(GetPVarType(playerid, "FamilyEdit") == PLAYER_VARTYPE_NONE || !response) {
				return 1;
			}
			new familyid = GetPVarInt(playerid, "FamilyEdit");
			new rank = GetPVarInt(playerid, "RankEdit")-1;
			new EFamilyPermissions:flag = FamilyFlagDescription[listitem][EFFIFlags];
			if(Families[familyid][EFamilyRankPerms][rank] & flag) {
				Families[familyid][EFamilyRankPerms][rank] &= ~flag;
			} else {
				Families[familyid][EFamilyRankPerms][rank] |= flag;
			}
			query[0] = 0;
			format(query, sizeof(query), "UPDATE `families` SET `rank%dperms` = %d WHERE `id` = %d", rank+1,_:Families[familyid][EFamilyRankPerms][rank],SQLIDFromFamily(familyid));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			showFamilyPermissionsMenu(playerid, familyid, rank+1);
		}
		case EFamilyDialog_HQBuy: {
			if(!response) return 1;
			new msg[128];
			new price = FamilyHQs[listitem][EFamilyHQInfoPrice];
			if(GetMoneyEx(playerid) < price) {
				SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money.");
				return 1;
			}
			GiveMoneyEx(playerid, -price);
			new Float:X, Float:Y, Float:Z;
			GetPlayerPos(playerid, X, Y, Z);
			new sqlid = GetPVarInt(playerid,"Family");
			new fid = FindFamilyBySQLID(sqlid);
			Families[fid][EFamilyHQID] = listitem;
			Families[fid][EFamilyHQX] = X;
			Families[fid][EFamilyHQY] = Y;
			Families[fid][EFamilyHQZ] = Z;
			format(msg, sizeof(msg), "Congratulations on your purchase of a %s",FamilyHQs[listitem][EFamilyHQInfoName]);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
			saveFamily(fid);
		}
		case EFamilyDialog_BuySafe: {
			handleBuyFamilySafe(playerid, listitem);
		}
		case EFamilyDialog_BuyHouseSafe: { //didn't feel like making a whole function for one dialog
			handleBuyHouseSafe(playerid, listitem);
		}
		case EFamilyDialog_UpgradeSafe: {
			handleUpgradeSafe(playerid, listitem);
		}
		case EFamilyDialog_ToggleFamilyFlags: {
			if(!response) {
				DeletePVar(playerid, "FamilyEditing");
				return 1;
			}
			new famid = GetPVarInt(playerid, "FamilyEditing");
			DeletePVar(playerid, "FamilyEditing");
			new FamilyFlags:flag = FamilyFlagTypesDesc[listitem][EFamilyFlag];
			if(Families[famid][EFFlags] & flag) {
				toggleFamilyType(famid, listitem, 0);
			} else {
				toggleFamilyType(famid, listitem, 1);
			}
			ShowFamilyFlags(playerid, famid);
		}
	}
	return 1;
}
sendFamilyInvite(id, player) {
	dialogstr[0] = 0;
	format(dialogstr,sizeof(dialogstr),"You have been invited into \"%s\". Do you wish to join?",GetFamilyName(id));
	SetPVarInt(player, "FamilyInvite", id);
	ShowPlayerDialog(player, EFamilyDialog_InviteDialog, DIALOG_STYLE_MSGBOX, "{00BFFF}Family Invite", dialogstr, "Accept", "Decline");
	return 1;
}
showFamilyPermissionsMenu(playerid, familyid, rank) {
	new EFamilyPermissions:rankperms = Families[familyid][EFamilyRankPerms][rank-1];
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new statustext[32]; 
	for(new i=0;i<sizeof(FamilyFlagDescription);i++) {
		if(rankperms&FamilyFlagDescription[i][EFFIFlags]) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}NO";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",FamilyFlagDescription[i][EFFIDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	/*
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		new title[64];
		format(title,sizeof(title),"AND NOT Rank Flags\nAND Rank Flags\nSet Rank Flags(%d)",_:rankperms);
		strcat(dialogstr,title,sizeof(dialogstr));
	}
	*/
	SetPVarInt(playerid, "RankEdit", rank);
	SetPVarInt(playerid, "FamilyEdit", familyid);
	ShowPlayerDialog(playerid, EFamilyDialog_PermissionsEdit, DIALOG_STYLE_LIST, "Modify Admin Permissions",dialogstr,"Toggle", "Done");
}
familiesTryEnterExit(playerid) {
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(Families);i++) {
		new interior = Families[i][EFamilyHQID];
		if(interior == -1) continue;
		if(IsPlayerInRangeOfPoint(playerid,3.5,FamilyHQs[interior][EFamilyHQInfoX],FamilyHQs[interior][EFamilyHQInfoY],FamilyHQs[interior][EFamilyHQInfoZ])) {
			if(GetPlayerVirtualWorld(playerid) == GetFamilyHQVW(i)) {
				SetPlayerPos(playerid, Families[i][EFamilyHQX], Families[i][EFamilyHQY], Families[i][EFamilyHQZ]);
				SetPlayerVirtualWorld(playerid, 0);
				SetPlayerInterior(playerid, 0);
			}
		} else if(IsPlayerInRangeOfPoint(playerid, 3.5, Families[i][EFamilyHQX], Families[i][EFamilyHQY], Families[i][EFamilyHQZ])) {
			if(FamilyHQs[interior][EFamilyHQInfoFreeze] == 1) { //Needs freeze
				WaitForObjectsToStream(playerid);
			}
			SetPlayerPos(playerid, FamilyHQs[interior][EFamilyHQInfoX],FamilyHQs[interior][EFamilyHQInfoY],FamilyHQs[interior][EFamilyHQInfoZ]);
			SetPlayerVirtualWorld(playerid, GetFamilyHQVW(i));
			SetPlayerInterior(playerid, FamilyHQs[interior][EFamilyHQInfoInt]);
		}
	}
	return 1;
}
GetFamilyHQVW(familyid) {
	return familyid+10000;
}
saveFamily(familyid) {
	new msg[256];
	format(msg, sizeof(msg), "UPDATE `families` SET `hq` = %d, `hqx` = %f, `hqy` = %f, `hqz` = %f WHERE `id` = %d", Families[familyid][EFamilyHQID], Families[familyid][EFamilyHQX], Families[familyid][EFamilyHQY], Families[familyid][EFamilyHQZ], Families[familyid][EFamilySQLID]);
	mysql_function_query(g_mysql_handle, msg, true, "EmptyCallback", "");	
}
numberOfFamilyMembersInRadius(playerid, Float:radius) {
	new Float:X,Float:Y,Float:Z;
	new num;
	new family = GetPVarInt(playerid, "Family");
	GetPlayerPos(playerid, X, Y, Z);
	foreach(Player, i) {
		if(IsPlayerInRangeOfPoint(i, radius, X, Y, Z)) {
			if(GetPVarInt(i, "Family") == family)
				num++;
		}	
	}
	return num;
}
numberOfFMembersInRadiusOfPoint(family, Float:radius, Float:X, Float:Y, Float:Z) {
	new num;
	foreach(Player, i) {
		if(IsPlayerInRangeOfPoint(i, radius, X, Y, Z)) {
			if(FindFamilyBySQLID(GetPVarInt(i, "Family")) == family)
				num++;
		}	
	}
	return num;
}
IsFamilyColourUsed(colour) {
	//colour 0 = default colour(white)
	if(colour < 1 || colour > sizeof(FamilyColours)-1) return 0;
	for(new i=0;i<sizeof(Families);i++) {
		if(Families[i][EFamilySQLID] != 0) { 
			if(FamilyColours[colour] == FamilyColour(i)) {
				return 1;
			}
		}
	}
	return 0;
}
YCMD:setfamilyowner(playerid, params[], help) {
	new user,famid;
	if(!sscanf(params,"du",famid,user)) {
		if(famid < 0 || famid > sizeof(Families) || Families[famid][EFamilySQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family!");
			return 1;
		}
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		format(Families[famid][EFamilyOwner], MAX_PLAYER_NAME, "%s", GetPlayerNameEx(user, ENameType_RPName_NoMask));
		new msg[128];
		format(msg, sizeof(msg), "UPDATE `families` SET `owner` = %d WHERE `id` = %d",GetPVarInt(user, "CharID"),Families[famid][EFamilySQLID]);
		mysql_function_query(g_mysql_handle, msg, false, "EmptyCallback", "");
		SendClientMessage(playerid, COLOR_DARKGREEN, "Family Owner Changed!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setfamilyowner [famid] [user]");
	}
	return 1;
}
YCMD:familymembers(playerid, params[], help) {
	new fid = GetPVarInt(playerid, "Family");
	new rfid = fid;
	fid = FindFamilyBySQLID(rfid);
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_FamilyAdmin) {
		sscanf(params, "d", fid);
		if(!IsValidFamily(fid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family!");
			return 1;
		}
		rfid = SQLIDFromFamily(fid);
	}
	if(fid == -1) {	
		return 1;
	}
	new string[128];
//	HintMessage(playerid, COLOR_DARKGREEN, "Hint: Do /ofamilymembers to see offline family members!");
	format(string, sizeof(string), "|__Family members of %s__|", Families[fid][EFamilyName]);
	SendClientMessage(playerid, X11_WHITE, string);
	foreach(Player, i) {
		new rank = GetPVarInt(i, "Rank");
		if(GetPVarInt(i, "Family") == rfid) {
			format(string, sizeof(string), "%s - %s(%d)",GetPlayerNameEx(i, ENameType_RPName_NoMask),getRankName(fid, rank),rank);
			SendClientMessage(playerid, X11_WHITE, string);
		}
	}
	SendClientMessage(playerid, X11_WHITE, "-------------------");
	return 1;
}
YCMD:flock(playerid, params[], help) {
	new index;
	if (!sscanf(params, "d", index))
    {
		index--;
		new rank = GetPVarInt(playerid, "Rank")-1;
		new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
		if(family == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
			return 1;
		}
		new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
		new carid = FindFamilyCar(family, index);
		if(carid == -1) {
			SendClientMessage(playerid, X11_RED2, "You don't have a vehicle at this slot!");
			return 1;
		}
		if(~rankperms & EFamilyPerms_CanDriveCar) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetVehiclePos(carid, X, Y, Z);
		new Float:dist = getLockDistance(VehicleInfo[carid][EVLockType]);
		if(dist != 0 && !IsPlayerInRangeOfPoint(playerid, dist, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far from your vehicle");
			return 1;
		}
		LockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"The %s has been locked.",VehiclesName[GetVehicleModel(carid)-400]);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	return 1;
}
YCMD:funlock(playerid, params[], help) {
	new index;
	if (!sscanf(params, "d", index))
    {
		index--;
		new rank = GetPVarInt(playerid, "Rank")-1;
		new family = FindFamilyBySQLID(GetPVarInt(playerid, "Family"));
		if(family == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in a family");
			return 1;
		}
		new carid = FindFamilyCar(family, index);
		if(carid == -1) {
			SendClientMessage(playerid, X11_RED2, "You don't have a vehicle at this slot!");
			return 1;
		}
		new EFamilyPermissions:rankperms = EFamilyPermissions:Families[family][EFamilyRankPerms][rank];
		if(~rankperms & EFamilyPerms_CanDriveCar) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetVehiclePos(carid, X, Y, Z);
		new Float:dist = getLockDistance(VehicleInfo[carid][EVLockType]);
		if(dist != 0 && !IsPlayerInRangeOfPoint(playerid, dist, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far from your vehicle");
			return 1;
		}
		UnlockCar(carid);
		new msg[128];
		format(msg,sizeof(msg),"The %s has been unlocked.",VehiclesName[GetVehicleModel(carid)-400]);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	return 1;
}
sendPlayerFamilyNotes(playerid, famid) {
	format(query, sizeof(query), "SELECT `noteid`,`setter`.`username`,`seton`,`note` FROM `familynotes` INNER JOIN `accounts` AS `setter` ON `setter`.`id` = `familynotes`.`setby` WHERE `familyid` = %d",SQLIDFromFamily(famid));
	mysql_function_query(g_mysql_handle, query, true, "OnRecieveFamilyNotes", "dd",playerid,famid);
}
YCMD:famnotes(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists the notes of a family");
		return 1;
	}
	new famid;
	if(!sscanf(params, "d", famid)) {
		if(famid < 0 || famid > sizeof(Families)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid family ID.");
			return 1;
		}
		sendPlayerFamilyNotes(playerid, famid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /famnotes [familyid]");
	}
	return 1;
}
YCMD:addfamnote(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Add an account note");
		return 1;
	}
	new note[(128*2)+1],famid;
	if(!sscanf(params, "ds[128]", famid, note)) {
		if(famid < 0 || famid > sizeof(Families)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid family ID.");
			return 1;
		}
		mysql_real_escape_string(note, note);
		format(query, sizeof(query), "INSERT INTO `familynotes` SET `note` = \"%s\",`familyid` = %d,`setby` = %d",note,SQLIDFromFamily(famid), GetPVarInt(playerid, "AccountID"));
		mysql_function_query(g_mysql_handle, query, true, "OnAddFamilyNote", "dd",playerid, famid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /addfamnote [familyid] [note]");
	}
	return 1;
}
AddFamilyNote(famid, setter, reason[]) {
	new treason[(128*2)+1];
	mysql_real_escape_string(reason, treason);
	new setbyid = 0;
	if(setter != INVALID_PLAYER_ID && setter != -1) {
		setbyid = GetPVarInt(setter, "AccountID");
	}
	format(query, sizeof(query), "INSERT INTO `familynotes` SET `note` = \"%s\",`familyid` = %d,`setby` = %d",treason,SQLIDFromFamily(famid), setbyid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
YCMD:delfamnote(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Delete a family note");
		return 1;
	}
	new noteid;
	if(!sscanf(params, "d", noteid)) {
		format(query, sizeof(query), "DELETE FROM `familynotes` WHERE `noteid` = %d",noteid);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /delfamnote [noteid]");
	}
	return 1;
}
forward OnDeleteFamNote(playerid, noteid);
public OnDeleteFamNote(playerid, noteid) {
	format(query, sizeof(query),"* %d notes deleted",mysql_affected_rows());
	SendClientMessage(playerid, X11_YELLOW_2, query);
	return 1;
}
forward OnAddFamilyNote(playerid, familyid);
public OnAddFamilyNote(playerid, familyid) {
	new id = mysql_insert_id();
	format(query, sizeof(query), "* Note added as id %d",id);
	SendClientMessage(playerid, X11_YELLOW_2, query);
	return 0;
}
forward OnRecieveFamilyNotes(playerid, familyid);
public OnRecieveFamilyNotes(playerid, familyid) {
	new rows,fields;
	cache_get_data(rows,fields);
	format(query, sizeof(query), "**** Family Notes for: %s ****",GetFamilyName(familyid));
	SendClientMessage(playerid, X11_WHITE, query);
	if(rows < 1) {
		SendClientMessage(playerid, X11_YELLOW, "* No Notes found");
		return 1;
	}
	new noteid, setby[MAX_PLAYER_NAME+1],seton[32],note[128];
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,query);
		noteid = strval(query);
		cache_get_row(i,1,setby);
		cache_get_row(i,2,seton);
		cache_get_row(i,3,note);
		format(query, sizeof(query), "* ID: %d Set By: %s Set On: %s Note: %s",noteid,setby,seton,note);
		SendClientMessage(playerid, X11_TOMATO_2, query);
	}
	return 0;
}
deleteFamilyNotes(fid) {
	format(query, sizeof(query), "DELETE FROM `familynotes` WHERE `familyid` = %d",SQLIDFromFamily(fid));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
resyncFamily(famid) {
	#pragma unused famid
	resyncTurfs();
}