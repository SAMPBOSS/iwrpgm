new boxer1 = -1, boxer2 = -1;
new boxercountdown = 10;
new boxerprize;
new boxingtimer;
boxingMatchActive() {
	return boxer1 != -1 || boxer2 != -1;
}
startFight(sender, playerid, fightbet) {
	boxer1 = sender;
	boxer2 = playerid;
	SetPVarInt(playerid, "IsBoxing", 1);
	SetPVarInt(sender, "IsBoxing", 1);
	SetPlayerInterior(playerid, 5);
	SetPlayerInterior(sender, 5);
	SetPlayerPos(playerid, 762.9852,2.4439,1001.5942);
	SetPlayerFacingAngle(playerid, 131.8632);
	SetCameraBehindPlayer(playerid);
	SetPlayerPos(sender, 758.7064,-1.8038,1001.5942);
	SetPlayerFacingAngle(sender, 313.1165);
	SetCameraBehindPlayer(sender);
	
	TogglePlayerControllableEx(playerid, 0);
	TogglePlayerControllableEx(sender, 0);
	//GameTextForPlayer(playerid, "~r~Waiting", 3000, 1);
	//GameTextForPlayer(sender, "~r~Waiting", 3000, 1);
	saveGuns(playerid, "BoxingGuns");
	saveGuns(sender, "BoxingGuns");
	boxerprize = fightbet;
	new Float:hp;
	
	GetPlayerHealth(playerid, hp);
	SetPVarFloat(playerid, "BoxingHP", hp);
	GetPlayerHealth(sender,hp);
	SetPVarFloat(sender, "BoxingHP", hp);
	GetPlayerArmourEx(playerid, hp);
	SetPVarFloat(playerid, "BoxingArmour", hp);
	GetPlayerArmourEx(sender, hp);
	SetPVarFloat(sender, "BoxingArmour", hp);
	new level = getSkillPoints(sender, 6);
	if(level >= 0 && level <= 50) { SetPlayerHealthEx(sender, 60); }
	else if(level >= 51 && level <= 100) { SetPlayerHealthEx(sender, 70); }
	else if(level >= 101 && level <= 200) { SetPlayerHealthEx(sender, 80); }
	else if(level >= 201 && level <= 400) { SetPlayerHealthEx(sender, 90); }
	else if(level >= 401) { SetPlayerHealthEx(sender, 100); }
	
	level = getSkillPoints(playerid, 6);
	if(level >= 0 && level <= 50) { SetPlayerHealthEx(playerid, 60); }
	else if(level >= 51 && level <= 100) { SetPlayerHealthEx(playerid, 70); }
	else if(level >= 101 && level <= 200) { SetPlayerHealthEx(playerid, 80); }
	else if(level >= 201 && level <= 400) { SetPlayerHealthEx(playerid, 90); }
	else if(level >= 401) { SetPlayerHealthEx(playerid, 100); }

	SetPlayerArmourEx(playerid, 0.0);
	SetPlayerArmourEx(sender, 0.0);
	
	SetPlayerHealthEx(playerid, 98.0);
	SetPlayerHealthEx(sender, 98.0);
	
	//A little decoration
	ApplyAnimation(playerid,"GYMNASIUM", "GYMshadowbox", 1.800001, 1, 0, 0, 1, 600);
	ApplyAnimation(sender,"GYMNASIUM", "GYMshadowbox", 1.800001, 1, 0, 0, 1, 600);
	boxingtimer = SetTimer("Countdown", 1000, true);
}
endMatch(winner, loser) {
	new string[128];
	if(winner == -1 || loser == -1) {
		if(IsPlayerConnectEx(boxer1)) 
			SendClientMessage(boxer1, COLOR_LIGHTBLUE, "* Match Interrupted");
		if(IsPlayerConnectEx(boxer2))
			SendClientMessage(boxer2, COLOR_LIGHTBLUE, "* Match Interrupted");
	} else {
		if(isBoxing(winner) || isBoxing(loser)) {
			GiveMoneyEx(loser, -boxerprize);
			GiveMoneyEx(winner, boxerprize); 
			format(string, sizeof(string), "* You won the $%s.", getNumberString(boxerprize));
			SendClientMessage(winner, COLOR_DARKGREEN, string);
			format(string, sizeof(string), "* You lost the $%s.", getNumberString(boxerprize));
			SendClientMessage(loser, COLOR_DARKGREEN, string);
			StartPlayerAnimation(winner, 37, INVALID_PLAYER_ID);
			GameTextForPlayer(loser, "~r~You lost", 3500, 1);
			GameTextForPlayer(winner, "~g~You won", 3500, 1);
			increaseSkillPoints(winner, 6);
			SetTimerEx("ExitBoxingMatch", 2000, false, "dd", winner, loser);
		}
	}
	DeletePVar(winner, "IsBoxing");
	DeletePVar(loser, "IsBoxing");
	boxer1 = -1;
	boxer2 = -1;
	boxerprize = -1;
	boxercountdown = 10;
}
isBoxing(playerid) {
	if(GetPVarType(playerid, "IsBoxing") != PLAYER_VARTYPE_NONE) {
		return 1;
	} else {
		return 0;
	}
}
boxingOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused killerid
	#pragma unused reason
	new winner, loser;
	loser = playerid;
	winner = boxer1;
	if(isAtGym(playerid) && isAtGym(killerid)) {
		if(winner == loser) {
			winner = boxer2;
		}
		endMatch(winner, loser);
	}
}
boxingOnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid) {
	#pragma unused amount
	#pragma unused issuerid
	if(playerid == boxer1 || playerid == boxer2) {
		if(weaponid != 0) {
			endMatch(-1, -1);
		}
		new Float:hp;
		GetPlayerHealth(playerid, hp);
		if(hp < 20.0) {
			new loser = playerid;
			new winner = boxer1;
			StartPlayerAnimation(playerid, 20, INVALID_PLAYER_ID);
			if(winner == loser) winner = boxer2;
			endMatch(winner, loser);
		}
	}
}

forward ExitBoxingMatch(player1, player2);
public ExitBoxingMatch(player1, player2) {
	restoreGuns(player1, "BoxingGuns");
	restoreGuns(player2, "BoxingGuns");
	SetPlayerPos(player1, 768.154174, 2.502297, 1000.722351);
	SetPlayerPos(player2, 768.154174, 2.502297, 1000.722351);
	
	SetPlayerHealthEx(player1, GetPVarFloat(player1, "BoxingHP"));
	SetPlayerHealthEx(player2, GetPVarFloat(player2, "BoxingHP"));
	SetPlayerArmourEx(player1, GetPVarFloat(player1, "BoxingArmour"));
	SetPlayerArmourEx(player2, GetPVarFloat(player2, "BoxingArmour"));
}

forward Countdown();
public Countdown() {
	new msg[128];
	format(msg, sizeof(msg), "~r~%d",boxercountdown--);
	GameTextForPlayer(boxer1,msg, 1000, 4);
	GameTextForPlayer(boxer2,msg, 1000, 4);
	if(boxercountdown < 1) {
		TogglePlayerControllableEx(boxer1, 1);
		TogglePlayerControllableEx(boxer2, 1);
		stopAllBoxingCountDowns();
	}
}

forward stopAllBoxingCountDowns();
public stopAllBoxingCountDowns() {
	KillTimer(boxingtimer);
	boxercountdown = 10; //Resets the 10 second count down
}

boxingOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	/* won't remove their money
	if(reason == 1) {
		new loser = playerid;
		new winner = boxer1;
		if(winner == loser) winner = boxer2;
		endMatch(winner, loser);
		return 1;
	}
	*/
	if(boxer1 == playerid || boxer2 == playerid) {
		endMatch(-1, -1);
	}
	return 1;
}