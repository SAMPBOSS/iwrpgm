
isGovernment(playerid) {
	new faction = GetPVarInt(playerid, "Faction");
	return getFactionType(faction) == EFactionType_Government;
}

YCMD:settax(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify the servers tax rate.");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!isGovernment(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't in the government!");
			return 1;
		}
		if(GetPVarInt(playerid, "Rank") < 6) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be rank 6 to do this!");
			return 1;
		}
	}
	new tax;
	if(!sscanf(params, "d", tax)) {
		if(tax < 0 || tax > 50) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid tax value!");
			return 1;
		}
		server_taxrate = tax;
		format(query, sizeof(query), "UPDATE `misc` SET `tax` = %d",tax);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		format(query, sizeof(query), "* The tax rate is now at: %d",tax);
		SendClientMessage(playerid, COLOR_DARKGREEN, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /settax [rate]");
	}
	return 1;
}
YCMD:setinterest(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify the servers interest rate.");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!isGovernment(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't in the government!");
			return 1;
		}
		if(GetPVarInt(playerid, "Rank") < 6) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be rank 6 to do this!");
			return 1;
		}
	}
	new interestrate;
	if(!sscanf(params, "d", interestrate)) {
		if(interestrate < 0 || interestrate > 6) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid tax value!");
			return 1;
		}
		server_interestrate = interestrate;
		format(query, sizeof(query), "UPDATE `misc` SET `interest` = %d",interestrate);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		format(query, sizeof(query), "* The interest rate is now at: %d",interestrate);
		SendClientMessage(playerid, COLOR_DARKGREEN, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setinterest [rate]");
	}
	return 1;
}