YCMD:news(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to speak on the news");
		return 1;
	}
	new string[256];
	new msg[256];
	new fid = GetPVarInt(playerid, "Faction");
	if(getPlayerFactionType(playerid) != EFactionType_SanNews) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in Fox News");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		ProxMessage(20.0, playerid, "* You hear static coming from the radio", COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		return 1;
	}
	if(!sscanf(params, "s[256]", msg)) {
		format(string, sizeof(string), "%s %s: %s",GetFactionName(fid), GetPlayerNameEx(playerid, ENameType_RPName_NoMask), msg);
		NewsMessage(COLOR_NEWS, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /news [message]");
	}
	return 1;
}
YCMD:putonair(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Puts a call on the air");
		return 1;
	}
	if(getPlayerFactionType(playerid) != EFactionType_SanNews) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in Fox News");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		ProxMessage(20.0, playerid, "* You hear static coming from the radio", COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		return 1;
	}
	new fid = GetPVarInt(playerid, "Faction");
	if(GetPVarType(playerid, "OnCall") != PLAYER_VARTYPE_NONE) {
		new caller = GetPVarInt(playerid, "OnCall");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* You are now on the air, anything you say will be recorded by the microphones.");
		SendClientMessage(caller, COLOR_LIGHTBLUE, "* You are now on the air, anything you say will be recorded by the microphones.");
		SetPVarInt(playerid, "OnAir", fid);
		SetPVarInt(caller, "OnAir", fid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't on the phone");
	}
	return 1;
}
YCMD:live(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Asks someone if you want to have a live conversation with them");
		return 1;
	}
	if(getPlayerFactionType(playerid) != EFactionType_SanNews) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in Fox News");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		ProxMessage(20.0, playerid, "* You hear static coming from the radio", COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		return 1;
	}
	new user;
	if(GetPVarType(playerid, "LiveChat") != PLAYER_VARTYPE_NONE) {
		endLiveChat(playerid);
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(playerid == user) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't do this to yourself!");
			return 1;
		}
		sendLiveOffer(playerid, user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /live [playerid/name]");
	}
	return 1;
}
endLiveChat(playerid) {
	new offerer = GetPVarInt(playerid, "LiveChat");
	//TogglePlayerControllableEx(playerid, 1);
	//TogglePlayerControllableEx(offerer, 1);
	DeletePVar(playerid, "LiveChat");
	DeletePVar(offerer, "LiveChat");
	return 1;
}
newsOnPlayerText(playerid, text[]) {
	new string[256];
	if(GetPVarType(playerid, "LiveChat") != PLAYER_VARTYPE_NONE) {
		if(IsRadioJammerActiveInArea(playerid)) {
			format(string, sizeof(string), "* You hear static coming from the radio");
			ProxMessage(20.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			return 1;
		}
		format(string, sizeof(string), "[Live] %s: %s", GetPlayerNameEx(playerid, ENameType_RPName), text);
		NewsMessage(COLOR_NEWS, string);
		return 1;
	}
	return 0;
}
handleLiveResponse(playerid, response) {
	new offerer = GetPVarInt(playerid, "LiveOffer");
	DeletePVar(playerid, "LiveOffer");
	if(!IsPlayerConnectEx(offerer)) {
		return 1;
	}
	new msg[128];
	if(response) {
		SetPVarInt(playerid, "LiveChat", offerer);
		SetPVarInt(offerer, "LiveChat", playerid);
		//TogglePlayerControllableEx(playerid, 0);
		//TogglePlayerControllableEx(offerer, 0);
		format(msg, sizeof(msg), "* You are now on the air, anything you say will be picked up by the microphone");
		SendClientMessage(offerer, COLOR_LIGHTBLUE, msg);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		format(msg, sizeof(msg), "* The user declined your live request", GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(offerer, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}
sendLiveOffer(playerid, user) {
	new msg[128];
	format(msg, sizeof(msg), "%s is offering you to a live conversation on the news. Do you accept?",GetPlayerNameEx(playerid, ENameType_RPName));
	SetPVarInt(user, "LiveOffer", playerid);
	ShowPlayerDialog(user, EFactionDialog_LiveOffer, DIALOG_STYLE_MSGBOX, "{00BFFF}Live Offer",msg, "Accept", "Reject");
}
NewsMessage(color, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(~EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoAds) {
				//SendClientMessage(i, color, msg);
				SendSplitClientMessage(i, color, msg, 0, 128);
			}
		}
	}
}