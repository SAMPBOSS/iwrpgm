
numMedicsOnline(onduty = 1) {
	new num, faction;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			faction = GetPVarInt(i, "Faction");
			if(getFactionType(faction) == EFactionType_EMS) {
				if(!onduty || isOnMedicDuty(i)) {
					num++;
				}
			}
		}
	}
	return num;
}
medicOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(isPlayerDying(playerid)) {
		finishDying(playerid);
	} else {
		destroyAllRelatedMedicPVars(playerid); //Fallback procedure
	}
}
destroyAllRelatedMedicPVars(playerid) {
	new Timer:timer = Timer:GetPVarInt(playerid, "MedicTimer");
	stop timer;
	ClearAnimations(playerid);
	DeletePVar(playerid, "PlayerDied");
	DeletePVar(playerid, "MedicTimer");
}
makePlayerDying(playerid, notcustom = 1) {
	if(isPlayerDying(playerid)) {
		finishDying(playerid);
		putPlayerInHospital(playerid);
		return 1;
	}
	if(GetPVarType(playerid, "MedicTimer") == PLAYER_VARTYPE_NONE) {
		new Timer:timer = repeat medicDeathTimer(playerid);
		SetPVarInt(playerid, "MedicTimer", _:timer);
	}
	loadSpecDetails(playerid);
	if(notcustom == 1) {
		ShowScriptMessage(playerid, "~r~Injured.~n~~w~Do /accept death or /service EMS",8000);
		ApplyAnimation(playerid, "KNIFE", "KILL_Knife_Ped_Die", 4.0, 0, 1, 1, 1, 0, 1);
	}
	SetPlayerHealthEx(playerid, 98.0);
	return 0;
}
revivePlayer(playerid) {
	new Timer:timer = Timer:GetPVarInt(playerid, "MedicTimer");
	stop timer;
	new Float:HP;
	GetPlayerHealth(playerid, HP);
	if(HP < 15.0) {
		HP = 15.0;
	} else {
		HP += 15.0;
	}
	SetPlayerHealthEx(playerid, HP);
	ClearAnimations(playerid);
	TogglePlayerControllableEx(playerid, 1);
	DeletePVar(playerid, "MedicTimer");
	clearTimesShot(playerid);
}
YCMD:revive(playerid, params[], help) {
	new Float:X, Float:Y, Float:Z, target;
	new faction = GetPVarInt(playerid, "Faction");
	if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
		if(getFactionType(faction) != EFactionType_EMS) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be a medic");
			return 1;
		}
		if(!isOnMedicDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't on medic duty!");
			return 1;
		}
	}
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
			if(target == playerid) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot revive yourself!");
				return 1;
			}
			GetPlayerPos(target, X, Y, Z);
			if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be around this person!");
				return 1;
			}
		}
		if(isPlayerDying(target)) {
			format(query, sizeof(query), "* %s takes out a defibrillator and shocks %s.",GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(target, ENameType_RPName));
			ProxMessage(30.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			revivePlayer(target);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This player is not dying.");
		}
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /revive [playerid]");
	}
	return 1;
	
}
YCMD:heal(playerid, params[], help) {
	new Float:X, Float:Y, Float:Z, target;
	new faction = GetPVarInt(playerid, "Faction");
	if(getFactionType(faction) != EFactionType_EMS) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a medic");
		return 1;
	}
	if(!isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't on medic duty!");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(target == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot heal yourself!");
			return 1;
		}
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be around this person!");
			return 1;
		}
		if(getPlayerDisease(target) != -1) {
			curePlayerDisease(target);
		}
		SetPlayerDrunkLevel(target, 0);
		SetPlayerHealthEx(target, 98.0);
		clearTimesShot(target);
		setDrugEffects(target, EDrugEffect_None); //Drug effects all gone after this
		format(query, sizeof(query), "* You healed %s.",GetPlayerNameEx(target, ENameType_RPName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		format(query, sizeof(query), "* You were healed by %s.",GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(target, COLOR_LIGHTBLUE, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /heal [playerid]");
	}
	return 1;
}
isPlayerDying(playerid) {
	return GetPVarType(playerid, "MedicTimer") != PLAYER_VARTYPE_NONE;
}
//called once every second as a count down
timer medicDeathTimer[5000](playerid) {
	new Float:HP;
	GetPlayerHealth(playerid, HP);
	if(HP > 3.0) {
		HP -= 2.0;
		SetPlayerHealthEx(playerid, HP);
	} else {
		finishDying(playerid);
	}
}
finishDying(playerid) {
	new Timer:timer = Timer:GetPVarInt(playerid, "MedicTimer");
	stop timer;
	new msg[128];
	ClearAnimations(playerid);
	putPlayerInHospital(playerid);
	format(msg, sizeof(msg), "* %s canceled their paramedic request.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
	SendMedicMessage(COLOR_LIGHTBLUE, 1, msg);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "Because your died, your paramedic request was canceled.");
	DeletePVar(playerid, "MedicRequest");
	DeletePVar(playerid, "MedicTimer");
}
SendMedicMessage(color, onduty, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			new faction = GetPVarInt(i, "Faction");
			if(getFactionType(faction) == EFactionType_EMS && (!onduty || isOnMedicDuty(i))) {
				SendClientMessage(i, color, msg);
			}
		}
	}
	SendBigFactionEarsMessage(color, msg);
}
isOnMedicDuty(playerid) {
	return GetPVarInt(playerid, "MedicDuty");
}
onTogMedicDuty(playerid) {
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(!isInsideHospital(playerid) && !isAtLAFD(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a hospital or at LAFD!");
			return 1;
		}
	}
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	new string[128];
	if(GetPVarType(playerid, "MedicDuty") == PLAYER_VARTYPE_NONE) {
		format(string, sizeof(string), "* %s %s goes on duty.",getFactionRankName(faction, rank),GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(10.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPVarInt(playerid, "MedicDuty", 1);
		saveGuns(playerid, "MedicGuns");
		SetPlayerSkin(playerid, 274);
		showEquipMenu(playerid);
	} else {
		restoreGuns(playerid, "MedicGuns");
		format(string, sizeof(string), "* %s %s goes off duty.",getFactionRankName(faction, rank),GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(10.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
		DeletePVar(playerid, "MedicDuty");
	}
	ProxMessage(10.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 0;
}
isMedic(playerid) {
	new faction = GetPVarInt(playerid, "Faction");
	if(getFactionType(faction) == EFactionType_EMS) {
		return 1;
	}
	return 0;
}