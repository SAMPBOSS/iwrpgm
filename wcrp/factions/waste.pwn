#define MAX_WASTEOBJS 		500
#define MAX_CAPACITY		60 //The maximum amount of objects a trash can can hold
#define WASTE_STREAM_DIST	50
#define MAX_WASTE_DESC		128
#define MAX_GARBAGE_TODO	15 //The maximum amount of garbage they've to pickup before a paycheck
#define GARBAGE_BAG_MDL		1264
#define MAX_DROP_POINTS		3
#define TRASHMAN_SKIN		260
//Return coords
#define TRASH_RETURNX		2324.80
#define TRASH_RETURNY		-2081.68
#define TRASH_RETURNZ		13.54

/* Pickup States */
enum {
	STATE_NONE = -1,
	STATE_EXITING_VEH = 0,
	STATE_PICKEDTRASH,
	STATE_DELIVERING,
	STATE_DROPPOINT,
	STATE_ATTRASHCAR,
}

/* Forwards */
forward setupWasteObject(playerid, objModel, Float:X, Float:Y, Float:Z, Float:RotX, Float:RotY, Float:RotZ, vw, int);
forward OnSetupSuccess(playerid);
forward wastePlace(playerid);
forward wasteEdit(playerid);
forward wasteDelete(playerid);
forward showWasteMenu(playerid);
/* End of forwards */

enum {
	EWaste_Menu = EWaste_Base + 1,
	EWaste_DropEx,
	EWaste_TakeMenu,
	EWaste_DropChoose,
};

enum EWasteObjsInfo {
	objSQLID,
	objID, //Model ID
	Float:objPosX,
	Float:objPosY,
	Float:objPosZ,
	Float:objAngX,
	Float:objAngY,
	Float:objAngZ,
	objVW,
	objIntID,
	objStreamID, //Streamer ID so we can destroy it later
	Text3D:objTextLabel,
};
new WasteObjects[MAX_WASTEOBJS][EWasteObjsInfo];

enum EWasteObjsInTrash {
	itemSQLID,
	ESafeItemType:itemType, //Item type, gun, money, etc
	itemExtraSQLID,
	itemObjID,
	itemAmount,
};
new ObjsInTrash[MAX_WASTEOBJS][MAX_CAPACITY][EWasteObjsInTrash];

enum ETrashDropPoints {
	EDropBeingUsed,
	Float:EDropSpotX,
	Float:EDropSpotY,
	Float:EDropSpotZ,
	Float:EDropAngleMin,
	Float:EDropAngleMax,
};
new TrashDropPoints[MAX_DROP_POINTS][ETrashDropPoints] = {
	{0, 2188.9438, -1995.9170, 13.5469, -14.16, 14.16}, 
	{0, 2184.0405, -1991.3450, 13.5469, -337.78, 337.78},
	{0, 2176.0862, -1982.8711, 13.5513, -333.18, 333.18}
};

enum ETrashBagObjs {
	EObjID,
};
new DroppedBags[MAX_GARBAGE_TODO][MAX_DROP_POINTS][ETrashBagObjs];

new ItemsInWaste[][ESafeSingleItems] = {
	{"None","None",ESafeItemType:-1},
	{"Cash","Money",ESafeItemType_Money},
	{"Pot","Pot",ESafeItemType_Pot},
	{"Coke","Coke",ESafeItemType_Coke},
	{"Gun","Unused",ESafeItemType_Gun},
	{"Meth","Meth",ESafeItemType_Meth},
	{"Materials A","MatsA",ESafeItemType_MatsA},
	{"Materials B","MatsB",ESafeItemType_MatsB},
	{"Materials C","MatsC",ESafeItemType_MatsC},
	{"Special Item","SpecialItem",ESafeItemType_SpecialItem}
};

//Player variables
new ObjEditMode[MAX_PLAYERS] = 0; //ObjEditMode(int) - 1 to edit/move the trash object, 2 to delete
new TrashCanIndex[MAX_PLAYERS] = -1;
new TotalTrashDone[MAX_PLAYERS] = 0;
new TrashPickupState[MAX_PLAYERS] = -1;
new TrashTimer[MAX_PLAYERS] = 0;

enum EBeenTrashCan {
	ETrashCanSQLID,
};
new TrashCanArray[MAX_PLAYERS][MAX_WASTEOBJS][EBeenTrashCan];

enum ETrashMDLInfo {
	ETrashCanModelID,
	ETrashCanRank,
};

new TrashCanMDLS[][ETrashMDLInfo] = {{1300, 4}, {1574, 4}, {1549, 4}, {3035, 4}, {1359, 4}, {1337, 4}, {1333, 4}, {1332, 4}, {1328, 4}};

enum {
	WasteType_Create,
	WasteType_Move,
	WasteType_Remove,
}

enum E_WasteMenu {
	E_DialogOptionText[128],
	E_WasteEditType,
	E_DoWasteCallBack[128],
	E_NeedRank,
}

enum EWasteMenuState {
	EWasteX_CurPage,
	EWasteX_RemainingCount,
	EWasteX_PageCount,
	EWasteX_InPreviewMode,
};
new WasteMenuState[MAX_PLAYERS][EWasteMenuState];

ESafeItemType:wFindItemType(index) {
	if(index < 0 || index > sizeof(ItemsInWaste)) return ESafeItemType:-1;
	return ItemsInWaste[index][ESafeSType];
}
WGetItemName(ESafeItemType:item, dst[], dstlen) {
	for(new i=0;i<sizeof(ItemsInWaste);i++) {
		if(ItemsInWaste[i][ESafeSType] == item) {
			format(dst, dstlen, "%s",ItemsInWaste[i][ESafeSItemName]);
			return 1;
		}
	}
	return 0;
}
WGetItemPvarName(ESafeItemType:item, dst[], dstlen) {
	for(new i=0;i<sizeof(ItemsInWaste);i++) {
		if(ItemsInWaste[i][ESafeSType] == item) {
			format(dst, dstlen, "%s",ItemsInWaste[i][ESafeSItemPVar]);
			return 1;
		}
	}
	return 0;
}
new WasteDialogMenu[][E_WasteMenu] = {
	{"Place A Trashcan", WasteType_Create, "wastePlace", 4},
	{"Move A Trashcan", WasteType_Move, "wasteEdit", 5},
	{"Remove A Trashcan", WasteType_Remove, "wasteDelete", 6}
};

wasteOnGameModeInit() {
	loadWasteObjects();
}
loadWasteObjects() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`, `objectid`, `objPosX`, `objPosY`, `objPosZ`, `objAngX`, `objAngY`, `objAngZ`, `objVW`, `objIntID` FROM `wasteobjects`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadWasteObjects", "");
}
forward OnLoadWasteObjects();
public OnLoadWasteObjects() {
	new rows, fields, index;
	new id_string[64];
	new itemdesc[128];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		//if(WasteObjects[i][objSQLID] != 0) continue; (it makes no sense to have a continue here? We're actually "reloading" the trash cans.
		index = findFreeWasteSlot();
		cache_get_row(i, 0, id_string); //unused but whatever
		WasteObjects[index][objSQLID] = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		WasteObjects[index][objID] = strval(id_string);
		
		cache_get_row(i, 2, id_string);
		WasteObjects[index][objPosX] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		WasteObjects[index][objPosY] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		WasteObjects[index][objPosZ] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		WasteObjects[index][objAngX] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		WasteObjects[index][objAngY] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		WasteObjects[index][objAngZ] = floatstr(id_string);
		
		cache_get_row(i, 8, id_string);
		WasteObjects[index][objVW] = strval(id_string);
		
		cache_get_row(i, 9, id_string);
		WasteObjects[index][objIntID] = strval(id_string);
		
		ResetTrashItems(index); //We're using this to initialize
		getWasteTextLabel(index, itemdesc, MAX_WASTE_DESC);
		WasteObjects[index][objStreamID] = CreateDynamicObject(WasteObjects[index][objID], WasteObjects[index][objPosX], WasteObjects[index][objPosY], WasteObjects[index][objPosZ], WasteObjects[index][objAngX], WasteObjects[index][objAngY], WasteObjects[index][objAngZ], WasteObjects[index][objVW], WasteObjects[index][objIntID]);
		WasteObjects[index][objTextLabel] = CreateDynamic3DTextLabel(itemdesc, 0x00FF00AA, WasteObjects[index][objPosX], WasteObjects[index][objPosY], WasteObjects[index][objPosZ]+1, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, WasteObjects[index][objVW], WasteObjects[index][objIntID]);
	}
	return 1;
}
getWasteTextLabel(index, dst[], dstlen) {
	new totalItems, Float:percent;
	totalItems = getTotalItemsInsideTrash(index);
	percent = ((float(totalItems) / float(MAX_CAPACITY)) * 100);
	format(dst, dstlen, "Trash\n Use \"/trash\" to interact\n Max Capacity: %d\n Filled: %.2f\%/100\%", MAX_CAPACITY, percent > 100.0 ? 100.0 : percent);
}
wasteOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused listitem
	#pragma unused inputtext
	new msg[128];
	dialogstr[0] = 0;
	switch(dialogid) {
		case EWaste_Menu: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the waste management menu.");
				return 1;
			}
			preListItemWasteCheck(playerid, listitem);
		}
		case EWaste_DropEx: {
			showWasteTakeMenu(playerid,!response);
		}
		case EWaste_TakeMenu: {
			SetPVarInt(playerid, "WItemIndex", listitem);
			if(GetPVarInt(playerid, "TrashTake") != 1) {
				if(response) {
					wasteDropItemMenu(playerid);
					return 1;
				} 
				return 1;
			}
			if(response) {
				new tIndex = TrashCanIndex[playerid];
				if(ObjsInTrash[tIndex][listitem][itemType] == ESafeItemType:-1) {
					SendClientMessage(playerid, X11_TOMATO_2, "There's nothing in this slot!");
					return 1;
				} else if(ObjsInTrash[tIndex][listitem][itemType] == ESafeItemType_Gun) {
					new gun, ammo, slot;
					decodeWeapon(ObjsInTrash[tIndex][listitem][itemAmount], gun, ammo);
					slot = GetWeaponSlot(gun);
					new curgun, curammo;
					GetPlayerWeaponDataEx(playerid, slot, curgun, curammo);
					if(curgun != 0) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are already holding a weapon in this slot!");
						return 1;
					}
					if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
						return 1;
					} else {
						GivePlayerWeaponEx(playerid, gun, ammo);
					}
					format(msg, sizeof(msg), "* %s takes something out the trash can.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully taken this item from the trash can");
					RemoveItemInTrash(tIndex, listitem);
					wasteUpdateTextLabel(tIndex);
					TrashCanIndex[playerid] = -1;
					return 1;
				} else if(ObjsInTrash[tIndex][listitem][itemType] == ESafeItemType_SpecialItem) { //special item
					if(HasSpecialItem(playerid)) {
						SendClientMessage(playerid, X11_TOMATO_2, "You are already holding a special item!");
						return 1;
					}
					GivePlayerItem(playerid, ObjsInTrash[tIndex][listitem][itemAmount]);
					RemoveItemInTrash(tIndex, listitem);
					wasteUpdateTextLabel(tIndex);
					TrashCanIndex[playerid] = -1;
					return 1;
				}
				new itemname[64];
				WGetItemPvarName(ESafeItemType:ObjsInTrash[tIndex][listitem][itemType], itemname, sizeof(itemname));
				new amount = GetPVarInt(playerid, itemname);
				amount += ObjsInTrash[tIndex][listitem][itemAmount];
				SetPVarInt(playerid, itemname, amount);

				GiveMoneyEx(playerid, 0); //sync money
				format(msg, sizeof(msg), "* %s takes something out the trash can.", GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SendClientMessage(playerid, COLOR_DARKGREEN, "You have successfully taken this item from the trash can.");
				RemoveItemInTrash(tIndex, listitem);
				wasteUpdateTextLabel(tIndex);
				TrashCanIndex[playerid] = -1;
			}
		}
		case EWaste_DropChoose: {
			if(!response) return 1;
			listitem++;
			new tIndex = TrashCanIndex[playerid];
			new itemSlot = GetPVarInt(playerid, "WItemIndex");
			new amount;
			if(ObjsInTrash[tIndex][itemSlot][itemType] != ESafeItemType:-1) {
				SendClientMessage(playerid, X11_TOMATO_2, "There's something in this slot already.");
				return 1;
			}
			if(ItemsInWaste[listitem][ESafeSType] == ESafeItemType_Gun) {
				new gun, ammo, gslot;
				gslot = GetWeaponSlot(GetPlayerWeaponEx(playerid));
				GetPlayerWeaponDataEx(playerid, gslot ,gun, ammo);
				if(gun < 1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You aren't carrying any weapon on that slot.");
					return 1;
				}
				new slot = GetWeaponSlot(gun);
				GetPlayerWeaponDataEx(playerid, slot, gun, ammo);
				ObjsInTrash[tIndex][itemSlot][itemType] = ESafeItemType_Gun;
				ObjsInTrash[tIndex][itemSlot][itemAmount] = encodeWeapon(gun, ammo);
				RemovePlayerWeapon(playerid, gun);
				format(msg, sizeof(msg), "* %s drops something into the trash can.", GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SendClientMessage(playerid, COLOR_DARKGREEN, "You have dropped the gun in the trash can.");
				wasteUpdateTextLabel(TrashCanIndex[playerid]);
				TrashCanIndex[playerid] = -1;
				DeletePVar(playerid, "WItemIndex");
				return 1;
			}
			amount = GetPVarInt(playerid, ItemsInWaste[listitem][ESafeSItemPVar]);
			if(amount < 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have any of that so you can't drop it!");
				return 1;
			}
			//amount -= itemAmount;
			ObjsInTrash[tIndex][itemSlot][itemType] = wFindItemType(listitem);
			ObjsInTrash[tIndex][itemSlot][itemAmount] = amount;
			/*
			new itemname[64];
			WGetItemName(ESafeItemType:ObjsInTrash[tIndex][itemSlot][itemType], itemname, sizeof(itemname));
			format(msg, sizeof(msg), "Amount: %d, Name: %s", ObjsInTrash[tIndex][itemSlot][itemAmount], itemname);
			SendClientMessage(playerid, X11_WHITE, msg);
			*/
			SetPVarInt(playerid, ItemsInWaste[listitem][ESafeSItemPVar], 0);
			GiveMoneyEx(playerid, 0); //sync money
			format(msg, sizeof(msg), "* %s drops something into the trash can.", GetPlayerNameEx(playerid, ENameType_RPName));
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SendClientMessage(playerid, COLOR_DARKGREEN, "You have dropped the item in the trash can.");
			wasteUpdateTextLabel(tIndex);
			DeletePVar(playerid, "WItemIndex");
			TrashCanIndex[playerid] = -1;
		}
	}
	return 1;
}
wasteDropItemMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=1;i<sizeof(ItemsInWaste);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",ItemsInWaste[i][ESafeSItemName]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EWaste_DropChoose, DIALOG_STYLE_LIST, "{00BFFF}Trash Menu",dialogstr, "Drop", "Cancel");
}
showWasteTakeMenu(playerid,take) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new amount;
	new itemname[64];
	new tIndex = TrashCanIndex[playerid];
	for(new i=0;i<MAX_CAPACITY;i++) {
		if(ObjsInTrash[tIndex][i][itemType] == ESafeItemType:-1) { //none
			strcat(dialogstr, "None\n", sizeof(dialogstr));
		} else {
			new ESafeItemType:type = wFindItemType(_:ObjsInTrash[tIndex][i][itemType]);
			amount = ObjsInTrash[tIndex][i][itemAmount];
			if(type == ESafeItemType_Gun) {
				new gun, ammo;
				decodeWeapon(ObjsInTrash[tIndex][i][itemAmount], gun, ammo);
				GetWeaponNameEx(gun, itemname, sizeof(itemname));
				amount = ammo;
			} else {
				WGetItemName(ESafeItemType:ObjsInTrash[tIndex][i][itemType], itemname, sizeof(itemname));
			}
			if(type == ESafeItemType_SpecialItem) {
				format(tempstr, sizeof(tempstr), "%s - %s\n", itemname, GetCarryingItemName(amount));
			} else {
				format(tempstr, sizeof(tempstr), "%s - %s\n", itemname, getNumberString(amount));
			}
			strcat(dialogstr, tempstr, sizeof(dialogstr));
		}
	}
	SetPVarInt(playerid, "TrashTake", take);
	ShowPlayerDialog(playerid, EWaste_TakeMenu, DIALOG_STYLE_LIST, "{00BFFF}Trash Menu",dialogstr, take == 1 ? ("Take") : ("Drop"), "Cancel");
}
setupWasteMenuState(playerid, curpage = 1) {
	new remaining_amount, pages, numwaste;
	new start_index = (curpage-1) * (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);

	clearModelSelectMenu(playerid);

	for(new i=0,c=0,j=1;i<sizeof(TrashCanMDLS);i++) {
		if(c++ >= (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS)*j) {
			if(++j == curpage)
				start_index = i;
		}
		numwaste++;
	}

	clearModelSelectMenu(playerid);
	if(curpage < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}

	for(new i=start_index,x=0;x<numwaste&&i<sizeof(TrashCanMDLS);i++) {		
		addItemToModelSelectMenu(playerid, TrashCanMDLS[i][ETrashCanModelID]," "," ");
		x++;
	}

	if(start_index < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}

	remaining_amount = numwaste % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numwaste) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	WasteMenuState[playerid][EWasteX_CurPage] = curpage;
	WasteMenuState[playerid][EWasteX_PageCount] = pages;
	WasteMenuState[playerid][EWasteX_RemainingCount] = remaining_amount;
	WasteMenuState[playerid][EWasteX_InPreviewMode] = 0;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numwaste, "Waste Menu", page_str, "OnWasteMenuSelect", -25.0000, 0.0000, -55.2600);
}
forward OnWasteMenuSelect(playerid, action, modelid, title[]);
public OnWasteMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			WasteMenuState[playerid][EWasteX_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		modelid = WasteMenuState[playerid][EWasteX_InPreviewMode];
		new Float: X, Float: Y, Float: Z;
		GetPlayerPos(playerid, X, Y, Z);
		wasteCreateObj(playerid, modelid, X, Y, Z, 0.0, 0.0, 0.0, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SelectObject(playerid);
	} else if(action == EModelPreviewAction_Next) {
		setupWasteMenuState(playerid, WasteMenuState[playerid][EWasteX_CurPage] + 1);
	} else if(action == EModelPreviewAction_Back) {
		setupWasteMenuState(playerid, WasteMenuState[playerid][EWasteX_CurPage] - 1);
	} else if(action == EModelPreviewAction_Exit) {
		if(WasteMenuState[playerid][EWasteX_InPreviewMode] != 0) {
			setupWasteMenuState(playerid, WasteMenuState[playerid][EWasteX_CurPage]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
	return 1;
}
preListItemWasteCheck(playerid, index) {
	new rank = GetPVarInt(playerid, "Rank");
	if(getPlayerFactionType(playerid) == EFactionType_WasteMgmt) {
		if(rank < WasteDialogMenu[index][E_NeedRank]) {
			showWasteMenu(playerid);
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have permissions for this");
			return 1;
		}
	}
	CallLocalFunction(WasteDialogMenu[index][E_DoWasteCallBack],"d", playerid);
	return 1;
}
findFreeWasteSlot() {
	for(new i=0;i<sizeof(WasteObjects);i++) {
		if(WasteObjects[i][objSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
findFreePlaceInTrash(index) { //Finds a free id inside a trash container
	for(new j=0; j<MAX_CAPACITY; j++) {
		if(ObjsInTrash[index][j][itemSQLID] == 0) {
			return j;
		}
	}
	return -1;
}
getTotalItemsInsideTrash(index) { //Gets the amount of items inside a trash can
	new x;
	for(new j=0; j<MAX_CAPACITY; j++) {
		if(ObjsInTrash[index][j][itemType] != ESafeItemType:-1) {
			x++;
		}
	}
	return x;
}
getClosestWasteObj(playerid, Float:radi = 5.0) {
	for(new i=0;i<sizeof(WasteObjects);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, WasteObjects[i][objPosX], WasteObjects[i][objPosY], WasteObjects[i][objPosZ])) {
			if(WasteObjects[i][objSQLID] != 0) {
				return i;
			}
		}
	}
	return -1;
}
ResetTrashItems(index) {
	for(new j=0; j<MAX_CAPACITY; j++) {
		ObjsInTrash[index][j][itemSQLID] = 0;
		ObjsInTrash[index][j][itemObjID] = 0;
		ObjsInTrash[index][j][itemAmount] = 0;
		ObjsInTrash[index][j][itemType] = ESafeItemType:-1;
	}
}
RemoveItemInTrash(index, slotid) {
	ObjsInTrash[index][slotid][itemSQLID] = 0;
	ObjsInTrash[index][slotid][itemObjID] = 0;
	ObjsInTrash[index][slotid][itemAmount] = 0;
	ObjsInTrash[index][slotid][itemType] = ESafeItemType:-1;
}
stock wasteCreateObj(playerid, objModel, Float:X, Float:Y, Float:Z, Float:RotX, Float:RotY, Float:RotZ, int, vw) {
	new index = findFreeWasteSlot();
	if(index == -1) {
		//Max reached! Don't add
		#if debug
		printf("Max reached, couldn't find a new object slot");
		#endif
		return 1;
	}
	query[0] = 0;
	format(query, sizeof(query), "INSERT INTO `wasteobjects` (`objectid`, `objPosX`, `objPosY`, `objPosZ`, `objAngX`, `objAngY`, `objAngZ`, `objVW`, `objIntID`) VALUES (%d,%f,%f,%f,%f,%f,%f,%d,%d)",objModel, X, Y, Z, RotX, RotY, RotZ, vw, int);
	mysql_function_query(g_mysql_handle, query, true, "setupWasteObject", "ddffffffdd", playerid, objModel, X,Y,Z,RotX,RotY,RotZ, vw, int);
	return 1;
}
public setupWasteObject(playerid, objModel, Float:X, Float:Y, Float:Z, Float:RotX, Float:RotY, Float:RotZ, vw, int) {
	new index;
	index = findFreeWasteSlot();
	if(index == -1) {
		#if debug
		printf("Max reached, couldn't find a new object slot");
		#endif
		return 1;
	}
	new sqlid = mysql_insert_id();
	WasteObjects[index][objSQLID] = sqlid;
	WasteObjects[index][objPosX] = X;
	WasteObjects[index][objPosY] = Y;
	WasteObjects[index][objPosZ] = Z;
	WasteObjects[index][objAngX] = RotX;
	WasteObjects[index][objAngY] = RotY;
	WasteObjects[index][objAngZ] = RotZ;
	WasteObjects[index][objVW] = vw;
	WasteObjects[index][objIntID] = int;
	WasteObjects[index][objStreamID] = CreateDynamicObject(objModel, X,Y,Z, RotX, RotY, RotZ, vw, int, -1, WASTE_STREAM_DIST);
	ResetTrashItems(index); //We're using this to initialize
	wasteMoveTextLabel(index, X, Y, Z);
	OnSetupSuccess(playerid);
	return 1;
}
public OnSetupSuccess(playerid) {
	movePlayerBack(playerid, 2.0);
	wasteEdit(playerid);
}
wasteOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#pragma unused x
	#pragma unused y
	#pragma unused z
	#pragma unused modelid
	if(ObjEditMode[playerid] != 0) {
		new index = findTrashByObjID(objectid);
		new sqlid = findTrashSQLIDByObjID(objectid);
		if(index != -1) {
			if(ObjEditMode[playerid] == 1) {
				EditDynamicObject(playerid, objectid);
			} else if(ObjEditMode[playerid] == 2) {
				DeleteTrash(sqlid);
				SendClientMessage(playerid, X11_YELLOW, "Trash Object Removed!");
				CancelEdit(playerid);
				ObjEditMode[playerid] = 0;
			}
		}
	}
	return 0;	
}
wasteOnPlayerEditObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	query[0] = 0;//[128];
	if(response != EDIT_RESPONSE_FINAL) {
		return 0;
	}
	new sqlid = findTrashSQLIDByObjID(objectid);
	new index = findTrashByObjID(objectid);
	if(ObjEditMode[playerid] != 0) {
		if(index != -1) {
			WasteObjects[index][objPosX] = x;
			WasteObjects[index][objPosY] = y;
			WasteObjects[index][objPosZ] = z;
			WasteObjects[index][objAngX] = rx;
			WasteObjects[index][objAngY] = ry;
			WasteObjects[index][objAngZ] = rz;
			wasteMoveTextLabel(index, x, y, z);
			SendClientMessage(playerid, X11_TOMATO_2, "[Waste]: Position saved!");
			format(query, sizeof(query), "UPDATE `wasteobjects` SET `objPosX` = %f, `objPosY` = %f, `objPosZ` = %f, `objAngX` = %f, `objAngY` = %f, `objAngZ` = %f WHERE `id` = %d",x,y,z,rx,ry,rz,sqlid);
			SetDynamicObjectPos(objectid, x, y, z);
			SetDynamicObjectRot(objectid, rx, ry, rz);
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
		}
		ObjEditMode[playerid] = 0;
	}
	return 1;
}
stock DeleteTrash(sqlid) {
	query[0] = 0;//[256];
	if(sqlid == -1) {
		#if debug
		printf("Error, cannot delete this.");
		#endif
		return 1;
	}
	format(query, sizeof(query), "DELETE FROM `wasteobjects` WHERE `id` = %d",sqlid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroyTrash(sqlid);
	return 1;
}
DestroyTrash(sqlid) { //By SQLID
	for(new i=0;i<sizeof(WasteObjects);i++) {
		if(WasteObjects[i][objSQLID] == sqlid) {
			DestroyDynamicObject(WasteObjects[i][objStreamID]);
			wasteDeleteTextLabel(i);
			ResetTrashItems(i); //They should be removed once the trash is gone
			WasteObjects[i][objStreamID] = 0;
			WasteObjects[i][objSQLID] = 0;
		}
	}
	return 1;
}
findTrashByObjID(objectid) {
	for(new i = 0; i < sizeof(WasteObjects); i++) {
		if(WasteObjects[i][objStreamID] == objectid) {
			#if debug
			printf("findTrashByObjID(%d)",i);
			#endif
			return i;
		}
	}
	return -1;
}
findTrashSQLIDByObjID(objid) {
	for(new i=0;i<sizeof(WasteObjects);i++) {
		if(WasteObjects[i][objStreamID] == objid) {
			#if debug
			printf("findTrashSQLIDByObjID(%d)",WasteObjects[i][objSQLID]);
			#endif
			return WasteObjects[i][objSQLID];
		}
	}
	return -1;
}
public wastePlace(playerid) {
	showWasteTrashMenu(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO]: Select a trash can from the dialog.");
	return 1;
}
public wasteEdit(playerid) {
	ObjEditMode[playerid] = 1;
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO]: Select the object to edit it.");
	return 1;
}
public wasteDelete(playerid) {
	ObjEditMode[playerid] = 2;
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "[INFO]: Select the object to delete.");
	return 1;
}
public showWasteMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(WasteDialogMenu);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",WasteDialogMenu[i][E_DialogOptionText]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EWaste_Menu, DIALOG_STYLE_LIST, "Waste Management Menu",dialogstr,"Ok", "Cancel");
}
showWasteTrashMenu(playerid) {
	setupWasteMenuState(playerid, 1);
}
wasteOnPlayerDisconnect(playerid) {
	ObjEditMode[playerid] = 0;
	TrashCanIndex[playerid] = -1;
	TotalTrashDone[playerid] = 0;
	TrashPickupState[playerid] = -1;
	wasteDelBagsTimer(playerid);
	if(GetPVarType(playerid, "TrashRoute") != PLAYER_VARTYPE_NONE) {
		if(TrashPickupState[playerid] == STATE_DROPPOINT) {
			new index = GetPVarInt(playerid, "TrashRouteIndex");
			makeTrashSpotAvailable(index, 1);
		}
		DeletePVar(playerid, "TrashRoute");
		DeletePVar(playerid, "TrashRouteIndex");
		DeletePVar(playerid, "TrashVehID");
		//DisablePlayerCheckpoint(playerid);
		DestroyAllPlayerCPS(playerid);
	}
	removeWasteBagFromPlayer(playerid);
}
wasteMoveTextLabel(index, Float: X, Float: Y, Float: Z) { //Simply deletes a text label and recreates it at a new pos, VW and INTID are off the index itself
	new itemdesc[128];
	wasteDeleteTextLabel(index);
	getWasteTextLabel(index, itemdesc, MAX_WASTE_DESC);
	WasteObjects[index][objTextLabel] = CreateDynamic3DTextLabel(itemdesc, 0x00FF00AA, X, Y, Z+1, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, WasteObjects[index][objVW], WasteObjects[index][objIntID]);
}
wasteDeleteTextLabel(index) {
	if(WasteObjects[index][objTextLabel] != Text3D:0) {
		DestroyDynamic3DTextLabel(WasteObjects[index][objTextLabel]);
		WasteObjects[index][objTextLabel] = Text3D:0;
	}
	return 1;
}
wasteUpdateTextLabel(index) {
	new itemdesc[128];
	getWasteTextLabel(index, itemdesc, MAX_WASTE_DESC);
	if(WasteObjects[index][objTextLabel] != Text3D:0) {
		UpdateDynamic3DTextLabelText(WasteObjects[index][objTextLabel],0x00FF00AA, itemdesc);
	}
}
YCMD:placetrashcan(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to place / edit a trash can");
		return 1;
	}
	if(getPlayerFactionType(playerid) != EFactionType_WasteMgmt) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in the Waste Management Faction");
		return 1;
	}
	showWasteMenu(playerid);
	return 1;
}
showTrashDropExMenu(playerid) {
	ShowPlayerDialog(playerid, EWaste_DropEx, DIALOG_STYLE_MSGBOX, "{00BFFF}Trash Menu","What would you like to do?", "Drop", "Examine");
}
findNewTrashCan(playerid, &sqlid = 0) {
	new index;
	new Float:oldDistance = 99999.000+1,Float:newdistance;
	for(new i=0; i<sizeof(WasteObjects); i++) {
		//if(getTotalItemsInsideTrash(i) != 0) {
		if(WasteObjects[i][objSQLID] != 0) {
			if(WasteObjects[i][objVW] == 0 && WasteObjects[i][objIntID] == 0) { //Don't get a trash can that may be in an interior
				if(TrashCanArray[playerid][i][ETrashCanSQLID] != WasteObjects[i][objSQLID]) { //If we haven't to that trash can..
					newdistance = GetPlayerDistanceFromPoint(playerid, WasteObjects[i][objPosX], WasteObjects[i][objPosY], WasteObjects[i][objPosZ]); //Get the distance for that trash can
					if(newdistance < oldDistance) { //If it's closer than the last one
						oldDistance = newdistance;
						index = i;
					}
				}
			}
		}
		//}
	}
	TrashCanArray[playerid][index][ETrashCanSQLID] = WasteObjects[index][objSQLID];
	sqlid = WasteObjects[index][objSQLID];
	return index;
}
wipeTrashCanHistory(playerid) {
	for(new j=0; j<sizeof(TrashCanArray); j++) {
		TrashCanArray[playerid][j][ETrashCanSQLID] = 0;
	}
	return 1;
}
/* Job */
startClean(playerid) {
	new rindex, sqlid;
	wipeTrashCanHistory(playerid);
	rindex = findNewTrashCan(playerid, sqlid);
	if(!sqlid) {
		wipeTrashCanHistory(playerid);
		rindex = findNewTrashCan(playerid); //try again..
	}
	if(GetPVarType(playerid, "TrashRouteIndex") == PLAYER_VARTYPE_NONE) {
		SetPVarInt(playerid, "TrashRoute",rindex);
		SetPVarInt(playerid, "TrashRouteIndex",rindex);
		SetPVarInt(playerid, "TrashVehID", GetPlayerVehicleID(playerid));
		SetSinglePlayerCP(playerid, WasteObjects[rindex][objPosX], WasteObjects[rindex][objPosY], WasteObjects[rindex][objPosZ], ECPType_Job, GetPVarInt(playerid, "Job"), 2.0);
		PlayerPlaySound(playerid, 1056, 0.0, 0.0, 0.0);
		ShowScriptMessage(playerid, "Go to the ~g~garbage ~w~spot.", 5000);
		if(GetPVarInt(playerid,"Sex") == 0) { //Only if they're male
			SetPlayerSkin(playerid, TRASHMAN_SKIN);
		}
		ApplyAnimation(playerid, "CAR", "Sit_relaxed", 4.1, 1, 0, 0, 0, 0);
	} else {
		wasteFinalizeTrashJob(playerid, 0);
	}
	return 1;
}
isAtTrashPoint(playerid) {
	for(new i=0; i<sizeof(TrashDropPoints); i++) {
		if(IsPlayerInRangeOfPoint(playerid, 5.0, TrashDropPoints[i][EDropSpotX], TrashDropPoints[i][EDropSpotY], TrashDropPoints[i][EDropSpotZ])) {
			return 1;
		}
	}
	return 0;
}
wasteOnManageTrashJob(playerid, checkpointid) {
	#if debug
	printf("wasteOnManageTrashJob has been called");
	#endif
	new index = GetPVarInt(playerid, "TrashRouteIndex");
	new Float: X, Float: Y, Float: Z;
	new vehicleid = GetPVarInt(playerid, "TrashVehID");
	if(!IsCheckPointForJob(checkpointid, GetPVarInt(playerid, "Job"))) {
		wasteRecalcJobPoint(playerid, vehicleid);
	}
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) { 
		onWasteManageDecideState(playerid, index);
		return 1;
	} else { //OnFoot
		if(TrashPickupState[playerid] != STATE_PICKEDTRASH && TrashPickupState[playerid] != STATE_DROPPOINT && TrashPickupState[playerid] != STATE_DELIVERING) { //This happens when we grab trash from a container as long as we haven't picked up all trash bags
			GetPosBehindVehicle(vehicleid, 5.0, X, Y, Z);
			SetSinglePlayerCP(playerid, X, Y, Z, ECPType_Job, GetPVarInt(playerid, "Job"), 2.0);
			giveOrRemoveWasteBag(playerid);
			ResetTrashItems(index);
			wasteUpdateTextLabel(index);
			TrashPickupState[playerid] = STATE_PICKEDTRASH;
			ApplyAnimation(playerid, "BOMBER", "BOM_PLANT_IN", 5.0, 0, 0, 0, 0, 450);
		} else { //The state changed to picked trash so we call this
			if(TotalTrashDone[playerid] < MAX_GARBAGE_TODO) { //This happens when we drop the trash in the truck
				new rindex, sqlid;
				rindex = findNewTrashCan(playerid, sqlid);
				if(!sqlid) {
					if(wipeTrashCanHistory(playerid)) {
						rindex = findNewTrashCan(playerid); //try again.
					}
				}
				ShowScriptMessage(playerid, "Get in the trash vehicle and continue.", 5000);
				SetPVarInt(playerid, "TrashRoute",rindex);
				SetPVarInt(playerid, "TrashRouteIndex",rindex);
				SetSinglePlayerCP(playerid, WasteObjects[rindex][objPosX], WasteObjects[rindex][objPosY], WasteObjects[rindex][objPosZ], ECPType_Job, GetPVarInt(playerid, "Job"), 2.0);
				giveOrRemoveWasteBag(playerid);
				TotalTrashDone[playerid]++;
				TrashPickupState[playerid] = STATE_NONE;
			} else {
				if(TrashPickupState[playerid] == STATE_DROPPOINT || TrashPickupState[playerid] == STATE_DELIVERING) {
					ShowScriptMessage(playerid, "Please get in the ~r~trashmaster~w~.", 5000);
					return 1;
				}
				sendToTrashPoint(playerid);
			}
		}
	}
	PlayerPlaySound(playerid, 1056, 0.0, 0.0, 0.0);
	return 1;
}
wasteRecalcJobPoint(playerid, vehicleid) { //Fail check (If they enter a checkpoint that has nothing to do with the job, it gets handled here)
	new Float: X, Float: Y, Float: Z;
	ShowScriptMessage(playerid, "That ~r~checkpoint ~w~has nothing to do with the job. Recalculating..", 5000);
	//DisablePlayerCheckpoint(playerid);
	DestroyAllPlayerCPS(playerid);
	switch(TrashPickupState[playerid]) {
		case STATE_NONE: {
			if(vehicleid != 0) {
				ShowScriptMessage(playerid, "~w~Get in the ~r~vehicle ~w~it has been marked on your map.", 5000);
				GetVehiclePos(vehicleid, X, Y, Z);
				SetSinglePlayerCP(playerid, X, Y, Z, ECPType_Job, GetPVarInt(playerid, "Job"));
			} else {
				ShowScriptMessage(playerid, "You must be in a ~r~Trash Cleaner vehicle~w~, do ~r~/jobcar ~w~near the vehicle point to spawn a trash car if none are available.", 5000);
				return 1;
			}
		}
		case STATE_DROPPOINT: {
			sendToTrashPoint(playerid);
		}
		case STATE_PICKEDTRASH: {
			if(TotalTrashDone[playerid] < MAX_GARBAGE_TODO) { //This happens when we drop the trash in the truck
				new rindex, sqlid;
				rindex = findNewTrashCan(playerid, sqlid);
				if(!sqlid) {
					if(wipeTrashCanHistory(playerid)) {
						rindex = findNewTrashCan(playerid); //try again.
					}
				}
				ShowScriptMessage(playerid, "Get in the trash vehicle and continue.", 5000);
				SetPVarInt(playerid, "TrashRoute",rindex);
				SetPVarInt(playerid, "TrashRouteIndex",rindex);
				SetSinglePlayerCP(playerid, WasteObjects[rindex][objPosX], WasteObjects[rindex][objPosY], WasteObjects[rindex][objPosZ], ECPType_Job, GetPVarInt(playerid, "Job"));
				giveOrRemoveWasteBag(playerid);
			} else {
				ShowScriptMessage(playerid, "~w~Get in the ~r~vehicle ~w~it has been marked on your map.", 5000);
				GetVehiclePos(vehicleid, X, Y, Z);
				SetSinglePlayerCP(playerid, X, Y, Z, ECPType_Job, GetPVarInt(playerid, "Job"));
			}
		}
		case STATE_DELIVERING: {
			if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
				ShowScriptMessage(playerid, "Please ~r~return ~w~the ~r~truck ~w~to collect your ~r~paycheck~w~.", 5000);
				SetSinglePlayerCP(playerid, TRASH_RETURNX, TRASH_RETURNY, TRASH_RETURNZ, ECPType_Job, GetPVarInt(playerid, "Job"));
			} else {
				ShowScriptMessage(playerid, "~w~Get in the ~r~vehicle ~w~it has been marked on your map.", 5000);
				GetVehiclePos(vehicleid, X, Y, Z);
				SetSinglePlayerCP(playerid, X, Y, Z, ECPType_Job, GetPVarInt(playerid, "Job"));
			}
		}
	}
	return 1;
}
sendToTrashPoint(playerid) {
	DestroyAllPlayerCPS(playerid);
	ShowScriptMessage(playerid, "Please take the ~r~trash ~w~to the dump spot.", 5000);
	new x = RandomEx(0, sizeof(TrashDropPoints));
	SetSingleAngledPlayerCP(playerid, TrashDropPoints[x][EDropSpotX], TrashDropPoints[x][EDropSpotY], TrashDropPoints[x][EDropSpotZ], TrashDropPoints[x][EDropAngleMax], ECPType_Job, GetPVarInt(playerid, "Job"));
	removeWasteBagFromPlayer(playerid);
	TrashPickupState[playerid] = STATE_DROPPOINT;
	SetPVarInt(playerid, "TrashRouteIndex", x);
}
onWasteManageDecideState(playerid, index) {
	switch(TrashPickupState[playerid]) {
		case STATE_DELIVERING: {
			wasteFinalizeTrashJob(playerid);
		}
		case STATE_DROPPOINT: {
			//index = GetPVarInt(playerid, "TrashRouteIndex");
			if(TrashDropPoints[index][EDropBeingUsed]) {
				ShowScriptMessage(playerid, "This spot is not ~r~available, ~w~please wait until it becomes ~r~available~w~.", 5000);
				return 1;
			}
			new Float:vehAngle;
			GetVehicleZAngle(GetPlayerVehicleID(playerid), vehAngle);
			if(isWithinRange(vehAngle, TrashDropPoints[index][EDropAngleMin], TrashDropPoints[index][EDropAngleMax])) {
				onReachDropPoint(playerid, index);
				return 1;
			} else {
				SetSingleAngledPlayerCP(playerid, TrashDropPoints[index][EDropSpotX], TrashDropPoints[index][EDropSpotY], TrashDropPoints[index][EDropSpotZ], TrashDropPoints[index][EDropAngleMax], ECPType_Job, GetPVarInt(playerid, "Job"));
				ShowScriptMessage(playerid, "The vehicle is ~r~not aligned properly~w~, please correct it. It should be facing ~r~backwards~w~.", 5000);
				return 1;
			}
		}
		default: {
			DestroyAllPlayerCPS(playerid);
			ShowScriptMessage(playerid, "Get off the trash vehicle and ~r~grab ~w~the ~r~bag~w~.", 5000);
			SetSinglePlayerCP(playerid, WasteObjects[index][objPosX], WasteObjects[index][objPosY], WasteObjects[index][objPosZ], ECPType_Job, GetPVarInt(playerid, "Job"));
		}
	}
	return 1;
}
onReachDropPoint(playerid, index) {
	new Float: CamX, Float: CamY, Float: CamZ, Float:X, Float: Y, Float: Z;
	GetPlayerPos(playerid, X, Y, Z);
	GetPosBehindVehicle(GetPlayerVehicleID(playerid), 8.0, CamX, CamY, CamZ);
	SetPlayerCameraPos(playerid, 2196.5464, -2003.3713, 17.8);
	InterpolateCameraPos(playerid, 2196.5464, -2003.3713, 17.8, 2176.85, -2005.0, 25.8, 2000 * MAX_GARBAGE_TODO, CAMERA_MOVE);
	InterpolateCameraLookAt(playerid, CamX, CamY, CamZ, CamX, CamY, CamZ, 2000 * MAX_GARBAGE_TODO, CAMERA_MOVE);
	//SetPlayerCameraLookAt(playerid, CamX, CamY, CamZ);
	//TrashTimer[playerid] = SetTimerEx("wasteDropBagsCamera", 2000, true, "dd", playerid, index);
	wasteDropBagsCamera(playerid, index);
	TogglePlayerControllableEx(playerid, false);
	//Docamerastuff here
	return 1;
}
forward wasteDropBagsCamera(playerid, pointid);
public wasteDropBagsCamera(playerid, pointid) {
	//DroppedBags[MAX_DROP_POINTS][MAX_GARBAGE_TODO][ETrashDropPoints];
	new msg[64];
	new Float:X, Float:Y, Float:Z, Float: vehAngle;
	if(wasteCountBagsAtPoint(pointid) >= MAX_GARBAGE_TODO) {
		ShowScriptMessage(playerid, "Please ~r~return ~w~the ~r~truck ~w~to collect your ~r~paycheck~w~.", 5000);
		SetSinglePlayerCP(playerid, TRASH_RETURNX, TRASH_RETURNY, TRASH_RETURNZ, ECPType_Job, GetPVarInt(playerid, "Job"));
		removeWasteBagFromPlayer(playerid);
		TrashPickupState[playerid] = STATE_DELIVERING;
		makeTrashSpotAvailable(pointid, 1);
		destroyAllFlyBags(pointid); //Destroy all the bags at that pointid as well
		//wasteDelBagsTimer(playerid);
		TogglePlayerControllableEx(playerid, true); //Re-allow control
		SetCameraBehindPlayer(playerid);
	} else {
		new index = getFreeBagAtPoint(pointid);
		GetPosBehindVehicle(GetPlayerVehicleID(playerid), 5.0, X, Y, Z);
		GetVehicleZAngle(GetPlayerVehicleID(playerid), vehAngle);
		DroppedBags[index][pointid][EObjID] = CreateDynamicObject(GARBAGE_BAG_MDL,X,Y,Z+0.5,0.0,0.0,vehAngle);
		MoveDynamicObject(DroppedBags[index][pointid][EObjID],
		X += ( -5.5 * floatsin(vehAngle, degrees) * floatcos(5.0 + RandomFloat(0.005,0.015), degrees) ),
		Y += ( -5.5 * floatcos(vehAngle, degrees) * floatcos(5.0 + RandomFloat(0.005,0.015), degrees) ),
		Z = ( TrashDropPoints[pointid][EDropSpotZ] ),
		15.0);
		format(msg, sizeof(msg), "~r~%d bag(s) ~w~to go..", MAX_GARBAGE_TODO-wasteCountBagsAtPoint(pointid));
		UpdatePlayerObjectsAtPos(playerid);
		PlayerPlaySound(playerid, 1056, 0.0, 0.0, 0.0);
		ShowScriptMessage(playerid, msg, 1000);
		if(TrashPickupState[playerid] == STATE_DROPPOINT) { //This is so bad lol
			SetTimerEx("wasteDropBagsCamera", 2000, false, "dd", playerid, pointid);
		}
	}
	return 1;
}
findBagIDByObjID(objid, &pointid = -1) {
	for(new p=0; p<sizeof(TrashDropPoints); p++) {
		for(new i=0; i<sizeof(DroppedBags); i++) {
			if(DroppedBags[i][p][EObjID] == objid) {
				pointid = p;
				return i;
			}
		}
	}
	return -1;
}
destroyAllFlyBags(index) {
	for(new i=0; i<sizeof(DroppedBags); i++) {
		if(DroppedBags[i][index][EObjID] != 0) {
			DestroyDynamicObject(DroppedBags[i][index][EObjID]);
			DroppedBags[i][index][EObjID] = 0;
		}
	}
}
wasteCountBagsAtPoint(index) {
	new x;
	for(new i=0; i<sizeof(DroppedBags); i++) {
		if(DroppedBags[i][index][EObjID] != 0) {
			x++;
		}
	}
	return x;
}
getFreeBagAtPoint(index) {
	for(new i=0; i<sizeof(DroppedBags); i++) {
		if(DroppedBags[i][index][EObjID] == 0) {
			return i;
		}
	}
	return -1;
}
wasteDelBagsTimer(playerid) {
	new timer = TrashTimer[playerid];
	KillTimer(timer);
	TrashTimer[playerid] = 0;
}
wasteFinalizeTrashJob(playerid, success = 1) {
	if(success) {
		ShowScriptMessage(playerid, "You have ~g~finished ~w~your work. You will ~g~receive ~w~your ~g~money ~w~next ~g~payday~w~.", 5000);
		new pay = GetPVarInt(playerid, "Payday");
		pay += 1500;
		SetPVarInt(playerid, "Payday", pay);
	} else {
		ShowScriptMessage(playerid, "You ~r~failed ~w~the ~r~job~w~.");
	}
	DeletePVar(playerid, "TrashRoute");
	DeletePVar(playerid, "TrashRouteIndex");
	DeletePVar(playerid, "TrashVehID");
	TotalTrashDone[playerid] = 0;
	TrashPickupState[playerid] = STATE_NONE;
	SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
	if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER) {
		ApplyAnimation(playerid, "CAR", "Sit_relaxed", 4.1, 1, 0, 0, 0, 0);
	}
	DestroyAllPlayerCPS(playerid);
	return 1;
}
OnPlayerWasteThinkJob() {
	new Float: X, Float: Y, Float: Z;
	new carid;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Job") == EJobType_TrashCleaner) {
				if(GetPVarType(i, "TrashRouteIndex") != PLAYER_VARTYPE_NONE) {
					if(GetPVarType(i, "TrashVehID") != PLAYER_VARTYPE_NONE) {
						carid = GetPVarInt(i, "TrashVehID");
						GetVehiclePos(carid, X, Y, Z);
						if(!IsPlayerInRangeOfPoint(i, 120.0, X, Y, Z)) {
							wasteFinalizeTrashJob(i, 0);
						}
					}
				}
			}
		}
	}
	return 1;
}
makeTrashSpotAvailable(index, notify = 0) {
	TrashDropPoints[index][EDropBeingUsed] = 0;
	if(notify) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				if(GetPVarInt(i, "Job") == EJobType_TrashCleaner) {
					if(TrashPickupState[i] == STATE_DROPPOINT) {
						SendClientMessage(i, X11_WHITE, "[INFO]: A new trash spot has become available, it has been marked on your map.");
						SetSinglePlayerCP(i, TrashDropPoints[index][EDropSpotX], TrashDropPoints[index][EDropSpotY], TrashDropPoints[index][EDropSpotZ], ECPType_Job, GetPVarInt(i, "Job"));
						PlayerPlaySound(i, 1056, 0.0, 0.0, 0.0);
					}
				}
			}
		}
	}
	return 1;
}
giveOrRemoveWasteBag(playerid) {
	new msg[128];
	if(GetPVarType(playerid, "CarryingWasteBag") != PLAYER_VARTYPE_NONE) {
		removeWasteBagFromPlayer(playerid);
		format(msg, sizeof(msg), "* %s drops the waste bag in the vehicle.",GetPlayerNameEx(playerid, ENameType_RPName));
	} else {
		attachWasteBagToPlayer(playerid);
		format(msg, sizeof(msg), "* %s grabs a waste bag from the trash container.",GetPlayerNameEx(playerid, ENameType_RPName));
	}
	//ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	SetPlayerChatBubble(playerid, msg, COLOR_PURPLE, 30.0, 5000);
}
attachWasteBagToPlayer(playerid) {
	if(IsPlayerAttachedObjectSlotUsed(playerid, 9)) {
		toggleAccessorySlot(playerid, 9);
	} else {
		SetPlayerAttachedObject(playerid, 9, GARBAGE_BAG_MDL, BONE_LHAND, 0.376658, -0.124655, 0.000000, 120.249526, 269.328002, 28.887514, 0.723283, 1.000000, 0.883125);
	}
	SetPVarInt(playerid, "CarryingWasteBag", 1);
}
removeWasteBagFromPlayer(playerid) {
	RemovePlayerAttachedObject(playerid, 9);
	if(GetPVarType(playerid, "CarryingWasteBag") != PLAYER_VARTYPE_NONE) {
		if(GetPVarInt(playerid, "CarryingWasteBag") == 1) {
			toggleAccessorySlot(playerid, 9);
			DeletePVar(playerid, "CarryingWasteBag");
		}
	}
	return 1;
}
/* Commands */
YCMD:trash(playerid, params[], help) {
	if(IsOnDuty(playerid) || isInPaintball(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this while on duty!");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this!");
		return 1;
	}
	TrashCanIndex[playerid] = getClosestWasteObj(playerid);
	if(TrashCanIndex[playerid] == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not anywhere near a trash can!");
		return 1;
	}
	showTrashDropExMenu(playerid);
	return 1;
}