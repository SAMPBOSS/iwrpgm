enum EnumCopObjectType {
	ECO_SpikeStrip,
	ECO_Misc,
};


enum ECopObjects {
	ECopObjectName[32],
	ECopObjectID,
	ECopObjectOutsideOnly,
	ECopObjectRequiredRank,
	EnumCopObjectType:ECopObjectType
};

new CopObjects[][ECopObjects] = {
	{"Long Spike Strips",2892,1,6,ECO_SpikeStrip},
	{"Small Spike Strips",2899,1,6,ECO_SpikeStrip},
	{"Road Block",981,1,6,ECO_Misc},
	{"Road Cone",1238,1,6,ECO_Misc},
	{"Road Barrier",1228,1,6,ECO_Misc},
	{"Solid Road Barrier",1422,1,6,ECO_Misc},
	{"CJ Road Barrier",1427,1,6,ECO_Misc},
	//{"Large Road Barrier",1662,1,6,ECO_Misc},
	{"Metal Road Barrier",1424,1,6,ECO_Misc},
	{"Detour sign",1425,1,6,ECO_Misc},
	{"Road Maitenance sign",3091,1,6,ECO_Misc},
	{"Light Road Barrier",1459,1,6,ECO_Misc},
	{"Water Barrier",1554,1,6,ECO_Misc},
	{"Solid Barrier",1237,1,6,ECO_Misc},
	{"Dead Cop (RP Reasons only)",3092,0,6,ECO_Misc},
	{"Police Line - Do Not Cross",19834,0,6,ECO_Misc}
	};
	
enum EPlacedCopObjectsInfo {
	EPCO_PlacerSQLID,
	EPCO_ObjectID,
	EPCO_ObjectIndex,
	EPCO_PlaceTime,
};

#define MAX_COP_OBJECTS 	100
#define POLICE_SHIELD 		18637
#define MAX_TASING_DISTANCE	6
#define RELOAD_TASER_TIME	3

new PlacedCopObjects[MAX_COP_OBJECTS][EPlacedCopObjectsInfo];

#define CUFF_BREAK_TIME 30

new ReloadTaserTimer[MAX_PLAYERS];
new TaserCoolDown[MAX_PLAYERS];
//new arrestpickup;
new spiketimer;


enum ELEODoorInfo {
	Float:ELEODoorX,
	Float:ELEODoorY,
	Float:ELEODoorZ,
	Float:ELEODoorEX,
	Float:ELEODoorEY,
	Float:ELEODoorEZ,
	ELEODoorInt,
	Float:ELEOAngle,
	ELEODoorName[32],
	ELEOOnly,
	ELEOExitInt,
	ELEOExitVW,
	ELEOVW,
	ELEOModelID,
	ELEOPickupID,
	ELEOPickupIDExit
};
new LEODoors[][ELEODoorInfo] = {
	{246.36, 87.38, 1003.64,245.09, 122.99, 1014.21,-1,180.0,"Cafeteria",0,-1, -1,-1,0,0},
	{14.668058, 32.404808, 1499.265991,1571.652099,-1694.586425,5.890625,16,181.0805,"LSPD Garage",1,0,0,2,1239,0},
	{1556.10, -1675.78, 28.39,1524.4916,-1677.7880,6.2188,0,90.0,"LSPD Roof",1,0,0,0,1239,0},
	{245.09, 122.99, 1014.21,246.36, 87.38, 1003.64,-1,180.0,"Office",0,-1,-1,-1,0,0},
	{245.70, 84.98, 1003.64,226.97, 137.78, 1022.65,-1,90.0,"Shooting Range",0,-1,-1,-1,0,0},
	{239.05, 81.93, 1005.03,202.46, 60.35, 1008.42,-1,0.0,"Briefing Room",0,-1,-1,-1,0,0},
	{228.15, 81.41, 1005.03,216.97, 84.18, 1004.54,-1,0.0,"Chief Office",0,-1,-1,-1,0,0},
	{238.362106, 139.275878, 1003.023437, 318.753845, -1509.855590, 24.921875,3,2.697039,"FBI Garage", 1, 0, 0, 3,1239,0},
	{189.033157, 179.348815, 1003.023437, 352.629699, -1483.033935, 76.539062, 3, 258.477661, "FBI Roof",1,0,0,3,1239,0},
	{1481.088989, -1776.986083, 3281.795410, 592.867431, -617.178588, -13.975000,1, 354.595581,"SADPS Garage",1,0,0,1,1239,0}
};
leoOnGameModeInit() {
	CreateDynamicPickup(1254, 16, 1559.508300, -1694.316650, 5.896991); //LSPD Arrest spot
	CreateDynamicPickup(1254, 16, 309.960906, -1516.312744, 24.921875); //FBI arrest spot
	CreateDynamicPickup(1254, 16, 597.397277, -619.103271, -13.975000);
	
	CreateDynamicPickup(1239, 16, -20.904359, -43.595165, 1499.265991); //LSPD MDC point
	CreateDynamic3DTextLabel("(( /mdc ))", 0x2BB00AA, -20.904359, -43.595165, 1499.265991+1, 10.0);
	CreateDynamicPickup(1239, 16, 262.111114, 184.586822, 1008.1718750, .worldid = 3, .interiorid = 3); //FBI MDC point
	CreateDynamic3DTextLabel("(( /mdc ))", 0x2BB00AA, 262.111114, 184.586822, 1008.171875+1, 10.0, .worldid = 3, .interiorid = 3);
	
	/*
	CreateDynamicPickup(1239, 16, 626.239318, -608.537231, 16.743902, .worldid = 0, .interiorid = 0);
	CreateDynamic3DTextLabel("(( /garage ))", 0x2BB00AA, 626.239318, -608.537231, 16.743902+1, 10.0, .worldid = 0, .interiorid = 0);
	
	CreateDynamicPickup(1239, 16, 637.328491, -619.281921, -3.506249, .worldid = 0, .interiorid = 0);
	CreateDynamic3DTextLabel("(( /garage ))", 0x2BB00AA, 637.328491, -619.281921, -3.506249+1, 10.0, .worldid = 0, .interiorid = 0);
	*/
	
	//626.239318, -608.537231, 16.743902, 261.438568
	for(new i=0;i<sizeof(LEODoors);i++) {
		if(LEODoors[i][ELEOModelID] != 0) {
			LEODoors[i][ELEOPickupID] =	CreateDynamicPickup(LEODoors[i][ELEOModelID], 16, LEODoors[i][ELEODoorX],LEODoors[i][ELEODoorY], LEODoors[i][ELEODoorZ], LEODoors[i][ELEOVW], LEODoors[i][ELEODoorInt]);
			LEODoors[i][ELEOPickupIDExit] =	CreateDynamicPickup(LEODoors[i][ELEOModelID], 16, LEODoors[i][ELEODoorEX],LEODoors[i][ELEODoorEY], LEODoors[i][ELEODoorEZ], LEODoors[i][ELEOExitVW], LEODoors[i][ELEOExitInt]);
		}
	}
}

enum iARInfo {
	iARDescrption[32],
	iARBailPrice,
	iARJailTime,
};
new ArrestReasons[][iARInfo] = {
	{"Gun without a license", 100,2},
	{"Speeding", 100,2},
	{"Wreckless Driving", 1500,5},
	{"Theft", 2000,5},
	{"Harrasment", 5000,10},
	{"Armed Theft", 5000,10},
	{"Grand Armed Theft", 10000,10},
	{"Illegal Nos", 500,7},
	{"Attempted Assault", 10000,10},
	{"Assault", 10000,10},
	{"Assault on a LEO", 15000,15},
	{"Premeditated Asssault", 15000,15},
	{"Rape", 15000,15},
	{"Attempted Rape", 10000,10},
	{"Robbery", 15000,15},
	{"Murder", 20000,20},
	{"Attempted Murder", 15000,15},
	{"Armed Roberry", 25000,20},
	{"Murder of a LEO", 35000,25},
	{"Treason", 1000000,45}
};

enum ECellInfo {
	Float:ECellX,
	Float:ECellY,
	Float:ECellZ,
	Float:ECellAngle,
	ECellInterior,
	ECellVW,
}


//new LSPDCells[][ECellInfo] = {{264.550476, 86.739013, 1001.039062,266.487792, 6, 2},{264.175109, 82.154594, 1001.039062,266.487792, 6, 2},{264.423248, 77.869659, 1001.039062,266.487792, 6, 2}};

//new LSPDCells[][ECellInfo] = {{1777.7198,-1554.7654,-4.6438,266.487792, 1, 36},{1769.2365,-1576.8395,-4.6438,266.487792, 1, 36}}; //Old LSPD Cells

new LSPDCells[][ECellInfo] = {
{-47.847213, -276.208343, 1005.405944, 0.0, 35, -1},
{-47.721431, -281.643432, 1005.405944, 0.0, 35, -1},
{-47.612052, -286.370086, 1005.405944, 0.0, 35, -1},
{-47.509273, -290.811309, 1005.405944, 0.0, 35, -1},
{-47.401241, -295.479522, 1005.405944, 0.0, 35, -1},
{-47.293426, -300.138183, 1005.405944, 0.0, 35, -1},
{-67.972999, -299.962768, 1005.405944, 0.0, 35, -1},
{-68.629814, -290.867248, 1005.405944, 0.0, 35, -1},
{-68.927482, -285.783172, 1005.405944, 0.0, 35, -1},
{-69.204925, -281.044586, 1005.405944, 0.0, 35, -1},
{-68.804992, -276.209655, 1005.405944, 0.0, 35, -1},
{-68.966323, -285.737030, 1001.601501, 0.0, 35, -1},
{-69.295089, -290.805908, 1001.601501, 0.0, 35, -1},
{-67.864692, -295.694152, 1001.601501, 0.0, 35, -1},
{-68.160278, -300.250915, 1001.601501, 0.0, 35, -1},
{-46.708965, -300.254150, 1001.601501, 0.0, 35, -1},
{-46.790077, -294.748657, 1001.601501, 0.0, 35, -1},
{-46.852939, -290.481658, 1001.601501, 0.0, 35, -1},
{-46.922714, -285.745330, 1001.601501, 0.0, 35, -1}
};

new FBICells[][ECellInfo] = {{197.651733, 174.396408, 1003.023437,352.655242,3,3},{193.986022, 174.809677, 1003.023437, 6.128678,3,3},
{198.272369, 161.909286, 1003.029968, 177.210327,3,3}};

new DilCells[][ECellInfo] = {
{1491.813842, -1759.269042, 3285.285888, 181.129531, 1, 1},
{1495.086181, -1759.587646, 3285.285888, 181.129531, 1, 1},
{1498.161499, -1759.527709, 3285.285888, 181.129531, 1, 1},
{1501.581542, -1758.761962, 3285.285888, 181.129531, 1, 1},
{1502.403564, -1768.097412, 3285.285888, 0.0, 1, 1},
{1502.403564, -1768.097412, 3285.285888, 0.0, 1, 1},
{1498.608276, -1767.307006, 3285.285888, 0.0, 1, 1},
{1495.308593, -1767.779296, 3285.285888, 0.0, 1, 1},
{1492.611206, -1768.010620, 3285.285888, 0.0, 1, 1}
};

enum EEquipInfo {
	EEquipName[32],
	EEquipFaction, //faction ID this is for
	EEquipMinRank, //minimum rank for this equip item
	EEquipGun1,
	EEquipGun2,
	EEquipGun3,
	EEquipGun4,
	EEquipGun5,
	EEquipGun6,
	EEquipGun7,
	EEquipGun8,
	EEquipGun9,
	EEquipGun10,
	EEquipGun11,
	EEquipGun12,
};

new EquipSlots[][EEquipInfo] = { //So difficult to read....
	//lspd
	{"Patrol Officer",1,1,24,25,29,0,41,3,0,0,0,0,0,0},
	{"Detective",1,5,43,24,0,0,0,0,0,0,0,0,0}, 
	{"Sergeant",1,6,24,25,29,41,3,0,0,0,0,0,0}, 
	{"Lieutenant",1,10,24,25,29,33,41,3,0,0,0,0,0},
	{"Captain/Chief",1,12,24,25,29,33,41,3,31,0,0,0,0},
	//SWAT - Makes part of LSPD - Rank starting from 3
	{"S.W.A.T. Special",1,3,24,0,0,0,0,3,31,0,0,0,0,0},
	{"S.W.A.T. Tactical",1,3,24,0,29,0,0,3,0,0,0,0,0,0},
	{"S.W.A.T. Sniper",1,3,23,0,0,34,0,3,0,0,0,0,0},
	//fbi
	//{"Staff",2,1,41,3,24,25,29,0,0,0,0,0,0,0},
	//{"Equipment",2,0,46,43,42,41,3,0,0,0,0,0,0,0},
	{"Probationary",2,1,41,3,24,25,0,0,0,0,0,0,0,0},
	{"Intern",2,2,41,3,24,25,29,0,0,0,0,0,0,0},
	{"Staff",2,3,41,3,24,25,29,31,0,0,0,0,0,0},
	{"Agent",2,4,41,3,24,29,30,31,34,0,0,0,0,0},
	{"Senior Agent",2,5,41,3,24,28,32,29,30,31,34,0,0,0},
	{"Special Agent",2,5,41,3,24,28,32,29,30,31,34,0,0,0},
	{"Deputy Director",2,5,41,3,24,28,32,29,30,31,34,0,0,0},
	{"Director",2,5,41,24,32,28,29,30,31,34,0,0,0,0},
	//mayor
	{"Overwatch",3,10,34,0,43,3,0,0,0,0,0,0,0,0},
	{"Guard Duty",3,10,24,0,41,3,0,0,0,0,0,0,0,0},
	{"Agent Duty",3,10,23,0,43,3,0,0,0,0,0,0,0,0},
	{"Mayor",3,0,0,0,0,0,0,0,0,0,0,0,0,0},
	//EMS/FF
	{"Trainee",5,1,0,0,0,0,0,0,0,0,0,15,0,0},
	{"Probationary",5,2,0,0,0,0,0,0,0,0,0,15,0,0},
	{"FF/EMT-B",5,3,6,0,0,0,0,0,0,0,42,15,0,0},
	{"FF/EMT-I",5,4,6,0,0,0,0,0,0,0,42,15,0,0},
	{"FF/EMT-A",5,5,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Lieutenant",5,6,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Captain",5,7,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Battalion Commander",5,8,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Battalion Chief",5,9,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Deputy Fire Chief",5,10,6,0,0,0,0,0,0,0,42,15,0,0},
	{"Fire Chief",5,11,6,0,0,0,0,0,0,0,42,15,0,0},
	//CA Dept of Corrections
	{"Police Officer",8,1,22,25,3,0,0,0,0,0,0,0,0,0}, 
	{"Field Training Officer",8,2,24,25,3,41,0,0,0,0,0,0,0}, 
	{"Sergeant",8,3,24,25,29,41,3,0,0,0,0,0,0}, 
	{"Lieutenant",8,4,24,25,29,33,41,3,0,0,0,0,0},
	{"Captain",8,5,24,25,29,33,41,3,31,0,0,0,0},
	{"Inspector",8,6,24,25,29,33,41,3,31,0,0,0,0},
	{"Deputy Chief",7,11,24,25,29,33,41,3,31,0,0,0,0},
	{"Chief",8,8,24,25,29,33,41,3,31,0,0,0,0}
};

numSpikeStrips() {
	new index;
	new num;
	for(new i=0;i<sizeof(PlacedCopObjects);i++) {
		if(PlacedCopObjects[i][EPCO_ObjectID] != 0) {
			index = PlacedCopObjects[i][EPCO_ObjectIndex];
			if(CopObjects[index][ECopObjectType] == ECO_SpikeStrip) {
				num++;
			}
		}
	}
	return num;
}
/*
YCMD:garage(playerid, params[], help) {
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
	}
	//637.328491, -619.281921, -3.506249, 100.430496
	if(IsPlayerInRangeOfPoint(playerid, 5.0, 626.239318, -608.537231, 16.743902)) {
		if(IsPlayerInAnyVehicle(playerid)) {
			new carid = GetPlayerVehicleID(playerid);
			SetVehiclePos(carid, 637.328491, -619.281921, -3.506249);			
		} else {
			SetPlayerPos(playerid, 637.328491, -619.281921, -3.506249);
		}
	} else if(IsPlayerInRangeOfPoint(playerid, 5.0, 637.328491, -619.281921, -3.506249)) {
		if(IsPlayerInAnyVehicle(playerid)) {
			new carid = GetPlayerVehicleID(playerid);
			SetVehiclePos(carid, 626.239318, -608.537231, 16.743902);			
		} else {
			SetPlayerPos(playerid, 626.239318, -608.537231, 16.743902);
		}
	}
	return 1;
}
*/
YCMD:impound(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for impounding a vehicle");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
		if(IsAtArrestSpot(playerid) == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at an impound spot(Arrest Spot)");
			return 1;
		}
	}
	new car = GetClosestVehicle(playerid);
	if(car == -1 || DistanceBetweenPlayerAndVeh(playerid, car) > 5.0 || GetVehicleVirtualWorld(car) != GetPlayerVirtualWorld(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near any vehicle!");
		return 1;
	}
	if(VehicleInfo[car][EVType] != EVehicleType_Owned) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not an owned vehicle!");
		return 1;
	}
	new price;
	if(!sscanf(params, "d", price)) {
		if(price < 0 || price > 100000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Price!");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "HQ: %s has impounded %s's %s!",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(VehicleInfo[car][EVOwner], ENameType_RPName_NoMask),VehiclesName[GetVehicleModel(car)-400]);
		SendCopMessage(TEAM_BLUE_COLOR, msg);
		putVehicleInImpound(car, 1, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /impound [price]");
	}
	return 1;
}

YCMD:placeobject(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to place a cop object");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(CopObjects);i++) {
		format(tempstr, sizeof(tempstr), "%s - Rank %d\n",CopObjects[i][ECopObjectName],CopObjects[i][ECopObjectRequiredRank]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFactionsDialog_CopPlace, DIALOG_STYLE_LIST, "{00BFFF}Object Menu", dialogstr, "Place", "Cancel");
	return 1;
}
YCMD:removeobjects(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes all cop objects");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	for(new i=0;i<sizeof(PlacedCopObjects);i++ ) {
		if(PlacedCopObjects[i][EPCO_ObjectID] != 0) {
			removeCopObject(PlacedCopObjects[i][EPCO_ObjectID]);
		}
	}
	SendClientMessage(playerid, X11_WHITE, "All objects have been removed!");
	return 1;
}
YCMD:removespikestrip(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to place a cop object");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	for(new i = 0; i < sizeof(PlacedCopObjects); i++) {
		if(PlacedCopObjects[i][EPCO_ObjectID] != 0) {
			new index = PlacedCopObjects[i][EPCO_ObjectIndex];
			GetObjectPos(PlacedCopObjects[i][EPCO_ObjectID], X, Y, Z);
			if(IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
				if(CopObjects[index][ECopObjectType] == ECO_SpikeStrip) {
					removeCopObject(PlacedCopObjects[i][EPCO_ObjectID]);
				}
			}
		}
  	}
	return 1;
}
handleCopPlace(playerid, item) {
	new rank = GetPVarInt(playerid, "Rank");
	//new obj = CopObjects[item][ECopObjectID];
	if(CopObjects[item][ECopObjectOutsideOnly]) {
		if(GetPlayerVirtualWorld(playerid) != 0 || GetPlayerInterior(playerid) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can only place this outside!");
			return 1;
		}
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(rank < CopObjects[item][ECopObjectRequiredRank]) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can only place this outside!");
			return 1;
		}
	}

	new index = findFreeCopObject();
	if(index == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "The maximum cop objects have been placed");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	PlacedCopObjects[index][EPCO_ObjectID] = CreateDynamicObject(CopObjects[item][ECopObjectID],X,Y,Z,0.0,0.0,0.0);
	PlacedCopObjects[index][EPCO_ObjectIndex] = item;
	PlacedCopObjects[index][EPCO_PlaceTime] = gettime();
	PlacedCopObjects[index][EPCO_PlacerSQLID] = GetPVarInt(playerid, "CharID");
	EditObject(playerid, PlacedCopObjects[index][EPCO_ObjectID]);
	if(numSpikeStrips() >= 0 && spiketimer == 0) {
		spiketimer = SetTimer("CheckStrips", 50, true);
	}
	return 1;
}
findFreeCopObject() {
	for(new i=0;i<sizeof(PlacedCopObjects);i++) {
		if(PlacedCopObjects[i][EPCO_ObjectID] == 0) {
			return i;
		}
	}
	return -1;
}
YCMD:copfix(playerid, params[], help) {
	new msg[128];
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && !isMedic(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned!");
		return 1;
	}
	new time = GetPVarInt(playerid, "RequestChatCooldown");
	new timenow = gettime();
	if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait before sending another report!");
		return 1;
	}
	SetPVarInt(playerid, "RequestChatCooldown", gettime());
	format(msg, sizeof(msg), "Report from %s: /AllowCopFix [%d] (/CopFix) ",GetPlayerNameEx(playerid, ENameType_CharName), playerid);
	ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
	SendClientMessage(playerid, X11_YELLOW, "The admins have been notified of your request");
	return 1;
}
YCMD:allowcopfix(playerid, params[], help) {
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!IsAnLEO(target) && !isGovernment(target) && !isMedic(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player isn't a cop!");
			return 1;
		}
		SetPVarInt(target, "CopFix", gettime()+60);
		SendClientMessage(target, X11_ORANGE, "An admin has copfixed you. You can now /duty and /equip for a short period of time!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /allowcopfix [playerid/name]");
	}
	return 1;
}
YCMD:duty(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Goes on/off cop duty");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && getFactionType(faction) != EFactionType_EMS) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop or a medic!");
		return 1;
	}
	if(getFactionType(faction) == EFactionType_EMS) {
		onTogMedicDuty(playerid);
		return 1;
	}
	new rank = GetPVarInt(playerid, "Rank");
	new string[128];
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(!IsAtDutySpot(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the equip spot!");
			return 1;
		}
	}
	if(GetPVarType(playerid, "CopDuty") == PLAYER_VARTYPE_NONE) {
		format(string, sizeof(string), "* %s %s goes on duty.",getFactionRankName(faction, rank),GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(10.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPVarInt(playerid, "CopDuty", 1);
		saveGuns(playerid, "CopGuns");
		showEquipMenu(playerid);
	} else {
		if(GetPVarInt(playerid, "HasTaser") == 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't go off duty with a taser! [/taser]");
			return 1;
		}
		restoreGuns(playerid, "CopGuns");
		format(string, sizeof(string), "* %s %s goes off duty.",getFactionRankName(faction, rank),GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(10.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPlayerColor(playerid, getNameTagColour(playerid));
		SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
		DeletePVar(playerid, "CopDuty");
	}
	SetPlayerColor(playerid, getNameTagColour(playerid));
	return 1;
}
YCMD:equip(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shows the equip menu");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && getFactionType(faction) != EFactionType_EMS) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop or a medic!");
		return 1;
	}
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(!IsAtDutySpot(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the equip spot!");
			return 1;
		}
	}
	if(!IsOnDuty(playerid) && !isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	showEquipMenu(playerid);
	return 1;
}
YCMD:policeshield(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives you a shield");
		return 1;
	}
	if(!IsAnLEO(playerid) && !isGovernment(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(!IsAtDutySpot(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the equip spot!");
			return 1;
		}
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	giveOrRemoveShield(playerid);
	return 1;
}
YCMD:arrest(playerid, params[], help) {
	new target;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for arresting a player");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(IsAtArrestSpot(playerid) == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at an arrest spot!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	new time, bail, reason[128];
	new Float:X, Float:Y, Float:Z;
	if(!sscanf(params, "k<playerLookup>dds[128]", target, time, bail, reason)) {
		if(!IsPlayerConnectEx(target)) {
		    SendClientMessage(playerid, COLOR_LIGHTRED, "   Player not found!");
		    return 1;
		}
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 15.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(time > 240) {
			SendClientMessage(playerid, X11_TOMATO_2, "Jail time too long!");
			return 1;
		}
		if(bail < 0 || bail > 100000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Bail Price!");
			return 1;
		}
		if(strlen(reason) < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must have a reason!");
			return 1;
		}
		ICJail(playerid,target,time,bail,IsAtArrestSpot(playerid));
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /arrest [playerid/name] [time] [bail(0 = no bail)] [reason]");
	}
	return 1;
}
handleCustomArrest(playerid, inputtext[], response) {
	if(!response) {
		    
	} else {
		new arresttime = strval(inputtext);
		if(arresttime < 0 || arresttime > 60) {
			ShowPlayerDialog(playerid,EFactionsDialog_CustomArrest,DIALOG_STYLE_INPUT,"Jail Time","Enter the jail time(minutes)","Ok","Cancel");
			return 1;
		}
		new atime = GetPVarInt(playerid, "ArrestTime");
		atime += arresttime;
		SetPVarInt(playerid, "ArrestTime", atime);
		ShowPlayerDialog(playerid,EFactionsDialog_BailPrice,DIALOG_STYLE_INPUT,"Bail Price","Enter the Bail Price","Ok","Cancel");
	}
	return 1;
}
handleArrestBail(playerid, inputtext[],response) {
	new string[128];
	new bailtime = strval(inputtext);
	new target = GetPVarInt(playerid, "ArrestTarget");
	if(!response) {
		format(string, sizeof(string), "/arrest %d",target);
		Command_ReProcess(playerid, string, false);
		return 1;
	}
	if(bailtime < 0 || bailtime > 500000) {
			ShowPlayerDialog(playerid,EFactionsDialog_CustomArrest,DIALOG_STYLE_INPUT,"Jail Time","Enter the jail time(minutes)","Ok","Cancel");
			return 1;
	}
	new bail = GetPVarInt(playerid, "ArrestBail");
	bail += bailtime;
	SetPVarInt(playerid, "ArrestBail", bail);
	new time = GetPVarInt(playerid, "ArrestTime");
	format(string,sizeof(string),"New arrest time for %s: Bail: %d Time: %d",GetPlayerNameEx(target,ENameType_RPName_NoMask),bail,time);
	SendClientMessage(playerid, X11_WHITE, string);
	format(string, sizeof(string), "/arrest %d",GetPVarInt(playerid, "ArrestTarget"));
	Command_ReProcess(playerid, string, false);
	return 0;
}
handleArrestChoose(playerid, index,response) {
	#pragma unused response
	new string[128];
	if(index-sizeof(ArrestReasons) == 0) {
		ShowPlayerDialog(playerid,EFactionsDialog_CustomArrest,DIALOG_STYLE_INPUT,"Jail Time","Enter the jail time(minutes)","Ok","Cancel");
	} else if(index-sizeof(ArrestReasons) == 1) { //jail them, they are finished with the dialog
		ICJail(playerid,GetPVarInt(playerid, "ArrestTarget"),GetPVarInt(playerid, "ArrestTime"),GetPVarInt(playerid, "ArrestBail"),IsAtArrestSpot(playerid));
		DeletePVar(playerid, "ArrestBail");
		DeletePVar(playerid, "ArrestTime");
		DeletePVar(playerid, "ArrestTarget");
	} else {
		new bail = GetPVarInt(playerid, "ArrestBail");
		bail += ArrestReasons[index][iARBailPrice];
		SetPVarInt(playerid, "ArrestBail", bail);
		new time = GetPVarInt(playerid, "ArrestTime");
		time += ArrestReasons[index][iARJailTime];
		SetPVarInt(playerid, "ArrestTime", time);
	    format(string,sizeof(string),"New arrest time for %s: Bail: %d Time: %d",GetPlayerNameEx(playerid,ENameType_RPName_NoMask),bail,time);
		SendClientMessage(playerid, X11_WHITE, string);
		format(string, sizeof(string), "/arrest %d",GetPVarInt(playerid, "ArrestTarget"));
		Command_ReProcess(playerid, string, false);
	}
}
handlePoliceComputer(playerid, index, response) {
	if(!response) {
		SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the police computer menu.");
		return 1;
	}
	CallLocalFunction(ComputerDialogs[index][E_DoLEOCallBack],"d", playerid);
	return 1;
}
handlePoliceBackup(playerid, inputtext[], response) {
	if(!response) {
		showPoliceComputerDialog(playerid);
		return 1;
	}
	new playa;
	new explodearray[128][128];
	explode(inputtext, explodearray, ':');
	playa = strval(explodearray[1]);
	onHandlePoliceBackup(playerid, playa);
	return 1;
}
onHandlePoliceBackup(playerid, playa) {
	new msg[128];
	new Interior;
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playa, X, Y, Z);
	Interior = GetPlayerInterior(playa);
	if(Interior == 0) {
		SetPlayerCheckpoint(playerid, X, Y, Z,5.0);
	} else {
		new index = getStandingBusiness(playa);
		if(index != -1) { //Is in a business
			SetPlayerCheckpoint(playerid, Business[index][EBusinessEntranceX], Business[index][EBusinessEntranceY], Business[index][EBusinessEntranceZ],5.0);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "[Police Computer]: Couldn't track location.");
			return 1;
		}
	}
	SetPVarInt(playerid, "AnsweringPlayerBackup", playa);
	format(msg, sizeof(msg), "%s has answered the backup request of %s", GetPlayerNameEx(playerid,ENameType_RPName), GetPlayerNameEx(playa,ENameType_RPName));
	SendCopMessage(TEAM_BLUE_COLOR, msg);
	return 1;
}
handlePoliceBizLocation(playerid, index, response) {
	if(!response) {
		showPoliceComputerDialog(playerid);
		return 1;
	}
	SetPVarInt(playerid, "LeoHeadingToBiz", 1);
	SetPlayerCheckpoint(playerid, Business[index][EBusinessEntranceX], Business[index][EBusinessEntranceY], Business[index][EBusinessEntranceZ],5.0);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "[Police Computer]: The destination to the business you've requested, has been marked on your map");
	return 1;
}
forward cancelLEOBackup(playerid);
public cancelLEOBackup(playerid) {
	new msg[128];
	if(GetPVarType(playerid, "requestedLEOBackup") != PLAYER_VARTYPE_NONE) {
		format(msg, sizeof(msg), "%s canceled %s backup request!", GetPlayerNameEx(playerid,ENameType_RPName), getPossiveAdjective(playerid));
		SendCopMessage(TEAM_BLUE_COLOR, msg);
		DeletePVar(playerid, "requestedLEOBackup");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "[Police Computer]: You have cancelled your backup request!");
		clearRelatedBackupCheckPoints(playerid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "[Police Computer]: You haven't requested any backup!");
	}
	return 1;
}
forward clearRelatedBackupCheckPoints(playerid);
public clearRelatedBackupCheckPoints(playerid) {
	new answeringBackup;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsAnLEO(i)) {
				if(GetPVarType(i, "AnsweringPlayerBackup") != PLAYER_VARTYPE_NONE) {
					answeringBackup = GetPVarInt(i, "AnsweringPlayerBackup");
					if(answeringBackup == playerid) {
						DisablePlayerCheckpoint(i);
						DeletePVar(i, "AnsweringPlayerBackup");
					}
				}
			}
		}
	}
	return 1;
}
leoOnPlayerEnterCheckpoint(playerid) {
	new msg[128];
	new answeringBackup;
	if(GetPVarType(playerid, "AnsweringPlayerBackup") != PLAYER_VARTYPE_NONE) {
		answeringBackup = GetPVarInt(playerid, "AnsweringPlayerBackup"); 
		format(msg, sizeof(msg), "%s has arrived to the backup location of %s", GetPlayerNameEx(playerid,ENameType_RPName), GetPlayerNameEx(answeringBackup,ENameType_RPName));
		SendCopMessage(TEAM_BLUE_COLOR, msg);
		DeletePVar(playerid, "AnsweringPlayerBackup");
		DisablePlayerCheckpoint(playerid);
	}
	if(GetPVarType(playerid, "LeoHeadingToBiz") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "LeoHeadingToBiz");
		DisablePlayerCheckpoint(playerid);
	}
}
forward requestLEOBackup(playerid);
public requestLEOBackup(playerid) {
	new msg[128];
	new Interior;
	Interior = GetPlayerInterior(playerid);
	if(GetPVarType(playerid, "requestedLEOBackup") == PLAYER_VARTYPE_NONE) {
		SetPVarInt(playerid, "requestedLEOBackup", 1);
		if(Interior == 0) {
			format(msg, sizeof(msg), "%s has requested police backup at %s location! (( /policecomputer to answer the backup request ))", GetPlayerNameEx(playerid,ENameType_RPName), getPossiveAdjective(playerid));
		} else {
			new index = getStandingBusiness(playerid);
			if(index != -1) { //If he / she is standing at a business
				format(msg, sizeof(msg), "%s has requested police backup at %s! (( /policecomputer to answer the backup request ))", GetPlayerNameEx(playerid,ENameType_RPName), Business[index][EBusinessName]);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "[Police Computer]: Couldn't send backup request! Try again at a different location!");
			}
		}
		SendCopMessage(TEAM_BLUE_COLOR, msg);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "[Police Computer]: You have requested for backup already, clear your backup request to start a new one."); 
	}
	return 1;
}
forward answerLEOBackup(playerid);
public answerLEOBackup(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new faction;
	new rank;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsAnLEO(i)) {
				if(GetPVarType(i, "requestedLEOBackup") != PLAYER_VARTYPE_NONE) {
					rank = GetPVarInt(i, "Rank");
					faction = GetPVarInt(i, "Faction");
					format(tempstr,sizeof(tempstr),"%s %s ID: %d\n",getFactionRankName(faction, rank), GetPlayerNameEx(i,ENameType_RPName), i);
					strcat(dialogstr,tempstr,sizeof(dialogstr));
				}
			}
		}
	}
	ShowPlayerDialog(playerid, EFactionsDialog_LEOReqBackups, DIALOG_STYLE_LIST, "Officers requiring backup:",dialogstr,"Ok", "Cancel");
}
forward displayLEOBizList(playerid);
public displayLEOBizList(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(Business);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",Business[i][EBusinessName]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
		/*
		if(Business[i][EBusinessID] == id) {
			reloadBusiness(i);
			return 1;
		}
		*/
	}
	ShowPlayerDialog(playerid, EFactionsDialog_ShowBizList, DIALOG_STYLE_LIST, "Businesses:",dialogstr,"Ok", "Cancel");
}
YCMD:cuff(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Handcuffs a person");
		return 1;
	}
	new user;
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", user)) {
		new Float:X,Float:Y,Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		/*
		if(playerid == user) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this!");
			return 1;
		}
		*/
		new string[128];
		format(string, sizeof(string), "* You were cuffed by %s.", GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(user, COLOR_LIGHTBLUE, string);
		format(string, sizeof(string), "* You cuffed %s for %d minutes.",  GetPlayerNameEx(user, ENameType_RPName), (CUFF_BREAK_TIME));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
		format(string, sizeof(string), "* %s takes out his handcuffs from his utility belt and hand cuffs %s.",  GetPlayerNameEx(playerid, ENameType_RPName) , GetPlayerNameEx(user, ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPlayerSpecialAction(user, SPECIAL_ACTION_CUFFED);
		//TogglePlayerControllableEx(user, 0);
		if(GetPVarType(user, "TazerUnfreezeTimer") != PLAYER_VARTYPE_NONE) {
			KillTimer(GetPVarInt(user, "TazerUnfreezeTimer"));
			DeletePVar(user, "TazerUnfreezeTimer");
		}
		SetPVarInt(user, "CuffTime", gettime());
		SetPlayerAttachedObject(user, 8, 19418, 6, -0.011000, 0.028000, -0.022000, -15.600012, -33.699977, -81.700035, 0.891999, 1.000000, 1.168000);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /cuff [playerid/name]");
	}
	return 1;
}
YCMD:taser(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Pulls out a police officers taser");
		return 1;
	}
	if(!IsAnLEO(playerid) && !isGovernment(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(GetPVarInt(playerid, "HasTaser") == 1) {
		if(IsPlayerAttachedObjectSlotUsed(playerid, 8)) {
			RemovePlayerAttachedObject(playerid, 8);
		}
		DeletePVar(playerid, "HasTaser");
		restoreGuns(playerid, "TaserGuns");
	} else {
		saveGuns(playerid, "TaserGuns");
		SetPVarInt(playerid, "HasTaser", 1);
		SetPlayerAttachedObject(playerid, 8, 18642, 6, -0.011000, 0.028000, -0.022000, -15.600012, -33.699977, -81.700035, 0.891999, 1.000000, 1.168000);
		GivePlayerWeaponEx(playerid, 23, 17);
		format(query, sizeof(query), "* %s pulls out a tazer and charges it up ready to fire.",  GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(30.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Shoot someone with the taser to remove it, or do this command again");
	}
	return 1;
}
YCMD:uncuff(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Handcuffs a person");
		return 1;
	}
	new user;
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>", user)) {
		new Float:X,Float:Y,Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		if(playerid == user) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this!");
			return 1;
		}
		if(GetPVarType(user, "CuffTime") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person isn't cuffed!");
			return 1;
		}
		new string[128];
		format(string, sizeof(string), "* You were uncuffed by %s.", GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(user, COLOR_LIGHTBLUE, string);
		format(string, sizeof(string), "* You uncuffed %s",  GetPlayerNameEx(user, ENameType_RPName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
		format(string, sizeof(string), "* %s takes out a key and uncuffs %s.",  GetPlayerNameEx(playerid, ENameType_RPName) , GetPlayerNameEx(user, ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPlayerSpecialAction(user, SPECIAL_ACTION_NONE);
		TogglePlayerControllableEx(user, 1);
		DeletePVar(user, "CuffTime");
		if(IsPlayerAttachedObjectSlotUsed(user, 8)) {
			RemovePlayerAttachedObject(user, 8);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /uncuff [playerid/name]");
	}
	return 1;
}
YCMD:detain(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Detains a person in your car");
		return 1;
	}
	new user;
	new carid;
	if(!IsPlayerInAnyVehicle(playerid)) {
		carid = GetClosestVehicle(playerid);	
	} else {
		carid = GetPlayerVehicleID(playerid);
	}
	if(!isFactionCar(carid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This must be a faction car!");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	GetVehiclePos(carid, X, Y, Z);
	if(!IsPlayerInRangeOfPoint(playerid, 25.0, X, Y, Z)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
		return 1;
	}
	new seatid;
	if(!sscanf(params, "k<playerLookup>D(2)", user, seatid)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarType(user, "CuffTime") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This perosn must be cuffed");
			return 1;
		}
		PutPlayerInVehicleEx(user, carid, seatid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /detain [playerid/name] (seatid)");
	}
	return 1;
}
YCMD:suspect(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Suspects a person of a crime");
		return 1;
	}
	new target, crime[32];
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!IsAtMDC(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a computer!");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>s[32]",target, crime)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new level = GetPVarInt(target, "WantedLevel");
		level++;
		SetPlayerWantedLevel(target, level);
		SetPVarInt(target, "WantedLevel", level);
		SendCrimeMessage(target, playerid, crime);
		InsertCrime(target, playerid, crime);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /suspect [playerid/name] [crime]");
	}
	return 1;
}
YCMD:department(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Department Radio");
		return 1;
	}
	if(isInJail(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this in jail!");
		return 1;
	}
	if(isPlayerFrozen(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this while frozen!");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && !isMedic(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	new msg[128];
	new string[128];
	if(!sscanf(params, "s[128]",msg)) {
		jailIfOOC(playerid, msg);
		format(string, sizeof(string), "%s says (department): %s",GetPlayerNameEx(playerid, ENameType_RPName), msg);
		ProxMessage(20.0, playerid, string,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);		
		if(IsRadioJammerActiveInArea(playerid)) {
			format(string, sizeof(string), "* You hear static coming from the radio");
			ProxMessage(20.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			return 1;
		}
		format(string, sizeof(string), "** %s %s: %s",getFactionRankName(faction, rank), GetPlayerNameEx(playerid, ENameType_RPName_NoMask), msg);
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				if(IsAnLEO(i) || isGovernment(i) || isMedic(i)) {
					if(!IsRadioJammerActiveInArea(i)) {
						SendClientMessage(i, COLOR_ALLDEPT, string);
					}
				}
			}
		}
		SendDepartmentMsgToListeners(playerid, msg);
		SendBigFactionEarsMessage(COLOR_ALLDEPT, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /department [message]");
	}
	return 1;
}
YCMD:mdc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Looks a person up on the criminal computer");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!IsAtMDC(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a computer!");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "* The MDC is unable to connect to the police network");
		return 1;
	}
	new name[MAX_PLAYER_NAME];
	if(!sscanf(params,"s[" #MAX_PLAYER_NAME "]", name)) {
		IsNameTaken(name, "OnMDCLookup", playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /mdc [Name]");
		SendClientMessage(playerid, X11_TOMATO_2, "Note: Because this is an offline lookup too, you must type the EXACT name in");
	}
	return 1;
}
YCMD:policecomputer(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Starts up the police computer");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!IsAtMDC(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a computer!");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "* The computer is unable to connect to the police network");
		return 1;
	}
	showPoliceComputerDialog(playerid);
	return 1;
}
forward showPoliceComputerDialog(playerid);
public showPoliceComputerDialog(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(ComputerDialogs);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",ComputerDialogs[i][E_DialogOptionText]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EFactionsDialog_LEOCompOptions, DIALOG_STYLE_LIST, "Police Computer Menu - v1.0 Alpha",dialogstr,"Ok", "Cancel");
}
YCMD:wanted(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists wanted players");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!IsAtMDC(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a computer!");
		return 1;
	}
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "* Wanted Suspects *");
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(!isTinFoiled(i)) {
				new wanted = GetPlayerWantedLevel(i);
				new tickets = numTickets(i);
				if(wanted > 0 || tickets > 0) {
					format(msg, sizeof(msg), "* %s: %d tickets, %d stars",GetPlayerNameEx(i, ENameType_RPName_NoMask), tickets, wanted);
					SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				}
			}
		}
		
	}
	return 1;
}
CMD:undercover(playerid, params[])
{
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(GetPVarInt(playerid, "CopFix") < gettime()) {
		if(!IsAtDutySpot(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the equip spot!");
			return 1;
		}
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(GetPVarInt(playerid, "Rank") < 5) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your rank is too low(you must be rank 5)");
		return 1;
	}
	new skin;
    if(!sscanf(params, "i", skin))
    {
        switch(skin)
		{
		    case 0:
			{
			    new string[100];
				format(string,sizeof(string), "* %s puts %s undercover uniform back in the locker.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
				ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				SetPlayerSkin(playerid, 280);
			}
		    case 1:
		    {
		        SetPlayerSkin(playerid, 164);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as an Agent.");
				return 1;
			}
			case 2:
			{
				SetPlayerSkin(playerid, 2);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Hippy.");
				return 1;
			}
			case 3:
			{
				SetPlayerSkin(playerid, 170);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Gay man.");
				return 1;
			}
			case 4:
			{
				SetPlayerSkin(playerid, 21);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Hustler.");
				return 1;
			}
			case 5:
			{
				SetPlayerSkin(playerid, 60);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Random guy.");
				return 1;
			}
			case 6:
			{
				SetPlayerSkin(playerid, 72);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Swampy Hippy.");
				return 1;
			}
			case 7:
			{
				SetPlayerSkin(playerid, 152);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Hotel Waiter.");
				return 1;
			}
			case 8:
			{
				SetPlayerSkin(playerid, 233);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as a Random girl.");
				return 1;
			}
			case 9:
			{
				SetPlayerSkin(playerid, 192);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as Michelle Cannes.");
				return 1;
			}
			case 10:
			{
				SetPlayerSkin(playerid, 193);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* You are now undercover as Katie Zahn.");
				return 1;
			}
		}
		return 1;
	}
	else
	{
	    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Undercover [Suit ID]");
        SendClientMessage(playerid, COLOR_YELLOW, "_________Skin_List_________");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "1: Agent 2: Staff");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "3: Gay man 4: Hustler");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "5: Random Male 6: Swampy Hippy");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "7: Hotel Waiter (Girl) 8: Random Female");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "9: Michelle Cannes 10: Katie Zhan");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "0: Go back to cop uniform");
	}
	return 1;
}

CMD:uniform(playerid, params[])
{
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && !isMedic(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(GetPVarInt(playerid, "CopFix") < gettime() && !isMedic(playerid)) {
		if(!IsAtDutySpot(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the equip spot!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	} else if(isMedic(playerid)) {
		if(!isInsideHospital(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't inside the hospital!");
			return 1;
		}
	}
	new skin;
	new string[128];
	if(GetPVarInt(playerid, "Faction") == 1 || GetPVarInt(playerid, "Faction") == 7 || GetPVarInt(playerid, "Faction") == 8)
	{
    	if(!sscanf(params, "i", skin))
    	{
	        switch(skin)
	        {
	            case 0:
				{
					format(string,sizeof(string), "* %s puts %s police uniform back in the locker.", GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
				}
	        	case 1:
				{
					SetPlayerSkin(playerid, 71);
					format(string, sizeof(string), "* %s gets on a cadet uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 2:
				{
					SetPlayerSkin(playerid, 280);
					format(string, sizeof(string), "* %s gets on a LSPD uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 3:
				{
					SetPlayerSkin(playerid, 281);
					format(string, sizeof(string), "* %s gets on a SFPD uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 4:
				{
					SetPlayerSkin(playerid, 282);
					format(string, sizeof(string), "* %s gets on a LVPD uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 5:
				{
					SetPlayerSkin(playerid, 283);
					format(string, sizeof(string), "* %s gets on a sherrif uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 6:
				{
				    if(GetPVarInt(playerid, "Rank") >= 15)
				    {
						SetPlayerSkin(playerid, 288);
						format(string, sizeof(string), "* %s gets on the chief uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
						ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					}
					else
					{
					    SendClientMessage(playerid, COLOR_LIGHTRED,"	You are not authorized to wear this uniform!");
					}
					return 1;
				}
				case 7:
				{
					SetPlayerSkin(playerid, 284);
					format(string, sizeof(string), "* %s gets on the motorcycle uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 8:
				{
					SetPlayerSkin(playerid, 192);
					format(string, sizeof(string), "* %s gets on the female uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 9:
				{
					SetPlayerSkin(playerid, 265);
					format(string, sizeof(string), "* %s gets on the tenpenny uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 10:
				{
					SetPlayerSkin(playerid, 266);
					format(string, sizeof(string), "* %s gets on the pulaski uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 11:
				{
					SetPlayerSkin(playerid, 267);
					format(string, sizeof(string), "* %s gets on the hernandez uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 12:
				{
					SetPlayerSkin(playerid, 285);
					format(string, sizeof(string), "* %s gets on the S.W.A.T. uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 13:
				{
					SetPlayerSkin(playerid, 306);
					format(string, sizeof(string), "* %s gets on the female uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 14:
				{
					SetPlayerSkin(playerid, 307);
					format(string, sizeof(string), "* %s gets on the female uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 15:
				{
					SetPlayerSkin(playerid, 308);
					format(string, sizeof(string), "* %s gets on the female uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 16:
				{
					SetPlayerSkin(playerid, 309);
					format(string, sizeof(string), "* %s gets on the female uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				default:
				{
				    SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid uniform!");
				    return 1;
				}
			}
		}
		else
		{
		    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Uniform [Uniform ID]");
			SendClientMessage(playerid, COLOR_YELLOW, "_________Uniform List_________");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "0: No Uniform  1: Cadet Uniform");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "2: LSPD Uniform  3: SFPD Uniform");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "4: LVPD Uniform  5: Black Shirt Sheriff");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "6: Chief/Captain  7: Motorcycle Patrol");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "8: Female-Police  9: Tenpenny");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "10: Pulaski  11: Hernandez");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "12: S.W.A.T. Division 13: Female-Police 1");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "14: Female-Police 2 15: Female-Police 3");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "16: Female-Police 4");
			return 1;
		}
	}
	else if(GetPVarInt(playerid, "Faction") == 2)
	{
	    if(!sscanf(params, "i", skin))
    	{
	        switch(skin)
	        {
	            case 0:
				{
					format(string,sizeof(string), "* %s puts %s police uniform back in the locker.", GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
				}
				case 1:
				{
					SetPlayerSkin(playerid, 164);
					format(string, sizeof(string), "* %s gets on an agent uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 2:
				{
					SetPlayerSkin(playerid, 166);
					format(string, sizeof(string), "* %s gets on an agent suit.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 3:
				{
					SetPlayerSkin(playerid, 286);
					format(string, sizeof(string), "* %s gets on a FBI jacket.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 4:
				{
					SetPlayerSkin(playerid, 163);
					format(string, sizeof(string), "* %s gets on a bouncer uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 5:
				{
					SetPlayerSkin(playerid, 164);
					format(string, sizeof(string), "* %s gets on a bouncer suit.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				default:
				{
				    SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid Uniform!");
				    return 1;
				}
			}//switch
		}//Sscanf
		else
		{
		    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Uniform [Uniform ID]");
			SendClientMessage(playerid, COLOR_YELLOW, "_________Uniform List_________");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "0: No Uniform  1: Agent1");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "2: Agent2 3: FBI Jacket");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "4: Bouncer1  5: Bouncer2");
			return 1;
		}
	}
	/*
	else if(GetPVarInt(playerid, "Faction") == 3)
	{
	    if(!sscanf(params, "i", skin))
	    {
	        switch(skin)
	        {
	            case 0:
				{
					format(string,sizeof(string), "* %s puts %s police uniform back in the locker.", GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
				}
				case 1:
				{
					SetPlayerSkin(playerid, 285);
					format(string, sizeof(string), "* %s gets on an agent uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				default:
				{
				    SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid uniform!");
				    return 1;
				}
			}
	    }
	    else
		{
		    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Uniform [Uniform ID]");
			SendClientMessage(playerid, COLOR_YELLOW, "_________Uniform List_________");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "0: No Uniform  1: SWAT");
			return 1;
		}
	} */else if(GetPVarInt(playerid, "Faction") == 5) { //was 6 before for some reason
		if(!sscanf(params, "i", skin))
	    {
			switch(skin) {
	            case 0:
				{
					format(string,sizeof(string), "* %s puts %s EMS uniform back in the locker.", GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
				}
				case 1:
				{
					SetPlayerSkin(playerid, 274);
					format(string, sizeof(string), "* %s gets on a white paramedic uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 2:
				{
					SetPlayerSkin(playerid, 275);
					format(string, sizeof(string), "* %s gets on a blue paramedic uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 3:
				{
					SetPlayerSkin(playerid, 276);
					format(string, sizeof(string), "* %s gets on a green paramedic uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 4:
				{
					SetPlayerSkin(playerid, 277);
					format(string, sizeof(string), "* %s gets on a LA Firefighter uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 5:
				{
					SetPlayerSkin(playerid, 278);
					format(string, sizeof(string), "* %s gets on a LV Firefighter uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 6:
				{
					SetPlayerSkin(playerid, 279);
					format(string, sizeof(string), "* %s gets on a San Fierro Firefighter uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 7:
				{
					SetPlayerSkin(playerid, 93);
					format(string, sizeof(string), "* %s gets on a Los Angeles Paramedic uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				case 8:
				{
					SetPlayerSkin(playerid, 191);
					format(string, sizeof(string), "* %s gets on a LAFD uniform.", GetPlayerNameEx(playerid, ENameType_RPName));
					ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				default:
				{
				    SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid uniform!");
				    return 1;
				}
			}
		} else {
		    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Uniform [Uniform ID]");
			SendClientMessage(playerid, COLOR_YELLOW, "_________Uniform List_________");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "0: No Uniform  1: Los Angeles Paramedic");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "2: LV Paramedic  3: SF Paramedic");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "4: Los Angeles Firefighter  5: LV Paramedic");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "6: SF Firefighter	7: LAPMD Girl");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "8: LAFD Girl");
		}
	}
	return 1;
}
forward OnMDCLookup(playerid, name[]);
public OnMDCLookup(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	if(rows < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "* MDC: No one on record found!");
		return 1;
	}
	cache_get_row(0, 0, msg);
	MDCLookup(playerid, strval(msg));
	return 1;
}
MDCLookup(playerid, id) {
	new msg[256];	
	format(msg, sizeof(msg), "SELECT `c1`.`username`,`c1`.`phonenumber`,`c1`.`sex`,`c1`.`id` FROM `characters` AS `c1` WHERE `c1`.`id` = %d",id);
	mysql_function_query(g_mysql_handle, msg, true, "OnMDCRetrieve", "d",playerid);
}
forward OnMDCRetrieve(playerid);
public OnMDCRetrieve(playerid) {
	new string[128],id_string[128];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows < 1) return 0;
	SendClientMessage(playerid, TEAM_BLUE_COLOR,"______-=MOBILE DATA COMPUTER=-_______");
	cache_get_row(0, 0, id_string);
	format(string, sizeof(string), "Name : %s", id_string);
	SendClientMessage(playerid, X11_WHITE,string);
	cache_get_row(0, 1, id_string);
	format(string, sizeof(string), "Phone Number : %s", id_string);
	SendClientMessage(playerid, X11_WHITE,string);
	cache_get_row(0, 2, id_string);
	format(string, sizeof(string), "Sex : %s", GetSexName(strval(id_string)));
	SendClientMessage(playerid, X11_WHITE,string);
	cache_get_row(0, 3, id_string);
	new sqlid = strval(id_string);
	sendCrimes(playerid, sqlid);
	sendTickets(playerid, sqlid);
	return 1;
}
forward OnCrimesRetrieve(playerid);
public OnCrimesRetrieve(playerid) {
	new rows, fields;
	new id_string[128];
	new string[128];
	cache_get_data(rows, fields);
	SendClientMessage(playerid, TEAM_BLUE_COLOR,"______________ * Crimes * _______________");
	if(rows > 0) {
		for(new i=0;i<rows;i++) {
			cache_get_row(i, 2, id_string);
			if(!strcmp(id_string, "NULL", true)) {
				format(string, sizeof(string), "* Issuer: %s",id_string);
				SendClientMessage(playerid, X11_WHITE,string);
			}
			cache_get_row(i, 3, id_string);
			format(string, sizeof(string), "* Reason: %s",id_string);
			SendClientMessage(playerid, X11_WHITE,string);
			cache_get_row(i, 4, id_string);
			if(id_string[0] == '0') {
				format(string, sizeof(string), "* Issued: %s",id_string);
				SendClientMessage(playerid, X11_WHITE,string);
			}
			SendClientMessage(playerid, X11_WHITE,"*************************");
		}
	}
}
forward OnTicketsRetrieve(playerid);
public OnTicketsRetrieve(playerid) {
	new rows, fields;
	new id_string[128];
	new string[128];
	cache_get_data(rows, fields);
	SendClientMessage(playerid, TEAM_BLUE_COLOR,"______________ * Tickets * _______________");
	if(rows > 0) {
		for(new i=0;i<rows;i++) {
			cache_get_row(i, 2, id_string);
			format(string, sizeof(string), "* Issuer: %s",id_string);
			SendClientMessage(playerid, X11_WHITE,string);
			cache_get_row(i, 3, id_string);
			format(string, sizeof(string), "* Reason: %s",id_string);
			SendClientMessage(playerid, X11_WHITE,string);
			cache_get_row(i, 4, id_string);
			format(string, sizeof(string), "* Issued: %s",id_string);
			SendClientMessage(playerid, X11_WHITE,string);
			SendClientMessage(playerid, X11_WHITE,"*************************");
		}
	}
}
giveOrRemoveShield(playerid) {
	new msg[128];
	if(GetPVarType(playerid, "UsingSWATShield") != PLAYER_VARTYPE_NONE) {
		removeShieldFromPlayer(playerid);
		format(msg, sizeof(msg), "* %s has left their heavy enforcement police shield inside their locker.",GetPlayerNameEx(playerid, ENameType_RPName));
	} else {
		attachSWATShieldToPlayer(playerid);
		format(msg, sizeof(msg), "* %s has suited %sself up with a heavy enforcement police shield.",GetPlayerNameEx(playerid, ENameType_RPName), getThirdPersonPersonalPronoun(playerid));
	}
	ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
}
attachSWATShieldToPlayer(playerid) {
	if(IsPlayerAttachedObjectSlotUsed(playerid, 9)) {
		toggleAccessorySlot(playerid, 9);
	} else {
		//SetPlayerAttachedObject(playerid, 9, POLICE_SHIELD, BONE_RARM, 0.3, 0, 0, 0, 170, 270, 1, 1, 1);
		SetPlayerAttachedObject(playerid, 9, POLICE_SHIELD, BONE_LFOREARM, 0.35, 0.0,  0.0,  0.0, 0.0, 180.0);
	}
	SetPVarInt(playerid, "UsingSWATShield", 1);
}
removeShieldFromPlayer(playerid) {
	RemovePlayerAttachedObject(playerid, 9);
	if(GetPVarType(playerid, "UsingSWATShield") != PLAYER_VARTYPE_NONE) {
		if(GetPVarInt(playerid, "UsingSWATShield") == 1) {
			toggleAccessorySlot(playerid, 9);
			DeletePVar(playerid, "UsingSWATShield");
		}
	}
	return 1;
}
sendCrimes(playerid, sqlid) {
	format(query, sizeof(query), "SELECT `crimes`.`id`,`c1`.`username`,`c2`.`username`,`reason`,`issuetime` FROM `crimes` INNER JOIN `characters` AS `c1` ON `c1`.`id` = `suspect` LEFT JOIN `characters` AS `c2` ON `c2`.`id` = `issuer` WHERE `suspect` = %d",sqlid);
	mysql_function_query(g_mysql_handle, query, true, "OnCrimesRetrieve", "d",playerid);
}
sendTickets(playerid, sqlid) {
	format(query, sizeof(query), "SELECT `tickets`.`id`,`c1`.`username`,IFNULL(`c2`.`username`,\"Los Santos\"),`reason`,`issuetime` FROM `tickets` INNER JOIN `characters` AS `c1` ON `c1`.`id` = `owner` LEFT JOIN `characters` AS `c2` ON `c2`.`id` = `issuer` WHERE `owner` = %d",sqlid);
	mysql_function_query(g_mysql_handle, query, true, "OnTicketsRetrieve", "d",playerid);
}
InsertCrime(target, playerid, crime[]) {
	query[0] = 0;
	new crime_esc[(64*2)+1];
	mysql_real_escape_string(crime, crime_esc);
	format(query, sizeof(query), "INSERT INTO `crimes` (`issuer`,`suspect`,`reason`) VALUES (%d, %d, \"%s\")",GetPVarInt(playerid, "CharID"), GetPVarInt(target, "CharID"),crime_esc);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
SendCrimeMessage(target, playerid, crime[]) {
	new msg[128];
	format(msg, sizeof(msg), "HQ: All Units APB: Reporter: %s",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
	SendCopMessage(TEAM_BLUE_COLOR, msg);
	format(msg, sizeof(msg), "HQ: Crime: %s, Suspect: %s",crime,GetPlayerNameEx(target, ENameType_RPName_NoMask));
	SendCopMessage(TEAM_BLUE_COLOR, msg);
	return 1;
}
SendCopMessage(colour, const msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsAnLEO(i)) {
				SendClientMessage(i, colour, msg);
			}
		}
	}
	SendBigFactionEarsMessage(colour, msg);
}
showEquipMenu(playerid) {
	new temptxt[256];
	dialogstr[0] = 0;
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	for(new i=0;i<sizeof(EquipSlots);i++) {
		if(faction == EquipSlots[i][EEquipFaction]) {
			if(rank >= EquipSlots[i][EEquipMinRank]) {
				format(temptxt, sizeof(temptxt), "%s\n",EquipSlots[i][EEquipName]);
				strcat(dialogstr,temptxt,sizeof(dialogstr));
			}
		}
	}
	ShowPlayerDialog(playerid, EFactionDialog_EquipChoose, DIALOG_STYLE_LIST, "{00BFFF}Equip Menu",dialogstr, "Ok", "Cancel");
}
getEquipListedAt(playerid, index){
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	new x;
	for(new i=0;i<sizeof(EquipSlots);i++){
		if(faction == EquipSlots[i][EEquipFaction]) {
			if(rank >= EquipSlots[i][EEquipMinRank]) {
				if(x++ == index) 
					return i;
			}
		}
	}
	return -1;
}
IsAnLEO(playerid) {
	new faction = GetPVarInt(playerid, "Faction");
	if(faction < 1 || faction > sizeof(Factions)) {
		return 0;
	}
	return getFactionType(faction) == EFactionType_LEO;
}
IsOnDuty(playerid) {
	return GetPVarInt(playerid, "CopDuty");
}
onEquipResponse(playerid, index) {
	new slot = getEquipListedAt(playerid, index);
	new msg[128];
	if(GetPVarType(playerid, "CopDuty") == PLAYER_VARTYPE_NONE && !isOnMedicDuty(playerid)) {
		return 1;
	}
	if(GetPVarInt(playerid, "Rank") < EquipSlots[slot][EEquipMinRank]) {
		return 1;
	}
	ResetPlayerWeaponsEx(playerid);
	if(EquipSlots[slot][EEquipGun1] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun1], -1);
	}
	if(EquipSlots[slot][EEquipGun2] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun2], -1);
	}
	if(EquipSlots[slot][EEquipGun3] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun3], -1);
	}
	if(EquipSlots[slot][EEquipGun4] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun4], -1);
	}
	if(EquipSlots[slot][EEquipGun5] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun5], -1);
	}
	if(EquipSlots[slot][EEquipGun6] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun6], -1);
	}
	if(EquipSlots[slot][EEquipGun7] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun7], -1);
	}
	if(EquipSlots[slot][EEquipGun8] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun8], -1);
	}
	if(EquipSlots[slot][EEquipGun9] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun9], -1);
	}
	if(EquipSlots[slot][EEquipGun10] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun10], -1);
	}
	if(EquipSlots[slot][EEquipGun11] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun11], -1);
	}
	if(EquipSlots[slot][EEquipGun12] != 0) {
		GivePlayerWeaponEx(playerid, EquipSlots[slot][EEquipGun12], -1);
	}
	SetPlayerHealthEx(playerid, 100.0);
	SetPlayerArmourEx(playerid, 100.0);
	format(msg, sizeof(msg), "* %s has suited %sself up with %s equipment.",GetPlayerNameEx(playerid, ENameType_RPName), getThirdPersonPersonalPronoun(playerid),EquipSlots[slot][EEquipName]);
	ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 0;
}
IsAtDutySpot(playerid) {
	new faction = GetPVarInt(playerid, "Faction");
	if(IsPlayerInRangeOfPoint(playerid, 5.0, -2.977178, 17.388200, 1499.265991)) {
		return 1;
	}
	if(IsPlayerInRangeOfPoint(playerid, 10.0,223.302413, 186.100479, 1003.031250)) {
		if(GetPlayerVirtualWorld(playerid) == 3) {
			return 1;
		}
	}
	if(IsPlayerInRangeOfPoint(playerid, 25.0, 347.181213, 162.776077, 1014.187500)) {
		if(GetPlayerVirtualWorld(playerid) == 1 && GetPlayerInterior(playerid) == 3) {
			return 1;
		}
	}
	if(IsPlayerInRangeOfPoint(playerid, 25.0, 1459.325805, -1762.857421, 3285.285888)) {
		if(GetPlayerVirtualWorld(playerid) == 1 && GetPlayerInterior(playerid) == 1) {
			return 1;
		}
	}
	if(IsPlayerInRangeOfPoint(playerid, 25.0, -57.011417, -266.743530, 1005.561462)) {
		if(GetPlayerVirtualWorld(playerid) == -1 && GetPlayerInterior(playerid) == 35) {
			return 1;
		}
	}
	if(IsPlayerInRangeOfPoint(playerid, 5.0, 597.397277, -619.103271, -13.975000)) {
		return 2;
	}
	if(IsPlayerInRangeOfPoint(playerid, 75.0, 637.328491, -619.281921, -3.506249)) {
		return 2;
	}
	if(isInsideHospital(playerid) || isAtLAFD(playerid)) { //Don't feel like writing more lines of code when we have that
		if(getFactionType(faction) == EFactionType_EMS) { //Only if they are in the EMS/LAFD faction
			return 1; //isInsideHospital(playerid)
		}
	}
	return 0;
}
IsAtArrestSpot(playerid) {
	if(IsPlayerInRangeOfPoint(playerid, 5.0, 1559.508300, -1694.316650, 5.896991)) { //LSPD interior
		return 0;
	} 
	if(IsPlayerInRangeOfPoint(playerid, 5.0, 309.960906, -1516.312744, 24.921875)) { //FBI garage
		return 1;
	}
	if(IsPlayerInRangeOfPoint(playerid, 255.0, 197.847595, 178.960739, 1003.023437)) { //inside FBI HQ
		if(GetPlayerVirtualWorld(playerid) == 3 && GetPlayerInterior(playerid) == 3) {
			return 1;
		}
	}
	if(IsPlayerInRangeOfPoint(playerid, 5.0, 597.397277, -619.103271, -13.975000)) {
		return 2;
	}
	if(IsAtMDC(playerid)) {
		return 1;
	}
	return -1;
}
ICJail(playerid,target,time,bail,location) {
	new string[128];
	new money = (GetPlayerWantedLevel(target)*1000);
	if(money > 25000)
	    money = 25000;
	GiveMoneyEx(target, -money);
	GiveMoneyEx(playerid, money);
	
	if(bail > 0) {
		format(string, sizeof(string), "You are jailed for %d seconds.   Bail: $%s", time*60, getNumberString(bail));
	} else {
		format(string, sizeof(string), "You are jailed for %d seconds.   Bail: None", time*60);
	}
	new jailtime;
	jailtime += floatround((time*60));
	SendClientMessage(target, COLOR_LIGHTBLUE, string);
	new arrests = GetPVarInt(target, "Arrests");
	SetPVarInt(target, "Arrests", arrests+1);
	TogglePlayerControllableEx(target, true);
	SetPVarInt(target, "Bail", bail);
	SetPVarInt(target, "ReleaseTime", gettime() + jailtime);
	SetPVarInt(target, "WantedLevel", 0);
	SetPlayerWantedLevel(target, 0);
	SetPVarInt(target, "JailType", location);
	clearCrimes(target);
	if(location == 0) {
		new cell = RandomEx(0, sizeof(LSPDCells));
		SetPlayerPos(target, LSPDCells[cell][ECellX], LSPDCells[cell][ECellY], LSPDCells[cell][ECellZ]);
		SetPlayerFacingAngle(target, LSPDCells[cell][ECellAngle]);
		SetPlayerVirtualWorld(target, LSPDCells[cell][ECellVW]);
		SetPlayerInterior(target, LSPDCells[cell][ECellInterior]);
	} else if(location == 1) {
		new cell = RandomEx(0, sizeof(FBICells));
		SetPlayerPos(target, FBICells[cell][ECellX], FBICells[cell][ECellY], FBICells[cell][ECellZ]);
		SetPlayerFacingAngle(target, FBICells[cell][ECellAngle]);
		SetPlayerVirtualWorld(target, FBICells[cell][ECellVW]);
		SetPlayerInterior(target, FBICells[cell][ECellInterior]);
	} else if(location == 2) {
		new cell = RandomEx(0, sizeof(DilCells));
		SetPlayerPos(target, DilCells[cell][ECellX], DilCells[cell][ECellY], DilCells[cell][ECellZ]);
		SetPlayerFacingAngle(target, DilCells[cell][ECellAngle]);
		SetPlayerVirtualWorld(target, DilCells[cell][ECellVW]);
		SetPlayerInterior(target, DilCells[cell][ECellInterior]);
		
	}
	SetPlayerSpecialAction(target, SPECIAL_ACTION_NONE);
	TogglePlayerControllableEx(target, 1);
	DeletePVar(target, "CuffTime");
	hangupCall(target, 1);
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	format(string, sizeof(string), "%s has been sent to prison for %d minutes by %s %s.", GetPlayerNameEx(target,ENameType_RPName), time, getFactionRankName(faction, rank), GetPlayerNameEx(playerid,ENameType_RPName));
	SendSplitClientMessageToAll(X11_TOMATO_2, string, 0, sizeof(string));
	if(IsPlayerAttachedObjectSlotUsed(target, 8)) {
		RemovePlayerAttachedObject(target, 8);
	}
	return 1;
}
playerHasPrisonLifeSentence(playerid) {
	new flags = GetPVarInt(playerid, "UserFlags");
	if(flags & EUFRPSInPrison) { 
		return 1;
	}
	return 0;
}
putInLEOCells(playerid, location) {
	if(location == 0) {
		new cell = RandomEx(0, sizeof(LSPDCells));
		SetPlayerPos(playerid, LSPDCells[cell][ECellX], LSPDCells[cell][ECellY], LSPDCells[cell][ECellZ]);
		SetPlayerFacingAngle(playerid, LSPDCells[cell][ECellAngle]);
		SetPlayerVirtualWorld(playerid, LSPDCells[cell][ECellVW]);
		SetPlayerInterior(playerid, LSPDCells[cell][ECellInterior]);
	} else if(location == 1) {
		new cell = RandomEx(0, sizeof(FBICells));
		SetPlayerPos(playerid, FBICells[cell][ECellX], FBICells[cell][ECellY], FBICells[cell][ECellZ]);
		SetPlayerFacingAngle(playerid, FBICells[cell][ECellAngle]);
		SetPlayerVirtualWorld(playerid, FBICells[cell][ECellVW]);
		SetPlayerInterior(playerid, FBICells[cell][ECellInterior]);
	} else if(location == 2) {
		new cell = RandomEx(0, sizeof(DilCells));
		SetPlayerPos(playerid, DilCells[cell][ECellX], DilCells[cell][ECellY], DilCells[cell][ECellZ]);
		SetPlayerFacingAngle(playerid, DilCells[cell][ECellAngle]);
		SetPlayerVirtualWorld(playerid, DilCells[cell][ECellVW]);
		SetPlayerInterior(playerid, DilCells[cell][ECellInterior]);	
		
	}
}
leoOnLoadCharacter(playerid) {
	new jailtime = GetPVarInt(playerid, "ReleaseTime");
	new jailtype = GetPVarInt(playerid, "JailType");
	if(jailtime != 0 || playerHasPrisonLifeSentence(playerid)) {
		if(jailtype == 0) {
			new cell = RandomEx(0, sizeof(LSPDCells));
			SetPlayerPos(playerid, LSPDCells[cell][ECellX], LSPDCells[cell][ECellY], LSPDCells[cell][ECellZ]);
			SetPlayerFacingAngle(playerid, LSPDCells[cell][ECellAngle]);
			SetPlayerVirtualWorld(playerid, LSPDCells[cell][ECellVW]);
			SetPlayerInterior(playerid, LSPDCells[cell][ECellInterior]);
		} else if(jailtype == 1) {
			new cell = RandomEx(0, sizeof(FBICells));
			SetPlayerPos(playerid, FBICells[cell][ECellX], FBICells[cell][ECellY], FBICells[cell][ECellZ]);
			SetPlayerFacingAngle(playerid, FBICells[cell][ECellAngle]);
			SetPlayerVirtualWorld(playerid, FBICells[cell][ECellVW]);
			SetPlayerInterior(playerid, FBICells[cell][ECellInterior]);
		} else if(jailtype == 2) {
			new cell = RandomEx(0, sizeof(DilCells));
			SetPlayerPos(playerid, DilCells[cell][ECellX], DilCells[cell][ECellY], DilCells[cell][ECellZ]);
			SetPlayerFacingAngle(playerid, DilCells[cell][ECellAngle]);
			SetPlayerVirtualWorld(playerid, DilCells[cell][ECellVW]);
			SetPlayerInterior(playerid, DilCells[cell][ECellInterior]);		
		}
		SendClientMessage(playerid, COLOR_PURPLE, "** You wake up in Jail **");
	} else {
		checkCrimes(playerid);
		checkTickets(playerid);
	}
}
tryPayTickets(playerid) {
	new tprice = GetPVarInt(playerid, "TicketTotal");
	if(GetMoneyEx(playerid) < tprice) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
		return 1;
	}
	GiveMoneyEx(playerid, -tprice);
	query[0] = 0;
	new total = numTickets(playerid);
	format(query, sizeof(query), "DELETE FROM `tickets` WHERE `owner` = %d", GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	new pvarname[64];
	for(new i=0;i<total;i++) {
		format(pvarname, sizeof(pvarname), "TicketIssueName%d", i);
		DeletePVar(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "TicketIssuer%d",i);
		DeletePVar(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "TicketPrice%d",i);
		DeletePVar(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "TicketReason%d", i);
		DeletePVar(playerid, pvarname);
	}
	DeletePVar(playerid, "TicketTotal");
	SendClientMessage(playerid, COLOR_DARKGREEN, "* You have paid for your tickets.");
	return 1;
}
forward onCheckTickets(playerid);
public onCheckTickets(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	new reason[128],price,issuer[MAX_PLAYER_NAME],id_string[32];
	new pvarname[64];
	new tprice;
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, reason);
		cache_get_row(i, 1, issuer);
		cache_get_row(i, 2, id_string);
		price = strval(id_string);
		tprice += price;
		cache_get_row(i, 3, id_string);
		new slot = GetTicketSlot(playerid);
		format(pvarname, sizeof(pvarname), "TicketIssueName%d", slot);
		SetPVarString(playerid, pvarname, issuer);
		format(pvarname, sizeof(pvarname), "TicketIssuer%d",slot);
		SetPVarInt(playerid, pvarname, strval(id_string));
		format(pvarname, sizeof(pvarname), "TicketPrice%d",slot);
		SetPVarInt(playerid, pvarname, price);
		format(pvarname, sizeof(pvarname), "TicketReason%d", slot);
		SetPVarString(playerid, pvarname, reason);
	}
	SetPVarInt(playerid, "TicketTotal", tprice);
	if(rows != 0) {
		SetPVarInt(playerid, "NumTickets", rows);
		format(reason, sizeof(reason), "* You have %d unpaid tickets, totalling $%d!",rows, tprice);
		SendClientMessage(playerid, X11_WHITE, reason);
	}
	return 1;
}
checkTickets(playerid) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `tickets`.`reason`,IFNULL(`username`,\"Los Santos\"),`price`,`characters`.`id` FROM `tickets` LEFT JOIN `characters` on `issuer` = `characters`.`id` WHERE `owner` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "onCheckTickets", "d",playerid);
}
checkCrimes(playerid) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT COUNT(*) FROM `crimes` WHERE `suspect` = %d", GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "onCheckCrimes", "d",playerid);
}
YCMD:clear(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Clears a persons record");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(!IsAtMDC(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not at a computer!");
		return 1;
	}
	new user;
	if(!sscanf(params,"k<playerLookup>", user)) {
		query[0] = 0;
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		clearCrimes(user);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Players Crimes/Tickets Cleared!");
		SendClientMessage(user, COLOR_LIGHTBLUE, "* Your Crimes/Tickets have been cleared!");
		new msg[128];
		format(msg, sizeof(msg), "HQ: %s has cleared %s's crimes and tickets!",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(user, ENameType_RPName_NoMask));
		SendCopMessage(TEAM_BLUE_COLOR, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /clear [playerid/name]");
	}
	return 1;
}
clearCrimes(user) {
	format(query, sizeof(query), "DELETE FROM `crimes` WHERE `suspect` = %d",GetPVarInt(user, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query), "DELETE FROM `tickets` WHERE `owner` = %d",GetPVarInt(user, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SetPVarInt(user, "WantedLevel", 0);
	SetPlayerWantedLevel(user, 0);
	new total = numTickets(user);
	new pvarname[64];
	for(new i=0;i<total;i++) {
		format(pvarname, sizeof(pvarname), "TicketIssueName%d", i);
		DeletePVar(user, pvarname);
		format(pvarname, sizeof(pvarname), "TicketPrice%d",i);
		DeletePVar(user, pvarname);
		format(pvarname, sizeof(pvarname), "TicketReason%d",i);
		DeletePVar(user, pvarname);
	}
}
YCMD:megaphone(playerid, params[], help) {
	if(!IsPlayerHoldingMegaphone(playerid)) {
		if(!isInLEOVehicle(playerid) && !isInMedicVehicle(playerid) && !isInGovernmentVehicle(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This vehicle doesn't have a megaphone!");
			return 1;
		}
		if(!IsAnLEO(playerid) && !isGovernment(playerid) && !isMedic(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
	}
	new msg[128], string[128];
	if(!sscanf(params, "s[128]", msg)) {
		format(string, sizeof(string), "[%s: %s]",GetPlayerNameEx(playerid, ENameType_RPName), msg);
		ProxMessage(60.0, playerid, string,COLOR_YELLOW,COLOR_YELLOW,COLOR_YELLOW,COLOR_YELLOW,COLOR_YELLOW);
		sendMessageToHouses(playerid, 60.0, string, COLOR_YELLOW);
		sendMessageToBusinesses(playerid, 60.0, string, COLOR_YELLOW);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /megaphone [message]");
	}
	return 1;
}
YCMD:lifeprisonsentence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to toggle someone's life prison sentence. (Player must have a life sentence)");
		SendClientMessage(playerid, X11_WHITE, "Notice: Don't use this command to unprison someone, use \"/unprison\" instead.");
		return 1;
	}
	new string[128];
	new target, location;
	if(!sscanf(params, "k<playerLookup_acc>D(0)", target, location)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new flags = GetPVarInt(target, "UserFlags");
		if(~flags & EUFRPSInPrison) {
			flags |= EUFRPSInPrison;
			putInLEOCells(target, location);
			SetPVarInt(target, "JailType", location);
			format(string, sizeof(string), "%s has been given a life prison sentence for RP purposes by admin %s.", GetPlayerNameEx(target,ENameType_RPName), GetPlayerNameEx(playerid,ENameType_AccountName));
			SendSplitClientMessageToAll(X11_TOMATO_2, string, 0, sizeof(string));
		} else {
			flags &= ~EUFRPSInPrison;
			if(GetPVarType(target, "ReleaseTime") != PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_TOMATO_2, "The player is not in prison anymore but he / she hasn't been teleported since he / she has jail time, use \"/unprison\" for that.");
				return 1;
			}
			releaseFromJail(target);
		}
		SetPVarInt(target, "UserFlags", flags);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /lifeprisonsentence [playerid/name] [prison id]");
		SendClientMessage(playerid, X11_WHITE, "Prison ID's: 0 - LA Prison. 1 - FBI 2 - Dillimore Cells.");
		SendClientMessage(playerid, X11_WHITE, "By default the player gets sent to the LA Prison.");
	}
	return 1;
}
YCMD:government(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enter into government chat");
		return 1;
	}
	if(!IsAnLEO(playerid) && !isGovernment(playerid) && !isMedic(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop / Government / LAFD!");
		return 1;
	}
	if(!IsOnDuty(playerid) && !isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	new faction = GetPVarInt(playerid, "Faction");
	new rank = GetPVarInt(playerid, "Rank");
	if((faction == 1 && rank < 10) || faction != 1 && rank < 4) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your rank is too low!");
		return 1;
	}
	if(!IsGovernmentOpen()) {
		Broadcast(COLOR_WHITE, "|___________ Government Announcement ___________|");	
	}
	if(GetPVarType(playerid, "GovChat") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "GovChat");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Government chat ended.");
	} else {
		SetPVarInt(playerid, "GovChat", 1);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "(( Use 't' to talk. ))");
	}
	return 1;
}
IsGovernmentOpen() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "GovChat") == 1) {
				return 1;
			}
		}
	}
	return 0;
}
YCMD:ticket(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Issue a ticket on someone");
		return 1;
	}
	if(!IsAnLEO(playerid) && !isGovernment(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	new user, price, reason[64];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]",user,price,reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(price < 1 || price > 9999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Price!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 3.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
			return 1;
		}
		issueTicket(user, playerid, reason, price);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Ticket Issued");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ticket [user] [price] [reason]");
	}
	return 1;
}
forward onCheckCrimes(playerid);
public onCheckCrimes(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	new msg[128],id_string[32];
	cache_get_row(0, 0, id_string);
	new crimes = strval(id_string);
	SetPlayerWantedLevel(playerid, crimes);
	if(crimes != 0) {
		format(msg, sizeof(msg), "You have %d crime reports on you.", crimes);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	return 1;
}
releaseFromJail(playerid) {
	//SetPlayerPos(playerid, 1553.012939, -1675.824707, 16.195312);
	SetPlayerPos(playerid, 1800.2347,-1576.7334,14.0625);
	SetPlayerFacingAngle(playerid, 300.55);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, 0);
	SendClientMessage(playerid, X11_WHITE,"   You have paid Your debt to society.");
	GameTextForPlayer(playerid, "~g~Freedom~n~~w~Try to be a better citizen", 5000, 1);
	TogglePlayerControllableEx(playerid, 1);
}
JailTimer() {
	foreach(Player, i) {
		if(GetPVarType(i, "ReleaseTime") != PLAYER_VARTYPE_NONE) {
			new logintime = GetPVarInt(i, "CharLoginTime");
			new rtime = GetPVarInt(i, "ReleaseTime");
			if(gettime() > rtime) {
				if(gettime() - logintime >= 5) { //Wait a couple of seconds before doing it so it doesn't happen prior to spawn
					releaseFromJail(i);
					DeletePVar(i, "ReleaseTime");
				}
			}
		}
	}
}

stock IsAtMDC(playerid) {
	if(isInLEOVehicle(playerid)) {
		return 1;
	}
	if(IsPlayerInRangeOfPoint(playerid, 5.0, -20.904359, -43.595165, 1499.265991) || IsPlayerInRangeOfPoint(playerid, 5.0, 262.111114, 184.586822, 1008.171875)) { //LSPD, FBI
		return 1;
	}
	return 0;
}

GetTicketSlot(playerid) {
	new i = -1;
	new pvarname[64];
	do {
		format(pvarname, sizeof(pvarname), "TicketIssuer%d",++i);
	} while(GetPVarType(playerid, pvarname) != PLAYER_VARTYPE_NONE);
	return i;
}

issueTicket(playerid, issuer, reason[], price) {
	new slot = GetTicketSlot(playerid);
	new pvarname[64];
	query[0] = 0;
	format(pvarname, sizeof(pvarname), "TicketIssueName%d", slot);
	new name[MAX_PLAYER_NAME];
	if(issuer == -1) {
		format(name, sizeof(name), "Los Santos");
	} else {
		format(name, sizeof(name), "%s",GetPlayerNameEx(issuer,ENameType_RPName));
	}
	SetPVarString(playerid, pvarname, name);
	format(query, sizeof(query), "You have been issued a ticket by: %s", name);
	SendClientMessage(playerid, X11_ORANGE, query);
	format(query, sizeof(query), "Price: $%d, Reason: %s", price ,reason);
	SendClientMessage(playerid, COLOR_DARKGREEN, query);
	format(pvarname, sizeof(pvarname), "TicketIssuer%d",slot);
	SetPVarInt(playerid, pvarname, issuer);
	format(pvarname, sizeof(pvarname), "TicketPrice%d", slot);
	SetPVarInt(playerid, pvarname, price);
	format(pvarname, sizeof(pvarname), "TicketReason%d",slot);
	SetPVarString(playerid, pvarname, reason);
	new sqlid = -1;
	if(issuer != -1) {
		sqlid = GetPVarInt(issuer, "CharID");
	}
	new reason_esc[(128*2)+1];
	mysql_real_escape_string(reason,reason_esc);
	new tprice = GetPVarInt(playerid, "TicketTotal");
	tprice += price;
	SetPVarInt(playerid, "TicketTotal", tprice);
	format(query, sizeof(query), "INSERT INTO `tickets` (`issuer`,`owner`,`price`,`reason`) VALUES (%d,%d,%d,\"%s\")",sqlid,GetPVarInt(playerid, "CharID"),price,reason_esc);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	new num = GetPVarInt(playerid, "NumTickets");
	SetPVarInt(playerid, "NumTickets", ++num);
}
numTickets(playerid) {
	new index = -1;
	new pvarname[64];
	do {
		format(pvarname, sizeof(pvarname), "TicketIssuer%d",++index);
	} while(GetPVarType(playerid, pvarname) != PLAYER_VARTYPE_NONE);
	return index;
}
YCMD:tickets(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Displays your tickets and who set them");
		return 1;
	}
	new pvarname[64];
	new total = numTickets(playerid);
	new msg[128];
	new name[MAX_PLAYER_NAME],reason[128],price;
	new totalprice;
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "**** TICKETS ****");
	for(new i=0;i<total;i++) {
		format(pvarname, sizeof(pvarname), "TicketIssueName%d", i);
		GetPVarString(playerid, pvarname, name, sizeof(name));
		format(msg, sizeof(msg), "Ticket Issuer: {%s}%s",getColourString(COLOR_RED),name);
		SendClientMessage(playerid, X11_WHITE, msg);
		format(pvarname, sizeof(pvarname), "TicketPrice%d",i);
		price = GetPVarInt(playerid, pvarname);
		totalprice += price;
		format(pvarname, sizeof(pvarname), "TicketReason%d",i);
		GetPVarString(playerid, pvarname, reason, sizeof(reason));
		format(msg, sizeof(msg), "* Ticket Price: {%s}$%s{%s}, Reason: {%s}%s",getColourString(COLOR_GREEN), getNumberString(price),getColourString(X11_WHITE),getColourString(COLOR_RED), reason);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	format(msg, sizeof(msg), "Total Price: {%s}$%s",getColourString(COLOR_RED), getNumberString(totalprice));
	SendClientMessage(playerid, X11_WHITE, msg);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Don't forget to pay these before payday!");
	return 1;
}
leoOnPlayerShootWeapon(playerid, weaponid, hittype, damagedid) {
	#pragma unused weaponid
	new string[128];
	if(GetPVarInt(playerid, "HasTaser") == 1) {
		if(hittype != BULLET_HIT_TYPE_PLAYER) {
			ShowScriptMessage(playerid, "~r~Missed!~n~~w~Reloading ~r~taser~w~!.", 1000);
			SetPlayerArmedWeapon(playerid, 0);
			ReloadTaserTimer[playerid] = SetTimerEx("reloadTaser", 50, true, "dd", playerid, GetPlayerWeaponEx(playerid));
			TaserCoolDown[playerid] = gettime();
			return 1;
		}
		if(isValidTasingDistance(damagedid, playerid)) {
			format(string, sizeof(string), "* %s shoots with their Tazer which hooks onto %s and tazes him to the ground.", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(damagedid, ENameType_RPName));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			GameTextForPlayer(damagedid, "~r~Tazed", 2500, 3);
			TogglePlayerControllableEx(damagedid, 0);
			ApplyAnimation(damagedid, "CRACK", "crckdeth2", 4.0, 1, 0, 0, 0, 0);
			new timer = SetTimerEx("UnfreezeAndDeleteTaserTimer", 20000, false, "dd", damagedid, 1);
			SetPVarInt(damagedid, "TazerUnfreezeTimer",timer);
			if(IsPlayerAttachedObjectSlotUsed(playerid, 8)) {
				RemovePlayerAttachedObject(playerid, 8);
			}
			DeletePVar(playerid, "HasTaser");
			restoreGuns(playerid, "TaserGuns");
		}
	}
	return 1;
}
forward reloadTaser(playerid, weapon);
public reloadTaser(playerid, weapon) {
	new time = TaserCoolDown[playerid];
	new timenow = gettime();
	if(RELOAD_TASER_TIME-(timenow-time) > 0) {
		SetPlayerArmedWeapon(playerid, 0);
		return 1;
	}
	deleteTaserTimer(playerid);
	SetPlayerArmedWeapon(playerid, weapon);
	return 1;
}
deleteTaserTimer(playerid) {
	new timer = ReloadTaserTimer[playerid];
	KillTimer(timer);
	ReloadTaserTimer[playerid] = 0;
	TaserCoolDown[playerid] = 0;
}
isValidTasingDistance(damagedid, shooterid) {
	new Float: X, Float: Y, Float: Z;
	GetPlayerPos(damagedid, X, Y, Z);
	new Float: SDistance = GetPlayerDistanceFromPoint(shooterid, X, Y, Z);
	//printf("%f",SDistance);
	if(SDistance < MAX_TASING_DISTANCE) {
		return 1;
	}
	return 0;
}
leoOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	deleteTaserTimer(playerid);
}
forward UnfreezeAndDeleteTaserTimer(playerid);
public UnfreezeAndDeleteTaserTimer(playerid) {
	if(GetPVarType(playerid, "TazerUnfreezeTimer") != PLAYER_VARTYPE_NONE) {
		KillTimer(GetPVarInt(playerid, "TazerUnfreezeTimer"));
		DeletePVar(playerid, "TazerUnfreezeTimer");
		TogglePlayerControllableEx(playerid, 1);
	}
	return 1;
}
forward CheckStrips();
public CheckStrips()
{
	new Float:X, Float:Y, Float:Z;
	foreach(Player, playerid)
	{
		if(GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
    	{
    		for(new i = 0; i < sizeof(PlacedCopObjects); i++)
			{
				if(PlacedCopObjects[i][EPCO_ObjectID] != 0) {
					new index = PlacedCopObjects[i][EPCO_ObjectIndex];
					GetObjectPos(PlacedCopObjects[i][EPCO_ObjectID], X, Y, Z);
					if(IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
						if(CopObjects[index][ECopObjectType] == ECO_SpikeStrip) {
							new carid = GetPlayerVehicleID(playerid);
							new tirestatus, doorstatus, lightstatus, panelstatus;
							GetVehicleDamageStatus(carid,panelstatus, doorstatus, lightstatus, tirestatus);
							tirestatus = encode_tires(1,1,1,1);
							UpdateVehicleDamageStatus(carid, panelstatus, doorstatus, lightstatus, tirestatus);
						}
					}
				}
  	    	}
  		}
	}
	return 1;
}
isLEOObject(objectid) {
	for(new i=0;i<sizeof(PlacedCopObjects);i++) {
		if(PlacedCopObjects[i][EPCO_ObjectID] == objectid) {
			return 1;
		}
	}
	return 0;
}
YCMD:modifyobject(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modifys a cop object");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	SetPVarInt(playerid, "CopSelectType", 1);
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "(( Select the object you wish to modify ))");
	return 1;
}
YCMD:deleteobject(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys a cop object");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
	}
	SetPVarInt(playerid, "CopSelectType", 2);
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "(( Select the object you wish to delete ))");
	return 1;
}
leoOnPlayerSelectObject(playerid, objectid, modelid, Float:fX, Float:fY, Float:fZ) {
	#pragma unused fX
	#pragma unused fY
	#pragma unused fZ
	#pragma unused modelid
	new selecttype = GetPVarInt(playerid, "CopSelectType");
    if(/*type == SELECT_OBJECT_GLOBAL_OBJECT && */isLEOObject(objectid))
    {
		if(selecttype == 1) {
			SetPVarInt(playerid, "ModifyCopObject", 1);
			EditDynamicObject(playerid, objectid);
		} else if(selecttype == 2) {
			removeCopObject(objectid);
		}
    }
	DeletePVar(playerid, "CopSelectType");
}
leoOnPlayerEditObject( playerid, objectid, response, Float:fX, Float:fY, Float:fZ, Float:fRotX, Float:fRotY, Float:fRotZ) {
	if(!isLEOObject(objectid)) {
		return 1;
	}
	if(response == EDIT_RESPONSE_FINAL) {
		SetDynamicObjectPos(objectid, fX, fY, fZ);
		SetDynamicObjectRot(objectid, fRotX, fRotY, fRotZ);
	} else if(response == EDIT_RESPONSE_CANCEL) {
		if(GetPVarType(playerid, "ModifyCopObject") != PLAYER_VARTYPE_NONE) {
			removeCopObject(objectid);
			DeletePVar(playerid, "ModifyCopObject");
		}
	}
	return 1;
}
findCopIndexByObjectID(objectid) {
	for(new i = 0; i < sizeof(PlacedCopObjects); i++) {
		if(PlacedCopObjects[i][EPCO_ObjectID] == objectid) {
			return i;
		}
	}
	return -1;
}
removeCopObject(objectid) {
	new i = findCopIndexByObjectID(objectid);
	if(i != -1) { 
		PlacedCopObjects[i][EPCO_ObjectID] = 0;
		DestroyDynamicObject(objectid);
	}
	if(numSpikeStrips() < 1 && spiketimer == 0) {
		KillTimer(spiketimer);
		spiketimer = 0;
	}
}
/*
//Moved to jobs.pwn
YCMD:frisk(playerid, params[], help) {
	new target;
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 3.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
			return 1;
		}
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "|__________ Items with %s __________|", GetPlayerNameEx(target, ENameType_RPName));
		SendClientMessage(playerid, COLOR_WHITE, msg);
		if(GetPVarInt(target, "MatsA") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type A Materials");
		}
		if(GetPVarInt(target, "MatsB") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type B Materials");
		}
		if(GetPVarInt(target, "MatsC") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Type C Materials");
		}
		if(GetPVarInt(target, "Pot") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Pot");
		}
		if(GetPVarInt(target, "Coke") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Coke");
		}
		if(GetPVarInt(target, "Meth") > 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "| Meth");
		}
		if(HasSpecialItem(target)) {
			format(msg, sizeof(msg), "| %s", GetCarryingItemName(target));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
		new gun, ammo, gunname[32];
		for(new i=0;i<12;i++) {
			GetPlayerWeaponData(target, i, gun, ammo);
			if(gun != 0) {
				GetWeaponNameEx(gun, gunname, sizeof(gunname));
				format(msg, sizeof(msg), "| Weapon: %s",gunname);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
			}
		}
		format(msg, sizeof(msg), "* %s has frisked %s.", GetPlayerNameEx(playerid, ENameType_RPName) ,GetPlayerNameEx(target, ENameType_RPName));
		ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /frisk [playerid/name]");
	}
	return 1;
}
*/
YCMD:take(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes an item from someone");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	new target, item[32];
	new string[128];
	if(!sscanf(params, "k<playerLookup>s[32]", target, item)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(target, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
			return 1;
		}
		new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(target, "LicenseFlags");
		if(!strcmp(item, "DrivingLicense", true)) {
			lflags &= ~ELicense_Drivers;
			format(string, sizeof(string), "* You have taken away %s's drivers license.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your drivers license.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
		} else if(!strcmp(item, "GunLicense", true)) {
			lflags &= ~ELicense_Gun;
			format(string, sizeof(string), "* You have taken away %s's weapons license.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your weapons license.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
		} else if(!strcmp(item, "FlyingLicense", true)) {
			lflags &= ~ELicense_Flying;
			format(string, sizeof(string), "* You have taken away %s's flying license.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your flying license.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
		} else if(!strcmp(item, "Guns", true) || !strcmp(item, "Weapons", true)) {
			format(string, sizeof(string), "* You have taken away %s's weapons.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your weapons.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			ResetPlayerWeaponsEx(target);
		} else if(!strcmp(item, "Drugs", true)) {
			format(string, sizeof(string), "* You have taken away %s's drugs.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your drugs.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			SetPVarInt(target, "Pot", 0);
			SetPVarInt(target, "Coke", 0);
			SetPVarInt(target, "Meth", 0);
			deletePlayerDrugVars(target, 1); //1 means only drugs
		}else if(!strcmp(item, "Materials", true)) {
			format(string, sizeof(string), "* You have taken away %s's materials.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your materials.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			SetPVarInt(target, "MatsA", 0);
			SetPVarInt(target, "MatsB", 0);
			SetPVarInt(target, "MatsC", 0);
		}else if(!strcmp(item, "SpecialItem", true)) {
			format(string, sizeof(string), "* You have taken away %s's special items.", GetPlayerNameEx(target, ENameType_RPName_NoMask));
			SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
			format(string, sizeof(string), "* Officer %s has taken away your special items.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
			SendClientMessage(target, COLOR_LIGHTBLUE, string);
			RemovePlayerItem(target);
		}
		SetPVarInt(target ,"LicenseFlags", _:lflags);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /take [playerid/name] [type]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Types: DrivingLicense, ProfessionalDLicense, GunLicense, FlyingLicense, Guns, Materials, Drugs");
	}
	return 1;
}
YCMD:givegunlicense(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Give someone a gun license");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	if(GetPVarInt(playerid, "Rank") < 8) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not the required rank!");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!tryGiveGunLicense(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person already has a gun license!");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givegunlicense [playerid/name]");
	}
	return 1;
}
tryGiveGunLicense(playerid, temporary = 0) {
	new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
	if(lflags & ELicense_Gun) {
		return 0;
	}
	lflags |= ELicense_Gun;
	if(temporary) {
		lflags |= ELicense_IsTempGunLic;
	} else {
		lflags &= ~ELicense_IsTempGunLic;
	}
	SetPVarInt(playerid ,"LicenseFlags", _:lflags);
	return 1;
}
leoTryEnterExit(playerid) {
	new msg[32];
	for(new i=0;i<sizeof(LEODoors);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, LEODoors[i][ELEODoorX], LEODoors[i][ELEODoorY], LEODoors[i][ELEODoorZ])) {
			if(!IsAnLEO(playerid) && LEODoors[i][ELEOOnly] == 1) {
				GameTextForPlayer(playerid, "~r~Access Denied", 3000, 1);
				return 1;
			}
			format(msg, sizeof(msg), "~w~%s",LEODoors[i][ELEODoorName]);
			GameTextForPlayer(playerid, msg, 1000, 1);
			if(LEODoors[i][ELEOExitInt] != -1) {
				SetPlayerInterior(playerid,LEODoors[i][ELEOExitInt]);
			}
			if(LEODoors[i][ELEOExitVW] != -1) {
				SetPlayerVirtualWorld(playerid, LEODoors[i][ELEOExitVW]);
			}
			SetPlayerPos(playerid, LEODoors[i][ELEODoorEX], LEODoors[i][ELEODoorEY], LEODoors[i][ELEODoorEZ]);
		} else if(IsPlayerInRangeOfPoint(playerid, 1.5, LEODoors[i][ELEODoorEX], LEODoors[i][ELEODoorEY], LEODoors[i][ELEODoorEZ])) {
			if(!IsAnLEO(playerid) && LEODoors[i][ELEOOnly] == 1) {
				GameTextForPlayer(playerid, "~r~Access Denied", 3000, 1);
				return 1;
			}
			if(LEODoors[i][ELEODoorInt] != -1) {
				SetPlayerInterior(playerid,LEODoors[i][ELEODoorInt]);
			}
			if(LEODoors[i][ELEOVW] != -1) {
				SetPlayerVirtualWorld(playerid, LEODoors[i][ELEOVW]);
			}
			SetPlayerPos(playerid, LEODoors[i][ELEODoorX], LEODoors[i][ELEODoorY], LEODoors[i][ELEODoorZ]);
		}
	}
	return 0;
}
YCMD:cops(playerid, params[], help) {
	new msg[128];
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "** LEO Members Online **");
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(IsAnLEO(i)) {
				new faction = GetPVarInt(i, "Faction");
				new rank = GetPVarInt(i, "Rank");
				format(msg, sizeof(msg), "[%s] %s %s",GetFactionName(faction),getFactionRankName(faction, rank),GetPlayerNameEx(i, ENameType_RPName_NoMask));
				SendClientMessage(playerid, IsOnDuty(i)?X11_BLUE:X11_WHITE, msg);
			}
		}
	}
	return 1;
}
leoSysTimer() {
	foreach(Player, i) {
		if(GetPVarInt(i, "HasTaser")) {
			new gun, ammo;
			GetPlayerWeaponDataEx(i, 2, gun, ammo);
			if(gun == 0 || ammo == 0) {
				DeletePVar(i, "HasTaser");
				restoreGuns(i, "TaserGuns");
				if(IsPlayerAttachedObjectSlotUsed(i, 8)) {
					RemovePlayerAttachedObject(i, 8);
				}
			}
		}
	}
}