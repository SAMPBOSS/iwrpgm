#define MAX_WAREHOUSES 60

enum {
	EWareHouses_BuyMenu = EWareHouses_Base+1,
};
enum EWareHouseInfo {
	EWareHouse_SQLID,
	EWareHouse_Name[32],
	EWareHouse_OwnerType, //1 = user, 2 = family
	EWareHouse_OwnerSQLID, //sqlid of owner/family
	EWareHouse_Ownername[MAX_PLAYER_NAME], //the name if its a player owned biz
	EWareHouse_PickupID,
	Float:EWareHouse_X,
	Float:EWareHouse_Y,
	Float:EWareHouse_Z,
	EWareHouse_Interior,
	Text3D:EWareHouse_3DText,
	EWareHouse_Closed,
	EWareHouse_GunProfit, //how much money you make on a gun purchase(value of 25 gives you $25 per gun)
	EWareHouse_DrugProfit, //same but for drugs
	EWareHouse_Till,
	EWareHouse_Mats[3], //a, b, c
	EWareHouse_Drugs[3], //pot, coke, meth
};

new WareHouses[MAX_WAREHOUSES][EWareHouseInfo];

enum {
	EIllegalType_Gun,
	EIllegalType_Pot,
	EIllegalType_Coke,
	EIllegalType_Meth,
};

enum EWareHouseBuyable {
	EIllegalItem_GunID,
	EIllegalItem_Type,
	EIllegalItem_Price,
	EIllegalItem_Amount, //how many bullets/drugs to give
	//cost to business if gun
	EIllegalItem_MatsPriceA,
	EIllegalItem_MatsPriceB,
	EIllegalItem_MatsPriceC,
	EIllegalItem_TimeRequired, //Time required to buy the gun (in hours)
};

new WareHouseBuyableItems[][EWareHouseBuyable] = {
{15,EIllegalType_Gun,600,-1,650,0,0,0},
{5,EIllegalType_Gun,400,-1,300,0,0,0},
{4,EIllegalType_Gun,600,-1,650,0,0,400},
{2,EIllegalType_Gun,600,-1,650,0,0,0},
{1,EIllegalType_Gun,200,-1,300,0,0,0}
};

enum EWareHouseInteriors {
	Float:EIBX,
	Float:EIBY,
	Float:EIBZ,
	Float:EIBAngle,
	EIBInterior,
	EIBName[32],
	EIBPrice,
};
//new WareHouseInteriors[][EWareHouseInteriors] = {{422.292694, 2536.67560, 10.0, 95.035987,10, "Small House",500000}};
new WareHouseInteriors[][EWareHouseInteriors] = {{-2240.4685,137.0600,1035.4140, 270.0,6, "Warehouse",500000}};

wareHousesOnGameModeInit() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `w`.`id`,`w`.`name`,`w`.`ownertype`,`w`.`owner`,`c`.`username`,`w`.`x`,`w`.`y`,`w`.`z`,`w`.`interiortype`,`w`.`closed`,`w`.`till`,`w`.`matsa`,`w`.`matsb`,`w`.`matsc`,`w`.`pot`,`w`.`coke`,`w`.`meth`,`w`.`gunprofit`,`w`.`drugprofit` FROM `warehouses` AS `w` LEFT JOIN `characters` AS `c` ON `owner` = `c`.`id`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadWareHouse", "");
}
forward OnLoadWareHouse();
public OnLoadWareHouse() {
	new rows, fields;
	new id_string[32];
	cache_get_data(rows, fields);
	if(rows > MAX_WAREHOUSES) {
		printf("Warning: Increase MAX_WAREHOUSES to %d\n",rows);
	}
	for(new i=0;i<rows;i++) {
		new slot = findFreeWareHouse();
		if(slot == -1) {
			printf("WareHouses IS FULL!\n");
			continue;
		}
		cache_get_row(i, 0, id_string);
		WareHouses[slot][EWareHouse_SQLID] = strval(id_string);
		
		cache_get_row(i, 1, WareHouses[slot][EWareHouse_Name]);
		
		cache_get_row(i, 2, id_string);
		WareHouses[slot][EWareHouse_OwnerType] = strval(id_string);
		
		cache_get_row(i, 3, id_string);
		WareHouses[slot][EWareHouse_OwnerSQLID] = strval(id_string);
		
		
		if(WareHouses[slot][EWareHouse_OwnerType] == 1) {
			cache_get_row(i, 4, WareHouses[slot][EWareHouse_Ownername]);
		}
		
		new Float:X, Float:Y, Float:Z,intid;
		
		
		cache_get_row(i, 5, id_string);
		X = floatstr(id_string);
		cache_get_row(i, 6, id_string);
		Y = floatstr(id_string);
		cache_get_row(i, 7, id_string);
		Z = floatstr(id_string);
		cache_get_row(i, 8, id_string);
		intid = strval(id_string);
		
		cache_get_row(i, 9, id_string);
		WareHouses[slot][EWareHouse_Closed] = strval(id_string);
		
		cache_get_row(i, 10, id_string);
		WareHouses[slot][EWareHouse_Till] = strval(id_string);
		
		
		cache_get_row(i, 11, id_string);
		WareHouses[slot][EWareHouse_Mats][0] = strval(id_string);
		
		cache_get_row(i, 12, id_string);
		WareHouses[slot][EWareHouse_Mats][1] = strval(id_string);
		
		cache_get_row(i, 13, id_string);
		WareHouses[slot][EWareHouse_Mats][2] = strval(id_string);
		
		cache_get_row(i, 14, id_string);
		WareHouses[slot][EWareHouse_Drugs][0] = strval(id_string);		
		
		cache_get_row(i, 15, id_string);
		WareHouses[slot][EWareHouse_Drugs][1] = strval(id_string);		
		
		cache_get_row(i, 16, id_string);
		WareHouses[slot][EWareHouse_Drugs][2] = strval(id_string);		
		
		cache_get_row(i, 17, id_string);
		WareHouses[slot][EWareHouse_GunProfit] = strval(id_string);		
		
		cache_get_row(i, 18, id_string);
		WareHouses[slot][EWareHouse_DrugProfit] = strval(id_string);		
		
		WareHouses[slot][EWareHouse_X] = X;
		WareHouses[slot][EWareHouse_Y] = Y;
		WareHouses[slot][EWareHouse_Z] = Z;
		WareHouses[slot][EWareHouse_Interior] = intid;
		new biztext[128];
		format(biztext, sizeof(biztext), "{F5FAFA}%s",WareHouses[slot][EWareHouse_Name]);
		WareHouses[slot][EWareHouse_PickupID] = CreateDynamicPickup(1239, 16, X, Y, Z);
		WareHouses[slot][EWareHouse_3DText] = CreateDynamic3DTextLabel(biztext, getPointColour(i), X, Y, Z+1.0, 10.0);						
	}
}
forward OnCreateWareHouse();
public OnCreateWareHouse() {
	new id = mysql_insert_id();
	format(query, sizeof(query), "SELECT `w`.`id`,`w`.`name`,`w`.`ownertype`,`w`.`owner`,`c`.`username`,`w`.`x`,`w`.`y`,`w`.`z`,`w`.`interiortype`,`w`.`closed`,`w`.`till`,`w`.`matsa`,`w`.`matsb`,`w`.`matsc`,`w`.`pot`,`w`.`coke`,`w`.`meth`,`w`.`gunprofit`,`w`.`drugprofit`  FROM `warehouses` AS `w` LEFT JOIN `characters` AS `c` ON `owner` = `c`.`id` WHERE `w`.`id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadWareHouse", "");
	format(query, sizeof(query), "[AdmNotice]: Warehouse ID: %d",id);
	ABroadcast(X11_RED,query,EAdminFlags_BusinessAdmin);
}
createWareHouse(Float:X,Float:Y,Float:Z,interior,name[]) {
	new name_esc[(32*2)+1];
	mysql_real_escape_string(name, name_esc);
	format(query, sizeof(query), "INSERT INTO `warehouses` SET `name` = \"%s\", `x` = %f, `y` = %f, `z` = %f,`interiortype` = %d",name_esc,X,Y,Z,interior);
	mysql_function_query(g_mysql_handle, query, true, "OnCreateWareHouse", "");
}
setWareHouseName(sbiz, name[]) {
	new name_esc[(32*2)+1];
	mysql_real_escape_string(name, name_esc);
	format(WareHouses[sbiz][EWareHouse_Name], 32, "%s", name);
	format(query, sizeof(query), "UPDATE `warehouses` SET `name` = \"%s\" WHERE `id` = %d",name_esc,WareHouses[sbiz][EWareHouse_SQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	new biztext[128];
	format(biztext, sizeof(biztext), "{FF0000}%s",WareHouses[sbiz][EWareHouse_Name]);
	UpdateDynamic3DTextLabelText(WareHouses[sbiz][EWareHouse_3DText], COLOR_DARKGREEN, biztext);
	
}
getStandingWareHouse(playerid) {
	for(new i=0;i<sizeof(WareHouses);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 2.5, WareHouses[i][EWareHouse_X], WareHouses[i][EWareHouse_Y], WareHouses[i][EWareHouse_Z])) {
			if(GetPlayerVirtualWorld(playerid) == 0) {
				return i;
			}
		}
	}
	return -1;
}
YCMD:warehousename(playerid, params[], help) {
	new sbiz = getStandingWareHouse(playerid);
	new name[32];
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, sbiz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	if(!sscanf(params, "s[32]",name)) {
		setWareHouseName(sbiz, name);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Name updated!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehousename [name]");
	}
	return 1;
}
YCMD:warehouseinfo(playerid, params[], help) {
	new sbiz = getStandingWareHouse(playerid);
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Displays warehouse info");
		return 1;
	}
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, sbiz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	new msg[128];
	if(WareHouses[sbiz][EWareHouse_OwnerType] == 1) {
		format(msg, sizeof(msg), "* Owner: %s",WareHouses[sbiz][EWareHouse_Ownername]);
	} else {
		new fid = FindFamilyBySQLID(WareHouses[sbiz][EWareHouse_OwnerSQLID]);
		format(msg, sizeof(msg), "* Owner: %s (ID: %d)",GetFamilyName(fid),fid);
	}
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Till: $%s",getNumberString(WareHouses[sbiz][EWareHouse_Till]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Mats: A: %s B: %s C: %s",getNumberString(WareHouses[sbiz][EWareHouse_Mats][0]),getNumberString(WareHouses[sbiz][EWareHouse_Mats][1]),getNumberString(WareHouses[sbiz][EWareHouse_Mats][2]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Drugs: Pot: %s Coke: %s Meth: %s",getNumberString(WareHouses[sbiz][EWareHouse_Drugs][0]),getNumberString(WareHouses[sbiz][EWareHouse_Drugs][1]),getNumberString(WareHouses[sbiz][EWareHouse_Drugs][2]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Interior: %d",WareHouses[sbiz][EWareHouse_Interior]);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* GunProfit: %s DrugsProfit: %s",getNumberString(WareHouses[sbiz][EWareHouse_GunProfit]) ,getNumberString(WareHouses[sbiz][EWareHouse_DrugProfit]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	return 1;
}
setWareHouseOwner(biz, type, id) {
	new sqlid;
	if(type == 1) {
		sqlid = GetPVarInt(id, "CharID");
	} else {
		type = 2;
		sqlid = SQLIDFromFamily(id);
	}
	if(sqlid == -1) return 0;
	format(query, sizeof(query), "UPDATE `warehouses` SET `owner` = %d, `ownertype` = %d WHERE `id` = %d",sqlid,type, WareHouses[biz][EWareHouse_SQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	reloadWareHouse(biz);
	return 1;
}
reloadWareHouse(biz) {
	new sqlid = WareHouses[biz][EWareHouse_SQLID];
	unloadWareHouse(biz);
	format(query, sizeof(query), "SELECT `w`.`id`,`w`.`name`,`w`.`ownertype`,`w`.`owner`,`c`.`username`,`w`.`x`,`w`.`y`,`w`.`z`,`w`.`interiortype`,`w`.`closed`,`w`.`till`,`w`.`matsa`,`w`.`matsb`,`w`.`matsc`,`w`.`pot`,`w`.`coke`,`w`.`meth`,`w`.`gunprofit`,`w`.`drugprofit` FROM `warehouses` AS `w` LEFT JOIN `characters` AS `c` ON `owner` = `c`.`id` WHERE `w`.`id` = %d",sqlid);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadWareHouse", "");
}
unloadWareHouse(biz) {
	DestroyDynamicPickup(WareHouses[biz][EWareHouse_PickupID]);
	DestroyDynamic3DTextLabel(WareHouses[biz][EWareHouse_3DText]);	
	WareHouses[biz][EWareHouse_SQLID] = 0;
}
YCMD:warehouseprofit(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Set the warehouse owner");
		return 1;
	}
	new sbiz = getStandingWareHouse(playerid);
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, sbiz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	new type, amount;
	if(!sscanf(params, "dd", type, amount)) {
		if(amount < 0 || amount > 999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		setWareHouseProfit(sbiz, type, amount);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Biz profit updated");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehouseprofit [type(1 = guns)] [amount]");
	}
	return 1;
}
YCMD:warehouseowner(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Set the warehouse owner");
		return 1;
	}
	new sbiz = getStandingWareHouse(playerid);
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	new type, value[32];
	if(!sscanf(params, "ds[32]", type, value)) {
		if(type == 1) {
			new user = ReturnUser(value, playerid);
			if(!IsPlayerConnectEx(user))
			{
				SendClientMessage(playerid, X11_GREY, "That player is offline");
				return 1;
			}
			setWareHouseOwner(sbiz, type, user);
		} else {
			new familyid = strval(value);
			if(!IsValidFamily(familyid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Family");
				return 1;
			}
			setWareHouseOwner(sbiz, type, familyid);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehouseowner [type(1 = person, 2 = family)] [famid/playerid]");
	}
	return 1;
}
YCMD:warehousedeposit(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Deposit to a warehouse");
		return 1;
	}
	new sbiz = getStandingWareHouse(playerid);
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, sbiz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	new price;
	if(!sscanf(params, "d", price)) {
		if(GetMoneyEx(playerid) < price) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
			return 1;
		}
		if(price < 0 || price > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		WareHouses[sbiz][EWareHouse_Till] += price;
		GiveMoneyEx(playerid, -price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehousedeposit [amount]");
	}
	return 1;
}
YCMD:warehousewithdraw(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Withdraw from a warehouse");
		return 1;
	}
	new sbiz = getStandingWareHouse(playerid);
	if(sbiz == -1) {
		sbiz = getWareHouseInside(playerid);
		if(sbiz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, sbiz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	new price;
	if(!sscanf(params, "d", price)) {
		if(WareHouses[sbiz][EWareHouse_Till] < price) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
			return 1;
		}
		if(price < 0 || price > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		WareHouses[sbiz][EWareHouse_Till] -= price;
		GiveMoneyEx(playerid, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehousewithdraw [amount]");
	}
	return 1;
}
YCMD:warehousestore(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Store mats/drugs into a business");
		return 1;
	}
	new type[32],amount;
	new biz = getStandingWareHouse(playerid);
	if(biz == -1) {
		biz = getWareHouseInside(playerid);
		if(biz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, biz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	if(!sscanf(params, "s[32]d", type, amount)) {
		if(amount < 0 || amount > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(!strcmp(type, "matsa", true)) {
			new matsa = GetPVarInt(playerid, "MatsA");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "MatsA", matsa);
			WareHouses[biz][EWareHouse_Mats][0] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Stored");
		} else if(!strcmp(type, "matsb", true)) {
			new matsa = GetPVarInt(playerid, "MatsB");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "MatsB", matsa);
			WareHouses[biz][EWareHouse_Mats][1] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Stored");
		} else if(!strcmp(type, "matsc", true)) {
			new matsa = GetPVarInt(playerid, "MatsC");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "MatsC", matsa);
			WareHouses[biz][EWareHouse_Mats][2] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Stored");
		} else if(!strcmp(type, "pot", true)) {
			new matsa = GetPVarInt(playerid, "Pot");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "Pot", matsa);
			WareHouses[biz][EWareHouse_Drugs][0] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Pot Stored");
		} else if(!strcmp(type, "coke", true)) {
			new matsa = GetPVarInt(playerid, "Coke");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "Coke", matsa);
			WareHouses[biz][EWareHouse_Drugs][1] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Coke Stored");
		} else if(!strcmp(type, "meth", true)) {
			new matsa = GetPVarInt(playerid, "Meth");
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			matsa -= amount;
			SetPVarInt(playerid, "Meth", matsa);
			WareHouses[biz][EWareHouse_Drugs][2] += amount;
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Meth Stored");
		} else {
			sendBizStats(playerid, biz);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehousestore [type] [amount]");
		SendClientMessage(playerid, X11_WHITE, "Types: mats(a-c), pot, coke, meth");
	}
	return 1;
}
sendBizStats(playerid, biz) {
	new msg[128];
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Till: $%s",getNumberString(WareHouses[biz][EWareHouse_Till]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Mats: A: %s B: %s C: %s",getNumberString(WareHouses[biz][EWareHouse_Mats][0]),getNumberString(WareHouses[biz][EWareHouse_Mats][1]),getNumberString(WareHouses[biz][EWareHouse_Mats][2]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Drugs: Pot: %s Coke: %s Meth: %s",getNumberString(WareHouses[biz][EWareHouse_Drugs][0]),getNumberString(WareHouses[biz][EWareHouse_Drugs][1]),getNumberString(WareHouses[biz][EWareHouse_Drugs][2]));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
}
YCMD:warehouseopen(playerid, params[], help) {
	new biz = getStandingWareHouse(playerid);
	if(biz == -1) {
		biz = getWareHouseInside(playerid);
		if(biz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, biz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	if(WareHouses[biz][EWareHouse_Closed] == 0) { //open
		WareHouses[biz][EWareHouse_Closed] = 1;
		GameTextForPlayer(playerid, "~w~Business ~r~Closed", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
	} else {
		WareHouses[biz][EWareHouse_Closed] = 0;
		GameTextForPlayer(playerid, "~w~Business ~g~Opened", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
	}
	format(query, sizeof(query), "UPDATE `warehouses` SET `closed` = %d WHERE `id` = %d",WareHouses[biz][EWareHouse_Closed]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 1;
}
playerControlsWareHouse(playerid, biz) {
	if(WareHouses[biz][EWareHouse_OwnerType] == 1) {
		if(WareHouses[biz][EWareHouse_OwnerSQLID] == GetPVarInt(playerid, "CharID")) {		
			return 1;
		}
	} else {
		new sqlid = WareHouses[biz][EWareHouse_OwnerSQLID];
		new famid = FindFamilyBySQLID(sqlid);
		if(famid == -1) return 0;
		if(GetPVarInt(playerid, "Family") == sqlid) {
			new rank = GetPVarInt(playerid, "Rank")-1;
			new EFamilyPermissions:rankperms = EFamilyPermissions:Families[famid][EFamilyRankPerms][rank];
			if(rankperms & EFamilyPerms_CanControlIBiz) {
				return 1;
			}
		}
	}
	return 0;
}
YCMD:warehousetake(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Store mats/drugs into a business");
		return 1;
	}
	new type[32],amount;
	new biz = getStandingWareHouse(playerid);
	if(biz == -1) {
		biz = getWareHouseInside(playerid);
		if(biz == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
			return 1;
		}
	}
	if(!playerControlsWareHouse(playerid, biz) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This is not your warehouse!");
		return 1;
	}
	if(!sscanf(params, "s[32]d", type, amount)) {
		if(amount < 0 || amount > 999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(!strcmp(type, "matsa", true)) {
			new matsa = WareHouses[biz][EWareHouse_Mats][0];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "MatsA");
			total += amount;
			WareHouses[biz][EWareHouse_Mats][0] -= amount;
			SetPVarInt(playerid, "MatsA", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Removed");
		} else if(!strcmp(type, "matsb", true)) {
			new matsa = WareHouses[biz][EWareHouse_Mats][1];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "MatsB");
			total += amount;
			WareHouses[biz][EWareHouse_Mats][1] -= amount;
			SetPVarInt(playerid, "MatsB", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Removed");
		} else if(!strcmp(type, "matsc", true)) {
			new matsa = WareHouses[biz][EWareHouse_Mats][2];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "MatsC");
			total += amount;
			WareHouses[biz][EWareHouse_Mats][2] -= amount;
			SetPVarInt(playerid, "MatsC", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Materials Removed");
		} else if(!strcmp(type, "pot", true)) {
			new matsa = WareHouses[biz][EWareHouse_Drugs][0];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "Pot");
			total += amount;
			WareHouses[biz][EWareHouse_Drugs][0] -= amount;
			SetPVarInt(playerid, "Pot", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Drugs Removed");
		} else if(!strcmp(type, "coke", true)) {
			new matsa = WareHouses[biz][EWareHouse_Drugs][1];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "Coke");
			total += amount;
			WareHouses[biz][EWareHouse_Drugs][1] -= amount;
			SetPVarInt(playerid, "Coke", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Drugs Removed");
		} else if(!strcmp(type, "meth", true)) {
			new matsa = WareHouses[biz][EWareHouse_Drugs][2];
			if(amount > matsa) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough");
				return 1;
			}
			new total = GetPVarInt(playerid, "Meth");
			total += amount;
			WareHouses[biz][EWareHouse_Drugs][2] -= amount;
			SetPVarInt(playerid, "Meth", total);
			saveWareHouseItems(biz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Drugs Removed");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /warehousestore [type] [amount]");
		SendClientMessage(playerid, X11_WHITE, "Types: mats(a-c), pot, coke, meth");
	}
	return 1;
}
YCMD:makewarehouse(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates an warehouse");
		return 1;
	}
	new name[32];
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	if(!sscanf(params,"s[32]", name)) {
		createWareHouse(X,Y,Z,0,name);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makewarehouse [name]");
	}
	return 1;
}
YCMD:deletewarehouse(playerid, params[], help) {
	new biz = getStandingWareHouse(playerid);
	if(biz == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing at a business");
		return 1;
	}
	deleteWareHouse(biz);
	return 1;
}

deleteWareHouse(biz) {
	format(query, sizeof(query), "DELETE FROM `warehouses` WHERE `id` = %d",WareHouses[biz][EWareHouse_SQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	unloadWareHouse(biz);
}
findFreeWareHouse() {
	for(new i=0;i<MAX_WAREHOUSES;i++) {
		if(WareHouses[i][EWareHouse_SQLID] == 0) {
			return i;
		}
	}
	return -1;
}
warehouseTryEnter(playerid) {
	for(new i=0;i<MAX_WAREHOUSES;i++) {
		if(WareHouses[i][EWareHouse_SQLID] != 0) {
			new interior = WareHouses[i][EWareHouse_Interior];
			if(IsPlayerInRangeOfPoint(playerid,1.5,WareHouses[i][EWareHouse_X], WareHouses[i][EWareHouse_Y], WareHouses[i][EWareHouse_Z])) {
				if(WareHouses[i][EWareHouse_Closed] == 0) {
					HintMessage(playerid, COLOR_DARKGREEN, "(( Do /buy to buy things ))");
					SetPlayerPos(playerid, WareHouseInteriors[interior][EIBX],WareHouseInteriors[interior][EIBY],WareHouseInteriors[interior][EIBZ]);
					SetPlayerInterior(playerid, WareHouseInteriors[interior][EIBInterior]);
					SetPlayerVirtualWorld(playerid, WareHouses[i][EWareHouse_SQLID]+25000);
					SetPlayerFacingAngle(playerid, WareHouseInteriors[interior][EIBAngle]);
				}
			} else if(IsPlayerInRangeOfPoint(playerid,1.5,WareHouseInteriors[interior][EIBX],WareHouseInteriors[interior][EIBY],WareHouseInteriors[interior][EIBZ])) {
				if(GetPlayerVirtualWorld(playerid) == WareHouses[i][EWareHouse_SQLID]+25000) {
					SetPlayerPos(playerid, WareHouses[i][EWareHouse_X], WareHouses[i][EWareHouse_Y], WareHouses[i][EWareHouse_Z]);
					SetPlayerInterior(playerid, 0);
					SetPlayerVirtualWorld(playerid, 0);
				}
			}
		}
	}
}
getWareHouseInside(playerid, Float:range = 150.0) {
	for(new i=0;i<MAX_WAREHOUSES;i++) {
		if(WareHouses[i][EWareHouse_SQLID] != 0) {
			new interior = WareHouses[i][EWareHouse_Interior];
			if(IsPlayerInRangeOfPoint(playerid,range,WareHouseInteriors[interior][EIBX],WareHouseInteriors[interior][EIBY],WareHouseInteriors[interior][EIBZ])) {
				if(GetPlayerVirtualWorld(playerid) == WareHouses[i][EWareHouse_SQLID]+25000) {
					return i;
				}
			}
		}
	}
	return -1;
}

showWareHouseMenu(playerid, ibiz) {
	dialogstr[0] = 0;
	new dispname[64];
	for(new i=0;i<sizeof(WareHouseBuyableItems);i++) {
		if(WareHouseBuyableItems[i][EIllegalItem_Type] == EIllegalType_Gun) {
			GetWeaponNameEx(WareHouseBuyableItems[i][EIllegalItem_GunID], dispname,sizeof(dispname));
			format(tempstr,sizeof(tempstr),"%s - $%s\n",dispname,getNumberString(WareHouseBuyableItems[i][EIllegalItem_Price]+WareHouses[ibiz][EWareHouse_GunProfit]));
		} else if(WareHouseBuyableItems[i][EIllegalItem_Type] == EIllegalType_Pot) {
			format(tempstr,sizeof(tempstr),"Pot %d - $%s\n",WareHouseBuyableItems[i][EIllegalItem_Amount],getNumberString(WareHouseBuyableItems[i][EIllegalItem_Price]+WareHouses[ibiz][EWareHouse_DrugProfit]));
		} else if(WareHouseBuyableItems[i][EIllegalItem_Type] == EIllegalType_Coke) {
			format(tempstr,sizeof(tempstr),"Coke %d - $%s\n",WareHouseBuyableItems[i][EIllegalItem_Amount],getNumberString(WareHouseBuyableItems[i][EIllegalItem_Price]+WareHouses[ibiz][EWareHouse_DrugProfit]));
		} else if(WareHouseBuyableItems[i][EIllegalItem_Type] == EIllegalType_Meth) {
			format(tempstr,sizeof(tempstr),"Meth %d - $%s\n",WareHouseBuyableItems[i][EIllegalItem_Amount],getNumberString(WareHouseBuyableItems[i][EIllegalItem_Price]+WareHouses[ibiz][EWareHouse_DrugProfit]));
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, _:EWareHouses_BuyMenu, DIALOG_STYLE_LIST, "Buy Menu",dialogstr,"Select", "Cancel");
}
WareHouseOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	if(!response) {
		return 0;
	}
	new biz = getWareHouseInside(playerid);
	if(biz == -1) return 0;
	switch(dialogid) {
		case EWareHouses_BuyMenu: {
			new price;
			new msg[128];
			if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Gun) {
				price = WareHouseBuyableItems[listitem][EIllegalItem_Price]+WareHouses[biz][EWareHouse_GunProfit];
			} else {
				price = WareHouseBuyableItems[listitem][EIllegalItem_Price]+WareHouses[biz][EWareHouse_DrugProfit];
			}
			if(GetMoneyEx(playerid) < price) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
				return 1;
			}
			if(!canBizSellItem(biz, listitem)) {
				SendClientMessage(playerid, X11_TOMATO_2, "There is not enough supplies to sell you this!");
				return 1;
			}
			if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Gun) {
				if(GetPVarInt(playerid, "ConnectTime") < WareHouseBuyableItems[listitem][EIllegalItem_TimeRequired]) {
					format(msg, sizeof(msg), "You don't have the required time to purchase the weapon you selected, you require at least %s hours to buy it.",getNumberString(WareHouseBuyableItems[listitem][EIllegalItem_TimeRequired]));
					SendClientMessage(playerid, X11_TOMATO_2, msg);
					return 1;
				}
			}
			GiveMoneyEx(playerid, -price);
			if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Gun) {
				GivePlayerWeaponEx(playerid, WareHouseBuyableItems[listitem][EIllegalItem_GunID],WareHouseBuyableItems[listitem][EIllegalItem_Amount]);
			} else if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Pot) {
				SetPVarInt(playerid, "Pot", GetPVarInt(playerid, "Pot")+WareHouseBuyableItems[listitem][EIllegalItem_Amount]);
			} else if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Coke) {
				SetPVarInt(playerid, "Coke", GetPVarInt(playerid, "Coke")+WareHouseBuyableItems[listitem][EIllegalItem_Amount]);
			} else if(WareHouseBuyableItems[listitem][EIllegalItem_Type] == EIllegalType_Meth) {
				SetPVarInt(playerid, "Meth", GetPVarInt(playerid, "Meth")+WareHouseBuyableItems[listitem][EIllegalItem_Amount]);
			}
			chargeIllegalBiz(biz, listitem);
		}
	}
	return 1;
}

chargeIllegalBiz(biz, index) {
	new price = WareHouseBuyableItems[index][EIllegalItem_Price];
	if(WareHouseBuyableItems[index][EIllegalItem_Type] == EIllegalType_Gun) {
		price += WareHouses[biz][EWareHouse_GunProfit];
		WareHouses[biz][EWareHouse_Mats][0] -= WareHouseBuyableItems[index][EIllegalItem_MatsPriceA];
		WareHouses[biz][EWareHouse_Mats][1] -= WareHouseBuyableItems[index][EIllegalItem_MatsPriceB];
		WareHouses[biz][EWareHouse_Mats][2] -= WareHouseBuyableItems[index][EIllegalItem_MatsPriceC];
	} else if(WareHouseBuyableItems[index][EIllegalItem_Type] == EIllegalType_Pot) {
		price += WareHouses[biz][EWareHouse_DrugProfit];
		WareHouses[biz][EWareHouse_Drugs][0] -= WareHouseBuyableItems[index][EIllegalItem_Amount];
	} else if(WareHouseBuyableItems[index][EIllegalItem_Type] == EIllegalType_Coke) {
		price += WareHouses[biz][EWareHouse_DrugProfit];
		WareHouses[biz][EWareHouse_Drugs][1] -= WareHouseBuyableItems[index][EIllegalItem_Amount];
	} else if(WareHouseBuyableItems[index][EIllegalItem_Type] == EIllegalType_Meth) {
		price += WareHouses[biz][EWareHouse_DrugProfit];
		WareHouses[biz][EWareHouse_Drugs][2] -= WareHouseBuyableItems[index][EIllegalItem_Amount];
	}
	WareHouses[biz][EWareHouse_Till] += price;
	saveWareHouseItems(biz);
}

canBizSellItem(biz, item) {
	if(WareHouseBuyableItems[item][EIllegalItem_Type] == EIllegalType_Gun) {
		if(WareHouses[biz][EWareHouse_Mats][0] < WareHouseBuyableItems[item][EIllegalItem_MatsPriceA]) {
			return 0;
		}
		if(WareHouses[biz][EWareHouse_Mats][1] < WareHouseBuyableItems[item][EIllegalItem_MatsPriceB]) {
			return 0;
		}
		if(WareHouses[biz][EWareHouse_Mats][2] < WareHouseBuyableItems[item][EIllegalItem_MatsPriceC]) {
			return 0;
		}
	} else if(WareHouseBuyableItems[item][EIllegalItem_Type] == EIllegalType_Pot) {
		if(WareHouses[biz][EWareHouse_Drugs][0] < WareHouseBuyableItems[item][EIllegalItem_Amount]) {
			return 0;
		}
	} else if(WareHouseBuyableItems[item][EIllegalItem_Type] == EIllegalType_Coke) {
		if(WareHouses[biz][EWareHouse_Drugs][1] < WareHouseBuyableItems[item][EIllegalItem_Amount]) {
			return 0;
		}
	} else if(WareHouseBuyableItems[item][EIllegalItem_Type] == EIllegalType_Meth) {
		if(WareHouses[biz][EWareHouse_Drugs][2] < WareHouseBuyableItems[item][EIllegalItem_Amount]) {
			return 0;
		}
	}
	return 1;
}
saveWareHouseItems(biz) {
	format(query, sizeof(query), "UPDATE `warehouses` SET `matsa` = %d, `matsb` = %d, `matsc` = %d, `pot` = %d, `coke` = %d, `meth` = %d, `till` = %d WHERE `id` = %d",
	WareHouses[biz][EWareHouse_Mats][0], WareHouses[biz][EWareHouse_Mats][1], WareHouses[biz][EWareHouse_Mats][2],
	WareHouses[biz][EWareHouse_Drugs][0], WareHouses[biz][EWareHouse_Drugs][1],  WareHouses[biz][EWareHouse_Drugs][2], 
	WareHouses[biz][EWareHouse_Till],
	WareHouses[biz][EWareHouse_SQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
}

setWareHouseProfit(biz, type, amount) {
	if(amount < 0) amount = 0;
	if(type == 1) {
		format(query, sizeof(query), "UPDATE `warehouses` SET `gunprofit` = %d WHERE `id` = %d",amount,WareHouses[biz][EWareHouse_SQLID]);
		WareHouses[biz][EWareHouse_GunProfit] = amount;
	} else {
		format(query, sizeof(query), "UPDATE `warehouses` SET `drugprofit` = %d WHERE `id` = %d",amount,WareHouses[biz][EWareHouse_SQLID]);
		WareHouses[biz][EWareHouse_DrugProfit] = amount;
	}
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
}