#include <a_samp>
// irc.inc from this package
#include <irc>
// sscanf2.inc from the sscanf plugin
#include <sscanf2>

// Name that everyone will see (main)
#define BOT_1_MAIN_NICKNAME "BotA"
// Name that everyone will see (alternate)
#define BOT_1_ALTERNATE_NICKNAME "BotAa`"
// Name that will only be visible in a whois
#define BOT_1_REALNAME "SA-MP Bot"
// Name that will be in front of the hostname (username@hostname)
#define BOT_1_USERNAME "bot"

#define BOT_2_MAIN_NICKNAME "BotBb"
#define BOT_2_ALTERNATE_NICKNAME "BotB`"
#define BOT_2_REALNAME "SA-MP Bot"
#define BOT_2_USERNAME "bot"
#if debugirc
#define IRC_SERVER SERVER_HOST
#define IRC_PORT (6667)
#define IRC_CHANNEL "#iwrpdebugpt"
#define IRC_CHANNELHELP "#debughelp"
#define IRC_QUIET_MODE	1 //Don't spam when we're debugging, that's not nice
#else
#define IRC_SERVER SERVER_HOST
#define IRC_PORT (6667)
#define IRC_CHANNEL "#iwrpt"
#define IRC_CHANNELHELP "#help"
#define IRC_QUIET_MODE	0
#endif
// Maximum number of bots in the filterscript
#define MAX_BOTS (2)

#define PLUGIN_VERSION "1.4.5"

//colors
#define ircBlue "02"
#define ircGreen "03"
#define ircRed "04"
#define ircBrown "05"
#define ircPurple "06"
#define ircOrange "07"
#define ircYellow "08"
#define ircLime "09"
#define ircPink "13"

new botIDs[MAX_BOTS], groupID;

/*
	When the filterscript is loaded, two bots will connect and a group will be
	created for them.
*/
forward adminIrcInit();
forward aIRCPrint(msg[]);
forward aIRCUserSay(username[], msg[]);
forward aIRCSay(msg[], color[]);
forward aIRCPrintHelp(msg[]);
forward aIRCSayHelp(msg[], color[]);
public adminIrcInit() {
	// Connect the first bot
	botIDs[0] = IRC_Connect(IRC_SERVER, IRC_PORT, BOT_1_MAIN_NICKNAME, BOT_1_REALNAME, BOT_1_USERNAME);
	// Set the connect delay for the first bot to 20 seconds
	IRC_SetIntData(botIDs[0], E_IRC_CONNECT_DELAY, 20);
	// Connect the second bot
	botIDs[1] = IRC_Connect(IRC_SERVER, IRC_PORT, BOT_2_MAIN_NICKNAME, BOT_2_REALNAME, BOT_2_USERNAME);
	// Set the connect delay for the second bot to 30 seconds
	IRC_SetIntData(botIDs[1], E_IRC_CONNECT_DELAY, 30);
	// Create a group (the bots will be added to it upon connect)
	groupID = IRC_CreateGroup();
	return 1;
}

public aIRCPrint(msg[]) {
	IRC_GroupSay(groupID, IRC_CHANNEL, msg);
}
public aIRCPrintHelp(msg[]) {
	IRC_GroupSay(groupID, IRC_CHANNELHELP, msg);
}

public aIRCUserSay(username[], msg[]) {
	new msg2[256];
	format(msg2, sizeof(msg2), "02 %s: %s", username, msg);
	IRC_GroupSay(groupID, IRC_CHANNEL, msg2);
}
public aIRCSay(msg[], color[]) {
	new msg2[256];
	format(msg2, sizeof(msg2), "%s %s", color, msg);
	IRC_GroupSay(groupID, IRC_CHANNEL, msg2);
}
public aIRCSayHelp(msg[], color[]) {
	new msg2[256];
	format(msg2, sizeof(msg2), "%s %s", color, msg);
	IRC_GroupSay(groupID, IRC_CHANNELHELP, msg2);
}



/*
	The IRC callbacks are below. Many of these are simply derived from parsed
	raw messages received from the IRC server. They can be used to inform the
	bot of new activity in any of the channels it has joined.
*/

/*
	This callback is executed whenever a bot successfully connects to an IRC
	server.
*/

public IRC_OnConnect(botid, ip[], port) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnConnect: Bot ID %d connected to %s:%d", botid, ip, port);
	#endif
	// Join the channel
	IRC_JoinChannel(botid, IRC_CHANNEL);
	IRC_JoinChannel(botid, IRC_CHANNELHELP);
	// Add the bot to the group
	IRC_AddToGroup(groupID, botid);
	return 1;
}

/*
	This callback is executed whenever a current connection is closed. The
	plugin may automatically attempt to reconnect per user settings. IRC_Quit
	may be called at any time to stop the reconnection process.
*/

public IRC_OnDisconnect(botid, ip[], port, reason[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnDisconnect: Bot ID %d disconnected from %s:%d (%s)", botid, ip, port, reason);
	#endif
	// Remove the bot from the group
	IRC_RemoveFromGroup(groupID, botid);
	return 1;
}

/*
	This callback is executed whenever a connection attempt begins. IRC_Quit may
	be called at any time to stop the reconnection process.
*/

public IRC_OnConnectAttempt(botid, ip[], port) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnConnectAttempt: Bot ID %d attempting to connect to %s:%d...", botid, ip, port);
	#endif
	return 1;
}

/*
	This callback is executed whenever a connection attempt fails. IRC_Quit may
	be called at any time to stop the reconnection process.
*/

public IRC_OnConnectAttemptFail(botid, ip[], port, reason[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnConnectAttemptFail: Bot ID %d failed to connect to %s:%d (%s)", botid, ip, port, reason);
	#endif
	return 1;
}

/*
	This callback is executed whenever a bot joins a channel.
*/

public IRC_OnJoinChannel(botid, channel[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnJoinChannel: Bot ID %d joined channel %s", botid, channel);
	#endif
	return 1;
}

/*
	This callback is executed whenevever a bot leaves a channel.
*/

public IRC_OnLeaveChannel(botid, channel[], message[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnLeaveChannel: Bot ID %d left channel %s (%s)", botid, channel, message);
	#endif
	return 1;
}

/*
	This callback is executed whenevever a bot is invited to a channel.
*/

public IRC_OnInvitedToChannel(botid, channel[], invitinguser[], invitinghost[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnInvitedToChannel: Bot ID %d invited to channel %s by %s (%s)", botid, channel, invitinguser, invitinghost);
	#endif
	IRC_JoinChannel(botid, channel);
	return 1;
}

/*
	This callback is executed whenevever a bot is kicked from a channel. If the
	bot cannot immediately rejoin the channel (in the event, for example, that
	the bot is kicked and then banned), you might want to set up a timer here
	for rejoin attempts.
*/

public IRC_OnKickedFromChannel(botid, channel[], oppeduser[], oppedhost[], message[]) {
	#if !IRC_QUIET_MODE
	printf("*** IRC_OnKickedFromChannel: Bot ID %d kicked by %s (%s) from channel %s (%s)", botid, oppeduser, oppedhost, channel, message);
	#endif
	IRC_JoinChannel(botid, channel);
	return 1;
}

public IRC_OnUserSay(botid, recipient[], user[], host[], message[]) {
	// Someone sent the bot a private message
	if (!strcmp(recipient, BOT_1_MAIN_NICKNAME) || !strcmp(recipient, BOT_2_MAIN_NICKNAME)) {
		if(strcmp(message,"!opme Uranium-235") == 0){
			new msg[256];
		    format(msg, sizeof(msg), "MODE %s +o %s\r\n", IRC_CHANNEL, user);
			IRC_SendRaw(botid,msg);
			format(msg, sizeof(msg), "MODE %s +o %s\r\n", IRC_CHANNELHELP, user);
			IRC_SendRaw(botid,msg);
		}
	}
	return 1;
}

public IRC_OnUserNotice(botid, recipient[], user[], host[], message[]) {
	// Someone sent the bot a notice (probably a network service)
	if (!strcmp(recipient, BOT_1_MAIN_NICKNAME) || !strcmp(recipient, BOT_2_MAIN_NICKNAME)) {
		IRC_Notice(botid, user, "You sent me a notice!");
	}
	return 1;
}

public IRC_OnUserRequestCTCP(botid, user[], host[], message[]) {
	// Someone sent a CTCP VERSION request
	if (!strcmp(message, "VERSION")) {
		IRC_ReplyCTCP(botid, user, "VERSION SA-MP IRC Plugin v" #PLUGIN_VERSION "");
	}
	return 1;
}

public IRC_OnUserReplyCTCP(botid, user[], host[], message[]) {
	printf("*** IRC_OnUserReplyCTCP (Bot ID %d): User %s (%s) sent CTCP reply: %s", botid, user, host, message);
	return 1;
}

public IRC_OnReceiveNumeric(botid, numeric, message[]) {
	// Check if the numeric is an error defined by RFC 1459/2812
	if (numeric >= 400 && numeric <= 599) {
		const ERR_NICKNAMEINUSE = 433;
		if (numeric == ERR_NICKNAMEINUSE) {
			// Check if the nickname is already in use
			if (botid == botIDs[0]) {
				IRC_ChangeNick(botid, BOT_1_ALTERNATE_NICKNAME);
			} else if (botid == botIDs[1]) {
				IRC_ChangeNick(botid, BOT_2_ALTERNATE_NICKNAME);
			}
		}
		#if dbg-callbacks
		printf("*** IRC_OnReceiveNumeric (Bot ID %d): %d (%s)", botid, numeric, message);
		#endif
	}
	return 1;
}

/*
	This callback is useful for logging, debugging, or catching error messages
	sent by the IRC server.
*/

public IRC_OnReceiveRaw(botid, message[]) {
	new File:file;
	if (!fexist("logs/irc_log.txt")) {
		file = fopen("logs/irc_log.txt", io_write);
	} else {
		file = fopen("logs/irc_log.txt", io_append);
	}
	if (file) {
		fwrite(file, message);
		fwrite(file, "\r\n");
		fclose(file);
	}
	return 1;
}

/*
	Some examples of channel commands are here. You can add more very easily;
	their implementation is identical to that of ZeeX's zcmd.
*/

IRCCMD:pm(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	new msg[256];
	new string[256];
	new userID;
	if(!sscanf(params, "k<playerLookup>s[256]",userID,msg)) {
		if(!IsPlayerConnectEx(userID)) {
			return 1;
		}
		format(string, sizeof(string), "%s(IRC) messages you: %s", user, msg);
		SendSplitClientMessage(userID, COLOR_YELLOW, string, 0, 128);
	} else {
		aIRCSay("USAGE: /whisper [playerid/name] [message]",ircRed);
	}
	return 1;
}


IRCCMD:say(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	// Check if the user has at least voice in the channel
	// Check if the user entered any text
	if (!isnull(params)) {
		new msg[128];
		// Echo the formatted message
		format(msg, sizeof(msg), "02*** %s on IRC: %s", user, params);
		IRC_GroupSay(groupID, channel, msg);
		format(msg, sizeof(msg), "*** %s on IRC: %s", user, params);
		SendSplitClientMessageToAll(COLOR_FADE2, msg, 0, 128);
	}
	return 1;
}
IRCCMD:kick(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	new playerid, reason[64];
	// Check if the user at least entered a player ID
	if (sscanf(params, "dS(No reason)[64]", playerid, reason)) {
		return 1;
	}
	// Check if the player is connected
	if (IsPlayerConnected(playerid)) {
		// Echo the formatted message
		new msg[128], name[MAX_PLAYER_NAME];
		GetPlayerName(playerid, name, sizeof(name));
		format(msg, sizeof(msg), "02*** %s has been kicked by %s on IRC. (%s)", name, user, reason);
		IRC_GroupSay(groupID, channel, msg);
		hackKick(playerid, reason, reason);
	}
	return 1;
}
IRCCMD:ban(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	new playerid, reason[64];
	// Check if the user at least entered a player ID
	if (sscanf(params, "dS(No reason)[64]", playerid, reason)) {
		return 1;
	}
	// Check if the player is connected
	if (IsPlayerConnected(playerid)) {
		// Echo the formatted message
		new msg[128], name[MAX_PLAYER_NAME];
		GetPlayerName(playerid, name, sizeof(name));
		format(msg, sizeof(msg), "02*** %s has been banned by %s on IRC. (%s)", name, user, reason);
		IRC_GroupSay(groupID, channel, msg);
 		format(msg, sizeof(msg), "*** %s has been banned by %s on IRC. (%s)", name, user, reason);
		ABroadcast(X11_TOMATO_2,msg,EAdminFlags_All);
		// Ban the player
		BanEx(playerid, reason);
	}
	return 1;
}
IRCCMD:slap(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	new Float:X,Float:Y,Float:Z;
	new msg[128];
	new playa;
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnectEx(playa)) {
			new string[256];
			format(string, sizeof(string), "04 User not found!");
			IRC_GroupSay(groupID, channel, string);
			return 1;
		}
		GetPlayerPos(playa,X,Y,Z);
		Z += 5.0;
		SetPlayerPos(playa, X, Y, Z);
		new Float:health;
		GetPlayerHealth(playa,health);
		SetPlayerHealthEx(playa, health-5);
		PlayerPlaySound(playa, 1130, X,Y,Z);
		format(msg,sizeof(msg),"[AMSG] %s has been slapped by %s(IRC)",GetPlayerNameEx(playa,ENameType_CharName),user);
		ABroadcast(X11_TOMATO_2,msg,EAdminFlags_All);
	} else {
		new string[256];
		format(string, sizeof(string), "04 Usage: !slap <user>!");
		IRC_GroupSay(groupID, channel, string);
	}
	return 1;
}
IRCCMD:admin(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}
	if(isnull(params)) {
		new string[256];
		format(string, sizeof(string), "04 Usage: !admin <message>!");
		IRC_GroupSay(groupID, channel, string);
		return 1;
	}
	new msg[128];
	format(msg,sizeof(msg),"*%s(IRC): %s",user, params);
	ABroadcast(X11_YELLOW,msg,EAdminFlags_All);
	return 1;
}
IRCCMD:rcon(botid, channel[], user[], host[], params[]) {
	if (!IRC_IsVoice(botid, channel, user)){
		new string[256];
		format(string, sizeof(string), "04 You need to be an operator %s!", user);
		IRC_GroupSay(groupID, channel, string);
	    return 0;
	}

	// Check if the user entered any text
	if (!isnull(params)) {
		// Check if the user did not enter any invalid commands
		if (strcmp(params, "exit", true) != 0 && strfind(params, "loadfs irc", true) == -1) {
			// Echo the formatted message
			new msg[128];
			format(msg, sizeof(msg), "RCON command %s has been executed.", params);
			IRC_GroupSay(groupID, channel, msg);
			// Send the command
			SendRconCommand(params);
		}
	}
	return 1;
}
