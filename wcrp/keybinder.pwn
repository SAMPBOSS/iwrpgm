/*
	PVars used here:
	KeyBindEnabled(0-MAX) int - keybinder enabled
	KeyBindKeys(0-MAX) int - bitflags of keys
	KeyBindType(0/1) int -  0 = command, 1 = speech
	KeyBindValue(128) string
*/

#define NUM_KEYBINDS 10
#define KEYBIND_COOLDOWN 3

enum {
	EKeyBindDialog_SelectSlot = EKeyBind_Base + 1,
	EKeyBindDialog_EditChoice,
	EKeyBindDialog_EnableDisable,
	EKeyBindDialog_ToggleKey,
	EKeyBindDialog_TypeChoose,
	EKeyBindDialog_TypeInput,
}
enum EKeybindKeys {
	ENiceName[64],
	EKeyID,
	EGametextKeyFoot[32],
	EGametextCar[32],
};
new KeybindKeys[][EKeybindKeys] = {
	{"Action Key", KEY_ACTION, "~k~~PED_ANSWER_PHONE~","~k~~VEHICLE_FIREWEAPON~"},
	{"Croutch Key", KEY_CROUCH, "~k~~PED_DUCK~", "~k~~VEHICLE_HORN~"},
	{"Fire Key", KEY_FIRE,"~k~~PED_FIREWEAPON~", "~k~~VEHICLE_FIREWEAPON~"},
	{"Sprint Key",KEY_SPRINT,"~k~~PED_SPRINT~","~k~~VEHICLE_ACCELERATE~"},
	{"Secondary Attack", KEY_SECONDARY_ATTACK,"~k~~VEHICLE_ENTER_EXIT~","~k~~VEHICLE_FIREWEAPON_ALT~"},
	{"Jump Key", KEY_JUMP,"~k~~PED_JUMPING~","~k~~VEHICLE_BRAKE~"},
	{"Look Right Key(Car Only)",KEY_LOOK_RIGHT,"","~k~~VEHICLE_LOOKRIGHT~"},
	{"Lock Target/Handbrake",KEY_HANDBRAKE,"~k~~PED_LOCK_TARGET~","~k~~VEHICLE_HANDBRAKE~"},
	{"Look Left(Car Only)",KEY_LOOK_LEFT,"","~k~~VEHICLE_LOOKLEFT~"},
	{"Submission Key",KEY_SUBMISSION,"","~k~~TOGGLE_SUBMISSIONS~"},
	{"Look Behind", KEY_LOOK_BEHIND,"~k~~PED_LOOKBEHIND~","~k~~VEHICLE_LOOKBEHIND~"},
	{"Walk Key",KEY_WALK,"~k~~SNEAK_ABOUT~",""},
	{"Analog Up(Car Only)",KEY_ANALOG_UP,"","~k~~VEHICLE_TURRETUP~"},
	{"Analog Down(Car Only)",KEY_ANALOG_DOWN,"","~k~~VEHICLE_TURRETDOWN~"},
	{"Analog Left",KEY_ANALOG_LEFT,"~k~~VEHICLE_LOOKLEFT~","~k~~VEHICLE_TURRETLEFT~"},
	{"Analog Right",KEY_ANALOG_RIGHT,"~k~~VEHICLE_LOOKRIGHT~","~k~~VEHICLE_TURRETRIGHT~"},
	{"Yes Key",KEY_YES,"~k~~CONVERSATION_YES~","~k~~CONVERSATION_YES~"},
	{"No Key",KEY_NO,"~k~~CONVERSATION_NO~","~k~~CONVERSATION_NO~"},
	{"Control Back",KEY_CTRL_BACK,"~k~~GROUP_CONTROL_BWD~","~k~~GROUP_CONTROL_BWD~"}
};
YCMD:keys(playerid, params[],help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "List keybind keys");
		return 1;
	}
	new msg[128];
	for(new i=0;i<sizeof(KeybindKeys);i++) {
		format(msg, sizeof(msg), "Key: %s(%d) On foot: {FF0000}%s{%s} in vehicle {FF0000}%s",KeybindKeys[i][ENiceName],KeybindKeys[i][EKeyID],KeybindKeys[i][EGametextKeyFoot],getColourString(COLOR_LIGHTBLUE),KeybindKeys[i][EGametextCar]);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}
YCMD:keybinds(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enable/Edit your key binds");
		return 1;
	}
	ShowKeybindsModify(playerid);
	return 1;
}
ShowKeybindsModify(playerid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new enabled;
	for(new i=0;i<NUM_KEYBINDS;i++) {
		enabled = isKeybindEnabled(playerid, i);
		format(tempstr, sizeof(tempstr), "{%s}Key Bind Slot %d\n",getColourString(enabled?COLOR_CUSTOMGOLD:COLOR_LIGHTRED),i+1);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EKeyBindDialog_SelectSlot, DIALOG_STYLE_LIST, "{00BFFF}Choose a slot", dialogstr, "Select", "Cancel");
}
keybindOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	new pvarname[64];
	switch(dialogid) {
		case EKeyBindDialog_SelectSlot: {
			if(!response) {
				DeletePVar(playerid, "KeybindSlot");
				return 1;
			}
			showKeybindEdit(playerid, listitem);
		}
		case EKeyBindDialog_EditChoice: {
			if(!response) {
				showModifyKeybindKeys(playerid);
				return 1;
			}
			switch(listitem) {
				case 0: { //enable/disable
					new slot = GetPVarInt(playerid, "KeybindSlot");
					format(pvarname, sizeof(pvarname), "KeybindEnabled%d",slot);	
					if(GetPVarInt(playerid, pvarname) == 1) {
						SetPVarInt(playerid, pvarname, 0);
					} else {
						SetPVarInt(playerid, pvarname, 1);
					}
					ShowKeybindsModify(playerid);
				}
				case 1: { //modify keys
					showModifyKeybindKeys(playerid);
				}
				case 2: { //edit type
					ShowPlayerDialog(playerid, EKeyBindDialog_TypeChoose, DIALOG_STYLE_LIST, "{00BFFF}Keybind Type", "Command\nSpeech", "Set", "Cancel");
				}
				case 3: {
					ShowPlayerDialog(playerid, EKeyBindDialog_TypeInput, DIALOG_STYLE_INPUT, "{00BFFF}Keybind Value", "Input the command or message you want to say, such as \"/b yeah!\" or \"Hey!\"", "Set", "Cancel");
				}
				
			}
		}
		case EKeyBindDialog_EnableDisable: {
			if(!response) {
				ShowKeybindsModify(playerid);
				return 1;
			}
			new slot = GetPVarInt(playerid, "KeybindSlot");
			format(pvarname, sizeof(pvarname), "KeybindEnabled%d",slot);	
			SetPVarInt(playerid, pvarname, response!=0?1:0);
			ShowKeybindsModify(playerid);
		}
		case EKeyBindDialog_ToggleKey: {
			if(!response) {
				ShowKeybindsModify(playerid);
				return 1;
			}
			new slot = GetPVarInt(playerid, "KeybindSlot");
			format(pvarname, sizeof(pvarname),"KeyBindKeys%d", slot);
			new keyflags = GetPVarInt(playerid, pvarname);		
			new key = KeybindKeys[listitem][EKeyID];
			keyflags ^= key;
			SetPVarInt(playerid, pvarname, keyflags);
			showModifyKeybindKeys(playerid);
		}
		case EKeyBindDialog_TypeChoose: {
			if(!response) {
				ShowKeybindsModify(playerid);
				return 1;
			}
			new slot = GetPVarInt(playerid, "KeybindSlot");
			format(pvarname, sizeof(pvarname),"KeyBindType%d", slot);
			SetPVarInt(playerid, pvarname, listitem);
			ShowKeybindsModify(playerid);
		}
		case EKeyBindDialog_TypeInput: {
			if(!response) {
				ShowKeybindsModify(playerid);
				return 1;
			}
			new slot = GetPVarInt(playerid, "KeybindSlot");
			format(pvarname, sizeof(pvarname), "KeyBindValue%d", slot);
			SetPVarString(playerid, pvarname, inputtext);
			ShowKeybindsModify(playerid);
		}
	}
	return 0;
}
showModifyKeybindKeys(playerid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new enabled;
	new pvarname[64];
	new slot = GetPVarInt(playerid, "KeybindSlot");
	format(pvarname, sizeof(pvarname), "KeyBindKeys%d",slot);
	new keys = GetPVarInt(playerid, pvarname);
	for(new i=0;i<sizeof(KeybindKeys);i++) {
		enabled = (keys&KeybindKeys[i][EKeyID]);
		format(tempstr, sizeof(tempstr), "{%s}%s\n",getColourString(enabled?COLOR_CUSTOMGOLD:COLOR_LIGHTRED),KeybindKeys[i][ENiceName]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EKeyBindDialog_ToggleKey, DIALOG_STYLE_LIST, "{00BFFF}Choose a slot", dialogstr, "Select", "Cancel");
}
showKeybindEdit(playerid, index) {
	SetPVarInt(playerid, "KeybindSlot", index);
	ShowPlayerDialog(playerid, EKeyBindDialog_EditChoice, DIALOG_STYLE_LIST, "{00BFFF}What do you want to edit?", "Enable/Disable\nEdit Keys\nEdit Type\nEdit Value", "Select", "Cancel");
}
isKeybindEnabled(playerid, i) {
	new pvarname[64];
	format(pvarname, sizeof(pvarname), "KeybindEnabled%d",i);	
	return GetPVarInt(playerid, pvarname);
}
saveKeybinds(playerid) {
	format(query, sizeof(query), "UPDATE `keybinds` SET ");
	for(new i=0;i<NUM_KEYBINDS;i++) {
		new pvarname[64];
		format(pvarname, sizeof(pvarname), "KeyBindKeys%d",i);
		new keys = GetPVarInt(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "KeyBindType%d",i);
		new type = GetPVarInt(playerid, pvarname);
		new keybindcmd[(128*2)+1];
		format(pvarname, sizeof(pvarname), "KeyBindValue%d",i);
		GetPVarString(playerid, pvarname, keybindcmd, sizeof(keybindcmd));
		mysql_real_escape_string(keybindcmd, keybindcmd);
		format(pvarname, sizeof(pvarname), "KeyBindEnabled%d",i);
		new enabled = GetPVarInt(playerid, pvarname);
		format(tempstr, sizeof(tempstr), "`key%denabled` = %d,`key%dkey` = %d,`key%dcmd` = \"%s\", `key%dtype` = %d,",i,enabled,i,keys,i,keybindcmd,i,type);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " WHERE `owner` = %d",GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
loadKeybinds(playerid) {
	format(query, sizeof(query), "SELECT ");
	for(new i=0;i<NUM_KEYBINDS;i++) {
		format(tempstr, sizeof(tempstr), "`key%denabled`,`key%dkey`,`key%dcmd`,`key%dtype`,",i,i,i,i);
		strcat(query, tempstr, sizeof(query));
	}
	query[strlen(query)-1] = 0;
	format(tempstr, sizeof(tempstr), " FROM `keybinds` WHERE `owner` = %d", GetPVarInt(playerid, "CharID"));
	strcat(query, tempstr, sizeof(query));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadKeybinds", "d",playerid);
}
checkKeybinds(playerid) {
	format(query, sizeof(query), "SELECT 1 FROM `keybinds` WHERE `owner` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "InsertKeybinds", "d", playerid);
}
forward InsertKeybinds(playerid);
public InsertKeybinds(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows < 1 ) {
		format(query, sizeof(query), "INSERT INTO `keybinds` SET `owner` = %d",GetPVarInt(playerid, "CharID"));
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		loadKeybinds(playerid);
	}
}
forward OnLoadKeybinds(playerid);
public OnLoadKeybinds(playerid) {
	new rows, fields;
	cache_get_data(rows, fields);
	new startrow;
	new id_string[128];
	new pvarname[64];
	for(new i=0;i<floatround(NUM_KEYBINDS/4);i++) {
		cache_get_row(0, startrow++,id_string);
		format(pvarname, sizeof(pvarname), "KeybindEnabled%d",i);
		SetPVarInt(playerid, pvarname, strval(id_string));
		cache_get_row(0, startrow++,id_string);
		format(pvarname, sizeof(pvarname), "KeyBindKeys%d",i);
		SetPVarInt(playerid, pvarname, strval(id_string));
		cache_get_row(0, startrow++,id_string);
		format(pvarname, sizeof(pvarname), "KeyBindValue%d",i);
		SetPVarString(playerid, pvarname, id_string);
		cache_get_row(0, startrow++,id_string);
		format(pvarname, sizeof(pvarname), "KeyBindType%d",i);
		SetPVarString(playerid, pvarname, id_string);
	}
}
keybindOnKeyStateChange(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	new pvarname[64];
	for(new i=0;i<NUM_KEYBINDS;i++) {
		format(pvarname, sizeof(pvarname), "KeyBindEnabled%d", i);
		new enabled = GetPVarInt(playerid, pvarname);
		format(pvarname, sizeof(pvarname), "KeyBindKeys%d", i);
		new key = GetPVarInt(playerid, pvarname);
		if(newkeys & key && enabled) {
			doKeybind(playerid, i);
		}
	}
}
doKeybind(playerid, key) {
	new pvarname[64];
	new type, string[128];
	new time = GetPVarInt(playerid, "KeybindCooldown");
	new timenow = gettime();
	if(KEYBIND_COOLDOWN-(timenow-time) > 0) {
		return 1;
	}
	SetPVarInt(playerid, "KeybindCooldown", gettime());
	format(pvarname, sizeof(pvarname), "KeyBindType%d",key);
	type = GetPVarInt(playerid, pvarname);
	format(pvarname, sizeof(pvarname), "KeyBindValue%d",key);
	GetPVarString(playerid, pvarname, string, sizeof(string));
	if(type == 0) {
		Command_ReProcess(playerid, string, false);
	} else {
		OnPlayerText(playerid, string);		
	}
	return 0;
}