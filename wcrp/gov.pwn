/*
pay tickets
health insurance - 3k
change car plates - 1k
namechange 2m

licenses:

car - 1k with test
car - 20k without test
flying 20k
sailing 2k
fishing 500
*/
#define MAX_ALLOWED_SPEED 90

new govpickup;
new dmvpickup;
new insurancepickup;
new airpinsurancepickup;
new impoundpickup;

new LessonCar[MAX_PLAYERS];
new TakingLesson[MAX_PLAYERS];
enum {
	EGovDialog_MainMenu = EGovDialog_Base + 1,
	EGovDialog_NameChange,
	EGovDialog_DMVMenu,
	EGovDialog_InsuranceMenu,
	EGovDialog_PlatesChooseCar,
	EGovDialog_EnterPlate,
	EGovDialog_EnterMarriageID,
	EGovDialog_AcceptMarriage,
	EGovDialog_ImpoundMenu,
};

enum EGovOptions {
	EGovName[32],
	EGovPrice,
	EGovCallback[64],
};


govOnGameModeInit() {
	govpickup = CreateDynamicPickup(1274, 16, 362.632507, 173.657699, 1008.382812); //VIP name change, etc was 1314
	dmvpickup = CreateDynamicPickup(1581, 16, 359.26, 206.66, 1008.38);//ID Card for DMV
	insurancepickup = CreateDynamicPickup(1581, 16, -2033.185180, -117.235755, 1035.171875);
	impoundpickup = CreateDynamicPickup(1581, 16, 20.7150, -10.0656, 1499.2660);
	airpinsurancepickup = CreateDynamicPickup(1581, 16, 1893.0431,-2328.8228,13.5469);
}
govOnPlayerPickupPickup(playerid, pickup) {
	if(pickup == govpickup) {
		showGovMenu(playerid);
		movePlayerBack(playerid, 3.0);
	} else if(pickup == dmvpickup) {
		showDMVMenu(playerid);
		movePlayerBack(playerid, 3.0);
	} else if(pickup == insurancepickup) {
		showInsuranceMenu(playerid, 1);
		movePlayerBack(playerid, 1.5);
	} else if(pickup == impoundpickup) {
		showImpoundMenu(playerid);
		movePlayerBack(playerid, 1.5);
	} else if(pickup == airpinsurancepickup) {
		showInsuranceMenu(playerid, 3);
		movePlayerBack(playerid, 1.5);
	}
}
toggleHealthInsurance(playerid) {
	new uflags = GetPVarInt(playerid, "UserFlags");
	if(uflags  & EUFHasHealthInsurance) {
		uflags &= ~EUFHasHealthInsurance;
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Health Insurance Removed!");
	} else {
		if(GetMoneyEx(playerid) < 3000) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money to buy health insurance");	
		} else {
			GiveMoneyEx(playerid, -3000);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Health Insurance Bought!");
			uflags |= EUFHasHealthInsurance;
		}
	}
	SetPVarInt(playerid, "UserFlags", uflags);
}
hasHealthInsurance(playerid) {
	new uflags = GetPVarInt(playerid, "UserFlags");
	if(uflags & EUFHasHealthInsurance) {
		return 1;
	}
	return 0;
}
showGovMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	strcat(dialogstr,"Option\tCost\n",sizeof(dialogstr));
	format(tempstr, sizeof(tempstr), "Pay Tickets\t$%s\n",getNumberString(GetPVarInt(playerid, "TicketTotal")));
	strcat(dialogstr,tempstr,sizeof(dialogstr));
	//strcat(dialogstr,"Health Insurance - $3000\n",sizeof(dialogstr));
	//strcat(dialogstr,"Change Vehicle Plate - $1000\n",sizeof(dialogstr));
	strcat(dialogstr,"Change Name\t$500,000\n",sizeof(dialogstr));
	new insurance = GetPVarInt(playerid, "UserFlags") & EUFHasHealthInsurance;
	if(!insurance) {
		format(tempstr, sizeof(tempstr), "Health Insurance\t$%s per payday\n", getNumberString(getHealthInsurancePrice(playerid)));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	} else {
		strcat(dialogstr, "Cancel Insurance\n",sizeof(dialogstr));
	}
	strcat(dialogstr,"Change Vehicle Plates\t$25,000\n",sizeof(dialogstr));
	strcat(dialogstr,"Marriage License\t$25,000\n",sizeof(dialogstr));
	strcat(dialogstr,"Cash Cheques\n",sizeof(dialogstr));
	ShowPlayerDialog(playerid, EGovDialog_MainMenu, DIALOG_STYLE_TABLIST_HEADERS, "Government Menu", dialogstr, "Buy", "Cancel");
}
getHealthInsurancePrice(playerid) {
	#pragma unused playerid
	return 200;
}
showInsuranceMenu(playerid, place = 1) {
	dialogstr[0] = 0;
	new insurance = GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
	if(insurance == 0) {
		format(tempstr, sizeof(tempstr), "Buy Insurance - $%s per payday\n",getNumberString(getInsurancePaydayPrice(playerid)));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	} else {
		format(tempstr, sizeof(tempstr), "Cancel Insurance\n");
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	new carid,index;
	while(((carid = findPlayerCar(playerid, index++)) != -1)) {
		if(isVehicleInImpound(carid) == place) {
		//if(GetVehicleVirtualWorld(carid) != 0) {
			format(tempstr, sizeof(tempstr), "Reclaim %s\n",VehiclesName[GetVehicleModel(carid)-400]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EGovDialog_InsuranceMenu, DIALOG_STYLE_LIST, "Insurance Menu", dialogstr, "Ok", "Cancel");
}
showImpoundMenu(playerid) {
	dialogstr[0] = 0;
	new carid, index;
	while(((carid = findPlayerCar(playerid, index++)) != -1)) {
		if(isVehicleInImpound(carid) == 2) {
			format(tempstr, sizeof(tempstr), "%s\t$%s\n",VehiclesName[GetVehicleModel(carid)-400],getNumberString(GetImpoundPrice(carid)));
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EGovDialog_ImpoundMenu, DIALOG_STYLE_LIST, "Impound Menu", dialogstr, "Ok", "Cancel");
}
showMarriageMenu(playerid) {
	ShowPlayerDialog(playerid, EGovDialog_EnterMarriageID, DIALOG_STYLE_INPUT, "Marriage Menu", "Enter the name or ID of the person you wish to marry", "Ok", "Cancel");
}
getInsurancePaydayPrice(playerid) {
	new carid;
	new index;
	new price;
	new aprice;
	while((carid = findPlayerCar(playerid, index++)) != -1) {
		if(IsACar(GetVehicleModel(carid))) {
			aprice = floatround((getModelValue(GetVehicleModel(carid))/10.0)*0.01);
			//if(aprice > 5550) aprice = 2550;
			price += aprice;
		}
	}
	return price+250;
}
getVehicleReclaimPrice(carid) {
	return floatround(getModelValue(GetVehicleModel(carid))*0.13);
}
showDMVMenu(playerid) {
	dialogstr[0] = 0;
	strcat(dialogstr,"License\tCost\n",sizeof(dialogstr));
	strcat(dialogstr,"Driving License\t$1000\n",sizeof(dialogstr));
	strcat(dialogstr,"Flying License\t$20,000\n",sizeof(dialogstr));
	strcat(dialogstr,"Sailing License\t$2000\n",sizeof(dialogstr));
	strcat(dialogstr,"Fishing License\t$2000\n",sizeof(dialogstr));
	strcat(dialogstr,"Driving License without test\t$20,000\n",sizeof(dialogstr));
	ShowPlayerDialog(playerid, EGovDialog_DMVMenu, DIALOG_STYLE_TABLIST_HEADERS, "Government Menu", dialogstr, "Buy", "Cancel");
}
showChangePlateMenu(playerid) {
	dialogstr[0] = 0;
	new carid;
	new index;
	strcat(dialogstr,"Vehicle\tPlate\n",sizeof(dialogstr));
	while((carid = findPlayerCar(playerid, index++)) != -1) {
		if(IsACar(GetVehicleModel(carid))) {
			format(tempstr, sizeof(tempstr), "%d. %s\t%s{FFFFFF}\n",index,VehiclesName[GetVehicleModel(carid)-400],getVehiclePlate(carid));
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EGovDialog_PlatesChooseCar, DIALOG_STYLE_TABLIST_HEADERS, "Government Menu", dialogstr, "Buy", "Cancel");	
}

govOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	switch(dialogid) {
		case EGovDialog_MainMenu: {
			if(!response) {
				return 0;
			}
			switch(listitem) {
				case 0: {
					tryPayTickets(playerid);
				}
				case 1: {
					ShowPlayerDialog(playerid, EGovDialog_NameChange, DIALOG_STYLE_INPUT, "Name Change", "Enter the name you would like to use", "Ok", "Cancel");
				}
				case 2: {
					toggleHealthInsurance(playerid);
				}
				case 3: {
					showChangePlateMenu(playerid);
				}
				case 4: {
					showMarriageMenu(playerid);
				}
				case 5: {
					sendPlayerCheques(playerid); //Show the player his / her cheques
				}
			}
		}
		case EGovDialog_NameChange: {
			if(!response) {
				return 0;
			}
			if(strlen(inputtext) > MAX_PLAYER_NAME || !NameIsRP(inputtext) || IsNameRestricted(inputtext)) {
				ShowPlayerDialog(playerid, EGovDialog_NameChange, DIALOG_STYLE_INPUT, "VIP Name Change","{FF0000}Warning: {FFFFFF}This name is NOT RP! you must enter an RP Name!\nEnter the name you would like to use","Ok", "Cancel");
			} else {
				IsNameTaken(inputtext, "OnGovNameTakenCheck", playerid);				
			}
		}
		case EGovDialog_DMVMenu: {
			if(!response) {
				return 0;
			}
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			switch(listitem) {
				case 0: {
					if(GetMoneyEx(playerid) >= 1000) {
						SendToDrivingTest(playerid);
						GiveMoneyEx(playerid, -1000);
						return 1;
					} else {
						SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
					}
				}
				case 1: {
					if(GetMoneyEx(playerid) >= 20000) {
						SendClientMessage(playerid, COLOR_LIGHTBLUE, "Congratulations, you now own a pilots license");
						lflags |= ELicense_Flying;
						GiveMoneyEx(playerid, -20000);
					}
				}
				case 2: {
					if(GetMoneyEx(playerid) >= 2000) {
						SendClientMessage(playerid, COLOR_LIGHTBLUE, "Congratulations, you now own a sailing license");
						lflags |= ELicense_Sailing;
						GiveMoneyEx(playerid, -2000);
					}
				}
				case 3: {
					if(GetMoneyEx(playerid) >= 2000) {
						SendClientMessage(playerid, COLOR_LIGHTBLUE, "Congratulations, you now own a fishing license");
						lflags |= ELicense_Fishing;
						GiveMoneyEx(playerid, -2000);
					}
				}
				case 4: {
					if(GetMoneyEx(playerid) >= 20000) {
						SendClientMessage(playerid, COLOR_LIGHTBLUE, "Congratulations, you now own a driving license");
						lflags |= ELicense_Drivers;
						GiveMoneyEx(playerid, -20000);
					}
				}
			}
			SetPVarInt(playerid, "LicenseFlags", _:lflags);
		}
		case EGovDialog_InsuranceMenu: {
				if(!response) {
					return 0;
				}
				new price = getInsurancePaydayPrice(playerid);
				if(listitem == 0) { //buy/cancel insurance
					new flags = GetPVarInt(playerid, "UserFlags");
					if(~flags & EUFHasCarInsurance) {
						if(GetMoneyEx(playerid) < price) {
							format(tempstr, sizeof(tempstr), "You must have $%s to buy insurance!",getNumberString(price));
							SendClientMessage(playerid, X11_TOMATO_2, tempstr);
							return 1;
						}
						new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
						if(~lflags & ELicense_Drivers) {
							SendClientMessage(playerid, X11_TOMATO_2, "You must have a drivers license!");
							return 1;
						}
						GiveMoneyEx(playerid, -price);
						flags |= EUFHasCarInsurance;
						SendClientMessage(playerid, COLOR_DARKGREEN, "Congratulations! You now have car insurance! You vehicle will be towed here if it gets destroyed");
					} else {
						flags &= ~EUFHasCarInsurance;
					}
					SetPVarInt(playerid, "UserFlags", flags);
				} else { //reclaiming insurance car
				new carid;
				new x, index;
				while(x != listitem && ((carid = findPlayerCar(playerid, index++)) != -1)) { //listitem-1 because of buy/cancel insurance
					if(isVehicleInImpound(carid) == 1 || isVehicleInImpound(carid) == 3) {
						x++;
					}
				}
				if(x > 0) {
					new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
					if(~lflags & ELicense_Drivers) {
						SendClientMessage(playerid, X11_TOMATO_2, "You must have a drivers license to take your vehicle out of the impound!");
						return 1;
					}
					/*
					price = getVehicleReclaimPrice(carid);
					if(GetMoneyEx(playerid) < price) {
						format(tempstr, sizeof(tempstr), "You must have $%s to get this!",getNumberString(price));
						SendClientMessage(playerid, X11_TOMATO_2, tempstr);
						return 1;
					}
					GiveMoneyEx(playerid, -price);
					*/
					removeVehicleFromImpound(carid);
					SendClientMessage(playerid, COLOR_DARKGREEN, "Your vehicle is now outside of the building.");
				}
			}
		}
		case EGovDialog_ImpoundMenu: {
				if(!response) return 0;
				new carid;
				new x = -1, index;
				while(x != listitem && ((carid = findPlayerCar(playerid, index++)) != -1)) { //listitem-1 because of buy/cancel insurance
					if(isVehicleInImpound(carid) == 2) {
						x++;
					}
				}
				if(x > -1) {
					new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
					if(~lflags & ELicense_Drivers) {
						SendClientMessage(playerid, X11_TOMATO_2, "You must have a drivers license to take your vehicle out of the impound!");
						return 1;
					}
					new price = GetImpoundPrice(carid);
					if(GetMoneyEx(playerid) < price) {
						SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
						return 1;
					}
					GiveMoneyEx(playerid, -price);
					removeVehicleFromImpound(carid);
					SendClientMessage(playerid, COLOR_DARKGREEN, "Your vehicle is now outside of the building.");
				}
		}
		case EGovDialog_PlatesChooseCar: {
			if(!response) {
				return 0;
			}
			new cindex,index;
			new carid;
			while(((carid = findPlayerCar(playerid, index++)) != -1)) {
				if(IsACar(GetVehicleModel(carid))) {
					if(cindex++ == listitem) break;
				}
			}
			SetPVarInt(playerid, "PlateCarIndex", index-1);
			ShowPlayerDialog(playerid, EGovDialog_EnterPlate, DIALOG_STYLE_INPUT, "Plate Change","This changes your vehicle plate\nYou can use colour codes(see /help)","Change", "Cancel");
		}
		case EGovDialog_EnterPlate: {
			if(!response) {
				return 0;
			}
			new car = GetPVarInt(playerid, "PlateCarIndex");
			if(GetMoneyEx(playerid) < 25000) {
				SendClientMessage(playerid, X11_TOMATO_2, "You need $25,000 for this!");
			} else {
				new carid = findPlayerCar(playerid, car);
				if(carid != -1) {
					GiveMoneyEx(playerid, -25000);
					//SetCarPlate(carid, FormatColourString(inputtext)); //FormatColourString Crashes everything
					SetCarPlate(carid, inputtext);
					SendClientMessage(playerid, COLOR_DARKGREEN, "* Vehicle Plates updated!");
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle Index!");
				}
			}
			DeletePVar(playerid, "PlateCarIndex");
		}
		case EGovDialog_EnterMarriageID: {
			new playa = sscanf_playerLookup(inputtext);
			new Float:X, Float:Y, Float:Z;
			if(!response) {
				return 1;
			}
			if(playa == INVALID_PLAYER_ID || playerid == playa) {
				ShowPlayerDialog(playerid, EGovDialog_EnterMarriageID, DIALOG_STYLE_INPUT, "Marriage Menu", "{FF0000}Invalid Player!\n\n{FFFFFF}Enter the name or ID of the person you wish to marry", "Ok", "Cancel");				
			} else {
				GetPlayerPos(playa, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(playerid, 60.0, X, Y, Z)) {
					ShowPlayerDialog(playerid, EGovDialog_EnterMarriageID, DIALOG_STYLE_INPUT, "Marriage Menu", "{FF0000}This player must be near you!\n\n{FFFFFF}Enter the name or ID of the person you wish to marry", "Ok", "Cancel");
					return 0;
				} else {
					new dialogtext[128];
					format(dialogtext, sizeof(dialogtext), "%s wishes to marry you, do you wish to accept?",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
					ShowPlayerDialog(playa, EGovDialog_AcceptMarriage, DIALOG_STYLE_MSGBOX, "Marriage Menu", dialogtext, "Yes", "No");
					SetPVarInt(playa, "MarryRequest", playerid);
				}
			}
		}
		case EGovDialog_AcceptMarriage: {
			new spouse = GetPVarInt(playerid, "MarryRequest");
			if(response == 1) {
				if(GetPVarInt(playerid, "SpouseID") != 0 || GetPVarInt(spouse, "SpouseID") != 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "* Either you, or your partner is currently married. Please divorce first.");
					SendClientMessage(spouse, X11_TOMATO_2, "* Either you, or your partner is currently married. Please divorce first.");
					return 1;
				}
				SendClientMessage(spouse, COLOR_LIGHTBLUE,"* The player has accepted the marriage request");
				marryPlayers(playerid, spouse);
			} else {
				SendClientMessage(spouse, X11_TOMATO_2,"* The player has declined the marriage request");
			}
			DeletePVar(playerid, "MarryRequest");
		}
	}
	return 1;
}
marryPlayers(playerid, spouse) {
	SetPVarInt(playerid, "SpouseID", GetPVarInt(spouse, "CharID"));
	SetPVarInt(spouse, "SpouseID", GetPVarInt(playerid, "CharID"));
	SetPVarString(playerid, "SpouseName", GetPlayerNameEx(spouse, ENameType_CharName));
	SetPVarString(spouse, "SpouseName", GetPlayerNameEx(playerid, ENameType_CharName));
	SendClientMessage(playerid, COLOR_DARKGREEN, "(( do /divorce to divorce your spouse! ))");
	SendClientMessage(spouse, COLOR_DARKGREEN, "(( do /divorce to divorce your spouse! ))");
	new string[128];
	if(GetPVarInt(playerid, "Sex") == 0 && GetPVarInt(spouse, "Sex") == 0) { //gay couple
		format(string, sizeof(string), "* We have a new gay couple! %s and %s have been married!",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPlayerNameEx(spouse, ENameType_RPName_NoMask));
	} else if(GetPVarInt(playerid, "Sex") == 1 && GetPVarInt(spouse, "Sex") == 1) {
		format(string, sizeof(string), "* We have a new lesbian couple! %s and %s have been married!",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPlayerNameEx(spouse, ENameType_RPName_NoMask));
	} else {
		format(string, sizeof(string), "* We have a new couple! %s and %s have been married!",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPlayerNameEx(spouse, ENameType_RPName_NoMask));
	}
	NewsMessage(TEAM_GROVE_COLOR,string);
}
YCMD:divorce(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Divorces a player");
	}
	new spouse = GetPVarInt(playerid, "SpouseID");
	spouse = findCharBySQLID(spouse);
	format(query, sizeof(query), "UPDATE `characters` SET `spouse` = 0 WHERE `id` = %d",GetPVarInt(playerid, "SpouseID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");	
	SetPVarString(playerid, "SpouseName", "No-One");
	DeletePVar(playerid, "SpouseID");
	if(spouse != INVALID_PLAYER_ID) {
		SetPVarString(spouse, "SpouseName", "No-One");	
		DeletePVar(spouse, "SpouseID");
		SendClientMessage(spouse, COLOR_DARKGREEN, "* Your spouse has divorced you.");
	}
	SendClientMessage(spouse, COLOR_DARKGREEN, "* You have been divorced");
	return 1;
}
playerHasCarInsurance(playerid) {
	return GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
}
SendToDrivingTest(playerid) {
	new msg[128];
	TakingLesson[playerid] = 1;
	SendClientMessage(playerid, COLOR_LIGHTBLUE,"Follow the checkpoints to earn your license.");
	SendClientMessage(playerid, X11_WHITE, "(( Hint: To cancel the driving test, use '/CancelDrivingTest' ))");
	format(msg, sizeof(msg), "Remember to not damage the vehicle or to go past the speed limit which is %d km/h", MAX_ALLOWED_SPEED);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	LessonCar[playerid] = CreateVehicle(410, 1414.74, -1648.76, 13.37, 270.0, 61, 61, 900);
	VehicleInfo[LessonCar[playerid]][EVFuel] = 100;
	SetPlayerPos(playerid, 1414.74, -1648.76, 13.37);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, 0);
	
	new time;
	gettime(time);
	if(6 < time < 20)
	{
		SetVehicleParamsEx(LessonCar[playerid], 1, 0, 0, 1, 0, 0, 0);
	}
	else
	{
		SetVehicleParamsEx(LessonCar[playerid], 1, 1, 0, 1, 0, 0, 0);
	}
	SetVehicleNumberPlate(LessonCar[playerid], "Student");
	PutPlayerInVehicleEx(playerid, LessonCar[playerid], 0);
	SetCameraBehindPlayer(playerid);
	SetPlayerRaceCheckpoint(playerid, 0, 1431.52, -1626.39, 13.03, 1457.62, -1444.83, 13.03, 7.0);
}
forward OnGovNameTakenCheck(playerid, name[]);
public OnGovNameTakenCheck(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	if(strlen(name) > MAX_PLAYER_NAME) {
		ShowPlayerDialog(playerid, EGovDialog_NameChange, DIALOG_STYLE_INPUT, "Name Change","{FF0000}Warning: {FFFFFF}The name you entered is too long\nEnter the name you would like to use","Ok", "Cancel");
		return 1;
	}
	if(rows > 0) {
		ShowPlayerDialog(playerid, EGovDialog_NameChange, DIALOG_STYLE_INPUT, "Name Change","{FF0000}Warning: {FFFFFF}This name is taken! You must enter another name!\nEnter the name you would like to use","Ok", "Cancel");
	} else {
		if(GetMoneyEx(playerid) < 500000) { 
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money!");
			return 1;
		}
		GiveMoneyEx(playerid, -500000);
		format(msg, sizeof(msg), "AdmWarn: %s has changed names to %s",GetPlayerNameEx(playerid, ENameType_CharName),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_SetName);
		changeName(playerid, name);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations on your new name!");
	}
	return 1;
}
govOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(LessonCar[playerid] > 0) {
		DestroyVehicle(LessonCar[playerid]);
		LessonCar[playerid] = 0;
	}
}

govOnPlayerEnterCheckpoint(playerid) {
	if(TakingLesson[playerid] > 0)
	{
		if(TakingLesson[playerid] == 1){ TakingLesson[playerid] = 2; SetPlayerRaceCheckpoint(playerid, 0, 1457.62, -1444.83, 13.03, 1597.12, -1442.99, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 2){ TakingLesson[playerid] = 3; SetPlayerRaceCheckpoint(playerid, 0, 1597.12, -1442.99, 13.03, 1611.03, -1314.21, 16.93, 6.0);}
	    else if(TakingLesson[playerid] == 3){ TakingLesson[playerid] = 4; SetPlayerRaceCheckpoint(playerid, 0, 1611.03, -1314.21, 16.93, 1783.49, -1283.92, 13.12, 6.0);}
	    else if(TakingLesson[playerid] == 4){ TakingLesson[playerid] = 5; SetPlayerRaceCheckpoint(playerid, 0, 1783.49, -1283.92, 13.12, 1868.11, -1263.60, 13.04, 6.0);}
	    else if(TakingLesson[playerid] == 5){ TakingLesson[playerid] = 6; SetPlayerRaceCheckpoint(playerid, 0, 1868.11, -1263.60, 13.04, 2053.38, -1263.13, 23.47, 6.0);}
	    else if(TakingLesson[playerid] == 6){ TakingLesson[playerid] = 7; SetPlayerRaceCheckpoint(playerid, 0, 2053.38, -1263.13, 23.47, 2074.01, -1229.18, 23.45, 6.0);}
	    else if(TakingLesson[playerid] == 7){ TakingLesson[playerid] = 8; SetPlayerRaceCheckpoint(playerid, 0, 2074.01, -1229.18, 23.45, 2159.85, -1222.64, 23.47, 6.0);}
	    else if(TakingLesson[playerid] == 8){ TakingLesson[playerid] = 9; SetPlayerRaceCheckpoint(playerid, 0, 2159.85, -1222.64, 23.47, 2178.15, -1134.55, 24.61, 6.0);}
	    else if(TakingLesson[playerid] == 9){ TakingLesson[playerid] = 10; SetPlayerRaceCheckpoint(playerid, 0, 2178.15, -1134.55, 24.61, 2098.58, -1058.25, 27.31, 6.0);}
	    else if(TakingLesson[playerid] == 10){ TakingLesson[playerid] = 11; SetPlayerRaceCheckpoint(playerid, 0, 2098.58, -1058.25, 27.31, 1840.27, -995.69, 36.20, 6.0);}
	    else if(TakingLesson[playerid] == 11){ TakingLesson[playerid] = 12; SetPlayerRaceCheckpoint(playerid, 0, 1840.27, -995.69, 36.20, 1761.73, -959.32, 45.49, 6.0);}
	    else if(TakingLesson[playerid] == 12){ TakingLesson[playerid] = 13; SetPlayerRaceCheckpoint(playerid, 0, 1761.73, -959.32, 45.49, 1623.73, -1025.03, 51.46, 6.0);}
	    else if(TakingLesson[playerid] == 13){ TakingLesson[playerid] = 14; SetPlayerRaceCheckpoint(playerid, 0, 1623.73, -1025.03, 51.46, 1609.78, -1161.38, 55.85, 6.0);}
	    else if(TakingLesson[playerid] == 14){ TakingLesson[playerid] = 15; SetPlayerRaceCheckpoint(playerid, 0, 1609.78, -1161.38, 55.85, 1589.80, -1496.13, 28.24, 6.0);}
	    else if(TakingLesson[playerid] == 15){ TakingLesson[playerid] = 16; SetPlayerRaceCheckpoint(playerid, 0, 1589.80, -1496.13, 28.24, 1603.38, -1783.97, 26.87, 6.0);}
	    else if(TakingLesson[playerid] == 16){ TakingLesson[playerid] = 17; SetPlayerRaceCheckpoint(playerid, 0, 1603.38, -1783.97, 26.87, 1634.79, -2046.63, 21.15, 6.0);}
	    else if(TakingLesson[playerid] == 17){ TakingLesson[playerid] = 18; SetPlayerRaceCheckpoint(playerid, 0, 1634.79, -2046.63, 21.15, 1438.87, -2117.03, 13.04, 6.0);}
	    else if(TakingLesson[playerid] == 18){ TakingLesson[playerid] = 19; SetPlayerRaceCheckpoint(playerid, 0, 1438.87, -2117.03, 13.04, 1315.88, -2257.14, 13.04, 6.0);}
	    else if(TakingLesson[playerid] == 19){ TakingLesson[playerid] = 20; SetPlayerRaceCheckpoint(playerid, 0, 1315.88, -2257.14, 13.04, 1335.90, -2198.58, 21.35, 6.0);}
	    else if(TakingLesson[playerid] == 20){ TakingLesson[playerid] = 21; SetPlayerRaceCheckpoint(playerid, 0, 1335.90, -2198.58, 21.35, 1598.11, -2196.50, 13.02, 6.0);}
	    else if(TakingLesson[playerid] == 21){ TakingLesson[playerid] = 22; SetPlayerRaceCheckpoint(playerid, 0, 1598.11, -2196.50, 13.02, 1750.22, -2168.93, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 22){ TakingLesson[playerid] = 23; SetPlayerRaceCheckpoint(playerid, 0, 1750.22, -2168.93, 13.03, 1532.74, -1987.72, 23.63, 6.0);}
	    else if(TakingLesson[playerid] == 23){ TakingLesson[playerid] = 24; SetPlayerRaceCheckpoint(playerid, 0, 1532.74, -1987.72, 23.63, 1532.06, -1885.63, 13.16, 6.0);}
	    else if(TakingLesson[playerid] == 24){ TakingLesson[playerid] = 25; SetPlayerRaceCheckpoint(playerid, 0, 1532.06, -1885.63, 13.16, 1469.51, -1869.55, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 25){ TakingLesson[playerid] = 26; SetPlayerRaceCheckpoint(playerid, 0, 1469.51, -1869.55, 13.03, 1391.34, -1776.10, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 26){ TakingLesson[playerid] = 27; SetPlayerRaceCheckpoint(playerid, 0, 1391.34, -1776.10, 13.03, 1330.11, -1729.98, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 27){ TakingLesson[playerid] = 28; SetPlayerRaceCheckpoint(playerid, 0, 1330.11, -1729.98, 13.03, 1314.66, -1637.57, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 28){ TakingLesson[playerid] = 29; SetPlayerRaceCheckpoint(playerid, 0, 1314.66, -1637.57, 13.03, 1353.21, -1583.12, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 29){ TakingLesson[playerid] = 30; SetPlayerRaceCheckpoint(playerid, 0, 1353.21, -1583.12, 13.03, 1426.86, -1637.22, 13.03, 6.0);}
	    else if(TakingLesson[playerid] == 30){ TakingLesson[playerid] = 31; SetPlayerRaceCheckpoint(playerid, 1, 1426.86, -1637.22, 13.03, 0.0, 0.0, 0.0, 6.0);}
		else if(TakingLesson[playerid] == 31)
		{
		    DisablePlayerRaceCheckpoint(playerid);
		    SendClientMessage(playerid, COLOR_DARKGREEN, "Congratulations! You earned your drivers license!");
			
			new ELicenseFlags:flags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			flags |= ELicense_Drivers;
			SetPVarInt(playerid, "LicenseFlags", _:flags);
			
   			RemovePlayerFromVehicle(playerid);
   			SetPlayerPos(playerid, 1423.09, -1635.79, 13.54);
   			SetPlayerFacingAngle(playerid, 180.0);
   			DestroyVehicle(LessonCar[playerid]);
			TakingLesson[playerid] = 0;
		}
	}
}
govOnPlayerStateChange(playerid, newstate, oldstate) {
	#pragma unused newstate
	if(oldstate == PLAYER_STATE_DRIVER)
	{
		if(TakingLesson[playerid] > 0) {
			stopDrivingTest(playerid);
		}

	}
}
stopDrivingTest(playerid) {
	TakingLesson[playerid] = 0;
	DestroyVehicle(LessonCar[playerid]);
	LessonCar[playerid] = 0;
	DisablePlayerRaceCheckpoint(playerid);
}
checkDrivingTestTimer() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(TakingLesson[i] > 0) {
				checkDrivingTest(i);
			}
		}
	}
}
checkDrivingTest(playerid) {
	new speed;
	new Float:vHealth;
	new msg[128];
	new veh;
	if(IsPlayerConnectEx(playerid)) {
		if(TakingLesson[playerid] > 1) {
			speed = GetSpeed(playerid);
			veh = GetPlayerVehicleID(playerid);
			GetVehicleHealth(veh, vHealth);
			if(speed > MAX_ALLOWED_SPEED) {
				format(msg, sizeof(msg), "You failed your driving test because you exceeded the speed limit which is %d km/h", MAX_ALLOWED_SPEED);
				SendClientMessage(playerid, X11_TOMATO_2, msg);
				stopDrivingTest(playerid);
				return 1;
			} else if(vHealth < 800.0) {
				SendClientMessage(playerid, X11_TOMATO_2, "You failed your driving test because you damaged the vehicle.");
				stopDrivingTest(playerid);
				return 1;
			}
		}
	}
	return 1;
}
YCMD:license(playerid, params[], help) {
	new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
	new hasinsurance = GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
	new msg[128];
	new yes[24] = "Acquired";
	new no[24] = "Not Acquired";
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Show your licenses to yourself");
		return 1;
	}
	SendClientMessage(playerid, COLOR_WHITE, "|__________________ Licenses __________________|");
	format(msg, sizeof(msg), "* Driving License: %s",lflags&ELicense_Drivers?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Flying License: %s",lflags&ELicense_Flying?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Fishing License: %s",lflags&ELicense_Fishing?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Sailing License: %s",lflags&ELicense_Sailing?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Weapons License: %s",lflags&ELicense_Gun?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Vehicle Insurance: %s",hasinsurance?yes:no);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	SendClientMessage(playerid, COLOR_WHITE, "|______________________________________________|");
	return 1;
}
YCMD:showlicenses(playerid, params[], help) {
	new yes[24] = "Acquired";
	new no[24] = "Not Acquired";
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Show your licenses to someone");
		return 1;
	}
	new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
	new hasinsurance = GetPVarInt(playerid, "UserFlags") & EUFHasCarInsurance;
	new target;
	if(!sscanf(params, "k<playerLookup>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /showlicenses [playerid/name]");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	if(GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target) || !IsPlayerInRangeOfPoint(target, 3.0, X, Y, Z)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
		return 1;		
	}
	new msg[128];
	format(msg, sizeof(msg), "|__________________ Licenses of %s __________________|",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
	SendClientMessage(target, COLOR_WHITE, msg);
	format(msg, sizeof(msg), "* Class A Driving License: %s",lflags&ELicense_Drivers?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Flying License: %s",lflags&ELicense_Flying?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Fishing License: %s",lflags&ELicense_Fishing?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Sailing License: %s",lflags&ELicense_Sailing?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Weapons License: %s",lflags&ELicense_Gun?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "* Vehicle Insurance: %s",hasinsurance?yes:no);
	SendClientMessage(target, COLOR_LIGHTBLUE, msg);
	SendClientMessage(target, COLOR_WHITE, "|______________________________________________|");
	return 1;
}
YCMD:agivelicense(playerid, params[], help) {
	new target, type[32];
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>s[32]", target, type)) {
		new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(target, "LicenseFlags");
		if(!strcmp(type, "Driving", true)) {
			lflags |= ELicense_Drivers;
		}
		else if(!strcmp(type, "Flying", true)) {
			lflags |= ELicense_Flying;
		} else if(!strcmp(type, "Fishing", true)) {
			lflags |= ELicense_Fishing;
		} else if(!strcmp(type, "Sailing", true)) {
			lflags |= ELicense_Sailing;
		} else if(!strcmp(type, "Weapons", true)) {
			lflags |= ELicense_Gun;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid License type!");
			return 1;
		}
		format(msg, sizeof(msg), "* You have given a %s license to %s",type,GetPlayerNameEx(target, ENameType_CharName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* You have been given a %s license",type);
		SendClientMessage(target, COLOR_LIGHTBLUE, msg);
		SetPVarInt(target, "LicenseFlags", _:lflags);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /agivelicense [playerid/name] [type]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Type: Driving, Flying, Fishing, Sailing, Weapons");
	}
	return 1;
}
YCMD:atakelicense(playerid, params[], help) {
	new target, type[32];
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>s[32]", target, type)) {
		new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
		if(!strcmp(type, "Driving", true)) {
			lflags &= ~ELicense_Drivers;
		}
		else if(!strcmp(type, "Flying", true)) {
			lflags &= ~ELicense_Flying;
		} else if(!strcmp(type, "Fishing", true)) {
			lflags &= ~ELicense_Fishing;
		} else if(!strcmp(type, "Sailing", true)) {
			lflags &= ~ELicense_Sailing;
		} else if(!strcmp(type, "Weapons", true)) {
			lflags &= ~ELicense_Gun;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid License type!");
			return 1;
		}
		format(msg, sizeof(msg), "* You have taken a %s license to %s",type,GetPlayerNameEx(playerid, ENameType_CharName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		format(msg, sizeof(msg), "* You have been taken a %s license",type);
		SendClientMessage(target, COLOR_LIGHTBLUE, msg);
		SetPVarInt(target, "LicenseFlags", _:lflags);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /atakelicense [playerid/name] [type]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Type: Driving, Flying, Fishing, Sailing, Weapons");
	}
	return 1;
}