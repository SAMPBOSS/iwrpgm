enum EBombFlags (<<= 1) {
	EBombFlags_None = 0,
	EBombFlags_EngineBomb = 1, //activate on car engine activation
};
enum EPlacedBombInfo {
	EPBIObjectID,
	Float:EBPI_BombPower,
	EBPI_BombReliability,
	EBPI_AttachedCar,
	EBPI_BombPlacer,
	EBombFlags:EBPI_BombFlags,
};
#define MAX_BOMBS 250
new PlacedBombs[MAX_BOMBS][EPlacedBombInfo];

YCMD:disarm(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Disarms a bomb");
		return 1;
	}
	new bomb = findBomb(playerid, 5.5);
	if(!IsPlayerInRangeOfBomb(playerid, 5.5, bomb)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be near the bomb!");
		return 1;
	}
	new r = random(12);
	format(query, sizeof(query), "* %s bends down and attempts to disarm the explosive.",GetPlayerNameEx(playerid, ENameType_RPName));
	ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	if(r < 5) {
		detonateBomb(bomb);
	} else {
		DestroyBomb(bomb);
		format(query, sizeof(query), "* The bomb has successfully been disarmed.",GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	}
	return 1;
}
YCMD:plantbomb(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Place a bomb");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, vw, interior;
	GetPlayerPos(playerid, X, Y, Z);
	vw = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);
	if(vw != 0 || interior != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot place this here");
		return 1;
	}
	new size, reliability;
	getPlayerHoldingBombInfo(playerid, size, reliability);
	if(size == 0 || reliability == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't holding a bomb!");
		return 1;
	}
	new slot = placeBombAtPoint(X,Y,Z, size, reliability);
	ApplyAnimation(playerid, "BOMBER","BOM_Plant_In",4.0,0,0,0,0,0);
	PlacedBombs[slot][EBPI_BombPlacer] = GetPVarInt(playerid, "CharID");
	format(query, sizeof(query), "* %s bends down and plants a C4 explosive.",GetPlayerNameEx(playerid, ENameType_RPName));
	ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	removePlayerBomb(playerid);
	HintMessage(playerid, COLOR_DARKGREEN, "Use /detonate to detonate the bomb.");
	return 1;
}
YCMD:carbomb(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Place a car bomb");
		return 1;
	}
	new car = GetClosestVehicle(playerid);
	new Float:X, Float:Y, Float:Z, Float:CX, Float:CY, Float:CZ;
	GetPlayerPos(playerid, X, Y, Z);
	GetVehiclePos(car, CX, CY, CZ);
	new size, reliability;
	getPlayerHoldingBombInfo(playerid, size, reliability);
	if(size == 0 || reliability == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't holding a bomb!");
		return 1;
	}
	if(IsPlayerInRangeOfPoint(playerid, 5.5, CX, CY, CZ)) {
		new slot = attachBombToCar(car, size, reliability);
		ApplyAnimation(playerid, "BOMBER","BOM_Plant_In",4.0,0,0,0,0,0);
		PlacedBombs[slot][EBPI_BombPlacer] = GetPVarInt(playerid, "CharID");
		format(query, sizeof(query), "* %s bends down and plants a C4 explosive.",GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		removePlayerBomb(playerid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too far away!");
	}
	HintMessage(playerid, COLOR_DARKGREEN, "Use /detonate to detonate the bomb.");
	return 1;
}
YCMD:enginebomb(playerid, params[], help) {
	new bomb = findBomb(playerid, 5.5);
	if(!IsPlayerInRangeOfBomb(playerid, 5.5, bomb)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be near the bomb!");
		return 1;
	}
	if(bomb != -1 && PlacedBombs[bomb][EBPI_AttachedCar] != 0) {
		if(PlacedBombs[bomb][EBPI_BombFlags] & EBombFlags_EngineBomb) {
			SendClientMessage(playerid, X11_TOMATO_2, "This bomb is already an engine bomb!");
			return 1;
		}
		PlacedBombs[bomb][EBPI_BombFlags] |= EBombFlags_EngineBomb;
		format(query, sizeof(query), "* %s bends down and presses some buttons on the C4 explosive.",GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(25.0, playerid, query, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be near a car bomb!");
	}
	return 1;
}
bombOnVehicleEngine(carid) {
	new bomb = getCarBomb(carid);
	if(bomb == -1) return 0;
	if(PlacedBombs[bomb][EBPI_BombFlags] & EBombFlags_EngineBomb) {
		killVehiclePassengers(carid);
		detonateBomb(bomb);
	}
	return 1;
}
getCarBomb(carid) {
	for(new i=0;i<sizeof(PlacedBombs);i++) {
		if(PlacedBombs[i][EPBIObjectID] != 0 && PlacedBombs[i][EBPI_AttachedCar] == carid) {
			return i;
		}
	}
	return -1;
}
IsPlayerInRangeOfBomb(playerid, Float:radi, bomb) {
	new Float:X, Float:Y, Float:Z;
	if(bomb < 0 || bomb > sizeof(PlacedBombs)) {
		return 0;
	}
	if(PlacedBombs[bomb][EPBIObjectID] == 0) {
		return 0;
	}
	new carid = PlacedBombs[bomb][EBPI_AttachedCar];
	if(carid != 0) {
		GetVehiclePos(carid, X, Y, Z);
	} else {
		GetDynamicObjectPos(PlacedBombs[bomb][EPBIObjectID], X, Y, Z);
	}
	return IsPlayerInRangeOfPoint(playerid, radi, X, Y, Z);
}
attachBombToCar(car, size, reliability) {
	new slot = findFreeBomb();
	if(slot == -1) return -1;
	new Float:X, Float:Y, Float:Z;
	GetVehiclePos(car, X, Y, Z);
	PlacedBombs[slot][EPBIObjectID] = CreateDynamicObject(19300, X, Y, Z-0.9, 0, 89.325012207031, 3.9700012207031);
	AttachDynamicObjectToVehicle(PlacedBombs[slot][EPBIObjectID], car, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	PlacedBombs[slot][EBPI_BombPower] = size;
	PlacedBombs[slot][EBPI_BombReliability] = reliability;
	PlacedBombs[slot][EBPI_AttachedCar] = car;
	return slot;
}
getPlayerHoldingBombInfo(playerid, &size = 0, &reliability = 0) {
	size = GetPVarInt(playerid, "BombSize");
	reliability = GetPVarInt(playerid, "BombReliability");
	return size != 0 && reliability != 0;
}
removePlayerBomb(playerid) {
	DeletePVar(playerid, "BombSize");
	DeletePVar(playerid, "BombReliability");
}
YCMD:detonate(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Detonates a bomb");
		return 1;
	}
	new bomb = findFirstBomb(playerid);
	if(bomb != -1) {
		detonateBomb(bomb);
		SendClientMessage(playerid, X11_TOMATO_2, "* Bomb Detonated!");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* Bomb not found!");
	}
	return 1;
}
detonateBomb(bombid) {
	new Float:X, Float:Y, Float:Z;
	GetDynamicObjectPos(PlacedBombs[bombid][EPBIObjectID],X,Y,Z);
	new rel = PlacedBombs[bombid][EBPI_BombReliability];
	new use = RandomEx(0, rel+1);
	new Float:size = PlacedBombs[bombid][EBPI_BombPower];
	if(use == rel) {
		SendAreaMessage(size*2.0, X, Y, Z, 0, "* You hear a buzzing noise.",COLOR_PURPLE);
	} else {
		new car = PlacedBombs[bombid][EBPI_AttachedCar];
		if(car != 0) {
			SetVehicleHealth(car, 249.0);
		}
		SendAreaMessage(size*5.0, X, Y, Z, 0, "* You hear a large explosion.",COLOR_PURPLE);
		CreateExplosion(X,Y,Z,7,size);
	}
	DestroyBomb(bombid);
}
DestroyBomb(bombid) {
	#if debug
	printf("Debug: Called DestroyBomb->DestroyDynamicObject.");
	#endif
	DestroyDynamicObject(PlacedBombs[bombid][EPBIObjectID]);
	PlacedBombs[bombid][EPBIObjectID] = 0;
	PlacedBombs[bombid][EBPI_BombPlacer] = 0;
	PlacedBombs[bombid][EBPI_AttachedCar]= 0;
	PlacedBombs[bombid][EBPI_BombFlags] = EBombFlags:0;
}
YCMD:givebomb(playerid, params[], help) {
	new target, size, reliability;
	if(!sscanf(params, "k<playerLookup_acc>D(25)D(25)", target, size, reliability)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(getPlayerHoldingBombInfo(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is already holding a bomb!");
			return 1;
		}
		SendClientMessage(target, COLOR_DARKGREEN, "You have been given a bomb!");
		givePlayerBomb(target, size, reliability);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givebomb [playerid] [size] [reliability]");
	}
	return 1;
}
givePlayerBomb(playerid, size, reliability) {
	SetPVarInt(playerid, "BombSize", size);
	SetPVarInt(playerid, "BombReliability", reliability);
}
findBomb(playerid, Float:radi) {
	for(new i=0;i<MAX_BOMBS;i++) {
		if(IsPlayerInRangeOfBomb(playerid, radi, i) && PlacedBombs[i][EPBIObjectID] != 0) {
			return i;
		}
	}
	return -1;
}
findFirstBomb(playerid) {
	new charid = GetPVarInt(playerid, "CharID");
	for(new i=0;i<MAX_BOMBS;i++) {
		if(PlacedBombs[i][EBPI_BombPlacer] == charid) {
			return i;
		}
	}
	return -1;
}
placeBombAtPoint(Float:X, Float:Y, Float:Z, Float:size, reliability) {
	new slot = findFreeBomb();
	if(slot == -1) return -1;
	PlacedBombs[slot][EPBIObjectID] = CreateDynamicObject(1654, X, Y, Z-0.9, 0, 89.325012207031, 3.9700012207031);
	PlacedBombs[slot][EBPI_BombPower] = size;
	PlacedBombs[slot][EBPI_BombReliability] = reliability;
	PlacedBombs[slot][EBPI_AttachedCar] = 0;
	return slot;
}
findFreeBomb() {
	for(new i=0;i<MAX_BOMBS;i++) {
		if(PlacedBombs[i][EPBIObjectID] == 0) {
			return i;
		}
	}
	return -1;
}