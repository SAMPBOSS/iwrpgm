#define MAX_HOUSE_ACCESSORIES 40
enum EHouseObjects {
	EHouseObjectID,
	EHouseObjectName[32],
	EHouseObjectPrice, //price in "furniture tokens"
	EHouseObjectInside, //inside only
	EHouseObjectCategory,
	EHouseObjectDonRank,
};
#define HOUSE_TOKEN_VALUE 200 //price per 1 token
#define HOUSEFURN_INSIDE_DIST 250.0
#define HOUSEFURN_OUTSIDE_DIST 15.0

new HouseFurnCategories[][] = {{"Beds"},{"Gym Accessories"},{"Electronics"},{"Chairs/Couches"},{"Tables"},{"Decorations"},{"General Furniture"},{"Kitchen"},{"Bathroom"},{"Doors"},{"Walls"},{"Long Walls"},{"Floor"},{"Door Wall"},{"Studio"},{"Random"}};

new BuyableHouseObjects[][EHouseObjects] = {
{1700,"Ugly Large Bed",2,0,0,0},
{1701,"Antique Bed Medium",2,0,0,0},
{1725,"Antique Bed Large",3,0,0,0},
{1812,"Ugly Single Bed",2,0,0,0},
{1745,"Ugly Medium Two Person Bed",4,0,0,0},
{1793,"Mattress",2,0,0,0},
{1795,"Ugly Swank Two Person Bed",3,0,0,0},
{1796,"Ugly One Person Bed",3,0,0,0},
{1797,"Nice Two Person Bed",4,0,0,0},
{1798,"Two Person Bed",5,0,0,0},
{1799,"Brown Two Person Bed",3,0,0,0},
{2298,"Swank Two Person Bed with Cab",5,0,0,0},
{2299,"Swank Two Person Bed",4,0,0,0},
{2301,"Medium Two Person Bed",4,0,0,0},
{2302,"Ugly Two Person Bed",3,0,0,0},
{2563,"Two Person Bed with Cabinet",7,0,0,6}, //silver
{2566,"Two Person Bed Black Cabinet",7,0,0,6}, //silver
{14446,"Zebra Two Person Bed XL",10,0,0,7}, //Gold
{2627,"Treadmill",3,0,1,0},
{2628,"Lat Pulldown",3,0,1,0},
{2629,"Bench Press",4,0,1,0},
{2630,"Gym Bike",3,0,1,0},
{2631,"Red Gymnastic Mat",4,0,1,0},
{2632,"Blue Gymnastic Mat",4,0,1,0},
{2915,"Two Dumbbells",5,0,1,0},
{3071,"One Dumbell",3,0,1,0},
{2099,"Ugly Brown Stereo System",3,0,2,0},
{2100,"Brown Stereo System",3,0,2,0},
{2102,"Black Stereo",4,0,2,0},
{2103,"White Stereo System",4,0,2,0},
{2104,"Long Black Stereo System",3,0,2,0},
{2225,"Swank Stereo",2,0,2,0}, //silver
{2226,"Black & Red Stereo",4,0,2,0},
{1429,"Small Brown TV",3,0,2,0},
{1518,"Medium Black TV",5,0,2,0},
{19786,"Huge Flat TV",5,0,2,0},
{19787,"Flat TV",5,0,2,0},
{1747,"Ugly Small Brown TV",2,0,2,0},
{1749,"Small Ugly Black TV",3,0,2,0},
{1750,"Small Brown & White TV",3,0,2,0},
{1751,"Small Grey TV",3,0,2,0},
{1752,"Black Swank TV",5,0,2,0},
{1781,"Ugly Grey TV",2,0,2,0},
{1786,"Long Black TV",4,0,2,0},
{1791,"Nice Black TV",4,0,2,0},
{1792,"Grey Swank TV",6,0,2,0},
{2224,"Orange TV",2,0,2,0},
{2296,"Large TV Unit",6,0,2,7}, //Gold
{2297,"Medium TV Unit",4,0,2,6}, //silver
{2595,"Small TV with DVD Player",4,0,2,0},
{2596,"Wall TV",3,0,2,0},
{14604,"Small TV Unit",3,0,2,0},
{2073,"Old Sealing light",2,0,2,0},
{1734,"Hanging Sealing light",2,0,2,0},
{2076,"Modern Sealing light",2,0,2,0},
{2069,"Modern Interior Light",2,0,2,0},
{2108,"Modern interior light w/ stand",2,0,2,0},
{15063,"Hanging Sealing Light",2,0,2,0},
{2196,"Working Lamp",2,0,2,0},
{2238,"Lava Lamp",2,0,2,0},
{2726,"Strip Lamp",2,0,2,0},
{3534,"Ugly Lamp",2,0,2,0},
{2190,"Computer",3,0,2,0},
{1998,"Ugly Desk with PC",3,0,2,0},
{1999,"Brown Desk with PC & Phone",3,0,2,0},
{2008,"Nice Desk with PC & Phone",3,0,2,0},
{2009,"Large white Desk with PC",3,0,2,0},
{2028,"Old Gaming Console", 5,0,2,0},
{1782,"Video Player", 2,0,2,0},
{1788,"Video Unit", 2,0,2,0},
{2278,"Old Coin Machine 1", 5,0,2,0},
{2279,"Old Coin Machine 2", 5,0,2,0},
{1886,"CCTV Camera",10,0,2,7}, //Gold
{1663,"Black Chair", 3,0,3,0},
{1671,"Nice Black Chair", 3,0,3,0},
{1714,"King Officer Chair", 3,0,3,7}, //Gold
{1720,"Brown & White Chair", 4,0,3,0},
{1721,"Ugly Chair", 3,0,3,0},
{1739,"Brown Dinning Chair", 4,0,3,0},
{1806,"Office Chair", 5,0,3,0},
{1810,"Ugly Folding Chair", 2,0,3,0},
{1811,"Brown Chair", 2,0,3,0},
{2079,"Black Dinning Chair", 3,0,3,0},
{2096,"Brown Rocking Chair", 3,0,3,0},
{2120,"Small Brown Dinning Chair", 2,0,3,6}, //Silver
{2121,"Red Chair", 3,0,3,0},
{2123,"Swank Chair", 3,0,3,0},
{2124,"Long Dinning Chair", 4,0,3,0},
{2636,"Brown Pizza Chair", 2,0,3,0},
{1704,"Single Black Sofa", 3,0,3,0},
{1705,"Single Brown Sofa", 3,0,3,6}, //Silver
{1708,"Single Ugly Sofa", 2,0,3,0},
{1711,"Single Light Brown Sofa", 3,0,3,0},
{1727,"Single Nice Sofa",3,0,3,0},
{1735,"Single Fancy Sofa",7,0,3,7}, //Gold
{1723,"Black Large Sofa", 4,0,3,0},
{1702,"Brown Large Sofa", 5,0,3,6}, //Silver
{1726,"Nice Large Sofa", 3,0,3,0},
{1728,"Ugly Large Sofa", 4,0,3,0},
{1706,"Pink Ugly Sofa", 6,0,3,0},
{1707,"Weird Looking Sofa", 4,0,3,0},
{1709,"XL Ugly Sofa", 8,0,3,7}, //Gold
{1433,"Ugly Table 1", 2,0,4,0},
{1516,"Ugly Table 2", 2,0,4,0},
{1826,"Nice Looking Table", 3,0,4,0},
{1827,"Round Glass Table", 4,0,4,6}, //Silver
{2311,"Ugly Table 3", 2,0,4,0},
{2313,"TV Table 1", 4,0,4,0},
{2314,"TV Table 2", 4,0,4,0},
{2315,"TV Table 3", 4,0,4,0},
{2321,"TV Table 4", 4,0,4,0},
{1737,"Brown Dinning Table", 3,0,4,0},
{1770,"Ugly Dinning Table", 2,0,4,0},
{2029,"Nice Dinning Table", 3,0,4,0},
{2030,"Round Dinning Table", 5,0,4,0},
{2031,"Weird Dinning Table", 2,0,4,0},
{2032,"Dinning Table 1", 4,0,4,0},
{2080,"Dinning Table 2", 4,0,4,0},
{2086,"Round Glass Dinning Table", 6,0,4,6}, //Silver
{2109,"Round Dinning Table", 4,0,4,0},
{2110,"Dinning Table 3", 4,0,4,0},
{2111,"Round Brown Dining Table", 5,0,4,0},
{2357,"Long Dining Table", 8,0,4,7}, //Gold
{2811,"Plant 1",1,0,5,0},
{2244,"Plant 2",1,0,5,0},
{2249,"Plant 3",1,0,5,0},
{338,"Pool Cue",1,0,5,0},
{1946,"Basketball Ball",1,0,5,0},
{2855,"Bike Magazines",1,0,5,0},
{2271,"Wall Frame",1,0,5,0},
{2281,"Wall Frame with picture",1,0,5,0},
{2277,"Cat Frame",1,0,5,0},
{2270,"Wall Frame with leafs",1,0,5,0},
{2280,"Scenery Frame",1,0,5,0},
{14456,"Old Hanging Sealing object",1,0,5,0},
{2078,"Antique medium sized cabinet",3,0,6,0},
{2007,"Grey Filing Cabinet",5,0,6,0},
{1744,"Wall Hanging Shelf",5,0,6,0},
{14455,"Tall Book shelf",5,0,6,0},
{2088,"High Wood Cabinet / Shelf",5,0,6,0},
{1741,"Low Wood Cabinet / Shelf",5,0,6,0},
{2131,"White Fridge",5,0,7,0},
{2132,"White Sink",3,0,7,0},
{2133,"White Counter",3,0,7,0},
{2134,"White Counter",3,0,7,0},
{2141,"White Small Fridge",3,0,7,0},
{2147,"Ugly Fridge",3,0,7,0},
{2170,"Ugly Cooker",3,0,7,0},
{2135,"Fancy Cooker",10,0,7,7}, //Gold
{2136,"Fancy Sink",10,0,7,7}, //Gold
{2137,"Fancy Counter",10,0,7,7}, //Gold
{2138,"Fancy Counter",10,0,7,7}, //Gold
{2139,"Fancy Counter",10,0,7,7}, //Gold
{2140,"Fancy Fridge",10,0,7,7}, //Gold
{2341,"White Corner Counter",3,0,7,0},
{2820,"Dirty Plates1",5,0,7,0},
{2822,"Dirty Plates2",5,0,7,0},
{2848,"Dirty Plates3",5,0,7,0},
{2849,"Dirty Plates4",5,0,7,0},
{2850,"Dirty Plates5",5,0,7,0},
{2851,"Dirty Plates6",5,0,7,0},
{2862,"Dirty Plates7",5,0,7,0},
{2863,"Dirty Plates8",5,0,7,0},
{14384,"Full Kitchen Set",5,0,7,0},
{2149,"Microwave1",5,0,7,0},
{2421,"Microwave2",5,0,7,6}, //Silver 
{19579,"BreadLoaf",5,0,7,0},
{19574,"Orange",5,0,7,0}, 
{19575,"Red Apple",5,0,7,0},
{19576,"Green Apple",5,0,7,0},
{19577,"Tomato",5,0,7,0}, 
{19578,"Banana",5,0,7,0}, 
{19561,"CerealBox 1",5,0,7,0}, 
{19562,"CerealBox 2",5,0,7,0}, 
{19563,"JuiceBox 1",5,0,7,0}, 
{19564,"JuiceBox 2",5,0,7,0},
{19565,"IceCreamBarsBox",5,0,7,0},
{19566,"FishFingersBox",5,0,7,0},
{19570,"Milk Bottle",5,0,7,0},
{19572,"Pisshbox",5,0,7,0},
{19573,"BriquettesBag",7,0,7,0}, 
{19581,"MarcosFryingPan",5,0,7,0},
{19583,"MarcosKnife",10,0,7,7}, //Gold
{19584,"MarcosSaucepan",6,0,7,0}, 
{19585,"MarcosPan",8,0,7,0},
{19586,"MarcosSpatula",7,0,7,0}, 
{2516,"White Nice bath tub",5,0,8,0},
{2520,"Bathroom shower",5,0,8,0},
{2526,"White Bath Tub",5,0,8,0},
{2523,"White Sink",5,0,8,0},
{2528,"White Toilet",5,0,8,0},
{2741,"Soap Dispenser",5,0,8,0},
{2750,"Hair Dryer",5,0,8,0},
{2515,"Modern White round sink",5,0,8,0},
{2742,"Hand Dryer",5,0,8,0},
{2211,"Bathroom sink unit",5,0,8,0},
{1491,"Fancy Swinging Door",5,0,9,7}, //Gold
{1492,"Ugly Swinging Door",5,0,9,0},
{1493,"Brown Door 28",5,0,9,6}, //Silver
{1494,"Ugly Door",5,0,9,0},
{1498,"Ugly White Door",5,0,9,0},
{1452,"Brown Swinging Door",5,0,9,6}, //Silver
{1504,"Red Door",5,0,9,7}, //Gold
{1505,"Blue Door",5,0,9,7}, //Gold
{1506,"White Door",5,0,9,6}, //Silver
{1507,"Yellow Door",5,0,9,7}, //Gold
{2395,"Sport Wall",3,0,10,0},
{19353,"White & Red Wall",5,0,10,0},
{19354,"Black Wall",5,0,10,0},
{19355,"Grey Wall",5,0,10,0},
{19356,"Brown Wall",5,0,10,0},
{19357,"White Wall",5,0,10,0},
{19358,"Black & Grey Wall",5,0,10,0},
{19359,"Light Brown Wall",5,0,10,0},
{19361,"Fancy Brown Wall",5,0,10,6},		//Silver
{19362,"Ugly Grey Wall",5,0,10,0},
{19364,"Grey Brick Wall",5,0,10,0},
{19365,"Light Blue Wall",5,0,10,0},
{19366,"Fancy Brown Wall",5,0,10,6},		//Silver
{19367,"Fancy Light Blue Wall",5,0,10,6},		//Silver
{19368,"Fancy Grey Wall",5,0,10,6},		//Silver
{19369,"Fancy light Grey Wall",5,0,10,6},		//Silver
{19445,"Long White & Red Wall",10,0,11,0},
{19446,"Long Black Wall",10,0,11,6},		//Silver
{19447,"Long Grey Wall",10,0,11,6},		//Silver
{19448,"Long Brown Wall",10,0,11,6},		//Silver
{19449,"Long White Wall",10,0,11,6},		//Silver
{19450,"Long Black & Grey Wall",10,0,11,6},		//Silver
{19451,"Long Light Brown Wall",10,0,11,6},		//Silver
{19453,"Long Fancy Brown Wall",10,0,11,7},		//Gold
{19454,"Long Ugly Grey Wall",10,0,11,6},		//Silver
{19455,"Long Ugly Brown Wall",10,0,11,6},		//Silver
{19456,"Long Brick Wall",10,0,11,6},		//Silver
{19457,"Long Fancy Blue Wall",10,0,11,7},		//Gold
{19459,"Long Fancy Blue Wall",10,0,11,7},		//Gold
{19460,"Long Fancy Dark Grey Wall",10,0,11,7},		//Gold
{19461,"Long Fancy Grey Wall",10,0,11,7},		//Gold
{19375,"Ugly Brown Floor",6,0,12,6},		//Silver
{19376,"Fancy Light Brown Floor",6,0,12,6},		//Silver
{19377,"Grey Floor",6,0,12,6},		//Silver
{19378,"Fancy Brown Floor",6,0,12,6},		//Silver
{19379,"Fancy Floor",6,0,12,6},		//Silver
{19383,"White & Red Door Wall",8,0,13,0},
{19384,"Black Door Wall",8,0,13,0},
{19385,"Grey Door Wall",8,0,13,0},
{19386,"Brown Door Wall",8,0,13,0},
{19387,"White Door Wall",8,0,13,0},
{19388,"Black & Grey Door Wall",8,0,13,0},
{19389,"Light Brown Door Wall",8,0,13,6},		//Silver
{19390,"Fancy Brown Door Wall",8,0,13,6},		//Silver
{19391,"Ugly Grey Door Wall",8,0,13,0},
{19392,"Ugly Brown Door",8,0,13,0},
{19393,"Brick Door Wall",8,0,13,0},
{19394,"Blue Door Wall",8,0,13,0},
{19395,"Fancy Dark Blue Wall",8,0,13,6},		//Silver
{19396,"Fancy Dark Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19397,"Fancy Grey Wall",8,0,13,6},		//Silver
{19608,"WoodenStage",8,0,14,7},		//Gold
{19609,"DrumKit",8,0,14,0},		
{19610,"Microphone",8,0,14,0},		
{19611,"MicrophoneStand",8,0,14,0},	
{19612,"GuitarAmp1",8,0,14,0},		
{19613,"GuitarAmp2",8,0,14,6},		//Silver
{19614,"GuitarAmp3",8,0,14,6},		//Silver
{19615,"GuitarAmp4",8,0,14,6},		//Silver
{19616,"GuitarAmp5",8,0,14,6},		//Silver
{19617,"GoldRecord",8,0,14,0},
{19630,"Fish", 1,0,15,7},		//Gold
{902,"StarFish",1,0,15,0},
{1599,"Fish", 1,15,0},
{1600,"Fish", 1,0,15,0},
{1601,"Fish", 1,0,15,0},
{1602,"Jellyfish", 1,0,15,0},
{1603,"Jellyfish", 1,0,15,6},		//Silver
{1604,"Fish",1,0,15,0},
{19632,"Firewood",8,0,15,0},
{19621,"Oilcan",5,0,15,6},		//Silver
{19622,"Broom",6,0,15,6},		//Silver
{19624,"Case",6,0,15,6},		//Silver
{19625,"Ciggy",7,0,15,6},		//Silver
{19626,"Spade",5,0,15,6},		//Silver
{19627,"Wrench",6,0,15,6},		//Silver
{19631,"Sledge Hammer",15,0,15,6}		//Gold
};

enum EHouseObjectInfo {
	EHouseObjectObjID,
	EHouseObjectType,
	EHouseObjectHouseID,
	EHouseObjectSQLID,
};
new HouseObjects[MAX_HOUSES][MAX_HOUSE_ACCESSORIES][EHouseObjectInfo];

enum EFurnitureMenuState {
	EFSMS_CurPage,
	EFSMS_RemainingCount,
	EFSMS_PageCount,
	EFSMS_InPreviewMode,
	EFSMS_MenuIndex
};
new FurnitureMenuState[MAX_PLAYERS][EFurnitureMenuState];

enum {
	EHouseFurnDialog_BuyFurnCat = EHouseFurniture_Base + 1,
	EHouseFurnDialog_Prompt,
	EHouseFurnDialog_DelFurn,
	EHouseFurnDialog_EditPrompt,
	EHouseFurnDialog_EditFurnMenu,
};

forward OnCreateHouseObject(houseid, object, Float:X, Float:Y, Float:Z, Float:RX, Float:RY, Float:RZ, inside);
forward OnLoadHouseObjects(houseid);
LoadHouseFurniture(houseid) {
	new sqlid = getHouseSQLID(houseid);
	if(sqlid != -1) {
		format(query, sizeof(query), "SELECT `id`,`objectid`,`X`,`Y`,`Z`,`RX`,`RY`,`RZ`,`inside` FROM `housefurniture` WHERE `house` = %d",sqlid);
		mysql_function_query(g_mysql_handle, query, true, "OnLoadHouseObjects", "d",sqlid);
	}
}
YCMD:buytokens(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to buy furniture tokens");
		return 1;
	}
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_FurnitureBanned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are banned from placing furniture!");
		return 1;
	}
	new numtokens;
	new business = getBusinessInside(playerid);
	if(business == -1 || !IsFurnitureStore(business)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be inside a furniture store!");
		return 1;
	}
	if(!sscanf(params,"d", numtokens)) {
		if(numtokens < 1 || numtokens > 1000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid amount, please choose a value between 1 and 1000.");
			return 1;
		}
		new price = HOUSE_TOKEN_VALUE*numtokens;
		if(GetMoneyEx(playerid) < price) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
			return 1;
		}
		GiveMoneyEx(playerid, -price);
		addToBusinessTill(business, floatround(price/2));
		new ntokens = GetPVarInt(playerid, "FurnitureTokens");
		SetPVarInt(playerid, "FurnitureTokens", ntokens+numtokens);
		SendClientMessage(playerid, COLOR_DARKGREEN, "Tokens Purchased!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /buytokens [numtokens]");
		format(query, sizeof(query), "* Price per token: $%d",HOUSE_TOKEN_VALUE);
		SendClientMessage(playerid, COLOR_DARKGREEN, query);
	}
	return 1;
}

public OnLoadHouseObjects(houseid) {
	new rows, fields;
	new id_string[128];
	new Float:X,Float:Y,Float:Z,Float:RX,Float:RY,Float:RZ,inside,id,object;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		id = strval(id_string);
		cache_get_row(i, 1, id_string);
		object = strval(id_string);
		cache_get_row(i, 2, id_string);
		X = floatstr(id_string);
		cache_get_row(i, 3, id_string);
		Y = floatstr(id_string);
		cache_get_row(i, 4, id_string);
		Z = floatstr(id_string);
		cache_get_row(i, 5, id_string);
		RX = floatstr(id_string);
		cache_get_row(i, 6, id_string);
		RY = floatstr(id_string);
		cache_get_row(i, 7, id_string);
		RZ = floatstr(id_string);
		cache_get_row(i, 8, id_string);
		inside = id_string[0];
		placeObject(houseid, object, X, Y, Z, RX, RY, RZ, inside, id);
	}
	return 1;
}
isFurnitureInValidRange(houseid, Float:X, Float:Y, Float:Z, inside) {
	new Float:ox, Float:oy, Float:oz,Float:range;
	if(!inside) {
		ox = Houses[houseid][EHouseX];
		oy = Houses[houseid][EHouseY];
		oz = Houses[houseid][EHouseZ];
		range = HOUSEFURN_OUTSIDE_DIST;
	} else {
		ox = Houses[houseid][EHouseExitX];
		oy = Houses[houseid][EHouseExitY];
		oz = Houses[houseid][EHouseExitZ];
		range = HOUSEFURN_INSIDE_DIST;
	}
	return GetPointDistance(X,Y,Z,ox,oy,oz) <= range;
	
}
YCMD:buyfurniture(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to buy furniture");
		return 1;
	}
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_FurnitureBanned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are banned from placing furniture!");
		return 1;
	}
	new houseid;
	houseid = getStandingHouse(playerid,HOUSEFURN_OUTSIDE_DIST);
	if(houseid == -1) {
		houseid = getStandingExit(playerid,HOUSEFURN_INSIDE_DIST);
	}
	if(houseid == -1 || !playerOwnsHouse(houseid, playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be inside or near your house");
		return 1;
	}
	if(findHouseFurnFreeIndex(houseid) == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have reached the maximum furniture limit!");
		return 1;
	}
	//showBuyFurnitureMenu(playerid,category);
	showBuyFurnitureCatMenu(playerid);
	return 1;
}
YCMD:editfurniture(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to edit furniture");
		return 1;
	}
	showEditFurniturePromptMenu(playerid);
	return 1;
}
YCMD:clearfurniture(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Clears a houses furniture");
		return 1;
	}
	new houseid;
	if(!sscanf(params,"d", houseid)) {
		if(houseid < 0 || houseid > sizeof(Houses) || Houses[houseid][EHouseSQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid House");
			return 1;
		}
		DestroyHouseObjects(houseid);
		SendClientMessage(playerid, COLOR_DARKGREEN, "House Objects destroyed!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /clearfurniture [houseid]");
	}
	return 1;
}
YCMD:deletefurniture(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to edit furniture");
		return 1;
	}
	showDeleteFurniturePromptMenu(playerid);
	return 1;
}
showEditFurniturePromptMenu(playerid) {
	ShowPlayerDialog(playerid, EHouseFurnDialog_EditPrompt, DIALOG_STYLE_MSGBOX, "{00BFFF}Edit Furniture Prompt:", "Would you like to edit the object manually or by list?", "Manual","List");
	return 1;
}
showDeleteFurniturePromptMenu(playerid) {
	ShowPlayerDialog(playerid, EHouseFurnDialog_Prompt, DIALOG_STYLE_MSGBOX, "{00BFFF}Delete Furniture Prompt:", "Would you like to delete the object manually or by list?", "Manual","List");
	return 1;
}
formatFurnitureEditMenu(houseid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	houseid = houseIDFromSQLID(houseid);
	for(new i=0;i<MAX_HOUSE_ACCESSORIES;i++) {
		if(HouseObjects[houseid][i][EHouseObjectSQLID] != 0) {
			format(tempstr, sizeof(tempstr), "%s\n",getFurnitureNameByObjType(HouseObjects[houseid][i][EHouseObjectType]));
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	return dialogstr;
	//ShowPlayerDialog(playerid, EHouseFurnDialog_DelFurn, DIALOG_STYLE_LIST, "{00BFFF}Furniture Menu", dialogstr, "Delete", "Cancel");
}
getFurnitureNameByObjType(index) {
	new retstring[128];
	format(retstring, sizeof(retstring), "%s\n",BuyableHouseObjects[index][EHouseObjectName]);
	return retstring;
}
listitemToSQLID(index, houseid) {
	new sqlid;
	houseid = houseIDFromSQLID(houseid);
	for(new i=0;i<MAX_HOUSE_ACCESSORIES;i++) {
		if(HouseObjects[houseid][i][EHouseObjectSQLID] == 0) {
			#if debug
			printf("index++: %d", index);
			#endif
			index++;
			continue;
		}
		if(HouseObjects[houseid][i][EHouseObjectSQLID] != 0) {
			if(HouseObjects[houseid][index][EHouseObjectSQLID] == HouseObjects[houseid][i][EHouseObjectSQLID]) {
				sqlid = HouseObjects[houseid][i][EHouseObjectSQLID];
				#if debug
				printf("Furniture SQLID: %d Index: %d",sqlid, index);
				#endif
				return sqlid;
			}
		}
	}
	return -1;
}
listitemToObjID(index, houseid) {
	new objid;
	houseid = houseIDFromSQLID(houseid);
	for(new i=0;i<MAX_HOUSE_ACCESSORIES;i++) {
		if(HouseObjects[houseid][i][EHouseObjectObjID] == 0) {
			#if debug
			printf("index++: %d", index);
			#endif
			index++;
			continue;
		}
		if(HouseObjects[houseid][i][EHouseObjectObjID] != 0) {
			if(HouseObjects[houseid][index][EHouseObjectObjID] == HouseObjects[houseid][i][EHouseObjectObjID]) {
				objid = HouseObjects[houseid][i][EHouseObjectObjID];
				#if debug
				printf("Furniture objid: %d Index: %d",objid, index);
				#endif
				return objid;
			}
		}
	}
	return -1;
}
findHouseFurnObjectByCategory(category, index) {
	new x;
	for(new i=0;i<sizeof(BuyableHouseObjects);i++) {
		if(BuyableHouseObjects[i][EHouseObjectCategory] == category) {
			if(x++ == index) {
				return i;
			}
		}
	}
	return -1;
}
findHouseFurnObjectByCategoryModelID(category, modelid) {
	new x;
	for(new i=0;i<sizeof(BuyableHouseObjects);i++) {
		if(BuyableHouseObjects[i][EHouseObjectCategory] == category) {
			if(BuyableHouseObjects[i][EHouseObjectID] == modelid) {
				return i;
			}
		}
	}
	return -1;
}
showBuyFurnitureCatMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(HouseFurnCategories);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",HouseFurnCategories[i]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EHouseFurnDialog_BuyFurnCat, DIALOG_STYLE_LIST, "{00BFFF}Furniture Categories", dialogstr, "Buy", "Cancel");
}
showBuyFurnitureMenu(playerid,category) {
	setupFurnitureMenuState(playerid, 1, category);
}
HouseFurnOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused response
	#pragma unused listitem
	#pragma unused inputtext
	new Float:X, Float:Y, Float:Z, interior/*, vw*/;
	new msg[128];
	GetPlayerPos(playerid, X, Y, Z);
	Y += 2.0;
	//vw = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);
	new houseid;
	houseid = getStandingHouse(playerid,HOUSEFURN_OUTSIDE_DIST);
	if(houseid == -1) {
		houseid = getStandingExit(playerid,HOUSEFURN_INSIDE_DIST);
	}
	if(houseid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be inside or near your house");
		return 1;
	}
	houseid = getHouseSQLID(houseid);
	if(houseid == -1) return 1;
	switch(dialogid) {
		case EHouseFurnDialog_BuyFurnCat: {
			showBuyFurnitureMenu(playerid,listitem);
		}
		case EHouseFurnDialog_Prompt: {
			if(response) {
				SendClientMessage(playerid, COLOR_DARKGREEN, "Select the item you wish to delete");
				SelectObject(playerid);
			} else {
				furnitureShowPlayerDialog(playerid, EHouseFurnDialog_DelFurn, houseid);
			}
			SetPVarInt(playerid, "FurnDelete", 1);
		}
		case EHouseFurnDialog_DelFurn: {
			if(response) {
				new sqlid = listitemToSQLID(listitem, houseid);
				if(sqlid != -1) {
					DestroyHouseObject(sqlid);
					SendClientMessage(playerid, X11_WHITE, "[INFO]: Object destroyed!");
					DeletePVar(playerid, "FurnDelete");
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "Error, couldn't delete object!");
				}
			}
		}
		case EHouseFurnDialog_EditPrompt: {
			if(response) {
				SelectObject(playerid);
				SendClientMessage(playerid, COLOR_DARKGREEN, "Select the item you wish to edit");
			} else {
				furnitureShowPlayerDialog(playerid, EHouseFurnDialog_EditFurnMenu, houseid);
			}
		}
		case EHouseFurnDialog_EditFurnMenu: {
			if(response) {
				new objid = listitemToObjID(listitem, houseid);
				if(objid != -1) {
					EditDynamicObject(playerid, objid);
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "Error, couldn't edit that object!");
				}
			}
		}
	}
	return 1;
}
furnitureShowPlayerDialog(playerid, dialog_type, houseid) {
	dialogstr[0] = 0;
	format(dialogstr, sizeof(dialogstr), "%s", formatFurnitureEditMenu(houseid));
	switch(dialog_type) {
		case EHouseFurnDialog_EditFurnMenu: {
			ShowPlayerDialog(playerid, EHouseFurnDialog_EditFurnMenu, DIALOG_STYLE_LIST, "{00BFFF}Furniture Menu", dialogstr, "Edit", "Cancel");
		}
		case EHouseFurnDialog_DelFurn: {
			ShowPlayerDialog(playerid, EHouseFurnDialog_DelFurn, DIALOG_STYLE_LIST, "{00BFFF}Furniture Menu", dialogstr, "Delete", "Cancel");
		}
	}
}
CreateHouseItem(houseid, objidx, Float:X, Float:Y, Float:Z, inside) {
	format(query, sizeof(query), "INSERT INTO `housefurniture` SET `X` = %f, `Y` = %f, `Z` = %f, `objectid` = %d, `house` = %d,`inside` = %d",X,Y,Z,objidx,houseid,inside);
	mysql_function_query(g_mysql_handle, query, true, "OnCreateHouseObject", "ddffffffd",houseid, objidx, X, Y, Z, 0.0,0.0,0.0,inside);
}
public OnCreateHouseObject(houseid, object, Float:X, Float:Y, Float:Z, Float:RX, Float:RY, Float:RZ, inside) {
	new sqlid = mysql_insert_id();
	placeObject(houseid, object, X, Y, Z, RX, RY, RZ, inside, sqlid);
}

placeObject(houseid, object, Float:X, Float:Y, Float:Z, Float:RX, Float:RY, Float:RZ, inside, sqlid) {
	houseid = houseIDFromSQLID(houseid);
	new idx = findHouseFurnFreeIndex(houseid);
	if(idx != -1) {
		new vw = 0, interior = 0;
		if(inside) {
			vw = houseGetVirtualWorld(houseid);
			interior = houseGetInterior(houseid);
		}
		HouseObjects[houseid][idx][EHouseObjectObjID] = CreateDynamicObject(BuyableHouseObjects[object][EHouseObjectID], X, Y, Z, RX, RY, RZ, vw, interior);
		HouseObjects[houseid][idx][EHouseObjectSQLID] = sqlid;
		HouseObjects[houseid][idx][EHouseObjectType] = object;
	}
	return idx;
}
findHouseFurnFreeIndex(houseid) {
	for(new i=0;i<MAX_HOUSE_ACCESSORIES;i++) {
		if(HouseObjects[houseid][i][EHouseObjectSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
YCMD:furnitureinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Returns information on the nearest piece of furniture");
		return 1;
	}
	SelectObject(playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Select the item you wish to see information on.");
	SetPVarInt(playerid, "FurnInfo", 1);
	return 1;
}
houseFurnOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#pragma unused x
	#pragma unused y
	#pragma unused z
	#pragma unused modelid
	new house, objidx,sqlid;
	if(findHouseFurnByObjID(objectid, house, objidx,sqlid) == 1) {
		if(playerOwnsHouse(house, playerid) || EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
			if(GetPVarInt(playerid, "FurnDelete") == 1) {
				DestroyHouseObject(sqlid);
				CancelEdit(playerid);
				DeletePVar(playerid, "FurnDelete");
			} else if(GetPVarInt(playerid, "FurnInfo") == 1) {
				CancelEdit(playerid);
				DeletePVar(playerid, "FurnInfo");
				SendClientMessage(playerid, X11_WHITE, "** Furniture Info **");
				format(query, sizeof(query), "* Owner %s[%d]",Houses[house][EHouseOwnerName],Houses[house][EHouseOwnerSQLID]);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
			} else {
				EditDynamicObject(playerid, objectid);
			}
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You must own this house to edit this!");
		}
	}
	return 0;
}
DestroyHouseObjects(houseid) {
	for(new i=0;i<MAX_HOUSE_ACCESSORIES;i++) {
		if(HouseObjects[houseid][i][EHouseObjectSQLID] != 0) {
			DestroyHouseObject(HouseObjects[houseid][i][EHouseObjectSQLID]);
		}
	}
}
DestroyHouseObject(sqlid) {
	for(new i=0;i<MAX_HOUSES;i++) {
		for(new x=0;x<MAX_HOUSE_ACCESSORIES;x++) {
			if(HouseObjects[i][x][EHouseObjectSQLID] == sqlid) {
				DestroyDynamicObject(HouseObjects[i][x][EHouseObjectObjID]);
				HouseObjects[i][x][EHouseObjectObjID] = 0;
				HouseObjects[i][x][EHouseObjectSQLID] = 0;
				format(query, sizeof(query), "DELETE FROM `housefurniture` WHERE `id` = %d",sqlid);
				mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
			}
		}
	}
	return -1;
}
houseFurnOnPlayerEditObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	if(response == EDIT_RESPONSE_UPDATE) {
		return 0;
	}
	new house, objidx,sqlid;
	if(findHouseFurnByObjID(objectid, house, objidx,sqlid) == 1) {
		if(playerOwnsHouse(house, playerid) || EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
			new inside = GetPlayerVirtualWorld(playerid) != 0;
			if(!isFurnitureInValidRange(house, x, y, z, inside) || response == EDIT_RESPONSE_CANCEL) {
				GetDynamicObjectPos(objectid, x, y, z);
				GetDynamicObjectRot(objectid, rx, ry, rz);
				SetDynamicObjectPos(objectid, x, y, z);
				SetDynamicObjectRot(objectid, rx, ry, rz);
				if(response != EDIT_RESPONSE_CANCEL)
					SendClientMessage(playerid, X11_TOMATO_2, "This object is too far!");
				else {
					SendClientMessage(playerid, X11_TOMATO_2, "Object Selection cancelled!");
				}
				return 1;
			} 
			SetDynamicObjectPos(objectid, x, y, z);
			SetDynamicObjectRot(objectid, rx, ry, rz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Position Saved!");
			format(query, sizeof(query), "UPDATE `housefurniture` SET `X` = %f, `Y` = %f, `Z` = %f, `RX` = %f, `RY` = %f, `RZ` = %f WHERE `id` = %d",x,y,z,rx,ry,rz,sqlid);
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You must own this house to edit this!");
		}
	}
	return 0;
}
findHouseFurnByObjID(objid, &house = 0, &objidx = 0, &sqlid = 0) {
	for(new i=0;i<MAX_HOUSES;i++) {
		for(new x=0;x<MAX_HOUSE_ACCESSORIES;x++) {
			if(HouseObjects[i][x][EHouseObjectObjID] == objid) {
				sqlid = HouseObjects[i][x][EHouseObjectSQLID];
				house = i;
				objidx = x;
				return 1;
			}
		}
	}
	return 0;
}
setupFurnitureMenuState(playerid, curpage, index) {
	new remaining_amount, pages, numobjs;
	new start_index = 0;
	new temptxt[128];

	new vehstr[32],vehprice[32];


	for(new i=0,c=0,j=1;i<sizeof(BuyableHouseObjects);i++) {
		if(BuyableHouseObjects[i][EHouseObjectCategory] == index) {
			if(c++ >= (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS)*j) {
				if(++j == curpage)
					start_index = i;
			}
			numobjs++;
		}
	}

	clearModelSelectMenu(playerid);
	if(curpage < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}

	for(new i=start_index,x=0;x<numobjs&&i<sizeof(BuyableHouseObjects);i++) {
		if(BuyableHouseObjects[i][EHouseObjectCategory] == index) {
			format(vehstr,sizeof(vehstr),"%s",BuyableHouseObjects[i][EHouseObjectName]);
			format(vehprice,sizeof(vehprice), "~g~%s tokens",getNumberString(BuyableHouseObjects[i][EHouseObjectPrice]));
			
			addItemToModelSelectMenu(playerid, BuyableHouseObjects[i][EHouseObjectID], vehprice,vehstr);
			x++;
		}
	}

	remaining_amount = numobjs % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numobjs) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	FurnitureMenuState[playerid][EFSMS_CurPage] = curpage;
	FurnitureMenuState[playerid][EFSMS_PageCount] = pages;
	FurnitureMenuState[playerid][EFSMS_RemainingCount] = remaining_amount;
	FurnitureMenuState[playerid][EFSMS_InPreviewMode] = 0;
	FurnitureMenuState[playerid][EFSMS_MenuIndex] = index;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numobjs, "Furniture Menu", page_str, "OnHouseFurnitureMenuSelect", 0.0, 0.0, 0.0);
}
forward OnHouseFurnitureMenuSelect(playerid, action, modelid, title[]);
public OnHouseFurnitureMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			FurnitureMenuState[playerid][EFSMS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		new Float:X, Float:Y, Float:Z, interior/*, vw*/;
		GetPlayerPos(playerid, X, Y, Z);
		Y += 2.0;
		//vw = GetPlayerVirtualWorld(playerid);
		interior = GetPlayerInterior(playerid);
		new houseid;
		houseid = getStandingHouse(playerid,HOUSEFURN_OUTSIDE_DIST);
		if(houseid == -1) {
			houseid = getStandingExit(playerid,HOUSEFURN_INSIDE_DIST);
		}
		if(houseid == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be inside or near your house");
			return 1;
		}
		houseid = getHouseSQLID(houseid);
		new tokens = GetPVarInt(playerid, "FurnitureTokens");
		if(houseid == -1) return 1;
		new obj = findHouseFurnObjectByCategoryModelID(FurnitureMenuState[playerid][EFSMS_MenuIndex], FurnitureMenuState[playerid][EFSMS_InPreviewMode]);
		if(obj != -1) {
			if(BuyableHouseObjects[obj][EHouseObjectInside] == 1 && interior == 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can only place this object inside!");
				return 1;
			}
			if(tokens < BuyableHouseObjects[obj][EHouseObjectPrice]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough tokens!");
				return 1;
			}
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
			new donateRank = GetPVarInt(playerid, "DonateRank");
			if(donateRank < BuyableHouseObjects[obj][EHouseObjectDonRank]) {
				format(tempstr, sizeof(tempstr), "You don't have the required donate rank to get this, this is for %s donators and above!", GetDonateRank(BuyableHouseObjects[obj][EHouseObjectDonRank]));
				SendClientMessage(playerid, X11_TOMATO_2, tempstr);
				return 1;
			}
			tokens -= BuyableHouseObjects[obj][EHouseObjectPrice];
			SetPVarInt(playerid, "FurnitureTokens", tokens);
			format(tempstr, sizeof(tempstr), "* Congratulations, you have bought a \"%s\". Use /editfurniture to move it!",BuyableHouseObjects[obj][EHouseObjectName]);
			SendClientMessage(playerid, COLOR_DARKGREEN, tempstr);
			CreateHouseItem(houseid, obj, X, Y, Z, interior!=0?1:0);
		}
	} else if(action == EModelPreviewAction_Next) {
		setupFurnitureMenuState(playerid, FurnitureMenuState[playerid][EFSMS_CurPage] + 1, FurnitureMenuState[playerid][EFSMS_MenuIndex]);
	} else if(action == EModelPreviewAction_Back) {
		setupFurnitureMenuState(playerid, FurnitureMenuState[playerid][EFSMS_CurPage]- 1, FurnitureMenuState[playerid][EFSMS_MenuIndex]);
	} else if(action == EModelPreviewAction_Exit) {
		if(FurnitureMenuState[playerid][EFSMS_InPreviewMode] != 0) {
			setupFurnitureMenuState(playerid, FurnitureMenuState[playerid][EFSMS_CurPage], FurnitureMenuState[playerid][EFSMS_MenuIndex]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
}