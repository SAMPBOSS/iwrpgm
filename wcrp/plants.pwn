enum EPlantFlags (<<= 1) {
	EPlantFlags_Watered = 1,
	EPlantFlags_Fertlized,
	EPlantFlags_Diseased,
	EPlantFlags_RunOver, //walked over by a person or a car
	EPlantFlags_Dying, //turn material yellow and slowly die over time
};
enum EPlantType {
	EPlantType_Pot,
	EPlantType_Coke,
	EPlantType_Opium,
	EPlantType_Flowers,
	EPlantType_Hemlock,
};
enum pPlantInfo
{
	pPlantSQLID,
	Float:pOriZ,//original Z value
	pOwnerSQLID,
	pOwner[MAX_PLAYER_NAME],
	pPlantTime,//time in seconds when it was planted
	pPlantStage, //stage in plants development
	pPlantObjectID,
	EPlantFlags:pPlantFlags,
	pPlantType, //Stores the SQLID of the drug
	pPlantInt,
	pPlantVW,
	Text3D:pPlantText,
};
#define MAX_PLANTS 2500
new PlantInfo[MAX_PLANTS][pPlantInfo];

YCMD:plantdrug(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to plant a specific drug");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	new type[32], msg[128], index, pvarid, amount;
	new Float:X, Float:Y, Float:Z;
	if(!sscanf(params, "s[32]", type)) {
		new plant = getStandingPlant(playerid, 1.0);
		index = findDrugIndexByName(playerid, type, pvarid);
		if(plant != -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't place this on top of another plant!");
			return 1;
		}
		if(index != -1) {
			if(Drugs[index][EDrugType] != DrugType_Plant) {
				SendClientMessage(playerid, X11_TOMATO_2, "This is not a plant so there's no seeds in it.");
				return 1;
			}
			amount = RandomEx(1, 4);
			GetPlayerPos(playerid, X, Y, Z);
			format(msg, sizeof (msg), "You've used %dg(s) of %s to plant this.", amount, Drugs[index][EDrugName]);
			SendClientMessage(playerid, X11_WHITE, msg);
			setDrugPVar(playerid, index, pvarid, amount, EDrugs_Type_Decrease);
			placePlant(playerid, Drugs[index][EDrugSQLID], X,Y,Z);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have any more of that drug so you can't plant it!");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "[USAGE]: /plantdrug <part of drug name>");
		return 1;
	}
	return 1;
}
YCMD:plantinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Show information on a plant");
		return 1;
	}
	new plant = getStandingPlant(playerid);
	if(plant == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a plant");
		return 1;
	}
	new msg[128];
	format(msg, sizeof(msg), "Type: %s", getPlantName(plant));
	SendClientMessage(playerid, COLOR_YELLOW, msg);
	format(msg, sizeof(msg), "Plant ID: %d SQL ID: %d", plant, PlantInfo[plant][pPlantSQLID]);
	SendClientMessage(playerid, COLOR_YELLOW, msg);
	format(msg, sizeof(msg), "Owner: %s(%d)", PlantInfo[plant][pOwner], PlantInfo[plant][pOwnerSQLID]);
	SendClientMessage(playerid, COLOR_YELLOW, msg);
	format(msg, sizeof(msg), "Plant Stage: %d", PlantInfo[plant][pPlantStage]);
	SendClientMessage(playerid, COLOR_YELLOW, msg);
	return 1;
}
YCMD:plantdestroy(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Show information on a plant");
		return 1;
	}
	new plant = getStandingPlant(playerid);
	if(plant == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a plant");
		return 1;
	}
	DestroyPlant(plant);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Plant Destroyed!");
	return 1;
}
getPlantByObjID(objid) {
	for(new i=0;i<sizeof(PlantInfo);i++) {
		if(PlantInfo[i][pPlantObjectID] == objid) {
			return i;
		}
	}
	return -1;
}

#pragma unused getPlantByObjID

plantsOnGameModeInit() {
	loadPlants();
}
loadPlants() {
	mysql_function_query(g_mysql_handle, "SELECT `plants`.`id`,`planter`,`plants`.`x`,`plants`.`y`,`plants`.`z`,Unix_Timestamp(`planttime`),`flags`,`username`,`stage`,`plants`.`type`, `plants`.`vw`, `plants`.`interior` FROM `plants` INNER JOIN `characters` ON `characters`.`id` = `plants`.`planter`", true, "OnLoadPlants", "");
}
forward OnLoadPlants();
public OnLoadPlants() {
	new rows, fields;
	new id_string[128], plantdesc[128];
	new index;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {

		index = findFreePlant();
		cache_get_row(i, 0, id_string);
		PlantInfo[index][pPlantSQLID] = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		PlantInfo[index][pOwnerSQLID] = strval(id_string);
		
		new Float:X, Float:Y, Float:Z;
		
		cache_get_row(i, 2, id_string);
		X = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		Y = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		Z = floatstr(id_string);
		
		PlantInfo[index][pOriZ] = Z;
		
		cache_get_row(i, 5, id_string);
		PlantInfo[index][pPlantTime] = strval(id_string);
		
		cache_get_row(i, 6, id_string);
		PlantInfo[index][pPlantFlags] = EPlantFlags:strval(id_string);
		
		cache_get_row(i, 7, PlantInfo[index][pOwner]);
		
		cache_get_row(i, 8, id_string);
		PlantInfo[index][pPlantStage] = strval(id_string);
		
		cache_get_row(i, 9, id_string);
		PlantInfo[index][pPlantType] = strval(id_string);
		
		cache_get_row(i, 10, id_string);
		PlantInfo[index][pPlantVW] = strval(id_string);

		cache_get_row(i, 11, id_string);
		PlantInfo[index][pPlantInt] = strval(id_string);

		Z += getPlantLength(index);
		PlantInfo[index][pPlantObjectID] = CreateDynamicObject(19473, X, Y, Z, 0.0, 0.0, 0.0, PlantInfo[index][pPlantVW], PlantInfo[index][pPlantInt]);
		
		format(plantdesc, sizeof(plantdesc), "{%s}[{%s}Plant{%s}]\n Name: %s\nStage: %d", getColourString(X11_WHITE),getColourString(COLOR_DARKGREEN),getColourString(X11_WHITE), getPlantName(index), PlantInfo[index][pPlantStage]);
		PlantInfo[index][pPlantText] = CreateDynamic3DTextLabel(plantdesc, 0x00FF00AA, X, Y, Z+2, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, PlantInfo[index][pPlantVW], PlantInfo[index][pPlantInt]);
		//`plants`.`id`,`planter`,`plants`.`x`,`plants`.`y`,`plants`.`z`,Unix_Timestamp(`planttime`),`flags`,`username`
	}
}
YCMD:harvest(playerid, params[], help) {
	new plant = getStandingPlant(playerid);
	if(plant == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a plant");
		return 1;
	}
	new maxp = 4;
	if(PlantInfo[plant][pPlantStage] < maxp) {
		SendClientMessage(playerid, X11_TOMATO_2, "This plant is not ready to harvest!");
		return 1;
	}
	playerPickPlant(playerid, plant);
	return 1;
}
YCMD:fertilize(playerid, params[], help) {
	new amount = GetPVarInt(playerid, "NumFertilizer");
	if(amount < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have any fertilizer");
		return 1;
	}
	amount--;
	SetPVarInt(playerid, "NumFertilizer", amount);
	new plant = getStandingPlant(playerid);
	if(plant == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a plant");
		return 1;
	}
	fertilizePlant(plant);
	SendClientMessage(playerid, X11_TOMATO_2, "Plant Fertilized");
	return 1;
}
YCMD:gotoplant(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a plant");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	new plant;
	if(!sscanf(params,"d",plant)) {
		if(plant < 0 || plant > sizeof(PlantInfo)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Plant!");
			return 1;
		}
		GetDynamicObjectPos(PlantInfo[plant][pPlantObjectID], X, Y, Z);
		SetPlayerPos(playerid, X, Y, Z);
		SetPlayerVirtualWorld(playerid, PlantInfo[plant][pPlantVW]);
		SetPlayerInterior(playerid, PlantInfo[plant][pPlantInt]);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported");
	}
	return 1;
}
YCMD:stageplant(playerid, params[], help) {
	new id;
	if(!sscanf(params, "d", id)) {
		StagePlantUp(id);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /stageplant [id]");
	}
	return 1;
}
placePlant(playerid, type, Float:X, Float:Y, Float:Z) {
	ApplyAnimation(playerid, "BOMBER","BOM_Plant_In",4.0,0,0,0,0,0);
	Z -= 2.5;
	
	format(query, sizeof(query), "INSERT INTO `plants` (`planter`,`x`,`y`,`z`,`type`,`vw`,`interior`) VALUES (%d,%f,%f,%f,%d,%d,%d)",GetPVarInt(playerid, "CharID"),X,Y,Z,type,GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));
	mysql_function_query(g_mysql_handle, query, true, "OnPlacePlant", "ddfffdd",playerid,type,X,Y,Z,GetPlayerVirtualWorld(playerid),GetPlayerInterior(playerid));
}
forward OnPlacePlant(playerid, type,Float:X, Float:Y, Float:Z, vw, interior);
public OnPlacePlant(playerid, type,Float:X, Float:Y, Float:Z, vw, interior) {
	new index = findFreePlant();
	new id = mysql_insert_id();
	new msg[128];
	if(index == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Maximum plants reached");
		format(msg, sizeof(msg), "DELETE FROM `plants` WHERE `id` = %d",id);
		mysql_function_query(g_mysql_handle, msg, false, "EmptyCallback", "");
		return -1;
	}
	PlantInfo[index][pOwnerSQLID] = id;
	PlantInfo[index][pPlantTime] = gettime();
	PlantInfo[index][pOriZ] = Z;
	PlantInfo[index][pPlantType] = type;
	PlantInfo[index][pPlantFlags] = EPlantFlags:0;
	PlantInfo[index][pPlantStage] = 0;
	format(PlantInfo[index][pOwner],MAX_PLAYER_NAME,"%s",GetPlayerNameEx(playerid, ENameType_CharName));
	PlantInfo[index][pOwnerSQLID] = GetPVarInt(playerid, "CharID");
	PlantInfo[index][pPlantObjectID] = CreateDynamicObject(19473, X, Y, Z, 0.0, 0.0, 0.0, vw, interior);
	PlantInfo[index][pPlantVW] = vw;
	PlantInfo[index][pPlantInt] = interior;

	format(msg, sizeof(msg), "{%s}[{%s}Plant{%s}]\n Name: %s\nStage: %d", getColourString(X11_WHITE),getColourString(COLOR_DARKGREEN),getColourString(X11_WHITE), getPlantName(index), PlantInfo[index][pPlantStage]);
	PlantInfo[index][pPlantText] = CreateDynamic3DTextLabel(msg, 0x00FF00AA, X, Y, Z+2, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, PlantInfo[index][pPlantVW], PlantInfo[index][pPlantInt]);

	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		format(msg, sizeof(msg), "Plant ID: %d SQL ID: %d",index,id);
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	}
	return 0;
}

findFreePlant() {
	for(new i=0;i<sizeof(PlantInfo);i++) {
		if(PlantInfo[i][pPlantObjectID] == 0) {
			return i;
		}
	}
	return -1;
}
Float:getPlantLength(i) {
	new Float:Z;
	new age = PlantInfo[i][pPlantStage];
	Z = 0.2*age;
	return Z;
}
StagePlantUp(id) {
	new Float:X, Float:Y, Float:Z, msg[128];
	GetDynamicObjectPos(PlantInfo[id][pPlantObjectID], X, Y, Z);
	if(PlantInfo[id][pPlantStage]++ < 6) {
		Z = PlantInfo[id][pOriZ] + getPlantLength(id);
		SetDynamicObjectPos(PlantInfo[id][pPlantObjectID], X, Y, Z);
		format(msg, sizeof(msg), "{%s}[{%s}Plant{%s}]\n Name: %s\nStage: %d", getColourString(X11_WHITE),getColourString(COLOR_DARKGREEN),getColourString(X11_WHITE), getPlantName(id), PlantInfo[id][pPlantStage]);
		UpdateDynamic3DTextLabelText(PlantInfo[id][pPlantText],X11_WHITE,msg);
	} else {
		setPlantDead(id);
	}
	if(PlantInfo[id][pPlantStage] > 24) {
		DestroyPlant(id);
	}
	savePlant(id);
}
setPlantDead(id, color = 0) {
	PlantInfo[id][pPlantFlags] |= EPlantFlags_Dying;
	SetDynamicObjectMaterial(PlantInfo[id][pPlantObjectID], 0, 856, "gta_proc_ferns","veg_bush2", color);
	SetDynamicObjectMaterial(PlantInfo[id][pPlantObjectID], 1, 856, "gta_proc_ferns","veg_bush2", color);
}
getStandingPlant(playerid, Float:radi = 5.0) {
	new Float:X, Float:Y, Float:Z;
	for(new i=0;i<sizeof(PlantInfo);i++) {
		if(PlantInfo[i][pPlantObjectID] != 0) {
			GetDynamicObjectPos(PlantInfo[i][pPlantObjectID], X, Y, Z);
			if(GetPlayerVirtualWorld(playerid) == PlantInfo[i][pPlantVW] && GetPlayerInterior(playerid) == PlantInfo[i][pPlantInt]) {
				if(IsPlayerInRangeOfPoint(playerid, radi, X, Y, Z)) {
						return i;
				}
			}
		}
	}
	return -1;
}
playerPickPlant(playerid, plant) {
	new turnout;
	turnout = RandomEx(1,25); //max pot from a single plant
	DestroyPlant(plant);
	if(PlantInfo[plant][pPlantFlags] & EPlantFlags_Diseased) {
		turnout -= RandomEx(0, floatround(turnout/2));
		if(turnout < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "This plant is heavily diseased, there was no useable turnout");
			return 1;
		}
	}
	/*
	if(~PlantInfo[i][pPlantFlags] & EPlantFlags_Watered) {
		SendClientMessage(playerid, X11_TOMATO_2, "You didn't water the plant! It died because of this.");
		return 1;
	}
	*/
	if(PlantInfo[plant][pPlantFlags] & EPlantFlags_Fertlized) {
		turnout *= 2;
	}
	new msg[128];
	format(msg, sizeof(msg), "You got %d grams of %s from the plant", turnout,getPlantName(plant));
	SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	//pPlantType contains the leading SQLID of the drug they planted
	new pvarid = getUsableDrugSpot(playerid, PlantInfo[plant][pPlantType], STATE_ANYTHING);
	if(pvarid != -1) {
		format(msg, sizeof(msg), "%dgs of %s have been added to your inventory.", turnout, getPlantName(plant));
		SendClientMessage(playerid, X11_WHITE, msg);
		setDrugPVar(playerid, getDrugIDBySQLID(PlantInfo[plant][pPlantType]), pvarid, turnout, EDrugs_Type_Increase); //The drug index and the pvarid (0, 1, 2) that we got from the getUsableDrugSpot function
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have any slots to store the drugs on, get rid of some of them.");
	}
	return 1;
}
stock getPlantName(plant) {
	new index, xret[32];
	index = getDrugIDBySQLID(PlantInfo[plant][pPlantType]);
	if(index != -1) {
		strcpy(xret, Drugs[index][EDrugName]);
	}
	return strlen(xret) != 0 ? (xret) : ("None");
}
DestroyAllPlantsByDrugSQLID(sqlid) {
	for(new i=0; i<sizeof(PlantInfo); i++) {
		if(PlantInfo[i][pPlantType] == sqlid) {
			DestroyPlant(i);
		}
	}
	return 1;
}
DestroyPlant(plant) {
	DestroyDynamic3DTextLabel(PlantInfo[plant][pPlantText]);
	DestroyDynamicObject(PlantInfo[plant][pPlantObjectID]);
	PlantInfo[plant][pPlantObjectID] = 0;
	new msg[128];
	format(msg, sizeof(msg), "DELETE FROM `plants` WHERE `id` = %d",PlantInfo[plant][pPlantSQLID]);
	mysql_function_query(g_mysql_handle, msg, false, "EmptyCallback", "");
}
fertilizePlant(plant) {
	PlantInfo[plant][pPlantFlags] |= EPlantFlags_Fertlized;
}

savePlant(plant) {
	format(query, sizeof(query), "UPDATE `plants` SET `stage` = %d, `flags` = %d WHERE `id` = %d",PlantInfo[plant][pPlantStage],_:PlantInfo[plant][pPlantFlags],PlantInfo[plant][pPlantSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
plantsOnPayday() {
	new time = gettime();
	for(new i=0;i<sizeof(PlantInfo);i++) {
		if(PlantInfo[i][pPlantObjectID] != 0) {
			if(time-PlantInfo[i][pPlantTime] > 1800) {
				StagePlantUp(i);
			}
		}
	}
}