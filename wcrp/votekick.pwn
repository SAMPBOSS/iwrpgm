new VoteKickTimer = -1;
new VoteKickInfoTimer = -1;
new VoteKickType = -1;
new VoteKickTarget = INVALID_PLAYER_ID;
new VoteKickReason[64];
new VoteStarter = INVALID_PLAYER_ID;
new Votes[2]; //0 = no, 1 = yes
new Voted[MAX_PLAYERS];
new LastVote;

#define VOTE_BAN_TIME 7200 //2 hours in seconds
#define VOTE_INFO_TIME 30000 //30 secs ms
#define VOTE_TIME 120000 //2 mins ms
#define VOTE_COOLDOWN 300

#pragma unused VoteStarter

forward ShowVoteStats();
forward FinishVoting();

voteKickOnPlayerDisconnect(playerid, reason) {
	if(reason != 3) {
		if(VoteKickTarget == playerid) {
			new msg[128];
			format(msg, sizeof(msg), "* Vote: %s has left the server, vote aborted.",GetPlayerNameEx(playerid, ENameType_CharName));
			SendGlobalAdminMessage(COLOR_LIGHTRED, msg);
			StopVoting();
		}
		Voted[playerid] = 0;
	}
}
CanDoVoting() {
	new timenow = gettime();
	if(REQUESTCHAT_COOLDOWN-(timenow-LastVote) > 0) {
		return 0;
	}
	if(VoteKickInfoTimer != -1) return 0;
	return 1;
}
YCMD:votekick(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for votekicking players");
		return 1;
	}
	if(GetPVarInt(playerid, "ConnectTime") < 100) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be 100 connect time to use this command.");
		return 1;
	}
	if(NumAdminsOnline() != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "An admin is online, use /report instead");
		return 1;
	}
	if(!CanDoVoting()) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait, a vote has happend recently");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned.");
		return 1;
	}
	new msg[128],reason[64];
	new target;
	if(!sscanf(params, "k<playerLookup_acc>s[64]", target,reason)) {
		if(playerid == target) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot vote kick yourself");
			return 1;
		}
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		format(msg, sizeof(msg), "SYSTEM: %s has started a votekick on %s: %s (( /vote yes/no ))",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(target, ENameType_CharName), reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, msg);
		StartVoting(target, reason, 1);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /votekick [playerid/name] [reason]");
	}
	return 1;
}
YCMD:voteban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for votekicking players");
		return 1;
	}
	if(GetPVarInt(playerid, "ConnectTime") < 500) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be 500 connect time to use this command.");
		return 1;
	}
	if(NumAdminsOnline() != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "An admin is online, use /report instead");
		return 1;
	}
	if(!CanDoVoting()) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait, a vote has happend recently");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned.");
		return 1;
	}
	new msg[128],reason[64];
	new target;
	if(!sscanf(params, "k<playerLookup_acc>s[64]", target,reason)) {
		if(playerid == target) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot vote ban yourself");
			return 1;
		}
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		format(msg, sizeof(msg), "SYSTEM: %s has started a voteban on %s: %s (( /vote yes/no ))",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(target, ENameType_CharName), reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, msg);
		StartVoting(target, reason, 2);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /votekick [playerid/name] [reason]");
	}
	return 1;
}
YCMD:stopvote(playerid, params[], help) {
	StopVoting();
	SendClientMessage(playerid, X11_TOMATO_2, "Voting stopped.");
	return 1;
}
YCMD:vote(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Casts a vote on the vote kick/ban");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are vote banned.");
		return 1;
	}
	new type[6];
	if(Voted[playerid] == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have already voted!");
		return 1;
	}
	if(VoteKickTarget == INVALID_PLAYER_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "A vote is not active.");
		return 1;
	}
	if(VoteKickTarget == playerid) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot vote, you are the target.");
		return 1;
	}
	if(GetPVarInt(playerid, "ConnectTime") < 50) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be above 50 playing hours to use this command.");
		return 1;
	}
	if(!sscanf(params,"s[6]",type)) {
		if(!strcmp(type, "yes", true)) {
			Votes[1]++;
		} else if(!strcmp(type, "no", true)) {
			Votes[0]++;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vote Type");
			SendClientMessage(playerid, X11_TOMATO_2, "Valid vote types are: No, Yes");
			return 1;
		}
		SendClientMessage(playerid, X11_TOMATO_2, "* Vote Submitted");
		Voted[playerid] = 1;
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /vote [yes/no]");
	}
	return 1;
}
StartVoting(target, reason[], type) {
	format(VoteKickReason, sizeof(VoteKickReason), "%s", reason);
	VoteKickInfoTimer = SetTimer("ShowVoteStats", VOTE_INFO_TIME, true);
	VoteKickTimer = SetTimer("FinishVoting", VOTE_TIME, false);
	VoteKickType = type;
	VoteKickTarget = target;
}

public FinishVoting() {
	new msg[128];
	format(msg, sizeof(msg), "* Vote results: %d yes, %d no",Votes[1],Votes[0]);
	new votetype[16];
	if(VoteKickType == 1) {
		strmid(votetype,"Kick",0, 4, sizeof(votetype));
	} else if(VoteKickType == 2) {
		strmid(votetype,"Ban",0, 3, sizeof(votetype));
	}
	SendGlobalAdminMessage(X11_ORANGE, msg);
	if(Votes[1] > Votes[0] && GetPVarInt(VoteKickTarget, "AdminFlags") == 0) { 
		format(msg, sizeof(msg), "* Vote %s successful. %s has been removed from the server.",votetype,GetPlayerNameEx(VoteKickTarget, ENameType_CharName));
		if(VoteKickType == 1) {
			KickEx(VoteKickTarget, "Vote Kicked");
		} else if(VoteKickType == 2) {
			BanPlayer(VoteKickTarget, "Vote Banned", -1, false, VOTE_BAN_TIME);
		}
	} else {
		format(msg, sizeof(msg), "* Vote %s failed.",votetype);
	}
	LastVote = gettime();
	SendGlobalAdminMessage(X11_ORANGE_2, msg);
	StopVoting();
}
StopVoting() {

	KillTimer(VoteKickInfoTimer);
	KillTimer(VoteKickTimer);
	
	VoteKickTimer = -1;
	VoteKickInfoTimer = -1;
	VoteKickType = -1;
	VoteKickTarget = INVALID_PLAYER_ID;
	VoteStarter = INVALID_PLAYER_ID;
	
	Votes[0] = 0;
	Votes[1] = 0;
	for(new i=0;i<MAX_PLAYERS;i++) {
		Voted[i] = 0;
	}
}
public ShowVoteStats() {
	new msg[128];
	new votetype[16];
	if(VoteKickType == 1) {
		strmid(votetype,"Kick",0, 4, sizeof(votetype));
	} else if(VoteKickType == 2) {
		strmid(votetype,"Ban",0, 3, sizeof(votetype));
	}
	format(msg, sizeof(msg), "* A Vote %s is in session, current votes: %d yes, %d no",votetype,Votes[1],Votes[0]);
	SendGlobalAdminMessage(X11_ORANGE, msg);
	format(msg, sizeof(msg), "* Vote Target: %s, Reason: %s",GetPlayerNameEx(VoteKickTarget, ENameType_CharName),VoteKickReason);
	SendGlobalAdminMessage(X11_ORANGE_1, msg);
	SendGlobalAdminMessage(X11_ORANGE_2, "* Type /vote yes, or /vote no to vote!");
}