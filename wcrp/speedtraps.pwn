new LastSpeedTrapTicket[MAX_PLAYERS];
enum ESpeedTrapInfo {
	ESpeedTrapName[32],
	Float:ESpeedTrapX,
	Float:ESpeedTrapY,
	Float:ESpeedTrapZ,
	Float:ESpeedTrapRX,
	Float:ESpeedTrapRY,
	Float:ESpeedTrapRZ,
	Float:ESpeedTrapMaxSpeed,
	ESpeedTrapTicketPrice, //+ percentage over limit
	Float:ESpeedTrapRadius,
	ESpeedTrapObjectID,
	Text3D:ESpeedTrapText,
};

new SpeedTraps[][ESpeedTrapInfo] = {
	{"Union Station",1815.40527, -1818.54895, 12.69509,   0.00000, 0.00000, 0.00000,70.0, 800, 35.0},
	{"Skate Park",1857.73633, -1407.80664, 12.53913,   0.00000, 0.00000, 182.33972, 90.0, 800, 35.0},
	{"Compton Gym", 2283.60718, -1727.00256, 12.81537,   0.00000, 0.00000, -96.96002, 100.0, 800, 35.0},
	{"Jefferson 1",2061.24194, -1200.00244, 22.74307,   0.00000, 0.00000, 0.00000, 90.0, 800, 35.0},
	{"Bank", 1619.88098, -1155.19617, 22.94629,   0.00000, 0.00000, -90.84000, 90.0, 800, 35.0},
	{"Commerce", 1340.78027, -1092.56409, 23.12203,   0.00000, 0.00000, -1.26000, 100.0, 1200, 85.0},
	{"Market",  1017.70972, -1389.88074, 12.26478,   0.00000, 0.00000, -87.41996, 120.0, 1600, 85.0},
	{"Pershing Square",  1528.13916, -1737.88721, 12.53719,   0.00000, 0.00000, 97.07999, 100.0, 1200, 85.0},
	{"Insurance", 2076.16113, -1871.28845, 12.53639,   0.00000, 0.00000, 0.00000, 90.0, 900, 50.0},
	{"East Los Angeles", 2861.71265, -1624.85229, 10.05111,   0.00000, 0.00000, -18.54000, 100.0, 1200, 50.0},
	{"Airport Highway",  2158.13428, -2422.42358, 12.54173,   0.00000, 0.00000, -17.40000, 100.0, 1200, 50.0},
	{"Hollywood", 1277.75989, -917.84918, 40.71056,   0.00000, 0.00000, -73.20000, 100.0, 1200, 50.0},
	{"Verona Beach", 694.66107, -1768.84668, 12.46514,   0.00000, 0.00000, 71.76000, 100.0, 1200, 50.0},
	{"Verdant Bluffs",1348.88684, -1867.78198, 12.53818,   0.00000, 0.00000, 64.68000, 100.0, 1200, 50.0}
};

#define SPEED_TRAP_COOLDOWN 20 //was 300
#define SPEED_DIFF_ALLOW 20 //amount of miles/km over limit before ticketing
SpeedTrapsOnGameModeInit() {
	new speedmsg[128];
	for(new i=0;i<sizeof(SpeedTraps);i++) {
		SpeedTraps[i][ESpeedTrapObjectID] = CreateDynamicObject(18880,SpeedTraps[i][ESpeedTrapX],SpeedTraps[i][ESpeedTrapY],SpeedTraps[i][ESpeedTrapZ],SpeedTraps[i][ESpeedTrapRX],SpeedTraps[i][ESpeedTrapRY],SpeedTraps[i][ESpeedTrapRZ]);
		format(speedmsg, sizeof(speedmsg), "Speed Camera\nLimit: {2641FE}%d km/h",floatround(SpeedTraps[i][ESpeedTrapMaxSpeed]));
		SpeedTraps[i][ESpeedTrapText] =  Create3DTextLabel(speedmsg, 0xFFFFFFFF,SpeedTraps[i][ESpeedTrapX],SpeedTraps[i][ESpeedTrapY],SpeedTraps[i][ESpeedTrapZ]+5.0, 40.0, 0, 0);
	}
}
SpeedTrapsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	LastSpeedTrapTicket[playerid] = 0;
}

GetSpeedTrapInArea(playerid) {
	for(new i=0;i<sizeof(SpeedTraps);i++) {
		if(IsPlayerInRangeOfPoint(playerid, SpeedTraps[i][ESpeedTrapRadius], SpeedTraps[i][ESpeedTrapX], SpeedTraps[i][ESpeedTrapY], SpeedTraps[i][ESpeedTrapZ])) {
			return i;
		}
	}
	return -1;
}
task CheckSpeedTraps[500] () {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPlayerState(i) == PLAYER_STATE_DRIVER) {
				new trap = GetSpeedTrapInArea(i);
				if(trap != -1) {
					new Float:speed = GetSpeed(i);
					if(speed > (SpeedTraps[trap][ESpeedTrapMaxSpeed]+SPEED_DIFF_ALLOW)) {
						if(PlayerIsInTheirCar(i))
							TrafficPoliceNotify(i, trap, speed);
					}
				}
			}
		}
	}
}
TrafficPoliceNotify(playerid, trap, Float:speed) {
	new msg[128];
	if(SPEED_TRAP_COOLDOWN-(gettime()-LastSpeedTrapTicket[playerid]) > 0) return 0;
	if(IsOnDuty(playerid) || GetPVarType(playerid, "MedicDuty") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	format(msg, sizeof(msg), "* The camera takes a snapshot of the vehicle's license plate and reports it to the authorities.");
	SendAreaMessage(60.0,SpeedTraps[trap][ESpeedTrapX],SpeedTraps[trap][ESpeedTrapY],SpeedTraps[trap][ESpeedTrapZ],0,msg,COLOR_PURPLE);
	format(msg, sizeof(msg), "HQ: Message from CCTV System: %d km/h in a %d km/h zone. Vehicle Name: %s. Location: %s",floatround(speed),floatround(SpeedTraps[trap][ESpeedTrapMaxSpeed]), VehiclesName[GetVehicleModel(carid)-400], SpeedTraps[trap][ESpeedTrapName]);
	SendCopMessage(TEAM_BLUE_COLOR, msg);
	LastSpeedTrapTicket[playerid] = gettime();
	return 1;
}
/*
GiveSpeedTrapTicket(playerid, trap, Float:speed) {
	new msg[64];
	if(SPEED_TRAP_COOLDOWN-(gettime()-LastSpeedTrapTicket[playerid]) > 0) return 0;
	if(IsOnDuty(playerid) || GetPVarType(playerid, "MedicDuty") != PLAYER_VARTYPE_NONE || GetPVarInt(playerid, "NumTickets") > 10) {
		return 1;
	}
	new price = floatround(SpeedTraps[trap][ESpeedTrapTicketPrice]/(SpeedTraps[trap][ESpeedTrapMaxSpeed]/speed));
	format(msg, sizeof(msg), "Speeding: %d km/h in a %d km/h zone",floatround(speed),floatround(SpeedTraps[trap][ESpeedTrapMaxSpeed]));
	issueTicket(playerid, -1, msg, price);
	LastSpeedTrapTicket[playerid] = gettime();
	return 1;
}
*/