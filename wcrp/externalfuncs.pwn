forward OOCOff(color, msg[]);
public OOCOff(color, msg[]) {
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			SendClientMessage(i, color, msg);
		}
	}
}
forward GetAdminLevel(playerid);
public GetAdminLevel(playerid) {
	//expects old style admin level, so lets convert!
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(aflags == EAdminFlags_BasicAdmin) return 1;
	if(aflags & EAdminFlags_Scripter) return 9999;
	if(aflags & EAdminFlags_ServerManager) return 1337;
	if(aflags != EAdminFlags_None) return 2;
	return 0;
}
forward GetMeth(playerid);
public GetMeth(playerid) {
	return GetPVarInt(playerid, "Meth");
}
forward GiveMeth(playerid, num);
public GiveMeth(playerid, num) {
	new meth = GetPVarInt(playerid, "Meth");
	SetPVarInt(playerid, "Meth", meth+num);
	return;
}
forward GetPot(playerid);
public GetPot(playerid) {
	return GetPVarInt(playerid, "Pot");
}
forward GivePot(playerid, num);
public GivePot(playerid, num) {
	SetPVarInt(playerid, "Pot", num+GetPVarInt(playerid, "Pot"));
	return;
}
forward GetMats(playerid, type);
public GetMats(playerid, type) {
	new pvarname[32];
	format(pvarname, sizeof(pvarname), "Mats%c",'A'+type);
	return GetPVarInt(playerid, pvarname);
}
forward GiveMats(playerid, type, num);
public GiveMats(playerid, type, num) {
	new pvarname[32];
	format(pvarname, sizeof(pvarname), "Mats%c",'A'+type);
	SetPVarInt(playerid, pvarname, GetPVarInt(playerid, pvarname)+num);
	return;
}
forward GiveCoke(playerid, num);
public GiveCoke(playerid, num) {
	SetPVarInt(playerid, "Coke", GetPVarInt(playerid, "Coke") + num);
	return;
}
forward GetCoke(playerid);
public GetCoke(playerid) {
	return GetPVarInt(playerid, "Coke");
}
forward GiveMoney(playerid, money);
public GiveMoney(playerid, money) {
	GiveMoneyEx(playerid, money);
}
forward GiveGun(playerid, gun, ammo);
public GiveGun(playerid, gun, ammo) {
	GivePlayerWeaponEx(playerid, gun, ammo);
}
forward GiveCar(playerid, carid, c1, c2, lock);
public GiveCar(playerid, carid, c1, c2, lock) {
	new retval = 1;
	new Float:X,Float:Y,Float:Z,Float:A;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	if(GetPlayerVirtualWorld(playerid) != 0 || GetPlayerInterior(playerid) != 0) {
		//place it at blueberry so it doesn't have to fall to its death
		X = 0.0;
		Y = 0.0;
		Z = 0.0;
		retval = 2; //indicate that the car isn't where its intended to be
	}
	CreatePlayerCar(playerid, carid, c1, c2, X, Y, Z, A, ELockType:lock);
	return retval;
}
forward NumFreeCars(playerid);
public NumFreeCars(playerid) {
	return GetPVarInt(playerid, "MaxCars") - getNumOwnedCars(playerid);
}
forward GetDPs(playerid);
public GetDPs(playerid) {
	return GetPVarInt(playerid, "DonatePoints");
}
forward GiveDPs(playerid, number);
public GiveDPs(playerid, number) {
	SetPVarInt(playerid, "DonatePoints", GetDPs(playerid) + number);
}