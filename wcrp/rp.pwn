/*
	PVars used in this script:
	UserFlags(int) - flags such as EUFHasDice
	PhoneStatus(int) - 0 = on, 1 = anonymous, 2 = off, 3 = PayPhone
	OnCall(int) - playerid - playerid of person you are talking to!
*/
enum (<<= 1) { //flags for different things the user has
	EUFHasDice = 1,
	EUFHasWalkieTalkie,
	EUFHasSpawned,
	EUFHasGPS,
	EUFHasMP3Player,
	EUFHasPhoneBook,
	EUFHasCarInsurance,
	EUFHasHealthInsurance,
	EUFHasMask, //unset on death
	EUFSpawnsAtHouse,
	EUFHasBackPack,
	EUFRPSInPrison,
};
enum {
	RP_ENormalAd,
	RP_ECompanyAd,
	RP_EBroadCastAd,
	RP_ENone,
}
enum E_Advertisements {
	E_DialogOptionText[128],
	E_BroadCastType,
	E_AdPrice,
	E_DoAdCallBack[128],
}
enum {
	ERP_BankChoose = ERPDialog_Base + 1,
	ERP_BankWithdraw,
	ERP_BankDeposit,
	ERP_BankTransfer,
	ERP_BankTransferName,
	ERP_SexOffer,
	ERP_FightOffer,
	ERP_BankTransferAmount,
	ERP_AdOptions,
	ERP_PostAd,
	ERP_AdShowAll,
	ERP_AdMsgBox,
	ERP_SearchAd,
};

#define MAX_RINGS 15 //maximum phone rings before call is aborted
#define DRUG_COOLDOWN 5
#define AD_COOLDOWN 120
#define AD_GLOBAL_COOLDOWN 30

new ringtimers[MAX_PLAYERS];

new bankpickup;
new lastad; //unix time of last ad
new Float:LastHealth[MAX_PLAYERS][2];

enum { //Jobs
	EJobType_None = -1,
	EJobType_TrashCleaner,
	EJobType_Drug_Dealer,
	EJobType_Arms_Dealer,
	EJobType_TaxiDriver,
	EJobType_Mercenary,
	EJobType_Mechanic,
	EJobType_Trucker,
	EJobType_Lawyer,
	EJobType_PizzaMan,
	EJobType_Crafter,
};

enum EATMInfo {
	Float:EATMInfoX,
	Float:EATMInfoY,
	Float:EATMInfoZ,
	Float:EATMInfoAngle,
	EATMInfoObjectID,
};

new Text3D:MaskLabels[MAX_PLAYERS];

//2942
new ATMs[][EATMInfo] = {
	{1750.370850, -1863.591919, 13.217603, 180.481705466, 0},
	{2139.389648, -1164.929321, 23.635086, 90.2409100289, 0},
	{1084.071167, -1817.118652, 13.305342, -89.3814160404, 0},
	{2404.129395, -1933.042236, 13.189775, 89.3814160404, 0},
	{1496.16443, -1749.90540, 15.06530, 180.00000, 0},
	{1928.58167, -1779.22620, 13.17470, 90.00000, 0},
	{2156.79956, -1733.11804, 13.25443, 1.57080, 0},
	{2233.25928, -1161.74805, 25.53060, -90.00000, 0},
	{2232.98584, -1343.25867, 23.61400, -90.00000, 0},
	{1350.33350, -1759.25916, 13.15160, 180.00000, 0},
	{1154.78699, -1460.91492, 15.43690, -90.00000, 0},
	{1102.21216, -1460.88464, 15.43690, 90.00000, 0},
	{1011.55371, -929.04718, 41.95280,  8.00000, 0},
	{1723.98486, -1637.08484, 19.83740, 180.00000, 0},
	{1789.22400, -1370.80920, 15.39780, -90.00000, 0},
	{2052.80713, -1897.55127, 13.17380, 1.57080, 0}
};

enum EGiveItems {
	EGiveName[32],
	EGivePVarName[10],
	EGiveShowName[64]
};
new GiveItems[][EGiveItems] = {{"Pot","Pot","%d grams of pot"},
								{"Coke","Coke","%d grams of coke"},
								{"Meth","Meth","%d grams of meth"},
								{"Gun","",""},
								{"Materials A","MatsA","%d materials"},
								{"Materials B","MatsB","%d materials"},
								{"Materials C","MatsC","%d materials"},
								{"Drugs","",""}
};

new Advertisements[][E_Advertisements] = {
	{"View Advertisements", RP_ENone, 0, "showAllAdvertisements"},
	{"Post an Advertisement", RP_ENormalAd, 500, "startPostAd"},
	{"Post a Company Advertisement", RP_ECompanyAd, 800, "startPostBizAd"},
	{"Find an Advertisement by Name", RP_ENone, 0, "startAdSearchProcess"},
	{"Post a Broadcaster Advertisement", RP_EBroadCastAd, 1000, "startBroadCasterAd"}
};

forward SendWalkieTalkieMessage(channel, color, msg[]);
forward StartRinging(caller, target);
isAtATM(playerid) {
	for(new i=0;i<sizeof(ATMs);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, ATMs[i][EATMInfoX], ATMs[i][EATMInfoY],ATMs[i][EATMInfoZ])) {
			return 1;
		}
	}
	return 0;
}

new Accents[][32] = {{"English"},{"French"},{"Russian"},{"Scottish"},{"Irish"},{"Spanish"},{"Southern"},{"Italian"},{"Australian"}
,{"Jamaican"},{"Israeli"},{"Dutch"},{"Brazilian"},{"Portuguese"},{"German"},{"Canadian"},{"Chinese"},{"Japanese"},{"Turkish"},{"Korean"},{"Estonian"},{"Sicilian"}};
YCMD:accent(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Change your accent");
		return 1;
	}
	new msg[128], accent;
	if(!sscanf(params, "d", accent)) {
		accent--;
		if(accent < 0 || accent > sizeof(Accents)) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Accent Removed!");
			DeletePVar(playerid, "Accent");
		} else {
			SetPVarString(playerid, "Accent", Accents[accent]);
			format(msg, sizeof(msg), "* Accent is now: %s",Accents[accent]);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /accent [accent]");
		query[0] = 0;
		strcat(query, "0. None ", sizeof(query));
		for(new i=0;i<sizeof(Accents);i++) {
			format(msg, sizeof(msg), "%d. %s ",i+1,Accents[i]);
			strcat(query, msg, sizeof(query));
			if(i%5 >= 4) {
				SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
				query[0] = 0;
			}
		}
		if(query[0] != 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
		}
	}
	return 1;
}
YCMD:withdraw(playerid, params[], help) {
	new withdraw;
	new string[50];
	new curfunds = GetPVarInt(playerid, "Bank");
	if(!isAtATM(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be at an ATM!");
		return 1;
	}
	if(sscanf(params, "d", withdraw)) {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /withdraw [amount]");
		return 1;
	}
	if(withdraw < 1 || withdraw > 10000) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
		return 1;
	}
	if(withdraw > curfunds) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
		return 1;
	}
	format(string, sizeof(string), "%s takes %d out from his bank account",GetPlayerNameEx(playerid, ENameType_CharName), withdraw);
	payLog(string);
	curfunds -= withdraw;
	SetPVarInt(playerid, "Bank", curfunds);
	GiveMoneyEx(playerid, withdraw);
	SendClientMessage(playerid, COLOR_GREENISHGOLD, "|___ BANK STATMENT ___|");
	format(string, sizeof(string), "  Old balance: $%s", getNumberString(curfunds));
	SendClientMessage(playerid, COLOR_DARKGREEN, string);
	format(string, sizeof(string), "  Cash withdrawn: $%s",getNumberString(withdraw));
	SendClientMessage(playerid, COLOR_DARKGREEN, string);
	format(string, sizeof(string), "  New balance: $%s", getNumberString(GetPVarInt(playerid, "Bank")));
	SendClientMessage(playerid, COLOR_DARKGREEN, string);
	SendClientMessage(playerid, COLOR_GREENISHGOLD, "|---------------------------------|");
	return 1;
}
forward StopTalking(playerid);
public StopTalking(playerid)
{
    ApplyAnimation(playerid, "CARRY", "crry_prtial", 2.0, 0, 0, 0, 0, 0);
}

RPOnPlayerText(playerid, text[]) {
	new string[256];
	new Float:sradius = 20.0;
	new carid;
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too weak to speak!");
		return 1;
	}
	if(playerHasGibs(playerid)) {
		GibsMessage(text);
	}
	if(GetPVarType(playerid, "911Call") != PLAYER_VARTYPE_NONE) {
		if(IsInCellPhoneJammerArea(playerid)) {
			hangupCall(playerid, 1);
		}
		carid = GetPlayerVehicleID(playerid);
		if(carid != 0 && carHasTrunk(carid)) {
			new windows = vehicleWindowsDown(carid);
			if(windows) {
				format(string, sizeof(string), "[Windows Down] %s says (cellphone): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
			} else {
				format(string, sizeof(string), "[Windows Up] %s says (cellphone): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
				sradius = 10.0;
			}
		} else {
			string = getCallingFromMessage(playerid, text);
		}
		SendCellPhoneToListeners(playerid, INVALID_PLAYER_ID, text);
		ProxMessage(sradius, playerid, string,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
		new stage = GetPVarInt(playerid, "911Call");
		switch(stage) {
			case 1: {
				if(!strcmp(text, "Police", true)) {
					SetPVarInt(playerid, "911Type", 1);
					SendClientMessage(playerid, COLOR_DBLUE, "Police HQ: Please give me a short description of the crime.");
				} else if(!strcmp(text, "Paramedic", true)) {
					SetPVarInt(playerid, "911Type", 2);
					SendClientMessage(playerid, COLOR_ALLDEPT, "Hospital: Please give me a short description of the Incident, and your location.");
				} else {
					SendClientMessage(playerid, COLOR_ALLDEPT, "Operator: I cannot understand you, do you want the Police, or Paramedic?");
					return 0;
				}
			}
			case 2: {
				if(GetPVarInt(playerid, "911Type") == 2) {
					SendClientMessage(playerid, COLOR_ALLDEPT, "Operator: Your message has been delivered to all on duty EMS members.");
					DeletePVar(playerid, "911Type");
					DeletePVar(playerid, "911Call");
					SetPlayerSpecialAction(playerid,SPECIAL_ACTION_STOPUSECELLPHONE);
					RemovePlayerAttachedObject(playerid, 9);
					format(query, sizeof(query), "EMS: %s",text);
					SendMedicMessage(COLOR_ALLDEPT, 1, query);
					format(query, sizeof(query), "EMS: Reporter: %s (%d)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPVarInt(playerid, "PhoneNumber"));
					SendMedicMessage(COLOR_ALLDEPT, 1, query);
					return 0;
				}
				SetPVarString(playerid, "911Description", text);
				SendClientMessage(playerid, COLOR_DBLUE, "Police HQ: Do you know the name of the person? Say their name, or say No if you don't");
			}
			case 3: {
				new desc[64];
				GetPVarString(playerid, "911Description", desc, sizeof(desc));
				if(!strcmp(text,"no", true)) {
					SendClientMessage(playerid, COLOR_DBLUE, "Police HQ: We have alerted all units in the area.");
					SendClientMessage(playerid, COLOR_DBLUE, "Thank you for reporting this crime");
					format(string, sizeof(string), "HQ: All Units APB: Reporter: %s (%d)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPVarInt(playerid, "PhoneNumber"));
					SendCopMessage(COLOR_DBLUE, string);
					format(string, sizeof(string), "HQ: Crime: %s, Suspect: Unknown",desc);
					SendCopMessage(COLOR_DBLUE, string);
					DeletePVar(playerid, "911Type");
					DeletePVar(playerid, "911Call");
					DeletePVar(playerid, "911Description");
					SetPlayerSpecialAction(playerid,SPECIAL_ACTION_STOPUSECELLPHONE);
					RemovePlayerAttachedObject(playerid, 9);
					return 0;
				} 
				new user = ReturnUser(text);
				if(!IsPlayerConnectEx(user)) {
					SendClientMessage(playerid, COLOR_DBLUE, "Police HQ: We couldn't hear you correctly, please repeat the name.");
					return 0;
				}
				SendClientMessage(playerid, COLOR_DBLUE, "Police HQ: We have alerted all units in the area.");
				SendClientMessage(playerid, COLOR_DBLUE, "Thank you for reporting this crime");
				format(string, sizeof(string), "HQ: All Units APB: Reporter: %s (%d)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask), GetPVarInt(playerid, "PhoneNumber"));
				SendCopMessage(COLOR_DBLUE, string);
				format(string, sizeof(string), "HQ: Crime: %s, Suspect: %s",desc,GetPlayerNameEx(user, ENameType_RPName_NoMask));
				SendCopMessage(COLOR_DBLUE, string);
			}
		}
		SetPVarInt(playerid, "911Call", ++stage);
		return 0;
	}
	if(GetPVarType(playerid, "OnCall") != PLAYER_VARTYPE_NONE) {
		if(IsInCellPhoneJammerArea(playerid)) {
			hangupCall(playerid, 1);
		}
		jailIfOOC(playerid, text);
		carid = GetPlayerVehicleID(playerid);
		if(carid != 0 && carHasTrunk(carid)) {
			new windows = vehicleWindowsDown(carid);
			if(windows) {
				format(string, sizeof(string), "[Windows Down] %s says (cellphone): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
				sradius = 15.0;
			} else {
				format(string, sizeof(string), "[Windows Up] %s says (cellphone): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
				sradius = 10.0;
			}
		} else {
			string = getCallingFromMessage(playerid, text);
		}
		ProxMessage(sradius, playerid, string,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
		string = getCallingFromMessage(playerid, text);
		if(GetPVarType(playerid, "OnAir") != PLAYER_VARTYPE_NONE) {
			new fid = GetPVarInt(playerid, "OnAir");
			format(string, sizeof(string), "[%s Caller] %s: %s",GetFactionName(fid), GetPlayerNameEx(playerid, ENameType_Phone_Name), text);
			NewsMessage(COLOR_NEWS, string);
			return 0;
		}
		new callerid = GetPVarInt(playerid, "OnCall");
		//SendClientMessage(callerid, COLOR_FADE1, string);
		ProxMessage(4.5, callerid, string, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
		SendCellPhoneToListeners(playerid, callerid, text);
		return 0;
	}
	if(GetPVarType(playerid, "GovChat") != PLAYER_VARTYPE_NONE) {
		new faction = GetPVarInt(playerid, "Faction");
		new rank = GetPVarInt(playerid, "Rank");
	    format(string, sizeof(string), "%s %s: %s", getFactionRankName(faction, rank), GetPlayerNameEx(playerid, ENameType_RPName_NoMask), text);
		Broadcast(TEAM_BLUE_COLOR, string);
		return 0;
	}
	if(GetPVarInt(playerid, "AdminDuty") != 0) {
		format(string, sizeof(string), "(( <%s> %s says: %s ))",getAdminName(playerid), GetPlayerNameEx(playerid, ENameType_RPName),text);
	} else {
		carid = GetPlayerVehicleID(playerid);
		if(carid != 0 && carHasWindows(carid)) {
			new windows = vehicleWindowsDown(carid);
			if(windows) {
				format(string, sizeof(string), "[Windows Down] %s%s says: %s",getAccentText(playerid), GetPlayerNameEx(playerid, ENameType_RPName),text);
			} else {
				format(string, sizeof(string), "[Windows Up] %s%s says: %s",getAccentText(playerid), GetPlayerNameEx(playerid, ENameType_RPName),text);
				sradius = 10.0;
			}
		} else {
			format(string, sizeof(string), "%s%s says: %s",getAccentText(playerid), GetPlayerNameEx(playerid, ENameType_RPName),text);
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
		if(!IsPlayerBlocked(playerid) && ~aflags & EAccountFlags_NoChatAnim) {
			new RandomStart;
			RandomStart = random(2);
			switch(RandomStart)
			{
				case 0:
				{
					ApplyAnimation(playerid,"PED","IDLE_CHAT",4.1,0,1,1,1,1);
				}
				case 1:
				{
					ApplyAnimation(playerid,"MISC", "Idle_Chat_02",4.1,0,1,1,1,1);
				}
			}
			new time=strlen(text)*100;
			SetTimerEx("StopTalking",time,0,"i",playerid);
		}
	}
	SetPlayerChatBubble(playerid, string, X11_WHITE, sradius, 5000);
	ProxMessage(sradius, playerid, string, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
	return 0;
}
getCallingFromMessage(playerid, text[]) {
	new string[128];
	if(!isCallingFromPayPhone(playerid)) {
		format(string, sizeof(string), "%s says (cellphone): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
	} else {
		format(string, sizeof(string), "%s says (Phone Booth): %s", GetPlayerNameEx(playerid, ENameType_Phone_Name),text);
	}
	return string;
}
RPOnGameModeInit() {
	Command_AddAltNamed( "windows", "wi");
	Command_AddAltNamed( "walkietalkie", "wt");
	Command_AddAltNamed( "walkietalkie", "walkitalki");
	Command_AddAltNamed( "ooc", "o");
	Command_AddAltNamed( "smoke", "usesmoke");
	Command_AddAltNamed( "shout", "s");
	Command_AddAltNamed( "whisper", "wisper");
	Command_AddAltNamed( "whisper", "w");
	Command_AddAltNamed( "close", "c");
	Command_AddAltNamed( "hangup", "h");
	Command_AddAltNamed("tunecolour", "tunecolor");
	Command_AddAltNamed("tunecolour", "tunecolors");
	Command_AddAltNamed("tunecolour", "tunecolours");
	Command_AddAltNamed("gate", "reception");
	Command_AddAltNamed("radio", "r");
	Command_AddAltNamed("family", "f");
	Command_AddAltNamed("animation", "an");
	Command_AddAltNamed("animation", "anim");
	Command_AddAltNamed("seatbelt", "sb");
	Command_AddAltNamed("government", "gov");
	Command_AddAltNamed("megaphone", "m");
	Command_AddAltNamed("requestchat", "endchat");
	Command_AddAltNamed("trunk", "boot");
	Command_AddAltNamed("hood", "bonnet");
	Command_AddAltNamed("taser", "tazer");
	Command_AddAltNamed("taser", "taze");
	Command_AddAltNamed("taser", "tase");
	Command_AddAltNamed("fuel", "fill");
	Command_AddAltNamed("department", "d");
	Command_AddAltNamed("pickup", "p");
	Command_AddAltNamed("newbie","n");
	Command_AddAltNamed("newb","n");
	Command_AddAltNamed("gunout","gout");
	Command_AddAltNamed("gunin","gin");
	Command_AddAltNamed("settings","tognews");
	Command_AddAltNamed("settings","togads");
	Command_AddAltNamed("settings","togad");
	Command_AddAltNamed("settings","toghints");
	Command_AddAltNamed("settings","togooc");
	Command_AddAltNamed("door","houselock");
	Command_AddAltNamed("door","hlock");
	Command_AddAltNamed("door","lockhouse");
	Command_AddAltNamed("door","lockh");
	Command_AddAltNamed("pickupitem","pickupgun");
	Command_AddAltNamed("factionlist","flist");
	Command_AddAltNamed("factionlist","cops");
	Command_AddAltNamed("trunkplayer","untrunkplayer");
	Command_AddAltNamed("number","phonebook");
	Command_AddAltNamed("l","low");
	Command_AddAltNamed("colourcodes","colorcodes");
	Command_AddAltNamed("suspect","su");
	Command_AddAltNamed("anim","an");
	Command_AddAltNamed("craftweapon","craftgun");
	Command_AddAltNamed("removespikestrip","rss");

	//UsePlayerPedAnims();
	for(new i=0;i<sizeof(ATMs);i++) {
		ATMs[i][EATMInfoObjectID] = CreateDynamicObject(2942, ATMs[i][EATMInfoX], ATMs[i][EATMInfoY], ATMs[i][EATMInfoZ], 0, 0, ATMs[i][EATMInfoAngle]);
	}
	//bankpickup = CreateDynamicPickup(1239, 16, 2310.480957, -8.286047, 26.742187); //pickup for bank (Old)

	bankpickup = CreateDynamicPickup(1239, 16, 26.5761, 9.4870, 500.3709);
	
	CreateDynamicPickup(1239,16, 1176.461059, -1323.873901, 14.022452);
	CreateDynamic3DTextLabel("(( /healme ))", 0x2BFF00AA, 1176.461059, -1323.873901, 14.022452 + 1, 30.0);
	
	CreateDynamicPickup(1239,16, 2029.497924, -1405.619995, 17.229307);
	CreateDynamic3DTextLabel("(( /healme ))", 0x2BFF00AA, 2029.497924, -1405.619995, 17.229307 + 1, 30.0);
	
	CreateDynamicPickup(1239,16, 2253.182617, 25.457563, 5.307678);
	CreateDynamic3DTextLabel("(( /ad ))", 0x2BFF00AA, 2253.182617, 25.457563, 5.307678 + 1, 30.0);
	
	
	
	new Hour, Minute, Second;
	gettime(Hour,Minute, Second);		
	new weather = random(7);
	SetWeather(weather);
	SetWorldTime(Hour);
	AllowInteriorWeapons(1);
}
YCMD:healme(playerid, params[], help) {
	if(!IsPlayerInRangeOfPoint(playerid, 3.0, 1176.461059, -1323.873901, 14.022452) && !IsPlayerInRangeOfPoint(playerid, 3.0, 2029.497924, -1405.619995, 17.229307)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the heal point!");
		return 1;
	}
	new msg[128];
	new price = 1500;
	new Float:HP;
	GetPlayerHealth(playerid, HP);
	if(HP > 95.0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't need healing!");
		return 1;
	}
	new insurance = hasHealthInsurance(playerid);
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(!playerDiseaseNoHealMe(playerid)) {
			new hprice = diseaseHealMeCost(disease);
			price += hprice;
			if(!diseaseInsured(disease)) {
				if(insurance) {
					SendClientMessage(playerid, X11_TOMATO_2, "This disease isn't covered under health insurance!");
					insurance = 0;
				}
			}
			if(insurance == 0) {
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
					return 1;
				}
			}
			curePlayerDisease(playerid);
		} else {
			if(!isPlayerDiseaseDorment(playerid)) {
				if(playerHasUncureableDisease(playerid)) {
					SendClientMessage(playerid, X11_TOMATO_2, "Your disease is uncureable.");
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You must see a doctor to cure your disease!");
				}
				
			}
		}
	}
	new cost = price;
	if(insurance == 0) {
		if(GetMoneyEx(playerid) < price) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
			return 1;
		}
		GiveMoneyEx(playerid, -price);
	}
	SendClientMessage(playerid, COLOR_GREENISHGOLD, "|___ MEDICAL BILL ___|");
	if(insurance) {
		SendClientMessage(playerid, COLOR_DARKGREEN, " Insurance: Yes");
		price = 0;
	} else {
		SendClientMessage(playerid, COLOR_DARKGREEN, " Insurance: No");
	}
	format(msg, sizeof(msg), " Cost: $%s", getNumberString(cost));
	SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	format(msg, sizeof(msg), " Money lost: $%s", getNumberString(price));
	SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	SendClientMessage(playerid, COLOR_GREENISHGOLD, "|-------Have a nice day!-------|");
	SetPlayerDrunkLevel(playerid, 0);
	SetPlayerHealthEx(playerid, 98.0);
	return 1;
}
RPOnPlayerPickupPickup(playerid, pickupid) {
	if(pickupid == bankpickup) {
		showBankMenu(playerid);
		movePlayerBack(playerid, 3.0);
	}
}
YCMD:flipcoin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to throw a coin in the air.");
		return 1;
	}
	if(GetMoneyEx(playerid) < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have any money!");
		return 1;
	}
	new smsg[8], msg[128];
	new faceornumber = RandomEx(0, 2);
	format(smsg, sizeof(smsg), "%s",faceornumber ? ("face") : ("number"));  
	format(msg, sizeof(msg), "%s throws a coin in the air.", GetPlayerNameEx(playerid, ENameType_RPName));
	ProxMessage(30.0,playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	format(msg, sizeof (msg), "The coin appears to land on %s.", smsg);
	ProxMessage(30.0,playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	return 1;
}
YCMD:mask(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggle Mask On/Off");
		return 1;
	}
	new userflags = GetPVarInt(playerid, "UserFlags");
	if(~userflags & EUFHasMask) {
		if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanHide) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have a mask!");
			return 1;
		}
	}
	if(playerIsMaskRestricted(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have the permissions to use a mask!");
		return 1;
	}
	if(GetPVarInt(playerid, "MaskOn") == 1) {
		SetPlayerMask(playerid, 0);
	} else {
		SetPlayerMask(playerid, 1);
	}
	return 1;
}
YCMD:checkmasked(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all masked players");
		return 1;
	}
	new user,msg[128];
	if(!sscanf(params, "k<playerLookup_MaskID>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		format(msg, sizeof(msg), "* %s[%d] : %d",GetPlayerNameEx(user, ENameType_CharName), user,GetPVarInt(user, "MaskID"));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "**** Masked Players ****");
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				if(GetPVarInt(i, "MaskOn") == 1) {
					format(msg, sizeof(msg), "* %s[%d] : %d",GetPlayerNameEx(i, ENameType_CharName), i,GetPVarInt(i, "MaskID"));
					SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				}
			}
		}
	}
	return 1;
}
rpOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused reason
	#pragma unused killerid
	if(MaskLabels[playerid] != Text3D:0) {
		DestroyDynamic3DTextLabel(MaskLabels[playerid]);
		MaskLabels[playerid] = Text3D:0;
	}
	SetPVarInt(playerid, "MaskOn", 0);
	new flags = GetPVarInt(playerid, "UserFlags");
	flags &= ~EUFHasMask; 
	SetPVarInt(playerid, "UserFlags", flags);
}
SetPlayerMask(playerid, status) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && i != playerid) {
			if(IsPlayerStreamedIn(playerid, i)) {
				ShowPlayerNameTagForPlayer(i, playerid, !status);
			}
		}
	}
	new msg[128];
	if(status) {
		if(MaskLabels[playerid] != Text3D:0) {
			DestroyDynamic3DTextLabel(MaskLabels[playerid]);
		}
		format(msg, sizeof(msg), "* %s puts a mask on",GetPlayerNameEx(playerid, ENameType_RPName));
		MaskLabels[playerid] = CreateDynamic3DTextLabel(getPlayer3dLabelText(playerid),X11_WHITE, 0.0, 0.0, 0.2, NAMETAG_DRAW_DISTANCE,playerid,.testlos=1);
	} else {
		format(msg, sizeof(msg), "* %s takes %s mask off",GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
		if(MaskLabels[playerid] != Text3D:0) {
			DestroyDynamic3DTextLabel(MaskLabels[playerid]);
			MaskLabels[playerid] = Text3D:0;
		}
	}
	ProxMessage(20.0, playerid, msg,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	SetPVarInt(playerid, "MaskOn", status);
}
RPOnPlayerStreamIn(playerid, forplayerid) {
	if(GetPVarInt(playerid, "MaskOn")) {
		if(GetPVarInt(forplayerid, "AdminDuty") == 0) {
			ShowPlayerNameTagForPlayer(forplayerid, playerid, 0);
		}
	}
}
ShowAllNameTagsForPlayer(playerid, show) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && i != playerid) {
			if(IsPlayerStreamedIn(playerid, i)) {
				if(GetPVarInt(i, "MaskOn")) {
					ShowPlayerNameTagForPlayer(playerid, i, show);
				}
			}
		}
	}
}
YCMD:fight(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Requests a player to fight you");
		return 1;
	}
	if(boxingMatchActive())
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "   There is already a fight going on! Wait for it to finish!");
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 20.0,765.9343, 0.2761, 1000.7173))
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "	You are not in Ganton Gym!");
		return 1;
	}
	new user, bet;
	if(!sscanf(params, "k<playerLookup>d", user, bet)) {
		new msg[128];
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not connected!");
			return 1;
		}
		if(user == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot fight yourself!");
			return 1;
		}
		if(bet < 0 || bet > 150000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(bet > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough money");
			return 1;
		}
		if(GetMoneyEx(playerid) < bet) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money for the bet!");
			return 1;
		}
		SetPVarInt(user, "FightOffer", playerid);
		SetPVarInt(user, "FightBet", bet);
		format(msg, sizeof(msg), "{FF0000}%s{FFFFFF} wants to fight you, if you lose, he will get $%d, if he loses, you will win it.",GetPlayerNameEx(playerid, ENameType_RPName),bet);
		ShowPlayerDialog(user, ERP_FightOffer, DIALOG_STYLE_MSGBOX, "{00BFFF}Fight Offer", msg, "Accept", "Reject");
		format(msg, sizeof(msg), "* Fight Offer sent to %s", GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fight [playerid/name] [price]");
	}
	return 1;
}
RPOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	switch(dialogid) {
		case ERP_BankChoose: {
			if(!response) return 0;
			switch(listitem) {
				case 0: {
					showWithdrawMenu(playerid);
				}
				case 1: {
					showDepositMenu(playerid);
				}
				case 2: {
					if(GetPVarInt(playerid, "Level") < 3) {
						SendClientMessage(playerid, X11_TOMATO_2, "You must be level 3 to use this!");
						return 1;
					}
					showTransferMenu(playerid);
				}
			}
		}
		case ERP_BankWithdraw: {
			if(!response) return 0;
			withdrawMoney(playerid, strval(inputtext));
		}
		case ERP_BankDeposit: {
			if(!response) return 0;
			depositMoney(playerid, strval(inputtext));
		}
		case ERP_BankTransferName: {
			if(response == 0) return 0;
			new playa = sscanf_playerLookup(inputtext);
			if(playa == INVALID_PLAYER_ID) {
				ShowPlayerDialog(playerid, ERP_BankTransferName, DIALOG_STYLE_INPUT, "Transfer Menu","{FF0000}User not found!\n\n{FFFFFF}Enter the name or ID of the player you would like to transfer money to","Ok", "Cancel");
				return 1;
			}
			SetPVarInt(playerid, "TransferPlayer", playa);
			ShowPlayerDialog(playerid, ERP_BankTransferAmount, DIALOG_STYLE_INPUT, "Transfer Menu - Amount","Enter the amount you would like to transfer","Transfer", "Cancel");
		}
		case ERP_BankTransferAmount: {
			if(!response) return 0;
			new amount = strval(inputtext);
			new playa = GetPVarInt(playerid, "TransferPlayer");
			DeletePVar(playerid, "TransferPlayer");
			new msg[128];
			if(amount < 1) { 
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
				return 1;
			}
			if(GetMoneyEx(playerid) < amount) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money on hand!");
				return 1;
			}
			format(msg, sizeof(msg), "* %s has transfered $%s into your bank account.",GetPlayerNameEx(playerid, ENameType_RPName),getNumberString(amount));
			SendClientMessage(playerid, X11_YELLOW, msg);
			format(msg, sizeof(msg), "* You have transfered $%s into %s's bank account.",getNumberString(amount),GetPlayerNameEx(playa, ENameType_RPName_NoMask));
			SendClientMessage(playa, X11_YELLOW, msg);
			
			GiveMoneyEx(playerid, -amount);
			new pbank = GetPVarInt(playa, "Bank");
			SetPVarInt(playa, "Bank", pbank + amount);
		}
		case ERP_SexOffer: {
			new sender = GetPVarInt(playerid, "SexOffer");
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You rejected the sex offer");
				SendClientMessage(sender, X11_LIGHTBLUE, "* The sex offer was rejected");
			} else {
				new string[128];
				format(string, sizeof(string), "* You had sex with %s.", GetPlayerNameEx(sender, ENameType_RPName));
				SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
				format(string, sizeof(string), "* %s had sex with you.", GetPlayerNameEx(playerid, ENameType_RPName));
				SendClientMessage(sender, COLOR_LIGHTBLUE, string);
				format(string, sizeof(string), "* %s has sex with %s.", GetPlayerNameEx(sender, ENameType_RPName), GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(15.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			}
			DeletePVar(sender, "SexOffer");
		}
		case ERP_FightOffer: {
			new sender = GetPVarInt(playerid, "FightOffer");
			new fightbet = GetPVarInt(playerid, "FightBet");
			if(GetPVarInt(playerid, "ConnectTime") <  10 || GetPVarInt(sender, "ConnectTime") < 10) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot fight at this playing hours(Need 10)!");
				SendClientMessage(sender, X11_TOMATO_2, "You cannot fight at this playing hours(Need 10)!");
				return 1;
			}
			if(GetMoneyEx(playerid) < fightbet) {
				SendClientMessage(sender, X11_TOMATO_2, "* The person does not have enough money for the bet!");
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
				return 1;
			}
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You rejected the offer");
				SendClientMessage(sender, X11_LIGHTBLUE, "* The offer was rejected");
			} else {
				new Float:X, Float:Y, Float:Z;
				GetPlayerPos(sender, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(playerid, 25.0, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "The person has left Gantom Gym");
					return 1;
				}
				if(sender != -1 && playerid != -1) {
					startFight(sender, playerid, fightbet);
				} else {
					SendClientMessage(playerid, X11_TOMATO_2, "You cannot start a fight with this person!");
				}
			}
		}
		case ERP_AdOptions: {
			if(!response) {
				SendClientMessage(playerid, X11_LIGHTBLUE, "* You closed the advertisement menu.");
				return 1;
			}
			CallLocalFunction(Advertisements[listitem][E_DoAdCallBack],"d", playerid);
		}
		case ERP_PostAd: {
			if(!response) {
				showAdvertisementMenu(playerid);
				return 1;
			}
			new AdvType = GetPVarInt(playerid, "AdvertType");
			processAdvertisement(playerid, inputtext, AdvType);
		}
		case ERP_AdShowAll: {
			if(!response) {
				showAdvertisementMenu(playerid);
				return 1;
			}
			findAdByText(playerid, inputtext);
		}
		case ERP_AdMsgBox: {
			if(!response) {
				showAdvertisementMenu(playerid);
				return 1;
			}
			new user = GetPVarInt(playerid, "LastUserSeenOnAd");
			startAdCall(playerid, user);
		}
		case ERP_SearchAd: {
			if(!response) {
				showAdvertisementMenu(playerid);
				return 1;
			}
			findAdByText(playerid, inputtext);
		}
	}
	return 1;
}
startAdCall(playerid, user) {
	SetPVarInt(user, "CalledBy",playerid);
	SetPVarInt(playerid, "Calling",user);
	StartCalling(playerid, user);
	return 1;
}
withdrawMoney(playerid, amount) {
	new msg[128];
	new bank = GetPVarInt(playerid, "Bank");
	if(amount < 1 || amount > 9999999) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
		return 1;
	}
	if(bank < amount) {
		format(msg, sizeof(msg), "You do not have enough money in your bank account, you are $%s short",getNumberString(amount-bank));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	bank -= amount;
	GiveMoneyEx(playerid, amount);
	SetPVarInt(playerid, "Bank", bank);
	format(msg, sizeof(msg), "You have successfully withdrawn $%s dollars, your new balance is $%s.", getNumberString(amount), getNumberString(bank));
	SendClientMessage(playerid, X11_WHITE, msg);
	return 1;
}
depositMoney(playerid, amount) {
	new msg[128];
	new money = GetMoneyEx(playerid);
	new bank = GetPVarInt(playerid, "Bank");
	if(amount < 1 || amount > 9999999) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
		return 1;
	}
	if(money < amount) {
		format(msg, sizeof(msg), "You do not have enough money, you are $%s short",getNumberString(amount-money));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	bank += amount;
	GiveMoneyEx(playerid, -amount);
	SetPVarInt(playerid, "Bank", bank);
	format(msg, sizeof(msg), "You have successfully deposited $%s dollars into your bank account, your new balance is $%s.", getNumberString(amount), getNumberString(bank));
	SendClientMessage(playerid, X11_WHITE, msg);
	format(msg, sizeof (msg), "** %s leans forward towards the counter, reaches it with his hands and deposits some money.", GetPlayerNameEx(playerid, ENameType_RPName));
	ProxMessage(30.0,playerid, msg, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
	return 1;
}
showWithdrawMenu(playerid) {
	ShowPlayerDialog(playerid, ERP_BankWithdraw, DIALOG_STYLE_INPUT, "Withdraw Menu","Enter the amount you would like to withdraw","Ok", "Cancel");
}
showDepositMenu(playerid) {
	ShowPlayerDialog(playerid, ERP_BankDeposit, DIALOG_STYLE_INPUT, "Deposit Menu","Enter the amount you would like to withdraw","Ok", "Cancel");
}
showTransferMenu(playerid) {
	ShowPlayerDialog(playerid, ERP_BankTransferName, DIALOG_STYLE_INPUT, "Transfer Menu","{FFFFFF}Enter the name or ID of the player you would like to transfer money to","Ok", "Cancel");
}
showBankMenu(playerid) {
	ShowPlayerDialog(playerid, ERP_BankChoose, DIALOG_STYLE_LIST, "Bank Menu","Withdraw\nDeposit\nWire Transfer","Ok", "Cancel");
}
forward showAdvertisementMenu(playerid);
public showAdvertisementMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(Advertisements);i++) {
		if(Advertisements[i][E_AdPrice] > 0) {
			format(tempstr,sizeof(tempstr),"%s $%s\n",Advertisements[i][E_DialogOptionText], getNumberString(Advertisements[i][E_AdPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s\n",Advertisements[i][E_DialogOptionText]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, ERP_AdOptions, DIALOG_STYLE_LIST, "Advertisement Menu",dialogstr,"Ok", "Cancel");
}
forward startPostAd(playerid);
public startPostAd(playerid) {
	SetPVarInt(playerid, "AdvertType", RP_ENormalAd);
	ShowPlayerDialog(playerid, ERP_PostAd, DIALOG_STYLE_INPUT, "Post an Advertisement:","{FFFFFF}Enter a description for the advertisement:","Post", "Cancel");
}
forward startBroadCasterAd(playerid);
public startBroadCasterAd(playerid) {
	SetPVarInt(playerid, "AdvertType", RP_EBroadCastAd);
	ShowPlayerDialog(playerid, ERP_PostAd, DIALOG_STYLE_INPUT, "Post an Advertisement:","{FFFFFF}Enter a description for the advertisement:","Post", "Cancel");
}
forward startPostBizAd(playerid);
public startPostBizAd(playerid) {
	SetPVarInt(playerid, "AdvertType", RP_ECompanyAd);
	ShowPlayerDialog(playerid, ERP_PostAd, DIALOG_STYLE_INPUT, "Post a Business Advertisement:","{FFFFFF}Enter a description for the advertisement:\n{FF0000}Please Note, these ads cannot be anonymous.","Post", "Cancel");
}
forward startAdSearchProcess(playerid);
public startAdSearchProcess(playerid) {
	ShowPlayerDialog(playerid, ERP_SearchAd, DIALOG_STYLE_INPUT, "Search an Advertisement:","{FFFFFF}What are you searching for?","Search", "Cancel");
}
deleteAdvertPVars(playerid) {
	DeletePVar(playerid, "AnonymousAd");
	DeletePVar(playerid, "RPAdText");
	DeletePVar(playerid, "LastUserSeenOnAd");
	DeletePVar(playerid, "AdvertType");
}
processAdvertisement(playerid, adtext[], adtype) {
	new string[256];
	new status = GetPVarInt(playerid, "PhoneStatus");
	if(getPhoneType(playerid) == 0 || GetPVarInt(playerid, "PhoneNumber") == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	if(isJailed(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this here!");
		return 1;
	}
	if(status == 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your phone is off!");
		return 1;
	}
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_AdBanned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are banned from making advertisements!");
		return 1;
	}
	new time = GetPVarInt(playerid, "AdCooldown");
	new timenow = gettime();
	if(AD_COOLDOWN-(timenow-time) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can make another ad",AD_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	if(AD_GLOBAL_COOLDOWN-(timenow-lastad) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can make an ad",AD_GLOBAL_COOLDOWN-(timenow-lastad));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	jailIfOOC(playerid, adtext);
	onProcessAdvertisement(playerid, adtext, adtype);
	return 1;
}
getAdPrice(adtype) {
	new adprice;
	for(new i=0; i < sizeof(Advertisements); i++) {
		if(adtype == Advertisements[i][E_BroadCastType]) {
			adprice = Advertisements[i][E_AdPrice];
		}
	}
	return adprice;
}
forward onProcessAdvertisement(playerid, adtext[], adtype);
public onProcessAdvertisement(playerid, adtext[], adtype) {
	new string[256];
	new payout;
	new status = GetPVarInt(playerid, "PhoneStatus");
	payout = (strlen(adtext)) * 8;
	payout += getAdPrice(adtype);
	if(GetMoneyEx(playerid) < payout) {
		format(string, sizeof(string), "	You don't have enough money! Your ad costs $%d!", payout);
		SendClientMessage(playerid, COLOR_LIGHTRED, string);
		return 1;
	}
	switch(adtype) {
		case RP_ENormalAd: {
			if(status == 1) {
				format(string, sizeof(string), "System: Last /Ad was writen by %s", GetPlayerNameEx(playerid, ENameType_CharName));
				ABroadcast(COLOR_YELLOW, string, EAdminFlags_BasicAdmin);
				format(string, sizeof(string), "%s, Contact: N/A",adtext);
				SetPVarInt(playerid, "AnonymousAd", 1);
			} else {
				format(string, sizeof(string), "%s, Contact: %s @ %d",adtext, GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPVarInt(playerid, "PhoneNumber"));
				SetPVarInt(playerid, "AnonymousAd", 0);
			}
		}
		case RP_ECompanyAd: {
			new num = GetNumOwnedBusinesses(playerid);
			if(num < 1) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't own a business!");
				return 1;
			}
			format(string, sizeof(string), "[Company Advertisement]: %s{%s}, Contact: %s @ %d", FormatColourString(adtext),getColourString(TEAM_GROVE_COLOR), GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPVarInt(playerid, "PhoneNumber"));
			NewsMessage(TEAM_GROVE_COLOR,string);
		}
		case RP_EBroadCastAd: {
			format(string, sizeof(string), "[Broadcast Advertisement]: %s{%s}, Contact: %s @ %d", FormatColourString(adtext),getColourString(TEAM_GROVE_COLOR), GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPVarInt(playerid, "PhoneNumber"));
			NewsMessage(TEAM_GROVE_COLOR,string);
		}
	}
	if(adtype == RP_ENormalAd) {
		SetPVarString(playerid, "RPAdText", string);
	}
	format(string, sizeof(string), "~r~Paid $%s~n~~w~Message contained: %d characters.", getNumberString(payout), strlen(adtext));
	//GameTextForPlayer(playerid, string, 5000, 5);
	ShowScriptMessage(playerid, string, 5000);
	format(string, sizeof(string), "You paid $%s for this ad(%d characters)", getNumberString(payout), strlen(adtext));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
	GiveMoneyEx(playerid, -payout);
	SetPVarInt(playerid, "AdCooldown", gettime());
	return 1;
}
forward getNumOfAdvertisements(playerid);
public getNumOfAdvertisements(playerid) {
	new adAmount = 0;
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "RPAdText") != PLAYER_VARTYPE_NONE) {
				adAmount++;
			}
		}
	}
	return adAmount;
}
forward showAllAdvertisements(playerid);
public showAllAdvertisements(playerid) {
	new adText[256];
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new numofAds;
	numofAds = getNumOfAdvertisements(playerid);
	if(numofAds < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There aren't any advertisements to show");
		return 1;
	}
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "RPAdText") != PLAYER_VARTYPE_NONE) {
				GetPVarString(i, "RPAdText", adText, sizeof(adText));
				format(tempstr, sizeof(tempstr), "Advertisement: %s\n", adText);
				strcat(dialogstr,tempstr,sizeof(dialogstr));
			}
		}
	}
	ShowPlayerDialog(playerid, ERP_AdShowAll, DIALOG_STYLE_LIST, "Advertisements:", dialogstr,"Ok", "Cancel");
	return 1;
}
forward findAdByText(playerid, toSearch[]);
public findAdByText(playerid, toSearch[]) {
	new theAd[256];
	new stringad[256];
	new lastuserseen;
	new numofAds;
	numofAds = getNumOfAdvertisements(playerid);
	if(numofAds < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There aren't any advertisements to show");
		return 1;
	}
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarType(i, "RPAdText") != PLAYER_VARTYPE_NONE) {
				GetPVarString(i, "RPAdText", theAd, sizeof(theAd));
				if(strlen(theAd) > strlen(toSearch)) {
					if(strfind(theAd, toSearch, true) != -1) {
						if(GetPVarInt(i, "AnonymousAd") == 1) {
							format(stringad, sizeof(stringad), "Advertisement: %s\nTelephone Number: Anonymous", theAd);
						} else {
							format(stringad, sizeof(stringad), "Advertisement: %s\nTelephone Number: %d\nFull Name: %s", theAd, GetPVarInt(i, "PhoneNumber"), GetPlayerNameEx(i, ENameType_RPName_NoMask));
						}
						lastuserseen = i;
						break;
					}
				} else {
					if(strfind(toSearch, theAd, true) != -1) {
						if(GetPVarInt(i, "AnonymousAd") == 1) {
							format(stringad, sizeof(stringad), "Advertisement: %s\nTelephone Number: Anonymous", theAd);
						} else {
							format(stringad, sizeof(stringad), "Advertisement: %s\nTelephone Number: %d\nFull Name: %s", theAd, GetPVarInt(i, "PhoneNumber"), GetPlayerNameEx(i, ENameType_RPName_NoMask));
						}
						lastuserseen = i;
						break;
					}
				}
			}
		}
	}
	SetPVarInt(playerid, "LastUserSeenOnAd", lastuserseen);
	if(strlen(stringad) < 80) {
		SendClientMessage(playerid, X11_TOMATO_2, "Advertisement not found");
		showAdvertisementMenu(playerid);
		return 1;
	} else {
		ShowPlayerDialog(playerid, ERP_AdMsgBox, DIALOG_STYLE_MSGBOX, "Advertisement:", stringad,"Call", "Cancel");
	}
	return 1;
}
RPOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	hangupCall(playerid, 1);
	deleteAdvertPVars(playerid);
	if(IsRobbingBusines(playerid) != -1) {
		StopRobBiz(playerid);
	}
	if(MaskLabels[playerid] != Text3D:0) {
		DestroyDynamic3DTextLabel(MaskLabels[playerid]);
		MaskLabels[playerid] = Text3D:0;
	}
	LastHealth[playerid][0] = 0.0;
	LastHealth[playerid][1] = 0.0;
}
RPOnCharLogin(playerid) {
	new mask;
	do {
		mask = RandomEx(MAX_PLAYERS,MAX_PLAYERS+100000);
	} while(findMaskUser(mask) != INVALID_PLAYER_ID);
	SetPVarInt(playerid,"MaskID", mask);
}
getPlayer3dLabelText(playerid) {
	new ret[128];
	new Float:HP, Float:Armour;
	GetPlayerHealth(playerid, HP);
	GetPlayerArmourEx(playerid, Armour);
	format(ret, sizeof(ret), "Stranger %d\nH: %d A: %d",GetPVarInt(playerid, "MaskID"),floatround(HP), floatround(Armour));
	return ret;
}

rpOnPlayerUpdate(playerid) {
//native Text3D:CreateDynamic3DTextLabel(const text[], color, Float:x, Float:y, Float:z, Float:drawdistance, attachedplayer = INVALID_PLAYER_ID, attachedvehicle = INVALID_VEHICLE_ID, testlos = 0, worldid = -1, interiorid = -1, playerid = -1, Float:streamdistance = 100.0);
//native UpdateDynamic3DTextLabelText(Text3D:id, color, const text[]);
	//maybe MaskOn shouldn't be a pvar
	if(MaskLabels[playerid] != Text3D:0) {
		new Float:HP, Float:Armour;
		GetPlayerHealth(playerid, HP);
		GetPlayerArmour(playerid, Armour);
		if(HP != LastHealth[playerid][0] || Armour != LastHealth[playerid][1]) {
			LastHealth[playerid][0] = HP;
			LastHealth[playerid][1] = Armour;
			UpdateDynamic3DTextLabelText(MaskLabels[playerid],X11_WHITE,getPlayer3dLabelText(playerid));
		}
	}
}
hangupCall(playerid,dead) { //includes attempts
	new caller = -1;
	endPayPhoneCall(playerid);
	if(GetPVarType(playerid, "OnCall") == PLAYER_VARTYPE_NONE && (GetPVarType(playerid, "NumRings") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "Calling") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "CalledBy") != PLAYER_VARTYPE_NONE)) { //trying to call someonet
		caller = GetPVarInt(playerid, "Calling");
		new timer = ringtimers[playerid];
		SendClientMessage(caller, COLOR_GREY, "The caller hung up.");
		SendClientMessage(playerid, COLOR_GREY, "You hung up.");
		RemovePlayerAttachedObject(playerid, 9);
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_STOPUSECELLPHONE);
		terminateCallPVars(playerid);
		terminateCallPVars(caller);
		KillTimer(timer);
	}
	caller = GetPVarInt(playerid, "OnCall");
	if(GetPVarType(playerid, "OnCall") != PLAYER_VARTYPE_NONE) {
		RemovePlayerAttachedObject(caller, 9);
		RemovePlayerAttachedObject(playerid, 9);
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_STOPUSECELLPHONE);
		SetPlayerSpecialAction(caller, SPECIAL_ACTION_STOPUSECELLPHONE);
		if(dead == 1) {
			SendClientMessage(caller, COLOR_GREY, "The line went dead...");
		} else if(dead == 2) {
			SendClientMessage(caller, COLOR_PURPLE, "You hear a loud cracking noise on the phone and a person walking away (( Payphone ))");
		} else {
			SendClientMessage(caller, COLOR_GREY, "The caller hung up.");
		}
		terminateCallPVars(playerid);
		terminateCallPVars(caller);
		endPayPhoneCall(caller);
	}
	//pvars won't be deleted if
	DeletePVar(playerid, "OnAir");
	DeletePVar(caller, "OnAir");
	return 1;
}
terminateCallPVars(playerid) { //Figured out we can just use this function instead of creating DeletePVars all over the place
	DeletePVar(playerid, "NumRings");
	DeletePVar(playerid, "Calling");
	DeletePVar(playerid, "CalledBy");
	DeletePVar(playerid, "CalledBy");
	DeletePVar(playerid, "OnCall");
}
YCMD:stats(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a player their game stats");
		return 1;
	}
	SendStats(playerid,playerid);
	return 1;
}
YCMD:smoke(playerid, params[], help) {
	new string[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Smokes a cigars");
		return 1;
	}
	new numcigs = GetPVarInt(playerid, "Cigars");
	if(numcigs == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough smokes!");
		return 0;
	}
	if(IsPlayerBlocked(playerid)) { //anim bug abuse "fix"
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this right now!");
		return 1;	
	}
	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_SMOKE_CIGGY);
    SendClientMessage(playerid, COLOR_DARKGREEN,"* Smoke used!");
	format(string, sizeof(string), "* %s gets out a smoke and lights it.", GetPlayerNameEx(playerid,ENameType_RPName));
	ProxMessage(15.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	SetPVarInt(playerid, "Cigars", --numcigs);
	return 1;
}
isBankInNegative(playerid) {
	new bankamount = GetPVarInt(playerid,"Bank");
	if(bankamount < 0) {
		return 1;
	}
	return 0;
}
SendStats(playerid,targetid) {
	new msg[128];
	format(msg,sizeof(msg),"*___________ %s __________*",GetPlayerNameEx(targetid,ENameType_CharName));
	SendClientMessage(playerid, COLOR_GREENISHGOLD, msg);
	new spousename[MAX_PLAYER_NAME+1];
	GetPVarString(targetid, "SpouseName", spousename, sizeof(spousename));
	
	new nextlevel = (GetPVarInt(targetid,"Level")+1);
	new expamount = nextlevel*levelexp;
	
	format(msg,sizeof(msg),"{%s}[OOC]:{%s} Level:[%s] Respect:[%s/%s] Time Played:[%s] NewbChat Rank:[%s] Cookies:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(GetPVarInt(targetid,"Level")),getNumberString(GetPVarInt(targetid,"RespectPoints")),getNumberString(expamount),getNumberString(GetPVarInt(targetid,"ConnectTime")),GetNewbieName(targetid),getNumberString(GetPVarInt(targetid,"Cookies")));
	SendClientMessage(playerid, COLOR_GREY, msg);
	
	//Donator stuff
	new donaterank = GetPVarInt(targetid,"DonateRank");
	if(donaterank > 0) {
		format(msg,sizeof(msg),"{%s}[OOC]:{%s} DonateRank:[%s] Donate Points:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetDonateRank(GetPVarInt(targetid,"DonateRank")),getNumberString(GetPVarInt(targetid,"DonatePoints")));
		SendClientMessage(playerid, COLOR_GREY, msg);
	}
	//IC
	format(msg,sizeof(msg),"{%s}[IC]:{%s} Sex:[%s] Cash:[$%s] Bank:[$%s] Spouse:[%s] Job: [%s] Wealth:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetSexName(GetPVarInt(targetid,"Sex")),getNumberString(GetPVarInt(targetid,"Money")),getNumberString(GetPVarInt(targetid,"Bank")),spousename,GetJobName(GetPVarInt(targetid,"Job")),getNumberString(getTotalWealth(targetid)));
	SendClientMessage(playerid, COLOR_GREY, msg);
	format(msg,sizeof(msg),"{%s}[Furniture]:{%s} Furniture Tokens:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(GetPVarInt(targetid,"FurnitureTokens")));
	SendClientMessage(playerid, COLOR_GREY, msg);
	
	format(msg,sizeof(msg),"{%s}[Health]:{%s} Hunger: [%d]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetHungerLevel(targetid));
	SendClientMessage(playerid, COLOR_GREY, msg);
	format(msg,sizeof(msg),"{%s}[Gadgets]:{%s} Phone Number:[%d], Walki-Talki Channel: [%d]", getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetPVarInt(targetid,"PhoneNumber"),GetPVarInt(targetid, "WTChannel"));
	SendClientMessage(playerid, COLOR_GREY, msg);
	format(msg,sizeof(msg),"{%s}[Items]:{%s} Cigars:[%s] LockPicks:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(GetPVarInt(targetid, "Cigars")),getNumberString(GetPVarInt(targetid, "VehLockpicks")));
	SendClientMessage(playerid, COLOR_GREY, msg);
	//materials
	new matpacks[3], drugpacks[3];
	matpacks[0] = GetPVarInt(targetid, "MatAPacks");
	matpacks[1] = GetPVarInt(targetid, "MatBPacks");
	matpacks[2] = GetPVarInt(targetid, "MatCPacks");
	drugpacks[0] = GetPVarInt(targetid, "PotSeeds");
	drugpacks[1] = GetPVarInt(targetid, "CokePlants");
	drugpacks[2] = GetPVarInt(targetid, "MethMaterials");
	if(matpacks[0] != 0 || matpacks[1] != 0 || matpacks[2] != 0) {
		format(msg,sizeof(msg),"{%s}[Materials]:{%s} MatAPacks:[%s] MatBPacks:[%s] MatCPacks:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(matpacks[0]),getNumberString(matpacks[1]),getNumberString(matpacks[2]));
		SendClientMessage(playerid, COLOR_LIGHTRED, msg);
	}
	new mats[3];
	mats[0] = GetPVarInt(targetid, "MatsA");
	mats[1] = GetPVarInt(targetid, "MatsB");
	mats[2] = GetPVarInt(targetid, "MatsC");
	if(mats[0] != 0 || mats[1] != 0 || mats[2] != 0) {
		format(msg,sizeof(msg),"{%s}[Materials]:{%s} Type A:[%s Packs] Type B:[%s Packs] Type C:[%s Packs]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(mats[0]),getNumberString(mats[1]),getNumberString(mats[2]));
		SendClientMessage(playerid, COLOR_GREY, msg);
	}
	//drugs
	if(drugpacks[0] != 0 || drugpacks[1] != 0 || drugpacks[2] != 0) {
		format(msg,sizeof(msg),"{%s}[Drugs]:{%s} PotSeeds:[%s] CokePlants:[%s] MethMaterials:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(drugpacks[0]),getNumberString(drugpacks[1]),getNumberString(drugpacks[2]));
		SendClientMessage(playerid, COLOR_LIGHTRED, msg);
	}
	new drugs[3];
	drugs[0] = GetPVarInt(targetid, "Pot");
	drugs[1] = GetPVarInt(targetid, "Coke");
	drugs[2] = GetPVarInt(targetid, "Meth");
	if(drugs[0] != 0 || drugs[1] != 0 || drugs[2] != 0) {
		format(msg,sizeof(msg),"{%s}[Drugs]:{%s} Pot:[%s Gram] Coke:[%s Gram] Meth:[%s Gram]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(drugs[0]),getNumberString(drugs[1]),getNumberString(drugs[2]));
		SendClientMessage(playerid, COLOR_GREY, msg);
	}
	drugsSendStatsMessage(playerid, targetid);
	//Affiliations
	new fid = GetPVarInt(targetid,"Family");
	if(fid != 0) {
		fid = FindFamilyBySQLID(fid);
	} else fid = -1;
	if(fid == -1 && GetPVarInt(targetid, "Faction") != 0) {
		fid = GetPVarInt(targetid, "Faction");
		format(msg,sizeof(msg),"{%s}[Affiliations]:{%s} Faction:[%s] Rank:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetFactionName(fid),getFactionRankName(fid,GetPVarInt(targetid, "Rank")));
		SendClientMessage(playerid, COLOR_GREY, msg);
		
	} else if(fid != -1) {
		format(msg,sizeof(msg),"{%s}[Affiliations]:{%s} Family:[%s] Rank:[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),GetFamilyName(fid),getRankName(fid,GetPVarInt(targetid, "Rank")));
		SendClientMessage(playerid, COLOR_GREY, msg);
	}
	//
	new jailtime = GetPVarInt(targetid, "ReleaseTime");
	new isajail;
	if(jailtime == 0) {
		jailtime = GetPVarInt(targetid, "AJailReleaseTime");
		isajail = 1;
	}
	jailtime -= gettime();
	if(jailtime > 0 || playerHasPrisonLifeSentence(targetid)) {
		if(playerHasPrisonLifeSentence(targetid)) {
			format(msg, sizeof(msg), "Jail Time Left: Life Sentence");
		} else {
			format(msg, sizeof(msg), "Jail Time Left: %s sec", getNumberString(jailtime));
		}
		SendClientMessage(playerid, isajail?COLOR_LIGHTRED:COLOR_LIGHTCYAN, msg);
	}
	format(msg, sizeof(msg), "{%s}[OOC Account State]:{%s} NumAJails:[%s] NumKicks:[%s] NumBans[%s]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(GetPVarInt(targetid, "NumAJAils")),getNumberString(GetPVarInt(targetid, "NumKicks")), getNumberString(GetPVarInt(targetid, "NumBans")));
	SendClientMessage(playerid, COLOR_LIGHTRED, msg);
	//
	new timeinseconds = gettime() - GetPVarInt(targetid, "AccountLoginTime");
	format(msg, sizeof(msg), "{%s}[Current Session]:{%s} Login Time:[%s seconds]",getColourString(COLOR_LIGHTBLUE),getColourString(COLOR_LIGHTCYAN),getNumberString(timeinseconds));
	SendClientMessage(playerid, COLOR_LIGHTRED, msg);
			
	SendClientMessage(playerid, COLOR_GREENISHGOLD,"___________________________________");
}

YCMD:time(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends the time to a person");
		return 1;
	}
    new mtext[20],string[128];
	new year, month,day;
	getdate(year, month, day);
	if(month == 1) { mtext = "January"; }
	else if(month == 2) { mtext = "February"; }
	else if(month == 3) { mtext = "March"; }
	else if(month == 4) { mtext = "April"; }
	else if(month == 5) { mtext = "May"; }
	else if(month == 6) { mtext = "June"; }
	else if(month == 7) { mtext = "July"; }
	else if(month == 8) { mtext = "August"; }
	else if(month == 9) { mtext = "September"; }
	else if(month == 10) { mtext = "October"; }
	else if(month == 11) { mtext = "November"; }
	else if(month == 12) { mtext = "December"; }
	new hour,minuite,second;
	gettime(hour,minuite,second);
	new seasonIndex = getActualSeason(month);
	new jailtime = GetPVarInt(playerid, "ReleaseTime");
	if(jailtime == 0) jailtime = GetPVarInt(playerid, "AJailReleaseTime");
	jailtime -= gettime();
	if (minuite < 10)
	{
		if (jailtime > 0)
		{
			format(string, sizeof(string), "~y~%d %s~n~~g~|~w~%d:0%d~g~|~n~Season: %s~n~~n~Temperature: %d|c~n~~w~Jail Time Left: %d sec", day, mtext, hour, minuite, Seasons[seasonIndex][E_SeasonName], servtemperature, jailtime);
		}
		else
		{
			format(string, sizeof(string), "~y~%d %s~n~~g~|~w~%d:0%d~g~|~n~Season: %s~n~Temperature: %d|c", day, mtext, hour, minuite, Seasons[seasonIndex][E_SeasonName], servtemperature);
		}
	}
	else
	{
		if (jailtime > 0)
		{
			format(string, sizeof(string), "~y~%d %s~n~~g~|~w~%d:%d~g~|~n~Season: %s~n~Temperature: %d|c~n~~w~Jail Time Left: %d sec", day, mtext, hour, minuite, Seasons[seasonIndex][E_SeasonName], servtemperature, jailtime);
		}
		else
		{
			format(string, sizeof(string), "~y~%d %s~n~~g~|~w~%d:%d~g~|~n~Season: %s~n~Temperature: %d|c", day, mtext, hour, minuite, Seasons[seasonIndex][E_SeasonName], servtemperature);
		}
	}
	//GameTextForPlayer(playerid, string, 5000, 1);
	ShowScriptMessage(playerid, string, 5000);
	format(string, sizeof(string), "* %s looks down at %s watch.",GetPlayerNameEx(playerid, ENameType_RPName),getPossiveAdjective(playerid));
	ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT && !IsPlayerBlocked(playerid))
	{
		ApplyAnimation(playerid,"COP_AMBIENT","Coplook_watch",4.1,0,0,0,0,0);
	}
	return 1;
}
YCMD:give(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives someone something");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	new type[10], target, amount;
	new msg[128];
	if(!sscanf(params, "k<playerLookup>s[10]I(1)", target, type, amount)) {
		if(amount < 1 || amount > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Player not found!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(playerid, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(target, 3.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		if(strcmp(type, "Gun", true) == 0) {
			if(IsOnDuty(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
				return 1;
			}
			new weapon = GetPlayerWeaponEx(playerid);
			if(weapon == 0 || isInPaintball(playerid)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be holding a weapon!");
				return 1;
			}
			new gun, ammo, weaponname[32];
			GetPlayerWeaponDataEx(target, GetWeaponSlot(weapon), gun, ammo);
			if(gun != 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "This player currently has a weapon in that slot!");
				return 1;
			}
			//GivePlayerWeaponEx(target, gun, ammo);
			if(GetPVarInt(target, "Level") < MINIMUM_GUN_LEVEL) {
				SendClientMessage(playerid, X11_TOMATO_2, "That player is not allowed to get guns yet.");
				SendClientMessage(target, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
				return 1;
			} else {
				GetPlayerWeaponDataEx(playerid, GetWeaponSlot(weapon), gun, ammo);
				GetWeaponNameEx(gun, weaponname, sizeof(weaponname));
				RemovePlayerWeapon(playerid, weapon);
				GivePlayerWeaponEx(target, gun, ammo);
			}
			format(msg, sizeof(msg), "* %s hands a %s to %s",GetPlayerNameEx(playerid, ENameType_RPName), weaponname, GetPlayerNameEx(target, ENameType_RPName));
			ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			return 1;
		}
		if(strcmp(type, "Drugs", true) == 0) {
			drugsShowGiveDrugMenu(playerid, target, amount);
			return 1;
		}
		for(new i=0;i<sizeof(GiveItems);i++) {
			if(strcmp(type, GiveItems[i][EGiveName], true) == 0 || (strcmp(type, GiveItems[i][EGivePVarName], true) == 0) && strlen(GiveItems[i][EGivePVarName]) > 0) {
				new total = GetPVarInt(playerid, GiveItems[i][EGivePVarName]);
				if(amount > total) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
					return 1;
				}
				total -= amount;
				SetPVarInt(playerid, GiveItems[i][EGivePVarName], total);
				total = GetPVarInt(target, GiveItems[i][EGivePVarName]);
				total += amount;
				SetPVarInt(target, GiveItems[i][EGivePVarName], total);
				if(!strcmp(GiveItems[i][EGivePVarName], "pot", true) || !strcmp(GiveItems[i][EGivePVarName], "coke", true) || !strcmp(GiveItems[i][EGivePVarName], "meth", true)) {
					new job = GetPVarInt(playerid, "Job");
					if(job == EJobType_Drug_Dealer) {
						if(target != playerid) {
							increaseJobPoints(playerid);
						}
					} else {
						SendClientMessage(playerid, X11_TOMATO_2, "Notice: Since you're not a drug dealer, you won't level up.");
					}
				}
				new tmsg[64];
				format(tmsg, sizeof(tmsg), GiveItems[i][EGiveShowName], amount);
				format(msg, sizeof(msg), "* You gave %s to %s.",tmsg, GetPlayerNameEx(target, ENameType_RPName));
				SendClientMessage(playerid, COLOR_DARKGREEN, msg);
				format(msg, sizeof(msg), "* You received %s from %s.",tmsg, GetPlayerNameEx(playerid, ENameType_RPName));
				SendClientMessage(target, COLOR_DARKGREEN, msg);
				format(msg, sizeof(msg), "* %s gives some %s to %s.", GetPlayerNameEx(playerid, ENameType_RPName), GiveItems[i][EGiveName],GetPlayerNameEx(target, ENameType_RPName));
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				return 1;
			}
		}
	} else {
		SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Give [playerid/PartOfName] [Item] [Amount]");
		format(msg, sizeof(msg), "Items: ");
		new tempmsg[64];
		new i;
		for(i=0;i<sizeof(GiveItems);i++) {
			if(tolower(GiveItems[i][EGiveName][0]) == 'm' || GiveItems[i][EGiveName][1] == 'a') { //materials
				format(tempmsg, sizeof(tempmsg), "%s, ", GiveItems[i][EGivePVarName]);
			} else {
				format(tempmsg, sizeof(tempmsg), "%s, ", GiveItems[i][EGiveName]);
			}
			strcat(msg, tempmsg, sizeof(msg));
			if(i%5 == 0 && i != 0) {
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				msg[0] = 0;
			}
		}
		msg[strlen(msg)-1] = 0;
		if(i%5 != 0)
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}
YCMD:id(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays basic information on a player");
		return 1;
	}
	new user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup>", user)) { //k<playerLookup_acc>
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		format(msg, sizeof(msg), "(%d): %s",user,GetPlayerNameEx(user, ENameType_CharName));
		SendClientMessage(playerid, X11_WHITE, msg);
		format(msg, sizeof(msg), "(Level: %s | Connect Time: %s | Ping: %s)",getNumberString(GetPVarInt(user, "Level")),getNumberString(GetPVarInt(user, "ConnectTime")), getNumberString(GetPlayerPing(user)));
		SendClientMessage(playerid, X11_WHITE, msg);
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(aflags & EAdminFlags_RangeBan) {
			new ip[16],GPCI[64];
			new version[40];
			GetPlayerIpEx(user, ip, sizeof(ip));
			gpci(user, GPCI, sizeof(GPCI));
			format(msg, sizeof(msg), "* IP: %s",ip);
			SendClientMessage(playerid, X11_YELLOW, msg);
			SendPlayerHost(playerid, user);
			format(msg, sizeof(msg), "* GPCI: %s",GPCI);
			SendClientMessage(playerid, X11_YELLOW, msg);
			GetPlayerVersion(playerid, version, sizeof(version));
			format(msg, sizeof(msg), "* SA-MP Version: %s",version);
			SendClientMessage(playerid, X11_YELLOW, msg);
		}
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /id [playerid/name]");
	}
	return 1;
}
pickupCall(playerid) {
	new caller = GetPVarInt(playerid, "CalledBy");
	DeletePVar(caller, "NumRings");
	new timer = ringtimers[caller];
	DeletePVar(caller, "ringtimer");
	DeletePVar(playerid, "CalledBy");
	KillTimer(timer);
	SetPVarInt(caller, "OnCall",playerid);
	SetPVarInt(playerid, "OnCall",caller);
	SendClientMessage(caller, COLOR_GREY, "They answered the call.");
	SendClientMessage(playerid, COLOR_GREY, "You answered the call.");
	return 1;
}
YCMD:me(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, X11_WHITE, "Sends an action to other players in the area.");
    }
    else
    {
        new str[256];
        if (isnull(params))
        {
            format(str, sizeof (str), "Usage: \"/%s [action]\"", Command_GetDisplayNamed("me", playerid));
            SendClientMessage(playerid, X11_WHITE, str);
        }
        else
        {
            format(str, sizeof (str), "** %s %s", GetPlayerNameEx(playerid, ENameType_RPName), params);
			ProxMessage(30.0,playerid, str, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
			SetPlayerChatBubble(playerid, str, COLOR_PURPLE, 30.0, 5000);
        }
    }
    return true;
}
YCMD:ame(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, X11_WHITE, "Sends an action to other players in the area.");
    }
    else
    {
        new str[256];
        if (isnull(params))
        {
            format(str, sizeof (str), "Usage: \"/%s [action]\"", Command_GetDisplayNamed("ame", playerid));
            SendClientMessage(playerid, X11_WHITE, str);
        }
        else
        {
			
            format(str, sizeof (str), "* %s %s", GetPlayerNameEx(playerid, ENameType_RPName), params);
			//ProxMessage(5.0,playerid, str, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
			SetPlayerChatBubble(playerid, str, COLOR_PURPLE, 5.0, 10000);
			SendClientMessage(playerid, COLOR_PURPLE, str);
        }
    }
    return true;
}
YCMD:do(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, X11_WHITE, "Sends an action to other players in the area.");
    }
    else
    {
        new str[256];
        if (isnull(params))
        {
            format(str, sizeof (str), "Usage: \"/%s [action]\"", Command_GetDisplayNamed("do", playerid));
            SendClientMessage(playerid, X11_WHITE, str);
        }
        else
        {
            format(str, sizeof (str), "%s (( %s ))", params, GetPlayerNameEx(playerid, ENameType_RPName));
			ProxMessage(30.0,playerid, str, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
        }
    }
    return true;
}
stock getAccentText(playerid) {
	new accenttext[32];
	new accent[32];
	GetPVarString(playerid, "Accent", accent, sizeof(accent));
	if(strlen(accent) < 3) { 
		accenttext[0] = 0;
	} else {
		format(accenttext, sizeof(accenttext), "[%s] ",accent);
	}
	return accenttext;
}
YCMD:shout(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shouts a message");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too weak to shout!");
		return 1;
	}
	new msg[256];
	new string[256];
	if(!sscanf(params, "s[256]",msg)) {
		format(string, sizeof(string), "%s%s shouts: %s",getAccentText(playerid),GetPlayerNameEx(playerid, ENameType_RPName),msg);
		ProxMessage(30.0, playerid, string,X11_WHITE,X11_WHITE,X11_WHITE,COLOR_FADE1,COLOR_FADE2);
		sendMessageToHouses(playerid, 30.0, string, COLOR_FADE1);
		sendMessageToBusinesses(playerid, 30.0, string, COLOR_FADE1);
		SetPlayerChatBubble(playerid, msg, X11_WHITE, 30.0, 5000);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /shout [message]");
	}
	return 1;
}
YCMD:low(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Says a message quietly");
		return 1;
	}
	new msg[256];
	new string[256];
	if(!sscanf(params, "s[256]",msg)) {
		format(string, sizeof(string), "%s%s Says [LOW]: %s",getAccentText(playerid),GetPlayerNameEx(playerid, ENameType_RPName),msg);
		ProxMessage(3.0, playerid, string,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /low [message]");
	}
	return 1;
}
YCMD:whisper(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Says a message quietly");
		return 1;
	}
	new msg[256];
	new string[256];
	new user;
	if(!sscanf(params, "k<playerLookup>s[256]",user,msg)) {
		if(!IsPlayerConnectEx(user)) {
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away");
			return 1;
		}
		format(string, sizeof(string), "%s whispers %s", GetPlayerNameEx(playerid, ENameType_RPName), msg);
		SendClientMessage(user,  COLOR_YELLOW, string);
		SendClientMessage(playerid,  COLOR_YELLOW, string);
		SendBigEarsMessage(COLOR_YELLOW, string);
		format(string, sizeof(string), "%s whispers something to %s.", GetPlayerNameEx(playerid, ENameType_RPName), GetPlayerNameEx(user, ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /whisper [playerid/name] [message]");
	}
	return 1;
}
YCMD:b(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, X11_WHITE, "Sends an action to other players in the area.");
    }
    else
    {
        new str[256];
        if (isnull(params))
        {
            format(str, sizeof (str), "Usage: \"/%s [message]\"", Command_GetDisplayNamed("b", playerid));
            SendClientMessage(playerid, X11_WHITE, str);
        }
        else
        {
            format(str, sizeof(str), "(( [%d] %s says: %s ))", playerid, GetPlayerNameEx(playerid, ENameType_RPName), params);
			ProxMessage(30.0,playerid, str, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
        }
    }
    return 1;
}
YCMD:close(playerid, params[], help)
{
    if (help)
    {
        SendClientMessage(playerid, X11_WHITE, "Sends an action to other players in the area.");
    }
    else
    {
        new str[256];
        if (isnull(params))
        {
            format(str, sizeof (str), "Usage: \"/%s [message]\"", Command_GetDisplayNamed("close", playerid));
            SendClientMessage(playerid, X11_WHITE, str);
			return 1;
        }
		format(str, sizeof(str), "%s mutters: %s", GetPlayerNameEx(playerid, ENameType_RPName), params);
        if(IsPlayerInAnyVehicle(playerid))
        {
		    SendVehicleMessage(GetPlayerVehicleID(playerid), COLOR_FADE1, str);
		}
		else
		{
			SetPlayerChatBubble(playerid, str, X11_WHITE, 3.0, 5000);
			ProxMessage(5.0, playerid, str, COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
        }
    }
    return 1;
}
YCMD:pay(playerid, params[], help) {
	new playa, money, string[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player money");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup>d", playa, money)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED_2, "User not found!");
			return 1;
		}
		if(playa == playerid) {
			SendClientMessage(playerid, X11_WHITE, "You cannot give money to yourself");
			return 1;
		}
		if(GetPVarInt(playerid, "ConnectTime") < 8) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be over 8 connect time to do this!");
			return 1;
		}
		if(money < 1 || money >= 10000000) {
			SendClientMessage(playerid, X11_WHITE, "Invalid Amount");
			return 1;
		}
		if(GetMoneyEx(playerid) < money) {
			SendClientMessage(playerid, X11_RED_2, "You do not have enough money");
			return 1;
		}
		new Float:X,Float:Y,Float:Z;
		GetPlayerPos(playa, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid,5.0,X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not near this player");
			return 1;
		}
		format(string, sizeof(string), "* You gave $%s to %s.", getNumberString(money), GetPlayerNameEx(playa, ENameType_RPName));
		SendClientMessage(playerid, COLOR_DARKGREEN, string);
		format(string, sizeof(string), "* You have received $%s from %s.", getNumberString(money), GetPlayerNameEx(playerid, ENameType_RPName));
		SendClientMessage(playa, COLOR_DARKGREEN, string);
		format(string, sizeof(string), "* %s takes out some cash, and hands it to %s.", GetPlayerNameEx(playerid, ENameType_RPName) ,GetPlayerNameEx(playa, ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		format(string, sizeof(string), "%s pays %s %d",GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(playerid, ENameType_CharName), money);
		payLog(string);
		if(money > 19999 && GetPVarInt(playerid, "ConnectTime") < 50 || money > 49999)
		{
			format(string, sizeof(string), "Possible Money Farming: %s has paid $%s to %s.", GetPlayerNameEx(playerid, ENameType_CharName), getNumberString(money), GetPlayerNameEx(playa, ENameType_CharName));
			ABroadcast(COLOR_YELLOW,string, EAdminFlags_BasicAdmin);
		}
		GiveMoneyEx(playerid, -money);
		GiveMoneyEx(playa, money);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /pay [playerid/name] [amount]");
	}
	return 1;
}
YCMD:channel(playerid, params[], help) {
	new channel;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players walkie talkie channel");
		return 1;
	}
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasWalkieTalkie) {
		SendClientMessage(playerid, X11_RED3, "You do not have a walkie talkie, buy one at a 24/7.");
		return 1;
	}
	if (!sscanf(params, "d", channel))
    {
		if(channel < 0 || channel > 9999) {
			SendClientMessage(playerid, X11_RED3, "Invalid channel. The channel must be above 0, and under 9999");
			return 1;
		}
		SetPVarInt(playerid, "WTChannel", channel);
		SendClientMessage(playerid, COLOR_DARKGREEN, "Channel set.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /channel [channel]");
	}
	return 1;
}
YCMD:phonebook(playerid, params[], help) {
	if(help) {
		return 1;
	}
	return 1;
}
/*
YCMD:number(playerid, params[], help) {
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasPhoneBook) {
	    SendClientMessage(playerid, COLOR_LIGHTRED, "   You don't have a phone book!");
	    return 1;
	}
	new player;
	if(!sscanf(params, "k<playerLookup_acc>",player))
	{
		if(IsPlayerConnectEx(player))
		{
		    new string[128];
		    if(GetPVarInt(player, "PhoneStatus") == 0) {
			    format(string,sizeof(string),"%s has the following phone number: %d",GetPlayerNameEx(player, ENameType_RPName_NoMask), GetPVarInt(player, "PhoneNumber"));
			    SendClientMessage(playerid, COLOR_WHITE, string);
		    } else {
				SendClientMessage(playerid, COLOR_WHITE, "This persons phone is either off, or unlisted.");
		    }
		} else {
		    SendClientMessage(playerid, COLOR_WHITE, "Player not found!");
		}
	} else {
		    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /number [playerid/PartOfName]");
		    SendClientMessage(playerid, COLOR_LIGHTBLUE, "This only shows people whos phone status is set to \"ON\"");
	}
	return 1;
}
*/
YCMD:walkietalkie(playerid, params[], help) {
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Talk over walkie talkie chat");
		return 1;
	}
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasWalkieTalkie) {
		SendClientMessage(playerid, X11_RED3, "You do not have a walkie talkie, buy one at a 24/7.");
		return 1;
	}
	if(isPlayerFrozen(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this while frozen!");
		return 1;
	}
	if(isInJail(playerid) != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this in jail!");
		return 1;
	}
	if(IsRadioJammerActiveInArea(playerid)) {
		format(msg, sizeof(msg), "* You hear static coming from the radio");
		ProxMessage(20.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		return 1;
	}
	new channel = GetPVarInt(playerid, "WTChannel");
	if(channel == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You must set a channel, use /channel");
		return 1;
	}
	jailIfOOC(playerid, params);
	format(msg, sizeof(msg), "* WalkiTalki %s: %s *", GetPlayerNameEx(playerid, ENameType_RPName),params);
	SendWTMsgToListeners(playerid, params, channel);
	SendWalkieTalkieMessage(channel, COLOR_MEDIUMAQUA, msg);
	format(msg, sizeof(msg), "%s says (Radio): %s", GetPlayerNameEx(playerid, ENameType_RPName), params);
	ProxMessage(20.0, playerid, msg,COLOR_FADE1,COLOR_FADE2,COLOR_FADE3,COLOR_FADE4,COLOR_FADE5);
	return 1;
}
YCMD:dice(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Rolls a dice");
		return 1;
	}
	new msg[128], dice;
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasDice) {
		SendClientMessage(playerid, X11_RED3, "You do not have a dice, buy one at a 24/7.");
		return 1;
	}
	if(!sscanf(params, "d", dice))
	{
		if(dice < 0 || dice > 10) {
			SendClientMessage(playerid, X11_WHITE, "You can't roll less than 0 dice, or more than 10!");
			return 1;
		}
	    if(dice == 1)
	    {
	        format(msg, sizeof(msg), "* %s throws a die that lands on {%s}%d{%s}.", GetPlayerNameEx(playerid, ENameType_RPName),getColourString(X11_ORANGE),random(6)+1,getColourString(COLOR_PURPLE));
			ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	    }
	    else
	    {
	        new rolled = random(6*dice)+1;
			format(msg, sizeof(msg), "* %s throws %d dice that land on {%s}%d{%s}.", GetPlayerNameEx(playerid, ENameType_RPName), dice, getColourString(X11_ORANGE),rolled,getColourString(COLOR_PURPLE));
			ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		return 1;
	}
	else
	{
	    SendClientMessage(playerid, X11_WHITE, "USAGE: /dice [number of dice to throw]");
	}
	return 1;
}
YCMD:call(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Call someone with your phone");
		return 1;
	}
	new number;
	if(isPlayerFrozen(playerid) || IsPlayerBlocked(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this while frozen!");
		return 1;
	}
	if(!sscanf(params,"d",number)) {
		tryCallNumber(playerid, number);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /call [number]");
	}
	return 1;
}
start911Call(playerid) {
	SetPVarInt(playerid, "911Call", 1);
	SendClientMessage(playerid, COLOR_ALLDEPT, "Operator: What service do you require? Say \"Police\" or \"Paramedic\"");
}
tryCallNumber(playerid, number) {
	new player = findPlayerFromPhoneNumber(number);
	if(getPhoneType(playerid) == 0  || isInJail(playerid) || GetPVarInt(playerid, "PhoneNumber") == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	if(GetPVarInt(player,"PhoneStatus") == 2 || getPlayerPhoneID(player) != -1 || GetPVarInt(player, "Calling") != PLAYER_VARTYPE_NONE || GetPVarInt(player, "CalledBy") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, COLOR_GREY, "You get a busy tone...");
		return 1;
	}
	if(GetPVarType(playerid, "Calling") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "CalledBy") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_WHITE, "You are already in a call");
		return 1;
	}
	if(IsInCellPhoneJammerArea(playerid)) {
		SendClientMessage(playerid, X11_WHITE, "Your phone currently has no service.");
		return 1;
	}
	if(number == 911) {
		SetPlayerAttachedObject(playerid, 9, 330, 6);
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_USECELLPHONE);
		start911Call(playerid);
		return 1;
	}
	if(player == -1 || number == 0 || isInJail(player)) {
		SendClientMessage(playerid, X11_RED2, "Phone number is not valid, or player is offline");
		return 1;
	}
	SetPVarInt(player, "CalledBy",playerid);
	SetPVarInt(playerid, "Calling",player);
	StartCalling(playerid, player);
	return 1;
}
trySendSMS(playerid, number, message[]) {
	new msg[128];
	new player = findPlayerFromPhoneNumber(number);
	if(player == -1 || getPhoneType(player) == 0 || isInJail(playerid) || isInJail(player)) {
		SendClientMessage(playerid, X11_RED2, "Phone number is not valid, or player is offline");
		return 1;
	}
	if(getPhoneType(playerid) == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	if(GetPVarInt(player,"PhoneStatus") == 2 || getPlayerPhoneID(player) != -1) {
		SendClientMessage(playerid, COLOR_GREY, "This phone is offline.");
		return 1;
	}
	new dispnumber = GetPVarInt(playerid, "PhoneNumber");
	if(GetPVarInt(playerid,"PhoneStatus") == 1) {
		dispnumber = 0;
	}
	jailIfOOC(playerid, message);
	format(msg,sizeof(msg),"SMS: %s, Sender %s (%d)",message,GetPlayerNameEx(playerid,ENameType_Phone_Name),dispnumber);
	SendClientMessage(player, X11_YELLOW, msg);
	SendClientMessage(playerid, X11_YELLOW, msg);
	format(msg,sizeof(msg),"* %s's phone beeps.",GetPlayerNameEx(player,ENameType_RPName));
	ProxMessage(20.0,player,msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 1;
}
YCMD:sms(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a SMS");
		return 1;
	}
	new number,message[64];
	if(!sscanf(params, "ds[64]", number, message)) {
		trySendSMS(playerid, number, message);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sms [number] [message]");
	}
	return 1;
}
YCMD:hangup(playerid, params[], help) {
	if(GetPVarType(playerid, "911Call") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "911Call");
		DeletePVar(playerid, "911Type");
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_STOPUSECELLPHONE);
		RemovePlayerAttachedObject(playerid, 9);
		SendClientMessage(playerid, X11_WHITE, "You hung up.");	
		return 1;
	}
	if(GetPVarType(playerid, "OnCall") == PLAYER_VARTYPE_NONE && (GetPVarType(playerid, "Ringer") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "CalledBy") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "Ringing") != PLAYER_VARTYPE_NONE)) {
		SetPVarInt(playerid, "WantHangup", 1);
		return 1;
	} else if(GetPVarType(playerid, "OnCall") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not in a phone call!");
		return 1;
	}
	hangupCall(playerid, 0);
	SendClientMessage(playerid, X11_WHITE, "You hung up.");
	return 1;
}
YCMD:pickup(playerid, params[], help) {
	if(GetPVarType(playerid, "CalledBy") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_WHITE, "You aren't being called by anyone.");
		return 1;
	}
	pickupCall(playerid);
	if(!IsPlayerInAnyVehicle(playerid)) {
		SetPlayerAttachedObject(playerid, 9, 330, 6);
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_USECELLPHONE);
	}
	return 1;
}
getPlayerPhoneID(playerid) {
	if(GetPVarType(playerid, "CalledBy") != PLAYER_VARTYPE_NONE) {
		return GetPVarInt(playerid, "CalledBy");
	}
	if(GetPVarType(playerid, "OnCall") != PLAYER_VARTYPE_NONE) {
		return GetPVarInt(playerid, "OnCall");
	}
	if(GetPVarType(playerid, "Calling") != PLAYER_VARTYPE_NONE) {
		return GetPVarInt(playerid, "Calling");
	}
	return -1;
}
YCMD:togphone(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles the status of the players phone");
		return 1;
	}
	if(getPhoneType(playerid) == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	new status = GetPVarInt(playerid, "PhoneStatus");
	if(status >= 2) {
		status = 0;
	} else {
		status++;
	}
	new text[32],msg[128];
	switch(status) {
		case 0: {
			strmid(text,"On",0,2,sizeof(text));
		}
		case 1: {
			strmid(text,"Anonymous",0,9,sizeof(text));
		}
		case 2: {
			strmid(text,"Off",0,3,sizeof(text));
		}
	}
	format(msg,sizeof(msg),"Phone set to: %s",text);
	SendClientMessage(playerid, X11_ORANGE, msg);
	SetPVarInt(playerid, "PhoneStatus", status);
	return 1;
}
YCMD:ooc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends an OOC Message");
		return 1;
	}
	if(isnull(params)) {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ooc [message]");
		return 1;
	}
	if(!noooc || EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BigEars) {
		new msg[256];
		format(msg,sizeof(msg),"(( %s: %s ))",GetPlayerNameEx(playerid, ENameType_CharName),params);
		OOCBroadcast(COLOR_OOC, msg);
	} else {
		SendClientMessage(playerid, X11_RED3, "OOC Chat is disabled");
	}
	return 1;
}
YCMD:noooc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles OOC Chat On/Off for the server");
		return 1;
	}
	if(noooc) {
		SendClientMessageToAll(X11_ORANGE, "   OOC chat channel has been enabled by an Admin !");
		noooc = 0;
	} else {
		SendClientMessageToAll(X11_ORANGE, "   OOC chat channel has been disabled by an Admin !");
		noooc = 1;
	}
	return 1;
}

YCMD:usepot(playerid, params[], help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to use pot");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are dying! You cannot do this!");
		return 1;
	}
	new string[128];
	new time = GetPVarInt(playerid, "DrugCooldown");
	new timenow = gettime();
	if(DRUG_COOLDOWN-(timenow-time) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can use any more drugs",DRUG_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	new meth = GetPVarInt(playerid, "Pot");
	if(meth > 0)
	{
		new highlvl;
		highlvl = GetPlayerDrunkLevel(playerid);
		SetPlayerDrunkLevel(playerid, highlvl + 2000);
		
		new Float:health;
		GetPlayerHealth(playerid, health);
		SetPlayerHealthEx(playerid, health + 10.0);

		
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_SMOKE_CIGGY );
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Pot used!");
		SetPVarInt(playerid, "Pot", meth-1);
		format(string, sizeof(string), "* %s smokes some pot.", GetPlayerNameEx(playerid,ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPVarInt(playerid, "DrugCooldown", timenow);
		SetPlayerHunger(playerid, PlayerHunger[playerid]-20);
		return 1;
	}
	else
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "   You don't have any pot left!");
	}
	return 1;
}
YCMD:usecoke(playerid, params[], help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to use pot");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are dying! You cannot do this!");
		return 1;
	}
	new string[128];
	new time = GetPVarInt(playerid, "DrugCooldown");
	new timenow = gettime();
	if(DRUG_COOLDOWN-(timenow-time) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can use any more drugs",DRUG_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	new meth = GetPVarInt(playerid, "Coke");
	if(meth > 0)
	{
		new highlvl;
		highlvl = GetPlayerDrunkLevel(playerid);
		SetPlayerDrunkLevel(playerid, highlvl + 2000);
		
		new Float:health;
        GetPlayerHealth(playerid, health);
		SetPlayerHealthEx(playerid, health + 25.0);
		
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_SMOKE_CIGGY );
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Coke used!");
		SetPVarInt(playerid, "Coke", meth-1);
		format(string, sizeof(string), "* %s snorts some coke.", GetPlayerNameEx(playerid,ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPVarInt(playerid, "DrugCooldown", timenow);
		SetPlayerHunger(playerid, PlayerHunger[playerid]-20);
		new dtaken = GetPVarInt(playerid, "DrugsTaken");
		SetPVarInt(playerid, "DrugsTaken", ++dtaken);
		if(dtaken > 4) { //Amount of drugs taken
			giveDisease(playerid, 2, 1);
			DeletePVar(playerid, "DrugsTaken");
		}
		if(PlayerHunger[playerid] < 50) {
			ApplyAnimation(playerid,"CRACK","crckdeth2",4.1,0,1,1,1,1);
			TogglePlayerControllableEx(playerid, 0);
			SetTimerEx("SetControllable",15000,false,"dd",playerid,1);
			ShowScriptMessage(playerid, "You have passed out, your system cannot process the drug.", 15000);
		}
		return 1;
	}
	else
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "   You don't have any coke left!");
	}
	return 1;
}

YCMD:usemeth(playerid, params[], help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to use meth");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are dying! You cannot do this!");
		return 1;
	}
	new string[128];
	new time = GetPVarInt(playerid, "DrugCooldown");
	new timenow = gettime();
	if(DRUG_COOLDOWN-(timenow-time) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can use any more drugs",DRUG_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	new meth = GetPVarInt(playerid, "Meth");
	if(meth > 0)
	{
		new highlvl;
		highlvl = GetPlayerDrunkLevel(playerid);
		SetPlayerDrunkLevel(playerid, highlvl + 2000);
		
		new Float:health;
		GetPlayerHealth(playerid, health);
		SetPlayerHealthEx(playerid, health + 10.0);
		/*
        GetPlayerArmour(playerid, health);
		SetPlayerArmourEx(playerid, health + 10.0);
		*/
		SetPlayerSpecialAction(playerid,SPECIAL_ACTION_SMOKE_CIGGY );
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Meth used!");
		SetPVarInt(playerid, "Meth", meth-1);
		format(string, sizeof(string), "* %s smokes some meth.", GetPlayerNameEx(playerid,ENameType_RPName));
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		SetPVarInt(playerid, "DrugCooldown", timenow);
		SetPlayerHunger(playerid, PlayerHunger[playerid]-20);
		new dtaken = GetPVarInt(playerid, "DrugsTaken");
		SetPVarInt(playerid, "DrugsTaken", ++dtaken);
		if(dtaken > 4) { //Amount of drugs taken
			giveDisease(playerid, 3, 1);
			DeletePVar(playerid, "DrugsTaken");
		}
		return 1;
	}
	else
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "   You don't have any meth left!");
	}
	return 1;
}

YCMD:sex(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offers a player to have sex");
		return 1;
	}
	new user;
	if(!IsPlayerInAnyVehicle(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	new msg[128];
	if(!sscanf(params,"k<playerLookup>",user)) {
		new time = GetPVarInt(playerid, "SexCooldown");
		new timenow = gettime();
		if(DRUG_COOLDOWN-(timenow-time) > 0) {
			format(msg, sizeof(msg), "You must wait %d seconds before you offer more sex",DRUG_COOLDOWN-(timenow-time));
			SendClientMessage(playerid, X11_TOMATO_2, msg);
			return 1;
		}
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(user == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You can't have sex with yourself!");
			return 1;
		}
		if(GetPlayerVehicleID(user) != GetPlayerVehicleID(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be in the same vehicle!");
			return 1;
		}
		SetPVarInt(playerid, "SexCooldown", gettime());
		SetPVarInt(user, "SexOffer", playerid);
		format(msg, sizeof(msg), "{FF0000}%s{FFFFFF} Wants to have sex with you",GetPlayerNameEx(playerid, ENameType_RPName));
		ShowPlayerDialog(user, ERP_SexOffer, DIALOG_STYLE_MSGBOX, "{00BFFF}Sex Offer", msg, "Accept", "Reject");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sex [playerid/name]");
	}
	return 1;
}
setCharacterSkin(playerid, skin) {
	if(skin < 0 || skin > 299) return 0;
	SetPVarInt(playerid,"SkinID",skin);
	SetPlayerSkin(playerid, skin);
	return 1;
}
stock GetSexName(sex) {
	new name[32];
	switch(sex) {
		case 0: {
			strmid(name, "Male",0, 4, sizeof(name));
		}
		case 1: {
			strmid(name, "Female",0, 6, sizeof(name));
		}
		case 2: {
			strmid(name, "Transvestite",0, 12, sizeof(name));
		}
	}
	return name;
}
public SendWalkieTalkieMessage(channel, color, msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "WTChannel") == channel) {
			if(!IsRadioJammerActiveInArea(i))
				SendClientMessage(i, color, msg);
		}
	}
	return 1;
}
findPlayerFromPhoneNumber(number) {
	foreach(Player, i) {
		if(GetPVarInt(i, "PhoneNumber") == number && IsPlayerConnectEx(i)) {
			return i;
		}
	}
	return -1;
}
StartCalling(caller, target) {
	new msg[128];
	if(GetPVarType(target, "OnCall") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(caller, COLOR_GREY, "You get a busy tone...");
		return 1;
	}
	if(GetPVarType(target, "PhoneStatus") == 2) {
		SendClientMessage(caller, COLOR_GREY, "You get a busy tone...");
		return 1;
	}
	new dispnumber = GetPVarInt(caller,"PhoneNumber");
	if(GetPVarInt(caller, "PhoneStatus") == 1) {
		dispnumber = 0;
	}
	SetPVarInt(target, "Ringer", caller);
	SetPVarInt(caller, "Ringing", target);
	new calltimer = SetTimerEx("StartRinging", 5000, true,"dd", caller, target);
	format(msg,sizeof(msg),"Your phone is ringing. Caller ~r~%s~w~(~g~%d~w~)",GetPlayerNameEx(caller,ENameType_Phone_Name),dispnumber);
	ShowScriptMessage(target, msg, 5000);
	if(GetPVarInt(target, "PhoneStatus") != 2) {
		format(msg, sizeof(msg), "* %s's phone begins to ring.", GetPlayerNameEx(target,ENameType_RPName));
		ProxMessage(25.0, target, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	}
	SendClientMessage(caller, COLOR_GREY, "Dialing...");
	//showHelpText(caller, "Dialing...");
	ringtimers[caller] = calltimer;
	if(!IsPlayerInAnyVehicle(caller)) {
		SetPlayerAttachedObject(caller, 9, 330, 6);
		SetPlayerSpecialAction(caller,SPECIAL_ACTION_USECELLPHONE);
	}
	return 1;
}
public StartRinging(caller, target) {
	new rings = GetPVarInt(caller,"NumRings");
	SetPVarInt(caller,"NumRings", rings+1);
	if(rings >= MAX_RINGS || GetPVarInt(caller, "WantHangup") || GetPVarInt(target, "WantHangup") || !IsPlayerConnectEx(caller) || !IsPlayerConnectEx(target)) {
		DeletePVar(caller, "NumRings");
		new timer = ringtimers[caller];
		KillTimer(timer);
		DeletePVar(target, "WantHangup");
		DeletePVar(caller, "WantHangup");
		SetPlayerSpecialAction(target,SPECIAL_ACTION_STOPUSECELLPHONE);
		SetPlayerSpecialAction(caller,SPECIAL_ACTION_STOPUSECELLPHONE);
		RemovePlayerAttachedObject(target, 9);
		RemovePlayerAttachedObject(caller, 9);
		DeletePVar(caller, "Calling");
		DeletePVar(target, "Ringer");
		DeletePVar(target, "CalledBy");
		SendClientMessage(target, COLOR_GREY, "Call Rejected");
		SendClientMessage(caller, COLOR_GREY, "Call Rejected");
		return 0;
	}
	new msg[128];
	format(msg, sizeof(msg), "* %s's phone rings.", GetPlayerNameEx(target,ENameType_RPName));
	ProxMessage(25.0, target, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	SendClientMessage(caller, COLOR_GREY, "Ringing...");
	return 0;
}
getPhoneType(playerid) {
	return GetPVarInt(playerid, "Phone");
}
OOCBroadcast(color, msg[]) {
	//for when /togooc is implemented
	//SendClientMessageToAll(color, msg);
	SendSplitClientMessageToAll(color, msg, 0, 128);
}
/*
YCMD:ad(playerid, params[], help) {
	if(getPhoneType(playerid) == 0 || GetPVarInt(playerid, "PhoneNumber") == 0) {
		SendClientMessage(playerid, COLOR_GREY, "You don't have a phone, buy one at a 24/7");
		return 1;
	}
	if(isJailed(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this here!");
		return 1;
	}
	new status = GetPVarInt(playerid, "PhoneStatus");
	if(status == 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your phone is off!");
		return 1;
	}
	if(GetPVarInt(playerid, "Level") < 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be level 2 for this!");
		return 1;
	}
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_AdBanned) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are banned from making advertisements!");
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 10.0, 2253.182617, 25.457563, 5.307678)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not inside Fox News!");
		return 1;
	}
	new time = GetPVarInt(playerid, "AdCooldown");
	new timenow = gettime();
	new result[128],string[256];
	if(AD_COOLDOWN-(timenow-time) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can make another ad",AD_COOLDOWN-(timenow-time));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	if(AD_GLOBAL_COOLDOWN-(timenow-lastad) > 0) {
		format(string, sizeof(string), "You must wait %d seconds before you can make an ad",AD_GLOBAL_COOLDOWN-(timenow-lastad));
		SendClientMessage(playerid, X11_TOMATO_2, string);
		return 1;
	}
	if(!sscanf(params, "s[128]",result)) {	
		jailIfOOC(playerid, result);
		new len = strlen(result);
		new payout = (len) * 4;
		payout += 5000;
		if(GetMoneyEx(playerid) < payout)
		{
			format(string, sizeof(string), "	You don't have enough money! Your ad costs $%d!", payout);
			SendClientMessage(playerid, COLOR_LIGHTRED, string);
			return 1;
		}
		lastad = timenow;
		if(status == 1)
		{
			format(string, sizeof(string), "Ad: %s{%s}, Contact: N/A",FormatColourString(result),getColourString(TEAM_GROVE_COLOR));
			NewsMessage(TEAM_GROVE_COLOR,string);
			format(string, sizeof(string), "System: Last /Ad was writen by %s", GetPlayerNameEx(playerid, ENameType_CharName));
			ABroadcast(COLOR_YELLOW, string, EAdminFlags_BasicAdmin);
		}
		else
		{
			format(string, sizeof(string), "Ad: %s{%s}, Contact: %s @ %d", FormatColourString(result),getColourString(TEAM_GROVE_COLOR), GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPVarInt(playerid, "PhoneNumber"));
			NewsMessage(TEAM_GROVE_COLOR,string);
		}
		GiveMoneyEx(playerid, -payout);
		SetPVarInt(playerid, "AdCooldown", gettime());
		format(string, sizeof(string), "~r~Paid $%s~n~~w~Message contained: %d characters.", getNumberString(payout), len);
		//GameTextForPlayer(playerid, string, 5000, 5);
		ShowScriptMessage(playerid, string, 5000);
		format(string, sizeof(string), "You paid $%s for this ad(%d characters)", getNumberString(payout), len);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ad [message]");
	}
	return 1;
}
*/

YCMD:ad(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to post or view advertisements.");
		return 1;
	}
	if(!IsPlayerInRangeOfPoint(playerid, 10.0, 2253.182617, 25.457563, 5.307678)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not inside Fox News!");
		return 1;
	}
	showAdvertisementMenu(playerid);
	return 1;
}
/*
//Gun Out CMD
YCMD:gunout(playerid, params[], help) 
{
	new string[128];
	new gout[10];
    if (!sscanf(params, "s[10]", gout))
    {
        if(!strcmp(gout, "ak47", true) || !strcmp(gout, "ak-47", true))
        {
			format(string,sizeof(string), "* %s takes out an AK-47 from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "m4", true))
        {
			format(string,sizeof(string), "* %s takes out an M4 from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "sniper", true))
        {
			format(string,sizeof(string), "* %s takes out a sniper rifle from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "rifle", true))
        {
			format(string,sizeof(string), "* %s takes out a hunting rifle from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "eagle", true))
        {
			format(string,sizeof(string), "* %s takes out a .50 AE Desert Eagle from %s gun holster and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "shotgun", true) || !strcmp(gout, "sgun", true))
		{
			format(string,sizeof(string), "* %s takes out a shotgun from %s back and cocks it.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "spas12", true) || !strcmp(gout, "s12", true))
		{
			format(string,sizeof(string), "* %s takes out an automated pump shotgun from %s back and cocks it.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
        else if(!strcmp(gout, "mp5", true))
        {
			format(string,sizeof(string), "* %s takes out an MP5 from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
        else if(!strcmp(gout, "uzi", true))
        {
			format(string,sizeof(string), "* %s takes out an UZI from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
        else if(!strcmp(gout, "tec9", true))
        {
			format(string,sizeof(string), "* %s takes out a TEC-9 from %s back and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "9mm", true))
        {
			format(string,sizeof(string), "* %s takes out a 9mm from %s gun holster and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "s9mm", true))
        {
			format(string,sizeof(string), "* %s takes out a silenced 9mm from %s gun holster and flips the safety switch off.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "bat", true))
        {
			format(string,sizeof(string), "* %s takes out a baseball bat.", GetPlayerNameEx(playerid,ENameType_RPName));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else if(!strcmp(gout, "rpg", true))
        {
			format(string,sizeof(string), "* %s slides an RPG off from %s back", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else
		{
		    SendClientMessage(playerid, COLOR_LIGHTRED, "	Invalid gun name!");
		    return 1;
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /Gout [Gun Name]");
	    SendClientMessage(playerid, COLOR_LIGHTBLUE, "Gun Names: AK-47 (ak47), M4, Sniper, Rifle, Eagle, Shotgun (sgun), Spas12, MP5, 9mm, s9mm, bat, tec9, uzi, rpg");
	}
	return 1;
}

YCMD:gunin(playerid, params[],help)
{
	new gout[10];
	new string[128];
    if (!sscanf(params, "s[10]", gout))
    {
        if(!strcmp(gout, "ak47", true) || !strcmp(gout, "ak-47", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s AK-47 on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "m4", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s M4 on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "sniper", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s sniper on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "rifle", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s hunting rifle on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "eaglec", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s .50 AE Desert Eagle back on %s gun holster.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "eagle", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on lifts %s shirt and places %s .50 AE Desert Eagle under it.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "shotgun", true) || !strcmp(gout, "sgun", true))
		{
			format(string,sizeof(string), "* %s places %s shotgun on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "spas12", true) || !strcmp(gout, "s12", true))
		{
			format(string,sizeof(string), "* %s places %s automated pump shotgun on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
        else if(!strcmp(gout, "mp5", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s MP5 under %s shirt.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "mp5c", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s MP5 on %s back.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "9mm", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on, lifts %s shirt and places %s 9mm under it.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "9mmc", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s 9mm back on %s gun holster.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "s9mm", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s silenced 9mm back on %s gun holster.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "bat", true))
        {
			format(string,sizeof(string), "* %s puts down the baseball bat.", GetPlayerNameEx(playerid,ENameType_RPName));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "uzi", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s UZI under %s shirt.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "tec9", true))
        {
			format(string,sizeof(string), "* %s flips the safety switch on and places %s TEC-9 under %s shirt.", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		}
		else if(!strcmp(gout, "rpg", true))
        {
			format(string,sizeof(string), "* %s slides an RPG on %s back", GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid));
			ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			SetPlayerChatBubble(playerid, string, COLOR_PURPLE, 3.0, 5000);
		}
		else
		{
		    SendClientMessage(playerid, COLOR_LIGHTRED, "	Invalid gun name!");
		    return 1;
		}
	}
	else
	{
	    SendClientMessage(playerid, COLOR_WHITE, "USAGE: /gin [Gun Name] or / gunin [Gun Name]");
	    SendClientMessage(playerid, COLOR_LIGHTBLUE, "Gun Names: AK-47 (ak47), M4, Sniper, Rifle, Eaglec, Eagle, Shotgun (sgun), Spas12, MP5, 9mm, 9mmc, s9mm, bat, tec9, uzi, rpg");
	}
	return 1;
}
*/
YCMD:kill(playerid, params[], help)
{
    if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT && IsPlayerBlocked(playerid) == 0 && !isInPaintball(playerid))
	{
		SetPlayerHealthEx(playerid, 0);
		GiveMoneyEx(playerid, -100);
		format(query, sizeof (query), "* %s Takes a weapon out and kills himself", GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(30.0,playerid, query, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE, COLOR_PURPLE);
		SendClientMessage(playerid, COLOR_GRAD1, "Because you topped yourself you lost 100 dollars.");
		return 1;
	}
	else
	{
		SendClientMessage(playerid, COLOR_LIGHTRED, "	You can't do that right now!");
	}
	return 1;
}
isJailed(playerid) {
	return GetPVarType(playerid, "ReleaseTime") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "AJailReleaseTime") != PLAYER_VARTYPE_NONE || playerHasPrisonLifeSentence(playerid);
}
YCMD:charity(playerid, params[], help) {
	new amount;
	new msg[128];
	if(!sscanf(params,"d",amount)) {
		if(amount < 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot donate less than $1 to charity.");
			return 1;
		}
		if(GetMoneyEx(playerid) < amount) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
			return 1;
		}
		GiveMoneyEx(playerid, -amount);
		format(msg, sizeof(msg), "You have donated $%s to charity.",getNumberString(amount));
		SendClientMessage(playerid, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /charity [amount]");
	}
	return 1;
}
