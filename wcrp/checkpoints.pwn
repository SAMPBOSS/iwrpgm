#define MAX_CPS 6000

new StandingCP[MAX_PLAYERS];

enum ECPType {
	ECPType_None = 0,
	ECPType_Job,
	ECPType_House,
	ECPType_Business,
	ECPType_Evidence,
	ECPType_Other,
};
enum ECheckPoints {
	CPID,
	Float: CPX,
	Float: CPY,
	Float: CPZ,
	CPInt,
	CPVW,
	ECPType:CPType,
	CPJobType,
	CPOwner,
	IsRaceCP,
};
new CheckPoints[MAX_CPS][ECheckPoints];

checkpointsOnGameModeInit() {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		CheckPoints[i][CPType] = ECPType_None;
		CheckPoints[i][CPID] = -1;
	}
	return 1;
}
SetSinglePlayerCP(playerid, Float: X, Float: Y, Float: Z, ECPType:type = ECPType_Other, jobid = -1, Float: size = 5.0, worldid = -1, interiorid = -1, Float:streamdistance = 2000.0) {
	DestroyAllPlayerCPS(playerid); //Delete them all first
	return CreateCP(playerid, X, Y, Z, type, jobid, size, worldid, interiorid, streamdistance);
}
CreateCP(playerid = -1, Float: X, Float: Y, Float: Z, ECPType:type = ECPType_Other, jobid = -1, Float: size = 5.0, worldid = -1, interiorid = -1, Float:streamdistance = MAXDEF_STREAM_DISTANCE) {
	new index = getFreeCPIndex();
	if(index != -1) {
		CheckPoints[index][CPID] = CreateDynamicCP(X, Y, Z, size, worldid, interiorid, playerid, streamdistance);
		CheckPoints[index][CPType] = type;
		CheckPoints[index][CPJobType] = jobid;
		CheckPoints[index][CPOwner] = playerid;
		CheckPoints[index][IsRaceCP] = false;
	} else {
		printf("CreateCP: Checkpoint array is full!");
	}
	return index; //Return the index for this cp
}
getCPIndexFromStreamerID(checkpointid) {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] != -1) {
			if(CheckPoints[i][CPID] == checkpointid) {
				return i;
			}
		}
	}
	return -1;
}
SetSingleAngledPlayerCP(playerid, Float: X, Float: Y, Float: Z, Float: Angle, ECPType:type = ECPType_Other, jobid = -1, Float: size = 5.0, worldid = -1, interiorid = -1, Float:streamdistance = 2000.0) {
	DestroyAllPlayerCPS(playerid); //Delete them all first
	return CreateAngledCP(playerid, 0, X, Y, Z, Angle, type, jobid, size, worldid, interiorid, streamdistance);
}
CreateAngledCP(playerid = -1, cptype = 0, Float: X, Float: Y, Float: Z, Float: Angle, ECPType:type = ECPType_Other, jobid = -1, Float: size = 5.0, worldid = -1, interiorid = -1, Float:streamdistance = MAXDEF_STREAM_DISTANCE) {
	new Float: nextx, Float:nexty, Float:nextz;
	new index = getFreeCPIndex();
	GetPosFromAngle(X, Y, Z, nextx, nexty, nextz, Angle, 6.0);
	if(index != -1) {
		CheckPoints[index][CPID] = CreateDynamicRaceCP(cptype, X, Y, Z, nextx, nexty, nextz, size, worldid, interiorid, playerid, streamdistance);
		CheckPoints[index][CPType] = type;
		CheckPoints[index][CPJobType] = jobid;
		CheckPoints[index][CPOwner] = playerid;
		CheckPoints[index][IsRaceCP] = true;
	} else {
		printf("CreateAngledCP: Checkpoint array is full!");
	}
	return index; //Return the index for this cp
}
DestroyCP(index) {
	#if debug
	printf("Successfully destroyed checkpoint with id %d!", index);
	#endif
	if(!CheckPoints[index][IsRaceCP]) {
		DestroyDynamicCP(CheckPoints[index][CPID]);
	} else {
		DestroyDynamicRaceCP(CheckPoints[index][CPID]);
	}
	CheckPoints[index][IsRaceCP] = false;
	CheckPoints[index][CPType] = ECPType_None;
	CheckPoints[index][CPJobType] = -1;
	CheckPoints[index][CPOwner] = -1;
	CheckPoints[index][CPID] = -1;
}
DestroyAllCPS() {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] != -1) {
			DestroyCP(i);
		}
	}
}
getFreeCPIndex() {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] == -1) {
			return i;
		}
	}
	return -1;
}
IsCheckPointInType(index, ECPType:type) {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] != -1) {
			if(CheckPoints[i][CPID] == index) {
				if(CheckPoints[i][CPType] == type) {
					return 1;
				}
			}
		}
	}
	return 0;
}
IsCheckPointForJob(index, jobType) {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] != -1) {
			if(CheckPoints[i][CPID] == index) {
				if(CheckPoints[i][CPJobType] == jobType) {
					return 1;
				}
			}
		}
	}
	return 0;
}
DestroyAllPlayerCPS(playerid) {
	for(new i=0; i<sizeof(CheckPoints); i++) {
		if(CheckPoints[i][CPID] != -1) {
			if(CheckPoints[i][CPOwner] == playerid) {
				DestroyCP(i);
			}
		}
	}
	return 1;
}
cpsOnPlayerEnterDynamicCP(playerid, checkpointid) { //Simply updates standingcp when the player enters a checkpoint
	if(IsValidDynamicCP(checkpointid)) {
		StandingCP[playerid] = checkpointid;
	}
}
getLastCheckPoint(playerid) {
	return StandingCP[playerid];
}
cpsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	DestroyAllPlayerCPS(playerid); //Delete all their checkpoints
}