enum EDiseaseFlags (<<= 1) {
	EDFI_STD = 0, //sex disease
	EDFI_Blood = 1, //if you are shot or something, you might get it
	EDFI_Airbourne, //really likely of getting it typically, you can RANDOMLY get it
	EDFI_Uncureable, //no known cure
	EDFI_NoHealMe, //a medic must cure it
	EDFI_Blindness, //cause blindess
	EDFI_RemoveHealth,
	EDFI_Deaf,
	EDFI_Hallucinations,
	EDFI_InstantDeath, //in /accept death you're just dead.
	EDFI_MustCK, //never lose, or go away
	EDFI_ProximitySpread, //spread if you're near by
	EDFI_Uninsured, //insurance doesn't covers it
	EDFI_Seizures, //gives you random seizures
	EDFI_Tourettes, //tourettes like symptoms
	EDFI_RandomDrunk,
	EDFI_HeartAttacks,
	EDFI_Gibbs, //gibbs messages
	EDFI_Drugs, //Drug Effects
};


enum EPlayerDiseaseInfo {
	EPDiseaseIndex,
	EPDiseaseTime, //time you caught
	EPDormentTime, //countdown in seconds, when zero the disease is "active"
	EPDeathWait, //seconds countdown, if 0 then 
};

new playerDisease[MAX_PLAYERS][EPlayerDiseaseInfo];

enum EDiseaseInfo {
	EDIName[64],
	EDIAirborneRisk, //factor of RANDOMLY catching it
	EDITransmissionChance, //chances of getting it
	EDIDiseaseLength, //how long until you die, or it goes away
	EDIDormentTime, //how long in seconds(typically 6-12 hours)
	EDIDeathTime, //how long in seconds until you start dying
	EDiseaseFlags:EDIFlags,
	EDI_HealMeCost,
	EDI_BaseCost, //actual cost for a paramedic to heal
	EDI_TreatmentCost, //if you can get pills
	Float:EDI_HealthRemove,
	EDI_Season,
};
enum {
	DISEASE_COLD,
	DISEASE_DRUGGIE,
	DISEASE_COCAINE,
	DISEASE_METH,
	DISEASE_ALCOHOLIC,
}
new Diseases[][EDiseaseInfo] = {
	{"Common Cold",75,75,120,120,1,EDFI_Airbourne,100,100,100,2.0,_:ESeasonType_Winter},
	{"Drug Addict",-1,-1,1800,120,1,EDFI_NoHealMe|EDFI_Drugs|EDFI_Hallucinations,100,100,100,2.0,-1},
	{"Cocaine",-1,-1,1800,120,1,EDFI_NoHealMe|EDFI_Drugs|EDFI_Hallucinations,100,100,100,2.0,-1}, //Makes the player a bit stronger
	{"Meth",-1,-1,1800,120,1,EDFI_NoHealMe|EDFI_Drugs|EDFI_Hallucinations,100,100,100,2.0,-1}, //Makes the player a bit stronger
	{"Alcoholic",-1,-1,900,120,1,EDFI_NoHealMe|EDFI_RandomDrunk,100,100,100,2.0,-1}
};
loadDiseases(playerid) {
	format(query, sizeof(query), "SELECT `disease`,`diseasetime`,`dormenttime`,`deathwait` FROM `characters` WHERE `id` = %d",GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadDiseases", "d",playerid);
}
saveDiseases(playerid) {
	format(query, sizeof(query), "UPDATE `characters` SET `disease` = %d, `diseasetime` = %d, `dormenttime` = %d, `deathwait` = %d WHERE `id` = %d",playerDisease[playerid][EPDiseaseIndex],playerDisease[playerid][EPDiseaseTime],playerDisease[playerid][EPDormentTime],playerDisease[playerid][EPDeathWait], GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
YCMD:disease(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "");
		return 1;
	}
	new d, target;
	if(!sscanf(params, "k<playerLookup_acc>d", target, d)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(d < 0 || d > sizeof(Diseases)) {
			curePlayerDisease(target);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Disease Removed!");
		} else {
			giveDisease(target, d, 1);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Disease Given!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /disease [playerid] [disease]");
	}
	return 1;
}
forward OnLoadDiseases(playerid);
public OnLoadDiseases(playerid) {
	new disease, dtime, dortime, id_string[128];
	new rows, fields;
	cache_get_data(rows, fields);
	playerDisease[playerid][EPDiseaseIndex] = -1;
	playerDisease[playerid][EPDiseaseTime] = 0;
	playerDisease[playerid][EPDormentTime] = 0;
	playerDisease[playerid][EPDeathWait] = 0;
	if(rows > 0) {
		cache_get_row(0, 0, id_string);
		disease = strval(id_string);
		cache_get_row(0, 1, id_string);
		dtime = strval(id_string);
		cache_get_row(0, 2, id_string);
		dortime = strval(id_string);
		
		playerDisease[playerid][EPDiseaseIndex] = disease;
		playerDisease[playerid][EPDiseaseTime] = dtime;
		playerDisease[playerid][EPDormentTime] = dortime;
	}
}
DiseaseTimer() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			diseaseCheck(i);
		}
	}
}
diseaseCheck(playerid) {
	new disease;
	new DrugFlags:aflags = DrugFlags:GetPVarInt(playerid, "drug_effects");
	if(aflags & EDrugEffect_TreatDiseases) {
		if(!playerHasUncureableDisease(playerid)) return 1;
	}	
	if((disease = getPlayerDisease(playerid)) == -1) {
		for(new i=0;i<sizeof(Diseases);i++) {
			if(playerGetsDisease(playerid, i)) {
				giveDisease(playerid, i, 1);
				continue;
			}
		}
	} else {
		if(playerDisease[playerid][EPDormentTime] > 0) {
			playerDisease[playerid][EPDormentTime]--;
			if(Diseases[disease][EDIFlags] & EDFI_ProximitySpread) {
				infectNearbyPlayers(playerid, disease);
			}
		} else {
			if(playerDisease[playerid][EPDiseaseTime] > 0) {
				playerDisease[playerid][EPDiseaseTime]--;
				doDiseaseEffect(playerid, disease);
			}
		}
	}
	return 1;
}
isPlayerDiseaseDorment(playerid) {
	return playerDisease[playerid][EPDormentTime] > 0;
}
curePlayerDisease(playerid) {
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		playerDisease[playerid][EPDiseaseIndex] = -1;
		playerDisease[playerid][EPDiseaseTime] = 0;
		playerDisease[playerid][EPDormentTime] = 0;
		playerDisease[playerid][EPDeathWait] = 0;
	}
}
playerDiseaseNoHealMe(playerid) {
	if(playerHasUncureableDisease(playerid)) return 1;
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(Diseases[disease][EDIFlags] & EDFI_NoHealMe) {
			return 1;
		}
	}
	return 0;	
}
playerDiseaseInstantDeath(playerid) {
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(Diseases[disease][EDIFlags] & EDFI_InstantDeath) {
			return 1;
		}
	}
	return 0;	
}
removeDiseaseOnDeath(playerid) {
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(~Diseases[disease][EDIFlags] & EDFI_InstantDeath) {
			return 1;
		}
	}
	return 0;	
}
playerHasUncureableDisease(playerid) {
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(Diseases[disease][EDIFlags] & EDFI_Uncureable) {
			return 1;
		}
	}
	return 0;	
}
playerHasGibs(playerid) {
	new disease = getPlayerDisease(playerid);
	if(disease != -1) {
		if(Diseases[disease][EDIFlags] & EDFI_Gibbs) {
			return 1;
		}
	}
	return 0;
}
forward GibsMessage(string[]);
public GibsMessage(string[]) {
	new alpha[] = "abcdefghijklmnopqrstuvwxyz";
	new was_upper;
	new curchar;
	for(new x=0;x<strlen(string);x++) {
		curchar = string[x];
		was_upper = curchar < 'a' ? true : false;
		curchar = alpha[(curchar + random(sizeof(alpha))) % sizeof(alpha)];
		string[x] = curchar;
		if(was_upper) {
			string[x] = toupper(string[x]);
		}
	}
}
doDiseaseEffect(playerid, disease) {
	new shoulddo = random(2);
	if(shoulddo == 1) {
		if(playerDisease[playerid][EPDeathWait] < 1) {
			if(Diseases[disease][EDIFlags] & EDFI_RemoveHealth) {
				new rhealth  = random(2);
				if(rhealth == 1) {
					new Float:HP;
					GetPlayerHealth(playerid, HP);
					HP -= Diseases[disease][EDI_HealthRemove];
					SetPlayerHealthEx(playerid, HP);
				}
			}
		} else {
			playerDisease[playerid][EPDeathWait]--;
		}
		if(Diseases[disease][EDIFlags] & EDFI_RandomDrunk) {
			new rdrunk  = random(25);
			if(rdrunk == 9) {
				SetPlayerDrunkLevel(playerid, 20000);
			}
		}
	}
}
new TourettesMsgs[][64] = {
{"Feel"},
{"Free"},
{"To"},
{"Get"},
{"Creative"},
{"Here"}
};
sendTourettesMessage(playerid) {
	new n = RandomEx(0, sizeof(TourettesMsgs)+1);
	RPOnPlayerText(playerid, TourettesMsgs[n]);
}
//for airbourne infection
infectNearbyPlayers(playerid, disease) {
	new Float:X, Float:Y, Float:Z, vw;
	GetPlayerPos(playerid, X, Y, Z);
	vw = GetPlayerVirtualWorld(playerid);
	foreach(Player, i) {
		if(IsPlayerInRangeOfPoint(i, 5.0, X, Y, Z)) {
			if(vw == GetPlayerVirtualWorld(i)) {
				if(playerGetsDisease(i, disease, 1) && i != playerid) {
					giveDisease(i, disease);
				}
			}
		}
	}
}
getPlayerDisease(playerid) {
	return playerDisease[playerid][EPDiseaseIndex];
}
giveDisease(playerid, disease, showmsg = 0) {
	new msg[128];
	new Year, Month, Day;
	getdate(Year, Month, Day);
	new season = getActualSeason(Month);
	if(Diseases[disease][EDI_Season] != season && Diseases[disease][EDI_Season] != -1) { //Check for season.. Some diseases only happen on certain seasons
		return 1;
	}
	playerDisease[playerid][EPDiseaseIndex] = disease;
	playerDisease[playerid][EPDiseaseTime] = Diseases[disease][EDIDiseaseLength];
	playerDisease[playerid][EPDormentTime] = Diseases[disease][EDIDormentTime];
	if(showmsg) {
		format(msg, sizeof(msg), "You have caught the %s disease.", Diseases[disease][EDIName]);
		ShowScriptMessage(playerid, msg);
	}
	return 1;
}
playerGetsDisease(playerid, disease, airborne = 0) {
	#pragma unused playerid
	new a = RandomEx(0,Diseases[disease][EDIAirborneRisk]+1);
	new b = RandomEx(0,Diseases[disease][EDITransmissionChance]+1);
	if(airborne) {
		if(Diseases[disease][EDIFlags] & EDFI_Airbourne) {
			if(a == Diseases[disease][EDIAirborneRisk]) {
				return 1;
			}
		}
	} else {
		if(b == Diseases[disease][EDITransmissionChance]) {
			return 1;
		}
	}
	return 0;
}
diseaseHealMeCost(disease) {
	return Diseases[disease][EDI_HealMeCost];
}
diseaseInsured(disease) {
	if(Diseases[disease][EDIFlags] & EDFI_Uninsured) {
		return 0;
	}
	return 1;
}