#define MAX_CAR_TOYS_PER_CAR 5
enum ECarToyInfo {
	ECarToy_ModelID,
	ECarToy_Price,
	ECarToy_PriceDPs,
	ECarToy_Name[32],
};
new CarToys[][ECarToyInfo] = {
	{19419,750000,100,"Cop Lights (Illegal)"},
	//{18647, 50000, 10, "Red Neon (Illegal)"},
	//{18648, 50000, 10, "Blue Neon"},
	//{18649, 50000, 10, "Green Neon"},
	//{18650, 50000, 10, "Yellow Neon"},
	//{18651, 50000, 10, "Pink Neon"},
	//{18652, 50000, 10, "White Neon"},
	{19308, 5000, 5, "Yellow/Black Taxi Sign"},
	{19309, 5000, 5, "Black/White Taxi Sign"},
	{19310, 5000, 5, "White/Black Taxi Sign"},
	{19311, 5000, 5, "Black/Yellow Taxi Sign"}
	//{19314, 7500, 10, "Bullhorn"}
};

new EditingCarToy[MAX_PLAYERS];

enum EVehCarToyInfo {
	EVehCarToy_ObjID,
	Float:EVehCarToy_X,
	Float:EVehCarToy_Y,
	Float:EVehCarToy_Z,
	Float:EVehCarToy_RX,
	Float:EVehCarToy_RY,
	Float:EVehCarToy_RZ,
	EVehCarToy_ToyIdx,
	EVehCarToy_SQLID,
};
new VehicleCarToys[MAX_VEHICLES][MAX_CAR_TOYS_PER_CAR][EVehCarToyInfo];

enum {
	ECarToys_ToyActionMenu = ECarToys_Base + 1,
	ECarToys_EditMenu,
};

loadOwnedCarToys(carid) {
	new sqlid = GetVehicleSQLID(carid);
	if(sqlid == -1) return 0;
	format(query, sizeof(query), "SELECT `X`,`Y`,`Z`,`RX`,`RY`,`RZ`,`id`,`toyidx` FROM `playercartoys` WHERE `carid` = %d",sqlid);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadCarToys", "d",carid);
	return 1;
}
forward OnLoadCarToys(carid);
public OnLoadCarToys(carid) {
	new toyidx, Float:X, Float:Y, Float:Z, Float:RX, Float:RY, Float:RZ;
	new rows, columns;
	new rowdata[32];
	cache_get_data(rows, columns);
	for(new i=0;i<rows;i++) {
	
		cache_get_row(i,0,rowdata);
		X = floatstr(rowdata);
		
		cache_get_row(i,1,rowdata);
		Y = floatstr(rowdata);
		
		cache_get_row(i,2,rowdata);
		Z = floatstr(rowdata);
		
		cache_get_row(i,3,rowdata);
		RX = floatstr(rowdata);
		
		cache_get_row(i,4,rowdata);
		RY = floatstr(rowdata);
		
		cache_get_row(i,5,rowdata);
		RZ = floatstr(rowdata);
		
		new sqlid;
		cache_get_row(i,6,rowdata);
		sqlid = strval(rowdata);
		
		cache_get_row(i,7,rowdata);
		toyidx = strval(rowdata);
		
		new slot = AddVehicleCarToy(carid, toyidx, 2);
		if(slot == -1) continue;
		VehicleCarToys[carid][slot][EVehCarToy_SQLID] = sqlid;
		SetCarObjectPos(carid, slot, VehicleCarToys[carid][slot][EVehCarToy_ObjID], X, Y, Z, RX, RY, RZ);
	}
	if(isVehicleInImpound(carid) != 0) {
		HideCarToys(carid, 1);
	}
}
carToyIdxFromModel(modelid) {
	for(new i=0;i<sizeof(CarToys);i++) {
		if(CarToys[i][ECarToy_ModelID] == modelid)
			return i;
	}
	return -1;
}
DestroyVehicleToys(carid, delete = 0) {
	for(new i=0;i<MAX_CAR_TOYS_PER_CAR;i++) {
		if(VehicleCarToys[carid][i][EVehCarToy_ObjID] != 0) {
			DestroyCarToy(VehicleCarToys[carid][i][EVehCarToy_ObjID], delete);
			VehicleCarToys[carid][i][EVehCarToy_SQLID] = 0; //just incase it was hidden, so the slot can be used by the next car
		}			
	}
}
CarToysOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	new carid = GetPlayerVehicleID(playerid);
	switch(dialogid) {
		case ECarToys_EditMenu: {
			new action = GetPVarInt(playerid, "CarToyAction");
			DeletePVar(playerid, "CarToyAction");
			if(!response) return 0;
			if(carid == INVALID_VEHICLE_ID || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
				SendClientMessage(playerid, X11_TOMATO_2, "* You must be in a vehicle!");
				return 1;
			}
			new x;
			for(new i=0;i<MAX_CAR_TOYS_PER_CAR;i++) {
				if(VehicleCarToys[carid][i][EVehCarToy_ObjID] != 0) {
					if(x++ == listitem) {
						if(action == 0) { //edit
							RespawnCarObjectForMoving(VehicleCarToys[carid][i][EVehCarToy_ObjID]);
						} else { //delete
							DestroyCarToy(VehicleCarToys[carid][i][EVehCarToy_ObjID]);
						}
					}
				}
			}
		}
		case ECarToys_ToyActionMenu: {
			if(!response) return 0;
			SetPVarInt(playerid, "CarToyAction", listitem);
			ShowCarToyEditMenu(playerid);
		}
	}
	return 1;
}

enum ECarToysMenuState {
	ECTMS_CurPage,
	ECTMS_RemainingCount,
	ECTMS_PageCount,
	ECTMS_InPreviewMode,
	ECTMS_VIPStore,
};
new CarToysMenuState[MAX_PLAYERS][ECarToysMenuState];
setupCarToysMenuState(playerid, curpage, isVIP = 0) {
	new remaining_amount, pages, numcartoys;
	new start_index = 0;
	new temptxt[128];

	new vehstr[32],vehprice[32];

	CarToysMenuState[playerid][ECTMS_VIPStore] = isVIP;

	for(new i=0,c=0,j=1;i<sizeof(CarToys);i++) {
		if(((CarToys[i][ECarToy_Price] != 0 && CarToysMenuState[playerid][ECTMS_VIPStore] == 0) || (CarToys[i][ECarToy_PriceDPs] != 0 && CarToysMenuState[playerid][ECTMS_VIPStore] == 1))) {
			if(c++ >= (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS)*j) {
				if(++j == curpage)
					start_index = i;
			}
			numcartoys++;
		}
	}

	clearModelSelectMenu(playerid);
	if(curpage < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	for(new i=start_index,x=0;x<numcartoys&&i<sizeof(CarToys);i++) {
			format(vehstr,sizeof(vehstr),"%s",CarToys[i][ECarToy_Name]);
			if(CarToysMenuState[playerid][ECTMS_VIPStore] == 0) {
				format(vehprice,sizeof(vehprice), "~g~$%s",getNumberString(CarToys[i][ECarToy_Price]));
			} else {
				format(vehprice,sizeof(vehprice), "~g~%s DPs",getNumberString(CarToys[i][ECarToy_PriceDPs]));		
			}			
			addItemToModelSelectMenu(playerid, CarToys[i][ECarToy_ModelID], vehprice,vehstr);
			x++;
	}

	remaining_amount = numcartoys % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numcartoys) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	CarToysMenuState[playerid][ECTMS_CurPage] = curpage;
	CarToysMenuState[playerid][ECTMS_PageCount] = pages;
	CarToysMenuState[playerid][ECTMS_RemainingCount] = remaining_amount;
	CarToysMenuState[playerid][ECTMS_InPreviewMode] = 0;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numcartoys, "Car Toys Menu", page_str, "OnCarToysMenuSelect", -5.0000, 25.0000, 18.0000);
}
forward OnCarToysMenuSelect(playerid, action, modelid, title[]);
public OnCarToysMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			CarToysMenuState[playerid][ECTMS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
			new buymode = CarToysMenuState[playerid][ECTMS_VIPStore];
			new price;
			new carid = GetPlayerVehicleID(playerid);
			if(carid == INVALID_VEHICLE_ID || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
				SendClientMessage(playerid, X11_TOMATO_2, "* You must be in a vehicle!");
				return 1;
			}
			new idx = carToyIdxFromModel(CarToysMenuState[playerid][ECTMS_InPreviewMode]);
			if(idx == -1) {
				SendClientMessage(playerid, X11_TOMATO_2, "* Internal script error, please make a forum report! (Take SS)!");
				return 1;
			}
			if(buymode == 0) { //money
				price = CarToys[idx][ECarToy_Price];
				if(GetMoneyEx(playerid) < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
					return 1;
				}
				GiveMoneyEx(playerid, -price);
				increaseJobPoints(playerid); //inc mechanic points
			} else if(buymode == 1) { //dps
				new dps = GetPVarInt(playerid, "DonatePoints");
				price = CarToys[idx][ECarToy_PriceDPs];
				if(dps < price) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough donator points!");
					return 1;
				}
				dps -= price;
				SetPVarInt(playerid, "DonatePoints", dps);
			}
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
			AddVehicleCarToy(carid, idx);
	} else if(action == EModelPreviewAction_Next) {
		setupCarToysMenuState(playerid, CarToysMenuState[playerid][ECTMS_CurPage] + 1, CarToysMenuState[playerid][ECTMS_VIPStore]);
	} else if(action == EModelPreviewAction_Back) {
		setupCarToysMenuState(playerid, CarToysMenuState[playerid][ECTMS_CurPage] - 1, CarToysMenuState[playerid][ECTMS_VIPStore]);
	} else if(action == EModelPreviewAction_Exit) {
		if(CarToysMenuState[playerid][ECTMS_InPreviewMode] != 0) {
			setupCarToysMenuState(playerid, CarToysMenuState[playerid][ECTMS_CurPage], CarToysMenuState[playerid][ECTMS_VIPStore]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
}


ShowCarToyBuyMenu(playerid, dps = 0) {
	setupCarToysMenuState(playerid, 1, dps);
}
YCMD:cartoys(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You must be in a vehicle!");
		return 1;
	}
	ShowPlayerDialog(playerid, ECarToys_ToyActionMenu, DIALOG_STYLE_LIST, "Car Toy Action Menu","Edit Toy\nDelete Toy\n","Select", "Cancel");
	return 1;
}
ShowCarToyEditMenu(playerid) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be in a vehicle!");
		return 1;
	}
	tempstr[0] = 0;
	dialogstr[0] = 0;
	for(new i=0;i<MAX_CAR_TOYS_PER_CAR;i++) {
		if(VehicleCarToys[carid][i][EVehCarToy_ObjID] != 0) {
			format(tempstr, sizeof(tempstr), "%s\n",CarToys[VehicleCarToys[carid][i][EVehCarToy_ToyIdx]][ECarToy_Name]);
			strcat(dialogstr, tempstr, sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, ECarToys_EditMenu, DIALOG_STYLE_LIST, "Select Car Toy",dialogstr,"Select", "Cancel");
	return 1;
}
YCMD:coplights(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You must be in a vehicle!");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
		return 1;
	}
	if(!IsOnDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
		return 1;
	}
	AddVehicleCarToy(carid, 0, 1);
	return 1;
}
YCMD:buycartoy(playerid, params[], help) {
	new carid = GetPlayerVehicleID(playerid);
	if(carid == INVALID_VEHICLE_ID || GetPlayerState(playerid) != PLAYER_STATE_DRIVER) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You must be in a vehicle!");
		return 1;
	}
	new dps = IsPlayerInRangeOfPoint(playerid, 5.0, 1264.86, -1669.73, 13.54);
	new job = GetPVarInt(playerid, "Job");
	if(job != EJobType_Mechanic && !dps) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a mechanic for this!");
		return 1;
	}
	if(VehicleInfo[carid][EVType] != EVehicleType_Owned) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You can only use this on owned vehicles");
		return 1;
	}
	ShowCarToyBuyMenu(playerid, dps);
	return 1;
}
DestroyCarToy(objid, sqldelete = 1) {
	new carid;
	new obj = findCarToyByObjID(objid,carid);
	if(obj == -1) return 0;
	VehicleCarToys[carid][obj][EVehCarToy_ObjID] = 0;
	if(VehicleCarToys[carid][obj][EVehCarToy_SQLID] > 0 && sqldelete) {
		format(query, sizeof(query), "DELETE FROM `playercartoys` WHERE `id` = %d",VehicleCarToys[carid][obj][EVehCarToy_SQLID]);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	}
	VehicleCarToys[carid][obj][EVehCarToy_SQLID] = 0;
	DestroyDynamicObject(objid);
	return 1;
}
findCarToyByObjID(objid, &carid = 0) {
	carid = INVALID_VEHICLE_ID;
	for(new i=0;i<MAX_VEHICLES;i++) {
		for(new x=0;x<MAX_CAR_TOYS_PER_CAR;x++) {
			if(VehicleCarToys[i][x][EVehCarToy_ObjID] == objid) {
				carid = i;
				return x;
			}
		}
	}
	return -1;
}
findFreeCarToySlot(carid) {
	for(new i=0;i<MAX_CAR_TOYS_PER_CAR;i++) {
		if(VehicleCarToys[carid][i][EVehCarToy_ObjID] == 0 && VehicleCarToys[carid][i][EVehCarToy_SQLID] == 0) {
			return i;
		}
	}
	return -1;
}
SaveVehicleCarToy(carid, toyidx, update) {
	if(update) {
		format(query, sizeof(query), "UPDATE `playercartoys` SET `X` = %f, `Y` = %f, `Z` = %f, `RX` = %f, `RY` = %f, `RZ` = %f WHERE `id` = %d",VehicleCarToys[carid][toyidx][EVehCarToy_X],VehicleCarToys[carid][toyidx][EVehCarToy_Y],VehicleCarToys[carid][toyidx][EVehCarToy_Z],VehicleCarToys[carid][toyidx][EVehCarToy_RX],VehicleCarToys[carid][toyidx][EVehCarToy_RY],VehicleCarToys[carid][toyidx][EVehCarToy_RZ],VehicleCarToys[carid][toyidx][EVehCarToy_SQLID]);
	} else {
		format(query, sizeof(query), "INSERT INTO `playercartoys` SET `X` = %f, `Y` = %f, `Z` = %f, `RX` = %f, `RY` = %f, `RZ` = %f, `carid` = %d, `toyidx` = %d",VehicleCarToys[carid][toyidx][EVehCarToy_X],VehicleCarToys[carid][toyidx][EVehCarToy_Y],VehicleCarToys[carid][toyidx][EVehCarToy_Z],VehicleCarToys[carid][toyidx][EVehCarToy_RX],VehicleCarToys[carid][toyidx][EVehCarToy_RY],VehicleCarToys[carid][toyidx][EVehCarToy_RZ],GetVehicleSQLID(carid),VehicleCarToys[carid][toyidx][EVehCarToy_ToyIdx]);
	}
	mysql_function_query(g_mysql_handle, query, true, "OnSaveCarToy", "ddd", carid, toyidx, update);
}
forward OnSaveCarToy(carid, toyidx, update);
public OnSaveCarToy(carid, toyidx, update) {
	new id = mysql_insert_id();
	if(!update) {
		VehicleCarToys[carid][toyidx][EVehCarToy_SQLID] = id;
	}
}
AddVehicleCarToy(carid, toyidx, nosql = 0) {
	new Float:CX, Float:CY, Float:CZ;
	new Float:Angle;
	GetVehicleZAngle(carid, Angle);
	
	GetVehiclePos(carid, CX, CY, CZ);
	new playerid = GetVehicleDriver(carid);
	new slot = findFreeCarToySlot(carid);
	if(slot == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You have enough vehicle toys already");
		return -1;
	}
	if(playerid == INVALID_PLAYER_ID && nosql != 2) {
		return -1;
	}
	new objid = CreateDynamicObject(CarToys[toyidx][ECarToy_ModelID], CX, CY, CZ+2.5, 0.0, 0.0, 0.0);
	VehicleCarToys[carid][slot][EVehCarToy_ObjID] = objid;
	VehicleCarToys[carid][slot][EVehCarToy_ToyIdx] = toyidx;
	if(nosql > 0) {
		VehicleCarToys[carid][slot][EVehCarToy_SQLID] = -1;
	} else {
		VehicleCarToys[carid][slot][EVehCarToy_SQLID] = 0;
	}
	if(nosql != 2) { //attaching is handled elsewhere
		EditDynamicObject(playerid, objid);
		EditingCarToy[playerid] = 1;
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Move the object to where you want it on the car.");
	}
	return slot;
}
RespawnCarObjectForMoving(objectid) {
	new carid;
	new cartoy = findCarToyByObjID(objectid,carid);
	
	new Float:CX, Float:CY, Float:CZ;
	new Float:Angle;
	GetVehicleZAngle(carid, Angle);

	GetVehiclePos(carid, CX, CY, CZ);
	DestroyDynamicObject(objectid);
	new playerid = GetVehicleDriver(carid);
	
	new toyidx = VehicleCarToys[carid][cartoy][EVehCarToy_ToyIdx];
	objectid = CreateDynamicObject(CarToys[toyidx][ECarToy_ModelID], CX, CY, CZ+2.5, 0.0, 0.0, 0.0);
	
	VehicleCarToys[carid][cartoy][EVehCarToy_ObjID] = objectid;
	
	if(playerid != INVALID_PLAYER_ID) {
		EditDynamicObject(playerid, objectid);
		EditingCarToy[playerid] = 1;
	} else {
		SetCarObjectPos(carid, cartoy, objectid, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	}
	return 1;
	
}
carToysOnEditObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	if(EditingCarToy[playerid] == 1) {
		new pcarid = GetPlayerVehicleID(playerid), carid;
		new cartoy = findCarToyByObjID(objectid,carid);
		if(pcarid == INVALID_VEHICLE_ID || pcarid != carid) {
			DestroyCarToy(objectid);
			SendClientMessage(playerid, X11_TOMATO_2, "You are not in a vehicle, car toy destroyed!");
			return 1;
		}
		if(response == EDIT_RESPONSE_FINAL || response == EDIT_RESPONSE_CANCEL) {
			EditingCarToy[playerid] = 0;
			new Float:cx, Float:cy, Float:cz;
			GetVehiclePos(pcarid, cx, cy, cz);
			
			if(GetPointDistance(x,y,z, cx, cy, cz) > 3.5) {
				RespawnCarObjectForMoving(objectid);
				SendClientMessage(playerid, X11_TOMATO_2, "* Vehicle toy is too far, please reposition it.");
				return 1;
			}
			ObjectPosToCarSpace(carid, x, y, z, rx, ry, rz);
			SetCarObjectPos(carid, cartoy, objectid, x, y, z, rx, ry, rz);
			if(VehicleCarToys[carid][cartoy][EVehCarToy_SQLID] != -1)
				SaveVehicleCarToy(carid, cartoy, VehicleCarToys[carid][cartoy][EVehCarToy_SQLID] != 0);
		}/* else if(response == EDIT_RESPONSE_CANCEL) {
			if(VehicleCarToys[carid][cartoy][EVehCarToy_SQLID] == 0) {
				SendClientMessage(playerid, X11_TOMATO_2, "* Vehicle Toy adding cancelled");
				DestroyCarToy(objectid);
			} else {
				SetCarObjectPos(carid, cartoy, objectid, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
			}
		}*/
	}
	return 0;
}
HideCarToys(carid, hide) {
	for(new i=0;i<MAX_CAR_TOYS_PER_CAR;i++) {
		if(VehicleCarToys[carid][i][EVehCarToy_SQLID] != 0) {			
			if(hide == 1) {
				DestroyDynamicObject(VehicleCarToys[carid][i][EVehCarToy_ObjID]);
				VehicleCarToys[carid][i][EVehCarToy_ObjID] = 0;
			} else {
				if(VehicleCarToys[carid][i][EVehCarToy_ObjID] == 0) {
					new toyidx = VehicleCarToys[carid][i][EVehCarToy_ToyIdx];
					new Float:CX, Float:CY, Float:CZ;
					GetVehiclePos(carid, CX, CY, CZ);
					new objid = CreateDynamicObject(CarToys[toyidx][ECarToy_ModelID], CX, CY, CZ+2.5, 0.0, 0.0, 0.0);
					VehicleCarToys[carid][i][EVehCarToy_ObjID] = objid;
					SetCarObjectPos(carid, i, objid, VehicleCarToys[carid][i][EVehCarToy_X], VehicleCarToys[carid][i][EVehCarToy_Y], VehicleCarToys[carid][i][EVehCarToy_Z], VehicleCarToys[carid][i][EVehCarToy_RX], VehicleCarToys[carid][i][EVehCarToy_RY], VehicleCarToys[carid][i][EVehCarToy_RZ]);
				}
			}
		}
	}
}
SetCarObjectPos(carid, cartoy, objectid, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	VehicleCarToys[carid][cartoy][EVehCarToy_X] = x;
	VehicleCarToys[carid][cartoy][EVehCarToy_Y] = y;
	VehicleCarToys[carid][cartoy][EVehCarToy_Z] = z;
	VehicleCarToys[carid][cartoy][EVehCarToy_RX] = rx;
	VehicleCarToys[carid][cartoy][EVehCarToy_RY] = ry;
	VehicleCarToys[carid][cartoy][EVehCarToy_RZ] = rz;
	AttachDynamicObjectToVehicle(objectid, carid, x, y, z, rx, ry, rz);
}
ObjectPosToCarSpace(carid, &Float:X, &Float:Y, &Float:Z, &Float:RX, &Float:RY, &Float:RZ) {
	new Float:CX, Float:CY, Float:CZ;
	new Float:CRZ;
	GetVehiclePos(carid, CX, CY, CZ);
	GetVehicleZAngle(carid, CRZ);
	
	X = X-CX;
	Y = Y-CY;
	Z = Z-CZ;
	
	X = X*floatcos(CRZ, degrees)+Y*floatsin(CRZ, degrees);
	Y = -X*floatsin(CRZ, degrees)+Y*floatcos(CRZ, degrees);
	
	RZ = RZ-CRZ;
}
cartoysOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#pragma unused modelid
	#pragma unused x
	#pragma unused y
	#pragma unused z
	if(EditingCarToy[playerid] == 2) {
		EditingCarToy[playerid] = 1;
		EditDynamicObject(playerid, objectid);		
	}
}