enum EDonatorInfo {
	EDonatorID,
	EDonatorOwner,
	EDonatorStatus, // 0 = disabled, 1 = enabled, -1 = off(won't be here tough, only in mysql)
};

enum EDonatorObjects {
	EDonatorModel,
	Float:EDonatorX,
	Float:EDonatorY,
	Float:EDonatorZ,
	Float:EDonatorRotX,
	Float:EDonatorRotY,
	Float:EDonatorRotZ,
	EDonatorInt,
	EDonatorVW,
	EDonatorObjectID,
	Float:EStreamDistance,
};
#define MAX_DONATORS 15
#define MAX_MAPPING 250

new DonatorMapping[MAX_DONATORS][EDonatorInfo];
new DonatorMappingObjects[MAX_DONATORS][MAX_MAPPING][EDonatorObjects];

donatorMappingOnGameModeInit() {
	mysql_function_query(g_mysql_handle, "SELECT `id`,`owner`,`enabled` FROM `donatorinfo` WHERE `enabled` != -1", true, "OnLoadDonatorInfo", "");
}
forward OnLoadDonatorInfo();
public OnLoadDonatorInfo() {
	new rows, fields;
	new id_string[128];
	cache_get_data(rows, fields);
	for(new i=0;i<rows&&i<sizeof(DonatorMapping);i++) {
		cache_get_row(i, 0, id_string);
		DonatorMapping[i][EDonatorID] = strval(id_string);
		cache_get_row(i, 1, id_string);
		DonatorMapping[i][EDonatorOwner] = strval(id_string);
		cache_get_row(i, 2, id_string);
		DonatorMapping[i][EDonatorStatus] = strval(id_string);
		
		LoadDonatorMapping(i);
	}
}
LoadDonatorMapping(index) {
	new id_string[128];
	format(id_string, sizeof(id_string), "SELECT `model`,`x`,`y`,`z`,`rx`,`ry`,`rz`,`streamdistance` FROM `donatorobjects` WHERE `owner` = %d",DonatorMapping[index][EDonatorID]);
	mysql_function_query(g_mysql_handle, id_string, true, "OnLoadDonatorObjects", "d",index);
} 
forward OnLoadDonatorObjects(index);
public OnLoadDonatorObjects(index) {
	new rows, fields;
	cache_get_data(rows, fields);
	new id_string[128];
	for(new i=0;i<rows&&i<MAX_MAPPING;i++) {
		cache_get_row(i, 0, id_string);
		DonatorMappingObjects[index][i][EDonatorModel] = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		DonatorMappingObjects[index][i][EDonatorX] = floatstr(id_string);
		
		cache_get_row(i, 2, id_string);
		DonatorMappingObjects[index][i][EDonatorY] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		DonatorMappingObjects[index][i][EDonatorZ] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		DonatorMappingObjects[index][i][EDonatorRotX] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		DonatorMappingObjects[index][i][EDonatorRotY] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		DonatorMappingObjects[index][i][EDonatorRotZ] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		DonatorMappingObjects[index][i][EStreamDistance] = floatstr(id_string);
		
		DonatorMappingObjects[index][i][EDonatorVW] = 0;
		DonatorMappingObjects[index][i][EDonatorInt] = 0;
		
		
		DonatorMappingObjects[index][i][EDonatorObjectID] = CreateDynamicObject(
		DonatorMappingObjects[index][i][EDonatorModel],
		DonatorMappingObjects[index][i][EDonatorX],
		DonatorMappingObjects[index][i][EDonatorY],
		DonatorMappingObjects[index][i][EDonatorZ],
		DonatorMappingObjects[index][i][EDonatorRotX],
		DonatorMappingObjects[index][i][EDonatorRotY],
		DonatorMappingObjects[index][i][EDonatorRotZ],
		DonatorMappingObjects[index][i][EDonatorVW],
		DonatorMappingObjects[index][i][EDonatorInt],-1,
		300.0);
	}
}
YCMD:disablemapping(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Disables donator mapping");
		return 1;
	}
	new id = findPlayerDonatorMapping(playerid);
	if(id == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your donator mapping was not found, or is not loaded, contact an admin.");
		return 1;
	}
	setMapping(id, 0);
	SendClientMessage(playerid, X11_TOMATO_2, "Mapping disabled!");
	return 1;
}
YCMD:enablemapping(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enables donator mapping");
		return 1;
	}
	new id = findPlayerDonatorMapping(playerid);
	if(id == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Your donator mapping was not found, or is not loaded, contact an admin.");
		return 1;
	}
	setMapping(id, 1);
	SendClientMessage(playerid, X11_TOMATO_2, "Mapping enabled!");
	return 1;
}
findPlayerDonatorMapping(playerid) {
	new sqlid = GetPVarInt(playerid, "CharID");
	for(new i=0;i<sizeof(DonatorMapping);i++) {
		if(DonatorMapping[i][EDonatorOwner] == sqlid) {
			return i;
		}
	}
	return -1;
}
setMapping(id, locked) {
	if(locked == 0 || locked == -1) {
		UnloadDonatorMapping(id);
	} else {
		LoadDonatorMapping(id);
	}
}
UnloadDonatorMapping(id) {
	for(new i=0;i<MAX_MAPPING;i++) {
		if(DonatorMappingObjects[id][i][EDonatorObjectID] != 0) {
			DestroyDynamicObject(DonatorMappingObjects[id][i][EDonatorObjectID]);
			DonatorMappingObjects[id][i][EDonatorObjectID] = 0;
		}
	}
}