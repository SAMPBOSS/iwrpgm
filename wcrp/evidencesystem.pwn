/*
	PVARS Used By This Script
	VictimEvidence(int) - Stores the SQLID of the victims evidence (any kind) if the victim was shot from a close distance
	VictimEvidenceType(int) - The type of evidence being stored (EvidenceType)
	EvidenceType (int) - The type of evidence to pvar
	EvidenceOwner (int) - The evidence SQLID.
	EvidenceOwnerName (char) - The name of the original person who owns the DNA / Evidence
*/
/* Defines */
#define MAX_EVIDENCE 			500
#define MAX_EVIDENCE_DESC 		64
#define MAX_EVIDENCE_TIME 		14400 	//4 hours
#define EVIDENCE_STREAM_DIST 	5.0
#define MAX_DECAY_STATES		6
#define BH_WAIT_TIME 			2		//Bullet hole wait time
#define MDL_FLIES				18698
#define MDL_BODYBAG				19944
#define MDL_BLOOD				19836
#define BODY_TRUNK_VW			10000
#define BODY_DISAPPEAR_TIME		900 	//Time the bodies take to disappear (15 minutes)
/* End of Defines */

new LastBulletHole[MAX_PLAYERS]; //number of seconds to ignore sending messages
new DraggingBody[MAX_PLAYERS] = -1;
new DragBodyTimer[MAX_PLAYERS] = -1;

enum EvidenceType {
	Evidence_None = 0,
	Evidence_FingerPrint,
	Evidence_FootPrint,
	Evidence_Blood,
	Evidence_Saliva,
	Evidence_Ammo,
	Evidence_Sweat,
	Evidence_Hair,
	Evidence_BulletHole,
	Evidence_Body,
	Evidence_BodyBag,
};
new EvidenceName[11][] = {
	{"None"},
	{"Finger Print"},
	{"Blue Print"},
	{"Blood"},
	{"Saliva"},
	{"Bullets"},
	{"Sweat"},
	{"Hair"},
	{"Mark"},
	{"Body"},
	{"Body Bag"}
};
enum EvidFlags (<<= 1) {
	EvidFlags_None = 0,
	EvidFlags_Burning = 1,
	EvidFlags_Decay,
	EvidFlags_BeingDragged,
	EvidFlags_BodyInBag,
};
enum EvidFlagsInfo {
	EvidFlags:EVFlag, //flag
	EVFlagDesc[64], //description of the flag
};
new EvidFlagDesc[][EvidFlagsInfo] = 
{
	{EvidFlags_None, 	"None"},
	{EvidFlags_Burning, "Burnt"},
	{EvidFlags_Decay, 	"Decay"},
	{EvidFlags_BeingDragged, "Being Dragged"}
};

enum pEvidenceInfo {
	pEvidenceDesc[MAX_EVIDENCE_DESC],
	pEvidenceSQLID,
	pOwnerSQLID,
	pOwner[MAX_PLAYER_NAME],
	pEvidenceTime,//time in seconds when it was dropped
	EvidenceType:pEvidenceType,
	Float:EvidenceX,
	Float:EvidenceY, 
	Float:EvidenceZ,
	EvidFlags:pEvidenceFlags,
	EvidenceInt,
	EvidenceVW,
	Amount,
	WeaponID,
	pActorID,
	pActorSkin,
	pBodyDeathCause,
	Float:pBodyBurn,
	Text3D:EvidenceTextLabel,
	EvidenceObjID,
	EvidenceInCarID, //The car id the evidence is on
	EvidenceIDInCar, //The ID of the evidence itself
};
new EvidenceInfo[MAX_EVIDENCE][pEvidenceInfo];

enum eStateDecay {
	State_Fresh,
	State_RigorMortis,
	State_Putrefaction,
	State_Decomposition,
	State_Skeleton,
}
enum eDecayStates {
	secsMin,
	secsMax,
	decayDesc[64],
	eStateDecay:eSD,
};
new BodyDecayStates[MAX_DECAY_STATES][eDecayStates] = {
	{0, 500, "Fresh", State_Fresh},
	{500, 1000, "Rigor Mortis", State_RigorMortis},
	{1000, 2000, "Putrefaction", State_Putrefaction},
	{2000, 4000, "Decomposition", State_Decomposition},
	{4000, 7200, "Skeleton", State_Skeleton},
	{7200, 14400, "Skeleton", State_Skeleton}
};

new Text3D:PEvidenceLabels[MAX_PLAYERS];

evidenceOnGameModeInit() {
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		EvidenceInfo[i][pActorID] = INVALID_ACTOR_ID; //Initialize everything as invalid
	}
	return 1;
}
evidenceOnClothes(attackerid, sentvictimid, EvidenceType:EvidType) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(sentvictimid, X, Y, Z);
	if(IsPlayerInRangeOfPoint(attackerid, 5.0, X, Y, Z)) {
		setEvidencePVars(attackerid, sentvictimid, EvidType);
		setEvidence3dTextLabel(attackerid, EvidType);
	}
	return 1;
}
setEvidencePVars(attackerid, sentvictimid, EvidenceType:EvidType) {
	SetPVarInt(attackerid, "VictimEvidence", sentvictimid);
	SetPVarInt(attackerid, "VictimEvidenceType", _:EvidType);
	return 1;
}
deleteEvidencePVars(playerid) {
	DeletePVar(playerid, "VictimEvidence");
	DeletePVar(playerid, "VictimEvidenceType");
	new pvarstring[64];
	for(new i=0;i<MAX_EVIDENCE;i++) {
		format(pvarstring,sizeof(pvarstring),"EvidenceOwnerName%d",i);
		if(GetPVarType(playerid, pvarstring) != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, pvarstring);
		}
		format(pvarstring,sizeof(pvarstring),"EvidenceType%d",i);
		if(GetPVarType(playerid, pvarstring) != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, pvarstring);
		}
		format(pvarstring,sizeof(pvarstring),"EvidenceOwner%d",i);
		if(GetPVarType(playerid, pvarstring) != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, pvarstring);
		}
		format(pvarstring,sizeof(pvarstring),"EvidenceGunID%d",i);
		if(GetPVarType(playerid, pvarstring) != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, pvarstring);
		}
	}
	return 1;
}
setEvidence3dTextLabel(playerid, EvidenceType:EvidType) {
	destroyEvidence3dTextLabel(playerid);
	new msg[MAX_EVIDENCE_DESC];
	format(msg, sizeof(msg), "%s", getEvidenceNameByType(EvidType, 1, 0));
	PEvidenceLabels[playerid] = CreateDynamic3DTextLabel(msg,X11_WHITE, 0.0, 0.0, 0.0-0.5, NAMETAG_DRAW_DISTANCE,playerid,.testlos=1);
	return 1;
}
destroyEvidenceOnPlayer(playerid) {
	destroyEvidence3dTextLabel(playerid);
	deleteEvidencePVars(playerid);
	return 1;
}
destroyEvidence3dTextLabel(playerid) {
	if(PEvidenceLabels[playerid] != Text3D:0) {
		DestroyDynamic3DTextLabel(PEvidenceLabels[playerid]);
		PEvidenceLabels[playerid] = Text3D:0;
	}
	return 1;
}
dropEvidence(playerid, EvidenceType:EvidType, amount = 1, gunid = 0, Float: X = 0.0, Float: Y = 0.0, Float: Z = 0.0) {
	new index = findFreeEvidenceSlot();
	new msg[128];
	if(index == -1) {
		//Max reached! Don't add evidence, and remove the old one
		destroyAllOldEvidence();
		return -1;
	}
	new VW, Interior;
	new Float: AngleF;
	if(EvidType != Evidence_BulletHole) {
		GetPlayerPos(playerid, X, Y, Z);
	} 
	Interior = GetPlayerInterior(playerid);
	VW = GetPlayerVirtualWorld(playerid);
	//EvidenceInfo[index][pEvidenceSQLID] = id;
	EvidenceInfo[index][EvidenceX] = X;
	EvidenceInfo[index][EvidenceY] = Y;
	EvidenceInfo[index][EvidenceZ] = Z;
	EvidenceInfo[index][pEvidenceType] = EvidType;
	EvidenceInfo[index][pEvidenceTime] = gettime();
	EvidenceInfo[index][EvidenceInt] = Interior;
	EvidenceInfo[index][EvidenceVW] = VW;
	EvidenceInfo[index][pEvidenceFlags] = EvidFlags:0;
	EvidenceInfo[index][pActorID] = INVALID_ACTOR_ID;

	switch(EvidType) {
		case Evidence_Ammo: {
			EvidenceInfo[index][Amount] = amount;	
			EvidenceInfo[index][WeaponID] = gunid;
		}
		case Evidence_Body: {
			GetPlayerFacingAngle(playerid, AngleF);
			EvidenceInfo[index][pActorID] = createBody(X, Y, Z, AngleF, VW, GetPVarInt(playerid, "SkinID"));
			EvidenceInfo[index][pActorSkin] = GetPVarInt(playerid, "SkinID");
			EvidenceInfo[index][EvidenceInt] = -1; //Actors have no interiors
			EvidenceInfo[index][pBodyDeathCause] = GetPVarInt(playerid, "ShotReason");
			EvidenceInfo[index][Amount] = getPlayerTimesShot(playerid);
			EvidenceInfo[index][pEvidenceFlags] |= EvidFlags_Decay; //It starts to decay as soon as it dies
			#if debug 
				printf("Actor ID: %d", EvidenceInfo[index][pActorID]);
			#endif
			getBodyTextLabel(index, EvidenceInfo[index][pEvidenceDesc], MAX_EVIDENCE_DESC);
		}
		case Evidence_Blood: {
			EvidenceInfo[index][EvidenceObjID] = CreateDynamicObject(MDL_BLOOD, EvidenceInfo[index][EvidenceX], EvidenceInfo[index][EvidenceY],EvidenceInfo[index][EvidenceZ]-0.95, 0.0, 0.0, 0.0,EvidenceInfo[index][EvidenceVW],-1);
		}
	}

	format(EvidenceInfo[index][pOwner],MAX_PLAYER_NAME,"%s",GetPlayerNameEx(playerid, ENameType_RPName));
	EvidenceInfo[index][pOwnerSQLID] = GetPVarInt(playerid, "CharID");

	if(EvidType != Evidence_Body) {
		format(EvidenceInfo[index][pEvidenceDesc], MAX_EVIDENCE_DESC, "%s", getEvidenceNameByType(EvidType, amount, gunid));
		EvidenceInfo[index][EvidenceTextLabel] = CreateDynamic3DTextLabel(EvidenceInfo[index][pEvidenceDesc], 0x2BFF00AA, EvidenceInfo[index][EvidenceX], EvidenceInfo[index][EvidenceY], (EvidType == Evidence_BulletHole) ? EvidenceInfo[index][EvidenceZ] : EvidenceInfo[index][EvidenceZ]-1.0, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, EvidenceInfo[index][EvidenceVW],EvidenceInfo[index][EvidenceInt],-1,EVIDENCE_STREAM_DIST);
	}
	
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		format(msg, sizeof(msg), "Evidence ID: %d",index);
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	}
	return index;
}
evidenceOnPlayerKeyState(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	new msg[256];
	new index = getClosestBodyIndex(playerid, 1.5);
	if(index == -1) {
		return 1;
	}
	if(newkeys & KEY_CROUCH) {
		SetPVarInt(playerid, "Crouched", 1);
		if(GetPVarType(playerid, "Crouched") != PLAYER_VARTYPE_NONE) {
			ShowScriptMessage(playerid, "Press ~r~\"~k~KEY_YES\" ~w~to get information about this ~r~body~w~.");
			return 1;
		}
	}
	if(newkeys & KEY_YES && GetPVarType(playerid, "Crouched") != PLAYER_VARTYPE_NONE) {
		getBodyTextLabel(index, msg, MAX_EVIDENCE_DESC);
		ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "Evidence Information:", msg, "Ok", "");
		DeletePVar(playerid, "Crouched");
	}
	return 1;
}
/* Bodies */
getBodyTextLabel(index, dst[], dstlen) {
	new wepname[24];
	GetWeaponNameEx(EvidenceInfo[index][pBodyDeathCause], wepname, sizeof(wepname));
	if(EvidenceInfo[index][pBodyDeathCause] >= 22) {
		format(dst, dstlen, "%s\n%s %s rounds\nState: %s", getEvidenceNameByType(EvidenceInfo[index][pEvidenceType], EvidenceInfo[index][Amount], EvidenceInfo[index][pBodyDeathCause], 1), EvidenceInfo[index][Amount] == 0 ? ("Unknown amount of") : getNumberString(EvidenceInfo[index][Amount]), wepname, getBodyStateDesc(index));
	} else {
		format(dst, dstlen, "%s\nDeath reason is unknown.\nState: %s", getEvidenceNameByType(EvidenceInfo[index][pEvidenceType], EvidenceInfo[index][Amount], EvidenceInfo[index][pBodyDeathCause], 1), getBodyStateDesc(index));
	}
}
getProperDecayStateIndex(index) {
	if(EvidenceInfo[index][pEvidenceFlags] & EvidFlags_Decay) {
		for(new i=0; i<sizeof(BodyDecayStates); i++) {
			if((gettime() - EvidenceInfo[index][pEvidenceTime]) >= BodyDecayStates[i][secsMin] && (gettime() - EvidenceInfo[index][pEvidenceTime]) <= BodyDecayStates[i][secsMax]) {
				return i;
			}
		}
	}
	return -1;
}
stock getBodyStateDesc(index) {
	new msg[128], xtmpstr[32], decStateIndex;
	new EvidFlags:aflags = EvidenceInfo[index][pEvidenceFlags];
	for(new i=0;i<sizeof(EvidFlagDesc);i++) {
		if(aflags & EvidFlagDesc[i][EVFlag]) {
			switch(EvidFlagDesc[i][EVFlag]) {
				case EvidFlags_Decay: {
					decStateIndex = getProperDecayStateIndex(index);
					format(xtmpstr, sizeof(xtmpstr), "%s - %s.", EvidFlagDesc[i][EVFlagDesc], BodyDecayStates[decStateIndex][decayDesc]);
					strcat(msg, xtmpstr);
				}
				case EvidFlags_Burning: {
					format(xtmpstr, sizeof(xtmpstr), "%s - %.2f\%.", EvidFlagDesc[i][EVFlagDesc], EvidenceInfo[index][pBodyBurn]);
					strcat(msg, xtmpstr);
				}
				case EvidFlags_BeingDragged: {
					format(xtmpstr, sizeof(xtmpstr), "%s.", EvidFlagDesc[i][EVFlagDesc]);
					strcat(msg, xtmpstr);
				}
				case EvidFlags_BodyInBag: {
					format(xtmpstr, sizeof(xtmpstr), "%s.", EvidFlagDesc[i][EVFlagDesc]);
					strcat(msg, xtmpstr);
				}
			}
			if(i != sizeof(EvidFlagDesc)-1) {
				strcat(msg, " | ");
			}
		}
	}
	return msg;
}
putBodyInBag(indexcopy) { //puts the body in a bag making an identical copy of all its info
	new index = findFreeEvidenceSlot();
	new Float: Angle;
	if(index == -1) {
		//Max reached! Don't add evidence, and remove the old one
		destroyAllOldEvidence();
		return 1;
	}
	EvidenceInfo[indexcopy][pEvidenceFlags] |= EvidFlags_BodyInBag; //Set the copy in a bag
	if(EvidenceInfo[index][EvidenceObjID] != 0) {
		#if debug
		printf("Debug: Called putBodyInBag->DestroyDynamicObject.");
		#endif
		DestroyDynamicObject(EvidenceInfo[index][EvidenceObjID]);
		EvidenceInfo[index][EvidenceObjID] = 0;
	} 
	EvidenceInfo[index][pEvidenceType] = Evidence_BodyBag;

	//Make the copy
	EvidenceInfo[index][EvidenceX] = EvidenceInfo[indexcopy][EvidenceX];
	EvidenceInfo[index][EvidenceY] = EvidenceInfo[indexcopy][EvidenceY];
	EvidenceInfo[index][EvidenceZ] = EvidenceInfo[indexcopy][EvidenceZ];
	EvidenceInfo[index][pEvidenceFlags] = EvidenceInfo[indexcopy][pEvidenceFlags]; 
	EvidenceInfo[index][pOwnerSQLID] = EvidenceInfo[indexcopy][pOwnerSQLID];
	EvidenceInfo[index][pEvidenceTime] = EvidenceInfo[indexcopy][pEvidenceTime];

	GetActorFacingAngle(EvidenceInfo[indexcopy][pActorID], Angle);
	EvidenceInfo[index][EvidenceObjID] = CreateDynamicObject(MDL_BODYBAG, EvidenceInfo[indexcopy][EvidenceX], EvidenceInfo[indexcopy][EvidenceY],EvidenceInfo[indexcopy][EvidenceZ]-1.0, 0.0, 0.0, Angle, EvidenceInfo[indexcopy][EvidenceVW],-1);
	/*
	if(EvidenceInfo[index][EvidenceTextLabel] != Text3D:0) {
		DestroyDynamic3DTextLabel(EvidenceInfo[index][EvidenceTextLabel]); //Destroy it and create a new one when we move it, otherwise it won't follow the body
		EvidenceInfo[index][EvidenceTextLabel] = Text3D:0;
	}
	*/

	getBodyTextLabel(indexcopy, EvidenceInfo[index][pEvidenceDesc], MAX_EVIDENCE_DESC);
	//EvidenceInfo[index][EvidenceTextLabel] = CreateDynamic3DTextLabel(EvidenceInfo[index][pEvidenceDesc], 0x2BFF00AA, EvidenceInfo[indexcopy][EvidenceX], EvidenceInfo[indexcopy][EvidenceY],EvidenceInfo[indexcopy][EvidenceZ]-1.0, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, GetActorVirtualWorld(EvidenceInfo[indexcopy][pActorID]),-1,-1,EVIDENCE_STREAM_DIST);
	return 1;
}
createBody(Float: X = 0.0, Float: Y = 0.0, Float: Z = 0.0, Float: AngleF = 0.0, vw, skin) {
	new actorid = CreateActor(skin, X, Y, Z, AngleF);
	if(IsValidActor(actorid)) {
		SetActorVirtualWorld(actorid, vw);
		ApplyActorAnimation(actorid, "PED", "KO_skid_back", 4.1, 0, 1, 1, 1, 0);
	}
	return actorid;
}
setBodyPosition(index, Float: X = 0.0, Float: Y = 0.0, Float: Z = 0.0, Float: Angle = 0.0, isfinal = 0) {
	if(IsValidActor(EvidenceInfo[index][pActorID])) {
		SetActorPos(EvidenceInfo[index][pActorID], X, Y, Z);
		SetActorFacingAngle(EvidenceInfo[index][pActorID], Angle);
		if(isfinal) {
			EvidenceInfo[index][pEvidenceFlags] &= ~EvidFlags_BeingDragged;
			getBodyTextLabel(index, EvidenceInfo[index][pEvidenceDesc], MAX_EVIDENCE_DESC);
			//EvidenceInfo[index][EvidenceTextLabel] = CreateDynamic3DTextLabel(EvidenceInfo[index][pEvidenceDesc], 0x2BFF00AA, X, Y, Z-1.0, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, GetActorVirtualWorld(EvidenceInfo[index][pActorID]),-1,-1,EVIDENCE_STREAM_DIST);
		}
		EvidenceInfo[index][EvidenceX] = X;
		EvidenceInfo[index][EvidenceY] = Y;
		EvidenceInfo[index][EvidenceZ] = Z;
	}
}
EvidenceIsBody(index) {
	if(EvidenceInfo[index][pActorID] != INVALID_ACTOR_ID) {
		return 1;
	}
	return 0;
}
getPlayerNumEvidenceAtPosByType(playerid, EvidenceType:EvidType, Float:range = 5.0) {
	new x;
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		if(IsPlayerInRangeOfPoint(playerid, range, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ])) {
			if(EvidenceInfo[i][pEvidenceType] == EvidType) {
				if(EvidenceInfo[i][pOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
					x++;
				}
			}
		}
	}
	return x;
}
/*
getEvidenceAtPosByType(playerid, EvidenceType:EvidType, Float:range = 5.0) {
	new x;
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		if(IsPlayerInRangeOfPoint(playerid, range, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ])) {
			if(EvidenceInfo[i][pEvidenceType] == EvidType) {
				x++;
			}
		}
	}
	return x;
}
*/
getClosestBodyIndex(playerid, Float:range = 5.0) {
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		if(IsPlayerInRangeOfPoint(playerid, range, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ])) {
			if(EvidenceInfo[i][pOwnerSQLID] != 0) {
				if(EvidenceIsBody(i)) {
					return i;
				}
			}
		}
	}
	return -1;
}
getBodyIDByEvidIndex(index) {
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		if(EvidenceInfo[i][pActorID] == EvidenceInfo[index][pActorID]) {
			return EvidenceInfo[index][pActorID];
		}
	}
	return -1;
}
getBodySkin(index) {
	return EvidenceInfo[index][pActorSkin];
}
destroyBodyAtEvidIndex(index) {
	#if debug
	printf("Actor destroyed");
	#endif
	DestroyActor(EvidenceInfo[index][pActorID]);
	EvidenceInfo[index][pActorID] = INVALID_ACTOR_ID;

}
evidenceBurnBody(index) {
	if(EvidenceInfo[index][pBodyBurn] >= 100) {
		destroyEvidence(index);
		//destroyBodyAtEvidIndex(index);
		//CreateDynamicObject(19380, -1219.98, -405.64, 69.92,   0.00, 90.00, 0.00,36,1); (Burnt corpse?)
	}
	if(~EvidenceInfo[index][pEvidenceFlags] & EvidFlags_Burning) {
		EvidenceInfo[index][pEvidenceFlags] |= EvidFlags_Burning;
	}
	EvidenceInfo[index][pBodyBurn] += ((servtemperature < 1 ? 1 : servtemperature) * 0.0001);
}
updateBodies() { //Gets called every sec
	new decStateIndex;
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		if(EvidenceIsBody(i)) {
			decStateIndex = getProperDecayStateIndex(i);
			if(decStateIndex != -1) {
				if((gettime() - EvidenceInfo[i][pEvidenceTime]) >= BODY_DISAPPEAR_TIME) { //Destroy the bodies after 30 secs
					destroyEvidence(i);
				}
				if(BodyDecayStates[decStateIndex][eSD] == State_RigorMortis) {
					if(!EvidenceInfo[i][EvidenceObjID]) {
						EvidenceInfo[i][EvidenceObjID] = CreateDynamicObject(MDL_FLIES, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY],EvidenceInfo[i][EvidenceZ]-1.0, 0.0, 0.0, 0.0,EvidenceInfo[i][EvidenceVW],-1);
					} else { //If we have one
						if(EvidenceInfo[i][pEvidenceFlags] & EvidFlags_BeingDragged) { //And it's moving
							SetDynamicObjectPos(EvidenceInfo[i][EvidenceObjID], EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY],EvidenceInfo[i][EvidenceZ]-1.0);
							//Just update the obj position
						}
					}
				}
			}
		}
	}
}
killDragTimer(playerid) {
	new timer = DragBodyTimer[playerid];
	if(timer != -1) {
		KillTimer(timer);
		dragBody(playerid, DraggingBody[playerid], 1);
	}
	DragBodyTimer[playerid] = -1;
	DraggingBody[playerid] = -1;
}
forward dragBody(playerid, bodyIndex, final);
public dragBody(playerid, bodyIndex, final) {
	new Float: X, Float: Y, Float: Z, Float: Angle, VW;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, Angle);
	Angle += 90;
	Angle = DEG_TO_RAD(Angle);
	X -= floatcos(Angle) * 1.5;
	Y -= floatsin(Angle) * 1.5;
	VW = GetPlayerVirtualWorld(playerid);
	setBodyPosition(bodyIndex, X, Y, Z, Angle, final);
	SetActorVirtualWorld(EvidenceInfo[bodyIndex][pActorID], VW);
	return 1;
}
/* End of bodies */
getEvidenceIDInCar(carid) {
	for(new i=0; i<sizeof(EvidenceInfo); i++) {
		if(EvidenceInfo[i][EvidenceInCarID] == carid) {
			return EvidenceInfo[i][EvidenceIDInCar];
		}
	}
	return -1;
}
evidenceOnPlayerShootWeapon(playerid, weaponid, hittype, hitid, Float: fX, Float: fY, Float: fZ) {
	#pragma unused hitid
	if(IsPlayerConnectEx(playerid)) {
		if(isInPaintball(playerid)) return 1;
		if(hittype == BULLET_HIT_TYPE_NONE) {
			new time = LastBulletHole[playerid];
			new timenow = gettime();
			if(BH_WAIT_TIME-(timenow-time) > 0) {
				return 1;
			}
			new Float: X, Float: Y, Float: Z;
			GetPlayerPos(playerid, X, Y, Z);
			dropEvidence(playerid, Evidence_Ammo, 1, weaponid);
			dropEvidence(playerid, Evidence_BulletHole, 1, weaponid, fX, fY, fZ);
			LastBulletHole[playerid] = gettime();
		}
	}
	return 1;
}
stock getEvidenceNameByType(EvidenceType:EvidType, amount = 1, gunid = 0, scriptmessage = 0) {
	new name[32];
	if(EvidType == Evidence_Ammo) {
		if(amount > 1) {
			format(name,sizeof(name),"{FFFFFF}%s shells (%d).",GunName[gunid],amount);
		} else {
			format(name,sizeof(name),"{FFFFFF}%s shell.",GunName[gunid]);
		}
	} else if(EvidType == Evidence_BulletHole) {
		format(name,sizeof(name),"{FFFFFF}%s (Hole).",GunName[gunid]);
	} else if(EvidType == Evidence_Blood) {
		format(name,sizeof(name),"{FF0000}Blood Stain");
	} else {
		format(name,sizeof(name),(!scriptmessage) ? ("{FFFFFF}%s") : ("%s"), EvidenceName[_:EvidType]);
	}
	return name;
}
findFreeEvidenceSlot() {
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		if(EvidenceInfo[i][pOwnerSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
isOldEvidence(index) {
	new time = gettime();
	if(EvidenceInfo[index][pOwnerSQLID] != 0) { //Needs to be owned
		if((time-EvidenceInfo[index][pEvidenceTime]) >= MAX_EVIDENCE_TIME) {
			return 1;
		}
	}
	return 0;
}
destroyAllOldEvidence() {
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		if(isOldEvidence(i)) {
			destroyEvidence(i);
		}
	}
}
destroyEvidence(index) {
	EvidenceInfo[index][pOwnerSQLID] = 0;
	EvidenceInfo[index][pEvidenceTime] = 0;
	EvidenceInfo[index][pEvidenceFlags] = EvidFlags:0;
	EvidenceInfo[index][EvidenceInCarID] = 0;
	EvidenceInfo[index][EvidenceIDInCar] = 0;
	EvidenceInfo[index][pEvidenceType] = Evidence_None;
	//EvidenceInfo[index][pEvidenceSQLID] = 0;
	if(EvidenceIsBody(index)) {
		destroyBodyAtEvidIndex(index);
	}
	if(EvidenceInfo[index][EvidenceObjID] != 0) {
		#if debug
		printf("Debug: Called destroyEvidence->DestroyDynamicObject.");
		#endif
		DestroyDynamicObject(EvidenceInfo[index][EvidenceObjID]);
	}
	if(EvidenceInfo[index][EvidenceTextLabel] != Text3D:0) {
		#if debug
			printf("Destroyed evidence with index %d", index);
		#endif
		DestroyDynamic3DTextLabel(EvidenceInfo[index][EvidenceTextLabel]);
	}
	EvidenceInfo[index][EvidenceTextLabel] = Text3D:0;
	EvidenceInfo[index][EvidenceObjID] = 0;
}
stock Float:getDistFromEvidenceAtXYZ(Float: X, Float: Y, Float: Z) {
	new Float:range;
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		range = GetPointDistance(X, Y, Z, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ]);
		if(range > 0.0) {
			return range;
		}
	}
	return 0.0;
}
getEvidenceIndexAtXYZ(Float: X, Float: Y, Float: Z, Float: range = 5.0) {
	new Float:xrange;
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		xrange = GetPointDistance(X, Y, Z, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ]);
		if(xrange < range) {
			return i;
		}
	}
	return -1;
}
getClosestEvidenceID(playerid) {
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 5.0, EvidenceInfo[i][EvidenceX], EvidenceInfo[i][EvidenceY], EvidenceInfo[i][EvidenceZ])) {
			if(EvidenceInfo[i][pOwnerSQLID] != 0) {
				return i;
			}
		}
	}
	return -1;
}
putEvidenceInBag(playerid, index) {
	new pvarindex = getEvidencePVarIndex(playerid);
	new indexformat[64];
	format(indexformat,sizeof(indexformat),"EvidenceType%d",pvarindex);
	SetPVarInt(playerid,indexformat,_:EvidenceInfo[index][pEvidenceType]); // save the string into a player variable
	format(indexformat,sizeof(indexformat),"EvidenceOwner%d",pvarindex);
	SetPVarInt(playerid,indexformat,EvidenceInfo[index][pOwnerSQLID]); // save the string into a player variable
	format(indexformat,sizeof(indexformat),"EvidenceOwnerName%d",pvarindex);
	SetPVarString(playerid,indexformat,EvidenceInfo[index][pOwner]); // save the string into a player variable this is the owner name
	format(indexformat,sizeof(indexformat),"EvidenceGunID%d",pvarindex);
	SetPVarInt(playerid,indexformat,EvidenceInfo[index][WeaponID]); // save the string into a player variable this is the weapon id
	SetPVarInt(playerid, "EvidenceIndex", pvarindex+1);
}
getEvidencePVarIndex(playerid) {
	if(GetPVarType(playerid, "EvidenceIndex") != PLAYER_VARTYPE_NONE) {
		new evidenceindex = GetPVarInt(playerid, "EvidenceIndex");
		return evidenceindex;
	}
	return 0;
}
showAllEvidence(playerid) {
	new textstring[128];
	new msg[128];
	new pvarstring[24];
	new pvarstringtype[24];
	new SEvidType, SGunID;
	SendClientMessage(playerid, COLOR_WHITE, "|__________________ Evidence __________________|");
	for(new i=0;i<MAX_EVIDENCE;i++) {
		format(pvarstring,sizeof(pvarstring),"EvidenceOwnerName%d",i);
		if(GetPVarType(playerid, pvarstring) != PLAYER_VARTYPE_NONE) {
			GetPVarString(playerid, pvarstring, msg, MAX_PLAYER_NAME);
			format(pvarstringtype,sizeof(pvarstringtype),"EvidenceType%d",i);
			SEvidType = GetPVarInt(playerid, pvarstringtype);
			format(pvarstringtype,sizeof(pvarstringtype),"EvidenceGunID%d",i);
			if(GetPVarType(playerid, pvarstringtype) != PLAYER_VARTYPE_NONE) {
				SGunID = GetPVarInt(playerid, pvarstringtype);
			} else {
				SGunID = 0;
			}
			format(textstring, sizeof(textstring), "Evidence Owner: %s Type: %s", msg, getEvidenceNameByType(EvidenceType:SEvidType, 0, SGunID));
			SendClientMessage(playerid, X11_WHITE, textstring);
		}
	}
	SendClientMessage(playerid, COLOR_WHITE, "|______________________________________________|");
	return 1;
}
checkEvidenceOnPlayer(playerid, targetid) {
	if(GetPVarType(targetid, "VictimEvidence") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "There's no evidence on this person's clothes");
		return 1;
	}
	new msg[128];
	new victimsblood = GetPVarInt(targetid, "VictimEvidence");
	format(msg,sizeof(msg),"The blood on this cloth belongs to: %s",GetPlayerNameEx(victimsblood, ENameType_RPName));
	SendClientMessage(playerid, X11_WHITE, msg);
	destroyEvidenceOnPlayer(playerid);
	return 1;
}
evidenceOnPlayerDisconnect(playerid) {
	destroyEvidenceOnPlayer(playerid);
	killDragTimer(playerid);
	LastBulletHole[playerid] = 0;
	return 1;
}
/* Bullet Holes & Ammo */
/* Commands */
YCMD:viewevidence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to examine the evidence you've picked up");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not a LEO Officer!");
		return 1;
	}
	if(!IsAtDutySpot(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need to be at your duty spot to check the evidence!");
		return 1;
	}
	showAllEvidence(playerid);
	return 1;
}
YCMD:viewevidenceonplayer(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to examine evidence on a player");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not a LEO Officer!");
		return 1;
	}
	if(!IsAtDutySpot(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You need to be at your duty spot to check the evidence on this person!");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 2.5, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are too far away from this person!");
			return 1;
		}
		if(IsPlayerInAnyVehicle(user) && GetPlayerState(playerid) == PLAYER_STATE_ONFOOT) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is inside a vehicle, the person must be on foot!");
			return 1;
		}
		checkEvidenceOnPlayer(playerid, user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /viewevidenceonplayer [playerid/name]");
	}
	return 1;
}
YCMD:pickevidence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to pick up evidence");
		return 1;
	}
	if(!IsAnLEO(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not a LEO Officer!");
		return 1;
	}
	new index = getClosestEvidenceID(playerid);
	new msg[128];
	if(index != -1) {
		if(EvidenceInfo[index][pEvidenceType] == Evidence_BulletHole) {
			SendClientMessage(playerid, X11_TOMATO_2, "This can't be picked up!");
			return 1;
		}
		ApplyAnimation(playerid, "BOMBER","BOM_Plant_In",4.0,0,0,0,0,0);
		putEvidenceInBag(playerid, index);
		if(EvidenceIsBody(index)) {
			format(msg, sizeof(msg), "* %s puts the body inside a bag and closes it. * You would hear the zipper go up. *", GetPlayerNameEx(playerid,ENameType_RPName));
			putBodyInBag(index);
		} else {
			if(EvidenceInfo[index][pEvidenceFlags] & EvidFlags_BodyInBag) {
				format(msg, sizeof(msg), "* %s clears out the body bag.", GetPlayerNameEx(playerid,ENameType_RPName));
			} else {
				format(msg, sizeof(msg), "* %s picks up some evidence and carefully places it into a plastic bag.", GetPlayerNameEx(playerid,ENameType_RPName));
			}
		}
		destroyEvidence(index);
		ProxMessage(25.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
			format(msg, sizeof(msg), "Evidence ID: %d",index);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		}
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not near any evidence!");
	}
	return 1;
}
YCMD:takeshower(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to take a shower when inside a house.");
		return 1;
	}
	new house = getStandingExit(playerid, 150.0);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a house!");
		return 1;
	}
	SendClientMessage(playerid, COLOR_LIGHTBLUE, "You took a shower and you feel much better now!");
	destroyEvidenceOnPlayer(playerid);
	return 1;
}
/*
YCMD:spawnbody(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Spawns a body.");
		return 1;
	}
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	new index, timedrop; 
	index = dropEvidence(playerid, Evidence_Body, 1, 0, X, Y, Z);
	if(index != -1) {
		if (!sscanf(params, "d", timedrop)) {
			EvidenceInfo[index][pEvidenceTime] += timedrop;
		} else {
			SendClientMessage(playerid, X11_WHITE,"USAGE: /spawnbody [time]");
		}
	}
	return 1;
}
*/
/* Body commands */
YCMD:dragbody(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to drag a body.");
		return 1;
	}
	new msg[128];
	new bodyIndex = getClosestBodyIndex(playerid);
	if(bodyIndex == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a body!");
		return 1;
	}
	if(DraggingBody[playerid] == -1) {
		if(EvidenceInfo[bodyIndex][pEvidenceFlags] & EvidFlags_BeingDragged) { //If it's being dragged already
			SendClientMessage(playerid, X11_TOMATO_2, "You can't drag that body since it's being dragged already!");
			return 1;
		}
		DraggingBody[playerid] = bodyIndex;
		DragBodyTimer[playerid] = SetTimerEx("dragBody", 100, true, "dd", playerid, bodyIndex);
		EvidenceInfo[bodyIndex][pEvidenceFlags] |= EvidFlags_BeingDragged;
		format(msg, sizeof(msg), "* %s starts dragging a body.", GetPlayerNameEx(playerid,ENameType_RPName));
	} else {
		killDragTimer(playerid);
		format(msg, sizeof(msg), "* %s stops dragging the body.", GetPlayerNameEx(playerid,ENameType_RPName));
	}
	ProxMessage(25.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 1;
}
YCMD:putbodyintrunk(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to put a body inside a vehicle's trunk.");
		return 1;
	}
	new msg[128];
	new bodyIndex = getClosestBodyIndex(playerid);
	new carid = GetClosestVehicle(playerid);
	new evidcar = getEvidenceIDInCar(carid);
	if(bodyIndex == -1 && evidcar == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near a body!");
		return 1;
	}
	new engine,lights,alarm,doors,bonnet,boot,objective;
	if(carid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "There are no cars!");
		return 1;
	}
	if(DistanceBetweenPlayerAndVeh(playerid, carid) > 5.0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near any vehicle!");
		return 1;
	}
	GetVehicleParamsEx(carid,engine,lights,alarm,doors,bonnet,boot,objective);
	if(boot == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "The trunk must be opened");
		return 1;
	}							
	if(evidcar == -1) {	
		format(msg, sizeof(msg), "* %s throws a body in the vehicle's trunk.", GetPlayerNameEx(playerid, ENameType_RPName));
		if(DraggingBody[playerid] != -1) { //Dragging the body?
			killDragTimer(playerid); //If there's a timer running, kill it, waste of resources otherwise and it'll keep dragging the actor
		}
		SetActorVirtualWorld(EvidenceInfo[bodyIndex][pActorID], BODY_TRUNK_VW);
		EvidenceInfo[bodyIndex][EvidenceInCarID] = carid;
		EvidenceInfo[bodyIndex][EvidenceIDInCar] = bodyIndex;
	} else {
		format(msg, sizeof(msg), "* %s pulls a body out of the vehicle's trunk.", GetPlayerNameEx(playerid, ENameType_RPName));
		dragBody(playerid, evidcar, 1);
		EvidenceInfo[evidcar][EvidenceInCarID] = -1;
		EvidenceInfo[evidcar][EvidenceIDInCar] = -1;
	}
	ProxMessage(25.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	return 1;
}
YCMD:destroyallevidence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Destroys all evidence from the server.");
		return 1;
	}
	for(new i=0;i<sizeof(EvidenceInfo);i++) {
		destroyEvidence(i);
	}
	return 1;
}
YCMD:deleteevidence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Delete evidence from the server.");
		return 1;
	}
	new index;
	if (!sscanf(params, "D(0)", index)) {
		if(index == 0) {
			for(new x=0; x<sizeof(EvidenceInfo); x++) {
				index = getClosestEvidenceID(playerid);
				if(index != -1) {
					destroyEvidence(index);
				}
			}
		} else {
			if(EvidenceInfo[index][pOwnerSQLID] != 0) {
				destroyEvidence(index);
				SendClientMessage(playerid, X11_WHITE, "[INFO]: Done!");
				return 1;
			} else {
				SendClientMessage(playerid, X11_WHITE, "Wrong ID, try /closestevidence");
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /deleteevidence [id - Optional]");
	}
	return 1;
}
YCMD:closestevidence(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gets the closest evidence.");
		return 1;
	}
	new index = getClosestEvidenceID(playerid);
	new msg[128];
	if(index != -1) {
		format(msg, sizeof(msg), "Closest Evidence ID is %d", index);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't near any evidence.");
		return 1;
	}
	return 1;
}