#define MAX_BUSINESS 200
#define BIZ_VW_OFFSET 1000
#define BIZROB_COOLDOWN 7200
#define GUN_BUY_TIME 7200

new bizinit = 0;
enum EBusinessType {
	EBusinessType_247, //0
	EBusinessType_D_NormCar, //1
	EBusinessType_D_FastCar, //2
	EBusinessType_D_Boat, //3
	EBusinessType_SexShop, //4
	EBusinessType_GasStation, //5
	EBusinessType_Restaurant, //6
	EBusinessType_PizzaStack, //aka fast food 7
	EBusinessType_PaintBall, //8 
	EBusinessType_Bar, //9 
	EBusinessType_D_Bike, //10
	EBusinessType_D_Plane, //11
	EBusinessType_Ammunation, //12
	EBusinessType_RaceTrack, //LS stadium 13
	EBusinessType_D_Truck, //transport trucks, etc 14
	EBusinessType_FurnitureStore, //15
	EBusinessType_FishStore, //16
	EBusinessType_VehicleRental, //17
	EBusinessType_DrugFactory, //18
};

enum EBusinessInfo {
	EBusinessID,
	EBusinessName[64],
	EBusinessPrice,
	EBusinessLevel,
	EBusinessOwner, //SQL ID OF OWNER
	EBusinessOwnerName[MAX_PLAYER_NAME], //character name of owner
	EBusinessExtortionist, //SQL ID of extortionist
	EBusinessExtortionName[MAX_PLAYER_NAME],
	Float:EBusinessEntranceX,
	Float:EBusinessEntranceY,
	Float:EBusinessEntranceZ,
	Float:EBusinessExitX,
	Float:EBusinessExitY,
	Float:EBusinessExitZ,
	EBusinessInterior,
	EBusinessVW,
	Text3D:EBusinessText,
	EBusinessPickup,
	EBusinessType:EBType,
	EBusinessLocked,//opened or not
	EBusinessEntranceFee,
	EBusinessExitPickup,
	EBusinessTill, //how much money is in the till
	EBusinessProducts,
	EBusinessMaxProducts,
	EBusinessLastRobbery, //unix time of last robbery
};
new Business[MAX_BUSINESS][EBusinessInfo];

enum EBCarInfo {
	EBCarModelID,
	EBCarPrice,
};

new EBDNormalCars[][EBCarInfo] = {
{400,20000},
{401,20000},
//{402,430000},
{404,15000},
{405,35000},
{410,20000},
{412,10000},
{413,15000},
{414,12000},
{418,15000},
{419,10000},
{422,15000},
{423,42000},
{426,55000},
{436,20000},
{439,70000},
{440,20000},
//{442,100000},
{456,60000},
{458,20000},
{466,10000},
{467,18000},
{474,10000},
//{477,65000},
{478,10000},
{479,15000},
{482,42000},
{483,50000},
{491,25000},
{492,30000},
{498,30000},
{499,17500},
{500,25000},
{505,30000},
{507,56000},
{508,70000},
{516,15000},
{517,25000},
//{562,65000},
{518,15000},
{526,22000},
{527,15000},
{529,10000},
{534,30000},
{535,57000},
{536,14000},
{540,41000},
{542,10000},
{543,10000},
{546,10000},
{547,10000},
{549,8000},
{550,15000},
{551,30000},
{554,25000},
//{555,37000},
//{560,149999},
{566,25000},
{567,15000},
{568,65000},
{575,18500},
{576,25000},
{585,10000},
{586,42000},
{588,42000},
{496,15000},
{579,60000},
{600,12000},
{604,1000},
{605,1000},
{409,400000},
{445, 53500}
//{470,350000},
//{580,125000}
};
new EBDFastCars[][EBCarInfo] = {
{402, 430000},
{421,220000},
{475,90000},
{480,205000},
{533,63500},
{545,150000},
{555, 57000},
{558,120000},
{559,200000},
{560,250000},
{561, 70000},
{565,100000},
{580, 90000},
{587,157000},
{589,41000},
{602,22000},
{603,257000}
};
new EBDBikes[][EBCarInfo] = {
//{481,600},
//{510,1200},
{509, 200},
{581,15000},
{461, 10000},
{462, 5000},
//{521, 25000},
{463, 13000},
{468, 11000},
{471, 25000},
{586, 20000}
};
new EBDBoats[][EBCarInfo] = {
{472,72000},
{473,20000},
{493,190000},
{595,40000},
{484,220000},
{453,120000},
{452,165000},
{446,200000},
{454,230000}
};
new EBDPlanes[][EBCarInfo] = {
{511,250000},
{512,300000},
{593,350000},
{519,950000},
{460,450000},
{513,550000},
{487,650000},
{469,450000}
};
new EBDTrucks[][EBCarInfo] = {
{499,80000},
{482,42000},
{422,15000},
{489,75000},
{609,75000},
{524,100000},
{578,80000},
{455,70000},
{403,80000},
{414,75000},
{443,230000},
{514,80000},
{413,50000},
{515,50000},
{440,95000},
{543,10000},
{459,65000},
{531,12000},
{552,75000},
{478,12000},
{456, 85000},
{428,200000}
};

enum EDrinkInfo {
	EDrinkName[32],
	EDrinkSpecialAction,
	EDrinkPrice,
	Float:EDrinkHealthBoost,
	EDrinkAction[32]
};
new Drinks[][EDrinkInfo] = {{"Water",SPECIAL_ACTION_NONE,10,5.0,  "* %s drinks a glass of water."},
							{"Sprunk",SPECIAL_ACTION_DRINK_SPRUNK,25,5.0,  "* %s drinks a sprunk."},
							{"Wine",SPECIAL_ACTION_DRINK_WINE ,55,5.0,  "* %s drinks a glass of wine."},
							{"Beer",SPECIAL_ACTION_DRINK_BEER ,35,5.0,  "* %s drinks a glass of beer."},
							{"Vodka",SPECIAL_ACTION_DRINK_WINE, 55, 5.0, "* %s drinks a vodka"}
							};
enum EGasPumpInfo {
	Float:EGasPumpX,
	Float:EGasPumpY,
	Float:EGasPumpZ
};
new GasPumps[][EGasPumpInfo] = {
	{1942.857910,-1773.203002,13.390598}, //Idlewood gas
	{1004.042236,-937.868041,42.179687}, //Vinewood gas
	{655.164245,-564.591308,16.335937}, //Dillimore gas
	{-92.035842,-1168.831542,2.447296}, //West LS Gas
	{2404.325439, -1383.679321, 24.214141}, //Pumps SE LS
	{-1605.830810, -2711.739990, 48.945312}, //angel pine
	{-1676.026245, 417.433990, 7.179687}, //sf near bridge
	{2115.626708, 918.848937, 10.820312}, //lv near highway/strip
	{2199.068603, 2476.527099, 10.820312}, //lv near pd
	{2141.96, -1739.21, 12.5506} //Idlewood's Gas Station in front of Ganton Gym
};

new DriveThrus[][EGasPumpInfo] = {
{1214.189941, -906.483276, 42.921615},
{2409.918212, -1487.343139, 23.825125},
{2376.004638, -1909.422485, 13.382812},
{799.527648, -1629.614013, 13.382812}
};

enum ECarRentalInfo {
	ECarRental_ModelID,
	ECarRental_HourlyRate
};
new RentableCars[][ECarRentalInfo] = {
 	{401,800},
 	{404,900},
 	{410,1000},
 	{418,800},
 	{436,800},
 	{479,1000},
 	{492,1000},
 	{540,1000},
 	{546,800},
 	{566,800},
 	{604,200},
 	{605,200}
};

#define GasMax 200
#define MAX_BUYABLE 10

enum {
	BIZ_DEALER_OFFSET = EBusiness_Base+1,
	EBusiness_247BuyMenu,
	EBusiness_247NumberInput,
	EBusiness_247BuyMenuEnd = EBusiness_247NumberInput + MAX_BUYABLE + 1,
	EBusiness_SexBuyMenu,
	EBusiness_SexNumberInput,
	EBusiness_SexBuyMenuEnd = EBusiness_SexNumberInput + MAX_BUYABLE + 1,
	BIZ_DEALER_DEALERSHIP_MENU,
	EBusiness_FastCarMenu,
	EBusiness_BikeCarMenu,
	EBusiness_BoatCarMenu,
	EBusiness_PlaneCarMenu,
	EBusiness_RestaurantMenu,
	EBusiness_DrinkMenu,
	EBusiness_LockBuy,
	EBusiness_FastFoodMenu,
	EBusiness_AmmunationMenu,
	EBusiness_LottoChoose,
	EBusiness_EnterLottoNumber,
	EBusiness_TruckCarMenu,
	EBusiness_FishStoreMenu,
	EBusiness_FishSell,
	EBusiness_RentalMenu,
	EBusiness_End,
};
enum EMenuType {
	EType_Item,
	EType_NumSelect, //to say 10 lockpicks
	EType_Menu,
};
enum EBuyableItems {
	EBIDisplayName[32],
	EBIPrice,
	EMenuType:EBIType,
	EBIOnBuyCallback[32],
};
new Buyable24Items[][EBuyableItems] = {
	{"Dice",5,EType_Item,"On247ItemBuy"},
	{"GPS",450,EType_Item,"On247ItemBuy"},
	{"Vehicle Tracer",50,EType_NumSelect,"On247ItemBuy"},
	{"Walkie Talkie",133,EType_Item,"On247ItemBuy"},
	{"Vehicle Lock Picks",30,EType_NumSelect,"On247ItemBuy"},
	{"Vehicle Locks",300,EType_Menu,"ShowVehicleLockBuy"},
	{"Gas Can",25,EType_NumSelect,"On247ItemBuy"},
	{"Lottery Tickets",5,EType_Menu,"ShowLotteryTicketBuy"},
	{"Cell Phone",250,EType_Item,"On247ItemBuy"},
	{"New Phone Number",50,EType_Item,"Generate247PhoneNumber"},
	{"Cigars",1,EType_NumSelect,"On247ItemBuy"},
	{"Fertilizer",10, EType_NumSelect,"On247ItemBuy"},
	{"Phone Book",5,EType_Item,"On247ItemBuy"},
	{"MP3 Player",50,EType_Item,"On247ItemBuy"},
	{"Mask",35,EType_Item,"On247ItemBuy"},
	{"Camera",50,EType_Item,"On247ItemBuy"},
	{"Shovel",25,EType_Item,"On247ItemBuy"},
	{"Flowers",5,EType_Item,"On247ItemBuy"},
	{"Pool Cue",55,EType_Item,"On247ItemBuy"},
	{"Cane",50,EType_Item,"On247ItemBuy"},
	{"Spray Can",45,EType_Item,"On247ItemBuy"},
	{"Backpack",40,EType_Item,"On247ItemBuy"},
	{"Matches",1,EType_NumSelect,"On247ItemBuy"},
	{"Chemicals",2,EType_NumSelect,"On247ItemBuy"}
};

new BuyableSexItems[][EBuyableItems] = {
	{"Purple Dildo",60,EType_Item,"OnSexItemBuy"},
	{"Dildo",30,EType_Item,"OnSexItemBuy"},
	{"Vibrator",45,EType_Item,"OnSexItemBuy"},
	{"Small Vibrator",35,EType_Item,"OnSexItemBuy"},
	{"Condoms",5,EType_NumSelect,"OnSexItemBuy"}
};

new BuyableRestaurantItems[][EBuyableItems] = {
	{"Small Meal",5,EType_Item,"OnRestaurantBuy"},
	{"Medium Meal",10,EType_Item,"OnRestaurantBuy"},
	{"Large Meal",15,EType_Item,"OnRestaurantBuy"}
};

new BuyableFastfoodItems[][EBuyableItems] = {
	{"Fries",2,EType_Item,"OnFastFoodBuy"},
	{"Burger",8,EType_Item,"OnFastFoodBuy"},
	{"Large Burger",10,EType_Item,"OnFastFoodBuy"}
};

new BuyableFishStoreItems[][EBuyableItems] = {
	{"Fish Burger",8,EType_Item,"OnFishBuy"},
	{"Large Fish Burger",10,EType_Item,"OnFishBuy"},
	{"Fish Sticks",10,EType_Item,"OnFishBuy"},
	{"Sell Fish",50,EType_Menu,"ShowSellFishMenu"}
};
enum EBuyableGuns {
	EBuyableGunType, //0 = gun, 1 = armour, 2 = health, 3 = special item
	EBuyableGunID,
	EBuyableGunPrice,
	EBuyableGunNeedLicense,
	EBuyableGunBuyLimit, //Specifies whether the weapon has a buy limit every 2 hours or not
};
new BuyableAmmunationGuns[][EBuyableGuns] = {
	{0,1,50,0,0},
	{0,5,70,0,0},
	{0,22,929,1,1},
	{0,23,1479,1,1},
	{0,24,2000,1,1}
	/*
	{0,25,13000,1}
	{1,50,5000,0},
	{2,50,1000,0},
	{0,41,250,0},
	{3,0,100000,0}
	*/
};

enum EBuyableCarLocks {
	ELockType:EBLockType,
	EBLockPrice,
};
new BuyableCarLocks[][EBuyableCarLocks] = {
	{ELockType_Simple,150},
	{ELockType_Remote,300}
	/*
	{ELockType_Advanced,15000},
	{ELockType_Satellite,50000},
	{ELockType_HighTech,100000},
	{ELockType_TitaniumLaser,120000},
	{ELockType_BioMetric, 220000}
	*/
};

enum EDealershipMenuState {
	EDSMS_CurPage,
	EDSMS_DealerType,
	EDSMS_RemainingCount,
	EDSMS_PageCount,
	EDSMS_InPreviewMode,
	EDSMS_BizID,
};
new DealerMenuState[MAX_PLAYERS][EDealershipMenuState];

forward onLoadBusiness();
forward onBusinessUpdate(index);
forward OnGeneratePhoneNumber(playerid);
forward On247ItemBuy(playerid, itemid, biz, total);
forward Generate247PhoneNumber(playerid, itemid, biz, total);
forward OnSexItemBuy(playerid, itemid, biz, total);

businessOnGameModeInit() {
	LoadBusinesses();
	for(new i=0;i<sizeof(DriveThrus);i++) {
		CreateDynamicPickup(1239, 16, DriveThrus[i][EGasPumpX],DriveThrus[i][EGasPumpY],DriveThrus[i][EGasPumpZ]);
		CreateDynamic3DTextLabel("(( /drivethru ))", X11_ORANGE,  DriveThrus[i][EGasPumpX],DriveThrus[i][EGasPumpY],DriveThrus[i][EGasPumpZ]+1.0,10.0);
	}
}
LoadBusinesses() {
	query[0] = 0;//[512];
	format(query,sizeof(query),"SELECT `business`.`id`,`name`,`owner`,`c1`.`username`,`extortion`,`c2`.`username`,`EntranceX`,`EntranceY`,`EntranceZ`,`ExitX`,`ExitY`,`ExitZ`,`business`.`interior`,`type`,`value`,`locked`,`entrancefee`,`till`,`products`,`maxproducts` FROM `business` LEFT JOIN `characters` AS `c1` ON `business`.`owner` = `c1`.`id` LEFT JOIN `characters` AS `c2` ON `business`.`extortion` = `c2`.`id`");
	mysql_function_query(g_mysql_handle, query, true, "onLoadBusiness", "");
}
public onLoadBusiness() {
	new data[128],id_val;
	new Float:f_val;
	new rows,fields;
	cache_get_data(rows,fields);
	printf("Number of businesses: %d\n", rows);
	for(new index=0;index<rows;index++) {
		new i = findFreeBusiness();
		cache_get_row(index, 0, data); //business SQL id
		id_val = strval(data);
		Business[i][EBusinessID] = id_val;
		cache_get_row(index, 1, data); //business name
		format(Business[i][EBusinessName],64,"%s",data);
		cache_get_row(index, 2, data); //owner SQL id
		id_val = strval(data);
		Business[i][EBusinessOwner] = id_val;
		if(Business[i][EBusinessOwner] == 0) {
			format(Business[i][EBusinessOwnerName],MAX_PLAYER_NAME,"No-One");
		} else if(Business[i][EBusinessOwner] == -1) {
			format(Business[i][EBusinessOwnerName],MAX_PLAYER_NAME,"The State");
		} else {
			cache_get_row(index, 3, data); //owner username
			format(Business[i][EBusinessOwnerName],MAX_PLAYER_NAME,"%s",data);
		}
		cache_get_row(index, 4, data); //extortion
		id_val = strval(data);
		Business[i][EBusinessExtortionist] = id_val;
		if(id_val != 0) {
			cache_get_row(index, 5, data); //extortionist username
			format(Business[i][EBusinessExtortionName],MAX_PLAYER_NAME,"%s",data);
		}
		cache_get_row(index, 6, data);
		f_val = floatstr(data);
		Business[i][EBusinessEntranceX] = f_val;
		cache_get_row(index, 7, data);
		f_val = floatstr(data);
		Business[i][EBusinessEntranceY] = f_val;
		cache_get_row(index, 8, data);
		f_val = floatstr(data);
		Business[i][EBusinessEntranceZ] = f_val;
		

		cache_get_row(index, 9, data);
		f_val = floatstr(data);
		Business[i][EBusinessExitX] = f_val;
		cache_get_row(index, 10, data);
		f_val = floatstr(data);
		Business[i][EBusinessExitY] = f_val;
		cache_get_row(index, 11, data);
		f_val = floatstr(data);
		Business[i][EBusinessExitZ] = f_val;
		cache_get_row(index, 12, data);
		id_val = strval(data);
		Business[i][EBusinessInterior] = id_val;
		cache_get_row(index, 13, data);
		id_val = strval(data);
		Business[i][EBType] = EBusinessType:id_val;
		cache_get_row(index, 14, data);
		id_val = strval(data);
		Business[i][EBusinessPrice] = id_val;
		cache_get_row(index, 15, data);
		id_val = strval(data);
		Business[i][EBusinessLocked] = id_val;
		Business[i][EBusinessVW] = i+BIZ_VW_OFFSET;
		cache_get_row(index, 16, data);
		id_val = strval(data);
		Business[i][EBusinessEntranceFee] = id_val;
		cache_get_row(index, 17, data);
		id_val = strval(data);
		Business[i][EBusinessTill] = id_val;
		
		cache_get_row(index, 18, data);
		id_val = strval(data);
		Business[i][EBusinessProducts] = id_val;
		
		cache_get_row(index, 19, data);
		id_val = strval(data);
		Business[i][EBusinessMaxProducts] = id_val;
		//1272:1273 was below
		Business[i][EBusinessPickup] = CreateDynamicPickup(Business[i][EBusinessOwner]>0?1239:1239, 16, Business[i][EBusinessEntranceX], Business[i][EBusinessEntranceY], Business[i][EBusinessEntranceZ]);
		getBiz3DLabelText(i, data, sizeof(data));
		Business[i][EBusinessText] = CreateDynamic3DTextLabel(data, X11_ORANGE, Business[i][EBusinessEntranceX], Business[i][EBusinessEntranceY], Business[i][EBusinessEntranceZ]+1.5,10.0);
		if(!IsDealership(i) && !IsFishStore(i) && !IsCarRental(i))
			Business[i][EBusinessExitPickup] = CreateDynamicPickup(1318, 16, Business[i][EBusinessExitX],Business[i][EBusinessExitY],Business[i][EBusinessExitZ], Business[i][EBusinessVW], Business[i][EBusinessInterior]);
		printf("Loaded business ID %d\n",Business[i][EBusinessID]);
	}
	if(bizinit == 0) {
		sellInactiveBusinesses();
		bizinit = 1;
	}
	printf("Businesses loaded.\n");
}
businessPickupPickup(playerid, pickupid) {
	for(new i=0;i<sizeof(Business);i++) {
		if(Business[i][EBusinessPickup] == pickupid) {
			if(Business[i][EBusinessLocked] == 1) break;
			if(IsDealership(i)) {
				GameTextForPlayer(playerid, "~g~Dealership~w~~n~Type /buy to buy a vehicle!", 2000, 3);
			} else if(IsFishStore(i)) {
				GameTextForPlayer(playerid, "~g~Fish Store~w~~n~Type /buy to buy food, and sell fish!", 2000, 3);
			} else if(IsCarRental(i)) {
				GameTextForPlayer(playerid, "~g~Car Rental~w~~n~Type /buy to rent a vehicle!", 2000, 3);
			}
		}
	}
}
YCMD:bizentrance(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Marks the entrance of a business for creation");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	SetPVarFloat(playerid,"BizEntranceX", X);
	SetPVarFloat(playerid,"BizEntranceY", Y);
	SetPVarFloat(playerid,"BizEntranceZ", Z);
	SendClientMessage(playerid, COLOR_DARKGREEN, "Business entrance Marked!");
	return 1;
}
YCMD:bizexit(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Marks the exit of a business for creation");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	SetPVarFloat(playerid,"BizExitX", X);
	SetPVarFloat(playerid,"BizExitY", Y);
	SetPVarFloat(playerid,"BizExitZ", Z);
	SetPVarInt(playerid, "BizInterior", GetPlayerInterior(playerid));
	SendClientMessage(playerid, COLOR_DARKGREEN, "Business exit Marked!");
	return 1;
}
YCMD:makebiz(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Marks the exit of a business for creation");
		return 1;
	}
	new type,price,level,name[64];
	if (!sscanf(params, "ddds[64]", type,price,level,name))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Attempting to create Business.");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /makebiz [type] [price] [level] [name]");
		SendClientMessage(playerid, X11_LIGHTBLUE,"Notice: use /bizentrance and /bizexit first!");
		return 1;
	}
	makeBusiness(GetPVarFloat(playerid,"BizEntranceX"),GetPVarFloat(playerid,"BizEntranceY"),GetPVarFloat(playerid,"BizEntranceZ"),GetPVarFloat(playerid,"BizExitX"),GetPVarFloat(playerid,"BizExitY"),GetPVarFloat(playerid,"BizExitZ"), GetPVarInt(playerid,"BizInterior"),EBusinessType:type,price,level,name);
	return 1;
}
YCMD:movebizexit(playerid, params[], help) {
	
}
YCMD:reloadbusiness(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Reloads a business");
		return 1;
	}
	new id;
	if (!sscanf(params, "d", id))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Reloading business");
		for(new i=0;i<sizeof(Business);i++) {
			if(Business[i][EBusinessID] == id) {
				reloadBusiness(i);
				return 1;
			}
		}
		SendClientMessage(playerid, X11_WHITE, "Failed to reload business");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /reloadbusiness [sqlid]");
	}
	return 1;
}

GetNumOwnedBusinesses(playerid) {
	new num;
	for(new i=0;i<sizeof(Business);i++) {
		if(Business[i][EBusinessOwner] == GetPVarInt(playerid, "CharID")) {
			num++;
		}
	}
	return num;
}
YCMD:buybiz(playerid, params[], help) {
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business");
		return 1;
	}
	new num = GetNumOwnedBusinesses(playerid);
	if(num >= GetPVarInt(playerid, "MaxBusinesses")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more businesses!");
		return 1;
	}
	if(Business[index][EBusinessOwner] != 0) {
		SendClientMessage(playerid, X11_WHITE, "This business is already owned");
		return 1;
	}
	if(GetPVarInt(playerid,"Level") < Business[index][EBusinessLevel]) {
		SendClientMessage(playerid, X11_WHITE, "You are not a high enough level to buy this.");
		return 1;
	}
	if(GetMoneyEx(playerid) < Business[index][EBusinessPrice]) {
		SendClientMessage(playerid, X11_WHITE, "You do not have enough money.");
		return 1;
	}
	GiveMoneyEx(playerid, -Business[index][EBusinessPrice]);
	setBusinessOwner(index, GetPVarInt(playerid, "CharID"));
	SendClientMessage(playerid, X11_GREEN, "Congratulations on your purchase.");
	return 1;
}
YCMD:extortion(playerid, params[], help) {
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business");
		return 1;
	}
	if((Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID") && !isBizAdmin(playerid)) || Business[index][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	new user, msg[128];
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		setExtortionist(index, GetPVarInt(user, "CharID"));
		format(msg, sizeof(msg), "* You have been made the extortionist of %s.",Business[index][EBusinessName]);
		SendClientMessage(user, COLOR_DARKGREEN, msg);
		format(msg, sizeof(msg), "* You have made %s the extortionist of %s.",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),Business[index][EBusinessName]);
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	} else {
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Extortionist Removed");
		setExtortionist(index, 0);
	}
	return 1;
}
YCMD:sellbiz(playerid, params[], help) {
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business");
		return 1;
	}
	if(!isBizAdmin(playerid) && Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID") || Business[index][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	GiveMoneyEx(playerid, floatround(Business[index][EBusinessPrice]*0.8));
	setBusinessOwner(index, 0);
	SendClientMessage(playerid, X11_GREEN, "You have sold your business!");
	return 1;
}
isBizAdmin(playerid) {
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BusinessAdmin) {
		return 1;
	}
	return 0;
}
YCMD:bizfee(playerid, params[], help) {
	new index = getStandingBusiness(playerid);
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set a business fee");
		return 1;
	}
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business");
		return 1;
	}
	if(!isBizAdmin(playerid) && Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID")) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	new fee;
	if (!sscanf(params, "d", fee))
    {
		if(fee < 0 || fee > 5) {
			if(!isBizAdmin(playerid)) {
				SendClientMessage(playerid, X11_RED2,"Fee must be above $0 and below $5");
				return 1;
			}
		}
		setBusinessFee(index, fee);
		SendClientMessage(playerid, X11_GREEN, "Fee updated");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /bizfee [fee]");
	}
	return 1;
}
YCMD:bizname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set a business name");
		return 1;
	}
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business.");
		return 1;
	}
	if(!isBizAdmin(playerid) && Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID")) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	new name[64];
	if(!sscanf(params, "s[64]",name)) {
		setBusinessName(index, name);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Name Set!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /bizname [name]");
	}
	return 1;
}
YCMD:open(playerid, params[], help) {
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business.");
		return 1;
	}
	if(!isBizAdmin(playerid) && Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID")) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	if(Business[index][EBusinessLocked] == 1) {
		Business[index][EBusinessLocked] = 0;
		GameTextForPlayer(playerid, "~w~Business ~g~Open", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
	} else {
		Business[index][EBusinessLocked] = 1;
		GameTextForPlayer(playerid, "~w~Business ~r~Closed", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
	}
	query[0] = 0;//[128];
	format(query,sizeof(query),"UPDATE `business` SET `locked` = %d WHERE `id` = %d", Business[index][EBusinessLocked], Business[index][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "onBusinessUpdate", "d",index);
	return 1;
}
YCMD:drivethru(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Buy food from a drive thru");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0) {
		SendClientMessage(playerid, X11_RED2, "You must be in a vehicle.");
		return 1;
	}
	new found = 0;
	for(new i=0;i<sizeof(DriveThrus);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 20.0, DriveThrus[i][EGasPumpX], DriveThrus[i][EGasPumpY], DriveThrus[i][EGasPumpZ])) {
			found = 1;
		}
	}
	if(!found) {
		SendClientMessage(playerid, X11_RED2, "You must be at a drive thru");
		return 1;
	}
	new bizid = getStandingBusiness(playerid, 55.0);
	if(bizid == -1) {
		SendClientMessage(playerid, X11_RED2, "Error: Associated business not found, contact an admin.");
		return 1;
	}
	if(Business[bizid][EBusinessLocked] == 1 || Business[bizid][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_RED2, "* Sorry, we are closed!");
		return 1;
	}
	showFastFoodMenu(playerid, bizid);
	return 1;
}
YCMD:fuel(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Fuels a players cars");
		return 1;
	}
	new carid = GetPlayerVehicleID(playerid);
	if(carid == 0) {
		new Float:X, Float:Y, Float:Z, foundcar = 1;
		carid = GetClosestVehicle(playerid);
		GetVehiclePos(carid, X, Y, Z);
		if(carid == -1 || !IsPlayerInRangeOfPoint(playerid, 8.0,X,Y,Z)){
			//SendClientMessage(playerid, X11_TOMATO_2, "You are not close enough to any vehicle!");
			//return 1;
			foundcar = 0;
		}
		new numcans = GetPVarInt(playerid, "GasCans");
		if(numcans > 0 && foundcar) {
			if(VehicleInfo[carid][EVEngine] == 1) {
				SendClientMessage(playerid, X11_RED2, "You can't fuel a car while it is running!");
				return 1;
			}
			new curfuel = VehicleInfo[carid][EVFuel];
			format(tempstr, sizeof(tempstr), "* %s fuels the vehicle.", GetPlayerNameEx(playerid, ENameType_RPName));
			ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			curfuel += 15;
			if(curfuel > 100) {
				curfuel = 100;				
				format(tempstr, sizeof(tempstr), "* Some fuel spills down the side.", GetPlayerNameEx(playerid, ENameType_RPName));
				ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			}
			VehicleInfo[carid][EVFuel] = curfuel;
			CheckGas();
			SetPVarInt(playerid, "GasCans", --numcans);
			return 1;
		}
		SendClientMessage(playerid, X11_RED2, "You must be in a vehicle.");
		return 1;
	}
	if(VehicleInfo[carid][EVEngine] == 1) {
		SendClientMessage(playerid, X11_RED2, "You can't fuel your car while it is running!");
		return 1;
	}
	new found = 0;
	for(new i=0;i<sizeof(GasPumps);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 20.0, GasPumps[i][EGasPumpX], GasPumps[i][EGasPumpY], GasPumps[i][EGasPumpZ])) {
			found = 1;
		}
	}
	if(!found) {
		SendClientMessage(playerid, X11_RED2, "You must be at a gas pump");
		return 1;
	}
	new bizid = getStandingBusiness(playerid, 35.0);
	if(bizid == -1) {
		SendClientMessage(playerid, X11_RED2, "Error: Associated business not found, contact an admin.");
		return 1;
	}
	if(Business[bizid][EBusinessLocked] == 1 || Business[bizid][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_RED2, "* Sorry, we are closed!");
		return 1;
	}
	format(tempstr, sizeof(tempstr), "* %s fuels the vehicle.", GetPlayerNameEx(playerid, ENameType_RPName));
	ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	GameTextForPlayer(playerid,"~w~~n~~n~~n~~n~~n~~n~~n~~n~~n~Re-Fueling Vehicle, please wait",2000,3);
	new RefuelWait = GasMax - VehicleInfo[carid][EVFuel];
	RefuelWait = (RefuelWait*100);
	SetTimerEx("FuelCar", RefuelWait, 0, "ddd", playerid, carid,bizid);
	return 1;
}
forward FuelCar(playerid, carid, business);
public FuelCar(playerid, carid, business) {
	new toadd = GasMax - VehicleInfo[carid][EVFuel];
	new price = toadd;
	if(VehicleInfo[carid][EVEngine] == 1) {
		SendClientMessage(playerid, X11_RED2, "Engine is running, cannot fuel vehicle");
		return 1;
	}
	if(GetMoneyEx(playerid) < price) {
		SendClientMessage(playerid, X11_RED2, "You do not have enough money to fill the vehicle");
		return 1;
	}
	/*
	if(Business[business][EBusinessProducts] < toadd) {
		SendClientMessage(playerid, X11_RED2, "* Sorry, we are out of fuel!");
		return 1;
	}
	*/
	format(tempstr,sizeof(tempstr),"Vehicle filled up for: $%s",getNumberString(price));
	SendClientMessage(playerid, COLOR_DARKGREEN, tempstr);
	VehicleInfo[carid][EVFuel] += toadd;
	addToBusinessProducts(business, -toadd);
	addToBusinessTill(business, floatround(price/2));
	CheckGas();
	return 1;
}
YCMD:buy(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to buy things from a business");
		return 1;
	}
	new i, foundentrance,foundexit;
	i = getBusinessInside(playerid);
	if(i == -1) {
		i = getStandingBusiness(playerid, 15.0);
		if(i == -1) {
			new ibiz = getWareHouseInside(playerid);
			if(ibiz == -1) {
			SendClientMessage(playerid, X11_WHITE, "Couldn't find a business");
			return 1;
			} else {
				showWareHouseMenu(playerid, ibiz);
				return 1;
			}
		} else {
			foundentrance = 1;
		}
	} else {
		foundexit = 1;
	}
	if(Business[i][EBusinessLocked] == 1 || Business[i][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This business is closed!");
		return 1;
	}
	SetPVarInt(playerid, "BuyingBiz", i);
	if(IsDealership(i) && foundentrance) {
		showDealershipMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_247 && foundexit) {
		show247Menu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_SexShop && foundexit) {
		showSexShopMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_Restaurant && foundexit) {
		showRestaurantMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_PizzaStack && foundexit) {
		showFastFoodMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_Ammunation && foundexit) {
		showAmmunationMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_FishStore && foundentrance) {
		showFishStoreMenu(playerid, i);
	} else if(IsCarRental(i) &&  foundentrance) {
		showRentalMenu(playerid, i);
	} else if(IsDrugFactory(i) && foundexit) {
		drugsPharmacyMenu(playerid, i);
	}
	return 1;
}
/*
YCMD:buy(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to buy things from a business");
		return 1;
	}
	new i, foundentrance,foundexit;
	i = getBusinessInside(playerid);
	if(i == -1) {
		i = getStandingBusiness(playerid, 15.0);
		if(i == -1) {
			SendClientMessage(playerid, X11_WHITE, "Couldn't find a business");
			return 1;
		} else {
			foundentrance = 1;
		}
	} else {
		foundexit = 1;
	}
	if(Business[i][EBusinessLocked] == 1 || Business[i][EBusinessOwner] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This business is closed!");
		return 1;
	}
	SetPVarInt(playerid, "BuyingBiz", i);
	if(IsDealership(i) && foundentrance) {
		showDealershipMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_247 && foundexit) {
		show247Menu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_SexShop && foundexit) {
		showSexShopMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_Restaurant && foundexit) {
		showRestaurantMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_PizzaStack && foundexit) {
		showFastFoodMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_Ammunation && foundexit) {
		showAmmunationMenu(playerid, i);
	} else if(Business[i][EBType] == EBusinessType_FishStore && foundentrance) {
		showFishStoreMenu(playerid, i);
	} else if(IsCarRental(i) &&  foundentrance) {
		showRentalMenu(playerid, i);
	}
	return 1;
}
*/
CanBuyGuns(playerid) {
	new time = gettime();
	new weaponbuytime = GetPVarInt(playerid, "WeaponBuyTime");
	if(GUN_BUY_TIME-(time-weaponbuytime) > 0) {
		return GUN_BUY_TIME-(time-weaponbuytime);
	}
	return 0;
}
sendMessageToBusinesses(playerid, Float:radi = 30.0, msg[], color) {
	for(new i=0;i<sizeof(Business);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, Business[i][EBusinessEntranceX],Business[i][EBusinessEntranceY],Business[i][EBusinessEntranceZ])) {
			SendAreaMessage(radi, Business[i][EBusinessExitX],Business[i][EBusinessExitY],Business[i][EBusinessExitZ], Business[i][EBusinessVW], msg, color);
		} else if(IsPlayerInRangeOfPoint(playerid, radi, Business[i][EBusinessExitX],Business[i][EBusinessExitY],Business[i][EBusinessExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == Business[i][EBusinessVW]) {
				SendAreaMessage(radi, Business[i][EBusinessEntranceX],Business[i][EBusinessEntranceY],Business[i][EBusinessEntranceZ], 0, msg, color);
			}
		}
	}
	return 1;
}
show247Menu(playerid, bizid) {
	#pragma unused bizid
	dialogstr[0] = 0;
	strcat(dialogstr,"Item\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(Buyable24Items);i++) {
		if(Buyable24Items[i][EBIType] == EType_Item) {
			format(tempstr,sizeof(tempstr),"%s\t$%s\n",Buyable24Items[i][EBIDisplayName],getNumberString(Buyable24Items[i][EBIPrice]));
		}else if(Buyable24Items[i][EBIType] == EType_NumSelect) {
			format(tempstr,sizeof(tempstr),"%s\t$%s/each\n",Buyable24Items[i][EBIDisplayName],getNumberString(Buyable24Items[i][EBIPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s\n",Buyable24Items[i][EBIDisplayName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, _:EBusiness_247BuyMenu, DIALOG_STYLE_TABLIST_HEADERS, "24/7 Buy Menu",dialogstr,"Select", "Quit");
	return 1;
}
showSexShopMenu(playerid, bizid) {
	#pragma unused bizid
	dialogstr[0] = 0;
	strcat(dialogstr,"Item\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(BuyableSexItems);i++) {
		if(BuyableSexItems[i][EBIType] == EType_Item) {
			format(tempstr,sizeof(tempstr),"%s\t$%s\n",BuyableSexItems[i][EBIDisplayName],getNumberString(BuyableSexItems[i][EBIPrice]));
		}else if(BuyableSexItems[i][EBIType] == EType_NumSelect) {
			format(tempstr,sizeof(tempstr),"%s\t$%s/each\n",BuyableSexItems[i][EBIDisplayName],getNumberString(BuyableSexItems[i][EBIPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s",BuyableSexItems[i][EBIDisplayName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, _:EBusiness_SexBuyMenu, DIALOG_STYLE_TABLIST_HEADERS, "Sex Shop Buy Menu",dialogstr,"Select", "Quit");
	return 1;
}
showRestaurantMenu(playerid, bizid) {
	#pragma unused bizid
	dialogstr[0] = 0;
	strcat(dialogstr,"Item\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(BuyableRestaurantItems);i++) {
		if(BuyableRestaurantItems[i][EBIType] == EType_Item) {
			format(tempstr,sizeof(tempstr),"%s\t$%s\n",BuyableRestaurantItems[i][EBIDisplayName],getNumberString(BuyableRestaurantItems[i][EBIPrice]));
		}else if(BuyableRestaurantItems[i][EBIType] == EType_NumSelect) {
			format(tempstr,sizeof(tempstr),"%s\t$%s/each\n",BuyableRestaurantItems[i][EBIDisplayName],getNumberString(BuyableRestaurantItems[i][EBIPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s",BuyableRestaurantItems[i][EBIDisplayName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, _:EBusiness_RestaurantMenu, DIALOG_STYLE_TABLIST_HEADERS, "Restaurant Buy Menu",dialogstr,"Select", "Quit");
	return 1;
}
showFastFoodMenu(playerid, bizid) {
	dialogstr[0] = 0;
	strcat(dialogstr,"Item\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(BuyableFastfoodItems);i++) {
		if(BuyableFastfoodItems[i][EBIType] == EType_Item) {
			format(tempstr,sizeof(tempstr),"%s\t$%s\n",BuyableFastfoodItems[i][EBIDisplayName],getNumberString(BuyableFastfoodItems[i][EBIPrice]));
		}else if(BuyableFastfoodItems[i][EBIType] == EType_NumSelect) {
			format(tempstr,sizeof(tempstr),"%s\t$%s/each\n",BuyableFastfoodItems[i][EBIDisplayName],getNumberString(BuyableFastfoodItems[i][EBIPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s",BuyableFastfoodItems[i][EBIDisplayName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	SetPVarInt(playerid, "BuyingBiz", bizid);
	ShowPlayerDialog(playerid, _:EBusiness_FastFoodMenu, DIALOG_STYLE_TABLIST_HEADERS, "Fast Food Buy Menu",dialogstr,"Select", "Quit");
	return 1;
}
showFishStoreMenu(playerid, bizid) {
	#pragma unused bizid
	dialogstr[0] = 0;
	strcat(dialogstr,"Item\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(BuyableFishStoreItems);i++) {
		if(BuyableFishStoreItems[i][EBIType] == EType_Item) {
			format(tempstr,sizeof(tempstr),"%s\t$%s\n",BuyableFishStoreItems[i][EBIDisplayName],getNumberString(BuyableFishStoreItems[i][EBIPrice]));
		}else if(BuyableFishStoreItems[i][EBIType] == EType_NumSelect) {
			format(tempstr,sizeof(tempstr),"%s\t$%s/each\n",BuyableFishStoreItems[i][EBIDisplayName],getNumberString(BuyableFishStoreItems[i][EBIPrice]));
		} else {
			format(tempstr,sizeof(tempstr),"%s",BuyableFishStoreItems[i][EBIDisplayName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, _:EBusiness_FishStoreMenu, DIALOG_STYLE_TABLIST_HEADERS, "Fast Food Buy Menu",dialogstr,"Select", "Quit");
}
showRentalMenu(playerid, bizid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	strcat(dialogstr,"Vehicle\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(RentableCars);i++) {
		format(tempstr, sizeof(tempstr), "%s\t$%s/hour\n", VehiclesName[RentableCars[i][ECarRental_ModelID]-400], getNumberString(RentableCars[i][ECarRental_HourlyRate]));
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	new model;
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(VehicleInfo[i][EVType] == EVehicleType_RentCar) {
			if(VehicleInfo[i][EVOwner] == playerid) {
				model = GetVehicleModel(i);
				format(tempstr, sizeof(tempstr), "Return %s\n", VehiclesName[model-400]);
				strcat(dialogstr,tempstr,sizeof(dialogstr));
			}
		}
	}
	ShowPlayerDialog(playerid, _:EBusiness_RentalMenu, DIALOG_STYLE_TABLIST_HEADERS, "Vehicle Rental Menu",dialogstr,"Select", "Cancel");
	SetPVarInt(playerid, "BuyingBiz", bizid);
}
showAmmunationMenu(playerid, bizid) {
	#pragma unused bizid
	dialogstr[0] = 0;
	new gunname[32];
	for(new i=0;i<sizeof(BuyableAmmunationGuns);i++) {
		if(BuyableAmmunationGuns[i][EBuyableGunType] == 0) {
			GetWeaponNameEx(BuyableAmmunationGuns[i][EBuyableGunID],gunname, sizeof(gunname));
			format(tempstr,sizeof(tempstr),"%s - $%s\n",gunname,getNumberString(BuyableAmmunationGuns[i][EBuyableGunPrice]));
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EBusiness_AmmunationMenu, DIALOG_STYLE_LIST, "Ammunation Buy Menu",dialogstr,"Buy", "Quit");
	return 1;
}
businessTryEnter(playerid) {
	for(new i=0;i<MAX_BUSINESS;i++) {
		if(Business[i][EBusinessID] != 0) {
			if(IsPlayerInRangeOfPoint(playerid,1.5,Business[i][EBusinessEntranceX], Business[i][EBusinessEntranceY], Business[i][EBusinessEntranceZ])) {
				enterBiz(playerid, i);
			} else if(IsPlayerInRangeOfPoint(playerid,1.5,Business[i][EBusinessExitX],Business[i][EBusinessExitY],Business[i][EBusinessExitZ])) {
				if(GetPlayerVirtualWorld(playerid) == Business[i][EBusinessVW]) {
					exitBiz(playerid, i);
				}
			}
		}
	}
}
enterBiz(playerid, index) {
	if((Business[index][EBusinessLocked] == 1 && Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID")) || Business[index][EBusinessOwner] == 0 || IsDealership(index) || IsFishStore(index) || IsCarRental(index)) {
		GameTextForPlayer(playerid, "~r~Locked", 2000, 1);
		return 1;
	}
	if(Business[index][EBusinessEntranceFee] != 0) {
		if(GetMoneyEx(playerid) < Business[index][EBusinessEntranceFee]) {
			SendClientMessage(playerid, X11_RED2, "You don't have enough money to enter.");
			return 1;
		} else {
			GiveMoneyEx(playerid, -Business[index][EBusinessEntranceFee]);
			format(tempstr, sizeof(tempstr), "~y~%s~n~~r~-$%s", Business[index][EBusinessName], getNumberString(Business[index][EBusinessEntranceFee]));
			GameTextForPlayer(playerid, tempstr, 2000, 3);
			bizIncome(index, Business[index][EBusinessEntranceFee]/2);
		}
	}
	if(Business[index][EBType] == EBusinessType_PaintBall) {
		SetPlayerVirtualWorld(playerid, Business[index][EBusinessVW]);
		putPlayerInPaintball(playerid);
		return 1;
	} else if(Business[index][EBType] == EBusinessType_RaceTrack) {
		SetPlayerVirtualWorld(playerid, Business[index][EBusinessVW]);
		putPlayerInRacing(playerid);
	}
	if(Business[index][EBusinessExitX] != 0 || Business[index][EBusinessExitY] != 0 || Business[index][EBusinessExitZ] != 0) {
		SetPlayerPos(playerid,Business[index][EBusinessExitX],Business[index][EBusinessExitY],Business[index][EBusinessExitZ]);
		SetPlayerInterior(playerid, Business[index][EBusinessInterior]);
		SetPlayerVirtualWorld(playerid, Business[index][EBusinessVW]);
	}
	showBusinessHelp(playerid, Business[index][EBType]);
	return 0;
}
exitBiz(playerid, index) {
	if(IsDealership(index) || IsFishStore(index)) {
		return 0;
	}
	SetPlayerPos(playerid,Business[index][EBusinessEntranceX],Business[index][EBusinessEntranceY],Business[index][EBusinessEntranceZ]);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, 0);
	return 1;
}
bizIncome(index, price) {
	Business[index][EBusinessTill] += price;
	query[0] = 0;//[128];
	format(query,sizeof(query),"UPDATE `business` SET `till` = %d WHERE `id` = %d",Business[index][EBusinessTill],Business[index][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
forward OnBusinessCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, EBusinessType:type, price, level,name[]);
public OnBusinessCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, EBusinessType:type, price, level,name[]) {
	new id = mysql_insert_id();
	new index = findFreeBusiness();
	new data[128];
	if(index == -1) {
			ABroadcast(X11_RED,"[AdmWarn]: Failed to create business. Business array is full.",EAdminFlags_BasicAdmin);
			return 0;
	}
	Business[index][EBusinessID] = id;
	format(Business[index][EBusinessName],64,"%s",name);
	Business[index][EBusinessEntranceX] = X;
	Business[index][EBusinessEntranceY] = Y;
	Business[index][EBusinessEntranceZ] = Z;
	Business[index][EBusinessExitX] = EX;
	Business[index][EBusinessExitY] = EY;
	Business[index][EBusinessExitZ] = EZ;
	Business[index][EBusinessInterior] = interior;
	Business[index][EBType] = EBusinessType:type;
	Business[index][EBusinessVW] = index+BIZ_VW_OFFSET;
	Business[index][EBusinessExtortionName][0] = 0;
	Business[index][EBusinessExtortionist] = 0;
	Business[index][EBusinessPrice] = price;
	Business[index][EBusinessPickup] = CreateDynamicPickup(1273, 16, Business[index][EBusinessEntranceX], Business[index][EBusinessEntranceY], Business[index][EBusinessEntranceZ]);
	if(Business[index][EBusinessExitX] != 0)
		Business[index][EBusinessExitPickup] = CreateDynamicPickup(1318, 16, Business[index][EBusinessExitX],Business[index][EBusinessExitY],Business[index][EBusinessExitZ], Business[index][EBusinessVW], Business[index][EBusinessInterior]);
	getBiz3DLabelText(index, data, sizeof(data));
	Business[index][EBusinessText] = CreateDynamic3DTextLabel(data, X11_ORANGE, Business[index][EBusinessEntranceX], Business[index][EBusinessEntranceY], Business[index][EBusinessEntranceZ]+1.5,10.0);
	format(data,sizeof(data),"[AdmNotice]: Business ID: %d",id);
	ABroadcast(X11_RED,data,EAdminFlags_BasicAdmin);
	return 1;
}
findFreeBusiness() {
	for(new i=0;i<sizeof(Business);i++) {
		if(Business[i][EBusinessID] == 0) {
			return i;
		}
	}
	return -1;
}
makeBusiness(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, EBusinessType:type, price, level,name[]) {
	query[0] = 0;
	mysql_real_escape_string(name,tempstr,g_mysql_handle);
	format(query,sizeof(query),"INSERT INTO `business` (`EntranceX`,`EntranceY`,`EntranceZ`,`ExitX`,`ExitY`,`ExitZ`,`interior`,`type`,`value`,`level`,`name`) VALUES (%f,%f,%f,%f,%f,%f,%d,%d,%d,%d,\"%s\")",
	X,Y,Z,EX,EY,EZ,interior,_:type,price,level,tempstr);
	mysql_function_query(g_mysql_handle, query, true, "OnBusinessCreate", "ffffffdddds",X,Y,Z,EX,EY,EZ,interior,_:type,price,level,name);
}
getBiz3DLabelText(index, dst[], len) {
	if(Business[index][EBusinessOwner] == 0) {
		format(dst, len, "{00FF00}$$ For Sale $$\n$%s\n\n{FF9933}%s",getNumberString(Business[index][EBusinessPrice]), Business[index][EBusinessName]);
	} else {
		new openmsg[32];
		if(Business[index][EBusinessLocked] == 0) {
			format(openmsg,sizeof(openmsg),"{00FF00}OPEN");
		} else {
			format(openmsg,sizeof(openmsg),"{FF0000}CLOSED");
		}
		new ext[32];
		if(Business[index][EBusinessExtortionist] != 0) {
			format(ext, sizeof(ext), "\nExtortion: %s",Business[index][EBusinessExtortionName]);
		} else ext[0] = 0;
		new fee[32];
		if(Business[index][EBusinessEntranceFee] != 0) {
			format(fee, sizeof(fee), "\nEntrance Fee: $%s",getNumberString(Business[index][EBusinessEntranceFee]));
		} else fee[0] = 0;
		format(dst, len, "%s\n\n{FF9933}%s%s\nOwner: %s%s", openmsg, Business[index][EBusinessName],fee,Business[index][EBusinessOwnerName],ext);
	}
}
public onBusinessUpdate(index) {
	reloadBusiness(index);
}
reloadBusiness(index) {
	new id = Business[index][EBusinessID];
	Business[index][EBusinessID] = 0;
	DestroyDynamic3DTextLabel(Business[index][EBusinessText]);
	DestroyDynamicPickup(Business[index][EBusinessPickup]);
	DestroyDynamicPickup(Business[index][EBusinessExitPickup]);
	query[0] = 0;//[512];
	format(query,sizeof(query),"SELECT `business`.`id`,`name`,`owner`,`c1`.`username`,`extortion`,`c2`.`username`,`EntranceX`,`EntranceY`,`EntranceZ`,`ExitX`,`ExitY`,`ExitZ`,`business`.`interior`,`type`,`value`,`locked`,`entrancefee`,`till`,`products`,`maxproducts` FROM `business` LEFT JOIN `characters` AS `c1` ON `business`.`owner` = `c1`.`id` LEFT JOIN `characters` AS `c2` ON `business`.`extortion` = `c2`.`id` WHERE `business`.`id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "onLoadBusiness", "");
}
setBusinessOwner(index, sqlid) {
	query[0] = 0;//[128];
	format(query,sizeof(query),"UPDATE `business` SET `owner` = %d WHERE `id` = %d", sqlid, Business[index][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "onBusinessUpdate", "d",index);
}
setBusinessFee(index, fee) {
	query[0] = 0;//[128];
	format(query,sizeof(query),"UPDATE `business` SET `entrancefee` = %d WHERE `id` = %d", fee, Business[index][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "onBusinessUpdate", "d",index);
}
getStandingBusiness(playerid, Float:distance = 1.5) {
	for(new i=0;i<sizeof(Business);i++) {
		if(IsPlayerInRangeOfPoint(playerid,distance,Business[i][EBusinessEntranceX], Business[i][EBusinessEntranceY], Business[i][EBusinessEntranceZ])) {
			return i;
		} else if(IsPlayerInRangeOfPoint(playerid,distance,Business[i][EBusinessExitX], Business[i][EBusinessExitY], Business[i][EBusinessExitZ])) {
				if(IsDealership(i) || IsFishStore(i)) {
					return i;
				}
			}
	}
	return -1;
}
getBusinessInside(playerid, Float:distance = 150.0) {
	new vw = GetPlayerVirtualWorld(playerid);
	for(new i=0;i<sizeof(Business);i++) {
		if(IsPlayerInRangeOfPoint(playerid,distance,Business[i][EBusinessExitX], Business[i][EBusinessExitY], Business[i][EBusinessExitZ])) {
			if(vw == Business[i][EBusinessVW]) {
				return i;
			}
		}
	}
	return -1;
}
YCMD:robbiz(playerid, params[], help) {
	new biz = getBusinessInside(playerid);
	if(biz == -1) {
		biz = getStandingBusiness(playerid, 15.0);
		if(biz == -1 || (!IsDealership(biz) && !IsFishStore(biz))) {
			SendClientMessage(playerid, X11_TOMATO_2, "You arn't inside a business");
			return 1;
		}
	}
	new weapon = GetPlayerWeaponEx(playerid);
	if(!isDangerousWeapon(weapon)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't armed!");
		return 1;
	}
	if(GetPVarType(playerid, "BizRobTimer") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are currently robbing a business");
		return 1;
	}
	new timenow = gettime();
	if(BIZROB_COOLDOWN-(timenow-Business[biz][EBusinessLastRobbery]) > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This business has been robbed recently, come back later!");
		return 1;
	}
	Business[biz][EBusinessLastRobbery] =  timenow;
	new biztimer = SetTimerEx("BusinessRob", 300000, false ,"dd", playerid, biz);
	SetPVarInt(playerid, "BizRobTimer", biztimer);
	new level = GetPVarInt(playerid, "WantedLevel");
	level += 6;
	SetPlayerWantedLevel(playerid, level);
	SetPVarInt(playerid, "WantedLevel", level);
    SendCopMessage(X11_BLUE, "|__All Units!__|");
	format(tempstr, sizeof(tempstr), "Armed robbery at %s!", Business[biz][EBusinessName]);
	SendCopMessage(COLOR_LIGHTRED, tempstr);
	InsertCrime(playerid, -1, "Armed Robbery");
	return 1;
}
IsRobbingBusines(playerid) {
	if(GetPVarType(playerid, "BizRobTimer") == PLAYER_VARTYPE_NONE) {
		return -1;
	}
	return GetPVarInt(playerid, "BizRobTimer");
}
StopRobBiz(playerid) {
	new biztimer = IsRobbingBusines(playerid);
	if(biztimer != -1) {
		KillTimer(biztimer);
		DeletePVar(playerid, "BizRobTimer");
	}
}
forward BusinessRob(playerid, biz);
public BusinessRob(playerid, biz) {
	new inbiz = getBusinessInside(playerid);
	if(biz != inbiz) {
		inbiz = getStandingBusiness(playerid, 55.0);
		if((!IsDealership(biz)  && !IsDealership(inbiz) && !IsCarRental(biz) && !IsCarRental(inbiz)) || inbiz != biz) {
			SendClientMessage(playerid, X11_TOMATO_2, "You didn't get any money because you left the business!");
			return 1;
		}
	}
	if(GetPlayerWeaponEx(playerid) < 22) {
		SendClientMessage(playerid, COLOR_LIGHTRED, "You need a Gun to Rob Businesses! The Cashier ran off!");
		return 1;
	}
	if(Business[biz][EBusinessTill] < 100) {
		SendClientMessage(playerid, X11_TOMATO_2, "This business is out of money!");
		return 1;
	}
	new money;
	new perc;
	perc = random(10)+1;
	money = floatround(Business[biz][EBusinessTill] / 200);
	money = floatround(money * perc);
	GiveMoneyEx(playerid, money);
	addToBusinessTill(biz, -money);
	format(tempstr, sizeof(tempstr), "* You got $%s out of the Robbery!", getNumberString(money));
	SendClientMessage(playerid, COLOR_DARKGREEN, tempstr);
	DeletePVar(playerid, "BizRobTimer");
	return 1;
}
YCMD:bizbank(playerid, params[], help) {
	SendClientMessage(playerid, X11_TOMATO_2, "Use /bizdeposit and /bizwithdraw!");
	return 1;
}
YCMD:biztill(playerid, params[], help) {
	SendClientMessage(playerid, X11_TOMATO_2, "Use /bizdeposit and /bizwithdraw!");
	return 1;
}
YCMD:bizwithdraw(playerid, params[], help) {
	new amount;
	new bizid = getBusinessInside(playerid);
	if(bizid == -1) {
		bizid = getStandingBusiness(playerid);
		if(!IsDealership(bizid) && !IsPaintball(bizid) && !IsFishStore(bizid) && !IsCarRental(bizid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You arn't inside your business");
			return 1;
		}
	}
	if((Business[bizid][EBusinessOwner] != GetPVarInt(playerid,"CharID") || Business[bizid][EBusinessOwner] == 0) && Business[bizid][EBusinessExtortionist] != GetPVarInt(playerid,"CharID")) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	if(!sscanf(params,"d",amount)) {
		if(amount < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(amount > Business[bizid][EBusinessTill]) {
			SendClientMessage(playerid, X11_TOMATO_2, "There isn't enough money in the till!");
			return 1;
		}
		addToBusinessTill(bizid, -amount);
		GiveMoneyEx(playerid, amount);
		SendClientMessage(playerid, COLOR_DARKGREEN, "Amount Withdrawn");
	} else {
		format(tempstr, sizeof(tempstr), "Till Amount: %d",Business[bizid][EBusinessTill]);
		SendClientMessage(playerid, X11_WHITE, tempstr);
	}
	return 1;
}

YCMD:bizdeposit(playerid, params[], help) {
	new amount;
	new bizid = getBusinessInside(playerid);
	if(bizid == -1) {
		bizid = getStandingBusiness(playerid);
		if(!IsDealership(bizid) && !IsPaintball(bizid) && !IsFishStore(bizid) && !IsCarRental(bizid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't inside your business");
			return 1;
		}
	}
	if((Business[bizid][EBusinessOwner] != GetPVarInt(playerid,"CharID") || Business[bizid][EBusinessOwner] == 0) && Business[bizid][EBusinessExtortionist] != GetPVarInt(playerid,"CharID")) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	if(!sscanf(params,"d",amount)) {
		if(amount < 1 || amount > 5000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(amount > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
			return 1;
		}
		addToBusinessTill(bizid, amount);
		GiveMoneyEx(playerid, -amount);
		SendClientMessage(playerid, COLOR_DARKGREEN, "Amount Deposited");
	}
	return 1;
}
setupDealershipMenuState(playerid, biztype, curpage = 1, bizid = -1) {
	new remaining_amount, pages, numcars;
	new start_index = (curpage-1) * (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	new temptxt[128];

	new vehstr[32],vehprice[32];

	clearModelSelectMenu(playerid);
	if(start_index < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	#define AddCarItemToPreviewer(%1) numcars = sizeof(%1); \
		for(new i=start_index;i<sizeof(%1);i++) { \
			format(vehstr,sizeof(vehstr),"%s",VehiclesName[%1[i][EBCarModelID]-400]); \
			format(vehprice,sizeof(vehprice), "~%c~$%s",GetMoneyEx(playerid)>=%1[i][EBCarPrice]?'g':'r',getNumberString(%1[i][EBCarPrice])); \
			addItemToModelSelectMenu(playerid, %1[i][EBCarModelID], vehstr,vehprice); \
		}

	switch(biztype) {
		case EBusinessType_D_NormCar: {
			AddCarItemToPreviewer(EBDNormalCars)
		}
		case EBusinessType_D_FastCar: {
			AddCarItemToPreviewer(EBDFastCars)
		}
		case EBusinessType_D_Truck: {
			AddCarItemToPreviewer(EBDTrucks)
		}
		case EBusinessType_D_Bike: {
			AddCarItemToPreviewer(EBDBikes)
		}
		case EBusinessType_D_Boat: {
			AddCarItemToPreviewer(EBDBoats)
		}
		case EBusinessType_D_Plane: {
			AddCarItemToPreviewer(EBDPlanes)
		}

	}

	remaining_amount = numcars % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numcars) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	DealerMenuState[playerid][EDSMS_BizID] = bizid;
	DealerMenuState[playerid][EDSMS_CurPage] = curpage;
	DealerMenuState[playerid][EDSMS_PageCount] = pages;
	DealerMenuState[playerid][EDSMS_RemainingCount] = remaining_amount;
	DealerMenuState[playerid][EDSMS_DealerType] = biztype;
	DealerMenuState[playerid][EDSMS_InPreviewMode] = 0;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numcars, "Dealership Menu", page_str, "OnDealershipMenuSelect", -25.0000, 0.0000, -55.2600);
}
forward OnDealershipMenuSelect(playerid, action, modelid, title[]);
public OnDealershipMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			DealerMenuState[playerid][EDSMS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		new modelid = DealerMenuState[playerid][EDSMS_InPreviewMode];
		new price = getModelValue(modelid, DealerMenuState[playerid][EDSMS_DealerType]);
		new biz = DealerMenuState[playerid][EDSMS_BizID];
		new num = getNumOwnedCars(playerid);
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		if(num >= GetPVarInt(playerid, "MaxCars")) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more cars");
			return 0;
		}
		format(tempstr,sizeof(tempstr),"Congratulations! You are now the owner of a %s",VehiclesName[modelid-400]);
		if(GetMoneyEx(playerid) >= price) {
			GiveMoneyEx(playerid, -price);
			SendClientMessage(playerid, COLOR_DARKGREEN, tempstr);
			new Float:X,Float:Y,Float:Z,Float:A;
			GetPlayerPos(playerid, X, Y, Z);
			GetPlayerFacingAngle(playerid, A);
			addToBusinessTill(biz, floatround(price/12));
			addToBusinessProducts(biz, -25);
			CreatePlayerCar(playerid,modelid, 0, 0, Business[biz][EBusinessExitX],Business[biz][EBusinessExitY],Business[biz][EBusinessExitZ], A, ELockType_Default,1);
		} else {
			SendClientMessage(playerid, X11_RED, "You don't have enough money");
		}
	} else if(action == EModelPreviewAction_Next) {
		setupDealershipMenuState(playerid, DealerMenuState[playerid][EDSMS_DealerType], DealerMenuState[playerid][EDSMS_CurPage] + 1, DealerMenuState[playerid][EDSMS_BizID]);
	} else if(action == EModelPreviewAction_Back) {
		setupDealershipMenuState(playerid, DealerMenuState[playerid][EDSMS_DealerType], DealerMenuState[playerid][EDSMS_CurPage] - 1, DealerMenuState[playerid][EDSMS_BizID]);
	} else if(action == EModelPreviewAction_Exit) {
		if(DealerMenuState[playerid][EDSMS_InPreviewMode] != 0) {
			setupDealershipMenuState(playerid,DealerMenuState[playerid][EDSMS_DealerType], DealerMenuState[playerid][EDSMS_CurPage], DealerMenuState[playerid][EDSMS_BizID]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
}
showDealershipMenu(playerid,index) {
    dialogstr[0] = 0;
	if(Business[index][EBusinessOwner] == 0 || Business[index][EBusinessLocked] == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Sorry we are closed!");
		return 1;
	}
	
	setupDealershipMenuState(playerid,Business[index][EBType], 1, index);
	return 0;
}
carRentalIdxFromModel(model) {
	for(new i=0;i<sizeof(RentableCars);i++) {
		if(RentableCars[i][ECarRental_ModelID] == model) {
			return i;
		}
	}
	return -1;
}
bOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	switch(dialogid) {
		case EBusiness_247BuyMenu: {
			if(!response || listitem < 0) {
				return 0;
			}
			if(Buyable24Items[listitem][EBIType] != EType_NumSelect) {
				CallLocalFunction(Buyable24Items[listitem][EBIOnBuyCallback],"dddd",playerid, listitem, GetPVarInt(playerid, "BuyingBiz"),0);
				DeletePVar(playerid, "BuyingBiz");
			} else {
				SetPVarInt(playerid, "BuyingItem", listitem);
				format(tempstr,sizeof(tempstr),"How many %s's would you like?",Buyable24Items[listitem][EBIDisplayName]);
				ShowPlayerDialog(playerid, _:EBusiness_247NumberInput, DIALOG_STYLE_INPUT, "How many?", tempstr,"Ok","Cancel");
			}
		}
		case EBusiness_247NumberInput: {
			if(!response) {
				return 0;
			}
			new item = GetPVarInt(playerid, "BuyingItem");
			new number = strval(inputtext);
			if(number < 1 || number > 9999) {
				format(tempstr,sizeof(tempstr),"How many %s's would you like?",Buyable24Items[item][EBIDisplayName]);
				ShowPlayerDialog(playerid, _:EBusiness_247NumberInput, DIALOG_STYLE_INPUT, "How many?", tempstr,"Ok","Cancel");
				return 0;
			}
			DeletePVar(playerid,"BuyingItem");
			CallLocalFunction(Buyable24Items[item][EBIOnBuyCallback],"dddd",playerid, item, GetPVarInt(playerid, "BuyingBiz"),number);
		}
		case EBusiness_SexBuyMenu: {
			if(!response) {
				return 0;
			}
			if(BuyableSexItems[listitem][EBIType] != EType_NumSelect) {
				CallLocalFunction(BuyableSexItems[listitem][EBIOnBuyCallback],"dddd",playerid, listitem, GetPVarInt(playerid, "BuyingBiz"),0);
				DeletePVar(playerid, "BuyingBiz");
			} else {
				SetPVarInt(playerid, "BuyingItem", listitem);
				format(tempstr,sizeof(tempstr),"How many %s's would you like?",BuyableSexItems[listitem][EBIDisplayName]);
				ShowPlayerDialog(playerid, _:EBusiness_SexNumberInput, DIALOG_STYLE_INPUT, "How many?", tempstr,"Ok","Cancel");
			}
		}
		case EBusiness_RestaurantMenu: {
			if(!response) {
				return 0;
			}
			if(BuyableRestaurantItems[listitem][EBIType] != EType_NumSelect) {
				CallLocalFunction(BuyableRestaurantItems[listitem][EBIOnBuyCallback],"dddd",playerid, listitem, GetPVarInt(playerid, "BuyingBiz"),0);
				DeletePVar(playerid, "BuyingBiz");
			}
		}	
		case EBusiness_FastFoodMenu: {
			if(!response) {
				return 0;
			}
			if(BuyableFastfoodItems[listitem][EBIType] != EType_NumSelect) {
				CallLocalFunction(BuyableFastfoodItems[listitem][EBIOnBuyCallback],"dddd",playerid, listitem, GetPVarInt(playerid, "BuyingBiz"),0);
				DeletePVar(playerid, "BuyingBiz");
			}
		}
		case EBusiness_SexNumberInput: {
			new item = GetPVarInt(playerid, "BuyingItem");
			new number = strval(inputtext);
			if(number < 1 || number > 9999) {
				format(tempstr,sizeof(tempstr),"How many %s's would you like?",BuyableSexItems[listitem][EBIDisplayName]);
				ShowPlayerDialog(playerid, _:EBusiness_SexNumberInput, DIALOG_STYLE_INPUT, "How many?", tempstr,"Ok","Cancel");
				return 0;
			}
			DeletePVar(playerid,"BuyingItem");
			CallLocalFunction(BuyableSexItems[item][EBIOnBuyCallback],"dddd",playerid, item, GetPVarInt(playerid, "BuyingBiz"),number);
		}
		case EBusiness_DrinkMenu: {
			if(!response) {
				return 0;
			}
			new biz = getBusinessInside(playerid);
			if(biz == -1) return 0;
			if(GetMoneyEx(playerid) < Drinks[listitem][EDrinkPrice]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
				return 1;
			}
			GiveMoneyEx(playerid, -Drinks[listitem][EDrinkPrice]);
			new Float:HP;
			GetPlayerHealth(playerid, HP);
			HP += Drinks[listitem][EDrinkHealthBoost];
			SetPlayerHealthEx(playerid, HP);
			addToBusinessTill(biz, floatround(Drinks[listitem][EDrinkPrice]/2));
			addToBusinessProducts(biz, -1);
			if(Drinks[listitem][EDrinkSpecialAction] != SPECIAL_ACTION_NONE) {
				SetPlayerSpecialAction(playerid, Drinks[listitem][EDrinkSpecialAction]);
			}
			format(tempstr, sizeof(tempstr), Drinks[listitem][EDrinkAction], GetPlayerNameEx(playerid, ENameType_RPName));
			ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			
		}
		case EBusiness_LockBuy: {
			if(!response) {
				return 0;
			}
			if(GetPVarType(playerid, "HoldingLock") != PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_TOMATO_2, "You already are holding a lock! Do /destroylock to remove it, or use it in a car!");
				return 1;
			}
			new biz = getBusinessInside(playerid);
			if(biz == -1) return 0;
			if(GetMoneyEx(playerid) < BuyableCarLocks[listitem][EBLockPrice]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
				return 1;
			}
			addToBusinessTill(biz, floatround(BuyableCarLocks[listitem][EBLockPrice]/2));
			addToBusinessProducts(biz, -1);
			GiveMoneyEx(playerid, -BuyableCarLocks[listitem][EBLockPrice]);
			SetPVarInt(playerid, "HoldingLock", _:BuyableCarLocks[listitem][EBLockType]);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Congratulations! Go into a car and do /uselock");
		}
		case EBusiness_AmmunationMenu: {
			if(!response) return 0;
			new biz = getBusinessInside(playerid);
			if(biz == -1) return 0;
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			if(~lflags & ELicense_Gun && BuyableAmmunationGuns[listitem][EBuyableGunNeedLicense]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have a weapons license!");
				return 1;
			}
			if(GetMoneyEx(playerid) < BuyableAmmunationGuns[listitem][EBuyableGunPrice]) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
				return 1;
			}
			#if BUYGUN_LIMIT
			new remaining = CanBuyGuns(playerid);
			if(BuyableAmmunationGuns[listitem][EBuyableGunBuyLimit] != 0) {
				if(remaining != 0) {
					new msg[128];
					format(msg, sizeof(msg), "You must wait {FF6347}%d{FFFFFF} minute(s) until you can buy another weapon!", remaining/60);
					SendClientMessage(playerid, X11_WHITE, msg);
					return 1;
				}
			}
			#endif
			if(BuyableAmmunationGuns[listitem][EBuyableGunType] == 0) {
				//GivePlayerWeaponEx(playerid,BuyableAmmunationGuns[listitem][EBuyableGunID], -1);
				if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
					return 1;
				} else {
					GivePlayerWeaponEx(playerid, BuyableAmmunationGuns[listitem][EBuyableGunID], -1);
				}
				SetPVarInt(playerid, "WeaponBuyTime",gettime()); //Set the weapon buy time limit
			} else if(BuyableAmmunationGuns[listitem][EBuyableGunType] == 2) {
				new Float:HP;
				GetPlayerHealth(playerid, HP);
				HP += float(BuyableAmmunationGuns[listitem][EBuyableGunID]);
				if(HP > MAX_HEALTH) HP = MAX_HEALTH;
				SetPlayerHealthEx(playerid, HP);
			} else if(BuyableAmmunationGuns[listitem][EBuyableGunType] == 3) {
				if(GetPVarInt(playerid, "SpecialItem") != -1) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are already holding a special item!");
					return 1;
				}
				GivePlayerItem(playerid, BuyableAmmunationGuns[listitem][EBuyableGunID]);
			}
			GiveMoneyEx(playerid, -BuyableAmmunationGuns[listitem][EBuyableGunPrice]);
			addToBusinessTill(biz, floatround(BuyableAmmunationGuns[listitem][EBuyableGunPrice]*0.50));
			SendClientMessage(playerid, COLOR_DARKGREEN, "Congratulations on your purchase");
		}
		case EBusiness_LottoChoose: {
			if(!response) return 0;
			new biz = getBusinessInside(playerid);
			if(biz == -1) return 0;
			switch(listitem) {
				case 0: {
					if(GetMoneyEx(playerid) < 250) {
						SendClientMessage(playerid, X11_TOMATO_2, "Not enough money!");
						return 1;
					}
					GiveMoneyEx(playerid, -250);
					addToBusinessTill(biz, 250);
					new number = giveRandomLottoNumber(playerid);
					format(query, sizeof(query), "* Your lotto number is: %d, you have a chance of winning the $%s jackpot!",number,getNumberString(getLottoFunds()));
					SendClientMessage(playerid, X11_YELLOW, query);
					adjustLottoFunds(5000); //add 5000 every time someone buys a ticket
					HintMessage(playerid, COLOR_DARKGREEN, "Do /lottoinfo to see lottery info, and /trashlotto to trash your ticket");
					return 1;
				}
				case 1: {
					ShowPlayerDialog(playerid, EBusiness_EnterLottoNumber,DIALOG_STYLE_INPUT, "Enter Desired Number", "Enter the lottery number you would like.", "Buy", "Cancel");
					return 1;
				}
			}
			return 1;
		}
		case EBusiness_EnterLottoNumber: {
			if(!response) return 0;
			new biz = getBusinessInside(playerid);
			if(biz == -1) return 0;
			if(GetMoneyEx(playerid) < 350) {
				SendClientMessage(playerid, X11_TOMATO_2, "Not enough money!");
				return 1;
			}
			GiveMoneyEx(playerid, -350);
			addToBusinessTill(biz, 350);
			adjustLottoFunds(5000); //add 5000 every time someone buys a ticket
			new number = strval(inputtext);
			if(isLottoNumberAcceptable(number)) {
				setPlayerLottoTicket(playerid, number);
				format(query, sizeof(query), "* Your lotto number is: %d, you have a chance of winning the $%s jackpot!",number,getNumberString(getLottoFunds()));
				SendClientMessage(playerid, X11_YELLOW, query);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "This lotto number is not acceptable! Try a different number!");
				format(query, sizeof(query), "* The lotto number must be above 1, and below %s",getNumberString(getLottoMax()));
				SendClientMessage(playerid, COLOR_DARKGREEN, query);
			}
			HintMessage(playerid, COLOR_DARKGREEN, "Do /lottoinfo to see lottery info, and /trashlotto to trash your ticket");
			return 1;
		}
		case EBusiness_FishStoreMenu: {
			if(!response || listitem < 0) {
				return 0;
			}
			if(BuyableFishStoreItems[listitem][EBIType] != EType_NumSelect) {
				CallLocalFunction(BuyableFishStoreItems[listitem][EBIOnBuyCallback],"dddd",playerid, listitem, GetPVarInt(playerid, "BuyingBiz"),0);
				DeletePVar(playerid, "BuyingBiz");
			}
		}
		case EBusiness_FishSell: {
			if(!response || listitem < 0) return 0;
			new fishname[64];
			new weight, value, slot;
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			if(~lflags & ELicense_Fishing) {
				SendClientMessage(playerid, X11_TOMATO_2, "Sorry, you do not have a fishing license!");
				return 1;
			}
			for(new i=0;i<MAX_HOLDABLE_FISH;i++) {
				GetFishInfoAt(playerid, fishname, sizeof(fishname), i, weight, value);
				if(value != 0) {
					if(slot == listitem) {
						GiveMoneyEx(playerid, value);
						RemoveFishAt(playerid, i);
						format(query, sizeof(query), "You sold your %s for $%s",fishname,getNumberString(value));
						SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
						return 1;
					}
					slot++;
				}
			}
			return 1;
		}
		case EBusiness_RentalMenu: {
			if(!response || listitem < 0) return 0;
			if(listitem >= sizeof(RentableCars)) {
				new caridx = listitem - sizeof(RentableCars);
				DestroyPlayerRentCar(playerid, caridx);
				SendClientMessage(playerid, X11_TOMATO_2, "* You have returned your rented vehicle.");
				return 1;
			}
			new price, model;
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			if(IsAFlyingVehicle(model)) {
				if(~lflags & ELicense_Flying) {
					SendClientMessage(playerid, X11_TOMATO_2, "Sorry, you don't have your flying license!");
					return 1;
				}
			} else {
				if(~lflags & ELicense_Drivers) {
					SendClientMessage(playerid, X11_TOMATO_2, "Sorry, you don't have your drivers license!");
					return 1;
				}
			}
			new biz = GetPVarInt(playerid, "BuyingBiz");
			model = RentableCars[listitem][ECarRental_ModelID];
			price = RentableCars[listitem][ECarRental_HourlyRate];
			if(GetMoneyEx(playerid) < price) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money");
				return 1;
			}
			GiveMoneyEx(playerid, -price);
			addToBusinessTill(biz, price);
			CreatePlayerRentCar(playerid, model, Business[biz][EBusinessExitX],Business[biz][EBusinessExitY],Business[biz][EBusinessExitZ]); 
			DeletePVar(playerid, "BuyingBiz");
		}
	}
	return 1;
}
YCMD:destroylock(playerid, params[], help) {
	if(GetPVarType(playerid, "HoldingLock") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "HoldingLock");
		SendClientMessage(playerid, COLOR_DARKGREEN, "Lock Deleted!");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't even own a lock!");
	}
	return 1;
}
addToBusinessTill(id, amount) {
	if(amount > 5000) amount = 5000;
	query[0] = 0;//[128];
	Business[id][EBusinessTill] += amount;
	format(query,sizeof(query),"UPDATE `business` SET `till` = %d WHERE `id` = %d",Business[id][EBusinessTill],Business[id][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
addToBusinessProducts(id, amount) {
	query[0] = 0;//[128];
	Business[id][EBusinessProducts] += amount;
	format(query,sizeof(query),"UPDATE `business` SET `products` = %d WHERE `id` = %d",Business[id][EBusinessProducts],Business[id][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
public On247ItemBuy(playerid, itemid, biz, total) {
	new flags = GetPVarInt(playerid, "UserFlags");
	if(Buyable24Items[itemid][EBIType] != EType_NumSelect) {
		if(Buyable24Items[itemid][EBIPrice] > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash");
			return 0;
		}
	}
	#define CheckItem(%1,%2) if(!strcmp(Buyable24Items[itemid][EBIDisplayName],%1)) { \
		if(~flags & %2) { \
			flags |= %2; \
			GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]); \
		} else { \
			format(tempstr, sizeof(tempstr),"You already have a %s",Buyable24Items[itemid][EBIDisplayName]); \
			SendClientMessage(playerid, X11_GREEN2, tempstr); \
			return 1;\
		} \
	}
	
	#define CheckItemInt(%1,%2) if(!strcmp(Buyable24Items[itemid][EBIDisplayName],%1)) { \
		if(GetPVarInt(playerid, %2) == 0) { \
			SetPVarInt(playerid, %2, 1); \
			GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]); \
		} else { \
			format(tempstr, sizeof(tempstr),"You already have a %s",Buyable24Items[itemid][EBIDisplayName]); \
			SendClientMessage(playerid, X11_GREEN2, tempstr); \
			return 1;\
		} \
	}
	
	#define CheckNumberedItem(%1,%2)\
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],%1)) { \
		if(floatround(Buyable24Items[itemid][EBIPrice]*total) > GetMoneyEx(playerid)) { \
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash"); \
			return 0; \
		} \
		GiveMoneyEx(playerid, floatround(-Buyable24Items[itemid][EBIPrice]*total)); \
		numitems = GetPVarInt(playerid, %2)+total; \
		SetPVarInt(playerid,%2,numitems); \
	}
	
	CheckItem("GPS",EUFHasGPS)
	CheckItem("Dice",EUFHasDice)
	CheckItem("MP3 Player", EUFHasMP3Player)
	CheckItem("Walkie Talkie",EUFHasWalkieTalkie)
	CheckItem("Phone Book",EUFHasPhoneBook)
	CheckItemInt("Cell Phone", "phone")
	
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Mask")) { 
		if(GetPVarInt(playerid, "ConnectTime") < 125/* || GetPVarInt(playerid, "Level") < 5*/) {
			SendClientMessage(playerid, X11_TOMATO_2, "You need 125 playing hours to use this!");
			return 1;
		}
		if(~flags & EUFHasMask) { 
			flags |= EUFHasMask; 
			GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]); 
		} else { 
			format(tempstr, sizeof(tempstr),"You already have a %s",Buyable24Items[itemid][EBIDisplayName]); 
			SendClientMessage(playerid, X11_GREEN2, tempstr); 
			return 1;
		} 
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Camera")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 9, weapon, ammo);
		if(weapon != 43 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 43, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Shovel")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 1, weapon, ammo);
		if(weapon != 6 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 6, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Flowers")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 10, weapon, ammo);
		if(weapon != 14 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 14, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Pool Cue")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 1, weapon, ammo);
		if(weapon != 7 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 7, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Golf Club")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 1, weapon, ammo);
		if(weapon !=  2 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 2, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Cane")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 10, weapon, ammo);
		if(weapon != 15 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 15, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Spray Can")) {
		new weapon, ammo;
		GetPlayerWeaponDataEx(playerid, 10, weapon, ammo);
		if(weapon != 41 && weapon != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in that slot, you cannot use this.");
			return 1;
		}
		GivePlayerWeaponEx(playerid, 41, -1);
		GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
		return 1;
	}
	if(!strcmp(Buyable24Items[itemid][EBIDisplayName],"Backpack")) {
		if(~flags & EUFHasBackPack) { 
			flags |= EUFHasBackPack; 
			GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]); 
			insertPlayerBackpacks(playerid);
			loadPlayerBackpacks(playerid);
		} else { 
			format(tempstr, sizeof(tempstr),"You already have a %s",Buyable24Items[itemid][EBIDisplayName]); 
			SendClientMessage(playerid, X11_GREEN2, tempstr); 
			return 1;
		}
	}
	
	new numitems;
	if(total != 0) {
		CheckNumberedItem("Vehicle Lock Picks","VehLockpicks")
		CheckNumberedItem("Vehicle Tracer","VehTracers")
		CheckNumberedItem("Cigars","Cigars")
		CheckNumberedItem("Fertilizer","NumFertilizer")
		CheckNumberedItem("Gas Can","GasCans")
		CheckNumberedItem("Matches","Matches")
		CheckNumberedItem("Chemicals","chemicals")
		format(tempstr,sizeof(tempstr),"Congratulations, you now own %s %s",getNumberString(numitems) ,Buyable24Items[itemid][EBIDisplayName]);
	} else {
		format(tempstr,sizeof(tempstr),"Congratulations, you now own a %s",Buyable24Items[itemid][EBIDisplayName]);
	}
	//addToBusinessTill(biz, floatround(Buyable24Items[itemid][EBIPrice]/2));
	addToBusinessTill(biz, floatround(Buyable24Items[itemid][EBIPrice]*3));
	SendClientMessage(playerid, X11_GREEN2, tempstr);
	SetPVarInt(playerid, "UserFlags", flags);
	#undef CheckNumberedItem
	#undef CheckItemInt
	#undef CheckItem
	return 0;
}
YCMD:tracecar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for finding your car");
		return 1;
	}
	new numowned = getNumOwnedCars(playerid);
	if(numowned == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have any cars!");
		return 1;
	}
	new numtracers = GetPVarInt(playerid, "VehTracers");
	if(numtracers < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have a vehicle tracer!");
		return 1;
	}
	new index;
	if(!sscanf(params,"d",index)) {
		if(index < 1 || index > numowned) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have that many cars!");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		new carid = findPlayerCar(playerid, --index);
		if(carid == -1 || GetVehicleVirtualWorld(carid) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Vehicle not found!");
			return 1;
		}
		GetVehiclePos(carid, X, Y, Z);
		SetPlayerCheckpoint(playerid, X, Y, Z, 3.0);
		SendClientMessage(playerid, COLOR_DARKGREEN, "The spot is now marked on your radar");
		HintMessage(playerid, COLOR_DARKGREEN, "Do /killcheckpoint to remove the checkpoint from your radar");
		SetPVarInt(playerid, "VehTracers", numtracers-1);
	} else {
		format(tempstr, sizeof(tempstr), "USAGE: /tracecar [1-%d]",numowned);
		SendClientMessage(playerid, X11_WHITE, tempstr);
	}
	return 1;
}
public OnSexItemBuy(playerid, itemid, biz, total) {
	#define CheckNumberedItem(%1,%2) if(!strcmp(Buyable24Items[itemid][EBIDisplayName],%1)) { \
		if(floatround(Buyable24Items[itemid][EBIPrice]*total) > GetMoneyEx(playerid)) { \
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash"); \
			return 0; \
		} \
		GiveMoneyEx(playerid, floatround(-Buyable24Items[itemid][EBIPrice]*total)); \
		numitems = GetPVarInt(playerid, %2)+total; \
		SetPVarInt(playerid,%2,numitems); \
	}
	new flags = GetPVarInt(playerid, "UserFlags");
	if(BuyableSexItems[itemid][EBIType] != EType_NumSelect) {
		if(BuyableSexItems[itemid][EBIPrice] > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash");
			return 0;
		}
	}
	if(!strcmp(BuyableSexItems[itemid][EBIDisplayName],"Purple Dildo"))
	{
		GivePlayerWeaponEx(playerid, 10, -1);
		GiveMoneyEx(playerid, -BuyableSexItems[itemid][EBIPrice]);
	}
	if(!strcmp(BuyableSexItems[itemid][EBIDisplayName],"Dildo"))
	{
		GivePlayerWeaponEx(playerid, 11, -1);
		GiveMoneyEx(playerid, -BuyableSexItems[itemid][EBIPrice]);
	}
	if(!strcmp(BuyableSexItems[itemid][EBIDisplayName],"Small Vibrator"))
	{
		GivePlayerWeaponEx(playerid, 12, -1);
		GiveMoneyEx(playerid, -BuyableSexItems[itemid][EBIPrice]);
	}
	if(!strcmp(BuyableSexItems[itemid][EBIDisplayName],"Vibrator"))
	{
		GivePlayerWeaponEx(playerid, 13, -1);
		GiveMoneyEx(playerid, -BuyableSexItems[itemid][EBIPrice]);
	}
	addToBusinessTill(biz, floatround(BuyableSexItems[itemid][EBIPrice]/2));
	new numitems;
	if(total != 0) {	
		CheckNumberedItem("Condoms", "condoms")
		format(tempstr,sizeof(tempstr),"Congratulations, you now own %s %s",getNumberString(numitems) ,BuyableSexItems[itemid][EBIDisplayName]);
	} else {
		format(tempstr,sizeof(tempstr),"Congratulations, you now own a %s",BuyableSexItems[itemid][EBIDisplayName]);
	}
	#undef CheckNumberedItem
	SendClientMessage(playerid, X11_GREEN2, tempstr);
	SetPVarInt(playerid, "UserFlags", flags);
	return 0;
}
public Generate247PhoneNumber(playerid, itemid, biz, total) {
	if(GetMoneyEx(playerid) < Buyable24Items[itemid][EBIPrice]) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money for this!");
		return 1;
	}
	GiveMoneyEx(playerid, -Buyable24Items[itemid][EBIPrice]);
	mysql_function_query(g_mysql_handle, "SELECT sub.random_number FROM (SELECT FLOOR(10000 + RAND() * 899999) AS random_number) sub WHERE sub.random_number NOT IN (SELECT `phonenumber` FROM `characters`) LIMIT 1", true, "OnGeneratePhoneNumber", "d",playerid);
	return 1;
}
public OnGeneratePhoneNumber(playerid) {
	new rows,fields,id;
	cache_get_data(rows,fields);
	if(rows > 0) {
		new data[16];
		cache_get_row(0, 0, data);
		id = strval(data);
		format(tempstr, sizeof(tempstr), "Your Phone Number is now: %d",id);
		SetPVarInt(playerid, "PhoneNumber", id);
		SendClientMessage(playerid, X11_WHITE, tempstr);
		format(tempstr,sizeof(tempstr), "UPDATE `characters` SET `phonenumber` = %d WHERE `id` = %d",id,GetPVarInt(playerid,"CharID"));
		mysql_function_query(g_mysql_handle, tempstr, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid, X11_RED3, "Warning: Could not generate phone number, contact an admin!");
	}
	return 0;
}
forward OnRestaurantBuy(playerid, itemid, biz, total);
public OnRestaurantBuy(playerid, itemid, biz, total) {
	if(BuyableRestaurantItems[itemid][EBIType] != EType_NumSelect) {
		if(BuyableRestaurantItems[itemid][EBIPrice] > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash");
			return 0;
		}
	}
	new Float:hp;
	GetPlayerHealth(playerid, hp);
	if(itemid == 0) {
		hp += 25.0;
	} else if(itemid == 1) {
		hp += 35.0;
	} else if(itemid == 2) {
		hp += 55.0;
	}
	format(tempstr, sizeof(tempstr), "* %s buys some %s",GetPlayerNameEx(playerid, ENameType_RPName), BuyableRestaurantItems[itemid][EBIDisplayName]);
	ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	GiveMoneyEx(playerid, -BuyableRestaurantItems[itemid][EBIPrice]);
	addToBusinessTill(biz, floatround(BuyableRestaurantItems[itemid][EBIPrice]/2));
	SetPlayerHealthEx(playerid, hp);
	new	boost = GetHungerLevel(playerid)+50;
	if(boost > 100) boost = 100;
	SetPlayerHunger(playerid, boost);
	return 0;
}
forward OnFishBuy(playerid, itemid, biz, total);
public OnFishBuy(playerid, itemid, biz, total) {
	if(BuyableFishStoreItems[itemid][EBIType] != EType_NumSelect) {
		if(BuyableFishStoreItems[itemid][EBIPrice] > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash");
			return 0;
		}
	}
	new Float:hp;
	GetPlayerHealth(playerid, hp);
	if(itemid == 0) {
		hp += 10.0;
	} else if(itemid == 1) {
		hp += 15.0;
	} else if(itemid == 2) {
		hp += 25.0;
	} else if(itemid == 3) {
		hp += 30.0;
	}
	format(tempstr, sizeof(tempstr), "* %s buys some %s",GetPlayerNameEx(playerid, ENameType_RPName), BuyableFishStoreItems[itemid][EBIDisplayName]);
	ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	GiveMoneyEx(playerid, -BuyableFishStoreItems[itemid][EBIPrice]);
	addToBusinessTill(biz, floatround(BuyableFishStoreItems[itemid][EBIPrice]/2));
	SetPlayerHealthEx(playerid, hp);
	new	boost = GetHungerLevel(playerid)+50;
	if(boost > 100) boost = 100;
	SetPlayerHunger(playerid, boost);
	return 0;
}
forward OnFastFoodBuy(playerid, itemid, biz, total);
public OnFastFoodBuy(playerid, itemid, biz, total) {
	if(BuyableFastfoodItems[itemid][EBIType] != EType_NumSelect) {
		if(BuyableFastfoodItems[itemid][EBIPrice] > GetMoneyEx(playerid)) {
			SendClientMessage(playerid, X11_RED3, "You don't have enough cash");
			return 0;
		}
	}
	new Float:hp;
	GetPlayerHealth(playerid, hp);
	if(itemid == 0) {
		hp += 10.0;
	} else if(itemid == 1) {
		hp += 15.0;
	} else if(itemid == 2) {
		hp += 25.0;
	} else if(itemid == 3) {
		hp += 30.0;
	} else if(itemid == 4) {
		hp += 35.0;
	}
	format(tempstr, sizeof(tempstr), "* %s buys some %s",GetPlayerNameEx(playerid, ENameType_RPName), BuyableFastfoodItems[itemid][EBIDisplayName]);
	ProxMessage(30.0, playerid, tempstr, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	GiveMoneyEx(playerid, -BuyableFastfoodItems[itemid][EBIPrice]);
	addToBusinessTill(biz, floatround(BuyableFastfoodItems[itemid][EBIPrice]/2));
	SetPlayerHealthEx(playerid, hp);
	new	boost = GetHungerLevel(playerid)+50;
	if(boost > 100) boost = 100;
	SetPlayerHunger(playerid, boost);
	return 0;
}
IsDealership(bizid) {
	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_D_NormCar || Business[bizid][EBType] == EBusinessType_D_FastCar || Business[bizid][EBType] == EBusinessType_D_Boat || Business[bizid][EBType] == EBusinessType_D_Bike || Business[bizid][EBType] == EBusinessType_D_Plane || Business[bizid][EBType] == EBusinessType_D_Truck)) {
		return 1;
	}
	return 0;
}
IsCarRental(bizid) {
	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_VehicleRental)) {
		return 1;
	}
	return 0;
}
IsFishStore(bizid) {

	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_FishStore)) {
		return 1;
	}
	return 0;
}
IsFurnitureStore(bizid) {
	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_FurnitureStore)) {
		return 1;
	}
	return 0;
}
IsPaintball(bizid) {
	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_PaintBall)) {
		return 1;
	}
	return 0;
 }
IsDrugFactory(bizid) {
	if(bizid != -1 && (Business[bizid][EBType] == EBusinessType_DrugFactory)) {
		return 1;
	}
	return 0;
}
IsAtBar(playerid) {
	new index = getBusinessInside(playerid);
	if(index != -1 && (Business[index][EBType] == EBusinessType_Bar)) {
		return 1;
	}
	if(IsPlayerInRangeOfPoint(playerid, 15.0,1207.23, -29.32, 1000.95) || IsPlayerInRangeOfPoint(playerid, 15.0,756.68, 1441.08, 1102.70) || IsPlayerInRangeOfPoint(playerid, 5.0,1214.61, -13.33, 1000.92) || IsPlayerInRangeOfPoint(playerid, 5.0,968.49, -46.56, 1001.11) || IsPlayerInRangeOfPoint(playerid, 5.0,1415.65, -1489.09, 20.43))
	{
		return 1;
	}
	else if(IsPlayerInRangeOfPoint(playerid, 15.0,-2658.95, 1410.31, 910.17) || IsPlayerInRangeOfPoint(playerid, 15.0,499.96, -20.35, 1000.67) || IsPlayerInRangeOfPoint(playerid, 5.0,495.55, -76.03, 998.75) || IsPlayerInRangeOfPoint(playerid, 5.0,-224.78, 1404.99, 27.77) || IsPlayerInRangeOfPoint(playerid, 5.0, 238.67, 112.34, 1013.85))
	{
		return 1;
	} else if(IsPlayerInRangeOfPoint(playerid, 15.0,681.12, -453.83, -25.62)) {
		return 1;
	}
	return 0;
}
YCMD:drink(playerid, params[], help) {
	if(!IsAtBar(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't at a bar!");
		return 1;
	}
	dialogstr[0] = 0;
	tempstr[0] = 0;
	strcat(dialogstr, "Drink\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(Drinks);i++) {
		format(tempstr, sizeof(tempstr), "%s\t$%s\n",Drinks[i][EDrinkName],getNumberString(Drinks[i][EDrinkPrice]));
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EBusiness_DrinkMenu, DIALOG_STYLE_TABLIST_HEADERS, "{00BFFF}Drink Menu", dialogstr,"Buy","Cancel");	
	return 1;
}
YCMD:givebiz(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for selling your house");
		return 1;
	}
	new index = getStandingBusiness(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a business");
		return 1;
	}
	if(!isBizAdmin(playerid) && (Business[index][EBusinessOwner] != GetPVarInt(playerid,"CharID") || Business[index][EBusinessOwner] == 0)) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this business.");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is too far away!");
			return 1;
		}
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Business Given");
		SendClientMessage(user, COLOR_DARKGREEN, "* Business Recieved");
		setBusinessOwner(index, GetPVarInt(user, "CharID"));
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givehouse [playerid/name]");
	}
	return 1;
}
forward ShowVehicleLockBuy(playerid, itemid, biz, total);
public ShowVehicleLockBuy(playerid, itemid, biz, total) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	strcat(dialogstr,"Lock Type\tCost\n",sizeof(dialogstr));
	for(new i=0;i<sizeof(BuyableCarLocks);i++) {
		format(tempstr, sizeof(tempstr), "%s\t$%s\n",LockName(BuyableCarLocks[i][EBLockType]),getNumberString(BuyableCarLocks[i][EBLockPrice]));
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EBusiness_LockBuy, DIALOG_STYLE_TABLIST_HEADERS, "{00BFFF}Vehicle Lock Menu", dialogstr,"Buy","Cancel");	
}
forward ShowLotteryTicketBuy(playerid, itemid, biz, total);
public ShowLotteryTicketBuy(playerid, itemid, biz, total) {
	if(getPlayerLottoTicket(playerid) != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You already have a lottery ticket!");
		return 0;
	}
	dialogstr[0] = 0;
	tempstr[0] = 0;
	strcat(dialogstr, "Ticket Type\tCost\n", sizeof(dialogstr));
	strcat(dialogstr, "Random Number assigned\t$250\n", sizeof(dialogstr));
	strcat(dialogstr, "Choose Number\t$350\n", sizeof(dialogstr));
	ShowPlayerDialog(playerid, EBusiness_LottoChoose, DIALOG_STYLE_TABLIST_HEADERS, "{00BFFF}Lotto Menu", dialogstr,"Buy","Cancel");	
	return 1;
}
forward ShowSellFishMenu(playerid, itemid, biz, total);
public ShowSellFishMenu(playerid, itemid, biz, total) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new fishname[64];
	new weight, value;
	for(new slot = 0;slot<MAX_HOLDABLE_FISH;slot++) {
		GetFishInfoAt(playerid, fishname, sizeof(fishname), slot, weight, value);
		if(value != 0) {
			format(tempstr, sizeof(tempstr), "Sell %s - $%s\n",fishname, getNumberString(value));
			strcat(dialogstr, tempstr, sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EBusiness_FishSell, DIALOG_STYLE_LIST, "{00BFFF}Fish Sell Menu", dialogstr,"Sell","Cancel");	
}
setExtortionist(biz, sqlid) {
	format(query, sizeof(query), "UPDATE `business` SET `extortion` = %d WHERE `id` = %d",sqlid,Business[biz][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "onBusinessUpdate", "d",biz);
}
setBusinessName(biz, name[]) {
	new name_esc[(64*2)+1];
	mysql_real_escape_string(name, name_esc);
	format(query, sizeof(query), "UPDATE `business` SET `name` = \"%s\" WHERE `id` = %d",name_esc,Business[biz][EBusinessID]);
	mysql_function_query(g_mysql_handle, query, true, "onBusinessUpdate", "d",biz);
}
getModelValue(model, type = -1) {
	new i;
	if(type == -1 || type == EBusinessType_D_NormCar) {
		for(i=0;i<sizeof(EBDNormalCars);i++) {
			if(EBDNormalCars[i][EBCarModelID] == model) {
				return EBDNormalCars[i][EBCarPrice];
			}
		}
	}
	if(type == -1 || type == EBusinessType_D_FastCar) {
		for(i=0;i<sizeof(EBDFastCars);i++) {
			if(EBDFastCars[i][EBCarModelID] == model) {
				return EBDFastCars[i][EBCarPrice];
			}
		}
	}
	if(type == -1 || type == EBusinessType_D_Bike) {
		for(i=0;i<sizeof(EBDBikes);i++) {
			if(EBDBikes[i][EBCarModelID] == model) {
				return EBDBikes[i][EBCarPrice];
			}
		}
	}
	if(type == -1 || type == EBusinessType_D_Truck) {
		for(i=0;i<sizeof(EBDTrucks);i++) {
			if(EBDTrucks[i][EBCarModelID] == model) {
				return EBDTrucks[i][EBCarPrice];
			}
		}
	}
	if(type == -1 || type ==  EBusinessType_D_Boat) {
		for(i=0;i<sizeof(EBDBoats);i++) {
			if(EBDBoats[i][EBCarModelID] == model) {
				return EBDBoats[i][EBCarPrice];
			}
		}
	}
	if(type == -1 || type == EBusinessType_D_Plane) {
		for(i=0;i<sizeof(EBDPlanes);i++) {
			if(EBDPlanes[i][EBCarModelID] == model) {
				return EBDPlanes[i][EBCarPrice];
			}
		}
	}
	return 0;
}
sellInactiveBusinesses() {
	mysql_function_query(g_mysql_handle, "SELECT `business`.`id` FROM `business` INNER JOIN `characters` ON `business`.`owner` = `characters`.`id`  INNER JOIN `accounts` ON `characters`.`accountid` = `accounts`.`id` WHERE `business`.`owner` != -1 AND `accounts`.`lastlogin` < DATE_SUB(CURDATE(),INTERVAL 2 WEEK)", true, "OnCheckInactiveBusinesses", "");
	//mysql_function_query(g_mysql_handle, "SELECT `h`.`id` FROM `business`  AS `h` LEFT JOIN `characters` AS `c` ON `c`.`id` = `h`.`owner` WHERE `c`.`username` IS NULL AND `h`.`owner` != -1", true, "OnCheckInactiveBusinesses", "");
	//Why were two queries written here?
}
forward OnCheckInactiveBusinesses();
public OnCheckInactiveBusinesses() {
	new rows, fields;
	new id, id_string[128];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		id = strval(id_string);
		new hid = findBizBySQLID(id);
		if(hid != -1) {
			setBusinessOwner(hid, -1);
		}
	}
}
findBizBySQLID(sqlid) {
	for(new i=0;i<MAX_BUSINESS;i++) {
		if(Business[i][EBusinessID] == sqlid) {
			return i;
		}
	}
	return -1;
}

GetTotalBusinessValue(playerid) {
	new charid = GetPVarInt(playerid,"CharID");
	new value;
	for(new i=0;i<MAX_BUSINESS;i++) {
		if(Business[i][EBusinessOwner] == charid) {
			value += Business[i][EBusinessPrice];
			value += Business[i][EBusinessTill];
		}
	}
	return value;
}

GetRentModelPrice(model) {
	for(new i=0;i<sizeof(RentableCars);i++) {
		if(RentableCars[i][ECarRental_ModelID] == model) {
			return RentableCars[i][ECarRental_HourlyRate];
		}
	}
	return 0;
}