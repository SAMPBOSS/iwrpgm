enum {
	EHelpDialog_Main = EHelp_Base + 1,
}
enum EHelpOptions {
	EHelpName[32],
	EHelpCallback[32],
}

enum EScriptUpdateInfo {
	EScriptUpdateText[128],
	EScriptUpdateType, //0 = player, 1 = admin, 2 = helper
}

new UpdateTypes[][] = {"Client Update", "Admin Update", "Helper Update", "Server Update"};

new ScriptUpdates[][EScriptUpdateInfo] = {
	/*
	{"Added \"/blockpm [playerid]\"",0},
	{"Some dialogs are now tabbed.",0},
	{"\"/drink\" now works in bar businesses.",0}
	*/
	{"Security fixes", 0},
	{"Revised and re-written some timers.", 3},
	{"Re-written the entire vehicle GUI. It's now simpler.", 0}
};

new HelpOptions[][EHelpOptions] = {{"Animations","AnimHelp"},
									{"Bank Commands","BankHelp"},
									{"Business Commands","BusinessHelp"},
									//{"Illegal Business Commands","IBusinessHelp"},
									{"Families","FamilyHelp"},
									{"Fishing","FishHelp"},
									{"General Commands","GeneralCmdHelp"},
									{"Housing Commands","HousingHelp"},
									{"Job Help","JobHelp"},
									{"Colour Codes","ColourHelp"},
									{"License Commands","LicenseHelp"},	
									{"Phone Commands","PhoneHelp"},	
									//{"Turf Commands","TurfHelp"},
									{"Drugs","DrugHelp"},
									{"Radio Station","RadioStationHelp"},
									{"Safe Commands","SafeHelp"},
									{"Vehicle Commands","OwnedCarHelp"},
									{"Vehicle Locks","CarLockHelp"},
									{"Storage Commands","StorageHelp"},
									{"FAQ","FAQQuestions"}};
									
enum ERandomTipInfo {
	ERandomTipName[128],
}
									
new RandomTips[][ERandomTipInfo] = {
	//{"You can use your locker at East L.A to store items such as guns, materials, and money"},
	{"You can get your driving license at City Hall"},
	//{"If you join a family you can participate in Turf wars and win items!"},
	{"You can buy a lotto ticket at any 24/7"},
	{"You can get a cell phone at any 24/7"},
	{"Do /settings to turn off these hints, and more!"},
	{"Check out our forums and website at: http://www.IW-RP.com"}
};
helpOnGameModeInit() {
	//CreateDynamicPickup(1239,16,1799.25, -1695.66, 13.53);
	//CreateDynamic3DTextLabel("Job Spot Moved!\n(( /jobspots ))", 0x2BB00AA, 1799.25, -1695.66, 13.53+1, 50.0);
}
YCMD:help(playerid, params[], help) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(HelpOptions);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",HelpOptions[i][EHelpName]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EHelpDialog_Main, DIALOG_STYLE_LIST, "{00BFFF}Help Menu", dialogstr, "Ok", "Cancel");
	return 1;
}
helpOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	if(!response) {
		return 0;
	}
	switch(dialogid) {
		case EHelpDialog_Main: {
			CallLocalFunction(HelpOptions[listitem][EHelpCallback],"d", playerid);
		}
	}
	return 1;
}
showHelpText(playerid, msg[]) {
	if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHints) {
		ShowScriptMessage(playerid, msg);
	}
}
HintMessage(playerid, color, msg[]) {
	#pragma unused color
	if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHints) {
		if(GetPVarInt(playerid, "ConnectTime") < 50) {
			SendClientMessage(playerid, color, msg);
			//showHelpText(playerid, msg);
		}
	}
	return 0;
}
MassHintMessage(color, msg[]) {
	#pragma unused color
	foreach(Player, i) {
		if(IsPlayerConnectEx(i))
			HintMessage(i, color, msg);
			//showHelpText(i, msg);
	}
}
YCMD:updates(playerid, params[], help) {
	new msg[64];
	format(msg, sizeof(msg), "{00BFFF}%s Updates",VERSION);

	dialogstr[0] = 0;
	tempstr[0] = 0;

	for(new i=0;i<sizeof(ScriptUpdates);i++) {
		new colour = COLOR_CUSTOMGOLD;
		if(ScriptUpdates[i][EScriptUpdateType] == 1) {
			colour = COLOR_RED;
			if(GetPVarInt(playerid, "AdminFlags") == 0)
				continue;
		}
		if(ScriptUpdates[i][EScriptUpdateType] == 2) {
			colour = COLOR_CUSTOMGOLD;
			if(GetPVarInt(playerid, "NewbieRank") == 0)
				continue;
		}
		format(tempstr,sizeof(tempstr), "{%s}%s{%s}: %s\n",getColourString(colour),UpdateTypes[ScriptUpdates[i][EScriptUpdateType]],getColourString(X11_WHITE),ScriptUpdates[i][EScriptUpdateText]);
		strcat(dialogstr,tempstr);
	}
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, msg, dialogstr, "Ok", "");
	return 1;
}
showBusinessHelp(playerid, EBusinessType:type) {
	new msg[128];
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHints) {
		return 1;
	}
	switch(type) {
		case EBusinessType_SexShop, EBusinessType_247, EBusinessType_GasStation:
		{
			format(msg, sizeof(msg), "Type ~r~/buy~w~ to buy ~r~things.");
		}
		case EBusinessType_PizzaStack: {
			format(msg, sizeof(msg), "Do ~r~/buy~w~ to buy ~r~food!.");
		}
		case EBusinessType_Bar: {
			format(msg, sizeof(msg), "Do ~r~/drink~w~ to buy a ~r~drink.");
		}
		case EBusinessType_FurnitureStore: {
			format(msg, sizeof(msg), "Do ~r~/buytokens~w~ to buy ~r~furniture tokens.");
		}
		case EBusinessType_DrugFactory: {
			format(msg, sizeof(msg), "Do ~r~/buy~w~ to buy a ~r~drug.");
		}
		default: {
			return 0;
		}
	}
	//HintMessage(playerid, COLOR_CUSTOMGOLD, msg);
	ShowScriptMessage(playerid, msg);
	return 0;
}
forward GeneralCmdHelp(playerid);
public GeneralCmdHelp(playerid) {
	//SendClientMessage(playerid, COLOR_DARKGREEN, "Generic Help");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Generic Help ***");
	SendClientMessage(playerid, COLOR_GRAD1,"/(W)hisper - Whisper something to someone near you.");
	SendClientMessage(playerid, COLOR_GRAD1,"/(S)hout - Shout something, and people can hear you from a larger distance.");
	SendClientMessage(playerid, COLOR_GRAD2,"/B - Talking OOC (Out Of Character) with someone near you.");
	SendClientMessage(playerid, COLOR_GRAD2,"/Ad - Display an advertisement that everyone sees, leaving your name and number.");
	SendClientMessage(playerid, COLOR_GRAD3,"/helpme - Ask a question in newbie chat.");
	SendClientMessage(playerid, COLOR_GRAD3,"/(O)oc - Talking OOC (Out Of Character) with everyone on the server.");
	SendClientMessage(playerid, COLOR_GRAD4,"/(L)ocal - Talking IC normally.");
	SendClientMessage(playerid, COLOR_GRAD4,"/(C)lose - Talking IC to people nearby or in the same vehicle as you.");
}
forward PhoneHelp(playerid);
public PhoneHelp(playerid) {
	//SendClientMessage(playerid, COLOR_RED,"Phone Help");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Phone Help ***");
	SendClientMessage(playerid, COLOR_GRAD1,"The phone is used to call people and text. Here are the commands:");
	SendClientMessage(playerid, COLOR_GRAD1,"/(P)ickup - Answers an incoming call.");
	SendClientMessage(playerid, COLOR_GRAD2,"/(H)angup - Hangs up on an active call.");
	SendClientMessage(playerid, COLOR_GRAD2,"/Call - Used to call someone");
	SendClientMessage(playerid, COLOR_GRAD3,"/Sms - Used to send a text message");
	//SendClientMessage(playerid, COLOR_GRAD1,"/Phone - Used to call, sms or look up contacts.");
	SendClientMessage(playerid, COLOR_GRAD3,"/TogPhone - Changes your phone mode.");
}
forward LicenseHelp(playerid);
public LicenseHelp(playerid) {
	//SendClientMessage(playerid, COLOR_RED,"Licenses");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Licenses ***");
	SendClientMessage(playerid, COLOR_GRAD1,"If you don't have any licenses you're probably doing something illegal.");
	SendClientMessage(playerid, COLOR_GRAD1,"Go to the City hall's right wing to check out the DMV.");
	SendClientMessage(playerid, COLOR_GRAD1,"There you can buy any licenses you might need for your job.");
	SendClientMessage(playerid, COLOR_GRAD1,"If you want to drive high performance vehicles, you require a permit and then to apply for the license (process takes 2 weeks).");
	//SendClientMessage(playerid, COLOR_GRAD1,"For a fake drivers license you can use '/Makefakelicense'.");
}
forward BankHelp(playerid);
public BankHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Bank ***");
	SendClientMessage(playerid, COLOR_GRAD1,"The bank is a safe place for your money.");
	SendClientMessage(playerid, COLOR_GRAD1,"You can withdraw and bank your money there and also /Wiretransfer to other people's accounts.");
    SendClientMessage(playerid, COLOR_GRAD1,"For things like rentals or bills you need to have money in your bank to pay them.");
	SendClientMessage(playerid, COLOR_GRAD1,"The commands are /Withdraw, /Wiretransfer, /Bank, /Balance.");
	SendClientMessage(playerid, COLOR_GRAD1,"Note: You can't deposit cash at ATMs.");
}
forward FAQQuestions(playerid);
public FAQQuestions(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Frequently asked questions ***");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: How do I become a Cop?  {FFFF00}A: Check the 'Los Angeles Police Department' section in the forum.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: How do I change my password?  {FFFF00}A: /Changepass");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: How do I buy a house or business?  {FFFF00}A: Look for green house icons that say 'For sale'.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: How do I buy a car?  {FFFF00}A: Look for car dealerships, there is one near Jeffersons Motel.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: Where can I get a job?  {FFFF00}A: For the legal jobs go to the city hall marked with a pink 'C'.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: How do I join a family?  {FFFF00}A: Find the location of a gang member and RP with them.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: Where is the bank?  {FFFF00}A: Look for $ icons on the map. There are one bank in LS, but a lot of ATMs.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: What do the name colors mean?  {FFFF00}A: Check out /Legend. It explains everything.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: What do I get for donating?  {FFFF00}A: You get DonationPoints(DP) to spend in our VIP mall.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: Where can I ask more questions?  {FFFF00}A: You may ask in (/helpme and of course in the forum.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: I saw someone hack. What should I do?  {FFFF00}A: Stay away from them. Do not pick up any hacked items or money. Report it right away.");
	SendClientMessage(playerid,COLOR_GRAD1,"Q: I'm not sure if my mod is legal here. What do I do?  {FFFF00}A: Advantage giving mods are banned. If you are unsure, ask on the forum.");
}
forward RadioStationHelp(playerid);
public RadioStationHelp(playerid) {
	//SendClientMessage(playerid,COLOR_RED,"Radio station");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Radio station ***");
	SendClientMessage(playerid,COLOR_GRAD1,"To use the custom radio track visit");
	SendClientMessage(playerid,COLOR_GRAD1,"http://kiwi6.com/, create an account, upload your songs");
	SendClientMessage(playerid,COLOR_GRAD1,"and use the link you get in game.");
	//SendClientMessage(playerid,COLOR_GRAD1,"/ibizinfo - Display information on your business");
	//SendClientMessage(playerid,COLOR_GRAD1,"/ibizname - Update your business name");
	SendClientMessage(playerid,COLOR_GRAD1,"/trunk open - Players around the vehicle will hear the music.");
	SendClientMessage(playerid,COLOR_GRAD1,"/radiostation [1 - 19] - Radio station inside your vehicle.");
	SendClientMessage(playerid,COLOR_GRAD1,"/radiostation [0] - turn off the radio.");
	SendClientMessage(playerid,COLOR_GRAD1,"/customradiotrack - Custom radio inside the vehicle.");
	//SendClientMessage(playerid,COLOR_GRAD1,"/ibizwithdraw - Withdraw money from your business till");
}
forward DrugHelp(playerid);
public DrugHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Drug Help ***");
	SendClientMessage(playerid,COLOR_GRAD1,"/businessdrugs - Allows you to view the drugs you've created at your house or business.");
	SendClientMessage(playerid,COLOR_GRAD1,"/usedrug <part of drug name> - Can be used to take a specific drug for example \"/usedrug coca\".");
	SendClientMessage(playerid,COLOR_GRAD1,"/drugtest [playerid] - Can be used by LEO's to drug test someone.");
	SendClientMessage(playerid,COLOR_GRAD1,"/give - Allows you to give another player some of your drugs.");
	SendClientMessage(playerid, COLOR_GRAD1, "");
	SendClientMessage(playerid, COLOR_GRAD1, "");
	SendClientMessage(playerid, X11_WHITE, "* General");
	SendClientMessage(playerid, COLOR_GRAD1, "Drugs have side effects that can be set upon the creation of them, all creations are final and can't be edited after.");
	SendClientMessage(playerid, COLOR_GRAD1, "Drugs cannot be deleted once they're created, you could however run out of stock for that drug.");
	SendClientMessage(playerid, COLOR_GRAD1, "Smoking a drug inside a vehicle whilst having the windows up, will cause the other passengers to get high.");
	SendClientMessage(playerid, COLOR_GRAD1, "Taking way too many drugs will cause you to overdose and probably die from it unless you get medical treatment.");
	SendClientMessage(playerid, COLOR_GRAD1, "To withdraw drugs from your business or house use /businessdrugs -> View Pr.. -> Drug Name.");
	SendClientMessage(playerid, COLOR_GRAD1, "To produce more drugs of an already existing type you need chemicals which can be bought at any 24/7.");
}
/*
forward TurfHelp(playerid);
public TurfHelp(playerid) {
	SendClientMessage(playerid,COLOR_RED,"Turf Wars");
	SendClientMessage(playerid,COLOR_BRIGHTRED,"Make sure you read '/TurfRules'!");
	SendClientMessage(playerid,COLOR_GRAD1,"There are many turfs around town for families to take over.");
	SendClientMessage(playerid,COLOR_GRAD1,"For a family to take over turfs they need a family color and a safe.");
	SendClientMessage(playerid,COLOR_GRAD1,"When your family owns a turf, it gives a payout into the safe every payday.");
	SendClientMessage(playerid,COLOR_GRAD1,"Commands:");
	SendClientMessage(playerid,COLOR_GRAD1,"/TakeOver - Start a turf-war.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Turfs - List all turfs");
	SendClientMessage(playerid,COLOR_GRAD1,"/TurfScore - List the turf scores.");
	SendClientMessage(playerid,COLOR_GRAD1,"/TurfPeople - Show and find other players in the turf-war.");
	SendClientMessage(playerid,COLOR_GRAD1,"/GiveTurf - Give a turf to another family.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Turfs - List all turfs");
}
*/
forward CarLockHelp(playerid);
public CarLockHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Car Locks ***");
	SendClientMessage(playerid, X11_WHITE, "{FFFF00}Default lock: {FFFFFF}Can't be purchased is in the car by default. Not very safe and is very easy to pick.");
	SendClientMessage(playerid, X11_WHITE, "{FFFF00}Simple lock: {FFFFFF}Safer than the default lock.");
	SendClientMessage(playerid, X11_WHITE, "{FFFF00}Remote lock: {FFFFFF}Includes an alarm and allows you to use the lock from far away.");
	SendClientMessage(playerid, X11_WHITE, "{FFFFFF}If you want to pick locks you use '/Pickcar'. The level of success depends on your lock pick skill and the cars lock quality.");
}
forward BusinessHelp(playerid);
public BusinessHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Business ***");
	SendClientMessage(playerid,COLOR_GRAD1,"You can make money with businesses. By selling your inventory.");
	SendClientMessage(playerid,COLOR_GRAD1,"/BizInfo - Shows Info of your Business");
	SendClientMessage(playerid,COLOR_GRAD1,"/BizFee - Sets the Fee of your Business(Depends on Type)");
	SendClientMessage(playerid,COLOR_GRAD1,"/BizName - Sets the Name of your Business(RP names only!)");
	SendClientMessage(playerid,COLOR_GRAD1,"/Extortion - Sets who extorts your Business");
	SendClientMessage(playerid,COLOR_GRAD1,"/SellBiz /BuyBiz - Sells/Buys a business(1 at a time)");
	SendClientMessage(playerid,COLOR_GRAD1,"/Open - Opens/Closes your business");
	SendClientMessage(playerid,COLOR_GRAD1,"/BizDeposit /BizWithdraw - Same as Bank CMDS, but for your business till");
}
forward StorageHelp(playerid);
public StorageHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Storage and Storage Areas ***");
	SendClientMessage(playerid,COLOR_GRAD1,"Storage areas can be used to store your items, storage areas can be locked.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Buystoragearea confirm - Buys a storage area");
	SendClientMessage(playerid,COLOR_GRAD1,"/Sellstorage - Sells the storage area");
	SendClientMessage(playerid,COLOR_GRAD1,"/Edititem - Can be used to set an item for sale or edit its position.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Grabitem - Can be used to grab an item with your mouse.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Drop - Used to drop an item");
	SendClientMessage(playerid,COLOR_GRAD1,"/Pickupitem - Used to pickup an item you're standing on.");
	/*
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Added storage areas. Commands are: \"/buystoragearea confirm\", \"/sellstorage\", \"/storagedoor\".");
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Added \"/grabitem\" to grab items off walls or other that can't be reached while standing.");
	*/
}
/*
forward IBusinessHelp(playerid);
public IBusinessHelp(playerid) {
	SendClientMessage(playerid,COLOR_RED,"Illegal Business");
	SendClientMessage(playerid,COLOR_GRAD1,"A player, or family, can start an illegal business");
	SendClientMessage(playerid,COLOR_GRAD1,"Your business must be created by an admin");
	SendClientMessage(playerid,COLOR_GRAD1,"It must be in a hidden, back alley, or somewhere else not out in the open");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizopen - Close or open your business");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizinfo - Display information on your business");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizname - Update your business name");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizstore - Store materials or drugs in your business vault");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibiztake - Take materials or drugs from your business vault");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizprofit - Set the profit offset for your items");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizdeposit - Deposit money in your business till");
	SendClientMessage(playerid,COLOR_GRAD1,"/ibizwithdraw - Withdraw money from your business till");
}
*/
forward HousingHelp(playerid);
public HousingHelp(playerid) {
	//SendClientMessage(playerid, COLOR_RED,"Housing");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Housing ***");
	SendClientMessage(playerid, COLOR_GRAD1,"Find a House that you like and buy it!");
	SendClientMessage(playerid, COLOR_GRAD1,"Not enough money? Rent one then. Check for the 'For Rent'.");
	SendClientMessage(playerid, COLOR_GRAD1, "/BuyHouse - Buy the House");
	SendClientMessage(playerid, COLOR_GRAD1, "/SellHouse - Sells your House");
	SendClientMessage(playerid, COLOR_GRAD1, "/GiveHouse - Give it to another Player");
	SendClientMessage(playerid, COLOR_GRAD1, "/SetRent - Adjust the price it costs to rent it.");
	//SendClientMessage(playerid, COLOR_GRAD1, "/ForRent - Makes people able to rent it.");
	SendClientMessage(playerid, COLOR_GRAD1, "/Rent - Rent the House");
	SendClientMessage(playerid, COLOR_GRAD1, "/Door - Shuts/Opens the House door.");
	//SendClientMessage(playerid, COLOR_GRAD1, "/Evict - Kicks the Renter out.");
	//SendClientMessage(playerid, COLOR_GRAD1, "/Knock - Knock on the front door.");
	//SendClientMessage(playerid, COLOR_GRAD1, "/DoorTalk - Talk to the person outside/inside through the door.");
	SendClientMessage(playerid, COLOR_GRAD1, "/HouseLights - Turns the lights on and off.");
	SendClientMessage(playerid, COLOR_GRAD1, "/HRadio - Controls the radio in the house.");
	//SendClientMessage(playerid, COLOR_GRAD1, "/HSafe - Use the house safe");
	SendClientMessage(playerid, COLOR_GRAD1, "/BuyHouseSafe - Buy a house safe - See Safe Help for more information");
	SendClientMessage(playerid, COLOR_GRAD1, "/Renters - List people renting from your house");
	SendClientMessage(playerid, COLOR_GRAD1, "/Evict - Kicks the Renter out.");
	
	
	SendClientMessage(playerid, COLOR_GRAD1, "");
	SendClientMessage(playerid, COLOR_GRAD1, "");
	SendClientMessage(playerid, X11_WHITE, "* House Furniture");
	SendClientMessage(playerid, COLOR_GRAD1, "We allow you to place house furniture inside, and around your house");
	SendClientMessage(playerid, COLOR_GRAD1, "Do NOT place your furniture off your property, or in NON-RP places");
	SendClientMessage(playerid, COLOR_GRAD1, ", or it will be removed, and you could be banned from placing furniture!");
	SendClientMessage(playerid, COLOR_GRAD1, "/buyfurniture - Buy furniture for your house");
	SendClientMessage(playerid, COLOR_GRAD1, "/editfurniture - Move your house furniture");
	SendClientMessage(playerid, COLOR_GRAD1, "/deletefurniture - Delete your house furniture");
	
	
}
forward FamilyHelp(playerid);
public FamilyHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Family Help ***");
	SendClientMessage(playerid, COLOR_GRAD1,"You can create Families in-game with '/createfamily'");
	SendClientMessage(playerid, COLOR_GRAD1,"After Family creation you need to '/adjust spawn' to set your HQ.");
	SendClientMessage(playerid, X11_ORANGE,"You need atleast 5 members to create a family.");
	SendClientMessage(playerid, COLOR_GRAD1,"/Findvan - Capture the Van.");
	//SendClientMessage(playerid, COLOR_GRAD1,"/Takeover - Take over turfs.");
	//SendClientMessage(playerid, COLOR_GRAD1,"/Givepoint - Give a point to another family.");
	//SendClientMessage(playerid, COLOR_GRAD1,"/Giveturf - Give a turf to another family.");
	SendClientMessage(playerid, COLOR_GRAD1,"/Invite /uninvite - invite/kick a player to your family.");
	SendClientMessage(playerid, COLOR_GRAD1,"/Quitfamily - Quits your current family.");
	SendClientMessage(playerid, COLOR_GRAD1,"/(F)amily - OOC Family Chat");
	SendClientMessage(playerid, COLOR_GRAD1,"/Adjust - Adjust family stats.");
	SendClientMessage(playerid, COLOR_GRAD1,"/GiveRank [id] [1-6] - promotes/demotes a family member.");
	SendClientMessage(playerid, COLOR_GRAD1,"/BuyFamilySafe - Buy a family Safe.");
	SendClientMessage(playerid, COLOR_GRAD1,"/fSafe - Access your families safe.");
	SendClientMessage(playerid, COLOR_GRAD1,"/BuyFamilyHQ - Buys a family HQ.");
	SendClientMessage(playerid, COLOR_GRAD1,"/MakeFamilyCar - Changes an owned vehicle into a family owned vehicle.");
	SendClientMessage(playerid, COLOR_GRAD1,"/ParkFamilyCar - Save the position of the family vehicle");
	SendClientMessage(playerid, COLOR_GRAD1,"/BuyFamilyColor - Buys a family color.");
	SendClientMessage(playerid, COLOR_GRAD1,"/MoveSafe - Move the family safe");
	SendClientMessage(playerid, COLOR_GRAD1,"/TagWall - Tag a wall");
	
	SendClientMessage(playerid, COLOR_GRAD1,"For more Info visit the forum: www.IW-RP.com/forum");
}
forward OwnedCarHelp(playerid);
public OwnedCarHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Personal Vehicles ***");
	SendClientMessage(playerid,COLOR_GRAD1,"You can buy up to 6 vehicles at dealerships.");
	SendClientMessage(playerid,COLOR_GRAD1,"These vehicles will always be where you left them or where someone else left them.");
	SendClientMessage(playerid,COLOR_GRAD1,"/Storecar - Stores your car in another dimension");
	SendClientMessage(playerid,COLOR_GRAD1,"/Lock /Unlock - Locks/unlocks the vehicle.");
	SendClientMessage(playerid,COLOR_GRAD1,"/SellCar - Gives the vehicle to another player. (/Accept car)");
	//SendClientMessage(playerid,COLOR_GRAD1,"/GiveCarKeys - Give a set of keys to another person (max of 10)");
	//SendClientMessage(playerid,COLOR_GRAD1,"/ResetCarKeys - Change the locks on your car.");
	SendClientMessage(playerid, COLOR_GRAD1,"/MyCars - Shows a list of your vehicles.");
	SendClientMessage(playerid, COLOR_GRAD1,"/TraceCar - Shows the Position of one of your vehicles.");
	SendClientMessage(playerid, COLOR_GRAD1,"/Hood, /Trunk, /Lights - Control the different things of your vehicle.");
	SendClientMessage(playerid, COLOR_GRAD1,"/Cartoys - Edit your vehicles car toys");
}
forward AnimHelp(playerid);
public AnimHelp(playerid) {
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE,"*** Animations ***");
	SendClientMessage(playerid, COLOR_GRAD1,"There are a lot of animations to choose from.");
	SendClientMessage(playerid, COLOR_GRAD1,"For a big list of animations type /animlist.");
	SendClientMessage(playerid, COLOR_GRAD1,"Animations support your RP and always look better than just regular /me's.");
	SendClientMessage(playerid, COLOR_GRAD1,"Want to look like a boss who crosses arms then type '/an crossarms'.");
	SendClientMessage(playerid, COLOR_GRAD1,"If you want to aim at someone then try doing '/an robman'.");
}
forward FishHelp(playerid);
public FishHelp(playerid) {
	//SendClientMessage(playerid, COLOR_RED, "Fishing Help");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, X11_WHITE, "*** Fishing ***");
	SendClientMessage(playerid, COLOR_GRAD1,"To be able to fish you need to buy a fish license.");
	SendClientMessage(playerid, COLOR_GRAD1,"You can buy a fishing license for City Hall.");
	SendClientMessage(playerid, COLOR_GRAD1,"You also need to buy a fishing rod, You can buy one from Binco.");
	SendClientMessage(playerid, COLOR_GRAD1,"You can sell fish at the fish store, or eat them.");
	SendClientMessage(playerid, COLOR_GRAD1,"FISH *** /fish /myfish /dropfish /eatfish");
}
forward JobHelp(playerid);
public JobHelp(playerid) {
	new job = GetPVarInt(playerid, "Job");
	switch(job) {
		case EJobType_TrashCleaner: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
			SendClientMessage(playerid,X11_WHITE,"*** Trash Cleaner ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /startclean");
			SendClientMessage(playerid,COLOR_GRAD2,"The Trash Cleaner cleans the Streets of garbage by following");
			SendClientMessage(playerid,COLOR_GRAD3,"the marked route.");
		}
		case EJobType_Drug_Dealer: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
			SendClientMessage(playerid,X11_WHITE,"*** Drug Dealer ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /give /harvest /plant /drugs /pickupspots");
			SendClientMessage(playerid,COLOR_GRAD2,"The Drug-Dealer sells drugs to people.");
			SendClientMessage(playerid,COLOR_GRAD3,"You need to find the drug spots, that are marked on the map with a 'D'.");
			SendClientMessage(playerid,COLOR_GRAD4,"The higher your drug dealing skill, the more drugs you can run.");
		}
		case EJobType_Arms_Dealer: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
			SendClientMessage(playerid,X11_WHITE,"*** Arms Dealer ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** (/Mat)erials /PickupSpots /craftweapon");
			SendClientMessage(playerid,COLOR_GRAD2,"The Arms Dealer sells weapons to people but to make weapons,");
			SendClientMessage(playerid,COLOR_GRAD3,"he needs Materials. Material spots are marked with a 'MC'");
			SendClientMessage(playerid,COLOR_GRAD4,"To find material spots, ask other arms dealers.");
		}
		case EJobType_Mechanic: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
			SendClientMessage(playerid,X11_WHITE,"*** Mechanic ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /Repair /Refill /TuneCar /TuneColors /TunePaintjob /Tow");
			SendClientMessage(playerid,COLOR_GRAD2,"The mechanic maintains all vehicles around.");
			SendClientMessage(playerid,COLOR_GRAD3,"The more you learn, the more kind of actions you can do.");
			SendClientMessage(playerid,COLOR_GRAD4,"The HQ is located in northern LS, under the highway bridges");
		}
		case EJobType_Mercenary: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
            SendClientMessage(playerid,X11_WHITE,"*** Mercenary ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /tie /find /untie /trunkplayer /untrunkplayer");
			SendClientMessage(playerid,COLOR_GRAD2,"A mercenary makes his money by bodyguarding people and");
			SendClientMessage(playerid,COLOR_GRAD3,"he can also find people anywhere in SA(Skill Depending)");
		}
		case EJobType_TaxiDriver: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** Taxi Driver ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /Fare");
			SendClientMessage(playerid,COLOR_GRAD2,"The Taxi Driver drives around town and gives people lifts.");
			SendClientMessage(playerid,COLOR_GRAD3,"Part of your income will go towards the taxi business.");
		}
		case EJobType_Lawyer: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** Lawyer ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /free /tinfoil");
			SendClientMessage(playerid,COLOR_YELLOW,"A lawyer can make money by hiding people from the authorities and freeing them.");
		}
		case EJobType_Crafter: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** Crafter ***");
			SendClientMessage(playerid,COLOR_GRAD1,"JOB *** /craftitem /giveitem /holditem /destroyitem");
			SendClientMessage(playerid,COLOR_YELLOW,"A crafter can make special items, for legal, or illegal purposes.");
		}
		case EJobType_PizzaMan: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
			SendClientMessage(playerid,X11_WHITE,"*** Pizza Man ***");
			SendClientMessage(playerid,COLOR_YELLOW,"Just get on a bike and deliver the pizzas to the houses.");
		}
	}
	new faction = GetPVarInt(playerid, "Faction");
	switch(getFactionType(faction)) {
		case EFactionType_LEO: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** LEO commands ***");
			SendClientMessage(playerid,COLOR_GRAD1,"Detain *** /(un)cuff /tazer /detain /arrest /frisk /take /removespikestrip (/rss) /impound");
			SendClientMessage(playerid,COLOR_GRAD2,"Misc *** /placeobject /destroyobject /modifyobject /watchcam /mdc ");
			SendClientMessage(playerid,COLOR_GRAD3,"Misc *** /coplights /clearbackpack");
			SendClientMessage(playerid,COLOR_GRAD4,"Detective *** /pickevidence /viewevidence /viewevidenceonplayer");
			
		}
		case EFactionType_SanNews: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** News commands ***");
			SendClientMessage(playerid,COLOR_GRAD1,"Misc *** /live /news /putonair");
		}
		case EFactionType_EMS: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** Medic commands ***");
			SendClientMessage(playerid,COLOR_GRAD1,"Misc *** /duty /revive /heal /megaphone");
		}
		case EFactionType_Government: {
			SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
		    SendClientMessage(playerid,X11_WHITE,"*** Government commands ***");
			SendClientMessage(playerid,COLOR_GRAD1,"Misc *** /duty /equip /gov /taser /megaphone");
		}
	}
}

YCMD:legend(playerid, params[], help)
{
	SendClientMessage(playerid, X11_ORANGE, " ");
	SendClientMessage(playerid, X11_ORANGE, "|__IWRP Name-Color Legend__|");
    SendClientMessage(playerid, X11_BLACK, "Black: Not logged in");
	SendClientMessage(playerid, X11_WHITE, "White: Normal players");
	SendClientMessage(playerid, COLOR_RED, "Red: Moderator on duty");
	SendClientMessage(playerid, COLOR_BRIGHTRED, "Bright Red: Admin on duty");
	SendClientMessage(playerid, TEAM_BLUE_COLOR, "Blue: LEO's");
	
	SendClientMessage(playerid, X11_ORANGE, " ");
	return 1;
}

forward ColourHelp(playerid);
public ColourHelp(playerid) {
	//SendClientMessage(playerid,COLOR_RED,"Colour Codes");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid,X11_WHITE,"***Colour Codes ***");
	SendClientMessage(playerid,COLOR_GRAD1,"We allow for colour substitution in some commands and dialogs.");
	SendClientMessage(playerid,COLOR_GRAD1,"To use this you simply write /cN in the text, where N is the corresponding colour ID");
	SendClientMessage(playerid,COLOR_GRAD1,"Do /colourcodes to see all available colours.");
	SendClientMessage(playerid,COLOR_GRAD1,"There is also character codes");
	SendClientMessage(playerid,COLOR_GRAD1,"Do /charcodes to see all available character codes.");
	SendClientMessage(playerid,COLOR_GRAD1,"This feature is currently only usable in /ad and vehicle plates.");
	SendClientMessage(playerid,COLOR_GRAD1,"Use /colourtest to test this formatting system");
	return 1;
}
forward SafeHelp(playerid);
public SafeHelp(playerid) {
	//SendClientMessage(playerid,COLOR_RED,"Safe Help");
	SendClientMessage(playerid, COLOR_GREEN,"_______________________________________");
	SendClientMessage(playerid, COLOR_GRAD1,"/buyfamilysafe - Buy a safe for your family HQ");
	SendClientMessage(playerid, COLOR_GRAD1,"/buyhousesafae - Buy a safe for your house");
	SendClientMessage(playerid, COLOR_GRAD1,"/fsafe - Storing / taking out items from your family safe.");
	SendClientMessage(playerid, COLOR_GRAD1,"/hsafe - Storing / taking out items from your house safe.");
	SendClientMessage(playerid, COLOR_GRAD1,"/deletesafe - Delete a safe and all items inside it");
	SendClientMessage(playerid, COLOR_GRAD1,"/upgradesafe - Upgrade your safe");
	SendClientMessage(playerid, COLOR_GRAD1,"/movesafe - Move your safe.");
	return 1;
}
task TipTimer[900000]() {
	new randtext = RandomEx(0,sizeof(RandomTips));
	MassHintMessage(COLOR_LIGHTCYAN, RandomTips[randtext][ERandomTipName]);
}