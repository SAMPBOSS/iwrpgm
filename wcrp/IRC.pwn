forward OnIRCCreate(owner);

YCMD:makeirc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Join/Part IRC channels");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		query[0] = 0;
		format(query, sizeof(query), "INSERT INTO `irc` (`owner`) VALUES (%d)",GetPVarInt(target, "CharID"));
		mysql_function_query(g_mysql_handle, query, true, "OnIRCCreate", "d",target);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makeirc [owner]");
	}
	return 1;
}
YCMD:irc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "List/Join/Part IRC channels");
		return 1;
	}
	new type[32],channel,pass[32];
	if(!sscanf(params, "s[32]D(-1)S()[32]", type, channel, pass)) {
		if(!strcmp(type, "status", true)) {
			sendIRCStatus(playerid);
		} else if(!strcmp(type, "join", true)) {
			if(GetPVarInt(playerid, "IRCChan") == 0) {
				tryIRCJoin(playerid, channel, pass);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "* You are in a channel, part the channel first");
			}
		} else if(!strcmp(type, "part", true) || !strcmp(type, "leave", true)) {
			if(GetPVarInt(playerid, "IRCChan") != 0) {
				removeFromIRC(playerid, 0);
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "* You aren't in a channel");
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /irc [mode]");
		SendClientMessage(playerid, X11_WHITE, "Modes: Join, Part, Status");
	}
	return 1;
}
YCMD:i(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends an IRC Message");
		return 1;
	}
	new smsg[128],msg[64];
	if(!sscanf(params, "s[64]", msg)) {
		format(smsg, sizeof(smsg), "* (( %s : %s )) *", GetPlayerNameEx(playerid, ENameType_CharName), msg);
		if(GetPVarInt(playerid, "IRCChan") == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "* You must be in an IRC Channel!");
			return 1;
		}
		sendIRCMessage(GetPVarInt(playerid, "IRCChan"), X11_YELLOW, smsg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /i [message]");
	}
	return 1;
}
YCMD:makeircadmin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone an IRC Admin");
		return 1;
	}
	new chan, user, rank;
	new msg[128];
	if(!sscanf(params, "ddD(2)", chan, user, rank)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPVarInt(user, "IRCChan", chan);
		SetPVarInt(user, "IRCRank", rank);
		format(msg, sizeof(msg), "* %s was assigned IRC Admin rank %d",GetPlayerNameEx(user, ENameType_CharName), rank);
		sendIRCMessage(chan, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makeircadmin [chan] [user] (rank)");
	}
	return 1;
}
YCMD:removeircadmin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Makes someone an IRC Admin");
		return 1;
	}
	new chan, user, rank;
	new msg[128];
	if(!sscanf(params, "dk<playerLookup_acc>", chan, user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPVarInt(user, "IRCChan", chan);
		SetPVarInt(user, "IRCRank", 0);
		format(msg, sizeof(msg), "* %s's privileges have been revoked",GetPlayerNameEx(user, ENameType_CharName), rank);
		sendIRCMessage(chan, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /removeircadmin [chan] [user]");
	}
	return 1;
}
YCMD:makeircowner(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set someone as the channel owner");
		return 1;
	}
	new chan, user, msg[128];
	if(!sscanf(params, "dk<playerLookup_acc>", chan, user)) {
		format(query, sizeof(query), "UPDATE `irc` SET `owner` = %d WHERE `id` = %d",GetPVarInt(user, "CharID"), chan);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		format(msg, sizeof(msg), "* %s has been made channel owner!",GetPlayerNameEx(user, ENameType_CharName));
		sendIRCMessage(chan, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makeircowner [chan] [playerid/name]");
	}
	return 1;
}
YCMD:airc(playerid, params[], help) {
	new rank = GetPVarInt(playerid, "IRCRank");
	new chan = GetPVarInt(playerid, "IRCChan");
	if(chan == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a channel!");
		return 1;
	}
	if(rank == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be an admin");
		return 1;
	}
	new type[32],extra[32],extra2[32];
	new msg[128];
	if(!sscanf(params, "s[32]S()[32]S()[32]", type, extra, extra2)) {
		if(!strcmp(type, "kick", true)) {
			new user = ReturnUser(extra, playerid);
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			if(GetPVarInt(user,"IRCChan") != GetPVarInt(playerid, "IRCChan")) {
				SendClientMessage(playerid, X11_TOMATO_2, "This person is not in your channel!");
				return 1;
			}
			removeFromIRC(user, 1);
		} else if(!strcmp(type, "password", true)) {
			setIRCPassword(GetPVarInt(playerid, "IRCChan"), extra);
		} else if(!strcmp(type, "list", true)) {
			foreach(Player, i) {
				if(IsPlayerConnectEx(i)) {
					format(msg, sizeof(msg), "* %s (%d)",GetPlayerNameEx(i, ENameType_CharName), GetPVarInt(i, "IRCRank"));
					SendClientMessage(playerid, X11_YELLOW, msg);
				}
			}
		} else if(!strcmp(type, "rank", true)) {
			if(strlen(extra2) < 1 || strlen(extra) < 1) {
				SendClientMessage(playerid, X11_WHITE, "USAGE: /airc rank [userid] [rankid]");
				return 1;
			}
			new rankid = strval(extra2);
			new user = ReturnUser(extra, playerid);
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			new userrank = GetPVarInt(user, "IRCRank");
			if(userrank >= rank || rankid >= userrank) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot put someone at an equal or higher rank than yourself!");
				return 1;
			}
			SetPVarInt(user,"IRCRank",rankid);
			format(msg, sizeof(msg), "* %s promoted %s to a level %d IRC Admin",GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(user, ENameType_CharName), rankid);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /airc [mode]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Modes: password, lock, kick, list, rank");
	}
	return 1;
}
setIRCPassword(channel, pass[]) {
	query[0] = 0;
	new pass_esc[32];
	mysql_real_escape_string(pass, pass_esc);
	format(query, sizeof(query), "UPDATE `IRC` SET `password` = \"%s\" WHERE `id` = %d", pass_esc, channel);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 1;
}
removeFromIRC(playerid, reason) {
	new sreason[32];
	switch(reason) {
		case 0: {
			strmid(sreason, "Parted", 0, 6, sizeof(sreason));
		}
		case 1: {
			strmid(sreason, "Kicked", 0, 6, sizeof(sreason));
		}
	}
	new msg[128];
	new channel = GetPVarInt(playerid, "IRCChan");
	format(msg, sizeof(msg), "* %s has left the channel (%s)",GetPlayerNameEx(playerid, ENameType_CharName), sreason);
	sendIRCMessage(channel, COLOR_DARKGREEN, msg);
	SetPVarInt(playerid, "IRCChan", 0);
	return 1;
}
public OnIRCCreate(owner) {
	new id = mysql_insert_id();
	new msg[128];
	format(msg, sizeof(msg), "* IRC SQL ID: %d",id);
	ABroadcast(X11_YELLOW, msg, EAdminFlags_IRCAdmin);
	return 1;
}
tryIRCJoin(playerid, channel, pass[]) {
	new pass_esc[(32*2)+1];
	mysql_real_escape_string(pass, pass_esc);
	format(query, sizeof(query), "select `motd`,`c`.`id`,`a`.`id` from `irc` LEFT JOIN `characters` AS `c` ON `irc`.`owner` = `c`.`id` LEFT JOIN `accounts` AS `a` ON `c`.`accountid` = `a`.`id` WHERE `irc`.`id` = %d AND `irc`.`password` = \"%s\"",channel,pass_esc);
	mysql_function_query(g_mysql_handle, query, true, "OnIRCTryJoin", "dd", playerid,channel);
}
forward OnIRCTryJoin(playerid,chan);
public OnIRCTryJoin(playerid,chan) {
	new rows, fields;
	new motd[64];
	new isadmin = 0;
	cache_get_data(rows, fields);
	if(rows > 0) {
		new msg[128];
		cache_get_row(0, 1, motd);
		if(strval(motd) == GetPVarInt(playerid, "CharID")) {
			isadmin = 1;
		}
		cache_get_row(0, 2, motd);
		if(strval(motd) == GetPVarInt(playerid, "AccountID")) {
			isadmin = 1;
		}
		cache_get_row(0, 0, motd);
		SetPVarInt(playerid, "IRCChan", chan);
		format(msg, sizeof(msg), "* %s has joined the channel!",GetPlayerNameEx(playerid, ENameType_CharName));
		sendIRCMessage(chan, COLOR_DARKGREEN, msg);
		if(isadmin) {
			SetPVarInt(playerid, "IRCRank", 2);
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Channel Owner Status Granted!");
		}
		format(msg, sizeof(msg), "* IRC MOTD: %s", motd);
		sendIRCMessage(chan, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* IRC Join failed - Invalid channel ID or password!");
	}
}
sendIRCStatus(playerid) {
	mysql_function_query(g_mysql_handle, "SELECT `characters`.`username`,`motd`,`locked`,`password`,`irc`.`id` FROM `irc` INNER JOIN `characters` ON `characters`.`id` = `irc`.`owner` ORDER BY `irc`.`id` ASC", true, "onLoadIRCStatus", "d", playerid);
}
forward onLoadIRCStatus(playerid);
public onLoadIRCStatus(playerid) {
	new msg[128],owner[MAX_PLAYER_NAME],motd[64],locked[10],password[32],id[10];
	new rows, fields;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, owner);
		cache_get_row(i, 1, motd);
		cache_get_row(i, 2, locked);
		cache_get_row(i, 3, password);
		cache_get_row(i, 4, id);
		format(msg, sizeof(msg), "* Channel ID: %s Owner: %s, Locked: %s Password: %d",id,owner,locked,strlen(password)>0?1:0);
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
	return 1;
}
sendIRCMessage(channel, color, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "IRCChan") == channel) {
				SendClientMessage(i, color, msg);
			}
		}
	}
	SendBigEarsMessage(color, msg);
}
YCMD:destroyirc(playerid, params[], help) {
	new id;
	if(!sscanf(params, "d", id)) {
		destroyIRCChannel(id);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /destroyirc [channel]");
	}
	return 1;
}
destroyIRCChannel(id) {
	format(query, sizeof(query), "DELETE FROM `irc` WHERE `id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query), "UPDATE `characters` SET `irc` = 0 WHERE `irc` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	foreach(Player, i) {
		if(GetPVarInt(i, "IRCChan") == id) {
			SetPVarInt(i, "IRCChan", 0);
			SendClientMessage(i, COLOR_LIGHTBLUE, "Your channel has been deleted by an admin!");
		}
	}
}