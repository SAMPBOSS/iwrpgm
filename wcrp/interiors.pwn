#define MAX_INTERIORS_LEN 32 //varchar length in mysql too

#define MAX_INTERIORS 32
enum EInteriorInfo {
	ESQLID,
	EInteriorName[32],
	Float:EEntranceInteriorX,
	Float:EEntranceInteriorY,
	Float:EEntranceInteriorZ,
	Float:EInteriorX,
	Float:EInteriorY,
	Float:EInteriorZ,
	Float:EInteriorAngle,
	EInteriorVW,
	EInteriorInt,
	EInteriorIconID,
	EInteriorMapID,
	EInteriorPickupID,
	EInteriorExitPickupID,
	EInteriorPickupModel,
	Text3D:EInteriorEntranceText,
	Text3D:EInteriorExitText,
	EInteriorFreeze
};

forward OnLoadInteriors();
forward loadInteriors();
new Interiors[MAX_INTERIORS][EInteriorInfo];
interiorsOnGameModeInit() {
	loadInteriors();
	loadInteriorAltCommands();
}
public loadInteriors() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`,`name`,`ex`,`ey`,`ez`,`x`,`y`,`z`,`vw`,`interior`,`iconid`,`model`,`angle`,`freeze` FROM `interiors` WHERE `enabled` = 1");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadInteriors", "");
}
public OnLoadInteriors() {
	new id_string[32];
	new rows, fields;
	cache_get_data(rows,fields);
	for(new i=0;i<rows&&i<sizeof(Interiors);i++) {
	
		cache_get_row(i, 0, id_string);
		Interiors[i][ESQLID] = strval(id_string);
		
		cache_get_row(i, 1, Interiors[i][EInteriorName]);
		
		cache_get_row(i, 2, id_string);
		Interiors[i][EEntranceInteriorX] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		Interiors[i][EEntranceInteriorY] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		Interiors[i][EEntranceInteriorZ] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		Interiors[i][EInteriorX] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		Interiors[i][EInteriorY] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		Interiors[i][EInteriorZ] = floatstr(id_string);
		
		cache_get_row(i, 8, id_string);
		Interiors[i][EInteriorVW] = strval(id_string);
		
		cache_get_row(i, 9, id_string);
		Interiors[i][EInteriorInt] = strval(id_string);
		
		cache_get_row(i, 10, id_string);
		Interiors[i][EInteriorIconID] = strval(id_string);
		
		cache_get_row(i, 11, id_string);
		Interiors[i][EInteriorPickupModel] = strval(id_string);
		
		cache_get_row(i, 12, id_string);
		Interiors[i][EInteriorAngle] = floatstr(id_string);
		
		cache_get_row(i, 13, id_string);
		Interiors[i][EInteriorFreeze] = strval(id_string);
		
		if(Interiors[i][EInteriorIconID] != -1) {
		
			new mapid = allocMapID();		
			Interiors[i][EInteriorMapID] = mapid;
		}
		new msg[128];
		Interiors[i][EInteriorPickupID] =  CreateDynamicPickup(Interiors[i][EInteriorPickupModel], 16, Interiors[i][EEntranceInteriorX], Interiors[i][EEntranceInteriorY], Interiors[i][EEntranceInteriorZ]);
		if(strlen(Interiors[i][EInteriorName]) > 0)
		//worldid = -1, interiorid = -1,
			Interiors[i][EInteriorExitPickupID] =  CreateDynamicPickup(Interiors[i][EInteriorPickupModel], 16, Interiors[i][EInteriorX], Interiors[i][EInteriorY], Interiors[i][EInteriorZ],Interiors[i][EInteriorVW]);
		format(msg, sizeof(msg), "%s\n(( Press F to exit. ))",Interiors[i][EInteriorName]);
		if(strlen(Interiors[i][EInteriorName]) > 0)
			Interiors[i][EInteriorExitText] = CreateDynamic3DTextLabel(msg, 0x2BFF00AA, Interiors[i][EInteriorX], Interiors[i][EInteriorY], Interiors[i][EInteriorZ]+1.5, 25.0,INVALID_PLAYER_ID, INVALID_VEHICLE_ID,0,Interiors[i][EInteriorVW]);
		format(msg, sizeof(msg), "%s\n(( Press F to enter. ))",Interiors[i][EInteriorName]);
		if(strlen(Interiors[i][EInteriorName]) > 0)
			Interiors[i][EInteriorEntranceText] = CreateDynamic3DTextLabel(msg, 0x2BFF00AA, Interiors[i][EEntranceInteriorX], Interiors[i][EEntranceInteriorY], Interiors[i][EEntranceInteriorZ]+1.5, 25.0);
		
	}
}
loadInteriorAltCommands() {
	Command_AddAltNamed( "enter", "exit");
}
YCMD:enter(playerid, params[], help) { //This is used for exiting as well
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to enter or exit interiors.");
	}
	new time = GetPVarInt(playerid, "EnterExitCooldown");
	new timenow = gettime();
	if(ENTEREXIT_COOLDOWN-(timenow-time) > 0) {
		return 1;
	}
	businessTryEnter(playerid);
	interiorTryEnterExit(playerid);
	familiesTryEnterExit(playerid);
	housesTryEnterExit(playerid);
	updateSpectators(playerid);
	factionTryEnterExit(playerid);
	apartmentTryEnterExit(playerid);
	joiningOrLeavingBBall(playerid); //basketball
	warehouseTryEnter(playerid);
	storageareaTryEnterExit(playerid);
	SetPVarInt(playerid, "EnterExitCooldown", gettime());
	return 1;
}
YCMD:reloadinteriors(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Reloads the Interiors from SQL");
	}
	unloadInteriors();
	loadInteriors();
	SendClientMessage(playerid, COLOR_DARKGREEN, "[Notice]: Interiors reloaded");
	return 1;
}
YCMD:interiorentrance(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Marks an interior");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	SetPVarFloat(playerid, "InteriorEntranceX", X);
	SetPVarFloat(playerid, "InteriorEntranceY", Y);
	SetPVarFloat(playerid, "InteriorEntranceZ", Z);
	SendClientMessage(playerid, COLOR_DARKGREEN, "[NOTICE]: Interior entrance marked");
	return 1;
}
YCMD:interiorexit(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Marks an interior exit");
		return 1;
	}
	new Float:X, Float:Y,Float:Z, Float:A,vw,interior;
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	vw = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);
	SetPVarFloat(playerid, "InteriorExitX",X);
	SetPVarFloat(playerid, "InteriorExitY",Y);
	SetPVarFloat(playerid, "InteriorExitZ",Z);
	SetPVarFloat(playerid, "InteriorExitAngle",A);
	SetPVarInt(playerid, "InteriorVW", vw);
	SetPVarInt(playerid, "InteriorInt", interior);
	SendClientMessage(playerid, COLOR_DARKGREEN, "[NOTICE]: Interior exit marked");
	return 1;
}
YCMD:interiorname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set an interiors name before creating it");
		return 1;
	}
	if(strlen(params) < 1) {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /interiorname [name]");
		return 1;
	}
	SetPVarString(playerid, "InteriorName", params);
	return 1;
}
YCMD:makeinterior(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Creates and loads an interior");
		return 1;
	}
	new name[((32*2)+1)];
	new model, iconid;
	if(!sscanf(params, "I(1239)I(-1)", model, iconid)) {
	query[0] = 0;
	GetPVarString(playerid, "InteriorName", name, 32);
	mysql_real_escape_string(name, name);
	format(query, sizeof(query), "INSERT INTO `interiors` (`name`,`ex`,`ey`,`ez`,`x`,`y`,`z`,`vw`,`interior`,`iconid`,`model`) VALUES (\"%s\",%f,%f,%f,%f,%f,%f,%d,%d,%d,%d)",
	name,GetPVarFloat(playerid, "InteriorEntranceX"),GetPVarFloat(playerid, "InteriorEntranceY"),GetPVarFloat(playerid, "InteriorEntranceZ"),GetPVarFloat(playerid, "InteriorExitX"),GetPVarFloat(playerid, "InteriorExitY"),GetPVarFloat(playerid, "InteriorExitZ"),GetPVarInt(playerid, "InteriorVW"),GetPVarInt(playerid, "InteriorInt"),iconid,model);
	unloadInteriors();
	mysql_function_query(g_mysql_handle, query, true, "loadInteriors", "");
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /makeinterior [modelid] [iconid]");
	}
	return 1;
}
removePlayersMapID(mapid) {
	foreach(Player, i) {
		if(IsPlayerConnected(i)) {
			RemovePlayerMapIcon(i, mapid);
		}
	}
}
unloadInteriors() {
	new i;
	for(i=0;i<sizeof(Interiors);i++) {
		if(Interiors[i][ESQLID] != 0) {
			Interiors[i][ESQLID] = 0;
			DestroyDynamicPickup(Interiors[i][EInteriorPickupID]);		
			DestroyDynamicPickup(Interiors[i][EInteriorExitPickupID]);		
			if(Interiors[i][EInteriorIconID] != -1) {
				freeMapID(Interiors[i][EInteriorMapID]);
				removePlayersMapID(Interiors[i][EInteriorMapID]);
			}
			DestroyDynamic3DTextLabel(Interiors[i][EInteriorExitText]);
			DestroyDynamic3DTextLabel(Interiors[i][EInteriorEntranceText]);
		}
	}
}
interiorsOnPlayerConnect(playerid) {
	loadMaps(playerid);
}
loadMaps(playerid) {
	for(new i=0;i<sizeof(Interiors);i++) {
		if(Interiors[i][ESQLID] != 0) {
			if(Interiors[i][EInteriorIconID] != -1) {
				SetPlayerMapIcon(playerid, Interiors[i][EInteriorMapID],Interiors[i][EEntranceInteriorX],Interiors[i][EEntranceInteriorY],Interiors[i][EEntranceInteriorZ],Interiors[i][EInteriorIconID],0, MAPICON_LOCAL);
			}
		}
	}
}
DynamicStreamerIntCheck(playerid) {
	SetTimerEx("onDynamicStreamerIntCheck",250,false,"d",playerid);
}
forward onDynamicStreamerIntCheck(playerid);
public onDynamicStreamerIntCheck(playerid) {
	if(GetPlayerInterior(playerid) != 0) {
		if(!isSpectating(playerid)) {
			if(!isPlayerFrozen(playerid)) {
				if(!isAtLoadInteriorPlace(playerid)) {
					if(isCloseToDynamicObject(playerid)) {
						WaitForObjectsToStream(playerid);
					}
				}
			}
		}
	}
	return 1;
}
isAtLoadInteriorPlace(playerid) {
	for(new i=0;i<sizeof(Interiors);i++) {
		if(Interiors[i][ESQLID] != 0) {
			if(IsPlayerInRangeOfPoint(playerid, 10.0, Interiors[i][EInteriorX],Interiors[i][EInteriorY],Interiors[i][EInteriorZ])) {
				return 1;
			}
		}
	}
	return 0;
}
interiorTryEnterExit(playerid, Float:radi = 3.0) {
	for(new i=0;i<sizeof(Interiors);i++) {
		if(Interiors[i][ESQLID] != 0) {
			if(IsPlayerInRangeOfPoint(playerid, radi, Interiors[i][EEntranceInteriorX],Interiors[i][EEntranceInteriorY],Interiors[i][EEntranceInteriorZ])) {
				if(Interiors[i][EInteriorFreeze]) {
					showHelpText(playerid, "You are being frozen to wait for the objects to stream.");
					TogglePlayerControllableEx(playerid, 0);
					SetTimerEx("SetControllable",2500,false,"dd",playerid,1);
				}
				SetPlayerPos(playerid, Interiors[i][EInteriorX], Interiors[i][EInteriorY], Interiors[i][EInteriorZ]);
				SetPlayerInterior(playerid, Interiors[i][EInteriorInt]);
				SetPlayerVirtualWorld(playerid, Interiors[i][EInteriorVW]);
				if(Interiors[i][EInteriorAngle] != 0.0){
					SetPlayerFacingAngle(playerid, Interiors[i][EInteriorAngle]);
				}
			} else if(IsPlayerInRangeOfPoint(playerid, radi, Interiors[i][EInteriorX], Interiors[i][EInteriorY], Interiors[i][EInteriorZ])) {
				if(GetPlayerVirtualWorld(playerid) == Interiors[i][EInteriorVW]) {
					SetPlayerPos(playerid, Interiors[i][EEntranceInteriorX],Interiors[i][EEntranceInteriorY],Interiors[i][EEntranceInteriorZ]);
					SetPlayerVirtualWorld(playerid, 0);
					SetPlayerInterior(playerid, 0);
				}
			}
		}
	}
	return 1;
}
tryMakePlayerEnterOrExit(playerid, Float:radi = 200.0) { //By default the radius is 200 because this is meant to be for things like the hospital
	interiorTryEnterExit(playerid, radi);
	return 1;
}