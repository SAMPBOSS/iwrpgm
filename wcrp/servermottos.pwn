#define MAX_MOTTOS 100

enum ESpecialMottoTypes {
	MottoType_Normal, //0 
	MottoType_Weather, //1
}
enum ESVMottoInfo {
	EMottoSQLID,
	ESVMottoName[128],
	ESpecialMottoTypes:ESVMottoType,
};

new ServerMottos[MAX_MOTTOS][ESVMottoInfo];

onServerMottoGameModeInit() {
	loadServerMottos();
}

loadServerMottos() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`,`message`,`type` FROM `servermottos`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadServerMottos", "");
}
forward OnLoadServerMottos();
public OnLoadServerMottos() {
	new rows, fields;
	new id_string[128];
	//new msg[128];
	new type;
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		if(ServerMottos[i][EMottoSQLID] != 0) continue; //Also used for reloading the mottos
		cache_get_row(i, 0, id_string);
		ServerMottos[i][EMottoSQLID] = strval(id_string);
		
		cache_get_row(i, 1, ServerMottos[i][ESVMottoName]);
		
		cache_get_row(i, 2, id_string);
		type = strval(id_string);
		
		ServerMottos[i][ESVMottoType] = ESpecialMottoTypes:type;
		/*
		#if debug
		if(ServerMottos[i][ESVMottoType] == MottoType_Normal) {
			printf(msg, sizeof(msg), "hostname %s", ServerMottos[i][ESVMottoName]);	
		} else {	
			printf(msg, sizeof(msg), "hostname %s", getSpecialMottoText(ServerMottos[i][ESVMottoName], _:MottoType_Weather));
		}
		#endif
		*/
		
	}
	return 1;
}

findRandServerMottoSQLID() {
	new maxamount;
	for(new i=0;i<sizeof(ServerMottos);i++) {
		if(ServerMottos[i][EMottoSQLID]) {
			maxamount++;
		}
	}
	if(maxamount != 0) {
		return RandomEx(0, maxamount);
	} else {
		return -1;
	}
}

changeHostNameText() {
	if(svnamechangeallowed == 1) {
		new index = findRandServerMottoSQLID();
		if(index == -1) return 0;
		new msg[128];
		if(servermaintenance == 0) {
			if(server_doublebonus != 0) {
				format(msg, sizeof(msg), "hostname Inglewood RP | Double Respect And Paycheck Bonus!");
			} else {
				if(ServerMottos[index][ESVMottoType] == MottoType_Normal) {
					format(msg, sizeof(msg), "hostname %s", ServerMottos[index][ESVMottoName]);
				} else {	
					format(msg, sizeof(msg), "hostname %s", getSpecialMottoText(ServerMottos[index][ESVMottoName], _:MottoType_Weather));
				}
			}
		} else {
			format(msg, sizeof(msg), "hostname Inglewood RP | Under Maintenance!");
		}
		if(index != -1) {
			SendRconCommand(msg);
		}
	}
	return 1;
}

stock getSpecialMottoText(text[],type) {
	new string[128];
	switch(type) {
		case MottoType_Weather: {
			new Year, Month, Day;
			getdate(Year, Month, Day);
			new index = getActualSeason(Month);
			format(string, sizeof(string),text,Seasons[index][E_SeasonName],servtemperature);
		}
	}
	return string;
}

task SwitchServerMottos[1000] () {
	changeHostNameText();
}