#define CHEAT_TIMER 500
#define MAX_HACK_WARNS 2
#define AC_SPAWN_COOLDOWN 5
#define MAX_HH_ARMS 3
#define AC_CHECK_COOLDOWN 1
#define MAX_ILLEGAL_CAR_MOD_WARNS 5
#define AFKHACK_WARN_TIME 15
#define SWITCHCHAR_WAIT_TIME 15
#define MAX_AFK_TIME 600
#define MAX_VEH_ENTRIES_PER_SEC 3
#define CARJACK_CHECK_COOLDOWN 2



#if chc-debug
#define ANTICHEAT_DBG 0
#elseif west-debug
#define ANTICHEAT_DBG 0
#else
#define ANTICHEAT_DBG 0
#endif

/*
	PVars used by this:
	GunScan0-11(int) - Weapon in slot
	GunSync(int)
*/
new LoginSync[MAX_PLAYERS]; //number of seconds to ignore sending messages
new GunScan[MAX_PLAYERS][13][2];
new GunSync[MAX_PLAYERS];
new HackWarns[MAX_PLAYERS];
new ACLastCheck[MAX_PLAYERS];
new LastAmmo[MAX_PLAYERS];
new numVehEntries[MAX_PLAYERS][2];//cleared every 1 second, if over 3 in 1 second ban
new currentCar[MAX_PLAYERS][3]; //ID of current car from onplayerentervehicle
new HealthHackWarns[MAX_PLAYERS][2];
new IllegalModWarns[MAX_PLAYERS];
new SyncWarns[MAX_PLAYERS];
new LastSwitchChar[MAX_PLAYERS];
new acdisabled;
new Float:TelePos[MAX_PLAYERS][6];
new playerFrozen[MAX_PLAYERS];
new Float:PlayerArmour[MAX_PLAYERS];
new LastUpdate[MAX_PLAYERS];
new PlayerActionInfo[MAX_PLAYERS][2];
new LastCarJackTime[MAX_PLAYERS];

forward SetPlayerHealthEx(playerid, Float:health);
forward SetPlayerArmourEx(playerid, Float:health);
forward GetMoneyEx(playerid);
forward GiveMoneyEx(playerid, money);
forward GivePlayerWeaponEx(playerid, gun, ammo);
forward GetPlayerWeaponEx(playerid);
forward GetWeaponSlot(weapon);
forward RemovePlayerWeapon(playerid, weaponid);
forward IsPlayerHoldingWeaponInSlot(playerid, slot);

public ResetPlayerWeaponsEx(playerid);


   new legalmods[48][22] = {
        {400, 1024,1021,1020,1019,1018,1013,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {401, 1145,1144,1143,1142,1020,1019,1017,1013,1007,1006,1005,1004,1003,1001,0000,0000,0000,0000},
        {404, 1021,1020,1019,1017,1016,1013,1007,1002,1000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {405, 1023,1021,1020,1019,1018,1014,1001,1000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {410, 1024,1023,1021,1020,1019,1017,1013,1007,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000},
        {415, 1023,1019,1018,1017,1007,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {418, 1021,1020,1016,1006,1002,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {420, 1021,1019,1005,1004,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {421, 1023,1021,1020,1019,1018,1016,1014,1000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {422, 1021,1020,1019,1017,1013,1007,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {426, 1021,1019,1006,1005,1004,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {436, 1022,1021,1020,1019,1017,1013,1007,1006,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000},
        {439, 1145,1144,1143,1142,1023,1017,1013,1007,1003,1001,0000,0000,0000,0000,0000,0000,0000,0000},
        {477, 1021,1020,1019,1018,1017,1007,1006,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {478, 1024,1022,1021,1020,1013,1012,1005,1004,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {489, 1024,1020,1019,1018,1016,1013,1006,1005,1004,1002,1000,0000,0000,0000,0000,0000,0000,0000},
        {491, 1145,1144,1143,1142,1023,1021,1020,1019,1018,1017,1014,1007,1003,0000,0000,0000,0000,0000},
        {492, 1016,1006,1005,1004,1000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {496, 1143,1142,1023,1020,1019,1017,1011,1007,1006,1003,1002,1001,0000,0000,0000,0000,0000,0000},
        {500, 1024,1021,1020,1019,1013,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {516, 1021,1020,1019,1018,1017,1016,1015,1007,1004,1002,1000,0000,0000,0000,0000,0000,0000,0000},
        {517, 1145,1144,1143,1142,1023,1020,1019,1018,1017,1016,1007,1003,1002,0000,0000,0000,0000,0000},
        {518, 1145,1144,1143,1142,1023,1020,1018,1017,1013,1007,1006,1005,1003,1001,0000,0000,0000,0000},
        {527, 1021,1020,1018,1017,1015,1014,1007,1001,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {529, 1023,1020,1019,1018,1017,1012,1011,1007,1006,1003,1001,0000,0000,0000,0000,0000,0000,0000},
        {534, 1185,1180,1179,1178,1127,1126,1125,1124,1123,1122,1106,1101,1100,0000,0000,0000,0000,0000},
        {535, 1121,1120,1119,1118,1117,1116,1115,1114,1113,1110,1109,0000,0000,0000,0000,0000,0000,0000},
        {536, 1184,1183,1182,1181,1128,1108,1107,1105,1104,1103,0000,0000,0000,0000,0000,0000,0000,0000},
        {540, 1145,1144,1143,1142,1024,1023,1020,1019,1018,1017,1007,1006,1004,1001,0000,0000,0000,0000},
        {542, 1145,1144,1021,1020,1019,1018,1015,1014,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {546, 1145,1144,1143,1142,1024,1023,1019,1018,1017,1007,1006,1004,1002,1001,0000,0000,0000,0000},
        {547, 1143,1142,1021,1020,1019,1018,1016,1003,1000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {549, 1145,1144,1143,1142,1023,1020,1019,1018,1017,1012,1011,1007,1003,1001,0000,0000,0000,0000},
        {550, 1145,1144,1143,1142,1023,1020,1019,1018,1006,1005,1004,1003,1001,0000,0000,0000,0000,0000},
        {551, 1023,1021,1020,1019,1018,1016,1006,1005,1003,1002,0000,0000,0000,0000,0000,0000,0000,0000},
        {558, 1168,1167,1166,1165,1164,1163,1095,1094,1093,1092,1091,1090,1089,1088,0000,0000,0000,0000},
        {559, 1173,1162,1161,1160,1159,1158,1072,1071,1070,1069,1068,1067,1066,1065,0000,0000,0000,0000},
        {560, 1170,1169,1141,1140,1139,1138,1033,1032,1031,1030,1029,1028,1027,1026,0000,0000,0000,0000},
        {561, 1157,1156,1155,1154,1064,1063,1062,1061,1060,1059,1058,1057,1056,1055,1031,1030,1027,1026},
        {562, 1172,1171,1149,1148,1147,1146,1041,1040,1039,1038,1037,1036,1035,1034,0000,0000,0000,0000},
        {565, 1153,1152,1151,1150,1054,1053,1052,1051,1050,1049,1048,1047,1046,1045,0000,0000,0000,0000},
        {567, 1189,1188,1187,1186,1133,1132,1131,1130,1129,1102,0000,0000,0000,0000,0000,0000,0000,0000},
        {575, 1177,1176,1175,1174,1099,1044,1043,1042,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {576, 1193,1192,1191,1190,1137,1136,1135,1134,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {580, 1023,1020,1018,1017,1007,1006,1001,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {589, 1145,1144,1024,1020,1018,1017,1016,1013,1007,1006,1005,1004,1000,0000,0000,0000,0000,0000},
        {600, 1022,1020,1018,1017,1013,1007,1006,1005,1004,0000,0000,0000,0000,0000,0000,0000,0000,0000},
        {603, 1145,1144,1143,1142,1024,1023,1020,1019,1018,1017,1007,1006,1001,0000,0000,0000,0000,0000}
    };

iswheelmodel(modelid) {
    new wheelmodels[17] = {1025,1073,1074,1075,1076,1077,1078,1079,1080,1081,1082,1083,1084,1085,1096,1097,1098};
    for(new wm; wm < sizeof(wheelmodels); wm++) {
        if (modelid == wheelmodels[wm])
            return true;
    }
    return false;
}

IllegalCarNitroIde(carmodel) {
    new illegalvehs[29] = { 581, 523, 462, 521, 463, 522, 461, 448, 468, 586, 509, 481, 510, 472, 473, 493, 595, 484, 430, 453, 452, 446, 454, 590, 569, 537, 538, 570, 449 };
    for(new iv; iv < sizeof(illegalvehs); iv++) {
        if (carmodel == illegalvehs[iv])
            return true;
    }
    return false;
}

stock islegalcarmod(vehicleide, componentid) {
    new modok = false;
    if ( (iswheelmodel(componentid)) || (componentid == 1086) || (componentid == 1087) || ((componentid >= 1008) && (componentid <= 1010))) {
        new nosblocker = IllegalCarNitroIde(vehicleide);
        if (!nosblocker)
            modok = true;
    } else {
        for(new lm; lm < sizeof(legalmods); lm++) {
            if (legalmods[lm][0] == vehicleide) {
                for(new J = 1; J < 22; J++) {
                    if (legalmods[lm][J] == componentid)
                        modok = true;
                }
            }
        }
    }
    return modok;
}
acOnPlayerRemoveNitro(playerid) {
	new carid = GetPlayerVehicleID(playerid);
	if(IsPlayerInAnyVehicle(playerid)) {
		removeNitro(carid);
	}
	return 1;
}
removeNitro(carid) { //Active on syncVehicleParams & OnVehicleMod
	new nitroids[3] = { 1008, 1009, 1010 };
	new component;
	for(new i=0; i < sizeof(nitroids); i++) {
		component = GetVehicleComponentInSlot(carid, CARMODTYPE_NITRO);
		if(component == nitroids[i]) {
			RemoveVehicleComponent(carid,component);
		}
	}
	return 1;
}
acOnGameModeInit() {
}
acOnCharLogin(playerid) {
	LoginSync[playerid] = 2;
}


switchCharWait(playerid) {
	new timenow = gettime();
	if(timenow-LastSwitchChar[playerid] > SWITCHCHAR_WAIT_TIME) {
		return 0;
	}
	return 1;
}
acOnPlayerDisconnect(playerid, reason) {
	if(reason != 3) { //3 = switchchar reason
		for(new i=0;i<12;i++) {
			GunScan[playerid][i][0] = 0;
			GunScan[playerid][i][1] = 0;
		}
		LastAmmo[playerid] = 0;
		GunSync[playerid] = 0;
		HackWarns[playerid] = 0;
		ACLastCheck[playerid] = 0;
		numVehEntries[playerid][0] = 0;
		numVehEntries[playerid][1] = 0;
		currentCar[playerid][0] = 0;
		currentCar[playerid][1] = 0;
		HealthHackWarns[playerid][0] = 0;
		HealthHackWarns[playerid][1] = 0;
		LoginSync[playerid] = 2;
		IllegalModWarns[playerid] = 0;
		PlayerArmour[playerid] = 0.0;
	} else {
		LastSwitchChar[playerid] = gettime();
		LoginSync[playerid] = 0;
	}
	LastUpdate[playerid] = 0;
	playerFrozen[playerid] = 0;
}

hackKick(playerid, reason[], showreason[]) {
	new msg[128];
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_AntiCheat || GetPVarInt(playerid, "HackKick") != PLAYER_VARTYPE_NONE) return 0;
	SetPVarInt(playerid, "HackKick", 1);
	if(strlen(showreason) > 1) {
		format(msg, sizeof(msg), "You have been kicked - %s",showreason);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
	}
	format(msg, sizeof(msg), "SYSTEM: %s has been kicked - %s",GetPlayerNameEx(playerid, ENameType_CharName),reason);
	ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
	#if ANTICHEAT_DBG
		acOnPlayerDisconnect(playerid, 1);
	#else
	if(strlen(msg) > 1) {
		KickEx(playerid, showreason);
	} else {
		KickEx(playerid, "-");
	}
	#endif
	return 1;
}
YCMD:disableanticheat(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enables or disables the server anti-cheat");
		return 1;
	}
	new msg[128];
	if(acdisabled) {
		acdisabled = 0;
		format(msg, sizeof(msg), "%s has enabled the anti-cheat.",GetPlayerNameEx(playerid, ENameType_AccountName));
	} else {
		acdisabled = 1;
		format(msg, sizeof(msg), "%s has disabled the anti-cheat.",GetPlayerNameEx(playerid, ENameType_AccountName));
	}
	ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
	return 1;
}
acOnPlayerSpawn(playerid) {
	SetPVarInt(playerid, "SpawnTime", gettime());
	new Float:health;
	GetPlayerHealth(playerid, health);
	if(health > MAX_HEALTH) {
		SetPlayerHealthEx(playerid, MAX_HEALTH);
	}
}
acCheck(playerid) {
	if(!IsPlayerConnected(playerid) || EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_AntiCheat || IsPlayerNPC(playerid) || switchCharWait(playerid)) {
		return 1;
	}
	new cur_time = gettime();
	new msg[128];
	new Float:hp,Float:armour;
	GetPlayerHealth(playerid,hp);
	GetPlayerArmour(playerid, armour);
	if(hp > MAX_HEALTH) {
		if(!isInPaintball(playerid)) {
			new spawntime = GetPVarInt(playerid, "SpawnTime");
			HealthHackWarns[playerid][0]++;
			if(AC_SPAWN_COOLDOWN-(cur_time-spawntime) < 0) {
				format(msg,sizeof(msg),"[AC] Possible Health hack detected on %s (%.02f health)",GetPlayerNameEx(playerid, ENameType_CharName),hp);
				if(LoginSync[playerid] < 1)
					ABroadcast(X11_TOMATO_2,msg, EAdminFlags_All);
				SetPlayerHealth(playerid, MAX_HEALTH);
			} else {
				SetPlayerHealth(playerid, MAX_HEALTH);
			}
		}
	}
	if(playerFrozen[playerid] == 1) {
		TogglePlayerControllable(playerid, 0);
	}
	if(armour > MAX_ARMOUR || armour > PlayerArmour[playerid]) {
		HealthHackWarns[playerid][1]++;
		format(msg,sizeof(msg),"[AC] Possible Armour hack detected on %s (%.02f armour - %.02f max)",GetPlayerNameEx(playerid, ENameType_CharName),armour,PlayerArmour[playerid]);
		ABroadcast(X11_TOMATO_2,msg, EAdminFlags_All);
		SetPlayerArmour(playerid,  PlayerArmour[playerid]);
	}
	if(GetPlayerMoney(playerid) != GetMoneyEx(playerid)) {
		ResetPlayerMoney(playerid);
		GivePlayerMoney(playerid, GetMoneyEx(playerid));
	}
	if(HealthHackWarns[playerid][0] > MAX_HH_ARMS) {
		hackKick(playerid, "Health Hacks - Possible Desync","SA-MP Desync");		
		return 1;
	}
	if(HealthHackWarns[playerid][1] > MAX_HH_ARMS) {
		#if ANTICHEAT_DBG
			acOnPlayerDisconnect(playerid, 1);
		#else
			BanPlayer(playerid, "Armour Hacks", -1, false, 0);
		#endif
		return 1;
	}
	tpCheck(playerid);
	flyCheck(playerid);
	new gunsync = GetPVarInt(playerid, "GunSync");
	if(gunsync <= 0)
	{
	    new wep,ammo;
		for (new w = 0; w < 12; w++)//For each weapon slot
		{
		    wep = 0;
		    ammo = 0;
			GetPlayerWeaponData(playerid, w, wep, ammo);//Get all his Weapon Data
			if(wep > 0 /*&& ammo != 0*/)//If he has a gun and the ammo also is not 0
			{
				if(ammo <= 0 && (GetPlayerWeapon(playerid) != wep || IsPlayerInAnyVehicle(playerid))) {
					continue;
				}
				if(GunScan[playerid][GetWeaponSlot(wep)][0] != wep)//If the gun was not given by the script
				{
					if(wep == 46) {
						continue;
					}
					if(GunSync[playerid] > 0) {
						continue;
					}
					HackWarns[playerid]++;
				    RemovePlayerWeapon(playerid, wep);
					SetPlayerArmedWeapon(playerid, 0);
					new gunname[32];
					GetWeaponName(wep,gunname,sizeof(gunname));
				    format(msg, sizeof(msg), "Hack Warning: %s attempted to hack a %s with %d bullets.", GetPlayerNameEx(playerid, ENameType_CharName), gunname,ammo);
					ABroadcast(X11_ORANGERED, msg, EAdminFlags_All);
					new maxwarns = MAX_HACK_WARNS;
					if(IsAnLEO(playerid) || isGovernment(playerid)) maxwarns *= 2;
					if(HackWarns[playerid] > maxwarns) {
						if(ammo == INFINITE_AMMO  || ammo == DefaultAmmo[wep] || LastAmmo[playerid] == ammo) {
							format(msg,sizeof(msg),"SYSTEM: %s(%s) has been kicked for suspected hacking of a %s with %d bullets",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName), gunname,ammo);
							hackKick(playerid, "Weapon Hacks", "Desync - Weapons");
						} else {
							new bmsg[128];
							format(bmsg,sizeof(bmsg),"SYSTEM: %s(%s) has been banned for hacking a %s with %d bullets",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName), gunname,ammo);
							strmid(msg, bmsg, 0, strlen(bmsg), sizeof(msg));
							format(msg, sizeof(msg), "Hacking a %s with %d bullets", gunname,ammo);
							BanPlayer(playerid, msg, -1, false, 0);
							strmid(msg, bmsg, 0, strlen(bmsg), sizeof(msg));
						}
						ABroadcast(X11_TOMATO_2,msg, EAdminFlags_All);
					}
					LastAmmo[playerid] = ammo;
					//hackKick(playerid, msg, "Weapon Hacks");
					break;
				}
			}
		}
	}
	if(currentCar[playerid][2] > 0) {
		currentCar[playerid][2]--;
	}
	if(GunSync[playerid] > 0)
		GunSync[playerid]--;
	if(LoginSync[playerid] > 0)
		LoginSync[playerid]--;

	new action = GetPlayerSpecialAction(playerid);
	if(action != SPECIAL_ACTION_NONE) {
		PlayerActionInfo[playerid][0] = action;
		PlayerActionInfo[playerid][1] = cur_time;
		if(action == SPECIAL_ACTION_USEJETPACK) {
			format(msg,sizeof(msg),"SYSTEM: %s(%s) has been banned for jetpack hacking",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName));
			ABroadcast(X11_TOMATO_2,msg, EAdminFlags_All);
			BanPlayer(playerid, "Jetpack hack", -1, false, 0);
		}		
	}

	if(numVehEntries[playerid][1] != 0 && cur_time > numVehEntries[playerid][1]+2) {
		numVehEntries[playerid][0] = 0;
		numVehEntries[playerid][1] = 0;
	}
	return 0;
}
forward SetPlayerSpecialActionEx(playerid, action);
public SetPlayerSpecialActionEx(playerid, action) {
	SetPlayerSpecialAction(playerid, action);
}
getRecentSpecialAction(playerid) {
	new action = SPECIAL_ACTION_NONE;
	if(PlayerActionInfo[playerid][0] != action) {
		if(gettime()-PlayerActionInfo[playerid][1] < AC_SPAWN_COOLDOWN) {
			action = PlayerActionInfo[playerid][0];
		}
	}
	return action;
}
acCarCheck(playerid) {
	if(IsPlayerInAnyVehicle(playerid)) {
		new seat = GetPlayerVehicleSeat(playerid);
		new c = GetPlayerVehicleID(playerid);
		if(PlayerOwnsCar(c,playerid)) return 1;
		if(currentCar[playerid][0] != c) {
			hackKick(playerid, "Vehicle Hacks(Controlling/TPing into cars)", "Desync");
		} else if(seat == 0 && currentCar[playerid][1] != 0 && seat != 128) {
			hackKick(playerid, "Vehicle Hacks(Controlling from passenger)", "Desync");
		}
	} else if(currentCar[playerid][0] != 0 && currentCar[playerid][2] < 1) {
			currentCar[playerid][0] = 0;
	}
	return 1;
}
acOnPlayerStateChange(playerid, newstate, oldstate) {
	if(!IsPlayerConnected(playerid) || EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_AntiCheat || acdisabled == 1) {
		return 1;
	}
	if((oldstate == PLAYER_STATE_ONFOOT && newstate == PLAYER_STATE_DRIVER) || (newstate == PLAYER_STATE_ONFOOT && oldstate == PLAYER_STATE_DRIVER)) {
		if(numVehEntries[playerid][1] == 0)
			numVehEntries[playerid][1] = gettime();
		numVehEntries[playerid][0]++;
		if(numVehEntries[playerid][0] > MAX_VEH_ENTRIES_PER_SEC) {
			hackKick(playerid, "Vehicle Hacks(Spamming)", "Desync");
			//BanPlayer(playerid, "Vehicle Hacks", -1, false, 0);
		}
	}
	return 0;
}
public GivePlayerWeaponEx(playerid, gun, ammo)
{
	if(gun <= 0 || ammo == 0)
		return 0;
	if(ammo < -15000 || ammo == -1) {
	    //ammo = INFINITE_AMMO;
		ammo = DefaultAmmo[gun];
	}
	new slot = GetWeaponSlot(gun);
	if(slot == -1) return 0;
	new tgun, tammo;
	GetPlayerWeaponDataEx(playerid, slot, tgun, tammo);
	if(tgun == gun) {
		//ammo += tammo;
	} else if(tgun != 0) {
		RemovePlayerWeapon(playerid, tgun);
	}
	
    GunSync[playerid] = 5;
    GunScan[playerid][slot][0] = gun;
	GunScan[playerid][slot][1] += ammo;
	
    GivePlayerWeapon(playerid, gun, ammo);
	return 1;
}
forward GetPlayerWeaponDataEx(playerid, slot, &weapon, &ammo);
public GetPlayerWeaponDataEx(playerid, slot, &weapon, &ammo) {
	new r_weapon, r_ammo;
	GetPlayerWeaponData(playerid, slot, r_weapon, r_ammo);
	if(GunScan[playerid][slot][0] == r_weapon && r_ammo > 0) {
		weapon = r_weapon;
		ammo = r_ammo;
	} else {
		weapon = 0;
		ammo = 0;
	}
}

public RemovePlayerWeapon(playerid, weaponid) {

	new plyWeapons[12];
	new plyAmmo[12];
	for(new slot = 0; slot != 12; slot++)
	{
		if(slot == GetWeaponSlot(weaponid)) {
			continue;
		}
		GetPlayerWeaponDataEx(playerid, slot, plyWeapons[slot], plyAmmo[slot]);
	}
	ResetPlayerWeaponsEx(playerid);
	for(new slot = 1; slot != 12; slot++)
	{
	    if(plyAmmo[slot] < -15000)
			plyAmmo[slot] = DefaultAmmo[plyWeapons[slot]];
			//plyAmmo[slot] = INFINITE_AMMO;
		GivePlayerWeaponEx(playerid, plyWeapons[slot], plyAmmo[slot]);
	}
	return 1;
}
public IsPlayerHoldingWeaponInSlot(playerid, slot) {
	new g,a;
	GetPlayerWeaponDataEx(playerid, slot, g, a);
	return g != 0;
}
public GetPlayerWeaponEx(playerid)
{
	new gun = GetPlayerWeapon(playerid);
	new slot = GetWeaponSlot(gun);
	if(slot == -1) return 0;
	if(GunScan[playerid][slot][0] == gun)
	    return gun;

	return 0;
}
public GetWeaponSlot(weapon) {
	if(weapon >= 0 && weapon <= 1) {
		return 0;
	}
	if(weapon >= 2 && weapon <= 9) {
		return 1;
	}
	if(weapon >= 10 && weapon <= 15) {
		return 10;
	}
	if(weapon >= 16 && weapon <= 18) {
		return 8;
	}
	if(weapon >= 22 && weapon <= 24) {
		return 2;
	}
	if(weapon >= 25 && weapon <= 27) {
		return 3;
	}
	if(weapon >= 28 && weapon <= 29 || weapon == 32) {
		return 4;
	}
	if(weapon >= 30 && weapon <= 31) {
		return 5;
	}
	if(weapon >= 33 && weapon <= 34) {
		return 6;
	}
	if(weapon >= 35 && weapon <= 38) {
		return 7;
	}
	if(weapon == 39) {
		return 8;
	}
	if(weapon == 40) {
		return 12;
	}
	if(weapon >= 41 && weapon <= 43) {
		return 9;
	}
	if(weapon >= 44 && weapon <= 46) {
		return 11;
	}
	return -1;
}
public ResetPlayerWeaponsEx(playerid) {
	for(new i=0;i<12;i++) {
		GunScan[playerid][i][0] = 0;
		GunScan[playerid][i][1] = 0;
	}
	ResetPlayerWeapons(playerid);
}
public SetPlayerHealthEx(playerid, Float:health) {
	if(health < 0.0) health = 0.0;
	if(health > MAX_HEALTH) {		
		//if(health < 0.0 || health > MAX_HEALTH+25.0) health = 0.0;
		//else
		health = MAX_HEALTH;
		
	}
	SetPlayerHealth(playerid, health);
	return 0;
}
public SetPlayerArmourEx(playerid, Float:health) {
	if(health < 0.0 || health > MAX_ARMOUR) {		
		if(health < 0.0 || health > MAX_ARMOUR+25.0) 
			health = 0.0;		
		else
			health = MAX_ARMOUR;
		
	}
	PlayerArmour[playerid] = health;
	SetPlayerArmour(playerid, health);
	return 0;
}
public GetMoneyEx(playerid) {
	return GetPVarInt(playerid, "Money");
}
public GiveMoneyEx(playerid, money) {
	new bmoney = GetMoneyEx(playerid);
	bmoney += money;
	SetPVarInt(playerid, "Money", bmoney);
	ResetPlayerMoney(playerid);
	GivePlayerMoney(playerid, bmoney);
}
SetMoneyEx(playerid, money) {
	SetPVarInt(playerid, "Money", money);
	ResetPlayerMoney(playerid);
	GivePlayerMoney(playerid, money);	
}
IsPlayerPaused(playerid) {
	new tnow = gettime();
	if(LastUpdate[playerid] != 0 && tnow-LastUpdate[playerid] > 5) return tnow-LastUpdate[playerid];
	return 0;
}
checkAFK() {
	if(numPlayersOnServer() < 15) return 0;
	new msg[128];
	new tnow = gettime();
	foreach(Player, i) {
		if(LastUpdate[i] == 0 || !IsPlayerConnectEx(i) || EAdminFlags:GetPVarInt(i, "AdminFlags") & EAdminFlags_AntiCheat || isInJail(i)) continue;
		if(tnow-LastUpdate[i] > MAX_AFK_TIME) {
			KickEx(i, "Exceeded Maximum AFK Time");
			format(msg, sizeof(msg), "SYSTEM: %s has been kicked for being AFK",GetPlayerNameEx(i, ENameType_CharName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
		}
	}
	return 1;
}
acOnPlayerUpdate(playerid) {
	if(acdisabled == 0 || switchCharWait(playerid)) {
		acCarCheck(playerid);
	}
	if(AC_CHECK_COOLDOWN-(gettime()-ACLastCheck[playerid]) < 0 && acdisabled == 0) {
		acCheck(playerid);
		ACLastCheck[playerid] = gettime();
	}
	LastUpdate[playerid] = gettime();
	return 1;
}

tpCheck(playerid)
{
	new string[128];
	new Float:maxspeed = 500.0;
	GetPlayerPos(playerid, TelePos[playerid][3], TelePos[playerid][4], TelePos[playerid][5]);
	if(TelePos[playerid][5] > 550.0)
	{
		TelePos[playerid][0] = 0.0;
		TelePos[playerid][1] = 0.0;
	}
	if(TelePos[playerid][0] != 0.0 && GetPlayerState(playerid) == PLAYER_STATE_DRIVER)
	{
		if(isFlyCar((GetVehicleModel(GetPlayerVehicleID(playerid))))) {
			return 1;
		}
	    new Float:xdist = TelePos[playerid][3]-TelePos[playerid][0];
		new Float:ydist = TelePos[playerid][4]-TelePos[playerid][1];
		new Float:sqxdist = xdist*xdist;
		new Float:sqydist = ydist*ydist;
		new Float:distance = (sqxdist+sqydist)/31;
		if(distance > maxspeed)
		{
			format(string, sizeof(string), "Hack Warning: [%d]%s %.0f mph",playerid,GetPlayerNameEx(playerid, ENameType_CharName),distance); //caused crash here
			if(LoginSync[playerid] < 1)
				ABroadcast(X11_TOMATO_2, string, EAdminFlags_All);
		}
	}
	if(TelePos[playerid][5] < 550.0 && TelePos[playerid][3] != 0.0)
	{
		TelePos[playerid][0] = TelePos[playerid][3];
		TelePos[playerid][1] = TelePos[playerid][4];
	}
	return 1;
}
flyCheck(playerid) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	new anim = GetPlayerAnimationIndex(playerid);
	new msg[128];
	if(anim != 0 && isSwimAnim(anim)) {
		if(Z >= 125.0 && (GetPlayerVirtualWorld(playerid) == 0 && GetPlayerInterior(playerid) == 0)) {
			format(msg, sizeof(msg), "[AC] Possible fly hack detected on %s",GetPlayerNameEx(playerid, ENameType_CharName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
		}
	}
}
TogglePlayerControllableEx(playerid, status) {
	playerFrozen[playerid] = status==1?0:1;
	TogglePlayerControllable(playerid, status);
}
isPlayerFrozen(playerid) {
	return playerFrozen[playerid];
}
acOnPlayerEnterVehicle(playerid, carid, ispassenger) {
	currentCar[playerid][0] = carid;
	currentCar[playerid][1] = ispassenger;
	currentCar[playerid][2] = 30;
}
acOnCarJackAttempt(playerid, newkeys, oldkeys) {
	#pragma unused playerid
	#pragma unused oldkeys
	new msg[128];
	new Float: X, Float: Y, Float: Z;
	new iEngine, iLights, iAlarm, iDoors, iBonnet, iBoot, iObjective;
	new carid = GetClosestVehicle(playerid); //Get the closest random vehicle
	new tnow = gettime();
	if(newkeys & KEY_SECONDARY_ATTACK) {
		GetPlayerPos(playerid, X, Y, Z);
		new Float: fDistance = GetVehicleDistanceFromPoint(carid, X, Y, Z);
		if(fDistance < 15.0) { //Are THEY near that vehicle? Otherwise it spams the text when they enter an interior
			if(IsVehicleOccupied(carid)) { //Is the car occupied? If not, it's not car jacking..
				GetVehicleParamsEx(carid, iEngine, iLights, iAlarm, iDoors, iBonnet, iBoot, iObjective);
				if(iDoors == 1) return 1; //Are the doors locked? If so, what's the point of going through this?
				new driver = GetVehicleDriver(carid); //Get the driver id for that vehicle.
				if(driver != INVALID_PLAYER_ID) { //If there's someone driving, then  do all of this, otherwise allow the player to get in the vehicle..
					ToggleACDoorsForPlayer(carid, playerid, 1); //Toggle the doors for the player
					ToggleACDoorsForPlayer(carid, driver, 1); //Toggle the doors for the driver
					SetTimerEx("ToggleACDoorsForPlayer",4000,false,"ddd",carid,driver,0); //Set them back to default after 4 seconds
					SetTimerEx("ToggleACDoorsForPlayer",4000,false,"ddd",carid,playerid,0); //Set them back to default after 4 seconds
					if(tnow-LastCarJackTime[playerid] > CARJACK_CHECK_COOLDOWN) {
						format(msg, sizeof(msg), "AdmWarn: Possible car jack attempt from [%d] %s",playerid, GetPlayerNameEx(playerid, ENameType_CharName));
						ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
						LastCarJackTime[playerid] = gettime();
					}
				}
			}
		}
	}

	/*
	new iEngine, iLights, iAlarm, iDoors, iBonnet, iBoot, iObjective;
	new carid = GetClosestVehicle(playerid);
	new tnow = gettime();
	GetVehicleParamsEx(carid, iEngine, iLights, iAlarm, iDoors, iBonnet, iBoot, iObjective);
	if(iDoors == 1) {
		return 1;
	}
	if(newkeys & KEY_SECONDARY_ATTACK) {
		if(IsVehicleOccupied(carid)) {
			new driver = GetVehicleDriver(carid);
			if(driver != INVALID_PLAYER_ID) {
				ToggleACDoorsForPlayer(carid, playerid, 1);
				ToggleACDoorsForPlayer(carid, driver, 1);
				SetTimerEx("ToggleACDoorsForPlayer",4000,false,"ddd",carid,driver,0);
				SetTimerEx("ToggleACDoorsForPlayer",4000,false,"ddd",carid,playerid,0);
				if(tnow-LastCarJackTime[playerid] > CARJACK_CHECK_COOLDOWN) {
					format(msg, sizeof(msg), "AdmWarn: Possible car jack attempt from [%d] %s",playerid, GetPlayerNameEx(playerid, ENameType_CharName));
					ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
					LastCarJackTime[playerid] = gettime();
				}
			}
		}
	}
	*/
	return 1;
}
forward ToggleACDoorsForPlayer(carid, playerid, doors);
public ToggleACDoorsForPlayer(carid, playerid, doors) {
	SetVehicleParamsForPlayer(carid, playerid, 0, doors);
}
PutPlayerInVehicleEx(playerid,vehicleid,seatid) {
	currentCar[playerid][0] = vehicleid;
	currentCar[playerid][1] = seatid;
	currentCar[playerid][2] = 3;
	PutPlayerInVehicle(playerid, vehicleid, seatid);
}
GetNumVehiclesPlayerIn(playerid) {
	new x;
	for(new i=0;i<MAX_VEHICLES;i++) {
		if(GetVehicleModel(i) != 0) {
			if(IsPlayerInVehicle(playerid, i)) {
				x++;
			}
		}
	}
	return x;
}
acOnVehicleMod(playerid, vehicleid, componentid) {
	new vehicleide = GetVehicleModel(vehicleid);
    new modok = islegalcarmod(vehicleide, componentid);
	new string[128];
	if(!modok) {
		if(++IllegalModWarns[playerid] > MAX_ILLEGAL_CAR_MOD_WARNS) {
			hackKick(playerid, "Illegal Car Mod Spamming", "Desync");
			removeCarComponents(vehicleid);
			return 1;
		}
		format(string, sizeof(string), "[AC] Illegal Car Mod from %s[%d]: %d[%d]",GetPlayerNameEx(playerid,ENameType_CharName),playerid,vehicleid,componentid); //caused crash here
		ABroadcast(X11_TOMATO_2, string, EAdminFlags_All);
		RemoveVehicleComponent(vehicleid, componentid);
	}
	return 0;
}
acOnPlayerGiveDamage(playerid, damagedid, Float:amount, weaponid) {
	#pragma unused weaponid 
	#pragma unused amount
	if(isInPaintball(playerid)) {
		if(!isInPaintball(damagedid)) {
			hackKick(playerid, "Weapon Hacks - Damaging outside Paintball", "Desync");
		}
	}
	refreshArmour(damagedid);
}
acOnPlayerTakeDamage(playerid, issuerid, Float:amount, weaponid, bodypart) {
	refreshArmour(playerid);
}
GetPlayerArmourEx(playerid, &Float:armour) {
	new Float:client_armour;
	GetPlayerArmour(playerid, client_armour);

	if(client_armour > PlayerArmour[playerid])
		client_armour = PlayerArmour[playerid];

	armour = client_armour;
}
refreshArmour(playerid) {
	new Float:armour;
	GetPlayerArmour(playerid, armour);
	if(armour < PlayerArmour[playerid]) {
		PlayerArmour[playerid] = armour;
		SetPlayerArmour(playerid, PlayerArmour[playerid]);
	}
}
acOnKeyStateChange(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	#pragma unused playerid
	if(newkeys & KEY_SECONDARY_ATTACK) {
		acOnCarJackAttempt(playerid, newkeys, oldkeys);
	}
}