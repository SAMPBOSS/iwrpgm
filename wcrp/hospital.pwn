enum EHospital_Location {
	ALLSAINTS_HOSPITAL,
	OTHER_HOSPITAL,
}

enum EHospitalSpawns {
	Float:SpawnX, 
	Float:SpawnY,
	Float:SpawnZ,
	Float:SpawnAngle,
	EHospital_Location:SpawnLocation,
	Freeze,
}

new HospitalSpawns[][EHospitalSpawns] = {
	{-1234.4934,-423.5258,71.7579,359.8155, ALLSAINTS_HOSPITAL, 1},
	{-1228.2040,-423.4597,71.7579,359.1888, ALLSAINTS_HOSPITAL, 1},
	{-1222.1825,-423.5334,71.7579,0.1522, ALLSAINTS_HOSPITAL, 1},
	{-1216.1604,-423.4316,71.7579,357.0188, ALLSAINTS_HOSPITAL, 1}
};

forward HospitalCharges(playerid);
hospitalOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused reason
	if(IsRobbingBusines(playerid) != -1) {
		StopRobBiz(playerid);
	}
	if(isInPaintball(playerid)) {
		if(killerid != INVALID_PLAYER_ID) {
			paintballAwardPoint(killerid);
		}
		return 1;
	}
	SetPVarInt(playerid, "PlayerDied", 1);
	new deaths = GetPVarInt(playerid, "Deaths");
	deaths++;
	SetPVarInt(playerid, "Deaths", deaths);
	if(killerid != INVALID_PLAYER_ID) {
		new kills = GetPVarInt(killerid, "Kills");
		kills++;
		SetPVarInt(killerid, "Kills", kills);
	}
	if(numMedicsOnline() != 0) {
		saveSpecDetails(playerid);
	}
	return 0;
}
hospitalOnPlayerSpawn(playerid) {
	SetPlayerHealthEx(playerid, MAX_HEALTH);
	if(isInPaintball(playerid)) {
		paintballRespawn(playerid);
		DeletePVar(playerid, "PlayerDied");
		return 1;
	}
	if(GetPVarInt(playerid, "AdminDuty") != 0) {
		return 1;
	}
	if(GetPVarInt(playerid, "PlayerDied") == 1) {
		if(numMedicsOnline() == 0 || playerDiseaseInstantDeath(playerid)) {
			putPlayerInHospital(playerid);
		} else {
			makePlayerDying(playerid);
		}
		if(removeDiseaseOnDeath(playerid)) {
			curePlayerDisease(playerid);
		}
		DeletePVar(playerid, "PlayerDied");
	}
	SetPlayerHealthEx(playerid, 98.0);
	return 1;
}
putPlayerInHospital(playerid) {
	if(!IsPlayerConnectEx(playerid)) return 0;
	if(GetPVarInt(playerid, "AJailReleaseTime") != 0) {
		SetPlayerPos(playerid, 2525.92, -1673.35, 14.86);
		SetPlayerVirtualWorld(playerid, playerid+15000);
		SetPlayerInterior(playerid, 0);
		ResetPlayerWeaponsEx(playerid);
		TogglePlayerControllableEx(playerid, 0);
		return 1;
	} else if(GetPVarInt(playerid, "ReleaseTime") != 0 || playerHasPrisonLifeSentence(playerid)) {
		putInLEOCells(playerid,GetPVarInt(playerid, "JailType"));
		return 1;
	}
	DeletePVar(playerid, "MedicCall");
	//GameTextForPlayer(playerid, "~n~~n~~n~~n~~n~~n~~n~~y~You are recovering", 12000, 3);
	new DrugFlags:aflags = DrugFlags:GetPVarInt(playerid, "drug_effects");
	ShowScriptMessage(playerid, "You are recovering...", aflags & EDrugEffect_FasterHealing ? 6000 : 11000);
	/* Hospital Spawn Stuff */
	new rand = random(4);
	SetPlayerInterior(playerid, 1);
	SetPlayerVirtualWorld(playerid, 36);
	SetPlayerPos(playerid,HospitalSpawns[rand][SpawnX], HospitalSpawns[rand][SpawnY], HospitalSpawns[rand][SpawnZ]+0.5);
	SetPlayerFacingAngle(playerid, HospitalSpawns[rand][SpawnAngle]);
	
	SetPlayerCameraPos(playerid, HospitalSpawns[rand][SpawnX], HospitalSpawns[rand][SpawnY]+1.0, HospitalSpawns[rand][SpawnZ]);
	SetPlayerCameraLookAt(playerid, -1238.6064,-423.3407,72.3286);
	SetPlayerDrunkLevel(playerid, 20000);

	ApplyAnimation(playerid,"CRACK","crckdeth2",4.1,0,1,1,1,1);
	TogglePlayerControllableEx(playerid, 0);
	//SetTimerEx("SetControllable",12000,false,"dd",playerid,1);
	/* Spawn End */
	SetPVarInt(playerid, "MatsA", 0);
	SetPVarInt(playerid, "MatsB", 0);
	SetPVarInt(playerid, "MatsC", 0);
	SetPVarInt(playerid, "Pot", 0);
	SetPVarInt(playerid, "Coke", 0);
	SetPVarInt(playerid, "Meth", 0);
	RemovePlayerItem(playerid);
	ResetPlayerWeaponsEx(playerid);
	SetPlayerHunger(playerid, 50);
	ApplyAnimation(playerid,"CRACK","crckdeth4",4.0,1,0,0,0,12000);
	SetTimerEx("HospitalCharges", aflags & EDrugEffect_FasterHealing ? 6000 : 11000, false, "d", playerid);
	/*
	if(GetPVarType(playerid, "HospitalTimer") != PLAYER_VARTYPE_NONE) {
		new hospitaltimer = SetTimerEx("HospitalCharges", 12000, false, "d", playerid);
		SetPVarInt(playerid, "HospitalTimer", hospitaltimer);
	}
	*/
	SetPlayerHealthEx(playerid, 98.0);
	saveSQLGuns(playerid);
	SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
	restoreAccessories(playerid);
	destroyAllRelatedMedicPVars(playerid);
	clearTimesShot(playerid);
	if(GetPVarType(playerid, "SpecX") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "SpecX");
	}
	return 1;
}
public HospitalCharges(playerid) {
	new hospitalcharge;
	new msg[256], insurancetext[8];
	//DeletePVar(playerid, "HospitalTimer");
	SetCameraBehindPlayer(playerid);
	SetPlayerDrunkLevel(playerid, 0); //Reset the drunk level
	TogglePlayerControllableEx(playerid, 1);
	hospitalcharge += 300; //hospital charge (was the base charge)
	new insurance = GetPVarInt(playerid, "UserFlags") & EUFHasHealthInsurance;
	if(insurance) {
		format(insurancetext, sizeof(insurancetext), "~g~Yes");
		hospitalcharge = 0;
	} else {
		format(insurancetext, sizeof(insurancetext), "~r~No");
	}
	GiveMoneyEx(playerid, -hospitalcharge);
	tryMakePlayerEnterOrExit(playerid); //This will attempt to make the player exit or enter an interior
	SetPlayerFacingAngle(playerid, 270.0); //Set their angle to 270.0 (Hospital Exit)
	format(msg, sizeof(msg), "~g~Medical Bill~n~~w~Insurance: %s~n~~w~Cost: $%s~n~Money Lost: $%s~n~Have a good day!",insurancetext, getNumberString(hospitalcharge), getNumberString(hospitalcharge));
	ShowScriptMessage(playerid, msg, 10000);
	return 1;
}
isInsideHospital(playerid) {
	if(IsPlayerInRangeOfPoint(playerid, 150.0, -1259.0489, -407.5331, 74.5859)) {
		if(GetPlayerVirtualWorld(playerid) == 36) {
			return 1;
		} else if(GetPlayerVirtualWorld(playerid) == 37) {
			return 2;
		}
	}
	return 0;
}
isAtLAFD(playerid) {
	if(IsPlayerInRangeOfPoint(playerid, 75.0, 2316.0898, -2003.3459, 13.5475)) {
		return 1;
	}
	return 0;
}