enum EHouseInfo {
	Float:EHouseX,
	Float:EHouseY,
	Float:EHouseZ,
	Float:EHouseExitX,
	Float:EHouseExitY,
	Float:EHouseExitZ,
	EHouseInterior,
	EHousePickupInt,
	EHousePickupVW,
	EHouseName[64],
	EHouseOwnerName[MAX_PLAYER_NAME],
	EHouseOwnerSQLID,
	EHouseSQLID,
	EHouseValue,
	EHouseLevel,
	EHousePickup,
	EHouseLocked,
	EHouseLights,
	EHouseRadioIndex,
	EHouseRentPrice, //0 = not renting
	Text3D:EHouseTextLabel,
};
new hinit = 0;
#define MAX_HOUSES 3000
#define HOUSE_TAX_RATE 0.006 //House tax rate

new Houses[MAX_HOUSES][EHouseInfo];
new Text:HouseLightsText;
forward OnLoadHouses();
forward ReloadHouse(houseid);

housesOnGameModeInit() {
	loadHouses();
	HouseLightsText = TextDrawCreate(1.0,1.0,"D");
	TextDrawAlignment(HouseLightsText,0);
	TextDrawBackgroundColor(HouseLightsText,0x00000000);
	TextDrawFont(HouseLightsText,1);
	TextDrawLetterSize(HouseLightsText,200.0,400.0);
	TextDrawColor(HouseLightsText,0x000000A9);
	TextDrawSetOutline(HouseLightsText,false);
	TextDrawSetProportional(HouseLightsText,false);
	TextDrawSetShadow(HouseLightsText,false);
	TextDrawUseBox(HouseLightsText, true);
	TextDrawBoxColor(HouseLightsText, 0x000000A9);
}
loadHouses() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `houses`.`id`,`houses`.`X`,`houses`.`Y`,`houses`.`Z`,`EX`,`EY`,`EZ`,`houses`.`interior`,`houses`.`name`,`price`,`owner`,`c1`.`username`,`locked`,`houses`.`rent`,COUNT(`hf`.`house`),`pickupint`,`pickupvw` FROM `houses` LEFT JOIN `characters` AS `c1` ON `houses`.`owner` = `c1`.`id` LEFT JOIN `housefurniture` `hf` ON `hf`.house = `houses`.`id` GROUP BY `hf`.`house`,`houses`.`id`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadHouses", "");
}
public OnLoadHouses() {
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		//if(Houses[i][EHouseSQLID] != 0) continue; //because this is also used for reloading houses
		new index = findFreeHouse();
		if(index == -1) {
			printf("House array full!");
			return 0;
		}
		cache_get_row(i, 0, id_string);
		Houses[index][EHouseSQLID] = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		Houses[index][EHouseX] = floatstr(id_string);
		
		cache_get_row(i, 2, id_string);
		Houses[index][EHouseY] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		Houses[index][EHouseZ] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		Houses[index][EHouseExitX] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		Houses[index][EHouseExitY] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		Houses[index][EHouseExitZ] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		Houses[index][EHouseInterior] = strval(id_string);
		
		cache_get_row(i, 8, Houses[index][EHouseName]);
		
		cache_get_row(i, 9, id_string);
		Houses[index][EHouseValue] = strval(id_string);

		cache_get_row(i, 10, id_string);
		Houses[index][EHouseOwnerSQLID] = strval(id_string);
		
		cache_get_row(i, 11, Houses[index][EHouseOwnerName]);
		
		cache_get_row(i, 12, id_string);
		Houses[index][EHouseLocked] = strval(id_string);
		if(Houses[index][EHouseOwnerSQLID] == 0) {
			Houses[index][EHouseLocked] = 1;
		}
		
		cache_get_row(i, 13, id_string);
		Houses[index][EHouseRentPrice] = strval(id_string);

		Houses[index][EHouseLights] = 1; //off by default
		Houses[index][EHouseRadioIndex] = -1;
		
		cache_get_row(i, 14, id_string);
		if(strval(id_string) > 0)
			LoadHouseFurniture(index);

		cache_get_row(i, 15, id_string);
		Houses[index][EHousePickupInt] = strval(id_string);

		cache_get_row(i, 16, id_string);
		Houses[index][EHousePickupVW] = strval(id_string);
		//1272:1273
		Houses[index][EHousePickup] = CreateDynamicPickup(Houses[index][EHouseOwnerSQLID]>0?19198:1273,16,Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseOwnerSQLID]>0 ? Houses[index][EHouseZ]+0.50 : Houses[index][EHouseZ], Houses[index][EHousePickupVW], Houses[index][EHousePickupInt]);
		
		new labeltext[256];
		getHouseTextLabel(index, labeltext, sizeof(labeltext));
		Houses[index][EHouseTextLabel] = CreateDynamic3DTextLabel(labeltext, 0x00FF00FF, Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseZ]+1.0,10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, Houses[index][EHousePickupVW], Houses[index][EHousePickupInt]); //15.0 was the old streaming range..
	}
	if(hinit == 0) {
		sellInactiveHouses();
		loadHouseSafes();
		hinit = 1;
	}
	return 1;
}
findFreeHouse() {
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
houseGetVirtualWorld(houseid) {
	return houseid+20000;
}
houseGetInterior(houseid) {
	return Houses[houseid][EHouseInterior];
}
housesTryEnterExit(playerid) {
	for(new i=0;i<sizeof(Houses);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ])) {
			if(GetPlayerInterior(playerid) == Houses[i][EHousePickupInt] && GetPlayerVirtualWorld(playerid) == Houses[i][EHousePickupVW]) {
				if(IsHouseLocked(i)) {
					GameTextForPlayer(playerid, "~r~Locked", 2000, 1);
					return 1;
				}
				if(Houses[i][EHouseLights] == 1) {
					TextDrawShowForPlayer(playerid, HouseLightsText);
				}
				if(Houses[i][EHouseRadioIndex] != -1) {
					new url[128];
					getRadioURL(Houses[i][EHouseRadioIndex], url, sizeof(url));
					PlayAudioStreamForPlayer(playerid, url);
				}
				SetPlayerPos(playerid,  Houses[i][EHouseExitX],Houses[i][EHouseExitY],Houses[i][EHouseExitZ]);
				SetPlayerInterior(playerid, Houses[i][EHouseInterior]);
				SetPlayerVirtualWorld(playerid, houseGetVirtualWorld(i));
			}
		} else if(IsPlayerInRangeOfPoint(playerid, 5.0, Houses[i][EHouseExitX],Houses[i][EHouseExitY],Houses[i][EHouseExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == houseGetVirtualWorld(i)) {
				if(Houses[i][EHouseLights] == 1) {
					TextDrawHideForPlayer(playerid, HouseLightsText);
				}
				if(Houses[i][EHouseRadioIndex] != -1) {
					StopAudioStreamForPlayer(playerid);
				}
				if(Houses[i][EHousePickupVW] != 0 || Houses[i][EHousePickupInt] != 0) {
					WaitForObjectsToStream(playerid);
				}
				SetPlayerPos(playerid, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ]);
				SetPlayerInterior(playerid, Houses[i][EHousePickupInt]);
				SetPlayerVirtualWorld(playerid, Houses[i][EHousePickupVW]);
			}
		}
	}
	return 1;
}
YCMD:displayhouseinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays information on a house");
		return 1;
	}
	new houseid;
	if(sscanf(params,"d",houseid)) {
		houseid = getStandingHouse(playerid);
		if(houseid == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
			return 1;
		}
	}
	if(houseid < 0 || houseid > sizeof(Houses) || Houses[houseid][EHouseSQLID] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid House");
		return 1;
	}
	format(query, sizeof(query), "******** %s[%d] ********",Houses[houseid][EHouseName],houseid);
	SendClientMessage(playerid, X11_WHITE, query);
	format(query, sizeof(query), "* House ID: %d SQLID: %d",houseid,Houses[houseid][EHouseSQLID]);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	format(query, sizeof(query), "* Owner %s[%d]",Houses[houseid][EHouseOwnerName],Houses[houseid][EHouseOwnerSQLID]);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	format(query, sizeof(query), "* Pos: (%f,%f,%f)",Houses[houseid][EHouseX],Houses[houseid][EHouseY],Houses[houseid][EHouseZ]);
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	format(query, sizeof(query), "* EPos: (%f,%f,%f,%d,%d)",Houses[houseid][EHouseExitX],Houses[houseid][EHouseExitY],Houses[houseid][EHouseExitZ],Houses[houseid][EHouseInterior],houseGetVirtualWorld(houseid));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	format(query, sizeof(query), "* Rent: %d Locked: %d",Houses[houseid][EHouseRentPrice],Houses[houseid][EHouseLocked] );
	SendClientMessage(playerid, COLOR_LIGHTBLUE, query);
	SendClientMessage(playerid, X11_WHITE, "*******************");
	return 1;
}
YCMD:sethouseowner(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a houses owner");
		return 1;
	}
	new houseid, user;
	if(!sscanf(params, "k<playerLookup_acc>D(-1)", user, houseid)) {
		if(!IsPlayerConnectEx(user)) {
			//SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			//return 1;
			user = -1;
		}
		if(houseid == -1) {
			houseid = getStandingHouse(playerid);
			if(houseid == -1) {
				SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
				return 1;
			}
		}
		if(houseid < 0 || houseid > sizeof(Houses) || Houses[houseid][EHouseSQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid House");
			return 1;
		}
		setHouseOwner(houseid, user);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Owner updated");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sethouseowner [user(or -1)] (house)");
	}
	return 1;
}
YCMD:gotohouse(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a houses owner");
		return 1;
	}
	new houseid;
	if(!sscanf(params,"d", houseid)) {
		if(houseid < 0 || houseid > sizeof(Houses) || Houses[houseid][EHouseSQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid House");
			return 1;
		}
		SetPlayerPos(playerid, Houses[houseid][EHouseX],Houses[houseid][EHouseY],Houses[houseid][EHouseZ]);
		SetPlayerInterior(playerid, Houses[houseid][EHousePickupInt]);
		SetPlayerVirtualWorld(playerid, Houses[houseid][EHousePickupVW]);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotohouse [houseid]");
	}
	return 1;
}
houseOnPlayerLogin(playerid) {
	SetTimerEx("HouseSync", 1000, false, "d", playerid);
}
forward HouseSync(playerid);
public HouseSync(playerid) {
	new house = getStandingExit(playerid, 150.0);
	if(house != -1) {
		if(Houses[house][EHouseLights] == 1) {
			TextDrawShowForPlayer(playerid, HouseLightsText);
		} else {
			TextDrawHideForPlayer(playerid, HouseLightsText);
		}
		if(Houses[house][EHouseRadioIndex] != -1) {
			new url[128];
			getRadioURL(Houses[house][EHouseRadioIndex], url, sizeof(url));
			PlayAudioStreamForPlayer(playerid, url);
		}
	}
}
YCMD:houselights(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggle house lights on/off");
	}
	new house = getStandingExit(playerid, 350.0);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in a house!");
		return 1;
	}
	toggleLights(playerid, house);
	return 1;
}
toggleLights(playerid, house) {
	new msg[128];
	switch(Houses[house][EHouseLights]) {
		case 1: {
			format(msg, sizeof(msg), "* %s switches the lights on.",GetPlayerNameEx(playerid, ENameType_RPName));
			setLights(house, 0);
		}
		case 0: {
			format(msg, sizeof(msg), "* %s switches the lights off.",GetPlayerNameEx(playerid, ENameType_RPName));
			setLights(house, 1);
		}
	}
	ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
}
stock setLights(house, lights) {
	foreach(Player, i) {
		if(getStandingExit(i, 350.0) == house) {
			if(lights) {
				TextDrawShowForPlayer(i, HouseLightsText);
				Houses[house][EHouseLights] = 1;
			} else {
				TextDrawHideForPlayer(i, HouseLightsText);
				Houses[house][EHouseLights] = 0;
			}
		}
	}
}
GetNumOwnedHouses(playerid) {
	new num;
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
			num++;
		}
	}
	return num;
}
makeHouse(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, name[], price, locked = 1, pickupint, pickupvw) {
	query[0] = 0;
	mysql_real_escape_string(name,tempstr,g_mysql_handle);
	format(query,sizeof(query),"INSERT INTO `houses` (`X`,`Y`,`Z`,`EX`,`EY`,`EZ`,`interior`,`name`,`price`,`locked`,`pickupint`,`pickupvw`) VALUES (%f,%f,%f,%f,%f,%f,%d,\"%s\",%d,%d,%d,%d)",
	X,Y,Z,EX,EY,EZ,interior,tempstr,price,locked,pickupint, pickupvw);
	mysql_function_query(g_mysql_handle, query, true, "OnHouseCreate", "ffffffdsdddd",X,Y,Z,EX,EY,EZ,interior,tempstr,price,locked,pickupint, pickupvw);
}
forward OnHouseCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, name[], price, locked, pickupint, pickupvw);
public OnHouseCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior, name[], price, locked, pickupint, pickupvw) {
	new id = mysql_insert_id();
	new index = findFreeHouse();
	new data[128];
	if(index == -1) {
		ABroadcast(X11_RED,"[AdmWarn]: Failed to create house. House array is full.",EAdminFlags_BasicAdmin);
		return 0;
	}
	Houses[index][EHouseSQLID] = id;
	Houses[index][EHouseX] = X;
	Houses[index][EHouseY] = Y;
	Houses[index][EHouseZ] = Z;
	Houses[index][EHouseExitX] = EX;
	Houses[index][EHouseExitY] = EY;
	Houses[index][EHouseExitZ] = EZ;
	Houses[index][EHouseInterior] = interior;
	format(Houses[index][EHouseName], 64, "%s", name);
	Houses[index][EHouseValue] = price;
	Houses[index][EHouseOwnerSQLID] = 0;
	format(Houses[index][EHouseOwnerName], 64, "None");
	Houses[index][EHousePickupVW] = pickupvw;
	Houses[index][EHousePickupInt] = pickupint;

	Houses[index][EHousePickup] = CreateDynamicPickup(Houses[index][EHouseOwnerSQLID]>0?19198:1273,16,Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseOwnerSQLID]>0 ? Houses[index][EHouseZ]+0.50 : Houses[index][EHouseZ], Houses[index][EHousePickupVW], Houses[index][EHousePickupInt]);
		
	getHouseTextLabel(index, data, sizeof(data));
	Houses[index][EHouseTextLabel] = CreateDynamic3DTextLabel(data, 0x00FF00FF, Houses[index][EHouseX], Houses[index][EHouseY], Houses[index][EHouseZ]+1.0,10.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, Houses[index][EHousePickupVW], Houses[index][EHousePickupInt]); //15.0 was the old streaming range..
		
	format(data,sizeof(data),"[AdmNotice]: House ID: %d",id);
	ABroadcast(X11_RED,data,EAdminFlags_BasicAdmin);
	return 1;
}
YCMD:houseentrance(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Marks the entrance of a house for creation");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	SetPVarFloat(playerid,"HouseEntranceX", X);
	SetPVarFloat(playerid,"HouseEntranceY", Y);
	SetPVarFloat(playerid,"HouseEntranceZ", Z);
	SendClientMessage(playerid, COLOR_DARKGREEN, "House entrance Marked!");
	return 1;
}
YCMD:houseexit(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Marks the exit of a house for creation");
		return 1;
	}
	new Float:X,Float:Y,Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	SetPVarFloat(playerid,"HouseExitX", X);
	SetPVarFloat(playerid,"HouseExitY", Y);
	SetPVarFloat(playerid,"HouseExitZ", Z);
	SetPVarInt(playerid, "HouseInterior", GetPlayerInterior(playerid));
	SendClientMessage(playerid, COLOR_DARKGREEN, "House exit Marked!");
	return 1;
}
YCMD:createhouse(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Adds a house to the database and the game.");
		return 1;
	}
	new price,name[64];
	if (!sscanf(params, "ds[64]", price,name))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Attempting to create House.");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /createhouse [price] [name]");
		SendClientMessage(playerid, X11_LIGHTBLUE,"Notice: use /houseentrance and /houseexit first!");
		SendClientMessage(playerid, X11_LIGHTBLUE,"Notice: Rest may need to be edited via the DB and then /reloadhouse [sql id]");
		return 1;
	}
	makeHouse(GetPVarFloat(playerid,"HouseEntranceX"),GetPVarFloat(playerid,"HouseEntranceY"),GetPVarFloat(playerid,"HouseEntranceZ"),GetPVarFloat(playerid,"HouseExitX"),GetPVarFloat(playerid,"HouseExitY"),GetPVarFloat(playerid,"HouseExitZ"), GetPVarInt(playerid,"HouseInterior"), name, price, 1, GetPlayerInterior(playerid), GetPlayerVirtualWorld(playerid));
	return 1;
}
YCMD:reloadhouse(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Reloads a house");
		return 1;
	}
	new id;
	if (!sscanf(params, "d", id))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Reloading house");
		for(new i=0;i<sizeof(Houses);i++) {
			if(Houses[i][EHouseSQLID] == id) {
				ReloadHouse(i);
				return 1;
			}
		}
		SendClientMessage(playerid, X11_WHITE, "Failed to reload house");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /reloadhouse [sqlid]");
	}
	return 1;
}
YCMD:buyhouse(playerid, params[], help) {
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying a house");
		return 1;
	}
	new num = GetNumOwnedHouses(playerid);
	if(num >= GetPVarInt(playerid, "MaxHouses")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more houses!");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This house is already owned by someone");
		return 1;
	}
	new confirmtext[32];
	if(!sscanf(params,"s[32]",confirmtext)) {
		if(strcmp(confirmtext,"confirm", true) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must type /buyhouse confirm");
			return 1;
		}
	} else {
		format(msg, sizeof(msg), "* This house costs $%s, type /buyhouse confirm",getNumberString(Houses[house][EHouseValue]));
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		return 1;
	}
	new money = GetMoneyEx(playerid);
	if(money < Houses[house][EHouseValue]) {
		format(msg, sizeof(msg), "You do not have enough money, you need $%s more to buy this house.",getNumberString(Houses[house][EHouseValue]-money));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	GiveMoneyEx(playerid, -Houses[house][EHouseValue]);
	setHouseOwner(house, playerid);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations on your new house!");
	return 1;
}
YCMD:sellhouse(playerid, params[], help) {
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for selling your house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	new housevalue = Houses[house][EHouseValue];
	new deduction = floatround(Houses[house][EHouseValue]*0.20);
	housevalue -= deduction;
	new confirmtext[32];
	if(!sscanf(params,"s[32]",confirmtext)) {
		if(strcmp(confirmtext,"confirm", true) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must type /sellhouse confirm");
			return 1;
		}
	} else {
		format(msg, sizeof(msg), "* You will get $%s for selling this house (( /sellhouse confirm ))",getNumberString(housevalue));
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		return 1;
	}
	setHouseOwner(house, -1);
	GiveMoneyEx(playerid, housevalue);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* You have sold your house!");
	return 1;
}
YCMD:houseskin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for changing a players skin from your house");
		return 1;
	}
	new skinid;
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	if(!sscanf(params, "d", skinid)) {
		if(IsValidSkin(skinid) && IsSkinAllowed(playerid, skinid)) {
			setCharacterSkin(playerid, skinid);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /houseskin [skinid]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Hint: {FFFFFF}A list of skin IDs can be found here: http://wiki.sa-mp.com/wiki/Category:Skins");
	}
	return 1;
}
YCMD:givehouse(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for selling your house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:X, Float:Y, Float:Z;
		GetPlayerPos(user, X, Y, Z);
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, X, Y, Z)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is too far away!");
			return 1;
		}
		SendClientMessage(playerid, COLOR_DARKGREEN, "* House Given");
		SendClientMessage(user, COLOR_DARKGREEN, "* House Recieved");
		setHouseOwner(house, user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givehouse [playerid/name]");
	}
	return 1;
}
YCMD:myhouses(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists a players houses");
		return 1;
	}
	new msg[128];
	new num;
	new charid = GetPVarInt(playerid, "CharID");
	new unlocked[] = "Unlocked";
	new locked[] = "Locked";
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseOwnerSQLID] == charid) {
			format(msg, sizeof(msg), "%d. %s (ID: %d, %s)",++num,Houses[i][EHouseName],i,Houses[i][EHouseLocked]==0?unlocked:locked);
			SendClientMessage(playerid, X11_WHITE, msg);
		}
	}
	return 1;
}
YCMD:displayplayerhouses(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays a players house");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new msg[128];
		new num;
		new charid = GetPVarInt(playa, "CharID");
		new unlocked[] = "Unlocked";
		new locked[] = "Locked";
		for(new i=0;i<sizeof(Houses);i++) {
			if(Houses[i][EHouseOwnerSQLID] == charid) {
				format(msg, sizeof(msg), "%d. %s (ID: %d, %s SQL ID: %d)",++num,Houses[i][EHouseName],i,Houses[i][EHouseLocked]==0?unlocked:locked,Houses[i][EHouseSQLID]);
				SendClientMessage(playerid, X11_WHITE, msg);
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /displayplayerhouses [playerid/name]");
	}
	return 1;
}
YCMD:door(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Locks/unlocks a house door");
		return 1;
	}
	new house = getStandingHouse(playerid);
	
	if(house == -1) {
		house = getStandingExit(playerid);
		if(house == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
			return 1;
		}
	}
	if(!hasHouseKeys(playerid, house)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	new msg[128];
	if(Houses[house][EHouseLocked] == 1) {
		format(msg, sizeof(msg), "* %s unlocked the door.", GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		GameTextForPlayer(playerid, "~w~House ~g~Unlocked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		Houses[house][EHouseLocked] = 0;
	} else {
		format(msg, sizeof(msg), "* %s locked the door.",  GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		GameTextForPlayer(playerid, "~w~House ~r~Locked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		Houses[house][EHouseLocked] = 1;
	}
	saveHouseInfo(house);
	return 1;
}
hasHouseKeys(playerid, house) {
	new rent = GetPVarInt(playerid, "Rent");
	return Houses[house][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID") || rent == Houses[house][EHouseSQLID];
}
getHouseIDForPlayer(playerid) {
	new rent = GetPVarInt(playerid, "Rent");
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID") || rent == Houses[i][EHouseSQLID]) {
			return i;
		}
	}
	return -1;
}
getHouseOwner(house) {
	return Houses[house][EHouseOwnerSQLID];
}
decideSpawnAtHouse(playerid) {
	new houseid = getHouseIDForPlayer(playerid);
	if(houseid != -1) {
		new userflags = GetPVarInt(playerid, "UserFlags");
		if(userflags & EUFSpawnsAtHouse) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: You're now spawning at your house because you have selected that option.");
			SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: You can always use '/togspawn' to spawn where you left off.");
			WaitForObjectsToStream(playerid);
			SetPlayerPos(playerid, Houses[houseid][EHouseExitX],Houses[houseid][EHouseExitY],Houses[houseid][EHouseExitZ]);
			SetPlayerInterior(playerid, Houses[houseid][EHouseInterior]);
			SetPlayerVirtualWorld(playerid, houseGetVirtualWorld(houseid));
		}
	}
	return 1;
}
YCMD:toghouselock(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unlocks a players house(temporarily)");
		return 1;
	}
	new house = getStandingHouse(playerid);
	
	if(house == -1) {
		house = getStandingExit(playerid);
		if(house == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
			return 1;
		}
	}
	if(Houses[house][EHouseLocked] == 1) {
		GameTextForPlayer(playerid, "~w~House ~g~Unlocked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		Houses[house][EHouseLocked] = 0;
	} else {
		GameTextForPlayer(playerid, "~w~House ~r~Locked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		Houses[house][EHouseLocked] = 1;
	}
	saveHouseInfo(house);
	return 1;
}
YCMD:gotohouseinterior(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to the interior of a house");
		return 1;
	}
	new house;
	if(!sscanf(params, "d", house)) {
		if(house < 0 || house > sizeof(Houses) || Houses[house][EHouseSQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid House ID");
			return 1;
		}
		SetPlayerPos(playerid, Houses[house][EHouseExitX],Houses[house][EHouseExitY],Houses[house][EHouseExitZ]);
		SetPlayerInterior(playerid, Houses[house][EHouseInterior]);
		SetPlayerVirtualWorld(playerid, houseGetVirtualWorld(house));
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotohouseinterior [houseid]");
	}
	return 1;
}
YCMD:sethousename(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Updates a houses description");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	new name[64],name_esc[(sizeof(name)*2)+1];
	if(!sscanf(params,"s[64]", name)) {
		query[0] = 0;
		mysql_real_escape_string(name, name_esc);
		format(Houses[house][EHouseName],64,"%s",name);
		format(query, sizeof(query), "UPDATE `houses` SET `name` = \"%s\" WHERE `id` = %d", name_esc, Houses[house][EHouseSQLID]);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");		
		new labeltext[128];
		getHouseTextLabel(house, labeltext, sizeof(labeltext));
		UpdateDynamic3DTextLabelText(Houses[house][EHouseTextLabel],0x00FF00FF, labeltext);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sethousename [name]");
		SendClientMessage(playerid, X11_WHITE, "You must be standing at the house pickup to do this.");
	}
	return 1;
}

public ReloadHouse(houseid) {
	query[0] = 0;
	//format(query, sizeof(query), "SELECT `houses`.`id`,`houses`.`X`,`houses`.`Y`,`houses`.`Z`,`EX`,`EY`,`EZ`,`houses`.`interior`,`houses`.`name`,`price`,`owner`,`c1`.`username`,`locked`,`rent` FROM `houses`  LEFT JOIN `characters` AS `c1` ON `houses`.`owner` = `c1`.`id` WHERE `houses`.`id` = %d",Houses[houseid][EHouseSQLID]);
	format(query, sizeof(query), "SELECT `houses`.`id`,`houses`.`X`,`houses`.`Y`,`houses`.`Z`,`EX`,`EY`,`EZ`,`houses`.`interior`,`houses`.`name`,`price`,`owner`,`c1`.`username`,`locked`,`houses`.`rent`,COUNT(`hf`.`house`),`houses`.`pickupint`,`houses`.`pickupvw` FROM `houses` LEFT JOIN `characters` AS `c1` ON `houses`.`owner` = `c1`.`id` LEFT JOIN `housefurniture` `hf` ON `hf`.house = `houses`.`id` WHERE `houses`.`id` = %d", Houses[houseid][EHouseSQLID]);
	Houses[houseid][EHouseSQLID] = 0;
	DestroyDynamicPickup(Houses[houseid][EHousePickup]);
	DestroyDynamic3DTextLabel(Houses[houseid][EHouseTextLabel]);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadHouses", "");
}
saveHouseInfo(house) {
	query[0] = 0;
	format(query, sizeof(query), "UPDATE `houses` SET `locked` = %d,`owner` = %d WHERE `id` = %d",Houses[house][EHouseLocked],Houses[house][EHouseOwnerSQLID],Houses[house][EHouseSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
getStandingHouse(playerid, Float:radi = 2.0) {
	for(new i=0;i<sizeof(Houses);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ])) {
			if(GetPlayerInterior(playerid) == Houses[i][EHousePickupInt] && GetPlayerVirtualWorld(playerid) == Houses[i][EHousePickupVW]) {
				return i;
			}
		}
	}
	return -1;
}
getStandingExit(playerid, Float:radi = 5.0) {
	for(new i=0;i<sizeof(Houses);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, Houses[i][EHouseExitX],Houses[i][EHouseExitY],Houses[i][EHouseExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == houseGetVirtualWorld(i)) {
				return i;
			}
		}
	}
	return -1;
}
sendMessageToHouses(playerid, Float:radi = 30.0, msg[], color) {
	for(new i=0;i<sizeof(Houses);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ])) {
			SendAreaMessage(radi, Houses[i][EHouseExitX],Houses[i][EHouseExitY],Houses[i][EHouseExitZ], houseGetVirtualWorld(i), msg, color);
		} else if(IsPlayerInRangeOfPoint(playerid, radi, Houses[i][EHouseExitX],Houses[i][EHouseExitY],Houses[i][EHouseExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == houseGetVirtualWorld(i)) {
				SendAreaMessage(radi, Houses[i][EHouseX],Houses[i][EHouseY],Houses[i][EHouseZ], Houses[i][EHousePickupVW], msg, color);
			}
		}
	}
	return 1;
}
setHouseOwner(houseid, playerid) {
	query[0] = 0;
	if(playerid == -1 || playerid == INVALID_PLAYER_ID) {
		Houses[houseid][EHouseOwnerSQLID] = 0;
	} else {
		Houses[houseid][EHouseOwnerSQLID] = GetPVarInt(playerid, "CharID");
	}
	if(HouseHasSafe(houseid)) {
		DeleteHouseSafe(houseid);
	}
	Houses[houseid][EHouseLocked] = 1;
	DestroyHouseObjects(houseid);
	new labeltext[128];
	format(query, sizeof(query), "UPDATE `houses` SET `owner` = %d WHERE `id` = %d", GetPVarInt(playerid, "CharID"), Houses[houseid][EHouseSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	DestroyDynamicPickup(Houses[houseid][EHousePickup]);
	format(Houses[houseid][EHouseOwnerName],MAX_PLAYER_NAME,"%s",GetPlayerNameEx(playerid, ENameType_CharName));
	getHouseTextLabel(houseid, labeltext, sizeof(labeltext));
	UpdateDynamic3DTextLabelText(Houses[houseid][EHouseTextLabel],0x00FF00FF, labeltext);
	Houses[houseid][EHousePickup] = CreateDynamicPickup(Houses[houseid][EHouseOwnerSQLID]>0?19198:1273,16,Houses[houseid][EHouseX], Houses[houseid][EHouseY], Houses[houseid][EHouseOwnerSQLID]>0 ? Houses[houseid][EHouseZ]+0.50 : Houses[houseid][EHouseZ], Houses[houseid][EHousePickupVW], Houses[houseid][EHousePickupInt]);
}
getHouseTextLabel(houseid, dst[], dstlen) {
	if(Houses[houseid][EHouseOwnerSQLID] == 0) {
		format(dst, dstlen, "{FFFFFF}[{FF0000}PROPERTY{FFFFFF}]\n%s\nFor Sale\nPrice: {FFFFFF}[{FF0000}$%s{FFFFFF}]",Houses[houseid][EHouseName],getNumberString(Houses[houseid][EHouseValue]));
	} else {
		if(Houses[houseid][EHouseRentPrice] != 0)  {
			//format(dst, dstlen, "* %s\nOwner: {FF0000}%s\n{00FF00}Rent: {FF0000}$%s",Houses[houseid][EHouseName],Houses[houseid][EHouseOwnerName],getNumberString(Houses[houseid][EHouseRentPrice]));
			format(dst, dstlen, "{FFFFFF}[{FF0000}PROPERTY{FFFFFF}]\n%s\nRent: {FFFFFF}[{FF0000}$%s{FFFFFF}]",Houses[houseid][EHouseName],getNumberString(Houses[houseid][EHouseRentPrice]));
		} else {
			//format(dst, dstlen, "* %s\nOwner: {FF0000}%s",Houses[houseid][EHouseName],Houses[houseid][EHouseOwnerName]);
			format(dst, dstlen, "{FFFFFF}[{FF0000}PROPERTY{FFFFFF}]\n%s",Houses[houseid][EHouseName]);
		}
	}
}
YCMD:renters(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set the rent price for a house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	format(query, sizeof(query), "SELECT `username`,`id` FROM `characters` WHERE `renthouse` = %d",Houses[house][EHouseSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "OnCheckRenters", "dd",playerid, house);
	return 1;
}
forward OnCheckRenters(playerid, house);
public OnCheckRenters(playerid, house) {
	new rows, fields;
	cache_get_data(rows, fields);
	new msg[128];
	new id_string[128];
	new charid;
	SendClientMessage(playerid, X11_WHITE, "*** Renters ***");
	for(new i=0;i<rows;i++) {		
	
		cache_get_row(i, 1, id_string);
		charid = strval(id_string);
		
		cache_get_row(i, 0, id_string);
		format(msg, sizeof(msg), "* %s ID: %d",id_string,charid);
		new id = findCharBySQLID(charid);
		SendClientMessage(playerid, id!=INVALID_PLAYER_ID?X11_TOMATO_2:COLOR_LIGHTBLUE, msg);
	}
	SendClientMessage(playerid, X11_WHITE, "Do /evict [id] to evict someone");
	SendClientMessage(playerid, X11_WHITE, "If the players name is red, it means they are online");
	SendClientMessage(playerid, X11_WHITE, "Do /evictall to evict everyone.");
	return 1;
}
YCMD:togspawn(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unlocks a players house(temporarily)");
		return 1;
	}
	new userflags = GetPVarInt(playerid, "UserFlags");
	new houseid = getHouseIDForPlayer(playerid);
	
	if(houseid == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't own any houses!");
		return 1;
	}
	if(userflags & EUFSpawnsAtHouse) {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: You will now spawn where you last left off.");
		userflags &= ~EUFSpawnsAtHouse;
	} else {
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "[INFO]: You will now spawn at your house.");
		userflags |= EUFSpawnsAtHouse;
	}
	SetPVarInt(playerid, "UserFlags", userflags);
	return 1;
}
YCMD:evict(playerid, params[], help) {
	new charid;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Evict someone from your house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	if(!sscanf(params,"d",charid)) {
		if(charid < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Character ID!");
			return 1;
		}
		new p = findCharBySQLID(charid);
		if(p == INVALID_PLAYER_ID) {
			format(query, sizeof(query), "UPDATE `characters` SET `renthouse` = -1 WHERE `renthouse` = %d AND `id` = %d",Houses[house][EHouseSQLID],charid);
			mysql_function_query(g_mysql_handle, query, true, "OnRemoveRenter", "dd",playerid, house);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "* Player Evicted!");
			SetPVarInt(p, "Rent", 0);
			SendClientMessage(p, COLOR_DARKGREEN, "* You have been evicted from your house");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /evict [charid]");
	}
	return 1;
}
YCMD:evictall(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Evict someone from your house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "Rent") == Houses[house][EHouseSQLID]) {
				SetPVarInt(i, "Rent", 0);
				SendClientMessage(i, COLOR_DARKGREEN, "* You have been evicted from your house");
			}
		}
	}
	format(query, sizeof(query), "UPDATE `characters` SET `renthouse` = -1 WHERE `renthouse` = %d",Houses[house][EHouseSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	SendClientMessage(playerid, COLOR_DARKGREEN, "* All players have been evicted!");
	return 1;
}
forward OnRemoveRenter(playerid, house);
public OnRemoveRenter(playerid, house) {
	new numrows = mysql_affected_rows();
	if(numrows == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "* This person is not renting your house!");
	} else {
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Renter Removed!");
	}
}
YCMD:setrent(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Set the rent price for a house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] != GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your house!");
		return 1;
	}
	new price;
	if(!sscanf(params, "d", price)) {
		if(price < 0 || price > 5000) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		setHouseRent(house, price);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Rent price updated!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setrent [price]");
		SendClientMessage(playerid, X11_WHITE, "A rent price of 0 sets the house to be not rentable");
	}
	return 1;
}
getRentPrice(playerid) {
	new r = GetPVarInt(playerid, "Rent");
	if(r != 0) {
		new h = houseIDFromSQLID(r);
		if(h != -1) {
			return Houses[h][EHouseRentPrice];
		}
	}
	return 0;
}
YCMD:rent(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Rent a house");
		return 1;
	}
	new house = getStandingHouse(playerid);
	if(house == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a house");
		return 1;
	}
	if(Houses[house][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot rent your own house!");
		return 1;
	}
	if(GetPVarInt(playerid, "Rent") != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are already renting a house! Do /unrent!");
		return 1;
	}
	if(Houses[house][EHouseRentPrice] == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This house is not for rent!");
		return 1;
	}
	if(GetMoneyEx(playerid) < Houses[house][EHouseRentPrice]) {
		SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough money!");
		return 1;
	}
	GiveMoneyEx(playerid, -Houses[house][EHouseRentPrice]);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations! You are now a tennant at this house!");
	SetPVarInt(playerid, "Rent", Houses[house][EHouseSQLID]);
	return 1;
}
YCMD:unrent(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Cancel your rent");
		return 1;
	}
	if(GetPVarInt(playerid, "Rent") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't renting a house!");
		return 1;
	}
	SetPVarInt(playerid, "Rent", 0);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Rent cancelled");
	return 1;
}
setHouseRent(houseid, price) {
	new labeltext[128];
	format(query, sizeof(query), "UPDATE `houses` SET `rent` = %d WHERE `id` = %d", price, Houses[houseid][EHouseSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");		
	DestroyDynamicPickup(Houses[houseid][EHousePickup]);
	Houses[houseid][EHouseRentPrice] = price;
	getHouseTextLabel(houseid, labeltext, sizeof(labeltext));
	UpdateDynamic3DTextLabelText(Houses[houseid][EHouseTextLabel],0x00FF00FF, labeltext);
	Houses[houseid][EHousePickup] = CreateDynamicPickup(Houses[houseid][EHouseOwnerSQLID]>0?1272:1273,16,Houses[houseid][EHouseX], Houses[houseid][EHouseY], Houses[houseid][EHouseZ], Houses[houseid][EHousePickupVW], Houses[houseid][EHousePickupInt]);
}
IsHouseLocked(house) {
	return Houses[house][EHouseLocked] == 1;
}
SetHouseStation(house, radio) {
	Houses[house][EHouseRadioIndex] = radio;
	new url[128];
	getRadioURL(Houses[house][EHouseRadioIndex], url, sizeof(url));
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(getStandingExit(i, 150.0) == house) {
				StopAudioStreamForPlayer(i);
				if(radio != -1)
					PlayAudioStreamForPlayer(i, url);
			}
		}
	}
}
getHouseRadio(house) {
	return Houses[house][EHouseRadioIndex];
}

getHouseTax(playerid) {
	new tax;
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseOwnerSQLID] == GetPVarInt(playerid, "CharID")) {
			tax += floatround(Houses[i][EHouseValue] * HOUSE_TAX_RATE);
		}
	}
	return tax;
}
houseIDFromSQLID(sqlid) {
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseSQLID] == sqlid) {
			return i;
		}
	}
	return -1;
}
getHouseSQLID(house) {
	return Houses[house][EHouseSQLID];
}
sellInactiveHouses() {
	mysql_function_query(g_mysql_handle, "SELECT `houses`.`id` FROM `houses` INNER JOIN `characters` ON `houses`.`owner` = `characters`.`id`  INNER JOIN `accounts` ON `characters`.`accountid` = `accounts`.`id` WHERE `houses`.`owner` != 0 AND `accounts`.`lastlogin` < DATE_SUB(CURDATE(),INTERVAL 4 WEEK)", true, "OnCheckInactiveHouses", "");	
	
	//check for houses owned by non-existant characters
	mysql_function_query(g_mysql_handle, "SELECT `h`.`id` FROM `houses`  AS `h` LEFT JOIN `characters` AS `c` ON `c`.`id` = `h`.`owner` WHERE `c`.`username` IS NULL AND `h`.`owner` != 0", true, "OnCheckInactiveHouses", "");	
}
forward OnCheckInactiveHouses();
public OnCheckInactiveHouses() {
	new rows, fields;
	new id, id_string[128];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		id = strval(id_string);
		new hid = houseIDFromSQLID(id);
		if(hid != -1) {
			setHouseOwner(hid, INVALID_PLAYER_ID) ;
		}
	}
}
playerOwnsHouse(houseid, playerid) {
	new charid = GetPVarInt(playerid, "CharID");
	return Houses[houseid][EHouseOwnerSQLID] == charid;
}
getPlayerPropertiesValue(playerid) {
	new value;
	new charid = GetPVarInt(playerid, "CharID");
	for(new i=0;i<sizeof(Houses);i++) {
		if(Houses[i][EHouseOwnerSQLID] == charid) {
			value += Houses[i][EHouseValue];
		}
	}
	return value;
}