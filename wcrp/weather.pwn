/* Snow Defines */
#define MAX_SNOW_OBJECTS 4
#define SNOW_MDL_ID 18864
/* End of Snow Defines */
 
enum WeatherType (<<= 1) {
	EWeatherType_Clear = 1, //clear/blue sky/mild clouds
	EWeatherType_Cloudy,
	EWeatherType_Stormy,
	EWeatherType_Foggy,
	EWeatherType_HeatWave, 
	EWeatherType_SandStorm,
	EWeatherType_Rain,
	EWeatherType_Haze,
	EWeatherType_LightClouds, //don't mix with cloudy
	EWeatherType_Dark,
};
enum EGTAWeatherInfo {
	WeatherType:EGTAWeatherType,
	EGTAWeatherID,
}
new GTAWeather[][EGTAWeatherInfo] = {
{EWeatherType_Clear,0},
{EWeatherType_Clear,1},
{EWeatherType_Clear,2},
{EWeatherType_Clear,3},
{EWeatherType_Clear|EWeatherType_Cloudy,4},
{EWeatherType_Clear,5},
{EWeatherType_Clear,6},
{EWeatherType_Clear|EWeatherType_Cloudy,7},
{EWeatherType_Stormy,8},
{EWeatherType_Foggy,9},
{EWeatherType_Clear,10},
{EWeatherType_HeatWave,11},
{EWeatherType_Cloudy,12},
{EWeatherType_Clear,13},
{EWeatherType_Clear,14},
{EWeatherType_Clear,15},
{EWeatherType_Stormy,16},
{EWeatherType_HeatWave,17},
{EWeatherType_HeatWave,18},
{EWeatherType_SandStorm,19},
{EWeatherType_Haze|EWeatherType_Cloudy,20},
{EWeatherType_Clear,23},
{EWeatherType_Clear,24},
{EWeatherType_LightClouds,25},
{EWeatherType_LightClouds,26},
{EWeatherType_LightClouds,27},
{EWeatherType_Clear,28},
{EWeatherType_Dark, 30},
{EWeatherType_Dark|EWeatherType_LightClouds, 31},
{EWeatherType_Dark|EWeatherType_Haze, 32}
};
enum EWeatherInfo {
	EWeatherChance, //chance of this weather happening
	EWeatherTimeOffset, //change the players time by this amount
	WeatherType:EW_WeatherType,
	EWeatherMinTime, //min hour of day which this can happen(-1 = always)
	EWeatherMaxTime, //max hour of day which this can happen(-1 = always)
	EWeatherZoneIndex,
};

enum EWeatherZones {
	Float:EWZ_MaxX,
	Float:EWZ_MinX,
	Float:EWZ_MaxY,
	Float:EWZ_MinY,
	EWZ_Name[32],
	EWZ_DynamicZone,
	EWZ_CurrentWeather,
	EWZ_CurrentWeatherID,
	EWZ_WeatherLength, //number of paydays
	EWZ_CurWeatherLength,
};

new WeatherZones[][EWeatherZones] = {
	/*
	{3328.195, 2008.595, -1039.331, -3036.248,"East Los Santos",0,-1,-1,2,0},
	{1401.345, 105.1009, -1564.835, -2861.08, "Verona Beach",0,-1,-1,3,0},
	{2223.813, 1421.414, -1425.259, -2013.472, "Unity", 0, -1, -1,1,0}
	*/
	/*
	{128.90625, -2777.34375, 2976.5625, -832.03125, "Los Santos", 0, 1, 1, 2, 0},
	{-3000.0, -3070.3125, 199.21875, -1019.53125, "Country Side West", 0, 1, 1, 2, 0},
	{-175.78125, -832.03125, 3000.0, 386.71875, "Country Side North", 0, 1, 1, 2, 0}
	MinX, MinY, MaxX, MaxY
	*/
	{2976.5625, 128.90625, -832.03125, -2777.34375, "Los Santos", 0, 1, 1, 2, 0},
	{199.21875, -3000.0, -1019.53125, -3070.3125, "Country Side West", 0, 1, 1, 2, 0},
	{3000.0, -832.03125, 386.71875, -832.03125, "Country Side North", 0, 1, 1, 2, 0}
	
};
new WeatherInfo[][EWeatherInfo] = {
//{50,-1,EWeatherType_Clear,-1,-1,0},
/*
{15,-1,EWeatherType_Stormy,5,8,0}, //East LS
{55,-1,EWeatherType_Foggy,0,8,1}, //Verona Beach
{45,-1,EWeatherType_Cloudy,12,15,1}, //Verona Beach
{7,-1,EWeatherType_Foggy,15,20,2}, //Unity
{15,-1,EWeatherType_HeatWave,10,15,2} //Unity
*/
{10,-1,EWeatherType_LightClouds,5,17,0}, //Los Santos
{15,-1,EWeatherType_HeatWave,-1,-1,0}, //Los Santos
{4,-1,EWeatherType_Stormy,21,9,0}, //Los Santos
{8,-1,EWeatherType_Foggy,21,9,0}, //Los Santos
{4,-1,EWeatherType_Stormy,18,9,1}, //Country Side West
{25,-1,EWeatherType_Dark,5,8,1}, //Country Side West
{15,-1,EWeatherType_HeatWave,11,17,1}, //Country Side West
{8,-1,EWeatherType_Foggy,18,9,1}, //Country Side West
{4,-1,EWeatherType_Stormy,18,9,2}, //Country Side North
{25,-1,EWeatherType_Dark,5,8,2}, //Country Side North
{15,-1,EWeatherType_HeatWave,11,17,2}, //Country Side North
{8,-1,EWeatherType_Foggy,18,9,2} //Country Side North
};

enum ESeason_Types {
	ESeasonType_Autumn,
	ESeasonType_Winter,
	ESeasonType_Spring,
	ESeasonType_Summer,
}
enum EWeatherInfo_Seasons {
	E_SeasonName[32],
	ESeason_Types:E_CurrentSeason,
}
new Seasons[][EWeatherInfo_Seasons] = {
	{"Autumn", ESeasonType_Autumn},
	{"Winter", ESeasonType_Winter},
	{"Spring", ESeasonType_Spring},
	{"Summer", ESeasonType_Summer}
};
enum ESeason_Months {
	ESeason_Types:E_CurrentSeason,
	ESeason_Month,
	E_MinTemperature,
	E_MaxTemperature,
}
new SeasonMonths[][ESeason_Months] = {
	{ESeasonType_Autumn, 9, 15, 22},
	{ESeasonType_Autumn, 10, 15, 22},
	{ESeasonType_Autumn, 11, 15, 22},
	{ESeasonType_Winter, 12, -1, 13}, //December snow
	{ESeasonType_Winter, 1, 6, 13},
	{ESeasonType_Winter, 2, 8, 13},
	{ESeasonType_Winter, 3, 9, 13},
	{ESeasonType_Spring, 4, 20, 24},
	{ESeasonType_Spring, 5, 20, 24},
	{ESeasonType_Summer, 6, 24, 42},
	{ESeasonType_Summer, 7, 24, 42},
	{ESeasonType_Summer, 8, 24, 42}
};

new servweather;
new servtemperature;
new snowObject[MAX_PLAYERS][MAX_SNOW_OBJECTS];
new WeatherSnowing[MAX_PLAYERS];

weatherOnGameModeInit() {
	for(new i=0;i<sizeof(WeatherZones);i++) {
		WeatherZones[i][EWZ_DynamicZone] = CreateDynamicRectangle(WeatherZones[i][EWZ_MinX],WeatherZones[i][EWZ_MinY],WeatherZones[i][EWZ_MaxX],WeatherZones[i][EWZ_MaxY],0,0);
		changeWeather(i);
	}
	servweather = getRandomWeather();
	SetWeather(servweather);
	updateTemperature();
}
getRandomWeather() {
	if(!itsCold()) {
		new allowedweather[] = {0,1,2,3,4,5,6,7,10,11};
		new weather = random(sizeof(allowedweather)-1);
		return allowedweather[weather];
	} else {
		new allowedweather[] = {7,31};
		new weather = random(sizeof(allowedweather)-1);
		return allowedweather[weather];
	}
}
findWeatherZoneByArea(areaid) {
	for(new i=0;i<sizeof(WeatherZones);i++) {
		if(WeatherZones[i][EWZ_DynamicZone] == areaid) {
			return i;
		}
	}
	return -1;
}
weatherOnPlayerEnterDynamicArea(playerid, areaid) {
	new z = findWeatherZoneByArea(areaid);
	if(z != -1) {
		if(WeatherZones[z][EWZ_CurrentWeatherID] != -1) {
			if(!itsCold()) {
				SetPlayerWeather(playerid, WeatherZones[z][EWZ_CurrentWeatherID]);
				weatherInVehicleDecide(playerid);
			}
		}
	}
}
itsCold() { //Obvious note: If the server temperature is below 0, it returns 1
	if(servtemperature < 0) {
		return 1;
	}
	return 0;
}
/* Snow */
weatherOnPlayerConnect(playerid) {
	checkSnow(playerid);
}
destroySnow(playerid) {
	for(new i = 0; i < MAX_SNOW_OBJECTS; i++) {
		#if debug
		printf("Debug: Called destroySnow->DestroyDynamicObject.");
		#endif
		DestroyDynamicObject(snowObject[playerid][i]);
	}
	if(WeatherSnowing[playerid]) {
		WeatherSnowing[playerid] = 0;
	}
	return 1;
}
moveSnowTimer() {
	foreach(Player, i) {
		if(numPlayersOnServer() < 40) {
			checkSnow(i);
		} else {
			if(isSnowing(i)) {
				destroySnow(i);
			}
		}
	}
	return 1;
}
checkSnow(playerid) {
	new interior = GetPlayerInterior(playerid);
	if(interior < 1) {
		if(isSnowing(playerid) && itsCold()) {
			createSnow(playerid);
		} else {
			checkIfItShouldSnow(playerid);
		}
	}
	return 1;
}
checkIfItShouldSnow(playerid) {
	if(itsCold()) {
		SetPlayerWeather(playerid, 9);
		createSnow(playerid);
	} else {
		if(isSnowing(playerid)) {
			destroySnow(playerid);
		}
	}
	return 1;
}
createSnow(playerid) {
	new Float: X, Float: Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	destroySnow(playerid); //Destroy it so it first anyways so it doesn't duplicate from the login
	if(GetPlayerInterior(playerid) < 1) {
		for(new i = 0; i < MAX_SNOW_OBJECTS; i++) {
			snowObject[playerid][i] = CreateDynamicObject(SNOW_MDL_ID, X + random(1), Y + random(1), Z, 0, -1, 0, playerid);
		}
		if(!WeatherSnowing[playerid]) {
			WeatherSnowing[playerid] = 1;
		}
	}
	UpdatePlayerObjectsAtPos(playerid);
	return 1;
}
isSnowing(playerid) {
	if(WeatherSnowing[playerid]) {
		return 1;
	}
	return 0;
}
/* Snow End */
returnWeatherAtPlayersDynArea(playerid) {
	new z = getPlayerWeatherZone(playerid);
	if(z != -1) {
		if(WeatherZones[z][EWZ_CurrentWeatherID] != -1) {
			new WeatherID = WeatherZones[z][EWZ_CurrentWeatherID];
			return WeatherID;
		}
	}
	return -1;
}
/*
weatherInVehicleDecide(playerid) {
	new WeatherID = returnWeatherAtPlayersDynArea(playerid);
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		if(itsRainWeather(WeatherID)) {
			if(vehicleWipersOn(carid)) {
				SetPlayerWeather(playerid, 9);
			} else {
				SetPlayerWeather(playerid, WeatherID);
			}
		}
	} else {
		if(WeatherID != -1) {
			SetPlayerWeather(playerid, WeatherID);
		} else {
			SetPlayerWeather(playerid, servweather);
		}
	}
}
*/
weatherInVehicleDecide(playerid) {
	new WeatherID = returnWeatherAtPlayersDynArea(playerid);
	if(!itsRainWeather(WeatherID)) {
		return 1; //Don't do anything
	}
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		if(vehicleWipersOn(carid)) {
			SetPlayerWeather(playerid, 9);
		} else {
			SetPlayerWeather(playerid, WeatherID);
		}
	} else {
		if(WeatherID != -1) {
			SetPlayerWeather(playerid, WeatherID);
		} else {
			SetPlayerWeather(playerid, servweather);
		}
	}
	return 1;
}
itsRainWeather(WeatherID) {
	for(new i=0;i<sizeof(GTAWeather);i++) {
		if(GTAWeather[i][EGTAWeatherID] == WeatherID) {
			if(GTAWeather[i][EGTAWeatherType] & EWeatherType_Stormy) {
				return 1;
			}
		}
	}
	return 0;
}
weatherFlagFound(WeatherType:type) {
	new n;
	for(new i=0;i<sizeof(GTAWeather);i++) {
		if(GTAWeather[i][EGTAWeatherType] & type) {
			n++;
		}
	}
	return n;
}
getGTARandomWeather(WeatherType:type) {
	new r;
	if(weatherFlagFound(type) == 0) {
		return -1;
	}
	do {
		r = random(sizeof(GTAWeather));
		
	} while(~GTAWeather[r][EGTAWeatherType] & type);
	return GTAWeather[r][EGTAWeatherID];
}
weatherOnPlayerLeaveDynamicArea(playerid, areaid) {
	#pragma unused areaid
	if(!itsCold()) {
		SetPlayerWeather(playerid, servweather);
	}
}
changeWeather(index) {
	new n = numWeatherZones(index);
	if(n == 0) return -1;
	new r = random(n);
	new i = getWeather(index,r+1);
	if(i != -1) {
		if(shouldWeatherHappen(i)) {
			setAreaWeather(index, i);
			return i;
		}
	}
	setAreaWeather(index, -1);
	updateTemperature();
	return -1;
}
updateTemperature() {
	new Year, Month, Day;
	getdate(Year, Month, Day);
	new index = getMonthIndex(Month);
	setTemperature(SeasonMonths[index][E_MinTemperature], SeasonMonths[index][E_MaxTemperature]);
	return 1;
}
setTemperature(tempmin, tempmax) {
	new temperature = RandomEx(tempmin, tempmax);
	servtemperature = temperature;
	return 1;
}
getMonthIndex(month) {
	for(new i=0;i<sizeof(SeasonMonths);i++) {
		if(SeasonMonths[i][ESeason_Month] == month) {
			return i;
		}
	}
	return -1;
}
getActualSeason(month) {
	for(new i=0;i<sizeof(SeasonMonths);i++) {
		if(SeasonMonths[i][ESeason_Month] == month) {
			return _:SeasonMonths[i][E_CurrentSeason];
		}
	}
	return -1;
}
shouldWeatherHappen(index) {
	new r = random(100);
	if(r <= WeatherInfo[index][EWeatherChance]) {
		new hour,minutes,sec;
		gettime(hour,minutes,sec);
		if((hour < WeatherInfo[index][EWeatherMinTime] || WeatherInfo[index][EWeatherMaxTime] == -1) || (hour > WeatherInfo[index][EWeatherMaxTime] && WeatherInfo[index][EWeatherMaxTime] == -1)) {
			return 1;
		}
	}
	return 0;
}
YCMD:swapweather(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Update the areas weather");
		return 1;
	}
	swapWeather();
	return 1;
}
shouldChangeWeather(zone) {
	if(WeatherZones[zone][EWZ_CurrentWeatherID] != -1) {
		if(WeatherZones[zone][EWZ_CurWeatherLength]++ > WeatherZones[zone][EWZ_WeatherLength]) {
			return 1;
		} else return 0;
	}
	return 1;
}
swapWeather() {
	for(new i=0;i<sizeof(WeatherZones);i++) {
		if(shouldChangeWeather(i)) {
			changeWeather(i);
		}
	}
}
getPlayerWeatherZone(playerid) {
	for(new i=0;i<sizeof(WeatherZones);i++) {
		if(WeatherZones[i][EWZ_DynamicZone] != 0) {
			if(IsPlayerInDynamicArea(playerid, WeatherZones[i][EWZ_DynamicZone])) {
				return i;
			}
		}
	}
	return -1;
}
setAreaWeather(index, weather) {
	new w = -1;
	if(weather != -1) {
		w = getGTARandomWeather(WeatherInfo[weather][EW_WeatherType]);
	}
	WeatherZones[index][EWZ_CurrentWeatherID] = w;
	WeatherZones[index][EWZ_CurWeatherLength] = 0;
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(IsPlayerInDynamicArea(i, WeatherZones[index][EWZ_DynamicZone])) {
				if(WeatherZones[index][EWZ_CurrentWeatherID] == -1) {
					SetPlayerWeather(i, servweather);
				} else {
					SetPlayerWeather(i, WeatherZones[index][EWZ_CurrentWeatherID]);
				}
				weatherInVehicleDecide(i);
			}
		}
	}
}
getWeather(weather, index) {
	new r;
	for(new i=0;i<sizeof(WeatherInfo);i++) {
		if(WeatherInfo[i][EWeatherZoneIndex] == weather) {
			if(++r == index) {
				return i;
			}
		}
	}
	return -1;
}
numWeatherZones(index) {
	new r;
	for(new i=0;i<sizeof(WeatherInfo);i++) {
		if(WeatherInfo[i][EWeatherZoneIndex] == index) {
			r++;
		}
	}
	return r;
}
weatherOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	destroySnow(playerid);
}

task UpdateWeatherTimer[1000] () {
	moveSnowTimer();
}