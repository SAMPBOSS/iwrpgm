#define MAX_GATES 2000
#define GATE_CLOSE_TIME 15
#define MAX_MAP_OBJECTS 15000

enum EObjectsInfo {
	objSQLID,
	objID, //Model ID
	Float:objPosX,
	Float:objPosY,
	Float:objPosZ,
	Float:objAngX,
	Float:objAngY,
	Float:objAngZ,
	objVW,
	objIntID,
	objStreamID, //Streamer ID so we can destroy it later
};
new MapObjects[MAX_MAP_OBJECTS][EObjectsInfo];

enum EGateInfo {
	EGateCmd[32],
	EGateModelID,
	Float:EGateX, 
	Float:EGateY,
	Float:EGateZ, 	
	Float:EGateRotX,
	Float:EGateRotY,
	Float:EGateRotZ,
	FactionType:EGateFactionType,
	EGateJob,
	EGateState, //0 if closed, 1 if opened, 2 if disabled, 3 if forced opened
	Float:EOpenPosX,
	Float:EOpenPosY,
	Float:EOpenPosZ,
	EGateObjectID,
	EGateRoleText[64],
	Float:EGateFindDistance,
	Float:EGateMoveSpeed,
	Float:EGateOpenRotX,
	Float:EGateOpenRotY,
	Float:EGateOpenRotZ,
	EGateInterior,
	EGateVW,
	EGateFaction,
	EGateLastMoveTime, //unix timestamp of last move time
	EGateHouseSQLID, //SQLID of the house its associated with
};
new Gates[MAX_GATES][EGateInfo];

enum EGateTextInfo {
	EGateName[64],
	EGateMatText[64],
	EGateMaterialIndex,
	EGateMaterialSize,
	EGateFont[32],
	EGateSize,
	EGateBold,
	EGateFontColour,
	EGateBackColour, 
	EGateTextAlignment,
}
new GateTextureInfo[][EGateTextInfo] = {{"lspd1","LSPD",0,OBJECT_MATERIAL_SIZE_64x64,"Arial",24,1,0xFFFFFFFF, 0, 0}};

setupGateTextures() {
	
	for(new i=0;i<sizeof(GateTextureInfo);i++) {
		new index = findGateByName(GateTextureInfo[i][EGateName]);
		if(index != -1) {
			SetObjectMaterialText(Gates[index][EGateObjectID], GateTextureInfo[i][EGateMatText],
			GateTextureInfo[i][EGateMaterialIndex],GateTextureInfo[i][EGateMaterialSize],
			GateTextureInfo[i][EGateFont],GateTextureInfo[i][EGateSize],GateTextureInfo[i][EGateBold], GateTextureInfo[i][EGateFontColour],
			GateTextureInfo[i][EGateBackColour],GateTextureInfo[i][EGateTextAlignment]);
		}
	}
}
#pragma unused setupGateTextures

findGateByName(name[]) {
	for(new i=0;i<sizeof(Gates);i++) {
		if(strcmp(Gates[i][EGateCmd],name) == 0) {
			return i;
		}
	}
	return -1;
}
#pragma unused findGateByName

findClosestGate(playerid) {
	new Float:X, Float:Y, Float:Z;
	GetPlayerPos(playerid, X, Y, Z);
	for(new i=0;i<sizeof(Gates);i++) {
		if(IsPlayerInRangeOfPoint(playerid, Gates[i][EGateFindDistance], Gates[i][EGateX],Gates[i][EGateY],Gates[i][EGateZ])) {
			return i;
		}
	}
	return -1;
}
YCMD:lockgate(playerid, params[], help) {
	new gate = findClosestGate(playerid);
	if(gate == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too far away.");
		return 1;
	}
	switch(Gates[gate][EGateState]) {
		case 0: { //closed
			Gates[gate][EGateState] = 2;
		}
		case 1: { //opened
			Gates[gate][EGateState] = 3;
		}
		case 2: {
			Gates[gate][EGateState] = 0;
		}
		case 3: {
			Gates[gate][EGateState] = 1;
		}	
	}
	SendClientMessage(playerid, COLOR_DARKGREEN, "Gate Status set!");
	return 1;
}
YCMD:gate(playerid, params[], help) {
	new gate = findClosestGate(playerid);
	if(gate == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are too far away.");
		return 1;
	}
	tryOpenGate(playerid, gate);
	return 1;
}

CheckGates() {
	for(new i=0;i<sizeof(Gates);i++) {
		if(Gates[i][EGateState] == 1) { //opened
			if(Gates[i][EGateLastMoveTime]+GATE_CLOSE_TIME < gettime()) {
				toggleGate(i);
			}
		}
	}
}

tryOpenGate(playerid, gate) {
	new job = GetPVarInt(playerid, "Job");
	new aoverride = (EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter)?1:0;
	new msg[128];
	if(IsPlayerHoldingGateHacker(playerid)) {
		format(msg, sizeof(msg), "HQ: Message from Gate Security System: {EE0000}Forced Entry at gate: %s",Gates[gate][EGateCmd]);
		SendCopMessage(TEAM_BLUE_COLOR, msg);
		aoverride = 1;
	}
	if(Gates[gate][EGateJob] != -1 && !aoverride) {
		if(job != Gates[gate][EGateJob]) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have access to that gate");
			return 1;
		}
	}
	if(Gates[gate][EGateFactionType] != EFactionType_None && !aoverride) {
		if(Gates[gate][EGateFactionType] != getPlayerFactionType(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have access to that gate");
			return 1;
		}
	}
	if(Gates[gate][EGateFaction] != 0 && !aoverride) {
		new faction = GetPVarInt(playerid, "Faction");
		if(faction != Gates[gate][EGateFaction]) {
			SendClientMessage(playerid, X11_TOMATO_2, "You don't have access to that gate");
			return 1;
		}
	}
	if(Gates[gate][EGateHouseSQLID] != 0 && !aoverride) {
		new house = houseIDFromSQLID(Gates[gate][EGateHouseSQLID]);
		if(house != -1) {
			if(getHouseOwner(house) != 0 && !hasHouseKeys(playerid, house)) {
				SendClientMessage(playerid, X11_TOMATO_2, "You don't have access to that gate");
				return 1;
			}
		}
	}
	if(Gates[gate][EGateState] >= 2) {
		SendClientMessage(playerid, X11_TOMATO_2, "This gate is locked!");
		return 1;
	}
	new string[128];
	new opentext[64];
	switch(Gates[gate][EGateState]) {
		case 0: {
			format(opentext, sizeof(opentext), "open");
		}
		case 1: {
			format(opentext, sizeof(opentext), "close");
		}
	}
	
	//LSPD outer gate
	/*
	if(gate == 2) {
		toggleGate(3);
	} else if(gate == 3) {
		toggleGate(2);
	}
	*/
	//LSPD doors
	if(gate == 4) {
		toggleGate(5);
	} else if(gate == 5) {
		toggleGate(4);
	}
	
	//FBI interior 1
	if(gate == 9) {
		toggleGate(10);
	} else if(gate == 10) {
		toggleGate(9);
	}
	
	if(gate == 10) {
		toggleGate(11);
	} else if(gate == 11) {
		toggleGate(10);
	}
	
	if(gate == 11) {
		toggleGate(12);
	} else if(gate == 12) {
		toggleGate(11);
	}
	
	if(gate == 13) {
		toggleGate(14);
	} else if(gate == 14) {
		toggleGate(13);
	}
	
	if(gate == 15) {
		toggleGate(16);
	} else if(gate == 16) {
		toggleGate(15);
	}
	
	if(gate == 17) {
		toggleGate(18);
	} else if(gate == 18) {
		toggleGate(17);
	}
	/*
	if(gate == 19) {
		toggleGate(20);
	} else if(gate == 20) {
		toggleGate(19);
	}
	*/
	//FBI end
	
	toggleGate(gate);
	if(strlen(Gates[gate][EGateRoleText]) > 0) {
		format(string, sizeof(string), Gates[gate][EGateRoleText],GetPlayerNameEx(playerid, ENameType_RPName), getPossiveAdjective(playerid), opentext);
		ProxMessage(30.0, playerid, string, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	}
	return 0;
}
toggleGate(gate) {
	new Float:X, Float:Y, Float:Z;
	X = Gates[gate][EGateX] + Gates[gate][EOpenPosX];
	Y = Gates[gate][EGateY] + Gates[gate][EOpenPosY];
	Z = Gates[gate][EGateZ] + Gates[gate][EOpenPosZ];
	if(Gates[gate][EGateState] == 0) {
		Gates[gate][EGateState] = 1;
		if(Gates[gate][EGateOpenRotX] != 0.0 || Gates[gate][EGateOpenRotY] != 0.0 ||  Gates[gate][EGateOpenRotZ] != 0.0) {
			SetDynamicObjectRot(Gates[gate][EGateObjectID], Gates[gate][EGateOpenRotX], Gates[gate][EGateOpenRotY], Gates[gate][EGateOpenRotZ]);
		}
		MoveDynamicObject(Gates[gate][EGateObjectID], X, Y, Z, Gates[gate][EGateMoveSpeed]);
	} else if(Gates[gate][EGateState] == 1) {
		Gates[gate][EGateState] = 0;
		SetDynamicObjectRot(Gates[gate][EGateObjectID], Gates[gate][EGateRotX], Gates[gate][EGateRotY], Gates[gate][EGateRotZ]);
		MoveDynamicObject(Gates[gate][EGateObjectID], Gates[gate][EGateX], Gates[gate][EGateY], Gates[gate][EGateZ], Gates[gate][EGateMoveSpeed]);
	}
	Gates[gate][EGateLastMoveTime] = gettime();

}
mappingOnGameModeInit() {
	loadGates();
	loadMapping();
}
loadMapping() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`, `objectid`, `objPosX`, `objPosY`, `objPosZ`, `objAngX`, `objAngY`, `objAngZ`, `objVW`, `objIntID` FROM `mapping` WHERE `objEnabled` = 1");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadMapping", "");
}

forward OnLoadMapping();
public OnLoadMapping() {
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		//if(MapObjects[i][objSQLID] != 0) continue; (it makes no sense to have a continue here? We're actually "reloading" the mapping.
		cache_get_row(i, 0, id_string); //unused but whatever
		MapObjects[i][objSQLID] = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		MapObjects[i][objID] = strval(id_string);
		
		cache_get_row(i, 2, id_string);
		MapObjects[i][objPosX] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		MapObjects[i][objPosY] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		MapObjects[i][objPosZ] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		MapObjects[i][objAngX] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		MapObjects[i][objAngY] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		MapObjects[i][objAngZ] = floatstr(id_string);
		
		cache_get_row(i, 8, id_string);
		MapObjects[i][objVW] = strval(id_string);
		
		cache_get_row(i, 9, id_string);
		MapObjects[i][objIntID] = strval(id_string);
		
		MapObjects[i][objStreamID] = CreateDynamicObject(MapObjects[i][objID], MapObjects[i][objPosX], MapObjects[i][objPosY], MapObjects[i][objPosZ], MapObjects[i][objAngX], MapObjects[i][objAngY], MapObjects[i][objAngZ], MapObjects[i][objVW], MapObjects[i][objIntID]);
	}
	return 1;
}
reloadMapping() { //This can be used to reload the mapping (admin command)
	removeAllMapping(); //Remove it
	loadMapping(); //Load it again
	foreach(Player, i) { //It doesn't make sense to have this here but it's here because the code looks neater like so..
		removeBuildings(i); //Remove buildings for every individual player
	}
}
removeAllMapping() {
	for(new i=0;i<sizeof(MapObjects);i++) {
	    if(MapObjects[i][objStreamID] != 0) {
	        DestroyDynamicObject(MapObjects[i][objStreamID]);
	    }
	}
}
mappingOnPlayerConnect(playerid) {	
	removeBuildings(playerid);
}
removeBuildings(playerid) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `objID`, `objPosX`, `objPosY`, `objPosZ`, `objRange` FROM `removebuildings` WHERE `objRemove` = 1");
	mysql_function_query(g_mysql_handle, query, true, "OnRemoveBuildingsForPlayer", "d", playerid);
}
forward OnRemoveBuildingsForPlayer(playerid);
public OnRemoveBuildingsForPlayer(playerid) {
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	new rObjID, Float: rPosX, Float: rPosY, Float: rPosZ, Float: rRange;
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		rObjID = strval(id_string);
		
		cache_get_row(i, 1, id_string);
		rPosX = floatstr(id_string);
		
		cache_get_row(i, 2, id_string);
		rPosY = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		rPosZ = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		rRange = floatstr(id_string);
		
		RemoveBuildingForPlayer(playerid, rObjID, rPosX, rPosY, rPosZ, rRange);
	}
	return 1;
}
loadGates() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `GateCMD`, `GateModelID`, `EGateX`, `EGateY`, `EGateZ`, `EGateRotX`, `EGateRotY`, `EGateRotZ`, `EGateFactionType`, `EGateJob`, `EOpenPosX`, `EOpenPosY`, `EOpenPosZ`, `EGateRoleText`, `EGateFindDistance`, `EGateMoveSpeed`, `EGateOpenRotX`, `EGateOpenRotY`, `EGateOpenRotZ`, `EGateInterior`, `EGateVW`, `EGateFaction`, `EGateHouseSQLID` FROM `gates` where `EGateEnabled` = 1");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadGates", "");
	return 1;
}
forward OnLoadGates();
public OnLoadGates() {
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, Gates[i][EGateCmd]);
		
		cache_get_row(i, 1, id_string);
		Gates[i][EGateModelID] = strval(id_string);
		
		cache_get_row(i, 2, id_string);
		Gates[i][EGateX] = floatstr(id_string);
		
		cache_get_row(i, 3, id_string);
		Gates[i][EGateY] = floatstr(id_string);
		
		cache_get_row(i, 4, id_string);
		Gates[i][EGateZ] = floatstr(id_string);
		
		cache_get_row(i, 5, id_string);
		Gates[i][EGateRotX] = floatstr(id_string);
		
		cache_get_row(i, 6, id_string);
		Gates[i][EGateRotY] = floatstr(id_string);
		
		cache_get_row(i, 7, id_string);
		Gates[i][EGateRotZ] = floatstr(id_string);
		
		cache_get_row(i, 8, id_string);
		Gates[i][EGateFactionType] = FactionType:strval(id_string);
		
		cache_get_row(i, 9, id_string);
		Gates[i][EGateJob] = strval(id_string);
		
		cache_get_row(i, 10, id_string);
		Gates[i][EOpenPosX] = floatstr(id_string);
		
		cache_get_row(i, 11, id_string);
		Gates[i][EOpenPosY] = floatstr(id_string);
		
		cache_get_row(i, 12, id_string);
		Gates[i][EOpenPosZ] = floatstr(id_string);
		
		cache_get_row(i, 13, Gates[i][EGateRoleText]);
		
		cache_get_row(i, 14, id_string);
		Gates[i][EGateFindDistance] = floatstr(id_string);
		
		cache_get_row(i, 15, id_string);
		Gates[i][EGateMoveSpeed] = floatstr(id_string);
		
		cache_get_row(i, 16, id_string);
		Gates[i][EGateOpenRotX] = floatstr(id_string);
		
		cache_get_row(i, 17, id_string);
		Gates[i][EGateOpenRotY] = floatstr(id_string);
		
		cache_get_row(i, 18, id_string);
		Gates[i][EGateOpenRotZ] = floatstr(id_string);
		
		cache_get_row(i, 19, id_string);
		Gates[i][EGateInterior] = strval(id_string);
		
		cache_get_row(i, 20, id_string);
		Gates[i][EGateVW] = strval(id_string);
		
		cache_get_row(i, 21, id_string);
		Gates[i][EGateFaction] = strval(id_string);
		
		cache_get_row(i, 22, id_string);
		Gates[i][EGateHouseSQLID] = strval(id_string);
		
		Gates[i][EGateObjectID] = CreateDynamicObject(Gates[i][EGateModelID],Gates[i][EGateX],Gates[i][EGateY],Gates[i][EGateZ], Gates[i][EGateRotX], Gates[i][EGateRotY], Gates[i][EGateRotZ],Gates[i][EGateVW],Gates[i][EGateInterior]);
	}
	//setupGateTextures();
	return 1;
}
removeAllGates() {
	for(new i=0;i<sizeof(Gates);i++) {
	    if(Gates[i][EGateObjectID] != 0) {
	        DestroyDynamicObject(Gates[i][EGateObjectID]);
	    }
	}
}
reloadGates() { //For admin command
	removeAllGates(); //Remove them
	loadGates(); //Add them again
}
mappingOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#pragma unused x
	#pragma unused y
	#pragma unused z
	#pragma unused modelid
	new index = findMappingByObjID(objectid);
	if(GetPVarInt(playerid, "MappingInfo") == 1) {
		if(index != -1) {
			DisplayMappingInfo(playerid, index);
		}
	}
	return 0;	
}
DisplayMappingInfo(playerid, index) {
	new msg[128];
	format(msg, sizeof(msg), "* Mapping Object ID: %d SQLID: %d",MapObjects[index][objID],MapObjects[index][objSQLID]);
	SendClientMessage(playerid, X11_YELLOW, msg);
	DeletePVar(playerid, "MappingInfo");
	CancelEdit(playerid);
}
findMappingByObjID(objectid) {
	for(new i = 0; i < sizeof(MapObjects); i++) {
		if(MapObjects[i][objStreamID] == objectid) {
			#if debug
			printf("findMappingByObjID(%d)",i);
			#endif
			return i;
		}
	}
	return -1;
}
findMappingObjIDBySQLID(sqlid) {
	for(new i=0;i<sizeof(MapObjects);i++) {
		if(MapObjects[i][objSQLID] == sqlid) {
			#if debug
			printf("findMappingObjIDBySQLID(%d)",i);
			#endif
			return i;
		}
	}
	return -1;
}
findMappingSQLIDByObjID(objid) {
	for(new i=0;i<sizeof(MapObjects);i++) {
		if(MapObjects[i][objStreamID] == objid) {
			#if debug
			printf("findMappingSQLIDByObjID(%d)",MapObjects[i][objSQLID]);
			#endif
			return MapObjects[i][objSQLID];
		}
	}
	return -1;
}
/* Commands */
YCMD:reloadgates(playerid, params[], help) {
	reloadGates();
	SendClientMessageToAll(COLOR_CUSTOMGOLD, "An admin has reloaded the gates!");
	return 1;
}
YCMD:reloadmapping(playerid, params[], help) {
	reloadMapping();
	SendClientMessageToAll(COLOR_CUSTOMGOLD, "An admin has reloaded the mapping!");
	return 1;
}
YCMD:mappinginfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays info on a specific mapped object");
		return 1;
	}
	SelectObject(playerid);
	SetPVarInt(playerid, "MappingInfo", 1);
	SendClientMessage(playerid, X11_LIGHTBLUE, "Select the mapped object you wish to see information on");
	return 1;
}
