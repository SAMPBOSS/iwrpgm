enum ERaceSpawnInfo {
	Float:ERaceSpawnX,
	Float:ERaceSpawnY,
	Float:ERaceSpawnZ,
	Float:ERaceSpawnAngle,
	ERaceVehicleID,
	ERacePlayerID,
	ERacePoint,
	ERaceLap,
	ERaceLastTime,
	ERaceTotalTime,
	PlayerText:ERaceTextDraw,
	ERaceTotalStages, //sum of persons stage
};
enum {
	ERacing_ChooseCar  = ERacing_Base + 1,
};
#define NUM_RACE_LAPS 4
new RaceSpawns[][ERaceSpawnInfo] = {
{-1401.540527, -239.490570, 1043.053466, 348.258087,0,INVALID_PLAYER_ID,0,0,0,0,PlayerText:0,0},
{-1398.411987, -240.139236, 1043.083496, 347.436676,0,INVALID_PLAYER_ID,0,0,0,0,PlayerText:0,0},
{-1395.069580, -240.511108, 1043.101440, 354.499176,0,INVALID_PLAYER_ID,0,0,0,0,PlayerText:0,0},
{-1390.566040, -241.233032, 1043.154663, 3.790367,0,INVALID_PLAYER_ID,0,0,0,0,PlayerText:0,0}
};
enum ERaceCheckPoints {
	Float:ERaceCheckPointX,
	Float:ERaceCheckPointY,
	Float:ERaceCheckPointZ,
};
new RacePoints[][ERaceCheckPoints] = {
{-1441.537353, -130.482315, 1045.687500},
{-1529.724487, -206.556427, 1050.414916},
{-1490.812622, -267.584716, 1049.905273},
{-1401.450317, -239.379943, 1050.786132},
{-1332.211914, -134.116638, 1050.272705},
{-1265.549194, -212.922622, 1050.428710},
{-1319.007080, -278.816833, 1047.219116},
{-1390.931152, -250.378768, 1043.351928}
};
new RaceCars[] = {
602,429,496,402,541,415,589,587,565,494,502,503,411,559,603,475,506,451,558,477,571,424,495,504,
522,468,471,581,461
};
new racetimer = -1;
#define RACE_COUTNDOWN 5
new racetick = RACE_COUTNDOWN;
new racestart;
openRaceMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(RaceCars);i++) {
		format(tempstr, sizeof(tempstr), "%s\n",VehiclesName[RaceCars[i]-400]);
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, ERacing_ChooseCar, DIALOG_STYLE_LIST, "{00BFFF}Racing Menu", dialogstr,"Select","Cancel");	
	//HintMessage(playerid, COLOR_DARKGREEN, "Do /startrace to start the countdown!");
	SendClientMessage(playerid, COLOR_DARKGREEN, "Do /startrace to start the countdown!");
}
RacingOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	if(!response) return 0;
	switch(dialogid) {
		case ERacing_ChooseCar: {
			CreateRaceCar(playerid, listitem);
		}
	}
	return 1;
}
racingOnPlayerDeath(playerid, killerid, reason) {
	#pragma unused killerid
	#pragma unused reason
	new s = findPlayerRaceSlot(playerid);
	if(s != -1) {
		removePlayerFromRace(playerid);
	}
}
CreateRaceCar(playerid, car) {
	new c = findFreeRaceCarSlot();
	if(c != -1) {
		new vw = GetPlayerVirtualWorld(playerid);
		new interior = GetPlayerInterior(playerid);
		RaceSpawns[c][ERaceVehicleID] = CreateVehicle(RaceCars[car],RaceSpawns[c][ERaceSpawnX],RaceSpawns[c][ERaceSpawnY],RaceSpawns[c][ERaceSpawnZ],RaceSpawns[c][ERaceSpawnAngle], -1, -1, 3600);
		SetVehicleVirtualWorld(RaceSpawns[c][ERaceVehicleID],vw);
		LinkVehicleToInterior(RaceSpawns[c][ERaceVehicleID],interior);
		PutPlayerInVehicleEx(playerid, RaceSpawns[c][ERaceVehicleID],0);
		VehicleInfo[RaceSpawns[c][ERaceVehicleID]][EVFuel] = 100;
		VehicleInfo[RaceSpawns[c][ERaceVehicleID]][EVType] = EVehicleType_Uninit;
		VehicleInfo[RaceSpawns[c][ERaceVehicleID]][EVRadioStation] = -1;
		RaceSpawns[c][ERacePlayerID] = playerid;
		RaceSpawns[c][ERacePoint] = 0;
		RaceSpawns[c][ERaceTotalStages] = 0;
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "Sorry! We have reached our maximum capacity of racers!");
	}
	
}
isRaceActive() { //is anyone in a car
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERaceVehicleID] != 0) {
			return 1;
		}
	}
	return 0;
}
racingOnPlayerStateChange(playerid, newstate, oldstate) {
	#pragma unused newstate
	new s = findPlayerRaceSlot(playerid);
	if(s != -1) {
		if(oldstate == PLAYER_STATE_DRIVER) {
			removePlayerFromRace(playerid);
		}
	}
}
removePlayerFromRace(playerid) {
	new s = findPlayerRaceSlot(playerid);
	if(s != -1) {
		DestroyVehicle(RaceSpawns[s][ERaceVehicleID]);
		PlayerTextDrawHide(playerid, RaceSpawns[s][ERaceTextDraw]);
		PlayerTextDrawDestroy(playerid, RaceSpawns[s][ERaceTextDraw]);
		SetPlayerPos(playerid, 2695.699218, -1704.843139, 11.843750);
		SetPlayerFacingAngle(playerid, 40.5);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerInterior(playerid, 0);
		
		RaceSpawns[s][ERacePoint] = 0;
		RaceSpawns[s][ERaceVehicleID] = 0;
		RaceSpawns[s][ERacePlayerID] = INVALID_VEHICLE_ID;
		RaceSpawns[s][ERaceLap] = 0;
		RaceSpawns[s][ERaceLastTime] = 0;
		RaceSpawns[s][ERaceTotalTime] = 0;
		RaceSpawns[s][ERaceTextDraw] = PlayerText:0;
		RaceSpawns[s][ERaceTotalStages] = 0;
		DisablePlayerRaceCheckpoint(playerid);
	}
}
racingOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	new s = findPlayerRaceSlot(playerid);
	if(s != -1) {
		removePlayerFromRace(playerid);
	}
}
findFreeRaceCarSlot() {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERaceVehicleID] == 0) {
			return i;
		}
	}
	return -1;
}

YCMD:startrace(playerid, params[], help) {
	new s = findPlayerRaceSlot(playerid);
	if(s != -1 && racetimer == -1) {
		startRace();
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not in a race car.");
	}
	return 1;
}

startRace() {
	racetimer = SetTimer("RaceTimer", 1000, true);
}
forward RaceTimer();
public RaceTimer() {
	new msg[128];
	if(racetick > 0) {
		format(msg, sizeof(msg), "~r~%d",racetick--);
		sendGameTextToRacers(msg);
	} else {
		sendGameTextToRacers("~g~GO!");
		startDriversCars();
		racetick = RACE_COUTNDOWN;
		racestart = tickcount();
		new otimer = racetimer;
		racetimer = -1;
		KillTimer(otimer);
	}
}
sendGameTextToRacers(msg[]) {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERaceVehicleID] != 0) {
			new driver = GetVehicleDriver(RaceSpawns[i][ERaceVehicleID]);
			if(driver != INVALID_PLAYER_ID) {
				GameTextForPlayer(driver, msg, 1000, 3);
			}
		}
	}
}
startDriversCars() {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERaceVehicleID] != 0) {
			new engine,lights,alarm,doors,bonnet,boot,objective;
			GetVehicleParamsEx(RaceSpawns[i][ERaceVehicleID],engine,lights,alarm,doors,bonnet,boot,objective);
			SetVehicleParamsEx(RaceSpawns[i][ERaceVehicleID],1,lights,alarm,1,bonnet,boot,objective);
			showNextPoint(RaceSpawns[i][ERacePlayerID]);
		}
	}
}

findPlayerRaceSlot(playerid) {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERacePlayerID] == playerid) {
			return i;
		}
	}
	return -1;
}
endRace() {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERacePlayerID] != INVALID_PLAYER_ID) {
			removePlayerFromRace(RaceSpawns[i][ERacePlayerID]);
		}
	}
}
showNextPoint(playerid) {
	new msg[128];
	new isstart = 0;
	new s = findPlayerRaceSlot(playerid);
	#pragma unused isstart
	if(s != -1) {
		new p = RaceSpawns[s][ERacePoint];
		new islast = p+1 >= sizeof(RacePoints);
		isstart = p==1;
		if(RaceSpawns[s][ERaceLastTime] != 0) {
			RaceSpawns[s][ERaceTotalTime] += tickcount() - racestart;
		}
		if(p == 1 && RaceSpawns[s][ERaceLap] == 0) {
			setupRaceTextDraw(playerid);
		}
		RaceSpawns[s][ERaceLastTime] = tickcount();
		if(!islast && RaceSpawns[s][ERacePoint] != -1) {
			SetPlayerRaceCheckpoint(playerid, 0, RacePoints[p][ERaceCheckPointX], RacePoints[p][ERaceCheckPointY], RacePoints[p][ERaceCheckPointZ], RacePoints[p+1][ERaceCheckPointX], RacePoints[p+1][ERaceCheckPointY], RacePoints[p+1][ERaceCheckPointZ],25.0);
			RaceSpawns[s][ERacePoint]++;
		} else if(RaceSpawns[s][ERacePoint] == -1) {
			RaceSpawns[s][ERaceLap]++;
			if(RaceSpawns[s][ERaceLap] >= NUM_RACE_LAPS) {
				new engine,lights,alarm,doors,bonnet,boot,objective;
				new car = RaceSpawns[s][ERaceVehicleID];
				GetVehicleParamsEx(car,engine,lights,alarm,doors,bonnet,boot,objective);
				SetVehicleParamsEx(car,0,lights,alarm,1,bonnet,boot,objective);
				DisablePlayerRaceCheckpoint(playerid);
				new place[][] = {{"first"},{"second"},{"third"},{"fourth"}};
				new splace = getRacerPlace(playerid)-1;
				if(splace > 3 || splace < 0) splace = 3;
				format(msg, sizeof(msg), "* %s has placed in %s",GetPlayerNameEx(playerid, ENameType_RPName),place[splace]);
				SendRaceMessage(COLOR_LIGHTBLUE, msg);
				return 1;
			}
			RaceSpawns[s][ERacePoint] = 1;
			p = 0;
			SetPlayerRaceCheckpoint(playerid, 0, RacePoints[p][ERaceCheckPointX], RacePoints[p][ERaceCheckPointY], RacePoints[p][ERaceCheckPointZ], RacePoints[p+1][ERaceCheckPointX], RacePoints[p+1][ERaceCheckPointY], RacePoints[p+1][ERaceCheckPointZ],25.0);
		} else {
			RaceSpawns[s][ERacePoint] = -1;
			SetPlayerRaceCheckpoint(playerid, 1, RacePoints[p][ERaceCheckPointX], RacePoints[p][ERaceCheckPointY], RacePoints[p][ERaceCheckPointZ], 0.0,0.0,0.0,25.0);
		}
		//RaceSpawns[s][ERaceTotalStages] += getRaceStage(s);
	}
	updateRaceTextDraws();
	return 1;
}
racingOnEnterCheckpoint(playerid) {
	if(findPlayerRaceSlot(playerid) != -1) {
		showNextPoint(playerid);
	}
}
SendRaceMessage(color, msg[]) {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERaceVehicleID] != 0) {
			new driver = GetVehicleDriver(RaceSpawns[i][ERaceVehicleID]);
			if(driver != INVALID_PLAYER_ID) {
				SendClientMessage(driver, color, msg);
			}
		}
	}
}
getRaceTextDrawText(playerid) {
	new positions[sizeof(RaceSpawns)];
	new text[512];
	new i;
	for(i=0;i<sizeof(positions);i++) {
		positions[i] = INVALID_PLAYER_ID;
	}
	for(i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERacePlayerID] != INVALID_PLAYER_ID) {
			new pos = getRacerPlace(RaceSpawns[i][ERacePlayerID]);
			if(positions[pos-1] == INVALID_PLAYER_ID ) {
				positions[pos-1] = RaceSpawns[i][ERacePlayerID];
			}
		}
	}
	new tstr[128];
	format(text, sizeof(text), "~g~Race Scoreboard~n~");
	for(i=0;i<sizeof(positions);i++) {
		if(positions[i] != INVALID_PLAYER_ID) {
			new s = findPlayerRaceSlot(positions[i]);
			if(s != -1) {
				new p = RaceSpawns[s][ERacePoint];
				if(p == -1) p = sizeof(RacePoints);
				format(tstr, sizeof(tstr), "~%c~%d. %s Point: %d/%d Lap: %d/%d~n~",positions[i]==playerid?'r':'g',i+1,GetPlayerNameEx(positions[i],ENameType_RPName),p,sizeof(RacePoints),RaceSpawns[s][ERaceLap]+1,NUM_RACE_LAPS);	
				strcat(text,tstr,sizeof(text));
			}
		}
	}
	return text;
}
putPlayerInRacing(playerid) {
	SetPlayerPos(playerid, -1404.331909, -257.401275, 1043.656250);
	SetPlayerFacingAngle(playerid, 327.933166);
	SetPlayerInterior(playerid, 7);
	openRaceMenu(playerid);
}
setupRaceTextDraw(playerid) {
	new s = findPlayerRaceSlot(playerid);
	if(s == -1) return 0;
	RaceSpawns[s][ERaceTextDraw] = CreatePlayerTextDraw(playerid,425.0, 152.0,getRaceTextDrawText(playerid));
	PlayerTextDrawFont(playerid, RaceSpawns[s][ERaceTextDraw], 1);
	PlayerTextDrawAlignment(playerid, RaceSpawns[s][ERaceTextDraw], 1);
	PlayerTextDrawSetProportional(playerid, RaceSpawns[s][ERaceTextDraw], 1);
	PlayerTextDrawSetOutline(playerid, RaceSpawns[s][ERaceTextDraw], 1);
	PlayerTextDrawUseBox(playerid, RaceSpawns[s][ERaceTextDraw], 1);
    PlayerTextDrawBoxColor(playerid, RaceSpawns[s][ERaceTextDraw], 0x00000066);
	PlayerTextDrawLetterSize(playerid, RaceSpawns[s][ERaceTextDraw], 0.250000, 1.800000);
	PlayerTextDrawShow(playerid, RaceSpawns[s][ERaceTextDraw]);
	return 1;	
}
updateRaceTextDraws() {
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(RaceSpawns[i][ERacePlayerID] != INVALID_PLAYER_ID) {
			updatePlayerRaceTextDraw(RaceSpawns[i][ERacePlayerID]);
		}
	}
}
updatePlayerRaceTextDraw(playerid) {
	new s = findPlayerRaceSlot(playerid);
	if(s == -1 || RaceSpawns[s][ERaceTextDraw] == PlayerText:0) return 0;
	PlayerTextDrawSetString(playerid, RaceSpawns[s][ERaceTextDraw], getRaceTextDrawText(playerid));
	return 1;
}
isInRace(playerid) {
	return findPlayerRaceSlot(playerid) != -1;
}
YCMD:raceinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays race debugging information");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup_acc>",user)) {
		new i = findPlayerRaceSlot(user);
		if(i != -1) {
			format(query, sizeof(query), "Slot: %d Point: %d Lap: %d TTime: %d LTime: %d Place: %d",i,RaceSpawns[i][ERacePoint],RaceSpawns[i][ERaceLap],RaceSpawns[i][ERaceTotalTime],RaceSpawns[i][ERaceLastTime],getRacerPlace(user));
			SendClientMessage(playerid, X11_TOMATO_2, query);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in a race");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /raceinfo [playerid/name]");
	}
	return 1;
}
/*
YCMD:raceinfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays race debugging information");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup_acc>",user)) {
		new i = findPlayerRaceSlot(user);
		if(i != -1) {
			format(query, sizeof(query), "Slot: %d Point: %d Lap: %d TTime: %d LTime: %d Place: %d Stage: %d",i,RaceSpawns[i][ERacePoint],RaceSpawns[i][ERaceLap],RaceSpawns[i][ERaceTotalTime],RaceSpawns[i][ERaceLastTime],getRacerPlace(user),getRaceStage(i));
			SendClientMessage(playerid, X11_TOMATO_2, query);
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "This person is not in a race");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /raceinfo [playerid/name]");
	}
	return 1;
}

getRacerPlace(playerid) {
	new place = 1;
	new s = findPlayerRaceSlot(playerid);
	if(s == -1) return 0;
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(i != s) {
			if(RaceSpawns[i][ERaceTotalTime] != 0) {
				if(((RaceSpawns[i][ERacePoint] >= RaceSpawns[s][ERacePoint] || (RaceSpawns[i][ERacePoint] == -1 || RaceSpawns[s][ERacePoint] == -1)) || RaceSpawns[i][ERaceLap] > RaceSpawns[s][ERaceLap]) || RaceSpawns[i][ERaceTotalTime] > RaceSpawns[s][ERaceTotalTime]) {
					if(RaceSpawns[s][ERaceLap] > RaceSpawns[i][ERaceLap]) {
						continue;
					}
					if(RaceSpawns[i][ERacePoint] == RaceSpawns[s][ERacePoint] && RaceSpawns[i][ERaceLap] == RaceSpawns[s][ERaceLap]) {
						if(RaceSpawns[s][ERaceTotalTime] < RaceSpawns[i][ERaceTotalTime]) {
							if(RaceSpawns[s][ERaceLastTime] < RaceSpawns[i][ERaceLastTime]) {
								continue;
							}
						}
					}
					if(RaceSpawns[i][ERaceLastTime] < RaceSpawns[s][ERaceLastTime]) {
						continue;
					}
					if(RaceSpawns[s][ERacePoint] == -1) {
						if(RaceSpawns[i][ERacePoint] != -1) {
							if(RaceSpawns[s][ERaceTotalTime] > RaceSpawns[i][ERaceTotalTime]) {
								continue;
							}
						}
					}
					
					if(RaceSpawns[i][ERaceLap] >= RaceSpawns[s][ERaceLap]) {
						if(RaceSpawns[i][ERaceLap] == RaceSpawns[s][ERaceLap]) {
							if(RaceSpawns[s][ERaceTotalTime] < RaceSpawns[i][ERaceTotalTime]) {
								if(RaceSpawns[s][ERacePoint] > RaceSpawns[i][ERacePoint]) {
									if(RaceSpawns[s][ERaceLastTime] < RaceSpawns[i][ERaceLastTime]) {
										continue;
									}
								}
							}
						}
						
						place++;
					}
				}
			}
		}
	}
	return place;
}

YCMD:drace(playerid, params[], help) {
	new id;
	new msg[128];
	if(!sscanf(params, "d", id)) {
		new place = 1;
		new s = findPlayerRaceSlot(id);
		if(s == -1) return 0;
		new mstage = getRaceStage(s);
		for(new i=0;i<sizeof(RaceSpawns);i++) {
			if(i != s) {
				if(RaceSpawns[i][ERacePlayerID] == INVALID_PLAYER_ID) continue;
				new stage = getRaceStage(i);
				if(stage > mstage) {
					place++;
					format(msg, sizeof(msg), "s1 %d %d %d",place,stage,mstage);
					SendClientMessage(playerid, X11_TOMATO_2, msg);
				} else if(stage == mstage) {
						if(RaceSpawns[s][ERaceLastTime] < RaceSpawns[i][ERaceLastTime]) {
							continue;
					}
					place++;
					format(msg, sizeof(msg), "s2 %d %d %d",place,stage,mstage);
					SendClientMessage(playerid, X11_TOMATO_2, msg);
				} else {
					continue;
				}
			}
		}
		
		format(msg, sizeof(msg), "%d",place);
		SendClientMessage(playerid, X11_WHITE, msg);
	}
	return 1;
}
*/
getRacerPlace(playerid) {
	new place = 1;
	new s = findPlayerRaceSlot(playerid);
	if(s == -1) return 0;
	for(new i=0;i<sizeof(RaceSpawns);i++) {
		if(i != s) {
			if(RaceSpawns[i][ERacePlayerID] == INVALID_PLAYER_ID) continue;
			new p1, p2;
			p1 =  RaceSpawns[i][ERacePoint];
			p2 = RaceSpawns[s][ERacePoint];
			
			if(p1 == -1) p1 = sizeof(RacePoints)+1;
			if(p2 == -1) p2 = sizeof(RacePoints)+1;
			if((RaceSpawns[i][ERaceLap] >= RaceSpawns[s][ERaceLap] && p1 > p2) || RaceSpawns[i][ERaceLap] > RaceSpawns[s][ERaceLap]) {
				place++;
			} else if(p2 == p1 && RaceSpawns[s][ERaceLap] == RaceSpawns[i][ERaceLap]) {
				if(RaceSpawns[s][ERaceLastTime] < RaceSpawns[i][ERaceLastTime]) {
					continue;
				}
				place++;
				printf("%d - s2 %d\n",playerid,place);
			} else {
				continue;
			}
		}
	}
	return place;
}
/*
getRaceStage(s) {
	new point = RaceSpawns[s][ERacePoint];
	if(point == -1) {
		point = sizeof(RacePoints);
	}
	point++;
	new lap = RaceSpawns[s][ERaceLap]+1;
	//race bug here from integer overflow
	return (point*lap)+RaceSpawns[s][ERaceTotalStages];
}
*/