forward adminOnPlayerConnect(playerid);
forward OnBanRetrieve(playerid);
forward ABroadcast(color, const msg[], EAdminFlags:level);
forward KickEx(playerid, reason[]);
forward BanPlayer(playerid, reason[], bannerid, bool:gpciban, expiretime);
forward adminOnPlayerLogin(playerid);
forward BanDeleteCallback(playerid, type, name[]);
forward AccountLookupCallback(playerid, name[]);
forward OnOfflineBanPlayer(playerid, name[],reason[]);
forward DelayedSpectate(playerid, target, targettype);

native gpci(playerid, const serial[], maxlen);



/*
	PVars used in this script
	AdminLevel(int) - admin level
	NewbieRank(int) - Newb rank
	Spectating(int) - ID of who you are spectating
	SpecHealth(int) - health before you started speccing
	SpecArmour(int) - armour before you started speccing
	SpecGun0-12(int) - guns before speccing
	SpecAmmo0-12(int) - ammo before speccing
*/

#define MAX_ADMIN_OVERRIDE_ATTEMPTS 3

#define REQUESTCHAT_COOLDOWN 60
#define DEFAULT_BAN_TIME 604800
#define AJAIL_MAX_BANTIME 86400
#define MAX_AJAILS_BEFORE_BAN 3
#define MAX_BLOCKED_PM	8

new BLOCKED_PMS[MAX_PLAYERS][MAX_BLOCKED_PM];

new gamesoff = 0;
new servermaintenance = 0;
#if debug
new noooc = 0;
new nonewb = 0;
new nopms = 0;
new nohelperchat = 0;
new svnamechangeallowed = 0; //Disables / enables the server mottos
#else
new noooc = 1;
new nonewb = 0;
new nopms = 0;
new nohelperchat = 0;
new svnamechangeallowed = 0; //Disables / enables the server mottos
#endif

new server_motd[128];
new server_amotd[128];
new server_taxrate;
new server_doublebonus;
new server_interestrate;
new server_rptest;

enum EAdminFlags (<<= 1) {
	EAdminFlags_None = 0, 
	EAdminFlags_BasicAdmin = 1, //kick, fine, etc 0
	EAdminFlags_CanGiveGuns, //1
	EAdminFlags_HouseAdmin, //2
	EAdminFlags_IRCAdmin, //3
	EAdminFlags_BusinessAdmin, //4
	EAdminFlags_AdminManage, //5 can see invisible admins/manipulate admin flags
	EAdminFlags_Scripter, //getpvars, reloadcmds, stuff only scripters should need 6
	EAdminFlags_HelperManage, // 7 manage helpers/teachers
	EAdminFlags_Unbannable, //unbanable under ANY circumstances 8
	EAdminFlags_CanRefundSelf, //can refund himself
	EAdminFlags_VehicleAdmin, //10 can manipulate player vehicles
	EAdminFlags_AntiCheat, //immune to the anticheat 11
	EAdminFlags_BigEars, //can /bigears, /bigfamilyears, /bigmouth
	EAdminFlags_SetName, //13
	EAdminFlags_CanHide, //can toggle/untoggle hidden on themselves 14
	EAdminFlags_Invisible, //Invisible on admins list(like 9999s before) 15
	EAdminFlags_GiveDrugs, //server manager, can restart the server/do rcon cmds, create job cars 16
	EAdminFlags_Unban, //can unban, oban, oprison people 17
	EAdminFlags_WeatherManage, //can manage weather/time 18
	EAdminFlags_GiveRestrictedGun, //can give restircted guns 19
	EAdminFlags_TeleportOthers, //can teleport other people 20
	EAdminFlags_CanBanAdmins, //can ban other admins(or any punishments) 21
	EAdminFlags_GiveMats, //can give materials 22
	EAdminFlags_StatsManage, //23 /setstat, /sethp, /setarmor
	EAdminFlags_FamilyAdmin, //24
	EAdminFlags_FactionAdmin, //25
	EAdminFlags_MassCmds, //26 can do /masstphere, /massmoney, /masskick, /massnuke
	EAdminFlags_Nuke, //27 can do things like /nuke, sparta, rangeban
	EAdminFlags_RangeBan, //28 can range ban
	EAdminFlags_GiveMoney, //29 can /givemoney /money
	EAdminFlags_Donations, //30 can give DPS/set donate rank
	EAdminFlags_ServerManager, //31 can restart the server, etc, use /noooc, /nopms, /nonewb
	EAdminFlags_All = -1,
};

enum EAdminFlagInfo {
	EAdminFlags:EAFIFlag, //flag ID
	EAFIDesc[64],//description of admin flag
};
new AdminFlagDescription[][EAdminFlagInfo] = 
	{
	{EAdminFlags_None, "Nothing"},
	{EAdminFlags_All, "Everything"},
	{EAdminFlags_BasicAdmin, "Moderator"},
	{EAdminFlags_CanGiveGuns, "Gun Refunder"},
	{EAdminFlags_HouseAdmin, "House Admin"},
	{EAdminFlags_IRCAdmin, "IRC Admin"},
	{EAdminFlags_BusinessAdmin, "Business Admin"},
	{EAdminFlags_AdminManage, "Can manage admins"},
	{EAdminFlags_Scripter, "Scripter"},
	{EAdminFlags_HelperManage, "Can manage helpers/teachers"},
	{EAdminFlags_Unbannable, "Can never be banned under any circumstances"},
	{EAdminFlags_CanRefundSelf, "Can refund yourself"},
	{EAdminFlags_VehicleAdmin, "Can manage player vehicles"},
	{EAdminFlags_AntiCheat, "Can use cheats and is immune to AntiCheat"},
	{EAdminFlags_BigEars, "Can use /bigears, /bigmouth, /noooc"},
	{EAdminFlags_SetName, "Can /setname, /setaccountname"},
	{EAdminFlags_CanHide, "Can Hide themselves from admin list"},
	{EAdminFlags_Invisible, "Invisible in admin list"},
	{EAdminFlags_GiveDrugs, "Can give drugs"},
	{EAdminFlags_Unban, "Can unban people"},
	{EAdminFlags_WeatherManage, "Can manage the weather"},
	{EAdminFlags_GiveRestrictedGun, "Can give restricted guns to people"},
	{EAdminFlags_TeleportOthers, "Can teleport other people to you"},
	{EAdminFlags_CanBanAdmins, "Can ban other admins"},
	{EAdminFlags_GiveMats, "Can spawn materials"},
	{EAdminFlags_StatsManage, "Can manage other players stats"},
	{EAdminFlags_FamilyAdmin, "Can manage families"},
	{EAdminFlags_FactionAdmin, "Can manage factions"},
	{EAdminFlags_MassCmds, "Can do /mass commands"},
	{EAdminFlags_Nuke, "Can /Nuke, /Sparta, /skydive"},
	{EAdminFlags_RangeBan, "Can Range ban"},
	{EAdminFlags_GiveMoney, "Can give money"},
	{EAdminFlags_Donations, "Can give dps/set donate rank"},
	{EAdminFlags_ServerManager, "Can restart/shutdown the server"}
	};

enum  EAdminCmds {
	ECmdName[32],
	ECmdNewbLevel,
	EAdminFlags:ECmdAdminFlags, //bitmask which must be matched
	ECmdMinConnectTime,
	ECmdMaxConnectTime, //n 
	ECmdMinLevel, //votekick, etc
};

enum ERequestchatEndReason {
	RcExit_Exited,
	RcExit_Disconnected,
	RcExit_TimedOut,
	RcExit_Kicked,
	RcExit_SwitchChar,
	RcExit_Unknown
};

new admincmds[][EAdminCmds] = {
	{"givecookie", 2, EAdminFlags_BasicAdmin,0, 0, 0},
	{"takecookie", 2, EAdminFlags_BasicAdmin,0, 0, 0},
	{"ninvite", 2, EAdminFlags_HelperManage,0, 0, 0},
	{"nuninvite", 2, EAdminFlags_HelperManage,0, 0, 0},
	{"maketeacher", 0, EAdminFlags_HelperManage,0, 0, 0},
	//{"newbie", 1, EAdminFlags_BasicAdmin,0, 50, 0},
	//{"helpers", 1, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"setint", 2, EAdminFlags_BasicAdmin,0, 0, 0},
	{"setworld", 2, EAdminFlags_BasicAdmin,0, 0, 0},
	{"ban",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"spectate",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"sban",0, EAdminFlags_CanHide, 0, 0, 0},
	{"kick",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"skick",0, EAdminFlags_CanHide, 0, 0, 0},
	{"sjail",0, EAdminFlags_CanHide, 0, 0, 0},
	{"slap",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"admin",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"adminduty",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"getcar",0, EAdminFlags_BasicAdmin|EAdminFlags_VehicleAdmin|EAdminFlags_TeleportOthers, 0, 0, 0},
	{"gotocar",0, EAdminFlags_BasicAdmin|EAdminFlags_VehicleAdmin|EAdminFlags_TeleportOthers, 0, 0, 0},
	{"goto",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotols",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotopoint",0, EAdminFlags_BasicAdmin, 0, 0, 0},	
	{"setint",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"setworld",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gethere",0, EAdminFlags_TeleportOthers, 0, 0, 0},
	{"mark",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotomark",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"fixcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"ip",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"destroycar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"respawncar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"createplayercar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"destroyplayercar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"makeadmin",0, EAdminFlags_AdminManage, 0, 0, 0},
	{"setadmintitle",0, EAdminFlags_AdminManage,0 ,0, 0},
	{"jetpack",0, EAdminFlags_AntiCheat, 0, 0, 0},
	{"bigmouth",0 ,EAdminFlags_BigEars, 0, 0, 0},
	{"bigfamilyears", 0, EAdminFlags_BigEars, 0, 0, 0},
	{"bigfactionears", 0, EAdminFlags_BigEars, 0, 0, 0},
	{"bigears", 0, EAdminFlags_BigEars, 0, 0, 0},
	{"reloadcmds",0, EAdminFlags_Scripter, 0, 0, 0},
	{"getplayervartype",0, EAdminFlags_Scripter, 0, 0, 0},
	{"getplayervars",0, EAdminFlags_Scripter, 0, 0, 0},
	{"getplayervar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"setplayervar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"deleteplayervar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"docommand",0, EAdminFlags_Scripter, 0, 0, 0},
	{"masscommand",0, EAdminFlags_Scripter, 0, 0, 0},	
	{"gpci",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"gpciban",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"setweather",0, EAdminFlags_WeatherManage, 0, 0, 0},
	{"setplayerweather",0, EAdminFlags_WeatherManage, 0, 0, 0},
	{"settime",0, EAdminFlags_WeatherManage, 0, 0, 0},
	{"nonewb",0, EAdminFlags_HelperManage, 0, 0, 0},
	{"noooc",0, EAdminFlags_ServerManager, 0, 0, 0},
	{"nodoublebonus",0, EAdminFlags_ServerManager, 0, 0, 0}, //enables or disables the double server bonus for everything
	{"clearallchats", 0, EAdminFlags_ServerManager,0, 0 ,0},
	{"setname",0, EAdminFlags_SetName, 0, 0, 0},
	{"setaccountname",0, EAdminFlags_SetName, 0, 0, 0},
	{"ba",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"fr",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"rt",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"lt",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"up",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"dn",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotolv",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotosf",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"destroyjobcar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"createjobcar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"savejobcar",0, EAdminFlags_Scripter, 0, 0, 0},
	{"givepot",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"givecoke",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"givemeth",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"givemats",0, EAdminFlags_GiveMats, 0, 0, 0},
	{"setplate",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"disableanticheat",0, EAdminFlags_Scripter, 0, 0, 0},
	{"adminmessage",0, EAdminFlags_BigEars, 0, 0, 0},
	{"tempcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"acceptfamily", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"declinefamily", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"destroyfamily", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"agiverank", 0, EAdminFlags_FamilyAdmin|EAdminFlags_FactionAdmin, 0, 0, 0},
	{"makefactionleader", 0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"makefactionmember", 0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"makebiz", 0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"deathmsg", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"getpos",0, EAdminFlags_Scripter, 0, 0, 0},
	{"movehq",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"setfuel",0, EAdminFlags_Scripter, 0, 0, 0},
	{"setdrunk",0, EAdminFlags_Scripter, 0, 0, 0},
	{"interiorentrance",0, EAdminFlags_Scripter, 0, 0, 0},
	{"interiorexit",0, EAdminFlags_Scripter, 0, 0, 0},
	{"makeinterior",0, EAdminFlags_Scripter, 0, 0, 0},
	{"interiorname",0, EAdminFlags_Scripter, 0, 0, 0},
	{"payday",0, EAdminFlags_Scripter, 0, 0, 0},
	{"reloadinteriors",0, EAdminFlags_Scripter, 0, 0, 0},
	{"lockcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"unlockcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"bizentrance",0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"bizexit",0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"defammo",0, EAdminFlags_Scripter, 0, 0, 0},
	{"check",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"createfamilycar",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"destroyfamilycar",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"displayfamilycars",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"agiveturf",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"gotohq",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"reloadfamilycar",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"fuelcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"fuelcars",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"commands",0, EAdminFlags_Scripter, 0, 0, 0},
	{"dpc",0, EAdminFlags_VehicleAdmin|EAdminFlags_BasicAdmin, 0, 0, 0},
	{"carmenu",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"carowner",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"closestcar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"entercar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"putincar",0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"unban",0, EAdminFlags_Unban, 0, 0, 0},
	{"unbanid",0, EAdminFlags_Unban, 0, 0, 0},
	{"oban",0, EAdminFlags_Unban, 0, 0, 0},
	{"unbanip",0, EAdminFlags_Unban, 0, 0, 0},
	{"givegun",0, EAdminFlags_CanGiveGuns, 0, 0, 0},
	{"takegun",0, EAdminFlags_BasicAdmin|EAdminFlags_CanGiveGuns, 0, 0, 0},
	{"takeguns",0, EAdminFlags_BasicAdmin|EAdminFlags_CanGiveGuns, 0, 0, 0},
	{"givemoney",0, EAdminFlags_GiveMoney, 0, 0, 0},
	{"money",0, EAdminFlags_GiveMoney, 0, 0, 0},
	{"acceptreport",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"denyreport",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"reports",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"setstat",0, EAdminFlags_StatsManage, 0, 0, 0},
	{"setgunskill",0, EAdminFlags_Scripter, 0, 0, 0},
	{"massheal",0,EAdminFlags_MassCmds,0,0,0},
	{"massarmour",0,EAdminFlags_MassCmds,0,0,0},
	{"masskick",0,EAdminFlags_MassCmds,0,0,0},
	{"massmoney",0,EAdminFlags_MassCmds,0,0,0},
	{"massdps",0,EAdminFlags_MassCmds,0,0,0},
	{"nuke",0,EAdminFlags_Nuke,0,0,0},
	{"smallnuke",0,EAdminFlags_Nuke,0,0,0},
	{"fine",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"freeze",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"unfreeze",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"fakefine",0, EAdminFlags_AdminManage, 0, 0, 0},
	{"sfine",0, EAdminFlags_CanHide, 0, 0, 0},
	{"gotohouse",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"clearfurniture",0, EAdminFlags_HouseAdmin, 0, 0, 0},	
	{"sethouseowner",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"setfamilyowner", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"displayplayerhouses",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"sethousename",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"gotohouseinterior",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"nopms", 0, EAdminFlags_ServerManager,0 ,0 ,0},
	{"noht", 0, EAdminFlags_ServerManager,0 ,0 ,0},
	{"toghouselock",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"restartserver",0 ,EAdminFlags_ServerManager, 0, 0, 0},
	{"shutdownserver",0 ,EAdminFlags_ServerManager, 0, 0, 0},
	{"servermaintenance",0 ,EAdminFlags_ServerManager, 0, 0, 0},
	{"thisissparta",0,EAdminFlags_Nuke, 0, 0, 0},
	{"playerdonated",0 ,EAdminFlags_Donations, 0, 0 ,0},
	{"sethp", 0, EAdminFlags_StatsManage, 0, 0, 0},
	{"setarmour", 0, EAdminFlags_StatsManage, 0, 0, 0},
	{"accountname", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"makeircadmin", 0, EAdminFlags_IRCAdmin, 0, 0, 0},
	{"allowcopfix", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"agivelicense", 0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"atakelicense", 0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"deathmessages", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"createpubliccar", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"destroypubliccar", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"savepubliccar", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"desync", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"teleportmenu", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"checkanim", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"netstats", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"doanim", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"clearanim", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"setmotd", 0, EAdminFlags_ServerManager, 0, 0, 0},
	{"setamotd", 0, EAdminFlags_ServerManager|EAdminFlags_AdminManage, 0, 0, 0},
	{"amotd", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"record", 0, EAdminFlags_Scripter, 0, 0 ,0},
	{"makeirc", 0, EAdminFlags_IRCAdmin,0 ,0, 0},
	{"destroyirc", 0, EAdminFlags_IRCAdmin,0 ,0, 0},
	{"makefamilyleader", 0, EAdminFlags_FamilyAdmin,0 ,0, 0},
	{"makefamilymember", 0, EAdminFlags_FamilyAdmin,0 ,0, 0},
	{"setteam", 0, EAdminFlags_Scripter,0 ,0, 0},
	{"setskin", 0, EAdminFlags_BasicAdmin,0 ,0, 0},
	{"savefactioncar", 0,  EAdminFlags_ServerManager, 0, 0, 0},
	{"skydive", 0, EAdminFlags_Nuke, 0, 0, 0},
	{"gotopickup", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gototurf", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"jobids", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"check", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"carowner", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"destroyfactioncar", 0,  EAdminFlags_ServerManager, 0, 0, 0},
	{"createfactioncar", 0,  EAdminFlags_ServerManager, 0, 0, 0},
	{"ajail", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"unprison", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"setcarhealth", 0, EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"oban", 0, EAdminFlags_Unban, 0, 0, 0},
	{"ojail", 0, EAdminFlags_Unban, 0, 0, 0},
	{"baninfo", 0, EAdminFlags_Unban, 0, 0, 0},
	{"listbans", 0, EAdminFlags_Unban, 0, 0, 0},
	{"ahelp", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"stageplant", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"gotoplant", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"plantinfo", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"invalidreport", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"respawnallcars", 0, EAdminFlags_BasicAdmin|EAdminFlags_VehicleAdmin, 0, 0, 0},
	{"movebizexit", 0, EAdminFlags_BusinessAdmin,0 ,0, 0},
	{"connectnpc", 0, EAdminFlags_Scripter,0 ,0, 0},
	{"kicknpc", 0, EAdminFlags_Scripter,0 ,0, 0},
	{"npcs", 0, EAdminFlags_Scripter,0 ,0, 0},
	{"fuelcars", 0, EAdminFlags_VehicleAdmin, 0, 0 ,0},
	{"mute", 0, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"nmute", 2, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"pointers", 0, EAdminFlags_Scripter, 0, 0 ,0},
	{"checkprison", 0, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"guninfo",0 ,EAdminFlags_BasicAdmin, 0, 0, 0},
	{"ahide", 0, EAdminFlags_CanHide, 0, 0 ,0},
	{"checkguns", 0, EAdminFlags_BasicAdmin|EAdminFlags_CanGiveGuns, 0, 0 ,0},
	{"clearfamilycolour", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"setfamilycolour", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"setfamilyname", 0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"givebomb", 0, EAdminFlags_CanGiveGuns,0 ,0 ,0},
	{"disease", 0, EAdminFlags_StatsManage,0 ,0 ,0},
	//{"needinghelp", 1, EAdminFlags_None, 0, 0 ,0},
	{"reportban", 0, EAdminFlags_RangeBan, 0, 0 ,0},
	{"lockgate", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"checklocker", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"checktrunk", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"checkmasked", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"swapweather",0, EAdminFlags_WeatherManage,0 ,0, 0},
	{"carid", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"sendtols", 0, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"raceinfo", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"addnote", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"delnote", 0, EAdminFlags_AdminManage, 0, 0, 0},
	{"adban", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"furnitureban", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"notes", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"getfuel", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"wfine",0,EAdminFlags_StatsManage,0 ,0, 0},
	{"wcheck",0,EAdminFlags_StatsManage,0 ,0, 0},
	{"setjackpot",0,EAdminFlags_Scripter,0,0,0},
	{"playlotto",0,EAdminFlags_Scripter,0,0,0},
	{"setcarlock", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"gotoitem", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"iteminfo", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"checkspectating", 0, EAdminFlags_AdminManage, 0, 0, 0},
	{"carprices", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"cleartrunk", 0, EAdminFlags_Scripter, 0, 0, 0},
	{"tempban", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"pban", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"rangebans",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"delrangeban",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"addrangeban",0, EAdminFlags_RangeBan, 0, 0, 0},
	{"ipflags",0, EAdminFlags_ServerManager,0 ,0 ,0},
	{"delipflag",0, EAdminFlags_ServerManager,0 ,0 ,0},
	{"addipflag",0, EAdminFlags_ServerManager,0 ,0 ,0},
	{"rconcmd", 0, EAdminFlags_Scripter, 0, 0 ,0},
	{"massgivegun", 0, EAdminFlags_MassCmds, 0, 0 ,0},
	{"masstakegun", 0, EAdminFlags_MassCmds, 0, 0 ,0},
	{"masstakeguns", 0, EAdminFlags_MassCmds, 0, 0 ,0},
	{"holdinggun", 0, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"furnitureinfo", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"allowdriveby",0 , EAdminFlags_AntiCheat,0 ,0 ,0},
	{"disablegames", 0, EAdminFlags_ServerManager, 0 ,0 ,0},
	{"connectionmessages", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	//{"checkplayertest", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"makeircowner", 0, EAdminFlags_IRCAdmin, 0, 0, 0},
	{"removeircadmin", 0, EAdminFlags_IRCAdmin, 0, 0, 0},
	//{"playcustomurl",0, EAdminFlags_Scripter, 0, 0, 0},
	{"makeapartment",0, EAdminFlags_Scripter, 0, 0, 0},
	{"gototag",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"deletetag",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"deletealltags",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"rulebreakers",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"gotofamsafe",0,EAdminFlags_FamilyAdmin,0 ,0 ,0},
	{"checkpaused",0,EAdminFlags_BasicAdmin,0 ,0 ,0},
	{"itemids",0,EAdminFlags_CanGiveGuns,0 ,0 ,0},
	{"agiveitem",0,EAdminFlags_CanGiveGuns,0 ,0 ,0},
	{"atakeitem",0,EAdminFlags_BasicAdmin,0 ,0 ,0},
	{"factions",0,EAdminFlags_BasicAdmin,0 ,0 ,0},
	{"togwounded",0, EAdminFlags_Scripter,0 ,0, 0},
	{"sethunger",0, EAdminFlags_StatsManage,0 ,0, 0},
	{"masssethunger",0, EAdminFlags_MassCmds, 0, 0, 0},
	{"walltaginfo", 0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"stopvoting",0, EAdminFlags_BasicAdmin, 0, 0 ,0},
	{"cmdsnoop", 0, EAdminFlags_AdminManage,0 ,0, 0},
	{"oremoveadmin", 0, EAdminFlags_AdminManage, 0, 0 ,0},
	{"ochangepass", 0, EAdminFlags_ServerManager, 0, 0 ,0},
	{"listadmins", 0, EAdminFlags_AdminManage, 0, 0 ,0},
	{"listhelpers", 0, EAdminFlags_HelperManage, 0, 0 ,0},
	{"oremovehelper", 0, EAdminFlags_HelperManage, 0, 0 ,0},
	{"sqlconnect", 0,EAdminFlags_ServerManager,0, 0, 0},
	{"sqldisconnect", 0,EAdminFlags_ServerManager,0, 0, 0},
	{"sqlquery", 0,EAdminFlags_ServerManager,0, 0, 0},
	{"deletechar", 0,EAdminFlags_ServerManager,0, 0, 0},
	{"deleteaccount", 0,EAdminFlags_ServerManager,0, 0, 0},
	{"tempname", 0, EAdminFlags_SetName,0, 0, 0},
	{"makewarehouse",0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"deletewarehouse",0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"warehouseowner",0, EAdminFlags_BusinessAdmin, 0, 0, 0},
	{"svmottosallowed",0, EAdminFlags_ServerManager, 0, 0, 0},
	{"reloadmapping",0, EAdminFlags_ServerManager, 0, 0, 0},
	{"reloadgates",0, EAdminFlags_ServerManager, 0, 0, 0},
	{"checkbackpack",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"norptest",0, EAdminFlags_ServerManager, 0, 0, 0},
	{"turfflags",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"togglefamilytype",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"givebikeperms",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"lifeprisonsentence",0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"mappinginfo",0, EAdminFlags_Scripter, 0, 0, 0},
	{"deleteallflammables",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"givemaskperms",0, EAdminFlags_BasicAdmin, 0, 0, 0},
	{"givedonatorpackage",0, EAdminFlags_Donations, 0, 0, 0},
	{"editdrug",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"givedrug",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"reloaddrug", 0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"reloadbusiness",0, EAdminFlags_Scripter, 0, 0, 0},
	{"makestoragearea",0, EAdminFlags_Scripter, 0, 0, 0},
	{"reloadstorage",0, EAdminFlags_Scripter, 0, 0, 0},
	{"gotostoragearea",0, EAdminFlags_Scripter, 0, 0, 0},
	{"refreshlogs",0, EAdminFlags_Scripter, 0, 0, 0},
	{"destroyallevidence",0, EAdminFlags_Scripter, 0, 0, 0},
	{"deleteevidence",0, EAdminFlags_Scripter, 0, 0, 0},
	{"closestevidence",0, EAdminFlags_Scripter, 0, 0, 0},
	{"delfamnote",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"addfamnote",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"famnotes",0, EAdminFlags_FamilyAdmin, 0, 0, 0},
	{"factionoff",0, EAdminFlags_FactionAdmin, 0, 0, 0},
	{"houseentrance",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"houseexit",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"createhouse",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"reloadhouse",0, EAdminFlags_HouseAdmin, 0, 0, 0},
	{"reviewdrug",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"deletedrug",0, EAdminFlags_GiveDrugs, 0, 0, 0},
	{"reloaditems",0, EAdminFlags_Scripter, 0, 0, 0},
	{"nonrpname", 0 , EAdminFlags_BasicAdmin, 0, 0, 0}
	//{"requestchat",0, 0, 0, 0, 0},
	//{"requesthelp",0, 0, 0, 50, 0}
};
enum EAdminCmdAlts {
	EACmdAltName[32],
	EACmdAltAltName[32],
};

enum {
	EAdminDialog_ModifyAdmin = EAdminDialog_Base + 1,
	EAdminDialog_SetAdminFlag,
	EAdminDialog_AdminHelp,
	EAdminDialog_AdminCmdHelp,
	EAdminDialog_SetStatMain,
	EAdminDialog_StatEdit,
	EAdminDialog_RequestChat,
	EAdminDialog_CarMenu,
	EAdminDialog_TPMenu,
	EAdminDialog_SetJob,
	EAdminDialog_SetSex,
	EAdminDialog_SetDonateRank,
	EAdminDialog_SetFightStyle,
	EAdminDialog_SetKicksAJBans,
	EAdminDialog_TestDecide,
	EAdminDialog_DoNothing,
	EAdminDialog_NonRPName,
	EAdminDialog_End = EAdminDialog_DoNothing,
}
enum {
	ERP_CheckingTest,
	ERP_DoingTest,
}
enum {
	EAdmin_SettingNumKicks,
	EAdmin_SettingNumBans,
	EAdmin_SettingNumAJails,
}
new admincmdalts[][EAdminCmdAlts] = {
	{"admin","a"},
	{"dpc", "displayplayercars"},
	{"setworld", "setvw"},
	{"setint", "setint"},
	{"getplayervar","getpvar"},
	{"getplayervartype","getpvartype"},
	{"getplayervars","getpvars"},
	{"setplayervar","setpvar"},
	{"deleteplayervar","deletepvar"},
	{"deleteplayervar","delpvar"},
	{"adminduty","aduty"},
	{"newbie","n"},
	{"newb","n"},
	{"takecookie","taketaco"},
	{"givecookie","givetaco"},
	{"givecookie","taco"},
	{"givecookie","cookie"},
	{"adminmessage","am"},
	{"disableanticheat","disableac"},
	{"disableanticheat","togac"},
	{"displayfamilycars","dfc"},
	{"acceptreport","ar"},
	{"denyreport","dr"},
	{"invalidreport","ir"},
	{"massarmour","massarmor"},
	{"setarmour","setarmor"},
	{"displayplayerhouses","dph"},
	{"massheal","masshp"},
	{"entercar","drivecar"},
	{"setaccountname","setaccount"},
	{"allowcopfix","acf"},
	{"teleportmenu","tpmenu"},
	{"fixcar","fixveh"},
	{"tempcar","veh"},
	{"thisissparta","sparta"},
	{"unprison","unjail"},
	{"ajail","prison"},
	{"ajail","aprison"},
	{"setcarhealth","setvehiclehealth"},
	{"destroycar","destroyvehicle"},
	{"ahelp","ah"},
	{"checkguns","checkweapons"},
	{"respawnallcars","respawncars"},
	{"respawnallcars","rac"},
	{"spectate","spec"},
	{"setfamilycolour","setfamilycolor"},
	{"furnitureban", "furnban"},
	{"lifeprisonsentence","lps"},
	{"nonrpname", "nrn"}
};

enum ESetStatType {
	ESetStatType_Int,
	ESetStatType_String,
	ESetStatType_Float,
	ESetStatType_CallFunction, //calls function in pvar name, only 2 params, playerid, and target user
};

enum ESetStatOptions {
	ESetStatName[64],
	ESetStatPVarName[64],
	ESetStatType:ESetStatOptionType,
	EAdminFlags:ESetStatRequiredFlag,
	ESetStatSendWarning,
};

new SetStatOptions[][ESetStatOptions] = {
											{"Level","Level",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Bank","Bank",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Respect","RespectPoints",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Connect Time","ConnectTime",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Donator Points","DonatePoints",ESetStatType_Int, EAdminFlags_Donations, 1},
											{"Newb Rank","NewbieRank",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Accent","Accent",ESetStatType_String, EAdminFlags_StatsManage, 0},
											{"Donate Rank","ShowSetDonateRank",ESetStatType_CallFunction, EAdminFlags_Donations, 1},
											{"Max Cars","MaxCars",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Max Houses","MaxHouses",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Max Businesses","MaxBusinesses",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Cookies","Cookies",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Set Job","ShowSetJobMenu",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Set Skill Level","ShowSetSkillMenu",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Set Sex","ShowSetSexMenu",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Furniture Tokens", "FurnitureTokens",ESetStatType_Int, EAdminFlags_StatsManage, 1},
											{"Set Fight Style","ShowSetFightStyleMenu",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Set NumKicks","setNumKicksCall",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Set NumBans","setNumBansCall",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1},
											{"Set NumAJails","setNumAJailsCall",ESetStatType_CallFunction, EAdminFlags_StatsManage, 1}
										};
										
enum ETeleportMenuOptions {
	ETPMenuName[64],
	Float:ETPMenuX,
	Float:ETPMenuY,
	Float:ETPMenuZ,
	Float:ETPMenuAngle,
	ETPMenuInterior,
	ETPMenuVW,
};

new TeleportOptions[][ETeleportMenuOptions] = {{"Blueberry",0.0,0.0,5.0,0.0,0,0},
												{"All Saints",1188.721313, -1327.980712, 13.560094, 266.963989,0,0},
												{"County General",2002.118896, -1459.253051, 13.976039, 91.969909, 0, 0},
												{"Unity",1845.091552, -1883.868286, 13.430412, 87.590660,0,0},
												{"Airport",1549.765625, -2327.221435, 13.554566, 305.962677,0,0},
												{"Verona Beach",272.020385, -1855.016845, 3.138944, 167.781250,0,0},
												{"Skate Park",1923.343627, -1402.317626, 13.570312, 102.607337,0,0},
												{"VIP Shop",1230.494628, -1654.971801, 11.796875, 278.702331,0,0},
												{"Hitman HQ",979.977172, -1534.099365, 13.576638, 258.672241,0,0},
												{"SAN News",771.163452, -1345.748779, 13.524566, 85.420623,0,0},
												{"Bank",1454.840698, -1032.919555, 23.656250, 275.615692,0,0},
												{"Jefferson",2154.411865, -1146.687744, 24.557231, 354.810729,0,0},
												{"Low Riders Garage",2645.167480, -2024.723754, 13.546875, 182.554824,0,0},
												{"Wheel Archs Garage",-2707.022949, 217.767456, 4.179687, 95.784019,0,0},
												{"TransFender Garage",1041.522827, -1039.072021, 31.771018, 356.526794,0,0},
												{"Pier",2760.265136, -2452.654296, 13.542481, 183.878387,0,0},
												{"FBI HQ",345.805755, -1529.614501, 33.342090, 149.987747,0,0},
												{"Ganton Gym",2231.828857, -1730.195434, 13.382812, 89.104393,0,0},
												{"Bayside", -2276.782226, 2352.660888, 4.565527, 55.755527,0 ,0},
												{"Dillimore", 665.454406, -580.834106, 16.063018, 89.482643, 0, 0},
												{"Palomino Creek", 2271.369140, 27.635885, 26.166128, 265.498046, 0, 0},
												{"LS Stadium", 2683.412109, -1683.787841, 9.150171, 100.126068,0 ,0},
												{"Insurance", 2060.800781, -1910.922851, 13.546875, 271.402252, 0, 0},
												{"Fishing Docks", 385.550140, -2075.771484, 7.835937, 273.933563, 0, 0}
											   };
										

adminOnGameModeInit() {
	for(new i=0;i<sizeof(admincmdalts);i++) {
		Command_AddAltNamed( admincmdalts[i][EACmdAltName], admincmdalts[i][EACmdAltAltName]);
	}
	Command_AddAltNamed("helperchat", "ht");
	Command_AddAltNamed("accepthelpreport", "ahr");
	mysql_function_query(g_mysql_handle, "SELECT `motd`,`amotd`,`tax`,`jackpot`,`doublebonus`,`interest`,`rptest` FROM `misc`", true, "OnLoadMOTD", "");
}
public adminOnPlayerConnect(playerid) {
	unloadCmds(playerid);
	checkBans(playerid);
	clearPMBlocks(playerid);
	return 1;
}
clearPMBlocks(playerid) {
	for(new i=0; i<MAX_BLOCKED_PM; i++) {
		BLOCKED_PMS[playerid][i] = -1; //Set all their blocked contacts to -1, we don't need to do this on disconnect if it happens on login
	}
	return 1;
}
adminOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	restoreSpectators(playerid);
}
isCmdAllowed(playerid, cmdidx) {
	new EAdminFlags:alevel,ctime,nlevel,level;
	level = GetPVarInt(playerid, "Level");
	ctime = GetPVarInt(playerid, "ConnectTime");
	alevel = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	nlevel = GetPVarInt(playerid, "NewbieRank");
	if(level >= admincmds[cmdidx][ECmdMinLevel] && admincmds[cmdidx][ECmdMinLevel] != 0) {
		return 1;
	}
	if(_:(alevel & admincmds[cmdidx][ECmdAdminFlags]) != 0 && admincmds[cmdidx][ECmdAdminFlags] != EAdminFlags_None) {
		return 1;
	}
	if(level >= admincmds[cmdidx][ECmdMinConnectTime] && admincmds[cmdidx][ECmdMinConnectTime] != 0) {
		return 1;
	}
	if(nlevel >= admincmds[cmdidx][ECmdNewbLevel] && admincmds[cmdidx][ECmdNewbLevel] != 0) {
		return 1;
	}
	if(ctime < admincmds[cmdidx][ECmdMaxConnectTime] && admincmds[cmdidx][ECmdMaxConnectTime] != 0) {
		return 1;
	}
	return 0;
}
public adminOnPlayerLogin(playerid) {
	loadCmds(playerid);
	new ajailtime = GetPVarInt(playerid, "AJailReleaseTime");
	if(ajailtime != 0) {
		AJail(playerid, ajailtime-gettime(), "Incomplete Jail Time");
		sendPlayerNotes(playerid, playerid, 1, 1);
	}
	sendAdminMOTD(playerid);
}
SetCmdAlts(cmdidx, playerid, bool:enable) {
	for(new i=0;i<sizeof(admincmdalts);i++) {
		if(!strcmp(admincmds[cmdidx][ECmdName],admincmdalts[i][EACmdAltName])) {
			Command_SetPlayerNamed(admincmdalts[i][EACmdAltAltName], playerid, enable);
		}
	}
	return 1;
}
YCMD:restartserver(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Restarts the server");
		return 1;
	}
	foreach(Player, i) {
		saveCharacterData(i);
	}
	SendRconCommand("gmx");
	return 1;
}
YCMD:shutdownserver(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shuts down the server");
		return 1;
	}
	SendRconCommand("exit");
	return 1;
}
YCMD:servermaintenance(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Locks the server");
		return 1;
	}
	SendRconCommand("password getoutnow");
	servermaintenance = 1;
	return 1;
}
YCMD:rconcmd(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Performs an RCON command");
		return 1;
	}
	new cmd[128];
	if(!sscanf(params,"s[128]",cmd)) {
		SendRconCommand(cmd);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /rconcmd [cmd]");
	}
	return 1;
}
YCMD:massgivegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives everyone a gun");
		return 1;
	}
	new gun,ammo,ignoreslot;
	if(!sscanf(params,"dD(-1)D(0)",gun,ammo,ignoreslot)) {
		new slot = GetWeaponSlot(gun);
		if(slot == -1) {
			SendClientMessage(playerid, X11_RED3, "Invalid Weapon ID");
			return 1;
		}
		foreach(Player, i) {
			new curgun,curammo;
			GetPlayerWeaponDataEx(i, slot, curgun, curammo);
			if(curgun != 0 && ignoreslot != 1) {
				continue;
			}
			GivePlayerWeaponEx(i, gun, ammo);
		}
		new msg[128];
		new weapon[32];
		GetWeaponNameEx(gun, weapon, sizeof(weapon));
		format(msg,sizeof(msg), "An Admin has given everyone a: %s with %d bullets",weapon,ammo);
		SendClientMessageToAll(COLOR_CUSTOMGOLD, msg);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /massgivegun [gun] (ammo) (ignoreslot)");
	}
	return 1;
}
YCMD:masstakegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes a gun away from everyone");
		return 1;
	}
	new gun;
	if(!sscanf(params, "d", gun)) {
		new curgun,curammo;
		new slot = GetWeaponSlot(gun);
		if(slot == -1) {
			SendClientMessage(playerid, X11_RED3, "Invalid Weapon ID");
			return 1;
		}
		foreach(Player, i) {
			GetPlayerWeaponDataEx(i, slot, curgun, curammo);
			if(curgun == gun) {
				RemovePlayerWeapon(i, gun);
			}
		}
		new msg[128];
		new weapon[32];
		GetWeaponNameEx(gun, weapon, sizeof(weapon));
		format(msg,sizeof(msg), "An Admin has taken everyones %s away",weapon);
		SendClientMessageToAll(COLOR_CUSTOMGOLD, msg);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /masstakegun [gunid]");
	}
	return 1;
}
YCMD:masstakeguns(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes everyones guns away");
		return 1;
	}
	foreach(Player, i) {
		ResetPlayerWeaponsEx(i);
	}
	SendClientMessageToAll(COLOR_CUSTOMGOLD, "An Admin has taken everyones guns away");
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	return 1;
}
YCMD:massdps(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes a gun away from everyone");
		return 1;
	}
	new dps;
	if(!sscanf(params, "d", dps)) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				new cdps = GetPVarInt(i, "DonatePoints");
				cdps += dps;
				SetPVarInt(i, "DonatePoints", cdps);
			}
		}
		new msg[128];
		format(msg,sizeof(msg), "An Admin has given everyone %d donator points",dps);
		SendClientMessageToAll(COLOR_CUSTOMGOLD, msg);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /massdps [dps]");
	}
	return 1;
}
YCMD:masscommand(playerid, params[], help) {
	new cmd[128], cmd_cpy[128];
	if(!sscanf(params, "s[128]", cmd)) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				strmid(cmd_cpy, cmd, 0, strlen(cmd), sizeof(cmd_cpy));	
				Command_ReProcess(i, cmd_cpy, false);
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /masscommand [cmd]");
	}
	return 1;
}
YCMD:holdinggun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all players holding a certain weapon");
		return 1;
	}
	new gun;
	new msg[128];
	if(!sscanf(params, "d", gun)) {
			new curgun,curammo;
			new slot = GetWeaponSlot(gun);
			if(slot == -1) {
				SendClientMessage(playerid, X11_RED3, "Invalid Weapon ID");
				return 1;
			}
			format(msg, sizeof(msg), "** Players holding weapon: %d", gun);
			SendClientMessage(playerid, X11_WHITE, msg);
			foreach(Player, i) {
				GetPlayerWeaponDataEx(i, slot, curgun, curammo);
				if(curgun == gun) {
					format(msg, sizeof(msg), "%s[%d]: %d - %d",GetPlayerNameEx(i, ENameType_CharName),i,curgun,curammo);
					SendClientMessage(playerid, GetPlayerWeaponEx(i)==curgun?X11_TOMATO_2:COLOR_CUSTOMGOLD, msg);
				}
			}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /holdinggun [gun]");
	}
	return 1;
}
/*
	Type = 0 = first time,
	1 = nonrp
	2 = taken
	3 = too long
*/
showNONRPNameDialog(playerid, type = 0) {
	if(type == 0) {
		ShowPlayerDialog(playerid, EAdminDialog_NonRPName, DIALOG_STYLE_INPUT, "Non RP Name", "Your name does not meet the standards of the server, please choose a new RP Name.  Example: John_Smith", "Ok","Cancel");	
	} else if(type == 1) {
		ShowPlayerDialog(playerid, EAdminDialog_NonRPName, DIALOG_STYLE_INPUT, "Non RP Name", "Your name does not meet the standards of the server, please choose a new RP Name.  Example: John_Smith\n{FF0000}This name is not acceptable, please choose another name.", "Ok","Cancel");	
	} else if(type == 2) {
		ShowPlayerDialog(playerid, EAdminDialog_NonRPName, DIALOG_STYLE_INPUT, "Non RP Name", "Your name does not meet the standards of the server, please choose a new RP Name.  Example: John_Smith\n{FF0000}This name is in use! Please enter another one!", "Ok","Cancel");	
	} else if(type == 3) {
		ShowPlayerDialog(playerid, EAdminDialog_NonRPName, DIALOG_STYLE_INPUT, "Non RP Name", "Your name does not meet the standards of the server, please choose a new RP Name.  Example: John_Smith\n{FF0000}This name is too long! Please enter another one!", "Ok","Cancel");	
	}
	
}
YCMD:nonrpname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Forces a player to change their name");
		return 1;
	}
	new target;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found.");
			return 1;
		}
		format(msg, sizeof(msg), "[AdmWarn] %s has forced %s to change thier name.",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(target, ENameType_CharName));
		ABroadcast(X11_YELLOW,msg,EAdminFlags_All);
		showNONRPNameDialog(target);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /nonrpname [playerid/name]");
	}
	return 1;
}
YCMD:playerdonated(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Rewards a player for donating money");
		return 1;
	}
	new playa, money,msg[128];
	if(!sscanf(params, "k<playerLookup_acc>d", playa, money)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found.");
			return 1;
		}
		format(msg, sizeof(msg), "AdmWarn: %s rewarded %s for donating $%s",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName), getNumberString(money));
		ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
		new DPs = GetPVarInt(playa, "DonatePoints");
		DPs += floatround((DP_VALUE*100)*money);
		SetPVarInt(playa, "DonatePoints", DPs);
		format(msg, sizeof(msg), "You have been rewarded for donating $%s! Thank you!", getNumberString(money));
		SendClientMessage(playa, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /playerdonated [playerid/name] [amount(dollars)]");
	}
	return 1;
}
YCMD:givedonatorpackage(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player a donator package");
		return 1;
	}
	new playa, package;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, package)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found.");
			return 1;
		}
		if(package < 5 || package > 7) {
			SendClientMessage(playerid, X11_TOMATO_2, "Wrong syntax, packages are: 5 - Bronze, 6 - Silver, 7 - Gold");
			return 1;
		}
		giveDonatorPackage(playerid, playa, package);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givedonatorpackage [playerid/name] [package(5 - Bronze, 6 - Silver, 7 - Gold)]");
	}
	return 1;
}
YCMD:getpos(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gets a players position");
		return 1;
	}
	new Float:X,Float:Y,Float:Z,Float:A;
	new interior, vw, playa;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		GetPlayerPos(playa, X, Y, Z);
		GetPlayerFacingAngle(playa, A);
		interior = GetPlayerInterior(playa);
		vw = GetPlayerVirtualWorld(playa);
		format(msg, sizeof(msg), "%s's pos: X: %f Y: %f Z: %f Angle: %f",GetPlayerNameEx(playa, ENameType_CharName), X, Y, Z, A);
		SendClientMessage(playerid, X11_WHITE, msg);
		format(msg, sizeof(msg), "Interior: %s Virtual World: %s", getNumberString(interior), getNumberString(vw));
		SendClientMessage(playerid, X11_WHITE, msg);
		if(IsPlayerInAnyVehicle(playa)) {
			new carid = GetPlayerVehicleID(playa);
			GetVehiclePos(carid, X, Y, Z);
			vw = GetVehicleVirtualWorld(carid);
			GetVehicleZAngle(carid, A);
			format(msg, sizeof(msg), "Vehicle %d's pos: X: %f Y: %f Z: %f Angle: %f",carid, X, Y, Z, A);
			SendClientMessage(playerid, X11_WHITE, msg);
			format(msg, sizeof(msg), "Virtual World: %s", getNumberString(vw));
			SendClientMessage(playerid, X11_WHITE, msg);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /getpos [playerid/name]");
	}
	return 1;
}
YCMD:ban(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Bans a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to ban %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been banned by %s: %s",GetPlayerNameEx(playa,ENameType_AccountName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		SendGlobalAdminMessage(X11_TOMATO_2,msg);
		if(GetPVarInt(playa, "AdminHidden") == 2) {
			KickEx(playa, msg);
			incPlayerBans(playa);
		} else {
			BanPlayer(playa, reason, playerid, false, 604800);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ban [playerid/name] [reason]");
	}
	return 1;
}
YCMD:tempban(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Bans a player");
		return 1;
	}
	new reason[128],msg[128],hours;
    if (!sscanf(params, "k<playerLookup_acc>ds[128]", playa, hours, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to ban %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been temp banned by %s for %d hours: %s",GetPlayerNameEx(playa,ENameType_AccountName),GetPlayerNameEx(playerid,ENameType_AccountName),hours,reason);
		SendGlobalAdminMessage(X11_TOMATO_2,msg);
		hours *= 3600;
		if(GetPVarInt(playa, "AdminHidden") == 2) {
			KickEx(playa, msg);
			incPlayerBans(playa);
		} else {
			BanPlayer(playa, reason, playerid, false, hours);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /tempban [playerid/name] [hours] [reason]");
	}
	return 1;
}
YCMD:pban(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Bans a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to ban %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been pbanned by %s: %s",GetPlayerNameEx(playa,ENameType_AccountName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		SendGlobalAdminMessage(X11_TOMATO_2,msg);
		if(GetPVarInt(playa, "AdminHidden") == 2) {
			KickEx(playa, msg);
			incPlayerBans(playa);
		} else {
			BanPlayer(playa, reason, playerid, false, 0);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /pban [playerid/name] [reason]");
	}
	return 1;
}
YCMD:gpciban(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Bans a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to ban %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been GPCI banned by %s: %s",GetPlayerNameEx(playa,ENameType_AccountName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
		if(GetPVarInt(playa, "AdminHidden") == 2) {
			KickEx(playa, msg);
			incPlayerBans(playa);
		} else {
			BanPlayer(playa, reason, playerid, true, 0);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gpciban [playerid/name] [reason]");
	}
	return 1;
}
YCMD:unban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unbans a player");
		return 1;
	}
	new name[(MAX_PLAYER_NAME*2)+1];
	query[0] = 0;//[256];
	if(!sscanf(params, "s[" #MAX_PLAYER_NAME "]", name)) {
		mysql_real_escape_string(name, name);
		format(query, sizeof(query), "DELETE FROM `bans` WHERE `accountid` = (SELECT `id` FROM `accounts` WHERE `username` = \"%s\") AND `nounban` = 0",name);
		mysql_function_query(g_mysql_handle, query, true, "BanDeleteCallback", "dds",playerid, 0, name);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unban [accountname]");
	}
	return 1;
}
YCMD:unbanid(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes a ban ID");
		return 1;
	}
	new banid;
	query[0] = 0;//[256];
	if(!sscanf(params, "d", banid)) {
		format(query, sizeof(query), "DELETE FROM `bans` WHERE `id` = %d AND `nounban` = 0",banid);
		mysql_function_query(g_mysql_handle, query, true, "BanDeleteCallback", "dds",playerid, 3, banid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unbanid [banid]");
	}
	return 1;
}
public BanDeleteCallback(playerid, type, name[]) {
	new msg[128];
	new numrows = mysql_affected_rows();
	if(numrows < 1) {
		SendClientMessage(playerid, X11_YELLOW, "* Ban not found.");
		return 1;
	} else {
		format(msg, sizeof(msg), "* %d matching bans removed", numrows);
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
	if(type == 2) {
		format(msg, sizeof(msg), "[AdmWarn]: %s has unbanned BanID %d",GetPlayerNameEx(playerid, ENameType_AccountName), name[0]);
	} else if(type == 1) {
		format(msg, sizeof(msg), "[AdmWarn]: %s has unbanned IP %s",GetPlayerNameEx(playerid, ENameType_AccountName), name);
	} else {
		format(msg, sizeof(msg), "[AdmWarn]: %s has unbanned %s",GetPlayerNameEx(playerid, ENameType_AccountName), name);
	}
	ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
	return 1;
}
public AccountLookupCallback(playerid, name[]) {
	new msg[128];
	new accountname[MAX_PLAYER_NAME+1];
	new rows,fields;
	cache_get_data(rows,fields);
	if(rows < 1) {
		SendClientMessage(playerid, X11_YELLOW, "* Account not found");
		return 1;
	}
	new accid;
	cache_get_row(0, 0, accountname);
	accid = strval(accountname);
	cache_get_row(0, 1, accountname);
	format(msg, sizeof(msg), "Account name tied to character: %s[%d]",accountname,accid);
	SendClientMessage(playerid, X11_YELLOW, msg);
	SendClientMessage(playerid, X11_WHITE, "* Characters *");
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 3, accountname);
		accid = strval(accountname);
		cache_get_row(i, 2, accountname);
		format(msg, sizeof(msg), "* %s[%d]",accountname,accid);
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
	return 1;
}
YCMD:accountname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lookup an account by character name");
		return 1;
	}
	new name[(MAX_PLAYER_NAME*2)+1];
	query[0] = 0;//[256];
	if(!sscanf(params, "s[" #MAX_PLAYER_NAME "]", name)) {
		mysql_real_escape_string(name, name);
		format(query, sizeof(query), "SELECT `c`.`accountid`,`a`.`username`,`c`.`username`,`c`.`id` FROM `characters` `c` INNER JOIN `accounts` `a` ON `a`.`id` = `c`.`accountid` WHERE ((SELECT `c1`.`accountid` FROM `characters` `c1` WHERE `c1`.`username` = \"%s\") = `c`.`accountid` OR `c`.`accountid` = (SELECT id from accounts where username = \"%s\"))",name,name);
		mysql_function_query(g_mysql_handle, query, true, "AccountLookupCallback", "ds",playerid,name);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /accountname [accountname]");
	}	return 1;
}
YCMD:unbanip(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Unbans an IP");
		return 1;
	}
	new name[(MAX_PLAYER_NAME*2)+1];
	query[0] = 0;//[128];
	if(!sscanf(params, "s[" #MAX_PLAYER_NAME "]", name)) {
		mysql_real_escape_string(name, name);
		format(query, sizeof(query), "DELETE FROM `bans` WHERE `ip` = INET_ATON(\"%s\") AND `nounban` = 0",name);
		mysql_function_query(g_mysql_handle, query, true, "BanDeleteCallback", "dds",playerid,1,name);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unbanip [accountname]");
	}
	return 1;
}
YCMD:oban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Bans an offline player");
		return 1;
	}
	new username[(MAX_PLAYER_NAME*2)+1],reason[128];
	if(!sscanf(params,"s[" #MAX_PLAYER_NAME "]s[128]",username,reason)) {
		mysql_real_escape_string(username, username);
		format(query, sizeof(query), "select INET_NTOA(`ip`),a.id FROM connections ca INNER JOIN characters c on c.id = ca.charid inner join accounts a on a.id = c.accountid where (c.username = \"%s\" OR a.username = \"%s\") ORDER BY ca.id DESC LIMIT 0,1",username,username);
		mysql_function_query(g_mysql_handle, query, true, "OnOfflineBanPlayer", "dss",playerid,username,reason);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /oban [username] [reason]");
	}
	return 1;
}

public OnOfflineBanPlayer(playerid, name[],reason[]) {
	new ip[16],accid[16];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "* User not found");
		return 1;
	}
	cache_get_row(0,0,ip);
	cache_get_row(0,1,accid);
	format(query, sizeof(query),"INSERT INTO `bans` SET `bannerid` = %d,`reason` = \"%s\",`accountid` = %d,`ip` = INET_ATON(\"%s\")",GetPVarInt(playerid, "AccountID"), reason, strval(accid),ip);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(query, sizeof(query), "[AdmWarn]: %s has OBanned %s: %s",GetPlayerNameEx(playerid, ENameType_AccountName), name, reason);
	ABroadcast(X11_TOMATO_2, query, EAdminFlags_All);
	return 1;
}
YCMD:ojail(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Offline jails a user");
		return 1;
	}
	new jailtime;
	new reason[128], msg[128];
	new name[(MAX_PLAYER_NAME*2)+1];
	if(!sscanf(params, "s[" #MAX_PLAYER_NAME "]ds[128]", name,jailtime,reason)) {
		mysql_real_escape_string(name, name);
		format(query, sizeof(query), "UPDATE `accounts` SET `ajailtime` = %d WHERE `username` = \"%s\"",jailtime*60,name);
		mysql_function_query(g_mysql_handle, query, true, "OnOfflineJail", "dsd",playerid,name,jailtime);
		format(msg, sizeof(msg), "OJailed by %s for %d minutes: Reason: %s",GetPlayerNameEx(playerid,ENameType_AccountName),jailtime,reason);
		oAddAccountNote(name, playerid, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ojail [username] [time] [reason]");
	}
	return 1;
}
forward OnOfflineJail(playerid,name[],time);
public OnOfflineJail(playerid,name[],time) {
	new numrows = mysql_affected_rows();
	if(numrows < 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "* User not found");
	} else {
		new msg[128];
		format(msg, sizeof(msg), "[AMSG] %s has been OJailed by %s for %d minutes",name,GetPlayerNameEx(playerid,ENameType_AccountName),time);
		ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
	}
	return 1;
}
oAddAccountNote(targetName[], setter, reason[]) {
	mysql_real_escape_string(targetName, targetName);
	format(query, sizeof(query), "SELECT `id` FROM `accounts` where `username` = \"%s\"",targetName);
	mysql_function_query(g_mysql_handle, query, true, "OnOAddAccountNote", "sds",targetName,setter,reason);
}
forward OnOAddAccountNote(targetName[], setter, reason[]);
public OnOAddAccountNote(targetName[], setter, reason[]) { //Adds account note while offline
	new accountname[MAX_PLAYER_NAME+1];
	new rows,fields;
	mysql_real_escape_string(reason, reason);
	cache_get_data(rows,fields);
	if(rows < 1) {
		SendClientMessage(setter, X11_YELLOW, "* Couldn't add note, account not found.");
		return 1;
	}
	new accid;
	cache_get_row(0, 0, accountname);
	accid = strval(accountname);
	format(query, sizeof(query), "INSERT INTO `accountnotes` SET `note` = \"%s\",`accountid` = %d,`setby` = %d",reason,accid,GetPVarInt(setter, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	return 1;
}
YCMD:ajail(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Admin Jails a player");
		return 1;
	}
	new user, time, reason[64];
	new string[128];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, time, reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		format(string, sizeof(string), "Jailed for %d minutes: %s",time,reason);
		AddAccountNote(user, playerid, string);
		format(string, sizeof(string), "System: %s was jailed for %d minutes by %s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), time, GetPlayerNameEx(playerid, ENameType_AccountName),reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, string);
		AJail(user, time*60, reason);
		#if DECIDEBAN_ONAJAIL
		decideBan(user);
		#endif
		incPlayerAJails(user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ajail [playerid/name] [time(mins)] [reason]");
	}
	return 1;
}
YCMD:sjail(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Silently jail someone");
		return 1;
	}
	new user, time, reason[64];
	new string[128];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, time, reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		format(string, sizeof(string), "System: %s was sjailed for %d minutes by %s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), time, GetPlayerNameEx(playerid, ENameType_AccountName),reason);
		ABroadcast(X11_TOMATO_2, string, EAdminFlags_All);
		AJail(user, time*60, reason);
		incPlayerAJails(user);
		#if DECIDEBAN_ONAJAIL
		decideBan(user);
		#endif
		format(string, sizeof(string), "SJailed for %d minutes: %s",time,reason);
		AddAccountNote(user, playerid, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ajail [playerid/name] [time(mins)] [reason]");
	}
	return 1;
}
YCMD:unprison(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Release someone for admin or IC prison");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "SYSTEM: %s has been released from prison by %s", GetPlayerNameEx(target, ENameType_CharName), GetPlayerNameEx(playerid, ENameType_AccountName));
		ABroadcast(X11_TOMATO_2, msg, EAdminFlags_BasicAdmin);
		SetPVarInt(target, "AJailReleaseTime",0);
		SetPVarInt(target, "ReleaseTime",0);
		decPlayerAJails(target);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unprison [playerid/name]");
	}
	return 1;
}
YCMD:allowdriveby(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Desyncs a player");
		return 1;
	}
	new id;
	if(!sscanf(params,"k<playerLookup_acc>", id)) {
		if(!IsPlayerConnectEx(id)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(GetPVarInt(id, "AllowDriveBy") == 1) {
			DeletePVar(id, "AllowDriveBy");
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Drive Bys disabled for player!");
		} else {
			SetPVarInt(id, "AllowDriveBy",1);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Drive Bys enabled for player!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /allowdriveby [playerid/name]");
	}
	return 1;
}
YCMD:desync(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Desyncs a player");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(GetPVarType(user, "Desynced") != PLAYER_VARTYPE_NONE) {
			DeletePVar(user, "Desynced");
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Player resynced!");
		} else {
			SetPVarInt(user, "Desynced", 1);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Player desynced!");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /desync [playerid/name]");
	}
	return 1;
}
YCMD:sban(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Silently Bans a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to ban %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_Unbannable);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been sbanned by %s: %s",GetPlayerNameEx(playa,ENameType_AccountName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
		BanPlayer(playa, reason, playerid, false, DEFAULT_BAN_TIME);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sban [playerid/name] [reason]");
	}
	return 1;
}
YCMD:ip(playerid, params[], help) {
	new playa;
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new msg[128],ip[16], ip2[16];
		GetPlayerIpEx(playa,ip,sizeof(ip));
		format(msg, sizeof(msg), "%s's IP: %s",GetPlayerNameEx(playa, ENameType_CharName),ip);
		SendClientMessage(playerid, X11_YELLOW, msg);
		new sentmatchmsg = 0;
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				if(i != playa) {
					GetPlayerIpEx(i,ip2,sizeof(ip2));
					if(!strcmp(ip,ip2,true)) {
						if(sentmatchmsg == 0) {
							sentmatchmsg = 1;
							SendClientMessage(playerid, X11_WHITE, "*** Others Matching this IP ***");
						}
						format(msg, sizeof(msg), "* %s[%d]",GetPlayerNameEx(i, ENameType_CharName),i);
						SendClientMessage(playerid, X11_YELLOW, msg);
					}
				}
			}
		}
		
		SendPlayerHost(playerid, playa);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ip [playerid/name]");
	}
	return 1;
}
SendPlayerHost(playerid, target) {
	new ip[16];
	GetPlayerIpEx(target, ip, sizeof(ip));
	rdns(ip, playerid);
}
public OnDNS(host[], ip[], extra) {
}
public OnReverseDNS(ip[], host[], extra) {
	if(extra != INVALID_PLAYER_ID) {
		new msg[128];
		format(msg, sizeof(msg), "* Host: %s", host);
		SendClientMessage(extra, X11_YELLOW, msg);
	}
}

YCMD:gpci(playerid, params[], help) {
	new playa;
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new msg[128],GPCI[64];
		gpci(playa, GPCI, sizeof(GPCI));
		format(msg, sizeof(msg), "%s's GPCI: %s",GetPlayerNameEx(playa, ENameType_CharName),GPCI);
		SendClientMessage(playerid, X11_YELLOW, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /GPCI [playerid/name]");
	}
	return 1;
}
YCMD:settest(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Checks the test of a player.");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") < 2){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a teacher!");
		return 1;
	}
	new target, setvalue;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>d", target, setvalue))	{
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(setvalue > 1 || setvalue < 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid range, choose 1 to force the RP test and 0 to disable it for a certain player!");
			return 1;
		}
		if(setvalue == 0) {
			format(msg, sizeof(msg), "Tests: %s has disabled %s's test.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
			deleteQuizAnswers(target);
		} else {
			format(msg, sizeof(msg), "Tests: %s has forced the RP test on %s.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		}
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
				continue;
			}
			SendClientMessage(i, X11_YELLOW, msg);
		}
		setTest(target, setvalue);
		if(setvalue == 0) {
			ShowPlayerDialog(target, EAdminDialog_DoNothing, DIALOG_STYLE_MSGBOX, "{00BFFF}Test:","Your Test has been disabled!", "Ok", "");
		}
		SetPVarInt(playerid, "PlayerEditing", target);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /settest [playerid] [0 (disable) - 1 (force)]");
	}
	return 1;
}
YCMD:checktest(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Checks the test of a player.");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") == 0){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a helper!");
		return 1;
	}
	new target, teststep;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", target))	{
		if(!IsPlayerConnected(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(target == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot correct your own test!");
			return 1;
		}
		teststep = GetPVarInt(target, "TestStep");
		if(teststep != 4) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user hasn't posted his results yet!");
		} else {
			format(msg, sizeof(msg), "Tests: %s is checking %s's test.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
			foreach(Player, i) {
				if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
					continue;
				}
				SendClientMessage(i, X11_YELLOW, msg);
			}
			SendClientMessage(target, COLOR_CUSTOMGOLD, "An administrator or helper is now checking your RP test!");
			SetPVarInt(playerid, "PlayerEditing", target);
			setRPTest3dTextLabel(playerid, ERP_CheckingTest);
			DeletePVar(target, "TestStep");
			GetAnswers(playerid, target);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /checktest [playerid]");
	}
	return 1;
}
YCMD:checktests(playerid, params[], help) {
	new msg[128];
	if(GetPVarInt(playerid, "NewbieRank") == 0){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a helper!");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "** Players who are doing or have completed the test **");
	foreach(Player, i) {
		if(GetPVarType(i, "TestStep") != PLAYER_VARTYPE_NONE) {
			format(msg, sizeof(msg), "%s", getTestStep(i));
			SendClientMessage(playerid, X11_YELLOW, msg);
		}
	}
	return 1;
}
getTestStep(playerid) {
	new msg[128];
	if(GetPVarType(playerid, "TestStep") != PLAYER_VARTYPE_NONE) {
		new teststep = GetPVarInt(playerid, "TestStep");
		switch(teststep) {
			case 4: {
				format(msg, sizeof(msg), "* %s[%d] has completed the RP test.",GetPlayerNameEx(playerid, ENameType_RPName),playerid);
			} 
			case 2: {
				format(msg, sizeof(msg), "{FF0000}* %s[%d] is currently doing the RP test.",GetPlayerNameEx(playerid, ENameType_RPName),playerid);
			}
		}
	}
	return msg;
}
saveSpecDetails(playerid) {
	new Float:X, Float:Y, Float:Z, interior, vw, Float:HP, Float:Armour, Float:Angle;
	if(GetPlayerState(playerid) == PLAYER_STATE_SPECTATING) {
		return 0;
	}
	vw = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);
	GetPlayerPos(playerid, X, Y, Z);
	GetPlayerFacingAngle(playerid, Angle);
	GetPlayerHealth(playerid, HP);
	GetPlayerArmour(playerid, Armour);
	saveGuns(playerid, "SpecGuns");
	SetPVarFloat(playerid, "SpecX",X);
	SetPVarFloat(playerid, "SpecY",Y);
	SetPVarFloat(playerid, "SpecZ",Z);
	SetPVarFloat(playerid, "SpecAngle", Angle);
	SetPVarInt(playerid, "SpecVW",vw);
	SetPVarInt(playerid, "SpecInterior",interior);
	SetPVarFloat(playerid, "SpecHP", HP);
	SetPVarFloat(playerid, "SpecArmour", Armour);
	return 0;
}
loadSpecDetails(playerid, setpos = 1) {
	new Float:X, Float:Y, Float:Z, interior, vw, Float:HP, Float:Armour, Float:Angle;
	if(GetPVarType(playerid, "SpecX") == PLAYER_VARTYPE_NONE && setpos == 1) {
		return 0;
	}
	X = GetPVarFloat(playerid, "SpecX");
	Y = GetPVarFloat(playerid, "SpecY");
	Z = GetPVarFloat(playerid, "SpecZ");
	vw = GetPVarInt(playerid, "SpecVW");
	Angle = GetPVarFloat(playerid, "SpecAngle");
	interior = GetPVarInt(playerid, "SpecInterior");
	HP = GetPVarFloat(playerid, "SpecHP");
	Armour = GetPVarFloat(playerid, "SpecArmour");
	restoreGuns(playerid, "SpecGuns");
	SetPlayerHealthEx(playerid, HP);
	SetPlayerArmourEx(playerid, Armour);
	SetPlayerVirtualWorld(playerid, vw);
	SetPlayerInterior(playerid, interior);
	if(setpos == 1) {
		SetPlayerPos(playerid, X, Y, Z);
		SetPlayerFacingAngle(playerid, Angle);
	}
	if(GetPVarInt(playerid, "AdminDuty") != 0) {
		if(GetPVarInt(playerid, "Sex") == 0) {
			SetPlayerSkin(playerid, 217);
		} else {
			SetPlayerSkin(playerid, 211);
		}
	} else {
		SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
	}
	restoreAccessories(playerid);
	DeletePVar(playerid, "SpecX");
	DeletePVar(playerid, "SpecY");
	DeletePVar(playerid, "SpecZ");
	DeletePVar(playerid, "SpecAngle");
	DeletePVar(playerid, "SpecVW");
	DeletePVar(playerid, "SpecInterior");
	DeletePVar(playerid, "SpecHP");
	DeletePVar(playerid, "SpecArmour");
	return 0;
}
YCMD:spectate(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to spectate a player or vehicle");
		return 1;
	}
	new idx;
	new x_nr[96],tmp[96],string[100];
	x_nr = strtok(params, idx);
	if(isnull(x_nr))
	{
		SendClientMessage(playerid, X11_WHITE, "USAGE: /Spectate [Name]");
		SendClientMessage(playerid, X11_LIGHTBLUE, "Available cmds: Player, vehicle, stop");
		return 1;
	}
	if(strcmp(x_nr,"player", true) == 0)
	{
		tmp = strtok(params,idx);
		if(isnull(tmp))
		{
			SendClientMessage(playerid, X11_WHITE, "USAGE: /spectate player [playerid/partofName]");
			return 1;
		}
		new playa = sscanf_playerLookup_acc(tmp); //returnUser(tmp,playerid);
		if(!IsPlayerConnectEx(playa) || playa == playerid)
		{
			SendClientMessage(playerid, X11_GREY, "That player is offline");
			return 1;
		}
		saveSpecDetails(playerid);
		TogglePlayerSpectating(playerid, 1);
		SetTimerEx("DelayedSpectate",250,false,"ddd",playerid, playa, 0);
		format(string,sizeof(string),"* Now spectating %s",GetPlayerNameEx(playa, ENameType_CharName));
		SetPVarInt(playerid, "SpecPlayer", playa);
	} else if(strcmp(x_nr,"vehicle", true) == 0) {
		tmp = strtok(params,idx);
		if(isnull(tmp))
		{
			SendClientMessage(playerid, X11_WHITE, "USAGE: /spectate player [playerid/partofName]");
			return 1;
		}
		new carid = strval(tmp);
		if(GetVehicleModel(carid) == 0) {
			SendClientMessage(playerid, X11_RED2, "Invalid vehicle ID");
			return 1;
		}
		saveSpecDetails(playerid);
		SetTimerEx("DelayedSpectate",250,false,"ddd",playerid, carid, 1);
		SetPVarInt(playerid, "SpecVeh", carid);
		format(string,sizeof(string),"* Now spectating Vehicle %d",carid);
	} else if(strcmp(x_nr, "stop", true) == 0 || strcmp(x_nr, "off", true) == 0) {
		TogglePlayerSpectating(playerid, 0);
		//Don't delete the PVars yet, do it in OnPlayerSpawn to restore their TP and weapons
		format(string,sizeof(string),"* You have stopped spectating");
		/* removed because its now done in adminOnPlayerStateChange
		DeletePVar(playerid, "SpecPlayer");
		DeletePVar(playerid, "SpecVeh");
		loadSpecDetails(playerid);
		*/
	} else {
		//assume spec id
		format(string, sizeof(string), "/spectate player %s",x_nr);
		Command_ReProcess(playerid, string, false);
		return 1;
	}
	SendClientMessage(playerid, X11_LIGHTBLUE, string);
	return 1;
}

public DelayedSpectate(playerid, target, targettype) {
	TogglePlayerSpectating(playerid, 1);
	if(targettype == 0) {
		SetPlayerInterior(playerid, GetPlayerInterior(target));
		SetPlayerVirtualWorld(playerid, GetPlayerVirtualWorld(target));
		if(IsPlayerInAnyVehicle(target)) {
			PlayerSpectateVehicle(playerid, GetPlayerVehicleID(target));
		} else {
			PlayerSpectatePlayer(playerid, target);
		}
	} else if(targettype == 1) {
		SetPlayerVirtualWorld(playerid, GetVehicleVirtualWorld(target));
		PlayerSpectateVehicle(playerid, target);
	}
}
YCMD:makeadmin(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Manage a players admin flags");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED2, "Player not connected");
			return 1;
		}
		if(EAdminFlags:GetPVarInt(playa, "AdminFlags") & EAdminFlags_Scripter && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this command on this person!");
			return 1;
		}
		ShowAdminFlagsMenu(playerid, playa);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makeadmin [id]");
	}
	return 1;
}
YCMD:reloadcmds(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Reloads someones admin permissions");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		unloadCmds(playa);
		loadCmds(playa);
		SendClientMessage(playerid, X11_WHITE, "Commands reloaded");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /reloadcmds [playerid/name]");
	}
	return 1;
}
YCMD:adminoverride(playerid, params[], help) {
	new msg[128];
	new pass[64];
	if(!sscanf(params,"s[64]", pass)) {
		if(!strcmp(pass, dini_Get("iwrp.ini","adminoverride"))) {
			SetPVarInt(playerid, "AdminFlags", _:EAdminFlags_All);
			//RELOAD admin permissions
			unloadCmds(playerid);
			loadCmds(playerid);
			format(msg,sizeof(msg),"AdmWarn: %s(%s) has used Admin Override!",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName));
			ABroadcast(X11_RED3,msg,EAdminFlags_ServerManager);
			SendClientMessage(playerid, X11_BLUE3, "Accepted!");
		} else {
			format(msg, sizeof(msg), "AdmWarn: %s[%d] failed an admin override",GetPlayerNameEx(playerid, ENameType_CharName), playerid);
			ABroadcast(X11_RED3,msg,EAdminFlags_All);
			new numoverrides = GetPVarInt(playerid, "FailedAdminOverrides");
			if(numoverrides >= MAX_ADMIN_OVERRIDE_ATTEMPTS) {
				format(msg, sizeof(msg), "AdmWarn: %s[%d] has been banned for failing Admin Override too many times",GetPlayerNameEx(playerid, ENameType_CharName), playerid, pass);
				ABroadcast(X11_RED3,msg,EAdminFlags_All);			
				BanPlayer(playerid, "Exceeded maximum Admin Override attempts", 0, false, 0);
				return 0;
			}
			SetPVarInt(playerid, "FailedAdminOverrides", ++numoverrides);
			return 0;
		}
	}
	return 1;
}
/*
YCMD:iamatester(playerid, params[], help) {
	new msg[128];
	if(!strcmp(params,"tester123")) {
		SetPVarInt(playerid, "AdminFlags", 1675048607);
		unloadCmds(playerid);
		loadCmds(playerid);
		format(msg,sizeof(msg),"AdmWarn: %s(%s) is now a tester!",GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName));
		ABroadcast(X11_RED3,msg,EAdminFlags_All);
		SendClientMessage(playerid, X11_BLUE3, "Accepted!");
		return 1;
	}
	return 0;
}
*/
/*
YCMD:playcustomurl(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Plays a custom radio URL");
		return 1;
	}
	new radiourl[128];
	new share;
    if (!sscanf(params, "s[128]d", radiourl, share)) {
		setPlayerCustomRadioURL(playerid, radiourl, share);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /playcustomurl [url] [(0 - Private) (1 - Stream)]");
		SendClientMessage(playerid, X11_WHITE, "USAGE: {FF0000}/playcustomurl 0 0 to stop the streaming.");
	}
	return 1;
}
*/
YCMD:kick(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Kicks a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to kick %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg, sizeof(msg), "Kicked: %s",reason);
		AddAccountNote(playa, playerid, msg);
		format(msg,sizeof(msg),"[AMSG] %s has been kicked by %s: %s",GetPlayerNameEx(playa,ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		SendGlobalAdminMessage(X11_TOMATO_2,msg);
		incPlayerKicks(playa);
		KickEx(playa, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /kick [playerid/name] [reason]");
	}
	return 1;
}
YCMD:skick(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Silently Kicks a player");
		return 1;
	}
	new reason[128],msg[128];
    if (!sscanf(params, "k<playerLookup_acc>s[128]", playa, reason))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid  && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to skick %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		format(msg,sizeof(msg),"[AMSG] %s has been skicked by %s: %s",GetPlayerNameEx(playa,ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName),reason);
		ABroadcast(X11_TOMATO_2,msg,EAdminFlags_All);
		Kick(playa);
		incPlayerKicks(playa);
		format(msg, sizeof(msg), "SKicked: %s",reason);
		AddAccountNote(playa, playerid, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /skick [playerid/name] [reason]");
	}
	return 1;
}
YCMD:adminmessage(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends an admin message to the entire server");
		return 1;
	}
	if(strlen(params) <= 1) {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /adminmessage [message]");
		return 1;
	}
	new msg[256];
	format(msg, sizeof(msg), "*** %s %s: %s",getAdminName(playerid),GetPlayerNameEx(playerid, ENameType_AccountName),params);
	//SendClientMessageToAll(COLOR_FADE2, msg);
	SendSplitClientMessageToAll(COLOR_FADE2, msg, 0, 128);
	return 1;
}
YCMD:slap(playerid, params[], help) {
	new Float:X,Float:Y,Float:Z;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Slaps a player");
		return 1;
	}
	new playa;
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if((~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_CanBanAdmins) && EAdminFlags:GetPVarInt(playa, "AdminFlags") != EAdminFlags_None && playa != playerid && GetPVarInt(playa, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "%s attempted to slap %s",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(playa,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_AdminManage);
			return 1;
		}
		GetPlayerPos(playa,X,Y,Z);
		Z += 5.0;
		SetPlayerPos(playa, X, Y, Z);
		new Float:health;
		GetPlayerHealth(playa,health);
		SetPlayerHealthEx(playa, health-5);
		PlayerPlaySound(playa, 1130, X,Y,Z);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg,sizeof(msg),"[AMSG] %s has been slapped by %s",GetPlayerNameEx(playa,ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName));
			ABroadcast(X11_TOMATO_2,msg,EAdminFlags_All);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /slap [playerid/name]");
	}
	return 1;
}
YCMD:gotols(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports a player to LS");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SetPlayerPos(playerid, 1529.6,-1691.2,13.3);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerInterior(playerid, 0);
	} else {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, 0, 0);
			LinkVehicleToInterior(carid, 0);
			SetVehicleVirtualWorld(carid, 0);
			SetVehiclePos(carid, 1529.6,-1691.2,13.3);
	}
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}
YCMD:sendtols(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports a player to LS");
		return 1;
	}
	new msg[128];
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!IsPlayerInAnyVehicle(target)) {
			SetPlayerPos(target, 1529.6,-1691.2,13.3);
			SetPlayerVirtualWorld(target, 0);
			SetPlayerInterior(target, 0);
		} else {
				new carid = GetPlayerVehicleID(target);
				TPEntireCar(carid, 0, 0);
				LinkVehicleToInterior(carid, 0);
				SetVehicleVirtualWorld(carid, 0);
				SetVehiclePos(carid, 1529.6,-1691.2,13.3);
		}
		format(msg,sizeof(msg),"[AMSG] %s has been sent to Los Santos by %s",GetPlayerNameEx(target,ENameType_CharName),GetPlayerNameEx(playerid,ENameType_AccountName));
		ABroadcast(X11_TOMATO_2,msg,EAdminFlags_All);
		SendClientMessage(target, X11_ORANGE, "You have been teleported.");
	}
	return 1;
}
YCMD:gotolv(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports a player to LS");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SetPlayerPos(playerid, 1699.2, 1435.1, 10.7);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerInterior(playerid, 0);
	} else {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, 0, 0);
			LinkVehicleToInterior(carid, 0);
			SetVehicleVirtualWorld(carid, 0);
			SetVehiclePos(carid, 1699.2, 1435.1, 10.7);
	}
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}
YCMD:gotosf(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports a player to LS");
		return 1;
	}
	if(!IsPlayerInAnyVehicle(playerid)) {
		SetPlayerPos(playerid, -2027.45, 174.58, 28.49);
		SetPlayerVirtualWorld(playerid, 0);
		SetPlayerInterior(playerid, 0);
	} else {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, 0, 0);
			LinkVehicleToInterior(carid, 0);
			SetVehicleVirtualWorld(carid, 0);
			SetVehiclePos(carid, -2027.45, 174.58, 28.49);
	}
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}
YCMD:goto(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a player");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_WHITE, "Invalid player!");
			return 1;
		}
		new Float:X,Float:Y,Float:Z,vw,interior;
		GetPlayerPos(playa, X, Y, Z);
		vw = GetPlayerVirtualWorld(playa);
		interior = GetPlayerInterior(playa);
		if(IsPlayerInAnyVehicle(playerid)) {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, interior, vw);
			LinkVehicleToInterior(carid, interior);
			SetVehicleVirtualWorld(carid, vw);
			SetVehiclePos(carid, X, Y, Z);
			
		} else {
			SetPlayerPos(playerid, X, Y, Z);
		}
		SetPlayerVirtualWorld(playerid, vw);
		SetPlayerInterior(playerid,interior);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /goto [playerid/name]");
	}
	return 1;
}
YCMD:gethere(playerid, params[], help) {
	new playa;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to a player");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>", playa))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_WHITE, "Invalid player!");
			return 1;
		}
		new Float:X,Float:Y,Float:Z,vw,interior;
		GetPlayerPos(playerid, X, Y, Z);
		vw = GetPlayerVirtualWorld(playerid);
		interior = GetPlayerInterior(playerid);
		if(IsPlayerInAnyVehicle(playa)) {
			new carid = GetPlayerVehicleID(playa);
			TPEntireCar(carid, interior, vw);
			LinkVehicleToInterior(carid, interior);
			SetVehicleVirtualWorld(carid, vw);
			SetVehiclePos(carid, X, Y, Z);
			
		} else {
			SetPlayerPos(playa, X, Y, Z);
		}
		SetPlayerVirtualWorld(playa, vw);
		SetPlayerInterior(playa,interior);
		SendClientMessage(playa, X11_ORANGE, "You have been teleported.");
	} else {
		SendClientMessage(playerid, X11_WHITE,  "USAGE: /gethere [playerid/name]");
	}
	return 1;
}
YCMD:massheal(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mass heals the server");
		return 1;
	}
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			SetPlayerHealthEx(i, 98.0);
		}
	}
	SendClientMessageToAll(COLOR_LIGHTBLUE, "* An admin healed everyone!");
	return 1;
}
YCMD:massarmour(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mass armours the server");
		return 1;
	}
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			SetPlayerArmourEx(i, 98.0);
		}
	}
	SendClientMessageToAll(COLOR_LIGHTBLUE, "* An admin armoured everyone!");
	return 1;
}
YCMD:masskick(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mass kicks the server");
		return 1;
	}
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(i != playerid)
				KickEx(i, "Mass Kicked");
		}
	}
	return 1;
}
YCMD:nonewb(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles Newb Chat On/Off for the server");
		return 1;
	}
	if(nonewb == 0) {
		SendClientMessageToAll(X11_ORANGE, "   Newb chat channel has been disabled by an Admin !");
		nonewb = 1;
	} else {
		SendClientMessageToAll(X11_ORANGE, "   Newb chat channel has been enabled by an Admin !");
		nonewb = 0;
	}
	return 1;
}
YCMD:svmottosallowed(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles the server name changes On/Off for the server");
		return 1;
	}
	if(svnamechangeallowed == 0) {
		SendClientMessageToAll(X11_ORANGE, "   The server automatic mottos have been enabled by an Admin !");
		svnamechangeallowed = 1;
	} else {
		SendClientMessageToAll(X11_ORANGE, "   The server automatic mottos have been disabled by an Admin !");
		svnamechangeallowed = 0;
	}
	return 1;
}

YCMD:nopms(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles Private Messages");
		return 1;
	}
	if(nopms) {
		nopms = 0;
		SendClientMessageToAll(X11_ORANGE, "Private Messages have been enabled by an admin");
	} else {
		nopms = 1;
		SendClientMessageToAll(X11_ORANGE, "Private Messages have been disabled by an admin");
	}
	return 1;
}
YCMD:noht(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles Helper Chat");
		return 1;
	}
	if(nohelperchat) {
		nohelperchat = 0;
		ABroadcast(X11_ORANGE, "Helper Chat has been enabled by an admin", EAdminFlags_All);
	} else {
		nohelperchat = 1;
		ABroadcast(X11_ORANGE, "Helper Chat has been disabled by an admin", EAdminFlags_All);
	}
	return 1;
}
YCMD:blockpm(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to block / unblock a user from sending you PM's");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup>", user)) {
		new msg[128];
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not connected!");
			return 1;
		}
		if(user == playerid) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot block yourself!");
			return 1;
		}
		if(EAdminFlags:GetPVarInt(user, "AdminFlags") & EAdminFlags_BasicAdmin || GetPVarInt(user, "NewbieRank") != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot block admins / helpers, if you have an issue with any helper or admin, do report them on the forums.");
			return 1;
		}
		for(new i=0; i<MAX_BLOCKED_PM; i++) {
			if(GetPVarInt(BLOCKED_PMS[playerid][i], "AccountID") == GetPVarInt(user, "AccountID")) {
				BLOCKED_PMS[playerid][i] = -1;
				format(msg, sizeof(msg), "* %s is now unblocked!", GetPlayerNameEx(user, ENameType_AccountName));
				SendClientMessage(playerid, X11_WHITE, msg);
				return 1;
			}
		}
		new slot = getFreeBlockPMIndex(playerid);
		if(slot == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Your entire list is full, consider unblocking some users by using \"/block [playerid]\" once again.");
			sendBlockedPMUsers(playerid);
			return 1;
		}
		BLOCKED_PMS[playerid][slot] = user;
		format(msg, sizeof(msg), "* %s is now PM blocked so he / she won't be able to send you messages anymore.", GetPlayerNameEx(user, ENameType_AccountName));
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	} else {
		if(getBlockedAmount(playerid)) { //If there are users blocked.. then show this thing
			SendClientMessage(playerid, X11_WHITE, "Blocked Users: ");
			sendBlockedPMUsers(playerid);
		}
		SendClientMessage(playerid, X11_WHITE, "USAGE: /blockpm [playerid/name]");
	}
	return 1;
}
getBlockedAmount(playerid) {
	new j = 0;
	for(new i=0; i<MAX_BLOCKED_PM; i++) {
		if(GetPVarType(BLOCKED_PMS[playerid][i], "AccountID") != PLAYER_VARTYPE_NONE) {
			if(IsPlayerConnectEx(BLOCKED_PMS[playerid][i])) {
				j++;
			}
		}
	}
	return j;
}
senderIsPMBlocked(playerid, targetid) {
	for(new i=0; i<MAX_BLOCKED_PM; i++) {
		if(GetPVarInt(BLOCKED_PMS[targetid][i], "AccountID") == GetPVarInt(playerid, "AccountID")) {
			return 1;
		}
	}
	return 0;
}
getFreeBlockPMIndex(playerid) {
	for(new i=0; i<MAX_BLOCKED_PM; i++) {
		if(GetPVarType(BLOCKED_PMS[playerid][i], "AccountID") == PLAYER_VARTYPE_NONE) {
			if(!IsPlayerConnectEx(BLOCKED_PMS[playerid][i])) {
				return i;
			}
		}
	}
	return -1;
}
sendBlockedPMUsers(playerid) {
	new msg[128];
	SendClientMessage(playerid, X11_WHITE, "------------------------");
	for(new i=0; i<MAX_BLOCKED_PM; i++) {
		if(BLOCKED_PMS[playerid][i] != -1) {
			format(msg, sizeof(msg), "[%d]: %s", BLOCKED_PMS[playerid][i], GetPlayerNameEx(BLOCKED_PMS[playerid][i], ENameType_AccountName));
			SendClientMessage(playerid, X11_TOMATO_2, msg);
		}
	}
	SendClientMessage(playerid, X11_WHITE, "------------------------");
	return 1;
}
YCMD:pm(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a private message");
		return 1;
	}
	new msg[256],user;
	new string[256];
	if(!sscanf(params, "k<playerLookup_acc>s[256]", user, msg)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(nopms && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) {
			SendClientMessage(playerid, X11_TOMATO_2, "PMs are disabled by an admin");
			return 1;
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(user, "AccountFlags");
		if((aflags & EAccountFlags_BlockPMS && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_BasicAdmin) || senderIsPMBlocked(playerid, user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This player has their PM's blocked so you can't send him or her a PM.");
			return 1;
		}
		format(string,sizeof(string),"(( PM from %s [%d]: %s ))",GetPlayerNameEx(playerid, ENameType_CharName), playerid, msg);
		//SendClientMessage(user, X11_YELLOW, string);
		SendSplitClientMessage(user, X11_YELLOW, string, 0, 128);
		format(string, sizeof(string), "(( PM sent to %s [%d]: %s ))",GetPlayerNameEx(user, ENameType_CharName), user, msg);
		//SendClientMessage(playerid, X11_YELLOW, string);
		SendSplitClientMessage(playerid, X11_YELLOW, string, 0, 128);
		format(string, sizeof(string), "(( PM from %s [%d] to %s [%d]: %s ))",GetPlayerNameEx(playerid, ENameType_CharName), playerid, GetPlayerNameEx(user, ENameType_CharName), user, msg);
		SendBigEarsMessage(COLOR_YELLOW, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /pm [playerid/name] [message]");
	}
	return 1;
}
YCMD:requestchat(playerid, params[], help) {
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(help) {
		if(aflags & EAdminFlags_BasicAdmin) {
			SendClientMessage(playerid, X11_WHITE, "Invites a person to a conversation with you");
		} else {
			SendClientMessage(playerid, X11_WHITE, "Asks to speak with an admin");
		}
		return 1;
	}
	new user;
	new msg[128];
	if(GetPVarType(playerid, "RequestChat") != PLAYER_VARTYPE_NONE) {
		endRequestChat(playerid, RcExit_Exited);
		return 1;
	}
	if(~aflags & EAdminFlags_BasicAdmin) {
		new time = GetPVarInt(playerid, "RequestChatCooldown");
		new timenow = gettime();
		if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must wait before sending another request chat");
			return 1;
		}
		SetPVarInt(playerid, "RequestChatCooldown", gettime());
		if(!sscanf(params, "k<playerLookup_acc>", user)) {
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
			format(msg, sizeof(msg), "* %s[%d] has asked to speak with %s (( /requestchat ))", GetPlayerNameEx(playerid, ENameType_CharName),playerid,GetPlayerNameEx(user, ENameType_AccountName));
			if(EAdminFlags:GetPVarInt(user, "AdminFlags") & EAdminFlags_BasicAdmin && GetPVarInt(user, "AdminHidden") != 2) {
				ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
			}
		} else {
			format(msg, sizeof(msg), "* %s[%d] has asked to speak to an admin (( /requestchat ))", GetPlayerNameEx(playerid, ENameType_CharName),playerid);
			ABroadcast(X11_ORANGE, msg, EAdminFlags_All);		
		}
		SendClientMessage(playerid, X11_YELLOW, "* Your request has been sent to the admins.");
	} else {
		if(!sscanf(params, "k<playerLookup_acc>", user)) {
			if(user == playerid) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can't do that!");
				return 1;
			}
			sendRequestChatDialog(playerid, user);
		} else {
			SendClientMessage(playerid, X11_WHITE, "USAGE: /requestchat [playerid/name]");
		}
	}
	return 1;
}
sendRequestChatDialog(playerid, user) {
	new msg[128];
	SetPVarInt(user, "RequestChatSender", playerid);
	format(msg, sizeof(msg), "* Offered to have a conversation with %s", GetPlayerNameEx(user, ENameType_AccountName));
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	format(msg, sizeof(msg), "%s wants to have an admin conversation with you. Do you wish to accept?",GetPlayerNameEx(playerid, ENameType_AccountName));
	ShowPlayerDialog(user, EAdminDialog_RequestChat, DIALOG_STYLE_MSGBOX, "{00BFFF}Admin Chat Request", msg, "Accept", "Decline");
	return 1;
}
YCMD:massmoney(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives the entire server money");
		return 1;
	}
	new money,msg[128];
	if(!sscanf(params, "d", money)) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i)) {
				GiveMoneyEx(i, money);
			}
		}
	}
	format(msg, sizeof(msg), "An admin gave everyone: $%s",getNumberString(money));
	SendClientMessageToAll(COLOR_LIGHTBLUE, msg);
	return 1;
}
YCMD:nuke(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Nukes a player");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:px, Float:py, Float:pz;
		GetPlayerPos(playa, px, py, pz);
		CreateExplosion(px, py, pz, 7, 100.0);
	}
	return 1;
}
YCMD:fine(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Fines a player");
		return 1;
	}
	new reason[64],money,user;
	new string[128];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, money, reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(money < 1 || money > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount");
			return 1;
		}
		format(string, sizeof(string), "* You gave %s a fine costing $%s, Reason: %s", GetPlayerNameEx(user, ENameType_AccountName), getNumberString(money), reason);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, string);
		format(string, sizeof(string), "* Admin %s has given you a fine costing $%s, Reason: %s", GetPlayerNameEx(playerid, ENameType_AccountName), getNumberString(money), reason);
		SendClientMessage(user, X11_ORANGE, string);
		format(string, sizeof(string), "System: %s was fined $%s by %s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), getNumberString(money), GetPlayerNameEx(playerid, ENameType_AccountName),reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, string);
		GiveMoneyEx(user, -money);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fine [playerid/name] [price] [reason]");
	}
	return 1;
}
YCMD:sfine(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Fines a player");
		return 1;
	}
	new reason[64],money,user;
	new string[128];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, money, reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(money < 1 || money > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount");
			return 1;
		}
		format(string, sizeof(string), "* You gave %s a fine costing $%s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), getNumberString(money), reason);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, string);
		format(string, sizeof(string), "* An Admin has given you a fine costing $%s, Reason: %s", getNumberString(money), reason);
		SendClientMessage(user, X11_ORANGE, string);
		format(string, sizeof(string), "System: %s was sfined $%s by SYSTEM, Reason: %s", GetPlayerNameEx(user, ENameType_CharName),getNumberString(money),reason);
		ABroadcast(X11_TOMATO_2, string, EAdminFlags_All);
		GiveMoneyEx(user, -money);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fine [playerid/name] [price] [reason]");
	}
	return 1;
}
YCMD:fakefine(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Fines a player");
		return 1;
	}
	new reason[64],money,user;
	new string[128];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, money, reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		if(money < 1 || money > 9999999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount");
			return 1;
		}
		format(string, sizeof(string), "* You gave %s a fine costing $%s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), getNumberString(money), reason);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, string);
		format(string, sizeof(string), "* Admin %s has given you a fine costing $%s, Reason: %s", GetPlayerNameEx(playerid, ENameType_AccountName), getNumberString(money), reason);
		SendClientMessage(user, X11_ORANGE, string);
		format(string, sizeof(string), "System: %s was fined $%s by %s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), getNumberString(money), GetPlayerNameEx(playerid, ENameType_AccountName),reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, string);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /fine [playerid/name] [price] [reason]");
	}
	return 1;
}
YCMD:freeze(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid,X11_WHITE, "Freezes a player");
		return 1;
	}
	new user, msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "SYSTEM: %s has been frozen by %s",GetPlayerNameEx(user, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_CharName));
			ABroadcast(COLOR_LIGHTRED,msg,EAdminFlags_All);
			SendClientMessage(user, X11_ORANGE, "* An admin froze you");
		}
		TogglePlayerControllableEx(user, 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /freeze [playerid/name]");
	}
	return 1;
}
YCMD:unfreeze(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid,X11_WHITE, "Freezes a player");
		return 1;
	}
	new user, msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "SYSTEM: %s has been unfrozen by %s",GetPlayerNameEx(user, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_CharName));
			ABroadcast(COLOR_LIGHTRED,msg,EAdminFlags_All);
			SendClientMessage(user, X11_ORANGE, "* An admin unfroze you");
		}
		TogglePlayerControllableEx(user, 1);
		
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /unfreeze [playerid/name]");
	}
	return 1;
}
YCMD:setname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes someones name");
		return 1;
	}
	new name[MAX_PLAYER_NAME],user;
	if(!sscanf(params, "k<playerLookup_acc>s[" #MAX_PLAYER_NAME "]", user, name)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!NameIsRP(name) || strlen(name) > MAX_PLAYER_NAME) {
			SendClientMessage(playerid, X11_TOMATO_2, "This name is non-RP!");
			return 1;
		}
		SetPVarInt(user, "NameChanger", playerid);
		IsNameTaken(name, "OnAdminNameTakenCheck", user);	
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setname [playerid/name] [new name]");
	}
	return 1;
}
YCMD:tempname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Temporarily changes someones name");
		return 1;
	}
	new name[MAX_PLAYER_NAME],user;
	if(!sscanf(params, "k<playerLookup_acc>s[" #MAX_PLAYER_NAME "]", user, name)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPlayerName(user, name);
		SetPVarString(user, "CharUserName", name);
		SendClientMessage(playerid, X11_YELLOW_2, "* Name Changed");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setname [playerid/name] [new name]");
	}
	return 1;
}
YCMD:setaccountname(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes someones name");
		return 1;
	}
	new name[MAX_PLAYER_NAME],user;
	if(!sscanf(params, "k<playerLookup_acc>s[" #MAX_PLAYER_NAME "]", user, name)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPVarInt(user, "NameChanger", playerid);
		IsAccountTaken(name, "OnAdminAccountTakenCheck", user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setname [playerid/name] [new name]");
	}
	return 1;
}

forward OnAdminNameTakenCheck(playerid, name[]);
public OnAdminNameTakenCheck(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	new sender = GetPVarInt(playerid, "NameChanger");
	DeletePVar(playerid, "NameChanger");
	if(strlen(name) > MAX_PLAYER_NAME) {
		SendClientMessage(sender, X11_TOMATO_2 , "This name is too long");
		return 1;
	}
	if(rows > 0) {
		SendClientMessage(sender, X11_TOMATO_2, "This name is taken!");
	} else {
		if(GetPVarInt(sender, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s has changed %s's name to %s",GetPlayerNameEx(sender, ENameType_AccountName),GetPlayerNameEx(playerid, ENameType_RPName),name);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_SetName);
		}
		changeName(playerid, name);
	}
	return 1;	
}
forward OnAdminAccountTakenCheck(playerid, name[]);
public OnAdminAccountTakenCheck(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	new sender = GetPVarInt(playerid, "NameChanger");
	DeletePVar(playerid, "NameChanger");
	if(strlen(name) > MAX_PLAYER_NAME) {
		SendClientMessage(sender, X11_TOMATO_2 , "This name is too long");
		return 1;
	}
	if(rows > 0) {
		SendClientMessage(sender, X11_TOMATO_2, "This name is taken!");
	} else {
		if(GetPVarInt(sender, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s has changed %s's account name to %s",GetPlayerNameEx(sender, ENameType_AccountName),GetPlayerNameEx(playerid, ENameType_RPName),name);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_SetName);
		}
		changeAccountName(playerid, name);
	}
	return 1;	
}
YCMD:thisissparta(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Spartas a player");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new Float:slx, Float:sly, Float:slz;
		GetPlayerPos(playa, slx, sly, slz);
		Broadcast(COLOR_BRIGHTRED, "THIS!!!");
		Broadcast(COLOR_BRIGHTRED, "IS!!!!");
		Broadcast(COLOR_BRIGHTRED, "SPARTAAAA!!!");
		SetPlayerPos(playa, slx, sly, slz+20);
		SetPlayerVelocity(playa, 100.0, 100.0, 125.0);
		PlayerPlaySound(playa, 1130, slx+25, sly+25, slz+25);
		new string[80];
		format(string, sizeof(string), "System: %s was sparta'd by %s.", GetPlayerNameEx(playa, ENameType_CharName), GetPlayerNameEx(playerid, ENameType_AccountName));
		ABroadcast(COLOR_LIGHTRED,string,EAdminFlags_Nuke);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /thisissparta [playerid]");
	}
	return 1;
}
//Event cmd
YCMD:skydive(playerid, params[], help)
{
	new playa;
	if(sscanf(params, "k<playerLookup_acc>", playa)) {
		playa = playerid;
	}
    new Float:rx, Float:ry, Float:rz;
	GetPlayerPos(playa, rx, ry, rz);
	GivePlayerWeaponEx(playa, 46, 1);
	SetPlayerPos(playa,rx, ry, rz+1500);
	SendClientMessage(playa, X11_WHITE, "GO!! GO!! GO!!");
	return 1;
}
YCMD:smallnuke(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Nukes a player");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new Float:px, Float:py, Float:pz;
		GetPlayerPos(playa, px, py, pz);
		CreateExplosion(px, py, pz, 7, 50.0);
	}
	return 1;
}
YCMD:mark(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "marks a spot on your teleporter");
		return 1;
	}
	new Float:X,Float:Y,Float:Z,Float:A;
	new interior,vw;
	GetPlayerPos(playerid,X, Y, Z);
	GetPlayerFacingAngle(playerid, A);
	interior = GetPlayerInterior(playerid);
	vw = GetPlayerVirtualWorld(playerid);
	SetPVarFloat(playerid, "TeleporterX", X);
	SetPVarFloat(playerid, "TeleporterY", Y);
	SetPVarFloat(playerid, "TeleporterZ", Z);
	SetPVarFloat(playerid, "TeleporterAngle", A);
	SetPVarInt(playerid, "TeleporterInt", interior);
	SetPVarInt(playerid, "TeleporterVW", vw);
	SendClientMessage(playerid, X11_GREEN, "Point marked on your teleporter");
	return 1;
}
YCMD:gotomark(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "teleports to your marked spot.");
		return 1;
	}
	if(GetPVarType(playerid, "TeleporterX") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_RED2, "Mark the spot with /mark first.");
		return 1;
	}
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		TPEntireCar(carid, GetPVarInt(playerid, "TeleporterInt"), GetPVarInt(playerid, "TeleporterVW"));
		LinkVehicleToInterior(carid, GetPVarInt(playerid, "TeleporterInt"));
		SetVehicleVirtualWorld(carid, GetPVarInt(playerid, "TeleporterVW"));
		SetVehiclePos(carid, GetPVarFloat(playerid, "TeleporterX"), GetPVarFloat(playerid, "TeleporterY"), GetPVarFloat(playerid, "TeleporterZ"));
		
	} else {
		SetPlayerPos(playerid, GetPVarFloat(playerid, "TeleporterX"), GetPVarFloat(playerid, "TeleporterY"), GetPVarFloat(playerid, "TeleporterZ"));
		SetPlayerFacingAngle(playerid, GetPVarFloat(playerid, "TeleporterAngle"));
		SetPlayerInterior(playerid, GetPVarInt(playerid, "TeleporterInt"));
		SetPlayerVirtualWorld(playerid, GetPVarInt(playerid, "TeleporterVW"));
	}
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	return 1;
}
YCMD:admins(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists in game admins");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	new EAdminFlags:userflags;
	new msg[128];
	SendClientMessage(playerid, COLOR_YELLOW, "*** Admins Online: ***");
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			userflags = EAdminFlags:GetPVarInt(i, "AdminFlags");
			if((~userflags & EAdminFlags_Invisible && (GetPVarInt(i, "AdminHidden") == 0 || aflags & EAdminFlags_All)) || aflags & EAdminFlags_ServerManager) {
				if(userflags != EAdminFlags_None) {
					if(GetPVarInt(i, "AdminHidden") != 2) {
						format(msg,sizeof(msg), "<%s>: [%s] %s",getAdminName(i), GetPlayerNameEx(i, ENameType_AccountName),GetPlayerNameEx(i, ENameType_CharName));
						SendClientMessage(playerid, GetPVarInt(i,"AdminHidden") == 1 ? X11_GREY : (GetPVarInt(i, "AdminDuty") == 0 ?COLOR_YELLOW2:getNameTagColour(i)), msg);
					}
				}
			}
		}
	}
	return 1;
}
YCMD:setadmintitle(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets an admins Job Title");
		return 1;
	}
	new playa, title[(32*2)+1];
	if(!sscanf(params, "k<playerLookup_acc>s[32]", playa, title)) {
		if(!IsPlayerConnectEx(playa) || EAdminFlags:GetPVarInt(playerid, "AdminFlags") == EAdminFlags_None) {
			SendClientMessage(playerid, X11_TOMATO_2 , "User not found");
			return 1;
		}
		SetPVarString(playa, "AdminTitle", title);
		query[0] = 0;//[128];
		mysql_real_escape_string(title, title);
		format(query,sizeof(query),"UPDATE `accounts` SET `admintitle` = \"%s\" WHERE `id` = %d",title, GetPVarInt(playa, "AccountID"));
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setadmintitle [playerid/name] [title]");
	}
	return 1;
}
YCMD:admin(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends an admin message");
		return 1;
	}
	if(isnull(params)) {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /admin [message]");
		return 1;
	}
	new msg[128];
	format(msg,sizeof(msg),"*%s %s: %s",getAdminName(playerid), GetPlayerNameEx(playerid,ENameType_AccountName),params);
	ABroadcast(X11_YELLOW,msg,EAdminFlags_All);
	return 1;
}
YCMD:newbie(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a newbie message");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "[INFO]: /newbie has been changed to /helpme");
	return 1;
}
YCMD:helpme(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a question to the helpers.");
		return 1;
	}
	if(isNewbMuted(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to use this because you're muted from it!");
		return 1;
	}
	new timenow = gettime();
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned!");
		return 1;
	}
	if(EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoNewb) {
		SendClientMessage(playerid, X11_TOMATO_2, "Re-enable your newbie chat in /settings!");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") == 0) {
		new time = GetPVarInt(playerid, "NewbieCooldown");
		if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must wait before using this again!");
			return 1;
		}
		SetPVarInt(playerid, "NewbieCooldown", gettime());
	}
	new smsg[128];
	if(!sscanf(params, "s[128]", smsg)) {
		new msg[128];
		format(msg,sizeof(msg),"%s [%d]%s asked: %s",GetNewbieName(playerid),playerid, GetPlayerNameEx(playerid,ENameType_CharName),smsg);
		if(nonewb && ~EAdminFlags:GetPVarInt(playerid, "AdminLevel") & EAdminFlags_HelperManage) {
			SendClientMessage(playerid, X11_RED3 ,"The helper chat is disabled!");
			return 1;
		}
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") != 0) {
				if(EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoHelperChat) {
					continue;
				}
				SendClientMessage(i, X11_YELLOW, msg);
			}
		}
		SendClientMessage(playerid, X11_YELLOW, "[INFO]: Your question has been sent, please wait until a helper or teacher answers you.");
		SetPVarInt(playerid, "SentHelpReport", 1);
		SetPVarString(playerid, "ReportHelpText", smsg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /helpme [message]");
	}
	return 1;
}
YCMD:askirc(playerid, params[], help) {
    if (help) {
        SendClientMessage(playerid, X11_WHITE, "Sends a message to the admins on the IRC server.");
    } else {
        new str[256];
        if (isnull(params)) {
            format(str, sizeof (str), "Usage: \"/%s [message]\"", "askirc");
            SendClientMessage(playerid, X11_WHITE, str);
        } else {
            format(str, sizeof(str), "02 %s[%d]: %s ", GetPlayerNameEx(playerid, ENameType_RPName),playerid, params);
			SendClientMessage(playerid, X11_YELLOW, "[INFO]: Your question has been sent to the IRC channel, please wait until a helper or teacher answers you.");
			aIRCPrintHelp(str);
        }
    }
    return 1;
}
YCMD:accepthelpreport(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Accepts a report");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a helper/teacher");
		return 1;
	}
	new user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarType(user, "SentHelpReport") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user has not sent a report!");
			return 1;
		}
		format(msg, sizeof(msg), "%s has accepted %s's help report", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName));
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") != 0) {
				if(EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoHelperChat) {
					continue;
				}
				SendClientMessage(i, COLOR_HELPERCHAT, msg);
			}
		}
		format(msg, sizeof(msg), "%s has accepted your help report", GetPlayerNameEx(playerid, ENameType_AccountName));
		SendClientMessage(user, X11_ORANGE, msg);
		DeletePVar(user, "SentHelpReport");
		DeletePVar(user, "ReportHelpText");
	}
	return 1;
}
YCMD:helpreports(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists unanswered help reports");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a helper/teacher");
		return 1;
	}
	new text[128];
	new report[128];
	SendClientMessage(playerid, X11_YELLOW, "Unanswered Help Reports: ");
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "SentHelpReport") == 1) {
				GetPVarString(i, "ReportHelpText", report, sizeof(report));
				format(text, sizeof(text), "Help Report: %s(%d): {EE0000}%s",GetPlayerNameEx(i, ENameType_CharName), i, report);
				SendClientMessage(playerid, X11_ORANGE, text);
			}
		}
	}
	return 1;
}
YCMD:helperchat(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Send a message to helpers");
		return 1;
	}
	new smsg[128];
	if(GetPVarInt(playerid, "NewbieRank") == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a helper/teacher");
		return 1;
	}
	if(nohelperchat) {
		SendClientMessage(playerid, X11_TOMATO_2, "Helper Chat is disabled!");
		return 1;
	}
	if(!sscanf(params, "s[128]",smsg)) {
		new msg[128];
		format(msg, sizeof(msg), "(( %s %s: %s ))",GetNewbieName(playerid), GetPlayerNameEx(playerid,ENameType_AccountName),smsg);
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") != 0) {
				if(EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_NoHelperChat) {
					continue;
				}
				SendClientMessage(i, COLOR_HELPERCHAT, msg);
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /helperchat [message]");
	}
	return 1;
}
//Teleport cmd //teleport admin
YCMD:dn(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points backwardss in Z axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx, sly, slz-3);
	return 1;
}
YCMD:up(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points forwards in Z axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx, sly, slz+3);
	return 1;
}
YCMD:lt(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points forwards in Y axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx, sly+3, slz);
	return 1;
}
YCMD:rt(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points backwards in Y axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx, sly-3, slz);
	return 1;
}

YCMD:fr(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points backwards in X axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx-3, sly, slz);
	return 1;
}

YCMD:ba(playerid,params[],help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Moves a player 3 points forward in X axis");
		return 1;
	}
	new Float:slx, Float:sly, Float:slz;
	GetPlayerPos(playerid, slx, sly, slz);
	SetPlayerPos(playerid, slx+3, sly, slz);
	return 1;
}
YCMD:jetpack(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player a jetpack");
		return 1;
	}
	SetPlayerSpecialAction(playerid, SPECIAL_ACTION_USEJETPACK);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	return 1;
}
YCMD:checkanim(playerid,params[], help)
{
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new animlib[32];
		new animname[32];
		new msg[128];
		GetAnimationName(GetPlayerAnimationIndex(target),animlib,32,animname,32);
		format(msg, sizeof(msg), "Current anim: (%d)%s %s", GetPlayerAnimationIndex(target), animlib, animname);
		SendClientMessage(playerid, COLOR_LIGHTBLUE,  msg);
    }
	return 1;
}

YCMD:givecoke(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player coke");
		return 1;
	}
	new num,curcoke,playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, num)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found");
			return 1;
		}
		if(num < -99999999 || num > 99999999)
		{
			SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid amount!");
			return 1;
		}
		curcoke = GetPVarInt(playa, "Coke");
		curcoke += num;
		SetPVarInt(playa, "Coke", curcoke);
		new string[60];
		format(string, sizeof(string),"* %sg Coke sent!", getNumberString(num));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD,string);
		format(string, sizeof(string),"* An admin sent you %sg Coke!", getNumberString(num));
		SendClientMessage(playa, COLOR_LIGHTBLUE,string);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string),"* %s has given %s %s Coke!",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName), getNumberString(num));
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE /givecoke [playerid/name] [amount]");
	}
	return 1;
}
YCMD:givepot(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player pot");
		return 1;
	}
	new num,curcoke,playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, num)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found");
			return 1;
		}
		if(num < -99999999 || num > 99999999)
		{
			SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid amount!");
			return 1;
		}
		curcoke = GetPVarInt(playa, "Pot");
		curcoke += num;
		SetPVarInt(playa, "Pot", curcoke);
		new string[60];
		format(string, sizeof(string),"* %sg Pot sent!", getNumberString(num));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD,string);
		format(string, sizeof(string),"* An admin sent you %sg Pot!", getNumberString(num));
		SendClientMessage(playa, COLOR_LIGHTBLUE,string);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string),"* %s has given %s %s Pot!",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName), getNumberString(num));
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE /givepot [playerid/name] [amount]");
	}
	return 1;
}
YCMD:givemeth(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player meth");
		return 1;
	}
	new num,curcoke,playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, num)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found");
			return 1;
		}
		if(num < -99999999 || num > 99999999)
		{
			SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid amount!");
			return 1;
		}
		curcoke = GetPVarInt(playa, "Meth");
		curcoke += num;
		SetPVarInt(playa, "Meth", curcoke);
		new string[60];
		format(string, sizeof(string),"* %sg Meth sent!", getNumberString(num));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD,string);
		format(string, sizeof(string),"* An admin sent you %sg Meth!", getNumberString(num));
		SendClientMessage(playa, COLOR_LIGHTBLUE,string);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string),"* %s has given %s %s Meth!",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName), getNumberString(num));
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE /givemeth [playerid/name] [amount]");
	}
	return 1;
}
YCMD:givemats(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player meth");
		return 1;
	}
	new num,type,curcoke,playa;
	if(!sscanf(params, "k<playerLookup_acc>cd", playa, type, num)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found");
			return 1;
		}
		if(num < -99999999 || num > 99999999)
		{
			SendClientMessage(playerid, COLOR_LIGHTRED, "   Invalid amount!");
			return 1;
		}
		type = toupper(type);
		if(type < 'A' || type > 'C') {
			SendClientMessage(playerid, X11_WHITE, "Invalid Mats Type!");
			return 1;
		}
		new string[60];
		format(string, sizeof(string), "Mats%c",type);
		curcoke = GetPVarInt(playa, string);
		curcoke += num;
		SetPVarInt(playa, string, curcoke);
		
		format(string, sizeof(string),"* %s Mats%c sent!", getNumberString(num),type);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD,string);
		format(string, sizeof(string),"* An admin sent you %s Mats%c!", getNumberString(num), type);
		SendClientMessage(playa, COLOR_LIGHTBLUE,string);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(string, sizeof(string),"* %s has given %s %s Mats%c!",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(playa, ENameType_CharName), getNumberString(num), type);
			ABroadcast(X11_YELLOW, string, EAdminFlags_AdminManage);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE /givemats [playerid/name] [type] [amount]");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "Valid Types: A, B, C");
	}
	return 1;
}
YCMD:setskin(playerid, params[], help) {
	new skinid, playa;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players skin");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>d", playa,skinid))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		if(skinid < 0 || skinid >= 300)
		{
		    SendClientMessage(playa, COLOR_LIGHTRED, "	Invalid skin! (0-299)");
		    return 1;
		}
		setCharacterSkin(playa, skinid);
		format(msg,sizeof(msg),"An admin set your skin to: %d",skinid);
		SendClientMessage(playa, X11_ORANGE, msg);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setskin [playerid/name] [skinid]");
	}
	return 1;
}
YCMD:check(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a players stats");
		return 1;
	}
	new playa;
	if(!sscanf(params,"k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found.");
			return 1;
		}
		SendStats(playerid, playa);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /check [playerid/name]");
	}
	return 1;
}
SendGlobalAdminMessage(color, msg[]) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(~EAccountFlags:GetPVarInt(i, "AccountFlags") & EAccountFlags_HideAdminMessages) {
				SendClientMessage(i, color, msg);
			}
		}
	}
}
YCMD:adminduty(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles admin duty");
		return 1;
	}
	//new msg[128];
	new aduty = GetPVarInt(playerid, "AdminDuty");
	if(aduty == 0) {
		/*
		format(msg,sizeof(msg),"%s %s is now on duty",getAdminName(playerid),GetPlayerNameEx(playerid, ENameType_AccountName));
		SendGlobalAdminMessage(X11_YELLOW, msg);
		*/
		if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") == EAdminFlags_BasicAdmin) {
			aduty = 1;
		} else {
			aduty = 2;
		}
		if(GetPVarInt(playerid, "Sex") == 0) {
			SetPlayerSkin(playerid, 217);
		} else {
			SetPlayerSkin(playerid, 211);
		}
		SetPVarInt(playerid, "AdminDuty", aduty);
		ShowAllNameTagsForPlayer(playerid, 1);
	} else {
		/*
		format(msg,sizeof(msg),"%s %s is now off duty",getAdminName(playerid),GetPlayerNameEx(playerid, ENameType_AccountName));
		SendGlobalAdminMessage(X11_YELLOW,msg);
		*/
		aduty = 0;
		SetPlayerSkin(playerid, GetPVarInt(playerid, "SkinID"));
		ShowAllNameTagsForPlayer(playerid, 0);
		DeletePVar(playerid, "AdminDuty");
	}
	
	SetPlayerColor(playerid, getNameTagColour(playerid));
	return 1;
}
YCMD:setgunskill(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a gun skill on a player");
		return 1;
	}
	new playa, skill, level;
	if(!sscanf(params, "k<playerLookup_acc>dd", playa, skill, level)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPlayerSkillLevel(playa, skill, level);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Skill Level changed");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setgunskill [playerid] [skill] [level]");
		SendClientMessage(playerid, X11_WHITE, "Skills: 9mm = 0, silenced 9mm = 1, deagle = 2, shotgun = 3, sawnoff = 4, spas12 = 5, uzi = 6, mpt = 7, ak47 = 8, m4 = 9, sniper = 10");
	}
	return 1;
}
YCMD:givegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player a gun");
		return 1;
	}
	new playa, gunid, ammo;
	new ignoreslot;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>dI(-1)I(0)", playa, gunid, ammo,ignoreslot)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new slot = GetWeaponSlot(gunid);
		if(slot == -1) {
			SendClientMessage(playerid, X11_RED3, "Invalid Weapon ID");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if((isRestrictedGun(gunid) && (~aflags & EAdminFlags_GiveRestrictedGun))) {
			SendClientMessage(playerid, X11_TOMATO_2, "This weapon is restricted.");
			return 1;
		}
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		new curgun,curammo;
		GetPlayerWeaponDataEx(playa, slot, curgun, curammo);
		if(curgun != 0 && ignoreslot != 1) {
			SendClientMessage(playerid, X11_WHITE, "This player is holding a weapon in this slot.");
			format(msg, sizeof(msg), "To ignore this warning, do /givegun %d %d %d 1",playa,gunid,ammo);
			SendClientMessage(playerid, X11_WHITE, msg);
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			new weapon[32];
			GetWeaponNameEx(gunid, weapon, sizeof(weapon));
			format(msg, sizeof(msg), "AdmWarn: %s gave %s a %s with %d bullets", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName),weapon, ammo);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		GivePlayerWeaponEx(playa, gunid, ammo);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givegun [playerid/name] [gunid] [ammo]");
	}
	return 1;
}
YCMD:takegun(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player a gun");
		return 1;
	}
	new msg[128];
	new playa, gunid;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, gunid)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new slot = GetWeaponSlot(gunid);
		if(slot == -1) {
			SendClientMessage(playerid, X11_RED3, "Invalid Weapon");
			return 1;
		}
		new curgun,curammo;
		new weapon[32];
		GetWeaponNameEx(gunid, weapon, sizeof(weapon));
		GetPlayerWeaponDataEx(playa, slot, curgun, curammo);
		if(curgun == 0) {
			SendClientMessage(playerid, X11_WHITE, "Error: Player doesn't have this gun");
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s took %s's %s away", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName),weapon);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		RemovePlayerWeapon(playa, gunid);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /takegun [playerid/name] [weaponid]");
	}
	return 1;
}
YCMD:takeguns(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player a gun");
		return 1;
	}
	new msg[128];
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s took %s's guns away", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName));
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		ResetPlayerWeaponsEx(playa);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /takeguns [playerid/name]");
	}
	return 1;
}
YCMD:checkguns(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Displays the weapons a person has");
		return 1;
	}
	new gun,ammo, user;
	new msg[128],gunname[32];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		format(msg, sizeof(msg), "******** %s's Weapons ********", GetPlayerNameEx(user, ENameType_CharName));
		SendClientMessage(playerid, X11_WHITE, msg);
		for(new i=0;i<12;i++) {
			GetPlayerWeaponData(user, i, gun, ammo);
			if(gun != 0) {
				new sgun, sammo;
				//check if the gun is hacked, show in red if it is
				GetPlayerWeaponDataEx(user, i, sgun, sammo);
				GetWeaponNameEx(gun, gunname, sizeof(gunname));
				format(msg, sizeof(msg), "%d. %s(%d) Ammo: %s", i, gunname, gun, getNumberString(ammo));
				SendClientMessage(playerid, sgun!=gun?COLOR_LIGHTRED:COLOR_LIGHTBLUE, msg);
			}
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /checkguns [playerid/name]");
	}
	return 1;
}
//PVAR MANAGEMENT
YCMD:getplayervartype(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Returns a PVar Type");
		return 1;
	}
	new type[16];
	new playa,name[64];
	if(!sscanf(params, "k<playerLookup_acc>s[64]",playa, name)) {
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found.");
			return 1;
		}
		new typeid = GetPVarType(playa, name);
		GetPVarTypeName(typeid, type, sizeof(type));
		format(name,sizeof(name),"Type: %s",type);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, name);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /GetPlayerVarType [playerid/name] [name]");
	}
	return 1;
}
YCMD:getplayervar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Returns the value of a PVar");
		return 1;
	}
	new playa,name[64];
	if(!sscanf(params, "k<playerLookup_acc>s[64]", playa, name)) {	
		new msg[128];
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found.");
			return 1;
		}
		new type = GetPVarType(playa, name);
		switch(type) {
			case PLAYER_VARTYPE_NONE: {
				format(msg,sizeof(msg),"PVar not found.");
			}
			case PLAYER_VARTYPE_INT: {
				new value = GetPVarInt(playa, name);
				format(msg,sizeof(msg),"PVar: %s : {FF0000}%d",name,value);
			}
			case PLAYER_VARTYPE_STRING: {
				new value[128];
				GetPVarString(playa, name, value, sizeof(value));
				format(msg,sizeof(msg),"PVar: %s : {FF0000}%s",name,value);
			}
			case PLAYER_VARTYPE_FLOAT: {
				new Float:value = GetPVarFloat(playa, name);
				format(msg,sizeof(msg),"PVar: %s : {FF0000}%f",name,value);
			}
		}
		SendClientMessage(playerid, COLOR_GREY, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /GetPlayerVar [playerid/name] [name]");
	}
	return 1;
}
YCMD:gotopoint(playerid, params[], help)
{
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleports to coordinates");
		return 1;
	}
    new Float:X, Float:Y, Float:Z;
    if(!sscanf(params, "fff", X, Y, Z))
    {
		if(!IsPlayerInAnyVehicle(playerid)) {
			SetPlayerPos(playerid, X, Y, Z);
			SetPlayerVirtualWorld(playerid, 0);
			SetPlayerInterior(playerid, 0);
		} else {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, 0, 0);
			LinkVehicleToInterior(carid, 0);
			SetVehicleVirtualWorld(carid, 0);
			SetVehiclePos(carid, X, Y, Z);
		}
        SetCameraBehindPlayer(playerid);
		
        SendClientMessage(playerid, X11_ORANGE, "You have been teleported!");
        return 1;
    }
    else
    {
        SendClientMessage(playerid, X11_WHITE, "USAGE: /Gotopoint [X] [Y] [Z]");
        SendClientMessage(playerid, X11_LIGHTBLUE, "Teleports you to a point in world space.");
	}
	return 1;
}
YCMD:deleteplayervar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes a players PVar");
		return 1;
	}
	new playa, name[64];
	if(!sscanf(params, "k<playerLookup_acc>s[64]", playa, name)) {
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found.");
			return 1;
		}
		new type = GetPVarType(playa, name);
		if(type == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_RED3, "Failed: PVar not found");
			return 1;
		}
		DeletePVar(playa, name);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /DeletePlayerVar [playerid/name] [name]");
	}
	return 1;
}
YCMD:setplayervar(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes a players PVar");
		return 1;
	}
	new playa, name[64], value[64],type;
	if(!sscanf(params, "k<playerLookup_acc>ds[64]s[64]", playa, type, name, value)) {
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found.");
			return 1;
		}
		switch(type) {
			case PLAYER_VARTYPE_INT: {
				SetPVarInt(playa, name, strval(value));
			}
			case PLAYER_VARTYPE_STRING: {
				SetPVarString(playa, name, value);
			}
			case PLAYER_VARTYPE_FLOAT: {
				SetPVarFloat(playa, name, floatstr(value));
			}
			default: {
				SendClientMessage(playerid, X11_RED3, "Invalid Var Type! The valid types are: 1(int), 2(string), 3(float)");
				return 1;
			}
		}
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[Notice]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /SetPlayerVar [playerid/name] [type] [name] [value]");
	}
	return 1;
}
YCMD:setdrunk(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players drunk level");
		return 1;
	}
	new level, playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa,level)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		SetPlayerDrunkLevel (playa, level);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setdrunk [playerid/name] [level]");
	}
	return 1;
}
YCMD:setteam(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players team");
		return 1;
	}
	new level, playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa,level)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		SetPlayerTeam(playa, level);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setteam [playerid/name] [team]");
	}
	return 1;
}
YCMD:getplayervars(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all PVars on a player");
		return 1;
	}
	new playa;
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {		
		new msg[128];
		if(!IsPlayerConnected(playa)) {
			SendClientMessage(playerid, X11_RED3, "User not found.");
			return 1;
		}
		new uindex = GetPVarsUpperIndex(playa);
		new name[64],type[32];
		new num;
		for(new i=0;i<uindex;i++) {
			GetPVarNameAtIndex(playa, i, name, sizeof(name));
			if(strlen(name) > 0) {
				GetPVarTypeName(GetPVarType(playa, name), type, sizeof(type));
				format(msg,sizeof(msg),"PVar: {FF0000}%s{AFAFAF}(%d) Type: {FF0000}%s",name,i,type);
				SendClientMessage(playerid, COLOR_GREY, msg);
				num++;
			}
		}
		format(msg,sizeof(msg),"Done. Total PVars: {FF0000}%d{FFFFFF} Upper Index: {FF0000}%d",num,uindex);
		SendClientMessage(playerid, X11_WHITE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /GetPVars [playerid/name]");
	}
	return 1;
}
//END PVAR MANAGEMENT
YCMD:money(playerid, params[], help) {
	new money, playa;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players money");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>d", playa, money))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		SetMoneyEx(playa, money);
		format(msg,sizeof(msg),"An admin set your money to: $%s",getNumberString(money));
		SendClientMessage(playa, X11_ORANGE, msg);
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s set %s's money to $%s dollars", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName), getNumberString(money));
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
			format(msg, sizeof(msg), "Money set to: %d",money);
			AddAccountNote(playa, playerid, msg);
		}
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /money [playerid/name] [money]");
	}
	return 1;
}
YCMD:givemoney(playerid, params[], help) {
	new money, playa;
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives a player money");
		return 1;
	}
	if (!sscanf(params, "k<playerLookup_acc>d", playa, money))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			format(msg, sizeof(msg), "AdmWarn: %s gave %s $%s dollars", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName), getNumberString(money));
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
			format(msg, sizeof(msg), "Money given: %d",money);
			AddAccountNote(playa, playerid, msg);
		}
		format(msg,sizeof(msg),"An admin gave you $%s",getNumberString(money));
		SendClientMessage(playa, X11_ORANGE, msg);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
		GiveMoneyEx(playa, money);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givemoney [playerid/name] [money]");
	}
	return 1;
}
YCMD:setarmour(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players armour level");
		return 1;
	}
	new Float:armour,playa;
	if (!sscanf(params, "k<playerLookup_acc>f", playa, armour))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			new msg[128];
			format(msg, sizeof(msg), "AdmWarn: %s set %s's armour to: %.02f", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName), armour);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		SetPlayerArmourEx(playa, armour);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setarmour [playerid/name] [value]");
	}
	return 1;
}
YCMD:sethp(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players health level");
		return 1;
	}
	new Float:armour,playa;
	if (!sscanf(params, "k<playerLookup_acc>f", playa, armour))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		if(playerid == playa) {
			if(~aflags & EAdminFlags_CanRefundSelf) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot refund yourself!");
				return 1;
			}
		}
		if(GetPVarInt(playerid, "AdminHidden") != 2) {
			new msg[128];
			format(msg, sizeof(msg), "AdmWarn: %s set %s's health to: %.02f", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playa, ENameType_CharName), armour);
			ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		}
		SetPlayerHealthEx(playa, armour);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sethp [playerid/name] [value]");
	}
	return 1;
}
YCMD:setworld(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a players virtual world");
		return 1;
	}
	new playa, world;
	if (!sscanf(params, "k<playerLookup_acc>d", playa, world))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		SetPlayerVirtualWorld(playa, world);
		SendClientMessage(playerid ,X11_ORANGE, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setworld [playerid/name] [world]");
	}
	return 1;
}
YCMD:setint(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Changes a players interior");
		return 1;
	}
	new playa, interior;
	if (!sscanf(params, "k<playerLookup_acc>d", playa, interior))
    {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		SetPlayerInterior(playa, interior);
		SendClientMessage(playerid ,X11_ORANGE, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setworld [playerid/name] [interior]");
	}
	return 1;
}
checkBans(playerid) {
	query[0] = 0;//[512];
	new GPCI[(32*2)+1],ip[16]; //new GPCI[(32*2)+1]
	gpci(playerid, GPCI, sizeof(GPCI));
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	new username[(MAX_PLAYER_NAME*2)+1];
	//GetPlayerNameEx isn't used so it can safely check before AccountName pvar is set
	GetPlayerName(playerid, username, sizeof(username));
	mysql_real_escape_string(username,username);
	mysql_real_escape_string(GPCI,GPCI);
	new query2[512];
	/*format(query, sizeof(query), "SELECT `reason`,`ac2`.`username`,`bantime`,`bans`.`id`,`bans`.`expiretime` FROM "
	"`bans` LEFT JOIN `accounts` AS `ac1` ON `bans`.`accountid` = `ac1`.`id` LEFT JOIN `accounts` AS `ac2` ON `bans`.`bannerid` = `ac2`.`id` "
	"LEFT JOIN `characters` AS `chars` ON `bans`.`accountid` = `chars`.`accountid` "
	"WHERE (`expiretime` = TIMESTAMP(0) OR `expiretime` > CURRENT_TIMESTAMP()) ");*/
	strcat(query2,"SELECT `reason`,`ac2`.`username`,`bantime`,`bans`.`id`,`bans`.`expiretime` FROM ",sizeof(query2));
	strcat(query2,"`bans` LEFT JOIN `accounts` AS `ac1` ON `bans`.`accountid` = `ac1`.`id` LEFT JOIN `accounts` AS `ac2` ON `bans`.`bannerid` = `ac2`.`id` ",sizeof(query2));
	strcat(query2,"LEFT JOIN `characters` AS `chars` ON `bans`.`accountid` = `chars`.`accountid` ",sizeof(query2));
	strcat(query2,"WHERE (`expiretime` = TIMESTAMP(0) OR `expiretime` > CURRENT_TIMESTAMP())",sizeof(query2));
	format(query,sizeof(query),	"%s AND (`ip` = INET_ATON(\"%s\") OR `GPCI` = \"%s\" OR `ac1`.`username` = \"%s\" OR `chars`.`username` = \"%s\")",query2,ip,GPCI,username,username);
	mysql_function_query(g_mysql_handle, query, true, "OnBanRetrieve", "d", playerid);
	
	query2[0] = 0;
	strcat(query2,"SELECT `r`.`id`,`reason`,`ac`.`username`,`seton`,INET_NTOA(`host`),INET_NTOA(`mask`) ",sizeof(query2));
	strcat(query2,"FROM `rangebans` AS `r` ",sizeof(query2));
	strcat(query2,"LEFT JOIN `accounts` AS `ac` ON `ac`.`id` = `setby`", sizeof(query2));
	format(query,sizeof(query),"%s WHERE (`host`&`mask`)=(INET_ATON(\"%s\")&`mask`)",query2,ip);
	mysql_function_query(g_mysql_handle, query, true, "OnRangeBanRetrieve", "d", playerid);
}
forward OnRangeBanRetrieve(playerid);
public OnRangeBanRetrieve(playerid) {
	new reason[128],banner[MAX_PLAYER_NAME],timestr[64],host[17],mask[17],id_str[12];
	new rows, fields;
	cache_get_data(rows, fields);
	if(rows > 0) {
		cache_get_row(0,0,id_str);
		cache_get_row(0,1,reason);
		cache_get_row(0,2, banner);
		cache_get_row(0,3, timestr);
		cache_get_row(0,4, host);
		cache_get_row(0,5, mask);
		if(strlen(banner) < 1 || !strcmp(banner,"NULL")) {
			strmid(banner, "SYSTEM", 0, 6, sizeof(banner));
		}
		SetPVarInt(playerid, "RangeBanID", strval(id_str));
		SetPVarString(playerid, "RangeBanReason", reason);
		SetPVarString(playerid, "RangeBanBanner", banner);
		SetPVarString(playerid, "RangeBanHost", host);
		SetPVarString(playerid, "RangeBanMask", mask);
	}
	return 1;
}
public OnBanRetrieve(playerid) {
	new reason[128],banner[MAX_PLAYER_NAME],timestr[64],expiretime[64];
	new id_str[128],id;
	new rows,fields;
	cache_get_data(rows,fields);
	//Record the ban, and ban the account if they attempt to log in to it, in OnPlayerLogin
	if(rows > 0) {
		cache_get_row(0,0,reason);
		cache_get_row(0,1,banner);
		cache_get_row(0,2, timestr);
		cache_get_row(0,3, id_str);
		cache_get_row(0,4, expiretime);
		id = strval(id_str);
		if(strlen(banner) < 1 || !strcmp(banner,"NULL")) {
			strmid(banner, "SYSTEM", 0, 6, sizeof(banner));
		}
		SetPVarString(playerid, "BanReason", reason);
		SetPVarString(playerid, "BanTime", timestr);
		SetPVarString(playerid, "Banner", banner);
		SetPVarString(playerid, "BanExpire", expiretime);
		SetPVarInt(playerid, "BanID", id);
		
	}
	return 1;
}
public ABroadcast(color, const msg[], EAdminFlags:level) {
	foreach(Player, i) {
		if(EAdminFlags:GetPVarInt(i, "AdminFlags") & level) {
			SendClientMessage(i, color, msg);
		}
	}
}
ConnectionMessage(color, const msg[]) {
	foreach(Player, i) {
		if(GetPVarInt(i, "ConnectionMessages") == 1) {
			SendClientMessage(i, color, msg);
		}
	}
}
public KickEx(playerid, reason[]) {
	SendClientMessage(playerid, X11_TOMATO_2, reason);
	SetTimerEx("TimedKick",250, false, "d",playerid);
	//Kick(playerid);
}
public BanPlayer(playerid, reason[], bannerid, bool:gpciban, expiretime) {
	new GPCI[64];
	query[0] = 0;//[256];
	new ip[16];
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags:EAdminFlags_Unbannable) {
		return 1;
	}
	if(gpciban) {
		gpci(playerid, GPCI, sizeof(GPCI));
	} else {
		GPCI[0] = 0;
	}
	new bannersqlid;
	if(bannerid == -1) {
		bannersqlid = 0;
	} else {
		bannersqlid = GetPVarInt(bannerid, "AccountID");
	}
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	new reason_esc[(128*2)+1];
	mysql_real_escape_string(reason,reason_esc);
	new timestamp[128];
	if(expiretime != 0) {
		format(timestamp, sizeof(timestamp), "DATE_ADD(CURRENT_TIMESTAMP(), INTERVAL %d SECOND)", expiretime);
	} else {
		format(timestamp, sizeof(timestamp), "TIMESTAMP(0)");
	}
	format(query,sizeof(query),"INSERT INTO `bans` (`accountid`,`GPCI`,`ip`,`bannerid`,`reason`,`expiretime`) VALUES (%d,\"%s\",INET_ATON(\"%s\"),%d,\"%s\",%s)",GetPVarInt(playerid,"AccountID"),GPCI,ip,bannersqlid,reason_esc,timestamp);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	incPlayerBans(playerid);
	Kick(playerid);
	format(query, sizeof(query), "Banned: Expire Time: %d hours %s",expiretime/3600,reason);
	AddAccountNote(playerid, bannerid, query);
	return 1;
}
stock GetNewbieName(playerid) {
	new ret[8];
	new level = GetPVarInt(playerid, "NewbieRank");
	if(level == 1) {
		strmid(ret, "Helper",0, 6, sizeof(ret));
	} else if(level == 2) {
		strmid(ret, "Teacher",0, 7, sizeof(ret));
	} else {
		strmid(ret, "Player",0, 6, sizeof(ret));
	}
	return ret;
}
unloadCmds(playerid) {
	for(new i=0;i<sizeof(admincmds);i++) {
		Command_SetPlayerNamed(admincmds[i][ECmdName], playerid, false);
		SetCmdAlts(i, playerid, false);
	}
}
loadCmds(playerid) {
	for(new i=0;i<sizeof(admincmds);i++) {
		if(isCmdAllowed(playerid,i )) {
			Command_SetPlayerNamed(admincmds[i][ECmdName], playerid, true);
			SetCmdAlts(i, playerid, true);
		} else {
			SetCmdAlts(i, playerid, false);
		}
	}
}
GetPVarTypeName(typeid, dst[], dstlen) {
	switch(typeid) {
		case PLAYER_VARTYPE_NONE: {
			strmid(dst,"none",0, 4, dstlen);
		}
		case PLAYER_VARTYPE_INT: {
			strmid(dst,"int",0, 3, dstlen);
		}
		case PLAYER_VARTYPE_STRING: {
			strmid(dst,"string",0, 6, dstlen);
		}
		case PLAYER_VARTYPE_FLOAT: {
			strmid(dst,"float",0, 5, dstlen);
		}
	}
	return 0;
}

adminOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	query[0] = 0;//[256];
	new editingadmin = GetPVarInt(playerid, "AdminEdit");
	switch(dialogid) {
		case EAdminDialog_ModifyAdmin: {
			if(GetPVarType(playerid,"AdminEdit") == PLAYER_VARTYPE_NONE) return 0;
			if(response) {
			new EAdminFlags:aflags = EAdminFlags:GetPVarInt(editingadmin, "AdminFlags");
			new EAdminFlags:setterflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
			if(listitem < sizeof(AdminFlagDescription)) {
			if(~setterflags & EAdminFlags_Scripter) {
				if(~setterflags & AdminFlagDescription[listitem][EAFIFlag]) {
					ShowAdminFlagsMenu(playerid, editingadmin);
					SendClientMessage(playerid, X11_TOMATO_2, "You cannot elevate your privileges.");
					return 1;
				}
			}
			if(AdminFlagDescription[listitem][EAFIFlag] == EAdminFlags_None) {
				aflags = EAdminFlags_None;
			} else if(aflags & AdminFlagDescription[listitem][EAFIFlag]) {
					aflags &= ~AdminFlagDescription[listitem][EAFIFlag];
				} else {
					aflags |= AdminFlagDescription[listitem][EAFIFlag];
				}
			SetPVarInt(editingadmin,"AdminFlags",_:aflags);
			unloadCmds(editingadmin);
			loadCmds(editingadmin);
			format(query,sizeof(query),"UPDATE `accounts` SET `adminlevel` = %d WHERE `id` = %d",GetPVarInt(editingadmin,"AdminFlags"), GetPVarInt(editingadmin,"AccountID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			ShowAdminFlagsMenu(playerid, editingadmin);
			} else {
				listitem -= sizeof(AdminFlagDescription);
				SetPVarInt(playerid, "AdminModType", listitem);
				ShowPlayerDialog(playerid,EAdminDialog_SetAdminFlag,DIALOG_STYLE_INPUT,"Enter Modification Value","Enter the value to perform the operation with:","Ok","Cancel");
				}
			} else {
				if(GetPVarInt(playerid, "AdminEditFlags") != GetPVarInt(editingadmin, "AdminFlags")) {
					format(query, sizeof(query), "* %s has edited %s's admin flags to be: %d",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(editingadmin, ENameType_AccountName),GetPVarInt(editingadmin,"AdminFlags"));
					ABroadcast(X11_YELLOW, query, EAdminFlags_All);
				}
				DeletePVar(playerid, "AdminEdit");
				DeletePVar(playerid, "AdminEditFlags");
			}
		}
		case EAdminDialog_SetAdminFlag: {
			if(GetPVarType(playerid,"AdminEdit") == PLAYER_VARTYPE_NONE) return 0;
			if(response) {
				new EAdminFlags:aflags = EAdminFlags:GetPVarInt(editingadmin, "AdminFlags");
				new type = GetPVarInt(playerid, "AdminModType");
				new EAdminFlags:value = EAdminFlags:strval(inputtext);
				switch(type) {
					case 0: {
						aflags &= ~value;
					}
					case 1: {
						aflags &= value;
					}
					case 2: {
						aflags = value;
					}
				
				}
				unloadCmds(editingadmin);
				loadCmds(editingadmin);
				SetPVarInt(editingadmin,"AdminFlags",_:aflags);
				format(query,sizeof(query),"UPDATE `accounts` SET `adminlevel` = %d WHERE `id` = %d",GetPVarInt(editingadmin,"AdminFlags"), GetPVarInt(editingadmin,"AccountID"));
				mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
				ShowAdminFlagsMenu(playerid, editingadmin);
			}
			DeletePVar(playerid, "AdminModType");
		}
		case EAdminDialog_AdminHelp: {
			if(!response) return 1;
			new flagidx = getAdminHelpItem(playerid, listitem);
			if(flagidx != -1)
				sendAdminHelp(playerid, AdminFlagDescription[flagidx][EAFIFlag]);
		}
		case EAdminDialog_AdminCmdHelp: {
			if(!response) return 1;
			new EAdminFlags:flag = EAdminFlags:GetPVarInt(playerid, "AHelpFlag");
			sendAdminCmdHelp(playerid, getListedCommand(flag, listitem));
			DeletePVar(playerid, "AHelpFlag");
		}
		case EAdminDialog_SetStatMain: {
			if(!response) {
				new target = GetPVarInt(playerid, "StatTarget");
				DeletePVar(playerid, "SetModifying");
				DeletePVar(playerid, "StatTarget");
				if(GetPVarType(target, "BeingModified") != PLAYER_VARTYPE_NONE)
					DeletePVar(target, "BeingModified");
				return 1;
			}
			showStatModify(playerid, getPlayerSetStatOption(playerid, listitem));
		}
		case EAdminDialog_StatEdit: {
			if(!response) return 1;
			new msg[128];
			new option = GetPVarInt(playerid, "SetModifying");
			new target = GetPVarInt(playerid, "StatTarget");
			if(GetPVarType(target, "BeingModified") == PLAYER_VARTYPE_NONE) {
				SendClientMessage(playerid, X11_WHITE, "Player has disconnected");
				DeletePVar(playerid, "SetModifying");
				DeletePVar(playerid, "StatTarget");
				return 1;
			}
			switch(SetStatOptions[option][ESetStatOptionType]) {
				case ESetStatType_Int: {
					SetPVarInt(target, SetStatOptions[option][ESetStatPVarName], strval(inputtext));
					format(msg, sizeof(msg), "An admin has modified your %s to be: %d",SetStatOptions[option][ESetStatName],strval(inputtext));
					SendClientMessage(target, X11_YELLOW, msg);
					format(msg, sizeof(msg), "You modified %s(%d)'s %s to be: %d",GetPlayerNameEx(target, ENameType_CharName), target ,SetStatOptions[option][ESetStatName],strval(inputtext));
					SendClientMessage(playerid, X11_YELLOW, msg);
				}
				case ESetStatType_String: {
					SetPVarString(target, SetStatOptions[option][ESetStatPVarName], inputtext);
					format(msg, sizeof(msg), "An admin has modified your %s to be: %s",SetStatOptions[option][ESetStatName],inputtext);
					SendClientMessage(target, X11_YELLOW, msg);
					format(msg, sizeof(msg), "You modified %s(%d)'s %s to be: %s",GetPlayerNameEx(target, ENameType_CharName), target ,SetStatOptions[option][ESetStatName],inputtext);
					SendClientMessage(playerid, X11_YELLOW, msg);
				}
				case ESetStatType_Float: {
					SetPVarFloat(target, SetStatOptions[option][ESetStatPVarName], floatstr(inputtext));
					format(msg, sizeof(msg), "An admin has modified your %s to be: %f",SetStatOptions[option][ESetStatName],floatstr(inputtext));
					SendClientMessage(target, X11_YELLOW, msg);
					format(msg, sizeof(msg), "You modified %s(%d)'s %s to be: %f",GetPlayerNameEx(target, ENameType_CharName), target ,SetStatOptions[option][ESetStatName],floatstr(inputtext));
					SendClientMessage(playerid, X11_YELLOW, msg);
				}
				case ESetStatType_CallFunction: {
					CallLocalFunction(SetStatOptions[option][ESetStatPVarName], "dd", playerid, target);
					return 1;
				}
			}
			if(SetStatOptions[option][ESetStatSendWarning] == 1) {
				format(msg, sizeof(msg), "AdmWarn: %s set %s's %s to %s", GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_CharName), SetStatOptions[option][ESetStatName], inputtext);
				ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
			}
			setStatSync(target);
		}
		case EAdminDialog_RequestChat: {
			new msg[128];
			new rcsender = GetPVarInt(playerid, "RequestChatSender");
			if(!response) {
				format(msg, sizeof(msg), "* Admin conversation with %s declined",GetPlayerNameEx(playerid, ENameType_AccountName));
				SendClientMessage(rcsender, COLOR_LIGHTBLUE, msg);
				format(msg, sizeof(msg), "* Admin conversation with %s declined",GetPlayerNameEx(rcsender, ENameType_AccountName));
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				return 1;
			} else {
				format(msg, sizeof(msg), "* Now in an admin conversation with %s",GetPlayerNameEx(playerid, ENameType_AccountName));
				SendClientMessage(rcsender, COLOR_LIGHTBLUE, msg);
				format(msg, sizeof(msg), "* Now in an admin conversation with %s",GetPlayerNameEx(rcsender, ENameType_AccountName));				
				SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
				
				SetPVarInt(playerid, "RequestChat", rcsender);
				SetPVarInt(rcsender, "RequestChat", playerid);
			}
		}
		case EAdminDialog_CarMenu: {
			if(!response) return 0;
			new Float:X,Float:Y,Float:Z,Float:A;
			GetPlayerPos(playerid, X, Y, Z);
			GetPlayerFacingAngle(playerid, A);
			new c1, c2;
			c1 = RandomEx(0,100);
			c2 = RandomEx(0,100);
			new msg[128];
			new model;
			model = listitem+400;
			if(isBannedVehicle(model) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
				SendClientMessage(playerid, COLOR_LIGHTRED, "You can't spawn that vehicle!");
				return 1;
			}
			new carid = CreateTempCar(model,X, Y,Z, A, c1, c2, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
			format(msg, sizeof(msg), "* Vehicle %d spawned",carid);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
		}
		case EAdminDialog_TPMenu: {
			if(!response) return 0;
			if(!IsPlayerInAnyVehicle(playerid)) {
				SetPlayerPos(playerid, TeleportOptions[listitem][ETPMenuX], TeleportOptions[listitem][ETPMenuY], TeleportOptions[listitem][ETPMenuZ]);
				SetPlayerVirtualWorld(playerid, TeleportOptions[listitem][ETPMenuVW]);
				SetPlayerInterior(playerid, 0);
				SetPlayerFacingAngle(playerid, TeleportOptions[listitem][ETPMenuAngle]);
			} else {
				new carid = GetPlayerVehicleID(playerid);
				TPEntireCar(carid, TeleportOptions[listitem][ETPMenuInterior], TeleportOptions[listitem][ETPMenuVW]);
				LinkVehicleToInterior(carid, TeleportOptions[listitem][ETPMenuInterior]);
				SetVehicleVirtualWorld(carid, TeleportOptions[listitem][ETPMenuVW]);
				SetVehicleZAngle(carid, TeleportOptions[listitem][ETPMenuAngle]);
				SetVehiclePos(carid, TeleportOptions[listitem][ETPMenuX], TeleportOptions[listitem][ETPMenuY], TeleportOptions[listitem][ETPMenuZ]);
			}
			SetCameraBehindPlayer(playerid);
			SendClientMessage(playerid, X11_ORANGE, "You have been teleported");
		}
		case EAdminDialog_SetJob: {
			if(!response) {
				DeletePVar(playerid, "SetJobTarget");
				return 0;
			}
			new target = GetPVarInt(playerid, "SetJobTarget");
			new msg[128];
			if(listitem < 0 || listitem > GetNumJobs()) {
				format(msg, sizeof(msg), "* An admin has removed your job");
				SendClientMessage(target, X11_YELLOW, msg);
				format(msg, sizeof(msg), "* You have removed %s's job",GetPlayerNameEx(target, ENameType_CharName));
				SendClientMessage(playerid, X11_YELLOW, msg);
				SetPVarInt(target, "Job", -1);
			} else {
				format(msg, sizeof(msg), "* An admin has set your job to %s",GetJobName(listitem));
				SendClientMessage(target, X11_YELLOW, msg);
				format(msg, sizeof(msg), "* You have set %s's job to %s",GetPlayerNameEx(target, ENameType_CharName),GetJobName(listitem));
				SendClientMessage(playerid, X11_YELLOW, msg);
				SetPVarInt(target, "Job", listitem);
			}
			DeletePVar(playerid, "SetJobTarget");
		}
		case EAdminDialog_SetSex: {
			if(!response) return 0;
			new msg[128];
			new target = GetPVarInt(playerid, "SetSexTarget");
			format(msg, sizeof(msg), "* An admin has set your sex to %s",GetSexName(listitem));
			SendClientMessage(target, X11_YELLOW, msg);
			format(msg, sizeof(msg), "* You have set %s's sex to %s",GetPlayerNameEx(target, ENameType_CharName),GetSexName(listitem));
			SendClientMessage(playerid, X11_YELLOW, msg);
			SetPVarInt(target, "Sex", listitem);
			DeletePVar(playerid, "SetSexTarget");
		}
		case EAdminDialog_SetDonateRank: {
			if(!response) return 0;
			new msg[128];
			new target = GetPVarInt(playerid, "SetDonateRankTarget");
			format(msg, sizeof(msg), "* An admin has set your donate rank to %s",GetDonateRank(listitem));
			SendClientMessage(target, X11_YELLOW, msg);
			format(msg, sizeof(msg), "* You have set %s's donate rank to %s",GetPlayerNameEx(target, ENameType_CharName),GetDonateRank(listitem));
			SendClientMessage(playerid, X11_YELLOW, msg);
			SetPVarInt(target, "DonateRank", listitem);
			DeletePVar(playerid, "SetDonateRankTarget");
			SetPlayerColor(target, getNameTagColour(target));
		}
		case EAdminDialog_SetFightStyle: {
			new msg[128];
			if(!response) return 0;
			switch(listitem) {
				case 0: listitem = FIGHT_STYLE_NORMAL;
				case 1: listitem = FIGHT_STYLE_BOXING;
				case 2: listitem = FIGHT_STYLE_KUNGFU;
				case 3: listitem = FIGHT_STYLE_KNEEHEAD;
				case 4: listitem = FIGHT_STYLE_GRABKICK;
				case 5: listitem = FIGHT_STYLE_ELBOW;
			}
			new target = GetPVarInt(playerid, "SetFightStyleTarget");
			format(msg, sizeof(msg), "* An admin has set your Fight Style to %s",GetFightStyle(listitem));
			SendClientMessage(target, X11_YELLOW, msg);
			format(msg, sizeof(msg), "* You have set %s's Fight Style to %s",GetPlayerNameEx(target, ENameType_CharName),GetFightStyle(listitem));
			SendClientMessage(playerid, X11_YELLOW, msg);
			SetPVarInt(target, "FightStyle", listitem);
			SetPlayerFightingStyle(target, listitem);
			DeletePVar(playerid, "SetDonateRankTarget");
		}
		case EAdminDialog_SetKicksAJBans: {
			if(!response) {
				return 0;
			}
			new target = GetPVarInt(playerid, "SetNumPropTarget"); 
			new numpropsetting = GetPVarInt(playerid, "NumPropSetting");
			new amount = strval(inputtext);
			setNumAJBansKicks(playerid, amount, numpropsetting, target);
			DeletePVar(playerid, "SetNumPropTarget");
			DeletePVar(playerid, "NumPropSetting");
		}
		case EAdminDialog_TestDecide: {
			new msg[128];
			new playerediting = GetPVarInt(playerid, "PlayerEditing");
			if(playerediting == playerid) {
				//Cannot correct his / her own test
				return 0;
			}
			if(response) {
				SendClientMessage(playerediting, COLOR_CUSTOMGOLD, "Your account has been accepted by an administrator or helper!");
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "You accepted the player's RP test!");
				deleteQuizAnswers(playerediting);
				new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerediting, "AccountFlags");
				if(~aflags & EAccountFlags_MustRedoTest) {
					OnQuizPass(playerediting);
				} else {
					ShowPlayerDialog(playerediting, EAdminDialog_DoNothing, DIALOG_STYLE_MSGBOX, "{00BFFF}Test:","Your Test has been accepted!", "Ok", "");
				}
				setTest(playerediting, 0);
				format(msg, sizeof(msg), "Tests: %s accepted %s's test.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playerediting, ENameType_AccountName));
				foreach(Player, i) {
					if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
						continue;
					}
					SendClientMessage(i, X11_YELLOW, msg);
				}
			} else {
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "You denied the player's RP test!");
				format(msg, sizeof(msg), "Tests: %s denied %s's test. Reason: %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(playerediting, ENameType_AccountName), inputtext);
				foreach(Player, i) {
					if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
						continue;
					}
					SendClientMessage(i, X11_YELLOW, msg);
				}
                format(msg, sizeof(msg),"Your account has been denied: %s",inputtext);
				SendClientMessage(playerediting, X11_TOMATO_2, msg);
				SetTimerEx("KickEx",1000, false, "ds",playerediting, msg);
			}
			SetPVarInt(playerid, "PlayerEditing", -1);
			DeletePVar(playerid, "CorrectingTest");
			destroyRPTest3dTextLabel(playerid);
			return 1;
		}
		case EAdminDialog_DoNothing: {
			return 1;
		}
		case EAdminDialog_NonRPName: {
			if(!response) {
				format(tempstr, sizeof(tempstr), "AdmWarn: %s has been kicked for refusing to change their name.",GetPlayerNameEx(playerid, ENameType_CharName));
				ABroadcast(X11_YELLOW, tempstr, EAdminFlags_BasicAdmin);
				KickEx(playerid, "Failure to change name");
				return 0;
			}
			if(NameIsRP(inputtext) && !IsNameRestricted(inputtext) && strlen(inputtext) < MAX_PLAYER_NAME) {
				IsNameTaken(inputtext, "OnNONRPNameTakenCheck", playerid);
			} else {
				showNONRPNameDialog(playerid,1);
			}
		}
	}
	return 1;
}
forward OnNONRPNameTakenCheck(playerid, name[]);
public OnNONRPNameTakenCheck(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	if(strlen(name) > MAX_PLAYER_NAME) {
		showNONRPNameDialog(playerid,3);
		return 1;
	}
	if(rows > 0) {
		showNONRPNameDialog(playerid,2);
	} else {
		format(msg, sizeof(msg), "AdmWarn: %s has changed name to %s via a NON-RP Name command.",GetPlayerNameEx(playerid, ENameType_CharName),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_BasicAdmin);
		changeName(playerid, name);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Your name has been changed.");
	}
	return 1;
}
setNumAJBansKicks(playerid, amount, E_PROPERTY_SETTING, targetid) {
	#if debug
	printf("Called setNumAJBansKicks(%d, %d, %d, %d)",playerid, amount, E_PROPERTY_SETTING, targetid);
	#endif
	new msg[128];
	switch(E_PROPERTY_SETTING) {
		case EAdmin_SettingNumKicks: {
			format(query, sizeof(query), "UPDATE `accounts` SET `numkicks` = %d WHERE `id` = %d",amount, GetPVarInt(targetid, "AccountID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			SetPVarInt(targetid, "NumKicks", amount);
		}
		case EAdmin_SettingNumAJails: {
			format(query, sizeof(query), "UPDATE `accounts` SET `numajails` = %d WHERE `id` = %d",amount, GetPVarInt(targetid, "AccountID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			SetPVarInt(targetid, "NumAJails", amount);
		}
		case EAdmin_SettingNumBans: {
			format(query, sizeof(query), "UPDATE `accounts` SET `numbans` = %d WHERE `id` = %d",amount, GetPVarInt(targetid, "AccountID"));
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
			SetPVarInt(targetid, "NumBans", amount);
		}
	}
	format(msg, sizeof(msg), "* An admin has set your number of %s to %d",getAJBansKickPropName(E_PROPERTY_SETTING), amount);
	SendClientMessage(targetid, X11_YELLOW, msg);
	format(msg, sizeof(msg), "* You have set %s's number of %s to %d",GetPlayerNameEx(targetid, ENameType_CharName), getAJBansKickPropName(E_PROPERTY_SETTING), amount);
	SendClientMessage(playerid, X11_YELLOW, msg);
	format(msg, sizeof(msg), "AdmWarn: %s has set %s's number of %s to %d",GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(targetid, ENameType_AccountName), getAJBansKickPropName(E_PROPERTY_SETTING), amount);
	ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
	return 1;
}
stock getAJBansKickPropName(E_PROP_ID) {
	new msg[128];
	switch(E_PROP_ID) {
		case EAdmin_SettingNumKicks: {
			format(msg, sizeof(msg), "Admin Kicks");
		}
		case EAdmin_SettingNumAJails: {
			format(msg, sizeof(msg), "Admin Jails");
		}
		case EAdmin_SettingNumBans: {
			format(msg, sizeof(msg), "Admin Bans");
		}
	}
	return msg;
}
endRequestChat(playerid, ERequestchatEndReason:reason) {
	new rcuser = GetPVarInt(playerid, "RequestChat");
	new msg[128];
	new reasonstr[32];
	switch(reason) {
		case RcExit_Disconnected: {
			strmid(reasonstr,"Disconnected",0, 12, sizeof(reasonstr));
		}
		case RcExit_TimedOut: {
			strmid(reasonstr,"Timed Out",0, 9, sizeof(reasonstr));
		}
		case RcExit_Kicked: {
			strmid(reasonstr,"Kicked",0, 6, sizeof(reasonstr));
		}
		case RcExit_Exited: {
			strmid(reasonstr,"Conversation Terminated",0, 23, sizeof(reasonstr));
		}
		case RcExit_SwitchChar: {
			strmid(reasonstr,"Switched Characters",0, 19, sizeof(reasonstr));
		}
		
		default: {
			strmid(reasonstr,"Unknown",0, 7, sizeof(reasonstr));
		}
	}
	if(GetPVarInt(rcuser,"RequestChat") == playerid && IsPlayerConnected(rcuser)) {
		format(msg, sizeof(msg), "* Admin conversation with %s ended (%s)",GetPlayerNameEx(playerid, ENameType_AccountName),reasonstr);
		SendClientMessage(rcuser, COLOR_LIGHTBLUE, msg);
	}
	if(IsPlayerConnected(playerid)) {
		format(msg, sizeof(msg), "* Admin conversation with %s ended (%s)",GetPlayerNameEx(rcuser, ENameType_AccountName),reasonstr);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	DeletePVar(playerid, "RequestChat");
	DeletePVar(rcuser, "RequestChat");
	return 1;
}
YCMD:bigmouth(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles Big Mouth");
		return 1;
	}
	if(GetPVarType(playerid, "BigMouth") != PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Big Mouth Off");
		DeletePVar(playerid, "BigMouth");
	} else {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "* Big Mouth On");
		SetPVarInt(playerid, "BigMouth", 1);
	}
	return 1;
}
adminsOnPlayerText(playerid, text[]) {
	if(GetPVarInt(playerid, "BigMouth") == 1) {
		return 2;
	}
	if(GetPVarType(playerid, "RequestChat") != PLAYER_VARTYPE_NONE) {
		new achatuser = GetPVarInt(playerid, "RequestChat");
		new msg[128];
		format(msg, sizeof(msg), "[Admin Chat] %s: %s",GetPlayerNameEx(playerid, ENameType_AccountName),text);
		SendClientMessage(achatuser, COLOR_PINK, msg);
		SendClientMessage(playerid, COLOR_PINK, msg);
		SendBigEarsMessage(COLOR_PINK, msg);
		return 1;
	}
	return 0;
}
setStatSync(playerid) {
	new level = GetPVarInt(playerid, "Level");
	SetPlayerScore(playerid, level);
	SetPlayerColor(playerid, getNameTagColour(playerid));
}
showStatModify(playerid, option) {
	if(SetStatOptions[option][ESetStatOptionType] == ESetStatType_CallFunction) {
		CallLocalFunction(SetStatOptions[option][ESetStatPVarName], "dd", playerid, GetPVarInt(playerid, "StatTarget"));
		return 1;
	}
	new msg[128];
	SetPVarInt(playerid, "SetModifying", option);
	format(msg, sizeof(msg), "{FFFFFF}Enter the value you would like to set the {FF0000}%s{FFFFFF} to",SetStatOptions[option][ESetStatName]);
	ShowPlayerDialog(playerid, EAdminDialog_StatEdit, DIALOG_STYLE_INPUT, "Set Stat",msg, "Select", "Quit");
	return 0;
}
sendAdminCmdHelp(playerid, cmdidx) {
	if(cmdidx > 0 && cmdidx < sizeof(admincmds)) {
		Command_ReProcess(playerid, admincmds[cmdidx][ECmdName], true);
	}
}
sendAdminHelp(playerid, EAdminFlags:flag) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	SetPVarInt(playerid, "AHelpFlag", _:flag);
	for(new i=0;i<sizeof(admincmds);i++) {
		if(flag & admincmds[i][ECmdAdminFlags]) {
			format(tempstr,sizeof(tempstr),"%s\n",admincmds[i][ECmdName]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EAdminDialog_AdminCmdHelp, DIALOG_STYLE_LIST, "Admin Help",dialogstr,"Select", "Exit");
	return 1;
}
getListedCommand(EAdminFlags:flag, index) {
	new x;
	for(new i=0;i<sizeof(admincmds);i++) {
		if(flag & admincmds[i][ECmdAdminFlags]) {
			if(x++ == index) {
				return i;
			}
		}
	}
	return -1;
}

YCMD:ahelp(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends admin help");
		return 1;
	}
	dialogstr[0] = 0;
	tempstr[0] = 0;
	new EAdminFlags:arights = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	for(new i=2;i<sizeof(AdminFlagDescription);i++) {
		if(arights & AdminFlagDescription[i][EAFIFlag]) {
			format(tempstr,sizeof(tempstr),"%s\n",AdminFlagDescription[i][EAFIDesc]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, EAdminDialog_AdminHelp, DIALOG_STYLE_LIST, "Admin Help",dialogstr,"Select", "Exit");
	return 1;
}
YCMD:carmenu(playerid, params[], help) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shows a list of cars to spawn");
	}
	for(new i=0;i<sizeof(VehiclesName);i++) {
			format(tempstr,sizeof(tempstr),"%s (ID: %d)\n",VehiclesName[i],i+400);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAdminDialog_CarMenu, DIALOG_STYLE_LIST, "Vehicle Spawn",dialogstr,"Select", "Exit");
	return 1;
}

YCMD:report(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for reporting a player");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned!");
		return 1;
	}
	new input[128],msg[128];
	if(!sscanf(params, "s[128]", input)) {
		new time = GetPVarInt(playerid, "RequestChatCooldown");
		new timenow = gettime();
		if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must wait before sending another report!");
			return 1;
		}
		SetPVarInt(playerid, "RequestChatCooldown", gettime());
		format(msg, sizeof(msg), "Report from [%d]%s: {EE0000}%s", playerid, GetPlayerNameEx(playerid, ENameType_CharName), input);
		ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
		SendClientMessage(playerid, X11_YELLOW, "Your report was sent to the admin team:");
		SendClientMessage(playerid, X11_YELLOW, input);
		SetPVarInt(playerid, "SentReport", 1);
		SetPVarString(playerid, "ReportText", input);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /report [message]");
	}
	return 1;
}
YCMD:stuck(playerid, params[], help) {
	new msg[128];
	new time = GetPVarInt(playerid, "RequestChatCooldown");
	new timenow = gettime();
	if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must wait before sending another report!");
		return 1;
	}
	if(GetPVarInt(playerid, "ReportBanned") == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are report banned!");
		return 1;
	}
	SetPVarInt(playerid, "RequestChatCooldown", gettime());
	format(msg, sizeof(msg), "Report from %s: /Unfreeze [%d] (/Stuck) ",GetPlayerNameEx(playerid, ENameType_CharName), playerid);
	ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
	SendClientMessage(playerid, X11_YELLOW, "The admins have been notified of your request");
	return 1;
}
YCMD:reports(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists unanswered reports");
		return 1;
	}
	new text[128];
	new report[128];
	SendClientMessage(playerid, X11_YELLOW, "Unanswered Reports: ");
	for(new i=0;i<MAX_PLAYERS;i++) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "SentReport") == 1) {
				GetPVarString(i, "ReportText", report, sizeof(report));
				format(text, sizeof(text), "Report: %s(%d): {EE0000}%s",GetPlayerNameEx(i, ENameType_CharName), i, report);
				SendClientMessage(playerid, X11_ORANGE, text);
			}
		}
	}
	return 1;
}
YCMD:acceptreport(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Accepts a report");
		return 1;
	}
	new user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarType(user, "SentReport") == PLAYER_VARTYPE_NONE && GetPVarType(user, "RequestChatCooldown") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user has not sent a report!");
			return 1;
		}
		format(msg, sizeof(msg), "%s has accepted %s's report", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName));
		ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
		format(msg, sizeof(msg), "%s has accepted your report", GetPlayerNameEx(playerid, ENameType_AccountName));
		SendClientMessage(user, X11_ORANGE, msg);
		DeletePVar(user, "SentReport");
		DeletePVar(user, "ReportText");
		DeletePVar(user, "RequestChatCooldown");
	}
	return 1;
}
YCMD:denyreport(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Accepts a report");
		return 1;
	}
	new user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarType(user, "SentReport") == PLAYER_VARTYPE_NONE && GetPVarType(user, "RequestChatCooldown") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user has not sent a report!");
			return 1;
		}
		format(msg, sizeof(msg), "%s has denied %s's report", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName));
		ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
		format(msg, sizeof(msg), "%s has denied your report", GetPlayerNameEx(playerid, ENameType_AccountName));
		SendClientMessage(user, X11_ORANGE, msg);
		DeletePVar(user, "SentReport");
		DeletePVar(user, "ReportText");
		DeletePVar(user, "RequestChatCooldown");
	}
	return 1;
}
YCMD:invalidreport(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Accepts a report");
		return 1;
	}
	new user;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
			return 1;
		}
		if(GetPVarType(user, "SentReport") == PLAYER_VARTYPE_NONE && GetPVarType(user, "RequestChatCooldown") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user has not sent a report!");
			return 1;
		}
		format(msg, sizeof(msg), "%s has marked %s's report as invalid!", GetPlayerNameEx(playerid, ENameType_AccountName), GetPlayerNameEx(user, ENameType_CharName));
		ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
		format(msg, sizeof(msg), "%s has marked your report as invalid", GetPlayerNameEx(playerid, ENameType_AccountName));
		SendClientMessage(user, X11_ORANGE, msg);
		DeletePVar(user, "SentReport");
		DeletePVar(user, "ReportText");
		DeletePVar(user, "RequestChatCooldown");
	}
	return 1;
}

YCMD:givecookie(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives someone a cookie");
	}
	new msg[128];
	new user;
	if(!sscanf(params, "k<playerLookup_acc>",user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new numcookies = GetPVarInt(user, "Cookies");
		format(msg, sizeof(msg), "* %s awarded you with a cookie.", GetPlayerNameEx(playerid, ENameType_CharName));
		SendClientMessage(user, X11_ORANGE, msg);
		format(msg, sizeof(msg), "* You awarded %s with a cookie.", GetPlayerNameEx(user, ENameType_CharName));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
		format(msg, sizeof(msg), "System: %s awarded a cookie to %s!", GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(user, ENameType_CharName));
		Broadcast(COLOR_CUSTOMGOLD, msg);
		SetPVarInt(user,"Cookies",++numcookies);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givecookie [playerid/name]");
	}
	return 1;
}
YCMD:takecookie(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives someone a cookie");
	}
	new msg[128];
	new user;
	if(!sscanf(params, "k<playerLookup_acc>",user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new numcookies = GetPVarInt(user, "Cookies");
		format(msg, sizeof(msg), "* %s took a cookie from you.", GetPlayerNameEx(playerid, ENameType_CharName));
		SendClientMessage(user, X11_ORANGE, msg);
		format(msg, sizeof(msg), "* You took a cookie from %s.", GetPlayerNameEx(user, ENameType_CharName));
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
		format(msg, sizeof(msg), "System: %s taken a cookie from %s!", GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(user, ENameType_CharName));
		Broadcast(COLOR_CUSTOMGOLD, msg);
		SetPVarInt(user,"Cookies",--numcookies);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givecookie [playerid/name]");
	}
	return 1;
}

YCMD:ninvite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Invites someone into the helper team");
		return 1;
	}
	new nlevel, playa;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		nlevel = GetPVarInt(playa, "NewbieRank");
		if(nlevel != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot invite this person into the newbie team");
			return 1;
		}
		format(msg, sizeof(msg), "(( %s invited %s into the newbie team ))", GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playa, ENameType_CharName));
		foreach(Player, i) {
			if(GetPVarInt(playerid, "ConnectTime") > 50) {
				if(GetPVarInt(playerid, "NewbieRank") < 1 && EAdminFlags:GetPVarInt(playerid, "AdminLevel") == EAdminFlags_None) {
					continue;
				}
			}
			SendClientMessage(i, COLOR_CUSTOMGOLD, msg);
		}
		SetPVarInt(playa, "NewbieRank", 1);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ninvite [playerid/name]");
	}
	return 1;
}
YCMD:nuninvite(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Invites someone into the helper team");
		return 1;
	}
	new nlevel, playa;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		nlevel = GetPVarInt(playa, "NewbieRank");
		if(nlevel != 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot uninvite this person from the newbie team");
			return 1;
		}
		format(msg, sizeof(msg), "(( %s kicked %s from the newbie team ))", GetPlayerNameEx(playerid, ENameType_CharName), GetPlayerNameEx(playa, ENameType_CharName));
		foreach(Player, i) {
			if(GetPVarInt(playerid, "ConnectTime") > 50) {
				if(GetPVarInt(playerid, "NewbieRank") < 1 && EAdminFlags:GetPVarInt(playerid, "AdminLevel") == EAdminFlags_None) {
					continue;
				}
			}
			SendClientMessage(i, COLOR_CUSTOMGOLD, msg);
		}
		SetPVarInt(playa, "NewbieRank", 0);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ninvite [playerid/name]");
	}
	return 1;
}
YCMD:maketeacher(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Invites or removes someone from the teacher team");
		return 1;
	}
	new nlevel, playa;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>", playa)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		nlevel = GetPVarInt(playa, "NewbieRank");
		if(nlevel == 2) {
			nlevel = 0;
		} else if(nlevel == 0 || nlevel == 1) {
			nlevel = 2;
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot invite this person into the newbie team");
			return 1;
		}
		if(nlevel == 2) {
			format(msg, sizeof(msg), "(( %s made %s a teacher ))", GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playa, ENameType_CharName));
		} else {
			format(msg, sizeof(msg), "(( %s removed %s's teacher status ))", GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(playa, ENameType_CharName));
		}
		SendClientMessage(playa, COLOR_CUSTOMGOLD, msg);
		foreach(Player, i) {
			if(GetPVarInt(i, "ConnectTime") > 50) {
				if(GetPVarInt(i, "NewbieRank") < 1 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
					continue;
				}
			}
			if(i != playa) {
				SendClientMessage(i, COLOR_CUSTOMGOLD, msg);
			}
		}
		SetPVarInt(playa, "NewbieRank", nlevel);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /maketeacher [playerid/name]");
	}
	return 1;
}

YCMD:setstat(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify a players stats");
		return 1;
	}
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not connected");
			return 1;
		}
		new EAdminFlags:flags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
		for(new i=0;i<sizeof(SetStatOptions);i++) {
			if(flags & SetStatOptions[i][ESetStatRequiredFlag]) {
				format(tempstr,sizeof(tempstr),"%s\n",SetStatOptions[i][ESetStatName]);
				strcat(dialogstr,tempstr,sizeof(dialogstr));
			}
		}
		SetPVarInt(playerid, "StatTarget", target);
		SetPVarInt(target, "BeingModified", playerid); //used for checking if player D/Ced while setstat menu was opened
		ShowPlayerDialog(playerid, EAdminDialog_SetStatMain, DIALOG_STYLE_LIST, "Set Stat",dialogstr,"Select", "Quit");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setstat [playerid/name]");
	}
	return 1;
}
getPlayerSetStatOption(playerid, index) {
	new x;
	new EAdminFlags:flags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	for(new i=0;i<sizeof(SetStatOptions);i++) {
		if(flags & SetStatOptions[i][ESetStatRequiredFlag]) {
			if(x++ == index) {
				return i;
			}
		}
	}
	return -1;
}
getAdminHelpItem(playerid, index) {
	new x;
	new EAdminFlags:arights = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	for(new i=2;i<sizeof(AdminFlagDescription);i++) {
		if(arights & AdminFlagDescription[i][EAFIFlag]) {
			if(x++ == index) {
				return i;
			}
		}
	}
	return -1;
}
ShowAdminFlagsMenu(playerid, playa) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playa, "AdminFlags");
	new EAdminFlags:setterflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	new statustext[32]; 
	for(new i=0;i<sizeof(AdminFlagDescription);i++) {
		if(aflags&AdminFlagDescription[i][EAFIFlag] || (AdminFlagDescription[i][EAFIFlag] == EAdminFlags_None && aflags == EAdminFlags_None)) {
			statustext = "{00FF00}YES";
		} else {
			statustext = "{FF0000}NO";
		}
		format(tempstr,sizeof(tempstr),"{FFFFFF}%s - %s\n",AdminFlagDescription[i][EAFIDesc],statustext);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	if(setterflags & EAdminFlags_Scripter) {
		new title[64];
		format(title,sizeof(title),"AND NOT AdminFlag\nAND AdminFlag\nSet AdminFlags(%d)",_:aflags);
		strcat(dialogstr,title,sizeof(dialogstr));
	}
	SetPVarInt(playerid, "AdminEdit", playa);
	SetPVarInt(playerid, "AdminEditFlags", GetPVarInt(playa, "AdminFlags"));
	ShowPlayerDialog(playerid, EAdminDialog_ModifyAdmin, DIALOG_STYLE_LIST, "Modify Admin Permissions",dialogstr,"Toggle", "Done");
}
stock getAdminName(playerid) {
	new name[32];
	GetPVarString(playerid, "AdminTitle", name, sizeof(name));
	return name;
}
adminOnPlayerStateChange(playerid, newstate, oldstate) {
	if(newstate == PLAYER_STATE_DRIVER || oldstate == PLAYER_STATE_DRIVER || newstate == PLAYER_STATE_PASSENGER || oldstate == PLAYER_STATE_PASSENGER) {
		updateSpectators(playerid);
	} else if(oldstate == PLAYER_STATE_SPECTATING && newstate != PLAYER_STATE_SPECTATING) {
		if(GetPVarType(playerid, "SpecPlayer") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "SpecVeh") != PLAYER_VARTYPE_NONE) {
			DeletePVar(playerid, "SpecPlayer");
			DeletePVar(playerid, "SpecVeh");
			loadSpecDetails(playerid);
		}
	}
}
isSpectating(playerid) {
	if(GetPVarType(playerid, "SpecPlayer") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "SpecVeh") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	return 0;
}
updateSpectators(playerid) {
	new incar = IsPlayerInAnyVehicle(playerid);
	new vw = GetPlayerVirtualWorld(playerid);
	new interior = GetPlayerInterior(playerid);
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && EAdminFlags:GetPVarInt(playerid,"AdminFlags") != EAdminFlags_None) {
			if(GetPVarType(i, "SpecPlayer") == PLAYER_VARTYPE_NONE) continue;
			if(GetPVarInt(i, "SpecPlayer") == playerid) {
				SetPlayerVirtualWorld(i, vw);
				SetPlayerInterior(i, interior);
				if(incar) {
					PlayerSpectateVehicle(i, GetPlayerVehicleID(playerid));
				} else {
					PlayerSpectatePlayer(i, playerid);
				}
			}
		}
	}
	return 0;
}
restoreSpectators(playerid) {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && EAdminFlags:GetPVarInt(playerid,"AdminFlags") != EAdminFlags_None) {
			if(GetPVarType(i, "SpecPlayer") == PLAYER_VARTYPE_NONE) continue;
			if(GetPVarInt(i, "SpecPlayer") == playerid) {
				TogglePlayerSpectating(i, 0);
			}
		}
	}
}
YCMD:setweather(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets the server weather");
		return 1;
	}
	new weather;
	if(!sscanf(params, "d", weather)) {
		SetWeather(weather);
		SendClientMessage(playerid, X11_ORANGE, "Weather Updated!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setweather [weatherid]");
	}
	return 1;
}
YCMD:setplayerweather(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets the server weather");
		return 1;
	}
	new weather, playa;
	if(!sscanf(params, "k<playerLookup_acc>d", playa, weather)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found.");
			return 1;
		}
		SetPlayerWeather(playa, weather);
		SendClientMessage(playerid, X11_ORANGE, "Weather Updated!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setweather [weatherid]");
	}
	return 1;
}
YCMD:settime(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets the server weather");
		return 1;
	}
	new weather, msg[128];
	if(!sscanf(params, "d", weather)) {
		SetWorldTime(weather);
		format(msg, sizeof(msg), "Time set to: %d:00",weather);
		Broadcast(X11_ORANGE, msg);
		SendClientMessage(playerid, X11_ORANGE, "Time updated!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /settime [hour]");
	}
	return 1;
}
YCMD:deathmessages(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enable or disable death messages");
		return 1;
	}
	if(GetPVarType(playerid, "DeathMessages") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "DeathMessages");
		SendClientMessage(playerid, X11_YELLOW, "Death Messages Disabled");
	} else {
		SetPVarInt(playerid, "DeathMessages", 1);
		SendClientMessage(playerid, X11_YELLOW, "Death Messages Enabled");
		
	}
	return 1;
}

YCMD:connectionmessages(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Enable or disable death messages");
		return 1;
	}
	if(GetPVarType(playerid, "ConnectionMessages") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "ConnectionMessages");
		SendClientMessage(playerid, X11_YELLOW, "Connection Messages Disabled");
	} else {
		SetPVarInt(playerid, "ConnectionMessages", 1);
		SendClientMessage(playerid, X11_YELLOW, "Connection Messages Enabled");
		
	}
	return 1;
}

adminOnPlayerDeath(playerid, killerid, reason) {
	new msg[128];
	new weapon[32];
	GetWeaponNameEx(reason, weapon, sizeof(weapon));
	foreach(Player, i) {
		if(GetPVarInt(i, "DeathMessages") == 1) {
			if(killerid != INVALID_PLAYER_ID) {
				format(msg, sizeof(msg), "[DEATH]: %s was killed by %s - %s(%d)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),GetPlayerNameEx(killerid, ENameType_RPName_NoMask),weapon,reason);
			} else {
				format(msg, sizeof(msg), "[DEATH]: %s was killed - %s(%d)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),weapon,reason);
			}
			SendClientMessage(i, COLOR_RED, msg);
		}
	}
}
forward AJail(playerid, time, reason[]);
public AJail(playerid, time, reason[]) {
	#pragma unused reason
	SetPlayerPos(playerid, 2525.92, -1673.35, 14.86);
	SetPlayerVirtualWorld(playerid, playerid+15000);
	SetPlayerInterior(playerid, 0);
	ResetPlayerWeaponsEx(playerid);
	TogglePlayerControllableEx(playerid, 0);
	sendPlayerNotes(playerid, playerid, 1, 1);
	SetPVarInt(playerid, "AJailReleaseTime", gettime()+time);
	hangupCall(playerid, 1);
}
checkAJail() {
	foreach(Player, i) {
		if(GetPVarType(i, "AJailReleaseTime") != PLAYER_VARTYPE_NONE) {
			new rtime = GetPVarInt(i, "AJailReleaseTime");
			if(gettime() > rtime) {
				releaseFromJail(i); //same LSPD releasing
				DeletePVar(i, "AJailReleaseTime");
			}
		}
	}
}
isInJail(playerid) {
	if(GetPVarType(playerid, "AJailReleaseTime") != PLAYER_VARTYPE_NONE) {
		return 1;
	}
	if(GetPVarType(playerid, "JailReleaseTime") != PLAYER_VARTYPE_NONE) {
		return 2;
	}
	if(playerHasPrisonLifeSentence(playerid)) {
		return 2;
	}
	return 0;
}
YCMD:killcheckpoint(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "Removes someones checkpoints");
		return 1;
	}
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(aflags & EAdminFlags_BasicAdmin) {
		new user;
		if(!sscanf(params, "k<playerLookup_acc>", user)) {
			if(!IsPlayerConnectEx(user)) {
				DisablePlayerCheckpoint(user);
				SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Checkpoint cleared");
				SendClientMessage(user, COLOR_CUSTOMGOLD, "An admin has cleared your checkpoints");
			} else {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			}
			return 1;
		}
	}
	DisablePlayerCheckpoint(playerid);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Checkpoint cleared");
	return 1;
}
YCMD:checkprison(playerid, params[], help) {
	new msg[128], smsg[8];
	new isajail, jailtime;
	SendClientMessage(playerid, X11_YELLOW, "* People in jail *");
	foreach(Player, i) {
		jailtime = GetPVarInt(i, "ReleaseTime");
		isajail = 0;
		if(jailtime == 0) {
			jailtime = GetPVarInt(i, "AJailReleaseTime");
			isajail = 1;
		}	
		jailtime -= gettime();
		if(jailtime > 0 || playerHasPrisonLifeSentence(i)) {
			format(smsg, sizeof(smsg), "%s",playerHasPrisonLifeSentence(i) ? ("Life") : getNumberString(jailtime)); 
			format(msg, sizeof(msg), "%s [%d] %s",GetPlayerNameEx(i, ENameType_CharName), i, smsg);
			SendClientMessage(playerid, isajail?COLOR_LIGHTRED:COLOR_CUSTOMGOLD, msg);
		}
	}
	return 1;
}

YCMD:teleportmenu(playerid, params[], help) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	for(new i=0;i<sizeof(TeleportOptions);i++) {
		format(tempstr,sizeof(tempstr),"%s\n",TeleportOptions[i][ETPMenuName]);
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EAdminDialog_TPMenu, DIALOG_STYLE_LIST, "Teleport Menu",dialogstr,"Teleport", "Cancel");
	return 1;
}
YCMD:oldcar(playerid, params[], help) {
	new target = playerid;
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_VehicleAdmin) {
		if(!sscanf(params, "k<playerLookup_acc>", target)) {
			if(!IsPlayerConnectEx(target)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
		}
	}
	new msg[128];
	if(GetPVarType(target, "LastCar") != PLAYER_VARTYPE_NONE) {
		new carid = GetPVarInt(playerid, "LastCar");
		format(msg, sizeof(msg), "* Last Vehicle ID: %d (Model: %d)",carid,GetVehicleModel(carid));
	} else {
		format(msg, sizeof(msg), "* Last Vehicle ID: None");
	}
	SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	return 1;
}
forward ShowSetJobMenu(playerid, targetid);
public ShowSetJobMenu(playerid, targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	for(new i=0;i<GetNumJobs();i++) {
		format(tempstr, sizeof(tempstr), "%s\n",GetJobName(i));
		strcat(dialogstr, tempstr, sizeof(dialogstr));
	}
	strcat(dialogstr, "None\n", sizeof(dialogstr));
	SetPVarInt(playerid, "SetJobTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetJob, DIALOG_STYLE_LIST, "Set Job",dialogstr,"Set", "Cancel");
}
YCMD:netstats(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Get server/player network stats");
		return 1;
	}
	new target;
	new stats[400+1];
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		GetPlayerNetworkStats(target, stats, sizeof(stats));
	} else {
		GetNetworkStats(stats, sizeof(stats));
	}
	ShowPlayerDialog(playerid, EDialogBase_End, DIALOG_STYLE_MSGBOX, "Network Stats", stats, "Close", "");
	
	return 1;
}
YCMD:doanim(playerid, params[], help) {
	new name[32],lib[32],Float:delta, loop, lockx, locky, freeze, time, forcesync;
	new target;
	if(!sscanf(params, "k<playerLookup_acc>s[32]s[32]F(4.0)I(1)I(1)I(1)I(0)I(0)I(0)", target, lib, name, delta, loop, lockx, locky, freeze, time, forcesync)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		ApplyAnimation(target, lib, name, delta, loop, lockx, locky, freeze, time, forcesync);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /doanim [playerid/name] [lib] [name] [delta] [loop] [lockx] [locky] [freeze] [time] [forcesync]");
	}
	return 1;
}
YCMD:clearanim(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Clears a persons animations");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		ClearAnimations(target);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /clearanim [playerid]");
	}
	return 1;
}
YCMD:record(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Start/Stop NPC recordings");
		return 1;
	}
	new user, type, filename[64];
	if(!sscanf(params, "k<playerLookup_acc>ds[64]", user, type, filename)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(GetPVarType(user, "RecordType") == PLAYER_VARTYPE_NONE) {
			SetPVarInt(user, "RecordType", type);
			StartRecordingPlayerData(user, type, filename);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "User is now being recorded");
		} else {
			StopRecordingPlayerData(user);
			DeletePVar(user, "RecordType");
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, "User is no longer being recorded");
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /record [playerid/name] [type] [filename]");
		SendClientMessage(playerid, X11_WHITE, "Types: 0 = none, 1 = driver, 2 = on foot");
	}
	return 1;
}
YCMD:setmotd(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify the MOTD");
		return 1;
	}
	new msg[128];
	new motd_esc[(sizeof(server_motd)*2)+1];
	if(!sscanf(params,"s[128]",server_motd)) {
		format(server_motd, sizeof(server_motd), "%s",FormatColourString(server_motd));
		format(msg, sizeof(msg), "* The server MOTD has been changed to: %s",server_motd);
		SendClientMessageToAll(X11_ORANGE, msg);
		mysql_real_escape_string(server_motd,motd_esc);
		format(query, sizeof(query), "UPDATE `misc` SET `motd` = \"%s\"",motd_esc);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid,X11_ORANGE, "MOTD Cleared!");
		server_motd[0] = 0;
		format(query, sizeof(query), "UPDATE `misc` SET `motd` = \"\"");
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	}
	return 1;
}
YCMD:setamotd(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Modify the admin MOTD");
		return 1;
	}
	new msg[128];
	new amotd_esc[(sizeof(server_amotd)*2)+1];
	if(!sscanf(params,"s[128]",server_amotd)) {
		format(server_amotd, sizeof(server_amotd), "%s",FormatColourString(server_amotd));
		format(msg, sizeof(msg), "* The admin MOTD has been changed to: %s",server_amotd);
		ABroadcast(X11_ORANGE, msg, EAdminFlags_All);
		mysql_real_escape_string(server_amotd,amotd_esc);
		format(query, sizeof(query), "UPDATE `misc` SET `amotd` = \"%s\"",amotd_esc);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid, X11_ORANGE, "Admin MOTD Cleared!");
		server_amotd[0] = 0;
		format(query, sizeof(query), "UPDATE `misc` SET `amotd` = \"\"");
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	}
	return 1;
}
YCMD:amotd(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shows you the admin MOTD");
		return 1;
	}
	sendAdminMOTD(playerid);
	return 1;
}
YCMD:motd(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Shows you the MOTD");
		return 1;
	}
	sendPlayerMOTD(playerid);
	return 1;
}
sendAdminMOTD(playerid) {
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") == EAdminFlags_None) return 0;
	new msg[128];
	if(server_amotd[0] != 0) {
		format(msg, sizeof(msg), "* Admin MOTD: {%s}%s",getColourString(COLOR_RED),server_amotd);
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
	return 0;
}
sendPlayerMOTD(playerid) {
	new msg[128];
	if(server_motd[0] != 0) {
		format(msg, sizeof(msg), "Server MOTD: {%s}%s",getColourString(COLOR_RED),server_motd);
		SendClientMessage(playerid, X11_ORANGE, msg);
	}
	return 0;
}
sendServerInfo(playerid) {
	new msg[128];
	/*
	SendClientMessage(playerid, X11_ORANGE,"---------------");
	format(msg, sizeof(msg), "Welcome to Inglewood Roleplay");
	SendClientMessage(playerid, X11_ORANGE,msg);
	format(msg, sizeof(msg), "Script Version: %s",VERSION);
	SendClientMessage(playerid, X11_ORANGE,msg);
	sendPlayerMOTD(playerid);
	SendClientMessage(playerid, X11_ORANGE,"---------------");
	*/
	sendPlayerMOTD(playerid);
	format(msg, sizeof(msg), "~w~Welcome To ~r~Inglewood Role Play~n~~w~Script Revision: ~r~ %s",VERSION);
	ShowScriptMessage(playerid, msg, 16000);
}
forward OnLoadMOTD();
public OnLoadMOTD() {
	new rows, fields;
	cache_get_data(rows, fields);
	new id_string[32];
	if(rows > 0) {
		cache_get_row(0, 0, server_motd);
		cache_get_row(0, 1, server_amotd);
		cache_get_row(0, 2, id_string);
		server_taxrate = strval(id_string);
		cache_get_row(0, 3, id_string);
		setLottoFunds(strval(id_string));
		cache_get_row(0, 4, id_string);
		server_doublebonus = strval(id_string);
		cache_get_row(0, 5, id_string);
		server_interestrate = strval(id_string);
		cache_get_row(0, 6, id_string);
		server_rptest = strval(id_string);
	}
}
YCMD:setcarhealth(playerid, params[], help) {
	new car, Float:health;
	if(!sscanf(params,"df",car,health)) {
		if(GetVehicleModel(car) == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Vehicle!");
			return 1;
		}
		if(health < 0.0 || health > 1000.0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid HP Value!");
			return 1;
		}
		SetVehicleHealth(car, health);
		SendClientMessage(playerid , COLOR_CUSTOMGOLD, "[NOTICE]: Done!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /setcarhealth [carid] [health]");
	}
	return 1;
}
YCMD:stopstream(playerid, params[], help) {
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	new user = playerid;
	if(aflags & EAdminFlags_BasicAdmin) {
		if(!sscanf(params, "k<playerLookup_acc>", user)) {
			if(!IsPlayerConnectEx(user)) {
				SendClientMessage(playerid, X11_TOMATO_2, "User not found");
				return 1;
			}
		}
	}
	StopAudioStreamForPlayer(user);
	SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Stream Stopped!");
	return 1;
}
YCMD:connectnpc(playerid, params[], help) {
	new name[32], script[32];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Connects an NPC");
		return 1;
	}
	if(!sscanf(params, "s[32]s[32]", name, script)) {
		ConnectNPC(name, script);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "Attempting to connect NPC!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /connectnpc [name] [scriptname]");
	}
	return 1;
}
YCMD:kicknpc(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Kicks an NPC");
		return 1;
	}
	new user;
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnected(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(!IsPlayerNPC(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "This isn't an NPC!");
			return 1;
		}
		Kick(user);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /kicknpc [playerid/name]");
	}
	return 1;
}
YCMD:npcs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all NPCs");
		return 1;
	}
	new name[MAX_PLAYER_NAME],msg[128];
	foreach(Bot, i) {
		GetPlayerName(i, name, sizeof(name));
		format(msg, sizeof(msg), "NPC: %s[%d]",name,i);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
	}
	return 1;
}
	
YCMD:mute(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mutes a player");
		return 1;
	}
	new user, time;
	if(!sscanf(params, "k<playerLookup_acc>D(30)",user, time)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User is not found");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "SYSTEM: %s was muted by %s for %d minutes",GetPlayerNameEx(user, ENameType_CharName), GetPlayerNameEx(playerid, ENameType_AccountName), time);
		SendClientMessageToAll(X11_TOMATO_2,msg);
		new stime = gettime();
		time *= 60; //convert to seconds
		SetPVarInt(user, "MuteTime",stime+time);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /mute [playerid/name] [time]");
	}
	return 1;
}
isMuted(playerid) {
	new mutetime = GetPVarInt(playerid, "MuteTime");
	new time = gettime();
	if(mutetime > time) {
			return 1;
	} else if(mutetime < 0) {
		DeletePVar(playerid, "MuteTime");
	}
	return 0;
}
isNewbMuted(playerid) {
	new mutetime = GetPVarInt(playerid, "NMuteTime");
	new time = gettime();
	if(mutetime > time) {
			return 1;
	} else if(mutetime < 0) {
		DeletePVar(playerid, "NMuteTime");
	}
	return 0;
}
YCMD:nmute(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Mutes a player");
		return 1;
	}
	new user, time;
	if(!sscanf(params, "k<playerLookup_acc>D(30)",user, time)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User is not found");
			return 1;
		}
		new msg[128];
		format(msg, sizeof(msg), "SYSTEM: %s was muted from newb chat by %s for %d minutes",GetPlayerNameEx(user, ENameType_CharName), GetPlayerNameEx(playerid, ENameType_AccountName), time);
		SendGlobalAdminMessage(X11_TOMATO_2,msg);
		new stime = gettime();
		time *= 60; //convert to seconds
		SetPVarInt(user, "NMuteTime",stime+time);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /nmute [playerid/name] [time]");
	}
	return 1;
}
YCMD:reportban(playerid, params[], help) {
	new user;
	if(!sscanf(params, "k<playerLookup_acc>", user)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		new banned = GetPVarInt(user, "ReportBanned");
		new msg[128];
		if(banned == 0) {
			format(msg, sizeof(msg), "SYSTEM: %s was report banned by %s",GetPlayerNameEx(user, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName));
			SetPVarInt(user, "ReportBanned", 1);
		} else {
			format(msg, sizeof(msg), "SYSTEM: %s was unreport banned by %s",GetPlayerNameEx(user, ENameType_CharName),GetPlayerNameEx(playerid, ENameType_AccountName));
			SetPVarInt(user, "ReportBanned", 0);
		}
		ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /reportban [playerid/name]");
	}
	return 1;
}
numUsersOnIP(playerid) {
	new ip[16],ip2[16];
	new num;
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	foreach(Player, i) {
		GetPlayerIpEx(i, ip2, sizeof(ip2));
		if(strcmp(ip,ip2) == 0) {
			num++;
		}
	}
	return num;
}
YCMD:needinghelp(playerid, params[], help) {
	new msg[128];
	if(GetPVarInt(playerid, "NewbieRank") == 0 ){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a helper!");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "** Players who need help **");
	foreach(Player, i) {
		if(GetPVarInt(i, "NeedHelp") == 1) {
			format(msg, sizeof(msg), "* %s[%d]",GetPlayerNameEx(i, ENameType_RPName),i);
			SendClientMessage(playerid, X11_YELLOW, msg);
		}
	}
	return 1;
}
YCMD:rulebreakers(playerid, params[], help) {
	new numkicks, numbans, numajails;
	new msg[128];
	new maxbans, maxkicks, maxjails;
	
	sscanf(params, "D(1000)D(0)D(0)", maxkicks,maxjails,maxbans);
	
	foreach(Player, i) {
		numkicks = GetPVarInt(i, "NumKicks");
		numbans = GetPVarInt(i, "NumBans");
		numajails = GetPVarInt(i, "NumAJails");
		if(numajails > maxjails || numbans > maxbans || numkicks > maxkicks) {
			format(msg, sizeof(msg), "* %s [%d] Kicked %d times | Jailed %d times | Banned %d times",GetPlayerNameEx(i, ENameType_RPName),i, numkicks, numajails, numbans);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
		}
	}
	return 1;
}
YCMD:requesthelp(playerid, params[], help) {
	if(isNewbMuted(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are muted from this!");
		return 1;
	}
	new timenow = gettime();
	if(GetPVarInt(playerid, "NewbieRank") == 0 && GetPVarInt(playerid, "ConnectTime") > 50) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be a newbie");
		return 1;
	}
	if(GetPVarInt(playerid, "NewbieRank") == 0) {
		new time = GetPVarInt(playerid, "NewbieCooldown");
		if(REQUESTCHAT_COOLDOWN-(timenow-time) > 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must wait before using this again!");
			return 1;
		}
		SetPVarInt(playerid, "NewbieCooldown", gettime());
	}
	SetPVarInt(playerid, "NeedHelp", 1);
	new msg[128];
	format(msg, sizeof(msg), "* %s is needing help (/gohelp)",GetPlayerNameEx(playerid, ENameType_RPName_NoMask));
	foreach(Player, i) {
		if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
			continue;
		}
		SendClientMessage(i, COLOR_CUSTOMGOLD, msg);
	}
	SendClientMessage(playerid, X11_YELLOW, "Your request has been sent to the helpers!");
	return 1;
}
YCMD:gohelp(playerid, params[], help) {
	new Float:X, Float:Y, Float:Z, interior, vw;
	new target;
	if(GetPVarInt(playerid, "NewbieRank") == 0 ){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a helper!");
		return 1;
	}
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(GetPVarType(target, "NeedHelp") == PLAYER_VARTYPE_NONE) {
			SendClientMessage(playerid, X11_TOMATO_2, "This user doesn't need help");
			return 1;
		}
		GetPlayerPos(playerid, X, Y, Z);
		vw = GetPlayerVirtualWorld(playerid);
		interior = GetPlayerInterior(playerid);
		SetPVarFloat(playerid, "HelpX", X);
		SetPVarFloat(playerid, "HelpY", Y);
		SetPVarFloat(playerid, "HelpZ", Z);
		SetPVarInt(playerid, "HelpInt", interior);
		SetPVarInt(playerid, "HelpVW", vw);
		GetPlayerPos(target, X, Y, Z);
		vw = GetPlayerVirtualWorld(target);
		interior = GetPlayerInterior(target);
		if(IsPlayerInAnyVehicle(playerid)) {
			new carid = GetPlayerVehicleID(playerid);
			TPEntireCar(carid, interior, vw);
			LinkVehicleToInterior(carid, interior);
			SetVehicleVirtualWorld(carid, vw);
			SetVehiclePos(carid, X, Y, Z);
			
		} else {
			SetPlayerPos(playerid, X, Y, Z);
		}
		new msg[128];
		format(msg, sizeof(msg), "%s has accepted the help request of %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		foreach(Player, i) {
			if(GetPVarInt(i, "NewbieRank") == 0 && EAdminFlags:GetPVarInt(i, "AdminLevel") == EAdminFlags_None) {
				continue;
			}
			SendClientMessage(i, X11_YELLOW, msg);
		}
		SetPlayerVirtualWorld(playerid, vw);
		SetPlayerInterior(playerid,interior);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
		DeletePVar(target, "NeedHelp");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gohelp [playerid/name]");
	}
	return 1;
}
YCMD:goback(playerid, params[], help) {
	if(GetPVarInt(playerid, "NewbieRank") == 0 ){
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't a helper!");
		return 1;
	}
	if(GetPVarType(playerid, "HelpX") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have no where to go back to!");
		return 1;
	}
	new Float:X, Float:Y, Float:Z, interior, vw;
	vw = GetPVarInt(playerid, "HelpVW");
	interior = GetPVarInt(playerid, "HelpInt");
	X = GetPVarFloat(playerid, "HelpX");
	Y = GetPVarFloat(playerid, "HelpY");
	Z = GetPVarFloat(playerid, "HelpZ");
	if(IsPlayerInAnyVehicle(playerid)) {
		new carid = GetPlayerVehicleID(playerid);
		TPEntireCar(carid, interior, vw);
		LinkVehicleToInterior(carid, interior);
		SetVehicleVirtualWorld(carid, vw);
		SetVehiclePos(carid, X, Y, Z);
		
	} else {
		SetPlayerPos(playerid, X, Y, Z);
	}
	SetPlayerVirtualWorld(playerid, vw);
	SetPlayerInterior(playerid,interior);
	SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	DeletePVar(playerid, "HelpVW");
	DeletePVar(playerid, "HelpInt");
	DeletePVar(playerid, "HelpX");
	DeletePVar(playerid, "HelpY");
	DeletePVar(playerid, "HelpZ");
	return 1;
}
YCMD:pointers(playerid, params[], help) {
	new msg[128];
	new codptr,datptr,heapptr,stpptr,stackptr,frameptr,codeptr;
	#emit LCTRL 0
	#emit STOR.S.pri codptr
	#emit LCTRL 1
	#emit STOR.S.pri datptr
	#emit LCTRL 2
	#emit STOR.S.pri heapptr
	#emit LCTRL 3
	#emit STOR.S.pri stpptr
	#emit LCTRL 4
	#emit STOR.S.pri stackptr
	#emit LCTRL 5
	#emit STOR.S.pri frameptr
	#emit LCTRL 6
	#emit STOR.S.pri codeptr
	format(msg, sizeof(msg), "COD: %x DAT %x HEA: %x STP: %x STACK: %x FRAME: %x CIP: %x",codptr,datptr,heapptr,stpptr,stackptr,frameptr,codeptr);
	SendClientMessage(playerid, X11_WHITE, msg);
	return 1;
}
YCMD:helpers(playerid, params[], help) {
	new msg[128];
	foreach(Player, i) {
		if(GetPVarInt(i, "NewbieRank") != 0 && ~EAdminFlags:GetPVarInt(i, "AdminFlags") & EAdminFlags_Invisible) {
			format(msg, sizeof(msg), "<%s> %s[%d]",GetNewbieName(i), GetPlayerNameEx(i, ENameType_CharName), i);
			SendClientMessage(playerid, X11_YELLOW, msg);
		}
	}
	return 1;
}
YCMD:clearchat(playerid, params[], help) {
	for(new i=0;i<50;i++) {
		SendClientMessage(playerid, X11_WHITE, " ");
	}
	return 1;
}
YCMD:clearallchats(playerid, params[], help) {
	foreach(Player, i) {
		for(new x=0;x<50;x++) {
			SendClientMessage(playerid, X11_WHITE, " ");
		}		
	}
	return 1;
}
YCMD:ahide(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Hide yourself from the admin list");
		return 1;
	}
	new msg[128];
	if(GetPVarInt(playerid, "AdminHidden") == 1) {
		format(msg, sizeof(msg), "* %s is no longer hidden on the admin list",GetPlayerNameEx(playerid, ENameType_AccountName));
		DeletePVar(playerid, "AdminHidden");
	} else {
		format(msg, sizeof(msg), "* %s is now hidden on the admin list",GetPlayerNameEx(playerid, ENameType_AccountName));
		SetPVarInt(playerid, "AdminHidden", 1);
	}
	ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
	return 1;	
}
YCMD:deathmsg(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sends a death message to the server");
		return 1;
	}
	new killer, victim, reason;
	if(!sscanf(params, "k<playerLookup_acc>dU(-1)", victim, reason, killer)) {
		if(!IsPlayerConnectEx(killer)) {
			killer = INVALID_PLAYER_ID;
		}
		SendDeathMessage(killer, victim, reason);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /deathmsg [victim] [reason] (killer)");
	}
	return 1;
}
YCMD:guninfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "List weapons by IDs");
		return 1;
	}
	dialogstr[0] = 0;
	for(new i=1;i<46;i++) {
		if(GetWeaponSlot(i) != -1) {
			format(tempstr, sizeof(tempstr), "%d. %s\n",i,GunName[i]);
			strcat(dialogstr,tempstr,sizeof(dialogstr));
		}
	}
	ShowPlayerDialog(playerid, 0, DIALOG_STYLE_MSGBOX, "{00BFFF}Weapons", dialogstr, "Ok", "");
	return 1;
}
YCMD:docommand(playerid, params[], help) {
	new target, cmd[128];
	if(!sscanf(params, "k<playerLookup_acc>s[128]",target, cmd)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		Command_ReProcess(target, cmd, false);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /docommand [playerid/name] [cmd]");
	}
	return 1;
}
YCMD:nodoublebonus(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles Double Bonus On/Off for the server");
		return 1;
	}
	if(server_doublebonus == 0) {
		SendClientMessageToAll(X11_ORANGE, "   Double Bonus has been enabled by an Admin !");
		updateDoubleBonus(1);
	} else {
		SendClientMessageToAll(X11_ORANGE, "   Double Bonus has been disabled by an Admin !");
		updateDoubleBonus(0);
	}
	return 1;
}
updateDoubleBonus(boolval) {
	server_doublebonus = boolval;
	format(query, sizeof(query), "UPDATE `misc` SET `doublebonus` = %d",server_doublebonus);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
YCMD:norptest(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Toggles the RP test On/Off for the server");
		return 1;
	}
	if(server_rptest == 0) {
		SendClientMessageToAll(X11_ORANGE, "   The RP test has been enabled by an Admin !");
		updateServerRPTest(1);
	} else {
		SendClientMessageToAll(X11_ORANGE, "   The RP test has been disabled by an Admin !");
		updateServerRPTest(0);
	}
	return 1;
}
updateServerRPTest(boolval) {
	server_rptest = boolval;
	format(query, sizeof(query), "UPDATE `misc` SET `rptest` = %d",server_rptest);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
decideBan(playerid) {
	new msg[128];
	new shouldBan;
	new ajails = getPlayerNumAjails(playerid);
	shouldBan = floatfract(floatdiv(ajails,MAX_AJAILS_BEFORE_BAN))==0.0&&ajails>0;
	if(shouldBan) {
		if(GetPVarInt(playerid, "ConnectTime") > 100) {
			new total_over = ajails/MAX_AJAILS_BEFORE_BAN;
			new numjails = MAX_AJAILS_BEFORE_BAN*total_over;
			new bantime = AJAIL_MAX_BANTIME*total_over;
			format(msg, sizeof(msg), "SYSTEM: %s has been banned for exceeding %d admin jails - %d day ban",GetPlayerNameEx(playerid, ENameType_CharName),numjails,total_over);
			ABroadcast(X11_TOMATO_2, msg, EAdminFlags_All);
			format(msg, sizeof(msg), "More than %d accumulated admin jails.", numjails);
			BanPlayer(playerid, msg, -1, false, bantime);
			return 1;
		}		
	}
	return 0;
}
getPlayerNumAjails(playerid) {
	new ajails = GetPVarInt(playerid, "NumAJails");
	return ajails;
}
incPlayerBans(playerid) {
	format(query, sizeof(query), "UPDATE `accounts` SET `numbans` = `numbans`+1 WHERE `id` = %d",GetPVarInt(playerid, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SetPVarInt(playerid, "NumBans", GetPVarInt(playerid, "NumBans")+1);
}
incPlayerKicks(playerid) {
	format(query, sizeof(query), "UPDATE `accounts` SET `numkicks` = `numkicks`+1 WHERE `id` = %d",GetPVarInt(playerid, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SetPVarInt(playerid, "NumKicks", GetPVarInt(playerid, "NumKicks")+1);
}
incPlayerAJails(playerid) {
	format(query, sizeof(query), "UPDATE `accounts` SET `numajails` = `numajails`+1 WHERE `id` = %d",GetPVarInt(playerid, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SetPVarInt(playerid, "NumAJails", GetPVarInt(playerid, "NumAJails")+1);
}

decPlayerAJails(playerid) {
	format(query, sizeof(query), "UPDATE `accounts` SET `numajails` = `numajails`-1 WHERE `id` = %d",GetPVarInt(playerid, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	SetPVarInt(playerid, "NumAJails", GetPVarInt(playerid, "NumAJails")-1);
}
sendPlayerNotes(playerid, targetid, limit = 50, desc = 0) {
	new msg[32];
	format(query, sizeof(query), "SELECT `noteid`,`setter`.`username`,`seton`,`note` FROM `accountnotes` INNER JOIN `accounts` AS `setter` ON `setter`.`id` = `accountnotes`.`setby` WHERE `accountid` = %d",GetPVarInt(targetid, "AccountID"));
	
	strcat(query, (!desc) ? (" ORDER BY `noteid` ASC ") : (" ORDER BY `noteid` DESC ")); //Descending or ascending?
	format(msg, sizeof(msg), "LIMIT %d", limit);
	strcat(query, msg);
	mysql_function_query(g_mysql_handle, query, true, "OnRecieveNotes", "dd",playerid,targetid);
}
YCMD:notes(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists a players notes");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		sendPlayerNotes(playerid, target);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /notes [playerid/name]");
	}
	return 1;
}
YCMD:addnote(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Add an account note");
		return 1;
	}
	new note[(128*2)+1],playa;
	if(!sscanf(params, "k<playerLookup_acc>s[128]", playa, note)) {
		if(!IsPlayerConnectEx(playa)) {
			SendClientMessage(playerid, X11_TOMATO_2, "Player not found!");
			return 1;
		}
		mysql_real_escape_string(note, note);
		format(query, sizeof(query), "INSERT INTO `accountnotes` SET `note` = \"%s\",`accountid` = %d,`setby` = %d",note,GetPVarInt(playa, "AccountID"), GetPVarInt(playerid, "AccountID"));
		mysql_function_query(g_mysql_handle, query, true, "OnAddNote", "dd",playerid, playa);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /addnote [playerid/name] [note]");
	}
	return 1;
}
AddAccountNote(target, setter, reason[]) {
	new treason[(128*2)+1];
	mysql_real_escape_string(reason, treason);
	new setbyid = 0;
	if(setter != INVALID_PLAYER_ID && setter != -1) {
		setbyid = GetPVarInt(setter, "AccountID");
	}
	format(query, sizeof(query), "INSERT INTO `accountnotes` SET `note` = \"%s\",`accountid` = %d,`setby` = %d",treason,GetPVarInt(target, "AccountID"), setbyid);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
}
YCMD:delnote(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Delete an account note");
		return 1;
	}
	new noteid;
	if(!sscanf(params, "d", noteid)) {
		format(query, sizeof(query), "DELETE FROM `accountnotes` WHERE `noteid` = %d",noteid);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /delnote [noteid]");
	}
	return 1;
}
forward OnDeleteNote(playerid, noteid);
public OnDeleteNote(playerid, noteid) {
	format(query, sizeof(query),"* %d notes deleted",mysql_affected_rows());
	SendClientMessage(playerid, X11_YELLOW_2, query);
	return 1;
}
forward OnAddNote(playerid, targetid);
public OnAddNote(playerid, targetid) {
	new id = mysql_insert_id();
	format(query, sizeof(query), "* Note added as id %d",id);
	SendClientMessage(playerid, X11_YELLOW_2, query);
	return 0;
}
forward OnRecieveNotes(playerid, targetid);
public OnRecieveNotes(playerid, targetid) {
	new rows,fields;
	cache_get_data(rows,fields);
	format(query, sizeof(query), "**** Account Notes for: %s ****",GetPlayerNameEx(targetid, ENameType_AccountName));
	SendClientMessage(playerid, X11_WHITE, query);
	if(rows < 1) {
		SendClientMessage(playerid, X11_YELLOW, "* No Notes found");
		return 1;
	}
	new noteid, setby[MAX_PLAYER_NAME+1],seton[32],note[128];
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,query);
		noteid = strval(query);
		cache_get_row(i,1,setby);
		cache_get_row(i,2,seton);
		cache_get_row(i,3,note);
		format(query, sizeof(query), "* ID: %d Set By: %s Set On: %s Note: %s",noteid,setby,seton,note);
		SendClientMessage(playerid, X11_TOMATO_2, query);
	}
	return 0;
}
YCMD:adban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Bans/Unbans someone from making advertisements");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(target, "AccountFlags");
		if(aflags & EAccountFlags_AdBanned) {
			aflags &= ~EAccountFlags_AdBanned;
			format(query, sizeof(query), "[AMSG]: %s has UnAdBanned %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		} else {
			aflags |= EAccountFlags_AdBanned;
			format(query, sizeof(query), "[AMSG]: %s has AdBanned %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		}
		SetPVarInt(target, "AccountFlags", _:aflags);
		ABroadcast(X11_TOMATO_2, query, EAdminFlags_BasicAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /adban [playerid/name]");
	}
	return 1;
}
YCMD:furnitureban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Bans/Unbans someone from placing furniture");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(target, "AccountFlags");
		if(aflags & EAccountFlags_FurnitureBanned) {
			aflags &= ~EAccountFlags_FurnitureBanned;
			format(query, sizeof(query), "[AMSG]: %s has UnFurniture Banned %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		} else {
			aflags |= EAccountFlags_FurnitureBanned;
			format(query, sizeof(query), "[AMSG]: %s has Furniture Banned %s",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		}
		SetPVarInt(target, "AccountFlags", _:aflags);
		ABroadcast(X11_TOMATO_2, query, EAdminFlags_BasicAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /furnitureban [playerid/name]");
	}
	return 1;
}
YCMD:givebikeperms(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives or Removes bike permissions to those who want to use the regular bicycles");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(target, "AccountFlags");
		if(aflags & EAccountFlags_IsBikeRestricted) {
			aflags &= ~EAccountFlags_IsBikeRestricted;
			format(query, sizeof(query), "[AMSG]: %s has given Bike Permissions to %s.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		} else {
			aflags |= EAccountFlags_IsBikeRestricted;
			format(query, sizeof(query), "[AMSG]: %s has taken %s's Bike Permissions.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		}
		SetPVarInt(target, "AccountFlags", _:aflags);
		ABroadcast(X11_TOMATO_2, query, EAdminFlags_BasicAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /givebikeperms [playerid/name]");
	}
	return 1;
}
YCMD:givemaskperms(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives or Removes mask permissions to those who want to use the masks from the 24/7's");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new EAccountFlags:aflags = EAccountFlags:GetPVarInt(target, "AccountFlags");
		if(aflags & EAccountFlags_IsMaskRestricted) {
			aflags &= ~EAccountFlags_IsMaskRestricted;
			format(query, sizeof(query), "[AMSG]: %s has given Mask Permissions to %s.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
		} else {
			aflags |= EAccountFlags_IsMaskRestricted;
			format(query, sizeof(query), "[AMSG]: %s has taken %s's Mask Permissions.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(target, ENameType_AccountName));
			SetPlayerMask(target, 0);
		}
		SetPVarInt(target, "AccountFlags", _:aflags);
		ABroadcast(X11_TOMATO_2, query, EAdminFlags_BasicAdmin);
	} else {
		SendClientMessage(playerid, X11_WHITE, "Usage: /givemaskperms [playerid/name]");
	}
	return 1;
}
YCMD:wcheck(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Check how much a player would be fined with /wfine");
		return 1;
	}
	new user, Float:percentage;
	if(!sscanf(params, "k<playerLookup_acc>f", user,percentage)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new money =  floatround(getTotalWealth(user)*(percentage/100));
		format(query, sizeof(query), "* Wealth: $%s",getNumberString(money));
		SendClientMessage(playerid, X11_YELLOW, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /wcheck [playerid/name] [percentage]");
	}
	return 1;
}
YCMD:wfine(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Fine a player a percentage of their total wealth");
		return 1;
	}
	new user, Float:percentage, reason[128];
	if(!sscanf(params, "k<playerLookup_acc>fs[128]", user,percentage,reason)) {
		if(!IsPlayerConnectEx(user)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found!");
			return 1;
		}
		new money =  floatround(getTotalWealth(user)*(percentage/100));
		new string[128];
		format(string, sizeof(string), "* You gave %s a fine costing $%s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName), getNumberString(money), reason);
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, string);
		format(string, sizeof(string), "* Admin %s has given you a fine costing $%s, Reason: %s", GetPlayerNameEx(playerid, ENameType_AccountName), getNumberString(money), reason);
		SendClientMessage(user, X11_ORANGE, string);
		format(string, sizeof(string), "System: %s was fined (%.02f Percent of %s wealth - $%s) by %s, Reason: %s", GetPlayerNameEx(user, ENameType_CharName),percentage, getPossiveAdjective(user),getNumberString(money), GetPlayerNameEx(playerid, ENameType_AccountName),reason);
		SendGlobalAdminMessage(COLOR_LIGHTRED, string);
		GiveMoneyEx(user, -money);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /wfine [playerid/name] [percentage] [reason]");
	}
	return 1;
}
YCMD:checkspectating(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "List admins who are spectating");
		return 1;
	}
	SendClientMessage(playerid, X11_WHITE, "***** Admins Spectating *****");
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "AdminHidden") == 2) continue;
			new spectarget = -1;
			if(GetPVarType(i, "SpecPlayer") != PLAYER_VARTYPE_NONE) {
				spectarget = GetPVarInt(i, "SpecPlayer");
				format(query, sizeof(query), "* %s: %s[%d]",GetPlayerNameEx(i, ENameType_AccountName),GetPlayerNameEx(spectarget, ENameType_CharName), spectarget);
			} else if(GetPVarType(i, "SpecVeh") != PLAYER_VARTYPE_NONE) {
				spectarget = GetPVarInt(i, "SpecVeh");
				format(query, sizeof(query), "* %s: Car: [%d]",GetPlayerNameEx(i, ENameType_AccountName), spectarget);
			}
			if(spectarget != -1) {
				SendClientMessage(playerid, X11_YELLOW, query);
			}
		}
	}
	return 1;
}
YCMD:bigfamilyears(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to see all family chats");
		return 1;
	}
	if(GetPVarInt(playerid, "BigFamilyEars") == 1) {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Family Ears Off");
		DeletePVar(playerid, "BigFamilyEars");
	} else {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Family Ears On");
		SetPVarInt(playerid, "BigFamilyEars", 1);
	}
	return 1;
}
YCMD:bigfactionears(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to see all faction chats");
		return 1;
	}
	if(GetPVarInt(playerid, "BigFactionEars") == 1) {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Faction Ears Off");
		DeletePVar(playerid, "BigFactionEars");
	} else {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Faction Ears On");
		SetPVarInt(playerid, "BigFactionEars", 1);
	}
	return 1;
}
YCMD:bigears(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows a player to see all chats");
		return 1;
	}
	if(GetPVarInt(playerid, "BigEars") == 1) {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Ears Off");
		DeletePVar(playerid, "BigEars");
	} else {
		SendClientMessage(playerid, X11_YELLOW_2, "* Big Ears On");
		SetPVarInt(playerid, "BigEars", 1);
	}
	return 1;
}
YCMD:rangebans(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_TOMATO_2, "List Range Bans");
		return 1;
	}
	mysql_function_query(g_mysql_handle, "SELECT `r`.`id`,`reason`,`ac`.`username`,`seton`,INET_NTOA(`host`),INET_NTOA(`mask`) FROM `rangebans` AS `r` LEFT JOIN `accounts` AS `ac` ON `ac`.`id` = `setby`", true, "OnListRangeBans", "d",playerid);
	return 1;
}
forward OnListRangeBans(playerid);
public OnListRangeBans(playerid) {
	new reason[128],banner[MAX_PLAYER_NAME],timestr[64],host[17],mask[17],id_str[12];
	new msg[128];
	new rows, fields;
	cache_get_data(rows, fields);
	SendClientMessage(playerid, X11_WHITE, "* Range Bans");
	for(new i=0;i<rows;i++) {
		cache_get_row(i,0,id_str);
		cache_get_row(i,1,reason);
		cache_get_row(i,2, banner);
		cache_get_row(i,3, timestr);
		cache_get_row(i,4, host);
		cache_get_row(i,5, mask);
		if(strlen(banner) < 1 || !strcmp(banner,"NULL")) {
			strmid(banner, "SYSTEM", 0, 6, sizeof(banner));
		}
		format(msg, sizeof(msg), "* ID: %d Set On: %s Set By: %s Host: %s Mask: %s Reason: %s",strval(id_str),timestr,banner,host,mask,reason);
		SendClientMessage(playerid, X11_YELLOW, msg);
	}
	return 1;
}
YCMD:delrangeban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Deletes a range ban");
		return 1;
	}
	new id;
	if(!sscanf(params, "d", id)) {
		format(query, sizeof(query), "DELETE FROM `rangebans` WHERE `id` = %d",id);
		mysql_function_query(g_mysql_handle, query, true, "OnDeleteRangeBan", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /delrangeban [sqlid]");
	}
	return 1;
}
YCMD:addrangeban(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Adds a range ban");
		return 1;
	}
	new host[(17*2)+1],mask[(17*2)+1],reason[64];
	if(!sscanf(params, "s[16]s[16]s[64]", host, mask, reason)) {
		mysql_real_escape_string(host, host);
		mysql_real_escape_string(mask, mask);
		mysql_real_escape_string(reason, reason);
		format(query, sizeof(query), "INSERT INTO `rangebans` SET `setby` = %d,`reason` = \"%s\",`mask` = INET_ATON(\"%s\"),`host` = INET_ATON(\"%s\")",GetPVarInt(playerid, "AccountID"), reason, mask, host);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
		SendClientMessage(playerid, X11_YELLOW, "* Range Ban Added!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /addrangeban [host] [mask] [reason]");
	}
	return 1;
}
forward OnDeleteRangeBan(playerid);
public OnDeleteRangeBan(playerid) {
	new numrows = mysql_affected_rows();
	if(numrows > 0) {
		SendClientMessage(playerid, X11_YELLOW, "* Range Ban Deleted");
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* Range Ban Not found!");
	}
	return 1;
}
forward ShowSetSexMenu(playerid,targetid);
public ShowSetSexMenu(playerid,targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	SetPVarInt(playerid, "SetSexTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetSex, DIALOG_STYLE_LIST, "Set Sex","Male\nFemale\nTransvestite","Set", "Cancel");
}
forward ShowSetDonateRank(playerid,targetid);
public ShowSetDonateRank(playerid,targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	SetPVarInt(playerid, "SetDonateRankTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetDonateRank, DIALOG_STYLE_LIST, "Set Donate Rank","None\nBeta Tester\nVIP\nPopulator\nTrusted Donator\nBronze\nSilver\nGold","Set", "Cancel");
}
forward ShowSetFightStyleMenu(playerid, targetid);
public ShowSetFightStyleMenu(playerid, targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	SetPVarInt(playerid, "SetFightStyleTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetFightStyle, DIALOG_STYLE_LIST, "Set Fight Style","Normal\nBoxing\nKung Fu\nKnee-Head\nGrab Kick\nElbow","Set", "Cancel");
}
forward setTest(playerid, type);
public setTest(playerid, type) {
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(playerid, "AccountFlags");
	if(type == 0) {
		aflags &= ~EAccountFlags_MustRedoTest;
		destroyRPTest3dTextLabel(playerid);
	} else {
		aflags |= EAccountFlags_MustRedoTest;
		ShowQuestion(playerid, 0);
	}
	SetPVarInt(playerid, "AccountFlags", _:aflags);
}
forward setNumKicksCall(playerid, targetid);
public setNumKicksCall(playerid, targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new msg[128];
	format(msg, sizeof(msg), "%s has: %d %s", GetPlayerNameEx(targetid, ENameType_AccountName), GetPVarInt(targetid, "NumKicks"), getAJBansKickPropName(EAdmin_SettingNumKicks));
	SetPVarInt(playerid, "NumPropSetting", EAdmin_SettingNumKicks);
	SetPVarInt(playerid, "SetNumPropTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetKicksAJBans, DIALOG_STYLE_INPUT, "Set Number Of Admin Kicks",msg,"Set","Cancel");
}
forward setNumBansCall(playerid, targetid);
public setNumBansCall(playerid, targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new msg[128];
	format(msg, sizeof(msg), "%s has: %d %s", GetPlayerNameEx(targetid, ENameType_AccountName), GetPVarInt(targetid, "NumBans"), getAJBansKickPropName(EAdmin_SettingNumBans));
	SetPVarInt(playerid, "NumPropSetting", EAdmin_SettingNumBans);
	SetPVarInt(playerid, "SetNumPropTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetKicksAJBans, DIALOG_STYLE_INPUT, "Set Number Of Admin Bans",msg,"Set","Cancel");
}
forward setNumAJailsCall(playerid, targetid);
public setNumAJailsCall(playerid, targetid) {
	tempstr[0] = 0;
	dialogstr[0] = 0;
	new msg[128];
	format(msg, sizeof(msg), "%s has: %d %s", GetPlayerNameEx(targetid, ENameType_AccountName), GetPVarInt(targetid, "NumAJails"), getAJBansKickPropName(EAdmin_SettingNumAJails));
	SetPVarInt(playerid, "NumPropSetting", EAdmin_SettingNumAJails);
	SetPVarInt(playerid, "SetNumPropTarget", targetid);
	ShowPlayerDialog(playerid, EAdminDialog_SetKicksAJBans, DIALOG_STYLE_INPUT, "Set Number Of Admin Jails",msg,"Set","Cancel");
}
YCMD:disablegames(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Disables/enables point/turf wars");
		return 1;
	}
	if(gamesoff == 1) {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Games Enabled");
		gamesoff = 0;
	} else {
		SendClientMessage(playerid, COLOR_CUSTOMGOLD, "[NOTICE]: Games Disabled");
		gamesoff = 1;
	}
	return 1;
}

YCMD:checkpaused(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "List all paused/afk players");
		return 1;
	}
	new msg[128];
	new pausetime;
	SendClientMessage(playerid, X11_WHITE, "*** Paused Players ***");
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && (pausetime = IsPlayerPaused(i)) != 0) {
			format(msg, sizeof(msg), "* %s[%d] - Paused %d seconds",GetPlayerNameEx(i, ENameType_CharName),i,pausetime);
			SendClientMessage(playerid, X11_TOMATO_2, msg);
		}
	}
	return 1;
}
CanDoAdminCommand(playerid) {
	if(~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_AdminManage) { //if not admin manage
		if(GetPVarType(playerid, "AdminDuty") == PLAYER_VARTYPE_NONE && GetPVarType(playerid, "AdminHidden") == PLAYER_VARTYPE_NONE) //if not on duty/adminhidden
			return 0;
	}
	return 1;
}
YCMD:cmdsnoop(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to spy on a players commands");
		return 1;
	}
	if(GetPVarType(playerid, "CmdSnoop") != PLAYER_VARTYPE_NONE) {
		DeletePVar(playerid, "CmdSnoop");
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "CMD Snooping stopped");
		return 1;
	}
	new target;
	if(!sscanf(params, "k<playerLookup_acc>", target)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		
		SetPVarInt(playerid, "CmdSnoop", target);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, "CMD Snooping started");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /cmdsnoop [playerid/name]");
	}
	return 1;
}
SendCmdToSnoopers(playerid, cmdtext[]) {
	new msg[128];
	foreach(Player, i) {
		if(GetPVarType(i, "CmdSnoop") != PLAYER_VARTYPE_NONE && GetPVarInt(i, "CmdSnoop") == playerid) {
			format(msg, sizeof(msg), "* Cmd: %s[%d]: ",GetPlayerNameEx(playerid, ENameType_CharName), playerid);
			SendClientMessage(i, X11_PURPLE, msg);
			SendClientMessage(i, X11_PURPLE, cmdtext);
		}
	}
}
IsAdminCmd(cmd[]) {
	new skipcmds[][64] = {"admin","a","adminduty","aduty","kick","ban","tempban","pban","ajail","unjail","unprison","prison","ahide",
	"adminhide","sban","skick", "reports", "acceptreport", "denyreport", "ar", "dr", "invalidreport", "ir","spec",
	"spectate","check","checkguns","checkmasked","slap","fine","checkpaused","checkprison","checklocker","checktrunk",
	"holdinggun","connectionmessages","deathmessages","bigears","bigfamilyears","bigfactionears","freeze","unfreeze","carowner","walltaginfo","factions","oban","ahelp","accountname","notes","listbans","baninfo"};
	for(new x=0;x<sizeof(skipcmds);x++) {
		if(!strcmp(cmd,skipcmds[x], true)) {
			return 0;
		}
	}
	for(new i=0;i<sizeof(admincmds);i++) {
		if(!strcmp(cmd,admincmds[i][ECmdName],true)) {
			if(admincmds[i][ECmdNewbLevel] == 0 && admincmds[i][ECmdMinConnectTime] == 0  && admincmds[i][ECmdMaxConnectTime] == 0  && admincmds[i][ECmdMinLevel] == 0)
				return 1;
		}
	}
	return 0;
}
adminOnCmdRecieved(playerid, cmdtext[]) {
	SendCmdToSnoopers(playerid, cmdtext);
	
	if(GetPVarInt(playerid, "AdminFlags") == 0) return 1;
	new idx = strfind(cmdtext," ", true);
	if(idx == -1)
		idx = strlen(cmdtext);
	new cmdname[64];
	strmid(cmdname, cmdtext, 1, idx, sizeof(cmdname));
	cmdtext[idx] = 0;
	if(IsAdminCmd(cmdname)) {
		if(!CanDoAdminCommand(playerid)) {
			return 0;
		}
	}
	return 1;
}
NumHelpersOnline() {
	new num = 0;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			if(GetPVarInt(i, "NewbieRank") != 0) {
				if(!IsPlayerPaused(i)) { //Only count the helpers that aren't AFK
					num++;
				}
			}
		}
	}
	return num;
}
NumAdminsOnline() {
	new num = 0;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			new EAdminFlags:flags = EAdminFlags:GetPVarInt(i, "AdminFlags");
			if(flags != EAdminFlags_None) {
				if(GetPVarInt(i, "AdminHidden") == 0 && ~flags & EAdminFlags_Invisible) {
					num++;
				}
			}
		}
	}
	return num;
}