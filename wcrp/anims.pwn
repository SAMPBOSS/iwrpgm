//Animation stuff
new gCurrentAnim[MAX_PLAYERS];

#if !defined SPECIAL_ACTION_PISSING
#define SPECIAL_ACTION_PISSING 68
#endif
enum EAnimTypes {
	EAnimType_Regular,
	EAnimType_SubAnims, //there are sub aniamtions to perform
	EAnimType_SpecialAction,
}

enum EAnimInfo {
	EAnimName[32],
	EAnimPerformOther, //can you perform this on another player, 0 = no one, 1 = optional, 2 = must be someone
	EAnimDescription[64],
	EAnimExtra, //extra info, such as pissing, or AnimSequence ID
	EAnimTypes:EAnimType, //special action, or regular animation
	EAnimOppositeAnim, //the anim the other player does if you try perform it on them
};

enum ESubAnimInfo {
	ESubAnimName[32],
	ESubAnimAnimID, //index of Animations array it relates to
	EAnimTypes:ESubAnimType,
	ESubAnimExtra,
	ESubAnimDescription[64],
	ESubAnimPerformOther, //can you perform this on another player, 0 = no one, 1 = optional, 2 = must be someone
	ESubAnimOppositeAnim,//sequence ID of opposite anim
};
enum EAnimSequenceInfo {
	EAnimSeqLib[32],
	EAnimSeqName[32],
	Float:EAnimSeqfDelta,
	EAnimSeqLoop,
	EAnimSeqLockX,
	EAnimSeqLockY,
	EAnimSeqFreeze,
	EAnimSeqTime,
	EAnimSeqForceSync,
	EAnimSpecialAction, //0 if no special action
	EAnimNextAnim, //the following animation
};

new Animations[][EAnimInfo] = {
	{"blowjob",1,"Perform a blowjob on someone",1,EAnimType_Regular,-1}, //0
	{"piss",0,"Perform a piss",0,EAnimType_Regular,-1}, //1
	{"dance",0,"Perform various dance moves", 0, EAnimType_SubAnims,-1}, //2
	{"dead",0,"Fall down on the ground",4,EAnimType_Regular,-1}, //3
	{"wallshoot",0, "Wall shooting", 0, EAnimType_SubAnims,-1}, //4
	{"ghands",0,"Gang Hands", 0, EAnimType_SubAnims,-1}, //5
	{"shadowbox",1,"Injured animation",19,EAnimType_Regular,-1}, //6
	{"injured",0,"Injured animation",20,EAnimType_Regular,-1}, //7
	{"wank",0,"Wank off",21,EAnimType_Regular,-1}, //8
	{"sleep",0,"Perform a sleeping animation",22,EAnimType_Regular,-1}, //9
	{"point",0,"Perform a pointing animation",23,EAnimType_Regular,-1}, //10
	{"deal",0,"Perform a drug dealing animation",24,EAnimType_Regular,-1}, //11
	{"idle",0,"Stand Idle",0,EAnimType_SubAnims,-1}, //12
	{"pay",0,"Perform a paying animation",26,EAnimType_Regular,-1}, //13
	{"crack",0,"Perform a crack animation",27,EAnimType_Regular,-1}, //14
	{"chat",0,"Perform a chat animation",28,EAnimType_Regular,-1}, //15
	{"flipoff",0,"Perform a in flip off animation",29,EAnimType_Regular,-1}, //16
	{"taichi",0,"Perform a taichi animation",30,EAnimType_Regular,-1}, //17
	{"slapass",0,"Perform a slap ass animation",31,EAnimType_Regular,-1}, //18
	{"wave",0,"Perform a waving animation",32,EAnimType_Regular,-1}, //19
	{"vomit",0,"Perform a vomit animation",33,EAnimType_Regular,-1}, //20
	{"cover",0,"Ducks down and hide",34,EAnimType_Regular,-1}, //21
	{"look",0,"Perform a looking animation",35,EAnimType_Regular,-1}, //22
	{"robman",0,"Perform a robbing animation",36,EAnimType_Regular,-1}, //23
	{"rap",0,"Perform a rapping animation",0,EAnimType_SubAnims,-1}, //24
	{"bomb",0,"Perform a bomb planting animation",38,EAnimType_Regular,-1}, //25
	{"smoke",0,"Perform various smoking animations",0,EAnimType_SubAnims,-1},  //26
	{"sit",0,"Perform various sitting animations",0,EAnimType_SubAnims,-1},  //27
	{"hitch",0,"Perform a hitching animation",49,EAnimType_Regular,-1},  //28
	{"scratch",0,"Perform a scratching animation",50,EAnimType_Regular,-1},  //29
	{"kiss",0,"Perform a kissing animation",0,EAnimType_SubAnims,-1}, //30
	{"look",0,"Perform a looking animation",70,EAnimType_Regular,-1}, //31
	{"aim",0,"Perform an aiming animation",71,EAnimType_Regular,-1}, //32
	{"cpr",0,"Perform a CPR animation",72,EAnimType_Regular,-1}, //33
	{"flag",0,"Perform a flagging animation",75,EAnimType_Regular,-1}, //34
	{"bat",0,"Perform a bat animation",0,EAnimType_SubAnims,-1},  //35
	{"lean",0,"Perform a leaning animation",0,EAnimType_SubAnims,-1},  //36
	{"angry",0,"Perform an angry animation",78,EAnimType_Regular,-1}, //37
	{"fixcar",0,"Perform a fix car animation",79,EAnimType_Regular,-1}, //38
	{"handsup",0,"Perform a hands up animation",SPECIAL_ACTION_HANDSUP,EAnimType_SpecialAction,-1}, //39
	{"walk",0,"Perform a walking animation",0,EAnimType_SubAnims,-1},  //40
	{"kick",0,"Perform a kicking animation",92,EAnimType_Regular,-1}, //41
	{"celebrate",0,"Perform a celebration animation",0,EAnimType_SubAnims,-1}, //42
	{"lowrider",0,"Perform an animation on a lowrider",0,EAnimType_SubAnims,-1}, //43
	{"carchat",0,"Perform a car chat animation",105,EAnimType_Regular,-1}, //44
	{"greet",0,"Perform a handshake",0,EAnimType_SubAnims,-1} //45
};

new SubAnims[][ESubAnimInfo] = {
	//dance moves
	{"1",2,EAnimType_SpecialAction,SPECIAL_ACTION_DANCE1, "Dance 1"},
	{"2",2,EAnimType_SpecialAction,SPECIAL_ACTION_DANCE2, "Dance 2"},
	{"3",2,EAnimType_SpecialAction,SPECIAL_ACTION_DANCE3, "Dance 3"},
	{"4",2,EAnimType_SpecialAction,SPECIAL_ACTION_DANCE4, "Dance 4"},
	//
	{"1",4,EAnimType_Regular,7, "Wall Shoot 1"},
	{"2",4,EAnimType_Regular,8, "Wall Shoot 2"},
	
	{"1",5,EAnimType_Regular,9, "Gang Hands 1"},
	{"2",5,EAnimType_Regular,10, "Gang Hands 2"},
	{"3",5,EAnimType_Regular,11, "Gang Hands 3"},
	{"4",5,EAnimType_Regular,12, "Gang Hands 4"},
	{"5",5,EAnimType_Regular,13, "Gang Hands 5"},
	{"6",5,EAnimType_Regular,14, "Gang Hands 6"},
	{"7",5,EAnimType_Regular,15, "Gang Hands 7"},
	{"8",5,EAnimType_Regular,16, "Gang Hands 8"},
	{"9",5,EAnimType_Regular,17, "Gang Hands 9"},
	{"10",5,EAnimType_Regular,18, "Gang Hands 10"},
	{"1",12,EAnimType_Regular,25, "Idle 1"},
	{"2",12,EAnimType_Regular,91, "Idle Tired"},
	{"3",12,EAnimType_Regular,93, "Check out graffiti"},
	{"1",24,EAnimType_Regular,37, "Laugh"},
	{"2",24,EAnimType_Regular,88, "Rap 1"},
	{"3",24,EAnimType_Regular,89, "Rap 2"},
	{"4",24,EAnimType_Regular,90, "Rap 3"},
	{"1",26,EAnimType_Regular,39, "Smoke Male Lean"},
	{"2",26,EAnimType_Regular,40, "Smoke Female Lean"},
	{"3",26,EAnimType_Regular,41, "Smoke Male Standing"},
	{"1",27,EAnimType_Regular,42, "Chair Sit"},
	{"2",27,EAnimType_Regular,43, "Male Ground Sit"},
	{"3",27,EAnimType_Regular,44, "Female Ground Sit"},
	{"4",27,EAnimType_Regular,45, "Bored Seat"},
	{"5",27,EAnimType_Regular,46, "Step Seat"},
	//{"6",27,EAnimType_Regular,47, "Right booth Seat"},
	//{"7",27,EAnimType_Regular,48, "left booth Seat"},
	{"1",30,EAnimType_Regular,62, "Kiss 1",2,63},
	{"2",30,EAnimType_Regular,64, "Kiss 2",2,65},
	{"3",30,EAnimType_Regular,66, "Kiss 3",2,67},
	{"4",30,EAnimType_Regular,68, "Kiss 4",2,69},
	{"1",35,EAnimType_Regular,68, "Bat on shoulder",2,69},
	{"2",35,EAnimType_Regular,68, "Neck crack",2,69},
	{"1",36,EAnimType_Regular,76, "Back lean",0},
	{"2",36,EAnimType_Regular,77, "Left lean",0},
	{"1",40,EAnimType_Regular,82, "Gangsta Walk 1"},
	{"2",40,EAnimType_Regular,83, "Gangsta Walk 2"},
	{"3",40,EAnimType_Regular,84, "Civil Walk"},
	{"4",40,EAnimType_Regular,85, "Woman Sexy Walk"},
	{"5",40,EAnimType_Regular,86, "Woman Normal Walk"},
	{"6",40,EAnimType_Regular,87, "Woman Busy Walk"},
	{"1",42,EAnimType_Regular,94, "Celebration 1"},
	{"2",42,EAnimType_Regular,95, "Celebration 2"},
	{"3",42,EAnimType_Regular,96, "Celebration 3 (BMX)"},
	{"1",43,EAnimType_Regular,97, "Lowrider 1"},
	{"2",43,EAnimType_Regular,98, "Lowrider 2"},
	{"3",43,EAnimType_Regular,99, "Lowrider 3"},
	{"4",43,EAnimType_Regular,100, "Lowrider 4"},
	{"5",43,EAnimType_Regular,101, "Lowrider 5"},
	{"6",43,EAnimType_Regular,102, "Lowrider 6"},
	{"7",43,EAnimType_Regular,103, "Lowrider 7"},
	{"8",43,EAnimType_Regular,104, "Lowrider 8"},
	{"1",45,EAnimType_Regular,106, "Handshake 1",2,106},
	{"2",45,EAnimType_Regular,107, "Handshake 2",2,107},
	{"3",45,EAnimType_Regular,108, "Handshake 3",2,108},
	{"4",45,EAnimType_Regular,109, "Handshake 4",2,109},
	{"5",45,EAnimType_Regular,110, "Handshake 5",2,110},
	{"6",45,EAnimType_Regular,111, "Handshake 6",2,111},
	{"7",45,EAnimType_Regular,112, "Handshake 7",2,112},
	{"8",45,EAnimType_Regular,113, "Handshake 8",2,113}
};

new AnimSequences[][EAnimSequenceInfo] = {
	{"PAULNMAC","Piss_Loop",4.1,1,0,0,0,0,0,SPECIAL_ACTION_PISSING, 55}, //0
	{"BLOWJOBZ", "BJ_Couch_Start_W", 4.1, 0, 0, 0, 0, 2000, 0, 0, 2}, //1
	{"BLOWJOBZ", "BJ_Couch_Loop_W", 5.1, 0, 0, 0, 0, 5000, 0, 0, -1}, //2
	{"BLOWJOBZ", "BJ_COUCH_END_W", 4.0, 0, 0, 0, 0, 0, 0, 0, -1}, //3
	{"ped","KO_skid_front",4.1,0,0,0,1,0,0,0, -1}, //4
	{"RIOT", "RIOT_ANGRY", 4.1, 0, 0, 0, 0, 0, 0, -1}, //5
	{"RIOT", "RIOT_FUKU", 4.1, 0, 0, 0, 0, 0, 0, -1}, //6
	{"HEIST9","swt_wllshoot_in_L",4.1,0,0,0,1,0,0,0, -1}, //7
	{"HEIST9","swt_wllshoot_in_R",4.1,0,0,0,1,0,0,0, -1}, //8
	{"GHANDS","gsign1", 4.1,0,0,0,0,0,0,0, -1}, //9
	{"GHANDS","gsign2", 4.1,0,0,0,0,0,0,0, -1}, //10
	{"GHANDS","gsign3", 4.1,0,0,0,0,0,0,0, -1}, //11
	{"GHANDS","gsign4", 4.1,0,0,0,0,0,0,0, -1}, //12
	{"GHANDS","gsign5", 4.1,0,0,0,0,0,0,0, -1}, //13
	{"GHANDS","gsign1LH", 4.1,0,0,0,0,0,0,0, -1}, //14
	{"GHANDS","gsign2LH", 4.1,0,0,0,0,0,0,0, -1}, //15
	{"GHANDS","gsign3LH", 4.1,0,0,0,0,0,0,0, -1}, //16
	{"GHANDS","gsign4LH", 4.1,0,0,0,0,0,0,0, -1}, //17
	{"GHANDS","gsign5LH", 4.1,0,0,0,0,0,0,0, -1}, //18
	{"GYMNASIUM","GYMshadowbox",4.1,1,0,0,1,0,0,0, -1}, //19
	{"SWEET","Sweet_injuredloop", 4.0, 1,0,0,0,0,0,0, -1}, //20
	{"PAULNMAC","wank_loop", 1.800001, 0,0,0,1,600,0,0, 54}, //21
	{"CRACK","crckdeth1",4.1, 0,0,0,1,0,0,0, -1}, //22
	{"ON_LOOKERS","Pointup_loop",4.1, 0,0,0,1,0,0,0, 60}, //23
	{"DEALER", "DEALER_DEAL", 4.0, 1,0,0,1,0,0,0, -1}, //24
	{"DEALER", "DEALER_IDLE", 4.0, 1,0,0,1,0,0,0, -1}, //25
	{"DEALER", "shop_pay", 4.0, 1,0,0,1,0,0,0, -1}, //26
	{"CRACK", "crckdeth2", 4.0, 1,0,0,1,0,0,0, -1}, //27
	{"ped", "IDLE_CHAT", 4.1, 0,0,0,0,0,1,0, -1}, //28
	{"ped", "FUCKU", 4.1, 0,1,1,1,0,0,0, -1}, //29
	{"PARK", "Tai_Chi_Loop", 4.1, 1,1,1,1,0,0,0, -1},  //30
	{"SWEET", "sweet_ass_slap", 4.1, 0,0,0,0,0,0,0, -1},  //31
	{"ON_LOOKERS", "wave_loop", 4.1, 1,0,0,0,0,0,0, -1},  //32
	{"FOOD", "EAT_Vomit_P", 4.1, 0,0,0,0,0,0,0, -1},  //33
	{"ped", "cower", 4.0, 0,0,0,0,0,0,0, -1},  //34
	{"COP_AMBIENT", "Coplook_loop", 4.0, 0,0,0,0,0,0,0, -1},  //35
	{"SHOP", "ROB_Loop_Threat", 4.0, 1,0,0,0,0,0,0, -1},  //36
	{"RAPPING", "Laugh_01", 4.0, 0,0,0,0,0,0,0, -1},  //37
	{"BOMBER", "BOM_Plant", 4.0, 0,0,0,0,0,0,0, -1},   //38
	{"SMOKING","M_smklean_loop",4.0, 1,0,0,0,0,0,0, -1},   //39
	{"SMOKING","F_smklean_loop",4.0, 1,0,0,0,0,0,0, -1},   //40
	{"SMOKING","M_smkstnd_loop",4.0, 1,0,0,0,0,0,0, -1},   //41
	{"ped","SEAT_idle",4.0, 1,0,0,0,0,0,0, 51},   //42
	{"BEACH", "ParkSit_M_loop",4.0, 1,0,0,0,0,0,0, -1},   //43
	{"BEACH", "ParkSit_W_loop",4.0, 1,0,0,0,0,0,0, -1},   //44
	{"BEACH", "SitnWait_loop_W",4.0, 1,0,0,0,0,0,0, -1},   //45
	{"Attractors", "Stepsit_loop",4.0, 1,0,0,0,0,0,0, 57},   //46
	{"FOOD", "FF_Sit_In_L",4.0, 0,0,0,1,0,0,0, 58},   //47
	{"FOOD", "FF_Sit_In_R",4.0, 0,0,0,1,0,0,0, 59},   //48
	{"MISC","Hiker_Pose",4.0, 0,0,0,1,0,0,0, -1},   //49
	{"MISC","Scratchballs_01",4.0, 0,0,0,1,0,0,0, -1},  //50
	{"ped", "SEAT_up",4.0, 0,0,0,1,1000,0,0, -1},  //51
	{"ON_LOOKERS", "wave_out",4.0, 0,0,0,1,1000,0,0, -1},  //52
	{"PARK", "Tai_Chi_Out",4.0, 0,0,0,1,1000,0,0, -1},  //53
	{"PAULNMAC", "wank_out",4.0, 0,0,0,1,1000,0,0, -1},  //54
	{"PAULNMAC", "Piss_out",4.0, 0,0,0,1,1000,0,0, -1},  //55
	{"CAR", "Fixn_Car_Out",4.0, 0,0,0,1,1000,0,0, -1},  //56 - add
	{"Attractors", "Stepsit_out",4.0, 0,0,0,1,1000,0,0, -1},  //57
	{"FOOD", "FF_Sit_Out_L_180",4.0, 0,0,0,1,1000,0,0, -1},  //58
	{"FOOD", "FF_Sit_Out_R_180",4.0, 0,0,0,1,1000,0,0, -1},  //59
	{"ON_LOOKERS", "Pointup_out",4.0, 0,0,0,1,1000,0,0, -1},  //60
	{"CARRY", "crry_prtial",4.0, 0,0,0,1,0,0,0, -1},  //61 - add
	{"KISSING", "Grlfrd_Kiss_01",4.0, 0,0,0,1,0,0,0, -1}, //62
	{"KISSING", "Playa_Kiss_01",4.0, 0,0,0,1,0,0,0, -1}, //63
	{"KISSING", "Grlfrd_Kiss_02",4.0, 0,0,0,1,0,0,0, -1}, //64
	{"KISSING", "Playa_Kiss_02",4.0, 0,0,0,1,0,0,0, -1}, //65
	{"KISSING", "Grlfrd_Kiss_03",4.0, 0,0,0,1,0,0,0, -1}, //66
	{"KISSING", "Playa_Kiss_03",4.0, 0,0,0,1,0,0,0, -1}, //67
	{"KISSING", "Grlfrd_Kiss_04",4.0, 0,0,0,1,0,0,0, -1}, //68
	{"KISSING", "Playa_Kiss_04",4.0, 0,0,0,1,0,0,0, -1}, //69
	{"ON_LOOKERS", "lkaround_loop",4.0, 0,0,0,1,0,0,0, -1}, //70
	{"ped","ARRESTgun",4.0, 0,0,0,1,0,0,0, -1}, //71
	{"MEDIC","CPR",4.0, 0,0,0,1,0,0,0, -1}, //72
	{"CAR","flag_drop",4.0, 0,0,0,1,0,0,0, -1}, //73
	{"CRACK","Bbalbat_Idle_01",4.0, 0,0,0,1,0,0,0, -1}, //74
	{"CRACK","Bbalbat_Idle_02",4.0, 0,0,0,1,0,0,0, -1}, //75
	{"GANGS","leanIDLE",4.0, 0,0,0,1,0,0,0, -1}, //76
	{"MISC","Plyrlean_loop",4.0, 0,0,0,1,0,0,0, -1},  //77
	{"RIOT","RIOT_ANGRY",4.0, 0,0,0,1,0,0,0, -1}, //78
	{"CAR","Fixn_Car_Loop",4.0, 1,0,0,1,10000,0,0, 80}, //79
	{"CAR", "Fixn_Car_Out",4.0, 0,0,0,1,0,0,0, -1},  //80
	//Walk anims
	{"ped", "WALK_DRUNK",4.0, 1,1,1,1,1,0,0, -1},  //81
	{"ped", "WALK_gang1",4.0, 1,1,1,1,1,0,0, -1},  //82
	{"ped", "WALK_gang2",4.0, 1,1,1,1,1,0,0, -1},  //83
	{"ped", "WALK_civi",4.0, 1,1,1,1,1,0,0, -1},  //84
	{"ped", "WOMAN_walksexy",4.0, 1,1,1,1,1,0,0, -1},  //85
	{"ped", "WOMAN_walknorm",4.0, 1,1,1,1,1,0,0, -1},  //86
	{"ped", "WOMAN_walkbusy",4.0, 1,1,1,1,1,0,0, -1},  //87
	//End of Walk anims
	{"RAPPING", "RAP_A_LOOP",4.0, 1,0,0,0,0,0,0, -1},  //88
	{"RAPPING", "RAP_B_LOOP",4.0, 1,0,0,0,0,0,0, -1},  //89
	{"RAPPING", "RAP_C_LOOP",4.0, 1,0,0,0,0,0,0, -1},  //90
	{"ped", "IDLE_TIRED",4.0, 1,0,0,0,0,0,0, -1},  //91
	{"FIGHT_D", "FIGHTD_G", 4.1, 0, 0, 0, 0, 0, 0, -1}, //92
	{"GRAFFITI", "GRAFFITI_CHKOUT", 4.1, 0, 0, 0, 0, 0, 0, -1}, //93
	{"BENCHPRESS", "gym_bp_celebrate", 4.1, 0, 0, 0, 0, 0, 0, -1}, //94
	{"FREEWEIGHTS", "gym_free_celebrate", 4.1, 0, 0, 0, 0, 0, 0, -1}, //95
	{"MISC", "BMX_celebrate", 4.1, 0, 0, 0, 0, 0, 0, -1}, //96
	{"LOWRIDER", "F_smklean_loop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //97
	{"LOWRIDER", "lrgirl_idleloop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //98
	{"LOWRIDER", "lrgirl_l0_loop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //99
	{"LOWRIDER", "lrgirl_l1_loop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //100
	{"LOWRIDER", "lrgirl_l2_loop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //101
	{"LOWRIDER", "lrgirl_hurry", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //102
	{"LOWRIDER", "prtial_gngtlkC", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //103
	{"LOWRIDER", "prtial_gngtlkD", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //104
	{"CAR_CHAT", "car_talkm_loop", 4.0, 0, 0, 0, 1, 0, 0, 0, -1}, //105
	{"GANGS", "hndshkaa", 4.0, 0,0,0,1,0,0,0, -1}, //106
	{"GANGS", "hndshkba", 4.0, 0,0,0,1,0,0,0, -1}, //107
	{"GANGS", "hndshkca", 4.0, 0,0,0,1,0,0,0, -1}, //108
	{"GANGS", "hndshkcb", 4.0, 0,0,0,1,0,0,0, -1}, //109
	{"GANGS", "hndshkda", 4.0, 0,0,0,1,0,0,0, -1}, //110
	{"GANGS", "hndshkea", 4.0, 0,0,0,1,0,0,0, -1}, //111
	{"GANGS", "hndshkfa", 4.0, 0,0,0,1,0,0,0, -1}, //112
	{"GANGS", "hndshkfa_swt", 4.0, 0,0,0,1,0,0,0, -1} //113
};



forward OnAnimationSequenceFinished(playerid, sequence);
public OnAnimationSequenceFinished(playerid, sequence) {
	if(AnimSequences[sequence][EAnimNextAnim] != -1) {
		new sidx = AnimSequences[sequence][EAnimNextAnim];
		ApplyAnimation(playerid, AnimSequences[sidx][EAnimSeqLib], AnimSequences[sidx][EAnimSeqName],
			AnimSequences[sidx][EAnimSeqfDelta],AnimSequences[sidx][EAnimSeqLoop],
			AnimSequences[sidx][EAnimSeqLockX],AnimSequences[sidx][EAnimSeqLockY],
			AnimSequences[sidx][EAnimSeqFreeze],AnimSequences[sidx][EAnimSeqTime]);
			
		SetTimerEx("OnAnimationSequenceFinished", AnimSequences[sidx][EAnimSeqTime], false, "dd",playerid, sidx);
	} else {
		StopAnimation(playerid);
	}
}
StartPlayerAnimation(playerid, animidx, target) {
	switch(Animations[animidx][EAnimType]) {
		case EAnimType_Regular: {
			new sidx = Animations[animidx][EAnimExtra];
			gCurrentAnim[playerid] = sidx;
			ApplyAnimation(playerid, AnimSequences[sidx][EAnimSeqLib], AnimSequences[sidx][EAnimSeqName],
			AnimSequences[sidx][EAnimSeqfDelta],AnimSequences[sidx][EAnimSeqLoop],
			AnimSequences[sidx][EAnimSeqLockX],AnimSequences[sidx][EAnimSeqLockY],
			AnimSequences[sidx][EAnimSeqFreeze],
			AnimSequences[sidx][EAnimSeqTime]);
			ShowScriptMessage(playerid, "~r~~k~~PED_SPRINT~ ~w~to stop the animation");
			if(AnimSequences[sidx][EAnimSpecialAction] != 0) {
				SetPlayerSpecialAction(playerid, AnimSequences[sidx][EAnimSpecialAction]);
			} else if(AnimSequences[sidx][EAnimNextAnim] >= 0) {
				SetTimerEx("OnAnimationSequenceFinished", AnimSequences[sidx][EAnimSeqTime], false, "dd",playerid, sidx);
			}
			if(target != INVALID_PLAYER_ID) {
				new oanim = FindOppositeAnim(animidx, 0);
				if(oanim == -1) oanim = animidx;
				gCurrentAnim[target] = oanim;
				SetPlayerToFacePlayer(playerid, target);
				SetPlayerToFacePlayer(target, playerid);
				StartPlayerSubAnim(target, oanim, INVALID_PLAYER_ID);
			}
		}
		case EAnimType_SpecialAction: {
			SetPlayerSpecialAction(playerid, Animations[animidx][EAnimExtra]);
		}
	}
}
forward StopAnimation(playerid);
public StopAnimation(playerid) {
	new curanim = gCurrentAnim[playerid];
	new didanim = 0;
	if(curanim != -1) {
		gCurrentAnim[playerid] = -1;
		if(AnimSequences[curanim][EAnimSeqLoop] == 1) {
			if(AnimSequences[curanim][EAnimNextAnim] >= 0) {
				new sidx = AnimSequences[curanim][EAnimNextAnim];
				ApplyAnimation(playerid, AnimSequences[sidx][EAnimSeqLib], AnimSequences[sidx][EAnimSeqName],
				AnimSequences[sidx][EAnimSeqfDelta],AnimSequences[sidx][EAnimSeqLoop],
				AnimSequences[sidx][EAnimSeqLockX],AnimSequences[sidx][EAnimSeqLockY],
				AnimSequences[sidx][EAnimSeqFreeze],
				AnimSequences[sidx][EAnimSeqTime]);
				new atime =  AnimSequences[sidx][EAnimSeqTime];
				if(atime == 0) atime = 1000;
				SetTimerEx("StopAnimation", atime, false, "d",playerid);
				didanim = 1;
			}
		}
	}
	if(didanim == 0) {
		ApplyAnimation(playerid, "CARRY", "crry_prtial", 4.1, 0, 0, 0, 0, 0);
		SetPlayerSpecialAction(playerid, SPECIAL_ACTION_NONE);
		//ClearAnimations(playerid);
	}
}
FindOppositeAnim(animidx, subanim = 0) {
	if(subanim == 0) {
		return Animations[animidx][EAnimOppositeAnim];
	} else {
		return SubAnims[animidx][ESubAnimOppositeAnim];
	}
}
StartPlayerSubAnim(playerid, animidx, target) {
	switch(SubAnims[animidx][ESubAnimType]) {
		case EAnimType_SpecialAction: {
			SetPlayerSpecialAction(playerid, SubAnims[animidx][ESubAnimExtra]);
		}
		case EAnimType_Regular: {
			new sidx = SubAnims[animidx][ESubAnimExtra];
			gCurrentAnim[playerid] = sidx;
			ApplyAnimation(playerid, AnimSequences[sidx][EAnimSeqLib], AnimSequences[sidx][EAnimSeqName],
			AnimSequences[sidx][EAnimSeqfDelta],AnimSequences[sidx][EAnimSeqLoop],
			AnimSequences[sidx][EAnimSeqLockX],AnimSequences[sidx][EAnimSeqLockY],
			AnimSequences[sidx][EAnimSeqFreeze],
			AnimSequences[sidx][EAnimSeqTime]);
			ShowScriptMessage(playerid, "~r~~k~~PED_SPRINT~ ~w~to stop the animation");
			if(AnimSequences[sidx][EAnimSpecialAction] != 0) {
				SetPlayerSpecialAction(playerid, AnimSequences[sidx][EAnimSpecialAction]);
			} else if(AnimSequences[sidx][EAnimSeqLoop] != 1 && AnimSequences[sidx][EAnimNextAnim] != -1) {
				SetTimerEx("OnAnimationSequenceFinished", AnimSequences[sidx][EAnimSeqTime], false, "dd",playerid, sidx);
			}
			if(target != INVALID_PLAYER_ID) {
				new oanim = FindOppositeAnim(animidx, 1);
				if(oanim == -1) oanim = animidx;
				SetPlayerToFacePlayer(playerid, target);
				SetPlayerToFacePlayer(target, playerid);
				ApplyAnimation(target, AnimSequences[oanim][EAnimSeqLib], AnimSequences[oanim][EAnimSeqName],
					AnimSequences[oanim][EAnimSeqfDelta],AnimSequences[oanim][EAnimSeqLoop],
					AnimSequences[oanim][EAnimSeqLockX],AnimSequences[oanim][EAnimSeqLockY],
					AnimSequences[oanim][EAnimSeqFreeze],
					AnimSequences[oanim][EAnimSeqTime]);
				ShowScriptMessage(target, "~r~~k~~PED_SPRINT~ ~w~to stop the animation");
				gCurrentAnim[target] = oanim;
				//StopAnimation(playerid)
				new ntime = AnimSequences[oanim][EAnimSeqTime];
				if(ntime == 0) ntime = 5000;
				SetTimerEx("StopAnimation", ntime, false, "dd",target, oanim);
			}
		}
	}
}
findSubAnimByName(animidx, name[]) {
	for(new i=0;i<sizeof(SubAnims);i++) {
		if(!strcmp(name,SubAnims[i][ESubAnimName], true) && SubAnims[i][ESubAnimAnimID] == animidx) {
			return i;
		}
	}
	return -1;
}

YCMD:anim(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Perform various animations");
		return 1;
	}
	if(isPlayerFrozen(playerid) || isPlayerDying(playerid) || isPlayerWounded(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this right now!");
		return 1;
	}
	new msg[128];
	new animname[64],subanim[32],sanimname[MAX_PLAYER_NAME],target = INVALID_PLAYER_ID;
	if(!sscanf(params,"s[64]S()[32]S()["#MAX_PLAYER_NAME"]",animname, subanim,sanimname)) {
		new animidx = -1;
		for(new i=0;i<sizeof(Animations);i++) {
			if(!strcmp(animname, Animations[i][EAnimName])) {
				animidx = i;
			}
		}
		if(animidx == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "Animation not found!");
			return 1;
		}
		if(Animations[animidx][EAnimType] == EAnimType_SubAnims) {
			if(strlen(subanim) < 1) {
				showSubAnimHelp(playerid, animidx);
				return 1;
			}
			new sidx = findSubAnimByName(animidx, subanim);
			if(sidx == -1) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Sub Animation!");
				return 1;
			}
			
			if(SubAnims[sidx][ESubAnimPerformOther] != 0) {
				if(SubAnims[sidx][ESubAnimPerformOther] == 2 && strlen(sanimname) < 1) {
					SendClientMessage(playerid, X11_WHITE, "USAGE: /anim [anim] [subanim] [playerid/name]");
					return 1;
				}
				target = sscanf_playerLookup(sanimname);
				if(target == INVALID_PLAYER_ID && SubAnims[sidx][ESubAnimPerformOther] == 2) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
					return 1;
				}
				new Float:X, Float:Y, Float:Z;
				GetPlayerPos(playerid, X, Y, Z);
				if(target != INVALID_PLAYER_ID && !IsPlayerInRangeOfPoint(target, 1.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are too far!");
					return 1;
				}
			}
			if(target == playerid) {
				SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this on yourself!");
				return 1;
			}
			if(target != INVALID_PLAYER_ID) {
				format(msg, sizeof(msg), "* %s wants to perform a %s on you (( /accept anim %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),animname,GetPVarInt(playerid, "MaskOn")?GetPVarInt(playerid, "MaskID"):playerid);
				SendClientMessage(target, COLOR_LIGHTBLUE, msg);
				SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Animation Offer Sent!");
				SetPVarInt(target, "OfferingAnim", sidx);
				SetPVarInt(target, "AnimOfferer", playerid);
				SetPVarInt(target, "AnimSubAnim", 1);
				return 1;
			}
			StartPlayerSubAnim(playerid, sidx,target);
		} else {
			if(Animations[animidx][EAnimPerformOther] != 0) {
				if(Animations[animidx][EAnimPerformOther] == 2 && strlen(sanimname) < 1) {
					SendClientMessage(playerid, X11_WHITE, "USAGE: /anim [anim] [subanim] [playerid/name]");
					return 1;
				}
				target = sscanf_playerLookup(sanimname);
				if(target == INVALID_PLAYER_ID && Animations[animidx][EAnimPerformOther] == 2) {
					SendClientMessage(playerid, X11_TOMATO_2, "Invalid User!");
					return 1;
				}
				new Float:X, Float:Y, Float:Z;
				GetPlayerPos(playerid, X, Y, Z);
				if(target != INVALID_PLAYER_ID && !IsPlayerInRangeOfPoint(target, 1.5, X, Y, Z) || GetPlayerVirtualWorld(playerid) != GetPlayerVirtualWorld(target)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You are too far!");
					return 1;
				}
				if(target == playerid) {
					SendClientMessage(playerid, X11_TOMATO_2, "You cannot use this on yourself!");
					return 1;
				}
				if(target != INVALID_PLAYER_ID) {
					format(msg, sizeof(msg), "* %s wants to perform a %s on you (( /accept anim %d ))",GetPlayerNameEx(playerid, ENameType_RPName_NoMask),animname,GetPVarInt(playerid, "MaskOn")?GetPVarInt(playerid, "MaskID"):playerid);
					SendClientMessage(target, COLOR_LIGHTBLUE, msg);
					SendClientMessage(playerid, COLOR_LIGHTBLUE, "* Animation Offer Sent!");
					SetPVarInt(target, "OfferingAnim", animidx);
					SetPVarInt(target, "AnimOfferer", playerid);
					SetPVarInt(target, "AnimSubAnim", 0);
					return 1;
				}
			}
			StartPlayerAnimation(playerid, animidx,target);
		}
	} else {
		showAnimHelp(playerid);
	}
	return 1;
}
YCMD:animlist(playerid, params[], help) {
	showAnimHelp(playerid);
	return 1;
}
/*
//WIP
YCMD:performanim(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Performs a custom animation");
		return 1;
	}
	if(isPlayerFrozen(playerid) || isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't use this right now!");
		return 1;
	}
	new msg[128];
	new animname[64],subanim[32],lock,target = INVALID_PLAYER_ID;
	if(!sscanf(params,"s[64]s[64]d",animname,subanim,lock)) {
		new animidx = -1;
		for(new i=0;i<sizeof(Animations);i++) {
			if(!strcmp(animname, Animations[i][EAnimName])) {
				animidx = i;
			}
		}
	}
	return 1;
}
*/
showAnimHelp(playerid) {
	new msg[128];
	for(new i=0;i<sizeof(Animations);i++) {
		format(msg, sizeof(msg), "%s - %s",Animations[i][EAnimName],Animations[i][EAnimDescription]);
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
}
showSubAnimHelp(playerid, animidx) {
	new msg[128];
	for(new i=0;i<sizeof(SubAnims);i++) {
		if(SubAnims[i][ESubAnimAnimID] == animidx) {
			format(msg, sizeof(msg), "%s - %s",SubAnims[i][ESubAnimName],SubAnims[i][ESubAnimDescription]);
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
		}
	}
}
animsOnGameModeInit() {
	//Animation Textdraw
	/* */
	return 1;
}

animsOnPlayerConnect(playerid) {
	gCurrentAnim[playerid] = -1;
}

animsOnKeyStateChange(playerid, newkeys, oldkeys) {
	if(GetPlayerState(playerid) == PLAYER_STATE_ONFOOT)
	{
		if(gCurrentAnim[playerid] != -1 && (newkeys & KEY_SPRINT) && !(oldkeys & KEY_SPRINT))
		{
			StopAnimation(playerid);
			return 1;
		}
	}
	return 1;
}
