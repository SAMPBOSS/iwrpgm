//Hunger level, from 0-100
//100 = full
//75 = satisfied
//50 = hungry
//< 45 = very hungry
//< 15 = starving
//< 5 = Extremely hungry + losing HP every minute
new PlayerText:HungerTextDraw[MAX_PLAYERS];
new PlayerHunger[MAX_PLAYERS];
new PlayerHungerShown[MAX_PLAYERS];
forward SetPlayerHunger(playerid, level);

hungerOnGameModeInit() {
	for(new i=0;i<MAX_PLAYERS;i++) {
		HungerTextDraw[i] = PlayerText:-1;
		PlayerHungerShown[i] = 0;
	}
}
hungerOnPlayerConnect(playerid) {
	HungerTextDraw[playerid] = CreatePlayerTextDraw(playerid, 634.000000, 424.000000, "Hunger: ~r~Uninitalized");
	PlayerTextDrawBackgroundColor(playerid, HungerTextDraw[playerid], 255);
	PlayerTextDrawFont(playerid, HungerTextDraw[playerid], 1);
	PlayerTextDrawLetterSize(playerid, HungerTextDraw[playerid], 0.350000, 1.200000);
	PlayerTextDrawColor(playerid, HungerTextDraw[playerid], -1);
	PlayerTextDrawSetOutline(playerid, HungerTextDraw[playerid], 1);
	PlayerTextDrawSetProportional(playerid, HungerTextDraw[playerid], 1);
	PlayerTextDrawAlignment(playerid, HungerTextDraw[playerid], 3);
	PlayerHungerShown[playerid] = 0;
}
hungerOnPlayerDisconnect(playerid, reason) {
	if(reason != 3) { //switchchar
		if(HungerTextDraw[playerid] != PlayerText:-1) {
			PlayerTextDrawHide(playerid, HungerTextDraw[playerid]);
			PlayerTextDrawDestroy(playerid, HungerTextDraw[playerid]);
		}
		HungerTextDraw[playerid] = PlayerText:-1;
	}
}
stock GetHungerName(playerid) {
	new level = PlayerHunger[playerid];
	new name[32];
	if(level <= 5) {
		format(name, sizeof(name), "~r~Extremely Hungry");
	} else if(level <= 15) {
		format(name, sizeof(name), "~r~Starving");
	} else if(level <= 45) {
		format(name, sizeof(name), "~r~Hungry");
	} else if(level <= 75) {
		format(name, sizeof(name), "~y~Satisfied");
	} else {
		format(name, sizeof(name), "~g~Full");
	}
	return name;
}
GetHungerLevel(playerid) {
	return PlayerHunger[playerid];
}
YCMD:sethunger(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players hunger level");
		return 1;
	}
	new target, level;
	if(!sscanf(params, "k<playerLookup_acc>D(100)", target, level)) {
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		SetPlayerHunger(target, level);
		SendClientMessage(playerid, X11_YELLOW, "Hunger set.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sethunger [playerid/name] (level)");
	}
	return 1;
}
YCMD:masssethunger(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Sets a players hunger level");
		return 1;
	}
	new level, msg[128];
	if(!sscanf(params, "D(100)", level)) {
		foreach(Player, i) {
			if(IsPlayerConnectEx(i))
				SetPlayerHunger(i, level);
		}
		format(msg, sizeof(msg), "An admin set everyones hunger to: %d",level);
		SendClientMessageToAll(COLOR_LIGHTBLUE, msg);
		SendClientMessage(playerid, X11_YELLOW, "Hunger set.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sethunger [playerid/name] (level)");
	}
	return 1;
}

public SetPlayerHunger(playerid, level) {
	new hungertext[64];
	PlayerHunger[playerid] = level;
	format(hungertext, sizeof(hungertext), "Hunger: %s",GetHungerName(playerid));
	PlayerTextDrawSetString(playerid, HungerTextDraw[playerid], hungertext);
	syncHungerTextDraw(playerid);
}
syncHungerTextDraw(playerid) {
	if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHungerTD)  { //hunger TD is on
		if(PlayerHungerShown[playerid] == 0) {
			PlayerTextDrawShow(playerid, HungerTextDraw[playerid]);
			PlayerHungerShown[playerid] = 1;
		}
	} else {
		PlayerTextDrawHide(playerid, HungerTextDraw[playerid]);
		PlayerHungerShown[playerid] = 0;	
	}
}
//hunger task called every 830 seconds (13.3 minutes)
task HungerTimer[830000]() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i) && !isInJail(i) && GetPlayerState(i) != PLAYER_STATE_SPECTATING) {
			new hlevel = GetHungerLevel(i);
			hlevel--;
			SetPlayerHunger(i, hlevel);
			if(hlevel <= 5) {
				//SendClientMessage(i, X11_TOMATO_2, "Warning: You are extremely hungry and are dying as a result.");
				ShowScriptMessage(i, "~r~Warning: ~w~You are extremely ~r~hungry~w~ and are ~r~dying~w~ as a result.", 8000);
				new Float:health;
				GetPlayerHealth(i, health);
				SetPlayerHealth(i, health-5.0);
			}
		}
	}
}