enum ECameraInfo {
	ECamName[32],
	Float:ECameraX,
	Float:ECameraY,
	Float:ECameraZ,
	Float:ECameraCamRotX,
	Float:ECameraCamRotY,
	Float:ECameraCamRotZ,
	Float:ECameraCamPosX,
	Float:ECameraCamPosY,
	Float:ECameraCamPosZ,
	Float:ECameraLookAtX,
	Float:ECameraLookAtY,
	Float:ECameraLookAtZ,
	ECameraInt,
	ECameraVW,
	ECamModelID,
	ECamObjectID,
	ECamLastWarnTime,
};

#define CAM_WARN_TIMEOUT_TIME 120

new Cameras[][ECameraInfo] = {{"Unity 1",1735.86, -1863.54, 20.73, 0.00, 0.00, -88.08,1735.3640, -1862.5308, 20.4238, 1734.7325, -1861.7527, 20.3938,0 ,0, 1622},
							  {"Unity 2",1788.09, -1867.30, 19.09,   0.00, 0.00, -92.04,1786.6105, -1866.7118, 19.4715,1785.7358, -1866.2227, 19.3014,0,0,1622},
							  {"Unity 3",1702.14, -1888.43, 19.27,   0.00, 0.00, 106.74,1702.5939, -1889.8429, 18.8963, 1702.9991, -1890.7561, 18.7213,0,0,1622},
							  {"LSPD Entrance",1553.88, -1672.82, 19.48,   0.00, 0.00, -0.42,1550.8767, -1673.1896, 19.0871, 1550.0753, -1673.7933, 18.9570,0,0,1622},
							  {"LSPD Garage",1524.93, -1676.02, 11.01,   -5.18, -3.05, 162.25,1527.2275, -1677.8651, 10.4859,1528.0829, -1678.3899, 10.3258,0,0,1616},
							  {"All Saints",1172.16, -1327.99, 17.60,   0.00, 0.00, 156.06,1173.8961, -1328.2769, 17.0020,1174.7219, -1327.7070, 16.8920,0,0,1622},
							  {"Jefferson Dealership",2137.02, -1154.83, 27.11,   0.00, 0.00, -91.86,2136.4912, -1153.7708, 26.7722,2136.1580, -1152.8247, 26.6122,0,0,1622},
							  {"Grotti Dealership",530.4159, -1293.4771, 25.4186,   0.0000, 0.0000, -86.5800,531.3474, -1292.1292, 24.9534,531.9603, -1291.3417, 24.8586,0,0,1622},
							  {"LSPD Interior",243.52, 71.99, 1007.17,   9.30, -29.40, 90.84,243.7235, 71.3338, 1006.5217, 244.1345, 70.4238, 1006.0563, 6, 2,1622},
							  {"FBI HQ",301.4640, 170.0520, 1010.5099,   0.0,0.0, -85.2600,300.9246, 171.2752, 1010.5349, 300.0112, 171.6788, 1010.4102, 3, 3,1622},
							  {"FBI HQ 2",230.1856, 139.6516, 1008.7007,   0.0000, 0.0000, -105.6000,231.9973, 141.7043, 1008.8755, 232.4569, 142.5907, 1008.5997, 3, 3,2921},
							  {"FBI Garage",304.1838, -1479.3014, 28.6940,   3.1200, 0.7200, 135.1200,305.0700, -1481.2413, 28.4434, 304.9156, -1482.2285, 28.3068, 0, 0, 1616},
							  {"Santa Maria Beach",356.16162, -2071.01733, 13.26333,   -3.06000, 3.30000, 77.04000, 355.6197, -2071.8516, 13.3333, 356.3904, -2072.4878, 13.1033, 0, 0, 1622},
							  {"Pizza Stack",2068.92, -1781.06, 16.2091,   0.0000, 0.0000, -248.0000, 2068.92, -1781.06, 16.3091, 2094.8328, -1765.6581, 13.5596, 0, 0, 1622}};

camsOnGameModeInit() {
	for(new i=0;i<sizeof(Cameras);i++) {
		Cameras[i][ECamObjectID] = CreateDynamicObject( Cameras[i][ECamModelID] , Cameras[i][ECameraX], Cameras[i][ECameraY], Cameras[i][ECameraZ] , Cameras[i][ECameraCamRotX] , Cameras[i][ECameraCamRotY] , Cameras[i][ECameraCamRotZ] , Cameras[i][ECameraVW], Cameras[i][ECameraInt]);
	}
	//LSPD watchcam
	CreateDynamicPickup(1239, 16, 7.017215, -8.180313, 1499.265991,-1,16);
	CreateDynamic3DTextLabel("(( /watchcam ))", 0x2BB00AA, 7.017215, -8.180313, 1499.265991+1, 50.0, .worldid = -1, .interiorid = 16);
	
	CreateDynamicPickup(1239, 16, 252.970809, 188.823455, 1008.171875,3,3);
	CreateDynamic3DTextLabel("(( /watchcam ))", 0x2BB00AA, 252.970809, 188.823455, 1008.171875+1, 50.0, .worldid = 3, .interiorid = 3);
	
}
YCMD:watchcam(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Watches a camera");
		return 1;
	}
	new camid;
	new EAdminFlags:aflags = EAdminFlags:GetPVarInt(playerid, "AdminFlags");
	if(~aflags & EAdminFlags_FactionAdmin) {
		if(!IsAnLEO(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't a cop!");
			return 1;
		}
		if(!IsOnDuty(playerid)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not on duty!");
			return 1;
		}
		if(!IsPlayerInRangeOfPoint(playerid, 5.0, 231.17, 78.84, 1005.04) && !IsPlayerInRangeOfPoint(playerid, 5.0, 252.970809, 188.823455, 1008.171875)) {
			SendClientMessage(playerid, X11_TOMATO_2, "You aren't at the camera spot!");
			return 1;
		}
	}
	if(!sscanf(params, "D(0)", camid)) {
		if(camid == -1) {
			playerStopWatchCam(playerid);
		} else {
			playerWatchCamera(playerid, camid);
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /watchcam [camid]");
	}
	return 1;
}
getCamText(cam,dst[],dstlen) {
	format(dst, dstlen, "~g~Camera: ~r~%s~n~~n~~b~Controls~n~~n~~g~Previous: ~r~~k~~PED_FIREWEAPON~~n~~g~Next: ~r~~k~~PED_LOCK_TARGET~~n~~g~Exit: ~r~~k~~PED_SPRINT~",Cameras[cam][ECamName]);
	return 0;
}
playerWatchCamera(playerid, camid) {
	new camtext[256];
	getCamText(camid, camtext, sizeof(camtext));
	new PlayerText:textdraw;
	if(GetPVarType(playerid, "CamWatchX") == PLAYER_VARTYPE_NONE) {
		textdraw = CreatePlayerTextDraw(playerid, 402.0, 193.0, camtext);
		PlayerTextDrawSetOutline(playerid, textdraw, 1);
		PlayerTextDrawSetShadow(playerid, textdraw, 0);
		PlayerTextDrawUseBox(playerid, textdraw, 1);
		PlayerTextDrawBackgroundColor(playerid, textdraw ,0x000000FF);
		PlayerTextDrawBoxColor(playerid, textdraw ,0x00000066);
		PlayerTextDrawFont(playerid, textdraw, 1);
		PlayerTextDrawShow(playerid, textdraw);
		SetPVarInt(playerid, "CamTextDraw", _:textdraw);
		new Float:X, Float:Y, Float:Z, Float:A;
		GetPlayerFacingAngle(playerid, A);
		GetPlayerPos(playerid, X, Y, Z);
		new Float:HP;
		GetPlayerHealth(playerid, HP);
		SetPVarFloat(playerid, "CamWatchHP", HP);
		GetPlayerArmourEx(playerid, HP);
		SetPVarFloat(playerid, "CamWatchArmour", HP);
		SetPVarFloat(playerid, "CamWatchX",X);
		SetPVarFloat(playerid, "CamWatchY",Y);
		SetPVarFloat(playerid, "CamWatchZ",Z);
		SetPVarFloat(playerid, "CamWatchAngle", A);
		SetPVarInt(playerid, "CamWatchInt", GetPlayerInterior(playerid));
		SetPVarInt(playerid, "CamWatchVW", GetPlayerVirtualWorld(playerid));
		TogglePlayerControllableEx(playerid, 0);
	} else {
		textdraw = PlayerText:GetPVarInt(playerid, "CamTextDraw");
		PlayerTextDrawSetString(playerid, textdraw, camtext);
	}
	SetPVarInt(playerid, "CamID", camid);
	SetPlayerInterior(playerid, Cameras[camid][ECameraInt]);
	SetPlayerVirtualWorld(playerid, Cameras[camid][ECameraVW]);
	SetPlayerPos(playerid, Cameras[camid][ECameraX], Cameras[camid][ECameraY]+100.0, Cameras[camid][ECameraZ]+100.0);
	SetPlayerCameraPos(playerid, Cameras[camid][ECameraCamPosX], Cameras[camid][ECameraCamPosY], Cameras[camid][ECameraCamPosZ]);
	SetPlayerCameraLookAt(playerid, Cameras[camid][ECameraLookAtX], Cameras[camid][ECameraLookAtY], Cameras[camid][ECameraLookAtZ], CAMERA_CUT);
	Streamer_UpdateEx(playerid, Cameras[camid][ECameraX], Cameras[camid][ECameraY], Cameras[camid][ECameraZ],Cameras[camid][ECameraVW],Cameras[camid][ECameraInt]);
}
playerStopWatchCam(playerid) {
	SetCameraBehindPlayer(playerid);
	SetPlayerPos(playerid, GetPVarFloat(playerid, "CamWatchX"), GetPVarFloat(playerid, "CamWatchY"), GetPVarFloat(playerid, "CamWatchZ"));
	SetPlayerFacingAngle(playerid, GetPVarFloat(playerid, "CamWatchAngle"));
	SetPlayerVirtualWorld(playerid, GetPVarInt(playerid, "CamWatchVW"));
	SetPlayerInterior(playerid, GetPVarInt(playerid, "CamWatchInt"));
	DeletePVar(playerid, "CamWatchX");
	DeletePVar(playerid, "CamWatchY");
	DeletePVar(playerid, "CamWatchZ");
	DeletePVar(playerid, "CamWatchAngle");
	DeletePVar(playerid, "CamWatchInt");
	DeletePVar(playerid, "CamWatchVW");
	DeletePVar(playerid, "CamID");
	new PlayerText:textdraw = PlayerText:GetPVarInt(playerid, "CamTextDraw");
	TogglePlayerControllableEx(playerid, 1);
	PlayerTextDrawHide(playerid, textdraw);
	PlayerTextDrawDestroy(playerid, textdraw);
}
camsOnPlayerKeyState(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	if(GetPVarType(playerid, "CamID") != PLAYER_VARTYPE_NONE) {	
			new cam = GetPVarInt(playerid, "CamID");
			if(newkeys & KEY_FIRE) {
				cam--;
			}
			if(newkeys & KEY_HANDBRAKE) {
				cam++;
			}
			if(newkeys & KEY_SPRINT) {
				playerStopWatchCam(playerid);
				return 1;
			}
			if(cam < 0) {
				cam = sizeof(Cameras)-1;
			} else if(cam > sizeof(Cameras)-1) {
				cam = 0;
			}
			playerWatchCamera(playerid, cam);
	}
	return 0;
}
getNearestCamera(playerid, Float:range = 60.0) {
	for(new i=0;i<sizeof(Cameras);i++) {
		if(IsPlayerInRangeOfPoint(playerid, range, Cameras[i][ECameraX], Cameras[i][ECameraY], Cameras[i][ECameraZ])) {
			return i;
		}
	}
	return -1;
}
new playerGunOut[MAX_PLAYERS];
camOnPlayerConnect(playerid) {
	playerGunOut[playerid] = 0;
}
camsOnPlayerUpdate(playerid) {
	new holding = GetPlayerWeaponEx(playerid);
	if(!isDangerousWeapon(holding)) return 0;
	if(playerGunOut[playerid] == 0 && holding != 0) {
		sendCameraWarning(playerid);
		playerGunOut[playerid] = 1;
	} else if(holding == 0) {
		playerGunOut[playerid] = 0;
	}
	return 1;
}
sendCameraWarning(playerid) {
	new msg[128];
	new cam = getNearestCamera(playerid);
	if(cam == -1) return 0;
	if(gettime()-Cameras[cam][ECamLastWarnTime] < CAM_WARN_TIMEOUT_TIME) {
		return 0;
	}
	if(IsInCameraDisablerArea(Cameras[cam][ECameraX],Cameras[cam][ECameraY],Cameras[cam][ECameraZ])) return 0;
	if(IsAnLEO(playerid) && IsOnDuty(playerid)) return 0;
	Cameras[cam][ECamLastWarnTime] = gettime();
	format(msg, sizeof(msg), "* The camera takes a snapshot of the suspect and reports it to the authorities. (( %s Camera ))",Cameras[cam][ECamName]);
	SendAreaMessage(60.0,Cameras[cam][ECameraX],Cameras[cam][ECameraY],Cameras[cam][ECameraZ],0,msg,COLOR_PURPLE);
	format(msg, sizeof(msg), "HQ: Message from CCTV System: {EE0000}Armed suspect near: %s",Cameras[cam][ECamName]);
	SendCopMessage(TEAM_BLUE_COLOR, msg);
	return 1;
}