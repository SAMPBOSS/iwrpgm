enum EStorageAreaInfo {
	Float:EStorageAreaX,
	Float:EStorageAreaY,
	Float:EStorageAreaZ,
	Float:EStorageAreaExitX,
	Float:EStorageAreaExitY,
	Float:EStorageAreaExitZ,
	EStorageAreaInterior,
	EStorageAreaName[64],
	EStorageAreaSQLID,
	EStorageAreaPickup,
	EStorageAreaLocked,
	EStorageAreaPrice,
	EStorageAreaOwner,
	EStorageOwnerName[MAX_PLAYER_NAME],
	Text3D:EStorageAreaTextLabel,
};
enum EStorageAreaIntType {
	Float: EIntStorageAreaXDoor,
	Float: EIntStorageAreaYDoor,
	Float: EIntStorageAreaZDoor,
	ETypeStorageADesc[32],
	EIntInteriorID,
}
new StorageAreaIntType[][EStorageAreaIntType] = {
	{302.03, 300.74, 875.73, "Storage Area Int 1", 41} //This is the exit for the 1st interior (Storage Area Int 1)
};
#define MAX_STORAGEAREAS 3000
new StorageAreas[MAX_STORAGEAREAS][EStorageAreaInfo];
forward OnLoadStorageAreas();
forward ReloadStorageArea(index);

storageOnGameModeInit() {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `s`.`id`,`s`.`X`,`s`.`Y`,`s`.`Z`,`s`.`EX`,`s`.`EY`,`s`.`EZ`,`s`.`interior`,`s`.`name`,`s`.`locked`,`s`.`price`,`s`.`owner`,`c1`.`username` FROM `storageareas` as `s` LEFT JOIN `characters` AS `c1` ON `s`.`owner` = `c1`.`id`");
	mysql_function_query(g_mysql_handle, query, true, "OnLoadStorageAreas", "");
}
public OnLoadStorageAreas() { //This is completely fine
	new rows, fields;
	new id_string[64];
	cache_get_data(rows, fields);
	for(new index=0;index<rows;index++) {
		new i = findFreeStorageArea();
		if(i == -1) {
			printf("StorageAreas Array IS FULL!\n");
			continue;
		}
		//if(StorageAreas[i][EStorageAreaSQLID] != 0) continue; //because this is also used for reloading StorageAreas, except that writing this here will cause it to not reload the StorageArea lol
		cache_get_row(index, 0, id_string);
		StorageAreas[i][EStorageAreaSQLID] = strval(id_string);
		
		cache_get_row(index, 1, id_string);
		StorageAreas[i][EStorageAreaX] = floatstr(id_string);
		
		cache_get_row(index, 2, id_string);
		StorageAreas[i][EStorageAreaY] = floatstr(id_string);
		
		cache_get_row(index, 3, id_string);
		StorageAreas[i][EStorageAreaZ] = floatstr(id_string);
		
		cache_get_row(index, 4, id_string);
		StorageAreas[i][EStorageAreaExitX] = floatstr(id_string);
		
		cache_get_row(index, 5, id_string);
		StorageAreas[i][EStorageAreaExitY] = floatstr(id_string);
		
		cache_get_row(index, 6, id_string);
		StorageAreas[i][EStorageAreaExitZ] = floatstr(id_string);
		
		cache_get_row(index, 7, id_string);
		StorageAreas[i][EStorageAreaInterior] = strval(id_string);
		
		cache_get_row(index, 8, StorageAreas[i][EStorageAreaName]);
		
		cache_get_row(index, 9, id_string);
		StorageAreas[i][EStorageAreaLocked] = strval(id_string);
		
		cache_get_row(index, 10, id_string);
		StorageAreas[i][EStorageAreaPrice] = strval(id_string);

		cache_get_row(index, 11, id_string);
		StorageAreas[i][EStorageAreaOwner] = strval(id_string);

		if(StorageAreas[i][EStorageAreaOwner] == 0) {
			format(StorageAreas[i][EStorageOwnerName],MAX_PLAYER_NAME,"No-One");
		} else if(StorageAreas[i][EStorageAreaOwner] == -1) {
			format(StorageAreas[i][EStorageOwnerName],MAX_PLAYER_NAME,"The State");
		} else {
			cache_get_row(index, 12, id_string); //owner username
			format(StorageAreas[i][EStorageOwnerName],MAX_PLAYER_NAME,"%s",id_string);
		}

		StorageAreas[i][EStorageAreaPickup] = CreateDynamicPickup(1239,16,StorageAreas[i][EStorageAreaX], StorageAreas[i][EStorageAreaY], StorageAreas[i][EStorageAreaZ]);
		
		new labeltext[256];
		getStorageAreaTextLabel(i, labeltext, sizeof(labeltext));
		StorageAreas[i][EStorageAreaTextLabel] = CreateDynamic3DTextLabel(labeltext, X11_ORANGE, StorageAreas[i][EStorageAreaX], StorageAreas[i][EStorageAreaY], StorageAreas[i][EStorageAreaZ]+1.5,10.0);
	}
	return 1;
}
storageGetVirtualWorld(storageareaid) {
	return storageareaid+20000;
}
storageareaGetInterior(storageareaid) {
	return StorageAreas[storageareaid][EStorageAreaInterior];
}
storageareaTryEnterExit(playerid) {
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(IsPlayerInRangeOfPoint(playerid, 1.5, StorageAreas[i][EStorageAreaX],StorageAreas[i][EStorageAreaY],StorageAreas[i][EStorageAreaZ])) {
			if(StorageAreas[i][EStorageAreaLocked] == 1) {
				GameTextForPlayer(playerid, "~r~Locked", 2000, 1);
				return 1;
			}
			WaitForObjectsToStream(playerid);
			SetPlayerPos(playerid,  StorageAreas[i][EStorageAreaExitX],StorageAreas[i][EStorageAreaExitY],StorageAreas[i][EStorageAreaExitZ]);
			SetPlayerInterior(playerid, storageareaGetInterior(i));
			SetPlayerVirtualWorld(playerid, storageGetVirtualWorld(i));
		} else if(IsPlayerInRangeOfPoint(playerid, 5.0, StorageAreas[i][EStorageAreaExitX],StorageAreas[i][EStorageAreaExitY],StorageAreas[i][EStorageAreaExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == storageGetVirtualWorld(i)) {
				SetPlayerPos(playerid, StorageAreas[i][EStorageAreaX],StorageAreas[i][EStorageAreaY],StorageAreas[i][EStorageAreaZ]);
				SetPlayerInterior(playerid, 0);
				SetPlayerVirtualWorld(playerid, 0);
			}
		}
	}
	return 1;
}
makeStorageArea(Float:X,Float:Y,Float:Z, name[], type, price) { //This is fine
	query[0] = 0;
	format(query,sizeof(query),"INSERT INTO `storageareas` SET `X` = %f, `Y` = %f, `Z` = %f, `EX` = %f, `EY` = %f, `EZ` = %f, `interior` = %d, `name` = \"%s\", `price`  = %d",X,Y,Z,StorageAreaIntType[type][EIntStorageAreaXDoor],StorageAreaIntType[type][EIntStorageAreaYDoor],StorageAreaIntType[type][EIntStorageAreaZDoor],StorageAreaIntType[type][EIntInteriorID],name,price);
	mysql_function_query(g_mysql_handle, query, true, "OnStorageAreaCreate", "ffffffdsd",X,Y,Z,StorageAreaIntType[type][EIntStorageAreaXDoor],StorageAreaIntType[type][EIntStorageAreaYDoor],StorageAreaIntType[type][EIntStorageAreaZDoor],StorageAreaIntType[type][EIntInteriorID],name,price);
}
forward OnStorageAreaCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior,name[],price);
public OnStorageAreaCreate(Float:X,Float:Y,Float:Z,Float:EX,Float:EY,Float:EZ,interior,name[],price) { //This is fine
	new id = mysql_insert_id();
	new index = findFreeStorageArea();
	new data[128];
	if(index == -1) {
		ABroadcast(X11_RED,"[AdmWarn]: Failed to create Storage Area. Storage Area array is full.",EAdminFlags_BasicAdmin);
		return 0;
	}
	StorageAreas[index][EStorageAreaSQLID] = id;
	format(StorageAreas[index][EStorageAreaName],64,"%s",name);
	StorageAreas[index][EStorageAreaX] = X;
	StorageAreas[index][EStorageAreaY] = Y;
	StorageAreas[index][EStorageAreaZ] = Z;
	StorageAreas[index][EStorageAreaExitX] = EX;
	StorageAreas[index][EStorageAreaExitY] = EY;
	StorageAreas[index][EStorageAreaExitZ] = EZ;
	StorageAreas[index][EStorageAreaInterior] = interior;
	StorageAreas[index][EStorageAreaPickup] = CreateDynamicPickup(1239, 16, StorageAreas[index][EStorageAreaX], StorageAreas[index][EStorageAreaY], StorageAreas[index][EStorageAreaZ]);
	StorageAreas[index][EStorageAreaPrice] = price;
	StorageAreas[index][EStorageAreaOwner] = 0;
	StorageAreas[index][EStorageAreaLocked] = 1;
	getStorageAreaTextLabel(index, data, sizeof(data));
	StorageAreas[index][EStorageAreaTextLabel] = CreateDynamic3DTextLabel(data, X11_ORANGE, StorageAreas[index][EStorageAreaX], StorageAreas[index][EStorageAreaY], StorageAreas[index][EStorageAreaZ]+1.5,10.0);
	format(data,sizeof(data),"[AdmNotice]: Storage Area ID: %d",id);
	ABroadcast(X11_RED,data,EAdminFlags_BasicAdmin);
	return 1;
}
findFreeStorageArea() {
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(StorageAreas[i][EStorageAreaSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
/* Start of admin cmds */
YCMD:reloadstorage(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Reloads a storage area");
		return 1;
	}
	new id;
	if (!sscanf(params, "d", id))
    {
		SendClientMessage(playerid, COLOR_DARKGREEN, "Reloading storage area");
		for(new i=0;i<sizeof(StorageAreas);i++) {
			if(StorageAreas[i][EStorageAreaSQLID] == id) {
				ReloadStorageArea(i);
				return 1;
			}
		}
		SendClientMessage(playerid, X11_WHITE, "Failed to reload storage area");
	} else {
		SendClientMessage(playerid, X11_WHITE,"USAGE: /reloadstorage [sqlid]");
	}
	return 1;
}
YCMD:makestoragearea(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE,"Creates a storage area");
		return 1;
	}
	new Float: X, Float: Y, Float: Z, name[32], price, type;
	if(!sscanf(params,"s[32]D(0)", name, price, type)) {
		GetPlayerPos(playerid, X, Y, Z);
		makeStorageArea(X, Y, Z, name, type, price);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /makestoragearea [name] [price] [int type default = 0]");
		SendClientMessage(playerid, X11_TOMATO_2, "If you add another interior other than 0 it'll cause players to spawn at 0, 0, 0 when they enter.");
	}
	return 1;
}
YCMD:gotostoragearea(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Takes you to a storage area's entrance.");
		return 1;
	}
	new storageareaid;
	if(!sscanf(params,"d", storageareaid)) {
		if(storageareaid < 0 || storageareaid > sizeof(StorageAreas) || StorageAreas[storageareaid][EStorageAreaSQLID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Storage Area");
			return 1;
		}
		SetPlayerPos(playerid, StorageAreas[storageareaid][EStorageAreaX],StorageAreas[storageareaid][EStorageAreaY],StorageAreas[storageareaid][EStorageAreaZ]);
		SetPlayerInterior(playerid, 0);
		SetPlayerVirtualWorld(playerid, 0);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotostoragearea [storageareaid]");
	}
	return 1;
}
/* End of admin cmd's */
YCMD:storagedoor(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Locks/unlocks a storage door");
		return 1;
	}
	new index = getStandingStorageArea(playerid);
	
	if(index == -1) {
		index = getStorageStandingExit(playerid);
		if(index == -1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a storage area");
			return 1;
		}
	}
	if(!hasStorageKeys(playerid, index)) {
		SendClientMessage(playerid, X11_TOMATO_2, "This is not your storage area!");
		return 1;
	}
	new msg[128];
	if(StorageAreas[index][EStorageAreaLocked] == 1) {
		format(msg, sizeof(msg), "* %s unlocked the storage door.", GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		GameTextForPlayer(playerid, "~w~Storage ~g~Unlocked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		StorageAreas[index][EStorageAreaLocked] = 0;
	} else {
		format(msg, sizeof(msg), "* %s locked the storage door.",  GetPlayerNameEx(playerid, ENameType_RPName));
		ProxMessage(15.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		GameTextForPlayer(playerid, "~w~Storage ~r~Locked", 5000, 6);
		PlayerPlaySound(playerid, 1145, 0.0, 0.0, 0.0);
		StorageAreas[index][EStorageAreaLocked] = 1;
	}
	query[0] = 0;
	format(query, sizeof(query), "UPDATE `storageareas` SET `locked` = %d WHERE `id` = %d",StorageAreas[index][EStorageAreaLocked],StorageAreas[index][EStorageAreaSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "ReloadStorageArea", "d",index);
	return 1;
}
YCMD:buystorage(playerid, params[], help) {
	new msg[128];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used for buying a storage area");
		return 1;
	}
	new num = GetNumOwnedStorage(playerid);
	if(num >= GetPVarInt(playerid, "MaxStorageAreas")) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more storage areas!");
		return 1;
	}
	new index = getStandingStorageArea(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing near a storage area");
		return 1;
	}
	if(StorageAreas[index][EStorageAreaOwner] != 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This storage area is already owned by someone");
		return 1;
	}
	new confirmtext[32];
	if(!sscanf(params,"s[32]",confirmtext)) {
		if(strcmp(confirmtext,"confirm", true) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You must type /buystorage confirm");
			return 1;
		}
	} else {
		format(msg, sizeof(msg), "* This storage area costs $%s, type /buystorage confirm",getNumberString(StorageAreas[index][EStorageAreaPrice]));
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		return 1;
	}
	new money = GetMoneyEx(playerid);
	if(money < StorageAreas[index][EStorageAreaPrice]) {
		format(msg, sizeof(msg), "You do not have enough money, you need $%s more to buy this storage area.",getNumberString(StorageAreas[index][EStorageAreaPrice]-money));
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	GiveMoneyEx(playerid, -StorageAreas[index][EStorageAreaPrice]);
	setStorageAreaOwner(index, GetPVarInt(playerid, "CharID"));
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations on your new storage area!");
	return 1;
}
YCMD:sellstorage(playerid, params[], help) {
	new index = getStandingStorageArea(playerid);
	if(index == -1) {
		SendClientMessage(playerid, X11_WHITE, "You must be standing at a storage area");
		return 1;
	}
	if(StorageAreas[index][EStorageAreaOwner] != GetPVarInt(playerid,"CharID") || StorageAreas[index][EStorageAreaOwner] == 0) {
		SendClientMessage(playerid, X11_WHITE, "You don't own this storage area.");
		return 1;
	}
	GiveMoneyEx(playerid, floatround(StorageAreas[index][EStorageAreaPrice]*0.8));
	setStorageAreaOwner(index, 0);
	SendClientMessage(playerid, COLOR_DARKGREEN, "You have sold your storage area!");
	return 1;
}
GetNumOwnedStorage(playerid) {
	new num;
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(StorageAreas[i][EStorageAreaOwner] == GetPVarInt(playerid, "CharID")) {
			if(StorageAreas[i][EStorageAreaSQLID] != 0) {
				num++;
			}
		}
	}
	return num;
}
public ReloadStorageArea(index) {
	new id = StorageAreas[index][EStorageAreaSQLID];
	StorageAreas[index][EStorageAreaSQLID] = 0;
	DestroyDynamicPickup(StorageAreas[index][EStorageAreaPickup]);
	DestroyDynamic3DTextLabel(StorageAreas[index][EStorageAreaTextLabel]);
	#if debug
	printf("Reloading Storage Area: %d", id);
	#endif
	query[0] = 0;//[512];
	format(query, sizeof(query), "SELECT `s`.`id`,`s`.`X`,`s`.`Y`,`s`.`Z`,`s`.`EX`,`s`.`EY`,`s`.`EZ`,`s`.`interior`,`s`.`name`,`s`.`locked`,`s`.`price`,`s`.`owner`,`c1`.`username` FROM `storageareas` as `s` LEFT JOIN `characters` AS `c1` ON `s`.`owner` = `c1`.`id` WHERE `s`.`id` = %d",id);
	mysql_function_query(g_mysql_handle, query, true, "OnLoadStorageAreas", "");
}
getStorageAreaTextLabel(index, dst[], len) {
	if(StorageAreas[index][EStorageAreaOwner] == 0) {
		format(dst, len, "{00FF00}$$ For Sale $$\n$%s\n\n{FF9933}%s",getNumberString(StorageAreas[index][EStorageAreaPrice]), StorageAreas[index][EStorageAreaName]);
	} else {
		new openmsg[32];
		if(StorageAreas[index][EStorageAreaLocked] == 0) {
			format(openmsg,sizeof(openmsg),"{00FF00}OPEN");
		} else {
			format(openmsg,sizeof(openmsg),"{FF0000}LOCKED");
		}
		format(dst, len, "%s\n\n{FF9933}%s\nOwner: %s", openmsg, StorageAreas[index][EStorageAreaName],StorageAreas[index][EStorageOwnerName]);
	}
}
/*
//Useless
IsStorageAreaLocked(storageareaid) {
	return StorageAreas[storageareaid][EStorageAreaLocked] == 1;
}
*/
getStandingStorageArea(playerid, Float:radi = 2.0) {
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, StorageAreas[i][EStorageAreaX],StorageAreas[i][EStorageAreaY],StorageAreas[i][EStorageAreaZ])) {
			return i;
		}
	}
	return -1;
}
getStorageStandingExit(playerid, Float:radi = 5.0) {
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(IsPlayerInRangeOfPoint(playerid, radi, StorageAreas[i][EStorageAreaExitX],StorageAreas[i][EStorageAreaExitY],StorageAreas[i][EStorageAreaExitZ])) {
			if(GetPlayerVirtualWorld(playerid) == storageGetVirtualWorld(i)) {
				return i;
			}
		}
	}
	return -1;
}
storageareaIDFromSQLID(sqlid) {
	for(new i=0;i<sizeof(StorageAreas);i++) {
		if(StorageAreas[i][EStorageAreaSQLID] == sqlid) {
			return i;
		}
	}
	return -1;
}
setStorageAreaOwner(index, sqlid) {
	query[0] = 0;//[128];
	if(sqlid == 0) { //It's being sold so..
		format(query,sizeof(query),"UPDATE `storageareas` SET `locked` = 1 WHERE `id` = %d", StorageAreas[index][EStorageAreaSQLID]); //Lock it as well
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	}
	format(query,sizeof(query),"UPDATE `storageareas` SET `owner` = %d WHERE `id` = %d", sqlid, StorageAreas[index][EStorageAreaSQLID]);
	mysql_function_query(g_mysql_handle, query, true, "ReloadStorageArea", "d",index);
}
sellInactiveStorage() {
	mysql_function_query(g_mysql_handle, "SELECT `storageareas`.`id` FROM `storageareas` INNER JOIN `characters` ON `storageareas`.`owner` = `characters`.`id`  INNER JOIN `accounts` ON `characters`.`accountid` = `accounts`.`id` WHERE `storageareas`.`owner` != -1 AND `accounts`.`lastlogin` < DATE_SUB(CURDATE(),INTERVAL 2 WEEK)", true, "OnCheckInactiveStorage", "");
}
forward OnCheckInactiveStorage();
public OnCheckInactiveStorage() {
	new rows, fields;
	new id, id_string[128];
	cache_get_data(rows, fields);
	for(new i=0;i<rows;i++) {
		cache_get_row(i, 0, id_string);
		id = strval(id_string);
		new hid = storageareaIDFromSQLID(id);
		if(hid != -1) {
			setStorageAreaOwner(hid, 0);
		}
	}
}
hasStorageKeys(playerid, index) {
	return StorageAreas[index][EStorageAreaOwner] == GetPVarInt(playerid, "CharID");
}