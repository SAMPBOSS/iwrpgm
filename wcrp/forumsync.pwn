

#define FORUM_CREATOR_LINK   "www.wc-rp.com/forumacc_creation.php"
#define FORUM_EDIT_LINK   "www.wc-rp.com/forumacc_update_stuff.php"

#define forum-sync false

forward OnForumAccountChange(index, response_code, data[]);
public OnForumAccountChange(index, response_code, data[]) {
	new msg[128];
    if(response_code == 200)//200 is the code for success
    {
        //Yes!
        if(strfind(data, "fail", true) != -1)
        {
            print("[ERROR]: Forum account creation failure! Response:");
            print(data);
            SendClientMessage(index, COLOR_BRIGHTRED, "[ERROR]: There was an error creating your forum account! Please contact an administrator about this.");
			return 0;
        }
        return 1;
    }
    else
    {
        //No!
        format(msg, sizeof(msg), "[ERROR]: There was an error updating your forum account! Please contact our staff with the message: \"Error %d\"", response_code);
        SendClientMessage(index, COLOR_BRIGHTRED, msg);
    }
	return 1;
}
registerForumAccount(playerid, username[],pass[],email[]) {
	#if forum-sync
	new request[256];
	format(request, sizeof(request), "forumusername=%s&forumpassword=%s&forumemail=%s", username, pass, email);
	HTTP(playerid, HTTP_POST, FORUM_CREATOR_LINK, request, "OnForumAccountCreate");
	#endif
}
setForumPassword(playerid, pass[]) {
	#if forum-sync
	new request[256];
	format(request, sizeof(request), "todo=2&oldforumusername=%s&forumpassword=%s", GetPlayerNameEx(playerid, ENameType_AccountName), pass);
	HTTP(playerid, HTTP_POST, FORUM_EDIT_LINK, request, "ForumAccResponse");
	#endif
}
forward OnForumPassChange(index, response_code, data[]);
public OnForumPassChange(index, response_code, data[]) {
	new msg[128];
    if(response_code == 200)//200 is the code for success
    {
        //Yes!
        if(strfind(data, "fail", true) != -1)
        {
            print("[ERROR]: Forum account creation failure! Response:");
            print(data);
            SendClientMessage(index, COLOR_BRIGHTRED, "[ERROR]: There was an error creating your forum account! Please contact an administrator about this.");
			return 0;
        }
        return 1;
    }
    else
    {
        //No!
        format(msg, sizeof(msg), "[ERROR]: There was an error updating your forum account! Please contact our staff with the message: \"Error %d\"", response_code);
        SendClientMessage(index, COLOR_BRIGHTRED, msg);
    }
	return 1;
}
forward OnForumAccountCreate(index, response_code, data[]);
public OnForumAccountCreate(index, response_code, data[]) {
	new msg[128];
    if(response_code == 200)//200 is the code for success
    {
        //Yes!
        if(strfind(data, "fail", true) != -1)
        {
            print("[ERROR]: Forum account creation failure! Response:");
            print(data);
            SendClientMessage(index, COLOR_BRIGHTRED, "[ERROR]: There was an error creating your forum account! Please contact an administrator about this.");
			return 0;
        }
        return 1;
    }
    else
    {
        //No!
        format(msg, sizeof(msg), "[ERROR]: There was an error creating your forum account! Please contact our staff with the message: \"Error %d\"", response_code);
        SendClientMessage(index, COLOR_BRIGHTRED, msg);
    }
	return 1;
}

setForumAccName(playerid, oldusername[], newusername[]) {
	#if forum-sync
	new request[256];
	format(request, sizeof(request), "todo=3&oldforumusername=%s&newforumusername=%s",oldusername, newusername);
	HTTP(playerid, HTTP_POST, FORUM_EDIT_LINK, request, "ForumAccResponse");
	#endif
}
