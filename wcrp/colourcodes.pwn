enum ETextReplaceInfo {
	EReplaceType, //1 = character, 2 = colour
	EReplaceCharacter, 
	EReplaceCode, //
	EReplaceData, //replace colour/string
};
new TextReplaceInfo[][ETextReplaceInfo] = {
{2,'c','1',X11_BLUE},
{2,'c','2',X11_RED},
{2,'c','3',X11_GREEN},
{2,'c','4',X11_YELLOW},
{2,'c','5',X11_PURPLE},
{2,'c','6',X11_DARKGREEN},
{2,'c','7',COLOR_LIGHTBLUE},
{2,'c','8',COLOR_LIGHTGREEN},
{2,'c','b',X11_ORANGE},
{2,'c','l',X11_PINK},
{1,'s','c','�'},
{1,'s','r','�'},
{1,'s','a','�'},
{1,'s','s','�'},
{1,'s','d','�'},
{1,'s','p','�'},
{1,'s','y','�'},
{1,'s','n','�'},
{1,'s','p','�'},
{1,'s','t','�'}
};
YCMD:colourcodes(playerid, params[], help) {
	new msg[128];
	new d;
	for(new i=0;i<sizeof(TextReplaceInfo);i++) {
		if(TextReplaceInfo[i][EReplaceType] == 2) {
			format(msg, sizeof(msg), "%d. /%c%c %sSample Text",++d,TextReplaceInfo[i][EReplaceCharacter],TextReplaceInfo[i][EReplaceCode],printTextReplace(i));
			SendClientMessage(playerid, X11_WHITE, msg);
		}
	}
	return 1;
}
YCMD:charcodes(playerid, params[], help) {
	new msg[128];
	new d;
	for(new i=0;i<sizeof(TextReplaceInfo);i++) {
		if(TextReplaceInfo[i][EReplaceType] == 1) {
			format(msg, sizeof(msg), "%d. /%c%c %s",++d,TextReplaceInfo[i][EReplaceCharacter],TextReplaceInfo[i][EReplaceCode],printTextReplace(i));
			SendClientMessage(playerid, X11_WHITE, msg);
		}
	}
	return 1;
}
YCMD:colourtest(playerid, params[], help) {
	new msg[128];
	if(!sscanf(params, "s[128]", msg)) {
		SendClientMessage(playerid, X11_WHITE, FormatColourString(msg));
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /colourtest [message]");
	}
	return 1;
}
FormatColourString(str[]) {
	new str_final[256];
	new tstr[3];
	new slen = strlen(str);
	if(slen > 128) {
		str[128] = 0;
	}
	for(new i=0;i<slen;i++) {
		if(str[i] == '/') {
			if(str[i+1] != '/' && (i == 0 || str[i-1] != '/')) {
				new c = findTextReplaceInfo(str[i+1],str[i+2]);
				if(c != -1) {
					strcat(str_final,printTextReplace(c),sizeof(str_final));
					i+=2;
					continue;
				}
			}
			
		}
		tstr[0] = str[i];
		strcat(str_final,tstr,sizeof(str_final));
	}
	return str_final;
}
findTextReplaceInfo(ch, code) {
	for(new i=0;i<sizeof(TextReplaceInfo);i++) {
		if(TextReplaceInfo[i][EReplaceCharacter] == ch && TextReplaceInfo[i][EReplaceCode] == code) {
			return i;
		}
	}
	return -1;
}
printTextReplace(c) {
	new ret[32];
	if(TextReplaceInfo[c][EReplaceType] == 1) {
		format(ret, sizeof(ret), "%c",TextReplaceInfo[c][EReplaceData]);
	} else if(TextReplaceInfo[c][EReplaceType] == 2) {
		format(ret, sizeof(ret), "{%s}",getColourString(TextReplaceInfo[c][EReplaceData]));
	}
	return ret;
}