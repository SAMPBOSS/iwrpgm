new File:disconnectlog;
new File:connectlog;
new File:killlog;
new File:cmdlog;
new File:paylog;
new File:itemlog;
new logstr[256];

loggingUpdateLogs() { //Just to give it a more descriptive name
	loggingOnGameModeExit(); //Close all the files
	loggingOnGameModeInit(); //Re-open and create new folders if need be
}
loggingOnGameModeInit() {
	new File:tfd;
	new year, month, day;
	new logstring[128];
	getdate(year, month, day);
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/", day, month, year);
	CreateDir(logstring);
	//Disconnections
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/Disconnections.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to create disconnect file!\n");
		} else {
			fclose(tfd);
		}
	}
	disconnectlog = fopen(logstring,io_append);
	if(!disconnectlog) {
		printf("Failed to open disconnect file!\n");
	}
	//Connections
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/Connections.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to create connect file!\n");
		} else {
			fclose(tfd);
		}
	}
	connectlog = fopen(logstring,io_append);
	if(!connectlog) {
		printf("Failed to open connect file!\n");
	}
	//Commands
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/cmdlog.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to create command log!\n");
		} else {
			fclose(tfd);
		}
	}
	cmdlog = fopen(logstring,io_append);
	if(!cmdlog) {
		printf("Failed to open command log\n");
	}
	//Kills
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/Kills.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to create kill log!\n");
		} else {
			fclose(tfd);
		}
	}
	killlog = fopen(logstring,io_append);
	if(!killlog) {
		printf("Failed to open kill log\n");
	}
	//Paylog
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/pay.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to create pay log!\n");
		} else {
			fclose(tfd);
		}
	}
	paylog = fopen(logstring,io_append);
	if(!paylog) {
		printf("Failed to open pay log\n");
	}
	//Itemlog
	format(logstring, sizeof(logstring), "logs/%d-%d-%d/itemlog.txt",day, month, year);
	if(!fexist(logstring)) {
		tfd = fopen(logstring, io_write);
		if(!tfd) {
			printf("Failed to item log!\n");
		} else {
			fclose(tfd);
		}
	}
	itemlog = fopen(logstring,io_append);
	if(!itemlog) {
		printf("Failed to item log\n");
	}
}
loggingOnGameModeExit() {
	if(disconnectlog) {
		fclose(disconnectlog);
	}
	if(connectlog) {
		fclose(connectlog);
	}
	if(cmdlog) {
		fclose(cmdlog);
	}
	if(killlog) {
		fclose(killlog);
	}
	if(paylog) {
		fclose(paylog);
	}
	if(itemlog) {
		fclose(itemlog);
	}
}
loggingOnCharLogin(playerid) {
	new ip[16];
	new gpciID[64];
	if(!connectlog) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	gpci(playerid, gpciID, sizeof(gpciID));
	GetPlayerIpEx(playerid, ip, sizeof(ip));
	format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] Player: %s || ID: %d || IP: %s || GPCI: %s connected. \r\n",day, month, year, hour, minute, second, GetPlayerNameEx(playerid, ENameType_CharName), playerid, ip, gpciID);
	fwrite(connectlog,logstr);
	aIRCSay(logstr, ircPurple);
	format(logstr, sizeof(logstr), "INSERT INTO `connections` SET `charid` = %d,`ip` = INET_ATON(\"%s\"),`gpci` = \"%s\",`connecttime` = CURRENT_TIMESTAMP()",GetPVarInt(playerid, "CharID"), ip, gpciID);
	mysql_function_query(g_mysql_handle, logstr, true, "InsertConnectLog", "d",playerid);
	return 0;
}
forward InsertConnectLog(playerid);
public InsertConnectLog(playerid) {
	new id = mysql_insert_id();
	SetPVarInt(playerid, "ConnectLogID", id);
}
loggingOnPlayerDisconnect(playerid, reason) {

	format(logstr, sizeof(logstr), "UPDATE `connections` SET `disconnecttime` = CURRENT_TIMESTAMP,`quitreason` = %d WHERE `id` = %d",reason,GetPVarInt(playerid, "ConnectLogID"));
	mysql_function_query(g_mysql_handle, logstr, true, "EmptyCallback", "");
	if(!disconnectlog || reason == 3) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] Name: %s || ID: %d disconnected(%d). \r\n",day, month, year, hour, minute, second,GetPlayerNameEx(playerid, ENameType_CharName),playerid,reason);
	fwrite(disconnectlog,logstr);
	aIRCSay(logstr, ircPurple);
	return 1;
}
loggingOnPlayerDeath(playerid, killerid, reason) {
	if(!killlog) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	if(killerid != INVALID_PLAYER_ID) {
		format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] %s killed by %s - %d\r\n",day, month, year, hour, minute, second,GetPlayerNameEx(playerid, ENameType_CharName),GetPlayerNameEx(killerid, ENameType_CharName), reason);
	} else {
		format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] %s killed - %d\r\n",day, month, year, hour, minute, second,GetPlayerNameEx(playerid, ENameType_CharName), reason);
	}
	fwrite(killlog,logstr);
	aIRCSay(logstr, ircRed);
	return 1;
}
loggingOnPlayerCommand(playerid, cmdtext[]) {
	if(!cmdlog) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] [%d] %s: %s\r\n",day, month, year, hour, minute, second,playerid,GetPlayerNameEx(playerid, ENameType_CharName), cmdtext);
	fwrite(cmdlog,logstr);
	aIRCSay(logstr, "14");
	return 1;
}
payLog(str[]) {
	if(!paylog) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] %s\r\n",day, month, year, hour, minute, second, str);
	fwrite(paylog,logstr);
	aIRCSay(logstr, ircPink);
	return 1;
}
itemLog(str[]) {
	if(!itemlog) {
		return 0;
	}
	new hour;
	new minute;
	new second;
	new day;
	new month;
	new year;
	gettime(hour,minute,second);
	getdate(year, month, day);
	format(logstr,sizeof(logstr),"[%02d/%02d/%02d %02d:%02d:%02d] %s\r\n",day, month, year, hour, minute, second, str);
	fwrite(itemlog,logstr);
	aIRCSay(logstr, ircPink);
	return 1;
}
//Commands
YCMD:refreshlogs(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Refreshes the logs.");
		return 1;
	}
	loggingUpdateLogs();
	SendClientMessage(playerid ,X11_ORANGE, "[NOTICE]: Done!");
	return 1;
}