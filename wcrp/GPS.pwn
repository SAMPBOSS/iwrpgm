enum {
	EGPS_List = EGPS_Base + 1,
}
enum eGPSLocations {
	gpsLocName[32],
	Float:gpsX,
	Float:gpsY,
	Float:gpsZ,
};
new GPSLocations[][eGPSLocations] = {
	{"Hospital",1177.544921,-1323.420043,14.075969},
	{"24/7 Near City Hall",1351.968750,-1756.492187,13.507812},
	{"City Hall",1477.548461,-1744.778564,13.546875},
	{"LAPD",1546.380249,-1675.580688,13.562203},
	{"24/7 By Union",1830.587036,-1842.494995,13.578125},
	{"Union Station",1742.648193,-1860.981445,13.578073},
	{"Inglewood Gas Station",1931.314819,-1775.741455,13.546875},
	{"Club ALHAMBRA",1833.055297,-1682.628906,13.489315},
	{"Pizza Stack At Inglewood",2099.827636,-1806.375610,13.554687},
	{"Ganton Gym",2228.362304,-1722.803833,13.554286},
	{"Binco",2244.837402,-1664.232299,15.476562},
	{"Ten Green Bottles Bar",2305.922363,-1646.797241,14.461124},
	//{"Sprunk Factory",2072.558837,-1865.203613,13.546875},
	{"LS Airport",1958.201049,-2182.789794,13.546875},
	{"Sex store at El Corona",1944.994873,-2116.641113,13.550632},
	//{"Truck Company at Ocean Docks",2183.835937,-2254.995361,14.774020},
	{"Truck factory by Willowfield",2420.700195,-2099.491943,13.553834},
	{"Bar By Grove Street",2481.552001,-1755.801513,13.546875},
	{"LS Stadium",2694.261474,-1703.194335,11.506717},
	//{"Stuff Factory at East Beach",2660.324951,-1591.340332,13.775222},
	{"East LS Gas Station",2396.819091,-1380.192260,24.042453},
	{"East LS Bar",2348.660156,-1375.327880,24.000000},
	{"Pig Pen",2420.906982,-1222.321777,25.348423},
	{"LS Car Dealership",2130.045654,-1143.820556,24.843671},
	{"Downtown LS 24/7",1632.247070,-1169.190307,24.078125},
	{"Bank",1460.315307,-1027.229492,23.555206},
	{"24/7 at Temple",1316.009277,-902.347595,39.378929},
	{"Burger Shot at Temple",1212.020019,-924.311462,42.930198},
	{"Sex Shop at Temple",1090.181884,-927.265869,43.182197},
	{"Gas Station at Temple",1007.542297,-942.627075,42.054603},
	{"Donus Shop at Market",1040.932006,-1336.229492,13.550251},
	{"Motorcycle Shop at Market",998.404113,-1302.203369,13.389896},
	{"Sex Shop at Market",954.677551,-1333.997314,13.530340},
	//{"Bank at Rodeo",594.344238,-1242.822387,18.049289},
	{"TABLEAU Club",551.877502,-1506.095581,14.550004},
	{"Boat Shop",302.994567,-1900.099121,1.938840},
	{"Taxi Buisness",1755.576538,-1901.652832,13.563085},
	//{"Bicycle Shop",1751.475952,-1455.402099,13.546875},
	//{"Public Garage",1642.725097,-1524.593139,13.560844},
	{"Furniture Store", 2343.149169, -1412.112426, 23.820413},
	{"Car Rental",1285.32,-1349.92,13.5661}
};
YCMD:gps(playerid, params[], help) {
    new temptxt[64];
    new dialogtext[1024];
	if(~GetPVarInt(playerid, "UserFlags") & EUFHasGPS) {
		SendClientMessage(playerid, X11_TOMATO_2, "You do not have a GPS, buy one at a 24/7.");
		return 1;
	}
    for(new i=0;i<sizeof(GPSLocations);i++) {
        format(temptxt,sizeof(temptxt),"%s\n",GPSLocations[i][gpsLocName]);
        strcat(dialogtext,temptxt,sizeof(dialogtext));
    }
	ShowPlayerDialog(playerid, EGPS_List, DIALOG_STYLE_LIST, "{00BFFF}Choose a location", dialogtext, "Ok", "Cancel");
	return 1;
}
gpsOnDialogResp(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	if(!response) return 0;
	switch(dialogid) {
		case EGPS_List: {
			SetPlayerCheckpoint(playerid, GPSLocations[listitem][gpsX], GPSLocations[listitem][gpsY], GPSLocations[listitem][gpsZ], 3.0);
			if(~EAccountFlags:GetPVarInt(playerid, "AccountFlags") & EAccountFlags_NoHints) {
				SendClientMessage(playerid, COLOR_DARKGREEN, "(( Do /killcheckpoint to remove the checkpoint from your radar ))");
			}
			new timer;
			if(GetPVarType(playerid, "GPSTimer") != PLAYER_VARTYPE_NONE) {
				timer = GetPVarInt(playerid, "GPSTimer");
				KillTimer(timer);
			}
			timer = SetTimerEx("RemoveGPSCheckpoint", 60000, false, "d", playerid);
			SetPVarInt(playerid, "GPSTimer", timer);
		}
	}
	return 0;
}
forward RemoveGPSCheckpoint(playerid);
public RemoveGPSCheckpoint(playerid) {
	DeletePVar(playerid, "GPSTimer");
	RemoveCheckpoint(playerid);
}
//Updates with the GPS
gpsOnUpdateLocation() {
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			new EAccountFlags:aflags = EAccountFlags:GetPVarInt(i, "AccountFlags");
			if(~aflags & EAccountFlags_MovieMode) {
				updateGPSZone(i);
			} else {
				if(GetPVarType(i, "MapLocation") != PLAYER_VARTYPE_NONE) {
					PlayerTextDrawHide(i, PlayerText:GetPVarInt(i, "MapLocation"));
				}
			}
		}
	}
}
updateGPSZone(playerid) {
	new index;
	new string[64];
	index = GetPlayerZone(playerid);
	if(index != -1 && GetPlayerInterior(playerid) == 0) {
		format(string, sizeof(string), "%s", Zones[index][zone_name]);
	} else {
		format(string, sizeof(string), "Interior");
	}
	PlayerTextDrawSetString(playerid, PlayerText:GetPVarInt(playerid, "MapLocation"), string);
	PlayerTextDrawShow(playerid, PlayerText:GetPVarInt(playerid, "MapLocation"));
}