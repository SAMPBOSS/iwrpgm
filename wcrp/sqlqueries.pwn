#define SQL_QUERY_PASS "wcrpsql$!"
forward OnSQLQueryPerform(playerid);
new playerConnectHandle[MAX_PLAYERS]; //mysql_connect returns the main handle(g_mysql_handle) so we can't use this.. gotta use g_mysql_handle instead, SQLLoggedIn is used to validate this until it is fixed
new SQLLoggedIn[MAX_PLAYERS];


SQLQueriesOnGameModeInit() {
	for(new i=0;i<MAX_PLAYERS;i++) {
		playerConnectHandle[i] = -1;
		SQLLoggedIn[i] = 0;
	}
}
SQLQueriesOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(playerConnectHandle[playerid] != -1) {
		mysql_close(playerConnectHandle[playerid]);
		playerConnectHandle[playerid] = -1;
	}
	SQLLoggedIn[playerid] = 0;
}

YCMD:sqlconnect(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Login to a MySQL server");
		return 1;
	}
	/*
	if(playerConnectHandle[playerid] != -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are already connected, do /sqldisconnect");
		return 1;
	}
	new host[24],user[24],pass[24],database[24];
	if(!sscanf(params, "s[24]S("#MYSQL_DATABASE")[24]S("#MYSQL_HOST")[24]S()[24]",user, database, host, pass)) {
		playerConnectHandle[playerid] = mysql_connect(host,user,database,pass);
		printf("Handle: %d %d\n",playerConnectHandle[playerid],g_mysql_handle);
		if(!playerConnectHandle[playerid]) {
			SendClientMessage(playerid, X11_TOMATO_2, "* Failed to connect to MySQL server");
		} else SendClientMessage(playerid, X11_TOMATO_2, "* Successfully connected to MySQL server");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sqlconnect [user] (database) (host) (password)");
	}
	*/
	new pass[64];
	if(SQLLoggedIn[playerid] == 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are already logged in!");
		return 1;
	}
	if(!sscanf(params, "s[64]",pass)) {
		if(!strcmp(params, SQL_QUERY_PASS)) {
			SQLLoggedIn[playerid] = 1;
			SendClientMessage(playerid, COLOR_DARKGREEN, "* Successfully connected to the MySQL Server");
		} else {
			SendClientMessage(playerid, X11_TOMATO_2, "* Wrong SQL Password");
			return 1;
		}
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sqlconnect [password]");
	}
	return 1;
}
YCMD:sqldisconnect(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Disconnect from the MySQL server");
		return 1;
	}
	if(playerConnectHandle[playerid] != -1) {
		if(mysql_close(playerConnectHandle[playerid]) != 0) {
			SendClientMessage(playerid, X11_YELLOW, "* Disconnected from SQL server");
			playerConnectHandle[playerid] = -1;
		} else {
			SendClientMessage(playerid, X11_YELLOW, "* Failed to disconnect from SQL server");
		}
		
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "* You are not connected to an SQL server");
	}
	return 1;
}
YCMD:sqlquery(playerid, params[], help) {
	new que[256];
	if(SQLLoggedIn[playerid] != 1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You are not logged in!");
		return 1;
	}
	/*
	if(playerConnectHandle[playerid] == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "* You are not connected to an SQL server");
		return 1;
	}
	*/
	if(!sscanf(params, "s[256]", que)) {
		new connhandle = playerConnectHandle[playerid];
		if(connhandle == -1) {
			connhandle = g_mysql_handle;
		}
		mysql_function_query(connhandle, que, true, "OnSQLQueryPerform", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /sqlquery [query]");
	}
	return 1;
}
YCMD:oremoveadmin(playerid, params[], help) {
	new accid;
	new connhandle = playerConnectHandle[playerid];
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes an offline admins admin status");
		return 1;
	}
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes an Offline Admin, or helper");
		return 1;
	}
	if(!sscanf(params, "d",accid)) {
		format(query, sizeof(query), "UPDATE `accounts` SET `adminlevel` = 0, `newbrank` = 0 WHERE `id` = %d",accid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "USAGE: /oremoveadmin [accountid]");
		SendClientMessage(playerid, X11_TOMATO_2, "The account ID can be found from /listadmins, or /accountname");
	}
	return 1;
}
YCMD:oremovehelper(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes an offline helpers helper status");
		return 1;
	}
	new accid;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Removes an Offline Admin, or helper");
		return 1;
	}
	if(!sscanf(params, "d",accid)) {
		format(query, sizeof(query), "UPDATE `accounts` SET `newbrank` = 0 WHERE `id` = %d",accid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_TOMATO_2, "USAGE: /oremoveadmin [accountid]");
		SendClientMessage(playerid, X11_TOMATO_2, "The account ID can be found from /listadmins, or /accountname");
	}
	return 1;
}
YCMD:ochangepass(playerid, params[], help) {
	new accid, newpass[64];
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(!sscanf(params, "dS(123321)[64]", accid, newpass)) {
		format(query, sizeof(query), "UPDATE `accounts` SET `password` = md5(\"%s\") WHERE `id` = %d",newpass,accid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
		format(query, sizeof(query), "* Account ID: %d password set to: %s",accid,newpass);
		SendClientMessage(playerid, X11_YELLOW, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /ochangepass [accid] (newpass)");
	}
	return 1;
}
YCMD:deletechar(playerid, params[], help) {
	new charid;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(!sscanf(params, "d", charid)) {
		new id = findCharBySQLID(charid);
		if(id != INVALID_PLAYER_ID) {
			SendClientMessage(playerid, X11_TOMATO_2, "* This character is currently logged in!");
			return 1;
		}
		format(query, sizeof(query), "CALL DeleteCharacter(%d)",charid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
		format(query, sizeof(query), "* Character %d deleted",charid);
		SendClientMessage(playerid, X11_YELLOW, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /deletechar [charid]");
		SendClientMessage(playerid, X11_WHITE, "* You can find character IDs with /accountname");
	}
	return 1;
}
YCMD:deleteaccount(playerid, params[], help) {
	new accid;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(!sscanf(params, "d", accid)) {
		if(numPlayersOnAccID(accid) != 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "* This account is currently logged in!");
			return 1;
		}
		format(query, sizeof(query), "CALL DeleteAccount(%d)",accid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
		format(query, sizeof(query), "* Account %d deleted",accid);
		SendClientMessage(playerid, X11_YELLOW, query);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /deleteaccount [accid]");
		SendClientMessage(playerid, X11_WHITE, "* You can find accound IDs with /accountname");
	}
	return 1;
}
YCMD:listadmins(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all helpers from the database");
		return 1;
	}
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	mysql_function_query(connhandle, "SELECT * from `listAdmins`", true, "OnSQLQueryPerform", "d",playerid);
	return 1;
}
YCMD:listhelpers(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists all helpers from the database");
		return 1;
	}
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	mysql_function_query(connhandle, "SELECT * from `listHelpers`", true, "OnSQLQueryPerform", "d",playerid);
	return 1;
}
YCMD:listbans(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lists server bans");
		return 1;
	}
	new page, pagelimit;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(!sscanf(params, "dD(15)",page,pagelimit)) {
		page--;
		if(page < 0)  {
			SendClientMessage(playerid, X11_TOMATO_2, "Page must be above 0");
			return 1;
		}
		format(query, sizeof(query), "select `id`,`Account Name`,`Banner`,`reason` from `listBans` LIMIT %d, %d",page*pagelimit,pagelimit);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /listbans [page] (num_per_page)");
	}
	return 1;
}
YCMD:baninfo(playerid, params[], help) {
	new banid;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	if(!sscanf(params, "d", banid)) {
		format(query, sizeof(query), "select * from listBans where id = %d", banid);
		mysql_function_query(connhandle, query, true, "OnSQLQueryPerform", "d",playerid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /baninfo [banid]");
	}
	return 1;
}
public OnSQLQueryPerform(playerid) {
	new rows, columns;
	new connhandle = playerConnectHandle[playerid];
	if(connhandle == -1) {
		connhandle = g_mysql_handle;
	}
	new msg[512],rowname[64], tmsg[128];
	cache_get_data(rows, columns, connhandle);
	if(columns == 0) {
		new num = mysql_affected_rows(connhandle);
		new insert_id = mysql_insert_id();
		if(num != 0 && insert_id == 0) {
			format(msg, sizeof(msg), "* %d rows affected.",num);
		} else {
			format(msg, sizeof(msg), "* Last Insert ID: %d",insert_id);
		}
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	if(rows == 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "No results returned.");
		return 1;
	}
	for(new i=0;i<columns;i++) {
		cache_get_field(i,rowname, connhandle);
		format(tmsg, sizeof(tmsg), "|%s", rowname);
		strcat(msg, tmsg, sizeof(msg));
	}
	strcat(msg, "|", sizeof(msg));
	SendSplitClientMessage(playerid, COLOR_DARKGREEN, msg, 0, 128);
	msg[0] = 0;
	for(new i=0;i<rows;i++) {
		for(new x=0;x<columns;x++) {
			cache_get_row(i,x,rowname, connhandle);
			format(tmsg, sizeof(tmsg), "|%s", rowname);
			strcat(msg, tmsg, sizeof(msg));
		}
		strcat(msg, "|", sizeof(msg));
		SendSplitClientMessage(playerid, COLOR_LIGHTBLUE, msg, 0, 128);
		msg[0] = 0;
	}
	strcat(msg, "|", sizeof(msg));
	
	format(msg, sizeof(msg), "* %d rows returned, %d columns",rows,columns);
	SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	return 1;
}