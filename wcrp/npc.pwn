npcOnGameModeInit() {
	//ConnectNPC("LSPD_Assistant", "LSPDAssist");
	//ConnectNPC("NPC_AKDude","npcak47");
}
YCMD:npc(playerid, params[], help) {
	if(!IsPlayerNPC(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		return 0;
	}
	new name[32];
	if(!sscanf(params, "s[32]", name)) {
		if(!strcmp(name, "LSPDAssist")) {
			SetPVarInt(playerid, "AdminFlags", _:(EAdminFlags_AntiCheat|EAdminFlags_Invisible));
			SetPlayerPos(playerid, 253.989852, 69.826805, 1003.640625);
			SetPlayerFacingAngle(playerid, 251.680236);
			SetPlayerVirtualWorld(playerid, 2);
			SetPlayerSkin(playerid, 267); 
			SetPVarString(playerid, "Accent", "Spanish");
			ApplyAnimation(playerid,"ped","SEAT_idle", 4.0, 1, 0, 0, 0, 0);
			
		}
		SetPlayerColor(playerid, COLOR_LIGHTBLUE);
	}
	return 1;
}
YCMD:lspcnpcmsg(playerid, params[], help) {
	if(!IsPlayerNPC(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		return 0;
	}
	new msg[128], input[64];
	if(!sscanf(params, "s[64]", input)) {
		format(msg, sizeof(msg), "HQ: Message from %s: %s",GetPlayerNameEx(playerid, ENameType_RPName), input);
		SendCopMessage(TEAM_BLUE_COLOR, msg);
	}
	return 1;
}
YCMD:lspdnpcpanic(playerid, params[], help) {
	if(!IsPlayerNPC(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		return 0;
	}
	new msg[128], input[64], target;
	if(!sscanf(params, "k<playerLookup_acc>s[64]", target, input)) {
		if(!IsAnLEO(target)) {
			format(msg, sizeof(msg), "HQ: Message from %s: {EE0000}%s",GetPlayerNameEx(playerid, ENameType_RPName), input);
			SendCopMessage(TEAM_BLUE_COLOR, msg);
		}
	}
	return 1;
}
YCMD:lspdnpcpanic_cop(playerid, params[], help) {
	if(!IsPlayerNPC(playerid) && ~EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		return 0;
	}
	new msg[128], input[64], target;
	if(!sscanf(params, "k<playerLookup_acc>s[64]", target, input)) {
		if(IsAnLEO(target)) {
			format(msg, sizeof(msg), "HQ: Message from %s: {FF0000}%s",GetPlayerNameEx(playerid, ENameType_RPName), input);
			SendCopMessage(TEAM_BLUE_COLOR, msg);
		}
	}
	return 1;
}
