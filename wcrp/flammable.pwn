/*
	PVARS Used By This Script
	N/A
*/
/* Defines */
#define MAX_FLAMMABLE 			1024
#define MAX_FLAMMABLE_DESC 		32
#define FLAMMABLE_STREAM_DIST 	5.0
/* Time */
#define MAX_FLAMMABLE_TIME 		1800 //30 Minutes
#define MAX_FIRE_TIME 			300 //5 Minutes
#define MAX_BONFIRE_TIME 		7200 //2 Hours
#define MAX_MATCH_TIME			5 //3 seconds
/* Objects */
#define MDL_GASCAN 				1650
#define MDL_FIRE 				18688
#define MDL_LIQUID				9831
/* End of Defines */

enum FlammableType {
	Flammable_Fuel,
	Flammable_Alcohol,
	Flammable_Wood, //Even though it's not a flammable..
	Flammable_Fire,
	Flammable_Match,
	Flammable_Molotov,
};
new FlammableName[6][] = {
	{"Fuel"},
	{"Alcohol"},
	{"Wood"},
	{"Fire"},
	{"Match"},
	{"Molotov"}
};
enum {
	State_None,
	State_Liquid,
	State_Fire,
	State_Burnt,
};
enum EFlammableStates {
	FlammableType:pFlammableType,
	pState,
};
new FlammableStates[][EFlammableStates] = { //Each flammable flag state is stored here
	{Flammable_Fuel, State_Liquid},
	{Flammable_Alcohol, State_Liquid},
	{Flammable_Fire, State_Fire},
	{Flammable_Match, State_Fire}
};
enum pFlammable {
	pFlammableDesc[MAX_FLAMMABLE_DESC],
	//pFlammableSQLID,
	pOwnerSQLID,
	pOwner[MAX_PLAYER_NAME],
	pFlammableTime,//time in seconds when it was dropped
	FlammableType:pFlammableType,
	Float:FlammableX,
	Float:FlammableY, 
	Float:FlammableZ,
	FlammableInt,
	FlammableVW,
	Amount,
	pFlammableState,
	FlammableObjID,
	Text3D:FlammableTextLabel,
};
new FlammableInfo[MAX_FLAMMABLE][pFlammable];
/* Functions */
spillFlammable(playerid, FlammableType:FlamType) {
	new index = findFreeFlammableSlot();
	new msg[128];
	if(index == -1) {
		//Max reached! Don't add any more flammable stuff but remove the old one
		destroyAllFlammableTypes();
		return 1;
	}
	printf("Index: %d", index);
	new Float:X, Float:Y, Float:Z;
	new VW, Interior;
	GetPlayerPos(playerid, X, Y, Z);
	Interior = GetPlayerInterior(playerid);
	VW = GetPlayerVirtualWorld(playerid);
	FlammableInfo[index][FlammableX] = X;
	FlammableInfo[index][FlammableY] = Y;
	FlammableInfo[index][FlammableZ] = Z;
	FlammableInfo[index][pFlammableType] = FlamType;
	FlammableInfo[index][pFlammableTime] = gettime();
	FlammableInfo[index][FlammableInt] = Interior;
	FlammableInfo[index][FlammableVW] = VW;
	FlammableInfo[index][pFlammableState] = getStateByFlamType(FlamType); //We don't really need flag values for now
	tryCreateFire(index, FlamType);
	format(FlammableInfo[index][pOwner],MAX_PLAYER_NAME,"%s",GetPlayerNameEx(playerid, ENameType_RPName));
	FlammableInfo[index][pOwnerSQLID] = GetPVarInt(playerid, "CharID");
	format(FlammableInfo[index][pFlammableDesc], MAX_FLAMMABLE_DESC, "%s", getFlammableNameByType(FlamType));
	FlammableInfo[index][FlammableTextLabel] = CreateDynamic3DTextLabel(FlammableInfo[index][pFlammableDesc], 0x2BFF00AA, X, Y, Z-1.0, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, FlammableInfo[index][FlammableVW],FlammableInfo[index][FlammableInt],-1,FLAMMABLE_STREAM_DIST);
	if(EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter) {
		format(msg, sizeof(msg), "Flammable ID: %d",index);
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	}
	return 1;
}
tryCreateFire(index, FlammableType:FlamType) {
	if(FlamType == Flammable_Molotov) {
		createFire(index); //Create fire at that index
	}
	new cIndex = findCloseFlammable(index, 10.0); //Find a close flammable to the specific index that was sent
	if(cIndex == -1) {
		return 1; //Couldn't find any indexes, halt now!
	}
	switch(FlammableInfo[cIndex][pFlammableState]) { //Do a switch based on the state of the found object
		case State_Liquid: { //If it's a liquid.. (the object on the ground, which is the found object)
			cIndex = findCloseFlamByState(cIndex, State_Fire, 10.0); //Find fire near that index
			if(cIndex != -1) {
				createFire(index); //Create fire at that index
			}
		}
		case State_Fire: { //If it's fire.. (the object on the ground, which is the found object)
			cIndex = findCloseFlamByState(cIndex, State_Liquid, 10.0); //Find a liquid near that index
			if(cIndex != -1) {
				createFire(index); //Create fire at that index
			}
		}
	}
	return 1;
}
createFire(index) {
	if(FlammableInfo[index][FlammableObjID] != 0) {
		#if debug
		printf("Debug: Called createFire->DestroyDynamicObject.");
		#endif
		DestroyDynamicObject(FlammableInfo[index][FlammableObjID]); //Delete it first so it doesn't get overriden, who knows what could happen, all these loops..
	}
	FlammableInfo[index][FlammableObjID] = CreateDynamicObject(MDL_FIRE,FlammableInfo[index][FlammableX], FlammableInfo[index][FlammableY],FlammableInfo[index][FlammableZ]-2.5, 0.0, 0.0, 0.0,FlammableInfo[index][FlammableVW],FlammableInfo[index][FlammableInt]);
	FlammableInfo[index][pFlammableState] = State_Fire;
	FlammableInfo[index][pFlammableType] = Flammable_Fire;
	format(FlammableInfo[index][pFlammableDesc], MAX_FLAMMABLE_DESC, "%s", getFlammableNameByType(FlammableInfo[index][pFlammableType]));
	UpdateDynamic3DTextLabelText(FlammableInfo[index][FlammableTextLabel],0xFFFFFFFF, FlammableInfo[index][pFlammableDesc]);
}
isOnFire(index) {
	return (FlammableInfo[index][pFlammableState]) == getStateByFlamType(Flammable_Fire) ? 1 : 0;
}
getStateByFlamType(FlammableType:FlamType) {
	for(new i=0; i<sizeof(FlammableStates); i++) {
		if(FlamType == FlammableStates[i][pFlammableType]) {
			return FlammableStates[i][pState];
		}
	}
	return -1;
}
stock getFlammableNameByType(FlammableType:FlamType) {
	new name[32];
	format(name,sizeof(name),"{FFFFFF}%s", FlammableName[_:FlamType]);
	return name;
}
findFreeFlammableSlot() {
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		if(FlammableInfo[i][pOwnerSQLID] == 0) {
			return i;
		}
	}
	return -1;
}
getFlammableAmount(justonce = 0) {
	new j = 0;
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		if(FlammableInfo[i][pFlammableState] != State_None) {
			j++;
			if(justonce) break; //Because we don't wanna run the loop like 500 times every 500 ms
		}
	}
	return j;
}
findOldFlammableTypes() {
	new time = gettime();
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		if((time-FlammableInfo[i][pFlammableTime]) >= MAX_FLAMMABLE_TIME) {
			return i;
		}
	}
	return -1;
}
destroyAllFlammableTypes() {
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		destroyFlammable(i);
	}
}
destroyFlammable(index) {
	FlammableInfo[index][pOwnerSQLID] = 0;
	FlammableInfo[index][pFlammableTime] = 0;
	DestroyDynamic3DTextLabel(FlammableInfo[index][FlammableTextLabel]);
	FlammableInfo[index][FlammableTextLabel] = Text3D:0;
	FlammableInfo[index][pFlammableState] = State_None;
	#if debug
	printf("Destroyed flammable %d and object %d.", index, FlammableInfo[index][FlammableObjID]);
	#endif
	if(FlammableInfo[index][FlammableObjID] != 0) {
		DestroyDynamicObject(FlammableInfo[index][FlammableObjID]);
	}
	return 1;
}
findCloseFlammable(index, Float:flrange = 10.0) {
	new Float:range;
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		range = GetPointDistance(FlammableInfo[index][FlammableX], FlammableInfo[index][FlammableY], FlammableInfo[index][FlammableZ], FlammableInfo[i][FlammableX], FlammableInfo[i][FlammableY], FlammableInfo[i][FlammableZ]);
		if(i != index) { //Make sure i is not equal to the index
			if(range < flrange) {
				return i;
			}
		}
	}
	return -1;
}
stock Float:distanceBtwnFlammables(flamIndex1, flamIndex2) {
	new Float:range;
	range = GetPointDistance(FlammableInfo[flamIndex1][FlammableX], FlammableInfo[flamIndex1][FlammableY], FlammableInfo[flamIndex1][FlammableZ], FlammableInfo[flamIndex2][FlammableX], FlammableInfo[flamIndex2][FlammableY], FlammableInfo[flamIndex2][FlammableZ]);
	if(range > 0.0) {
		return range;
	}
	return 0.0;
}
getClosestFlammableID(playerid, Float:range = 5.0) {
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		if(IsPlayerInRangeOfPoint(playerid, range, FlammableInfo[i][FlammableX], FlammableInfo[i][FlammableY], FlammableInfo[i][FlammableZ])) {
			if(FlammableInfo[i][pOwnerSQLID] != 0) {
				return i;
			}
		}
	}
	return -1;
}
findCloseFlamByType(index, FlammableType:FlamType, Float:range = 5.0) {
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(distanceBtwnFlammables(index, i) < range) {
			if(FlamType == FlammableInfo[i][pFlammableType]) {
				return i;
			}
		}
	}
	return -1;
}
findCloseFlamByState(index, FlamState, Float:range = 10.0) {
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(distanceBtwnFlammables(index, i) < range) {
			if(FlamState == FlammableInfo[i][pFlammableState]) {
				return i;
			}
		}
	}
	return -1;
}
getFlammableIndexAtXYZ(Float: X, Float: Y, Float: Z, Float: range = 5.0) {
	new Float:xrange;
	for(new i=0;i<sizeof(FlammableInfo);i++) {
		xrange = GetPointDistance(X, Y, Z, FlammableInfo[i][FlammableX], FlammableInfo[i][FlammableY], FlammableInfo[i][FlammableZ]);
		if(xrange < range) {
			return i;
		}
	}
	return -1;
}
getFlamIndexAtXYZByState(Float: X, Float: Y, Float: Z, FlamState, Float:range = 5.0) {
	new Float:xrange;
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(FlamState == FlammableInfo[i][pFlammableState]) {
			xrange = GetPointDistance(X, Y, Z, FlammableInfo[i][FlammableX], FlammableInfo[i][FlammableY], FlammableInfo[i][FlammableZ]);
			if(xrange < range) {
				return i;
			}
		}
	}
	return -1;
}
FireThink() {
	thinkFireDistance();
	thinkFireDeath();
	thinkPlayersBurn();
	thinkBurnEvidence();
	return 1;
}
thinkBurnEvidence() {
	new evIndex;
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(isOnFire(i)) {
			evIndex = getEvidenceIndexAtXYZ(FlammableInfo[i][FlammableX], FlammableInfo[i][FlammableY], FlammableInfo[i][FlammableZ]); //Within the 5.0 range
			if(evIndex != -1) {
				if(EvidenceInfo[evIndex][pEvidenceType] != Evidence_Ammo && EvidenceInfo[evIndex][pEvidenceType] != Evidence_Body && EvidenceInfo[evIndex][pEvidenceType] != Evidence_BodyBag) { //Burn it if it's not either ammo or a body
					destroyEvidence(evIndex);
				} else {
					if(EvidenceInfo[evIndex][pEvidenceType] == Evidence_Body) {
						evidenceBurnBody(evIndex);
					}
				}
			}
		}
	}
}
thinkPlayersBurn() {
	new cFlammable, Float:health;
	foreach(Player, i) {
		if(IsPlayerConnectEx(i)) {
			cFlammable = getClosestFlammableID(i, 1.0);
			if(cFlammable != -1) {
				if(isOnFire(cFlammable)) {
					if(FlammableInfo[i][pFlammableType] != Flammable_Match) { //If the thing on fire is not a match then burn them
						GetPlayerHealth(i, health);
						SetPlayerHealth(i, health-5.0);
					}
				}
			}
		}
	}
}
thinkFireDistance() {
	new time = gettime();
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(FlammableInfo[i][pOwnerSQLID] != 0) {
			if(FlammableInfo[i][pFlammableTime] > 0) {
				if((time-FlammableInfo[i][pFlammableTime]) < MAX_FLAMMABLE_TIME) {
					tryCreateFire(i, FlammableInfo[i][pFlammableType]);
				}
			}
		}
	}
}
thinkFireDeath() {
	new time = gettime();
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(isOnFire(i)) {
			if(FlammableInfo[i][pOwnerSQLID] != 0) {
				if((time-FlammableInfo[i][pFlammableTime]) >= MAX_FIRE_TIME) {
					if(findCloseFlamByType(i, Flammable_Wood) == -1) {
						destroyFlammable(i);
					} else {
						checkWoodAndFire(i); //WIP
					}
				}
				if((time-FlammableInfo[i][pFlammableTime]) >= MAX_MATCH_TIME) {
					if(FlammableInfo[i][pFlammableType] == Flammable_Match) {
						changeFlammableState(i, State_Burnt);
					}
				}
			}
		}
	}
	return 1;
}
changeFlammableState(index, FlamState) {
	FlammableInfo[index][pFlammableState] = FlamState;
	if(FlamState == State_Burnt) {
		format(FlammableInfo[index][pFlammableDesc], MAX_FLAMMABLE_DESC, "%s (Burnt)", getFlammableNameByType(FlammableInfo[index][pFlammableType]));
		UpdateDynamic3DTextLabelText(FlammableInfo[index][FlammableTextLabel], X11_BLACK, FlammableInfo[index][pFlammableDesc]);
	}
}
checkWoodAndFire(index) {
	new time = gettime();
	if(isOnFire(index)) {
		if(FlammableInfo[index][pOwnerSQLID] != 0) {
			if((time-FlammableInfo[index][pFlammableTime]) >= MAX_BONFIRE_TIME) {
				destroyFlammable(index);
			}
		}
	}
	return 1;
}
FlammablesThink() {
	new time = gettime();
	for(new i=0;i<sizeof(FlammableInfo); i++) {
		if(!isOnFire(i)) {
			if(FlammableInfo[i][pOwnerSQLID] != 0) {
				if((time-FlammableInfo[i][pFlammableTime]) >= floatround(MAX_FLAMMABLE_TIME/(servtemperature < 1 ? 1 : servtemperature))) {
					destroyFlammable(i);
				}
			}
		}
	}
}
giveOrRemoveGasCan(playerid) {
	if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE) {
		removeGasCanFromPlayer(playerid);
	} else {
		attachGasCanToPlayer(playerid);
	}
}
attachGasCanToPlayer(playerid) {
	if(IsPlayerAttachedObjectSlotUsed(playerid, 9)) {
		toggleAccessorySlot(playerid, 9);
	} else {
		//SetPlayerAttachedObject(playerid, 9, POLICE_SHIELD, BONE_RARM, 0.3, 0, 0, 0, 170, 270, 1, 1, 1);
		SetPlayerAttachedObject(playerid, 9, MDL_GASCAN, BONE_RHAND, 0.0, 0.0,  0.0,  0.0, 0.0, 0.0);
	}
}
removeGasCanFromPlayer(playerid) {
	RemovePlayerAttachedObject(playerid, 9);
	if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE) {
		if(GetPVarInt(playerid, "SpillingFuel") == 1) {
			toggleAccessorySlot(playerid, 9);
		}
	}
	return 1;
}
flammableOnPlayerDisconnect(playerid, reason) { //Nothing to write in here yet..
	#pragma unused reason
	DeletePVar(playerid, "SpillingFuel");
	DeletePVar(playerid, "LightingMatches");
	removeGasCanFromPlayer(playerid);
	return 1;
}
/* Commands */
YCMD:spillfuel(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to spill fuel.");
		return 1;
	}
	if(GetPVarInt(playerid, "GasCans") < 1) {
		ShowScriptMessage(playerid, "You don't have any ~r~gas cans~w~, you can buy one at any 24/7.");
		return 1;
	}
	if(GetPVarType(playerid, "LightingMatches") != PLAYER_VARTYPE_NONE) {
		ShowScriptMessage(playerid, "You ~r~can't ~w~light a ~r~match ~w~and spill ~r~fuel ~w~at the same time!", 6000);
		return 1;
	}
	giveOrRemoveGasCan(playerid); //Give them the object
	if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE) {
		ShowScriptMessage(playerid, "You've stopped spilling fuel.", 6000);
		DeletePVar(playerid, "SpillingFuel");
	} else {
		ShowScriptMessage(playerid, "You're now holding a can of ~r~gas~w~, press ~r~~k~~PED_FIREWEAPON~~w~ to spill fuel.", 6000);
		SetPVarInt(playerid, "SpillingFuel", 1);
	}
	return 1;
}
YCMD:lightmatch(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Lights up a match.");
		return 1;
	}
	if(GetPVarInt(playerid, "Matches") < 1) {
		ShowScriptMessage(playerid, "You don't have any ~r~matches~w~, you can buy them at any 24/7.");
		return 1;
	}
	if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE) {
		ShowScriptMessage(playerid, "You ~r~can't ~w~spill ~r~fuel ~w~and light a ~r~match ~w~at the same time!", 6000);
		return 1;
	}
	if(GetPVarType(playerid, "LightingMatches") != PLAYER_VARTYPE_NONE) {
		ShowScriptMessage(playerid, "You've put the matches back in your pocket.", 6000);
		DeletePVar(playerid, "LightingMatches");
	} else {
		ShowScriptMessage(playerid, "You're now holding a box of ~r~matches~w~, press ~r~~k~~PED_FIREWEAPON~~w~ to light one up.", 6000);
		SetPVarInt(playerid, "LightingMatches", 1);
	}
	return 1;
}
YCMD:deleteallflammables(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Used to delete all flammables");
		return 1;
	}
	destroyAllFlammableTypes();
	new msg[128];
	format(msg, sizeof(msg), "AdmWarn: %s has deleted all flammables.", GetPlayerNameEx(playerid, ENameType_AccountName));
	ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
	return 1;
}
/* Key Combinations / Keys */
flammableOnPlayerKeyState(playerid, newkeys, oldkeys) {
	#pragma unused oldkeys
	#if debug
	printf("flammableSystemOnPlayerKeyState(%d, %d, %d)", playerid, newkeys, oldkeys);
	#endif
	flammableSystemPredict(playerid, newkeys, oldkeys);
	if(GetPlayerWeaponEx(playerid) != 0) {
		if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE || GetPVarType(playerid, "LightingMatches") != PLAYER_VARTYPE_NONE) {
			ShowScriptMessage(playerid, "You can't perform this action while holding a weapon!");
			return 1;
		}
	}
	if(GetPVarType(playerid, "SpillingFuel") != PLAYER_VARTYPE_NONE) {
		if(newkeys & KEY_FIRE) {
			if(GetPVarInt(playerid, "GasCans") > 0) {
				SetPVarInt(playerid, "GasCans", GetPVarInt(playerid, "GasCans") -1);
				spillFlammable(playerid, Flammable_Fuel);
				ShowScriptMessage(playerid, "You've ~r~spilled~w~ some ~r~gas~w~ on the ground.", 6000);
			} else {
				ShowScriptMessage(playerid, "You've ran out of ~r~gas~w~!", 6000);
				DeletePVar(playerid, "SpillingFuel");
				removeGasCanFromPlayer(playerid);
			}
		}
	} 
	if(GetPVarType(playerid, "LightingMatches") != PLAYER_VARTYPE_NONE) {
		if(newkeys & KEY_FIRE) {
			if(GetPVarInt(playerid, "Matches") > 0) {
				SetPVarInt(playerid, "Matches", GetPVarInt(playerid, "Matches") -1);
				spillFlammable(playerid, Flammable_Match);
				ShowScriptMessage(playerid, "You've ~r~lighted up~w~ a ~r~match ~w~and dropped it on the ground.", 6000);
			} else {
				ShowScriptMessage(playerid, "You've ran out of ~r~matches~w~!", 6000);
				DeletePVar(playerid, "LightingMatches");
			}
		}
	}
	return 1;
}
/* Extinguish & Other Funcs */
flammableSystemPredict(shooterid, newkeys, oldkeys) {
	#pragma unused oldkeys
	new Float:fPX, Float:fPY, Float:fPZ, Float:fVX, Float:fVY, Float:fVZ, Float:object_x, Float:object_y, Float:object_z;
	const Float:fScale = 3.0;

	GetPlayerCameraPos(shooterid, fPX, fPY, fPZ);
	GetPlayerCameraFrontVector(shooterid, fVX, fVY, fVZ);

	object_x = fPX + floatmul(fVX, fScale);
	object_y = fPY + floatmul(fVY, fScale);
	object_z = fPZ + floatmul(fVZ, fScale);

	if(newkeys & KEY_FIRE) {
		switch(GetPlayerWeaponEx(shooterid)) {
			case 42: { //Fire extinguisher
				tryExtinguishFire(shooterid, object_x, object_y, object_z);
			}
			/*
			case 18: { //Molotov
				spillFlammable(shooterid, Flammable_Molotov);
			}
			*/
		}
	}
	return 1;
}
tryExtinguishFire(shooterid, Float:fx, Float:fy, Float:fz) {
	new flamIndex = getFlamIndexAtXYZByState(fx, fy, fz, State_Fire, 3.0);
	if(IsPlayerConnectEx(shooterid)) {
		if(flamIndex != -1) {
			#if debug
			printf("Destroyed flammable at %d", flamIndex);
			SetPlayerCheckpoint(shooterid, fx, fy, fz, 3.0);
			#endif
			ShowScriptMessage(shooterid, "Fire ~g~extinguished~w~!", 6000);
			destroyFlammable(flamIndex);
		}
	}
}
/* Tasks */
task FModuleThink[500] () {
	if(getFlammableAmount(1)) { //Greater than 0 then run the tasks, otherwise there's no point (Optimization purposes)
		FireThink();
		FlammablesThink();
	}
}