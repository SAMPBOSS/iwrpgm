
#define VIP_PICKUP_COOLDOWN 3
#define DP_IG_VALUE 50000
#define DONATOR_MONTH 2592000
#define COOKIE_DP_VALUE 1 //1 cookie = n dps
enum {
	EVIPDialog_BuyGun = EVIPShopDialog_Base + 1,
	EVIPDialog_BuyVehicle,
	EVIPDialog_SpecialMenu,
	EVIPDialog_NameChange,
	EVIPDialog_AccountChange,
	EVIPDialog_NumberChange,
	EVIPDialog_CookieToDPs
};

new VIPPickups[4];
new Text3D:VIPAccessoriesTextLabel;
new Text3D:CarToyTextLabel;

#pragma unused VIPAccessoriesTextLabel
#pragma unused CarToyTextLabel

enum VIPVehicleInfo {
	EVIPCarModelID,
	EVIPCarPrice,
};

new VIPVehicles[][VIPVehicleInfo] = {{562, 250000}, {429, 900000}, {541, 1200000}, {415, 850000}, {521, 50000}, {468, 30000}, {481, 550}, {510, 450}};

enum EVIPMenuState {
	EVIPS_CurPage,
	EVIPS_RemainingCount,
	EVIPS_PageCount,
	EVIPS_InPreviewMode,
};
new VIPMenuState[MAX_PLAYERS][EVIPMenuState];

enum VIPSpecialInfo {
	EVIPSpecialName[32],
	EVIPSpecialCallback[32],
	EVIPSpecialPrice,
};

/*VIP number - 20 DP*/

new VIPSpecialItems[][VIPSpecialInfo] = {
	{"Name Change","OnVIPSelectNameChange",20},
	{"Account Name Change","OnVIPSelectAccNameChange",30},
	{"VIP Phone Number","OnVIPSelectPhoneNumber",-1},
	{"Cookies to Donator Points","OnVIPCookieSelect",0},
	{"Increase Car Slots","OnVIPInCarSlots",50}};

VIPOnGameModeInit() {
	VIPPickups[0] = CreateDynamicPickup(1314, 16, 1237.88, -1668.57, 11.80); //VIP name change, etc
	VIPPickups[1] = CreateDynamicPickup(1239, 16, 1218.19, -1648.68, 11.80); //VIP cars
	VIPPickups[2] = CreateDynamicPickup(1239, 16, 1250.047851, -1671.093505, 12.723744); //VIP Accessories
	VIPPickups[3] = CreateDynamicPickup(1239, 16, 1264.86, -1669.73, 13.54); //VIO Car Toys
	VIPAccessoriesTextLabel = CreateDynamic3DTextLabel("(( VIP accessories ))", 0x2BB00AA, 1250.047851, -1671.093505, 12.723744+1, 50.0); //VIP Accessories Label
	CarToyTextLabel = CreateDynamic3DTextLabel("(( /buycartoy ))", 0x2BB00AA, 1264.86, -1669.73, 13.54+1, 50.0);//Car Toys
	
}
VIPOnPickupPickup(playerid, pickupid) {
	/*
	new time = GetPVarInt(playerid, "VIPPickupCooldown");
	new timenow = gettime();
	if(VIP_PICKUP_COOLDOWN-(timenow-time) > 0) {
		return 0;
	}
	SetPVarInt(playerid, "VIPPickupCooldown", timenow);
	*/
	if(pickupid == VIPPickups[0]) {
		showGovVIPMenu(playerid);
	} else if(pickupid == VIPPickups[1]) {
		showVIPVehicleMenu(playerid);
	} else if(pickupid == VIPPickups[2]) {
		showAccessoriesDialog(playerid, 1);
	} else {
		return 0;
	}
	movePlayerBack(playerid, 3.0);
	return 1;
}
/*
YCMD:givedps(playerid, params[], help ) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Gives your DPs away");
		return 1;
	}
	new amount, target, tdps;
	new msg[128];
	if(!sscanf(params, "k<playerLookup_acc>d", target, amount)) {
		tdps = GetPVarInt(playerid, "DonatePoints");
		if(!IsPlayerConnectEx(target)) {
			SendClientMessage(playerid, X11_TOMATO_2, "User not found");
			return 1;
		}
		if(amount <= 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		if(tdps - amount < 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough donator points");
			return 1;
		}
		SetPVarInt(playerid, "DonatePoints", tdps - amount);
		tdps = GetPVarInt(target, "DonatePoints");
		tdps += amount;
		SetPVarInt(target, "DonatePoints", tdps);
		format(msg, sizeof(msg), "AdmWarn: %s has given %s %s Donator Points",GetPlayerNameEx(playerid, ENameType_CharName) ,GetPlayerNameEx(target, ENameType_CharName), getNumberString(amount));
		ABroadcast(X11_YELLOW, msg, EAdminFlags_All);
		format(msg, sizeof(msg), "* You have been given %s donator points by %s", getNumberString(amount),GetPlayerNameEx(playerid, ENameType_CharName));
		SendClientMessage(target, X11_ORANGE, msg);
		format(msg, sizeof(msg), "* You have given %s donator points to %s", getNumberString(amount), GetPlayerNameEx(target, ENameType_CharName));
		SendClientMessage(playerid, X11_ORANGE, msg);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /givedps [playerid/name] [amount]");
	}
	return 1;
}
*/
showVIPVehicleMenu(playerid) {
	new vlist[sizeof(VIPVehicles)];
	for(new i=0;i<sizeof(VIPVehicles);i++) {
		vlist[i] = VIPVehicles[i][EVIPCarModelID];
	}
	setupVIPMenuState(playerid, 1);
}
vipOnPlayerLogin(playerid) {
	new time = gettime();
	new msg[128], donaterank;
	donaterank = GetPVarInt(playerid, "DonateRank");
	if(donaterank >= 5) { //5 = Bronze
		if(time > GetPVarInt(playerid, "DonatorExpTime")) {
			new ELicenseFlags:lflags = ELicenseFlags:GetPVarInt(playerid, "LicenseFlags");
			if(lflags & ELicense_IsTempGunLic) {
				lflags &= ~ELicense_Gun;
				lflags &= ~ELicense_IsTempGunLic;
			}
			format(msg, sizeof(msg), "Your %s VIP rank and your gun license have expired, setting you back to regular VIP.", GetDonateRank(donaterank));
			SetPVarInt(playerid, "DonateRank", 2);
			SetPVarInt(playerid ,"LicenseFlags", _:lflags);
			SendClientMessage(playerid, COLOR_CUSTOMGOLD, msg);
		}
	}
}
giveDonatorPackage(playerid, user, package) {
	new actualdps;
	actualdps = GetPVarInt(user, "DonatePoints");
	new totaltime = gettime() + DONATOR_MONTH;
	new msg[128];
	query[0] = 0;//[128];
	switch(package) {
		case 5: { //Bronze
			SetPVarInt(user, "NameChanges", 1);
			SetPVarInt(user, "DonatePoints", actualdps + 50);
		} 
		case 6: { //Silver
			SetPVarInt(user, "NameChanges", 2);
			SetPVarInt(user, "PhoneChanges", 1);
			SetPVarInt(user, "DonatePoints", actualdps + 200);
		} 
		case 7: { //Gold
			SetPVarInt(user, "NameChanges", 4);
			SetPVarInt(user, "PhoneChanges", 2);
			SetPVarInt(user, "DonatePoints", actualdps + 300);
		} 
	}
	if(!tryGiveGunLicense(user, 1)) {
		SendClientMessage(playerid, X11_TOMATO_2, "[Notice] VIP: This person already has a gun license.");
	}
	new EAccountFlags:aflags = EAccountFlags:GetPVarInt(user, "AccountFlags");
	aflags &= ~EAccountFlags_IsBikeRestricted;
	SetPVarInt(user, "AccountFlags", _:aflags);	//Give bike permissions when they get their package
	SetPVarInt(user, "DonateRank", package);
	SetPVarInt(user, "DonatorExpTime", totaltime);
	format(query, sizeof(query), "UPDATE `accounts` SET `donatorexptime` = FROM_UNIXTIME(%d), `namechanges` = %d, `phonechanges` = %d WHERE `id` = %d", totaltime, GetPVarInt(user, "NameChanges"),GetPVarInt(user, "PhoneChanges"),GetPVarInt(user, "AccountID"));
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback", "");
	format(msg, sizeof(msg), "AdmWarn: %s has given %s a %s donator package.",GetPlayerNameEx(playerid, ENameType_AccountName),GetPlayerNameEx(user, ENameType_CharName),GetDonateRank(package));
	ABroadcast(X11_YELLOW, msg, EAdminFlags_Donations);
}
VIPOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	new DPs = GetPVarInt(playerid, "DonatePoints");
	new msg[128];
	if(!response) {
		return 1;
	}
	switch(dialogid) {
		case EVIPDialog_SpecialMenu: {
			CallLocalFunction(VIPSpecialItems[listitem][EVIPSpecialCallback],"dd", playerid, listitem);
			return 1;
		}
		case EVIPDialog_NameChange: {
			if(NameIsRP(inputtext) && !IsNameRestricted(inputtext) && strlen(inputtext) < MAX_PLAYER_NAME) {
				IsNameTaken(inputtext, "OnVIPNameTakenCheck", playerid);
			} else {
				ShowPlayerDialog(playerid, EVIPDialog_NameChange, DIALOG_STYLE_INPUT, "VIP Name Change","{FF0000}Warning: {FFFFFF}This name is NOT RP! you must enter an RP Name!\nEnter the name you would like to use","Buy", "Cancel");
			}
		}
		case EVIPDialog_AccountChange: {
			IsAccountTaken(inputtext, "OnVIPAccountTaken", playerid);
		}
		case EVIPDialog_NumberChange: {
			IsPhoneNumberTaken(strval(inputtext), "OnVIPNumberTaken", playerid);
		}
		case EVIPDialog_CookieToDPs: {
			new total = strval(inputtext);
			if(total < 0 || total > 200) {
				SendClientMessage(playerid, X11_TOMATO_2, "Invalid Number, enter something above 0, and below 200.");
				return 1;
			}
			new cookies = GetPVarInt(playerid, "Cookies");
			new value = total * COOKIE_DP_VALUE;
			if(cookies < total) {
				SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough cookies!");
				return 1;
			}
			cookies -= total;
			SetPVarInt(playerid, "Cookies", cookies);
			DPs += value;
			format(msg, sizeof(msg), "* You bought %s DPs for %s cookies",getNumberString(total),getNumberString(total));
			SendClientMessage(playerid, X11_YELLOW, msg);
		}
	}
	SetPVarInt(playerid, "DonatePoints", DPs);
	return 1;
}
showGovVIPMenu(playerid) {
	dialogstr[0] = 0;
	tempstr[0] = 0;
	for(new i=0;i<sizeof(VIPSpecialItems);i++) {
		if(VIPSpecialItems[i][EVIPSpecialPrice] != -1) {
			format(tempstr,sizeof(tempstr),"%s - $%.02f(%d DP)\n",VIPSpecialItems[i][EVIPSpecialName],VIPSpecialItems[i][EVIPSpecialPrice]*DP_VALUE,VIPSpecialItems[i][EVIPSpecialPrice]);
		} else {
			format(tempstr,sizeof(tempstr),"%s - N/A\n",VIPSpecialItems[i][EVIPSpecialName]);
		}
		strcat(dialogstr,tempstr,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EVIPDialog_SpecialMenu, DIALOG_STYLE_LIST, "VIP Menu",dialogstr,"Buy", "Cancel");
}
forward OnVIPSelectNameChange(playerid, index);
public OnVIPSelectNameChange(playerid,index) {
	ShowPlayerDialog(playerid, EVIPDialog_NameChange, DIALOG_STYLE_INPUT, "VIP Name Change","{FFFFFF}Enter the name you would like to use","Buy", "Cancel");
}
forward OnVIPSelectAccNameChange(playerid, index);
public OnVIPSelectAccNameChange(playerid, index) {
	SetPVarInt(playerid, "DonateChargePrice", VIPSpecialItems[index][EVIPSpecialPrice]);
	ShowPlayerDialog(playerid, EVIPDialog_AccountChange, DIALOG_STYLE_INPUT, "VIP Account Name Change","{FFFFFF}Enter the account name you would like to use","Buy", "Cancel");	
}

forward OnVIPCookieSelect(playerid, index);
public OnVIPCookieSelect(playerid, index) {
	ShowPlayerDialog(playerid, EVIPDialog_CookieToDPs, DIALOG_STYLE_INPUT, "VIP Cookie to DPs","{FFFFFF}Enter how many cookies you would like to sell.","Sell", "Cancel");	
}
forward OnVIPInCarSlots(playerid, index);
public OnVIPInCarSlots(playerid, index) {
	new msg[128];
	new price = VIPSpecialItems[index][EVIPSpecialPrice];
	new dps = GetPVarInt(playerid, "DonatePoints");
	new carslots = GetPVarInt(playerid, "MaxCars");
	if(dps < price) {
		format(msg, sizeof(msg), "You do not have enough donator points! You need %d more to buy this!",price-dps);
		SendClientMessage(playerid, X11_TOMATO_2, msg);
		return 1;
	}
	carslots++;
	if(carslots >= 12) {
		SendClientMessage(playerid, X11_TOMATO_2, "You have reached the maximum car slots");
		return 1;
	}
	dps -= price;
	SetPVarInt(playerid, "DonatePoints", dps);
	SetPVarInt(playerid, "MaxCars", carslots);
	SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations, you bought an additional car slot!");
	return 1;
}

forward OnVIPSelectPhoneNumber(playerid, index);
public OnVIPSelectPhoneNumber(playerid, index) {
	ShowPlayerDialog(playerid, EVIPDialog_NumberChange, DIALOG_STYLE_INPUT, "VIP Phone Number Change","{FFFFFF}Enter the phone number you would like, without any spaces","Buy", "Cancel");	
}
forward OnVIPNameTakenCheck(playerid, name[]);
public OnVIPNameTakenCheck(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	if(strlen(name) > MAX_PLAYER_NAME) {
		ShowPlayerDialog(playerid, EVIPDialog_NameChange, DIALOG_STYLE_INPUT, "VIP Name Change","{FF0000}Warning: {FFFFFF}The name you entered is too long\nEnter the name you would like to use","Buy", "Cancel");
		return 1;
	}
	if(rows > 0) {
		ShowPlayerDialog(playerid, EVIPDialog_NameChange, DIALOG_STYLE_INPUT, "VIP Name Change","{FF0000}Warning: {FFFFFF}This name is taken! You must enter another name!\nEnter the name you would like to use","Buy", "Cancel");
	} else {
		if(GetPVarInt(playerid, "NameChanges") < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not have enough name changes left!");
			return 1;
		}
		format(msg, sizeof(msg), "AdmWarn: %s has changed names to %s via the VIP shop",GetPlayerNameEx(playerid, ENameType_CharName),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_SetName);
		changeName(playerid, name);
		SetPVarInt(playerid, "NameChanges", GetPVarInt(playerid, "NameChanges") - 1);
		format(msg, sizeof(msg), "* Congratulations on your new name! (%d name change(s) left)",GetPVarInt(playerid, "NameChanges"));
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
	}
	return 1;
}
forward OnVIPAccountTaken(playerid, name[]);
public OnVIPAccountTaken(playerid, name[]) {
	new msg[128];
	new rows,fields;
	cache_get_data(rows,fields);
	if(strlen(name) > MAX_PLAYER_NAME) {
		ShowPlayerDialog(playerid, EVIPDialog_AccountChange, DIALOG_STYLE_INPUT, "VIP Account Name Change","{FF0000}Warning: {FFFFFF}The account name you entered is too long\nEnter the name you would like to use","Buy", "Cancel");
		return 1;
	}
	if(rows > 0) {
		ShowPlayerDialog(playerid, EVIPDialog_AccountChange, DIALOG_STYLE_INPUT, "VIP Account Name Change","{FF0000}Warning: {FFFFFF}This account name is taken! You must enter another name!\nEnter the name you would like to use","Buy", "Cancel");
	} else {
		if(GetPVarType(playerid, "DonateChargePrice") != PLAYER_VARTYPE_NONE) {
			new price = GetPVarInt(playerid, "DonateChargePrice");
			new dps = GetPVarInt(playerid, "DonatePoints");
			if(dps < price) {
				format(msg, sizeof(msg), "You do not have enough donator points! You need %d more to buy this!",price-dps);
				SendClientMessage(playerid, X11_TOMATO_2, msg);
				return 1;
			}
			dps -= price;
			SetPVarInt(playerid, "DonatePoints", dps);
			DeletePVar(playerid, "DonateChargePrice");
		}
		format(msg, sizeof(msg), "AdmWarn: %s has changed accountnames to %s via the VIP shop",GetPlayerNameEx(playerid, ENameType_CharName),name);
		ABroadcast(X11_YELLOW, msg, EAdminFlags_SetName);
		changeAccountName(playerid, name);
		SendClientMessage(playerid, COLOR_DARKGREEN, "* Congratulations on your new account name!");
	}
	return 1;
}
forward OnVIPNumberTaken(playerid, number);
public OnVIPNumberTaken(playerid, number) {
	new rows,fields;
	cache_get_data(rows,fields);
	new msg[128];
	if(rows > 0 || number < 0) {
		ShowPlayerDialog(playerid, EVIPDialog_NumberChange, DIALOG_STYLE_INPUT, "VIP Phone Number Change","{FF0000}Warning: {FFFFFF}This phone number is taken!\nEnter the phone number you would like, without any spaces","Buy", "Cancel");
	} else {
		if(GetPVarInt(playerid, "PhoneChanges") < 1) {
			SendClientMessage(playerid, X11_TOMATO_2, "You do not enough name changes left!");
			return 1;
		}
		SetPVarInt(playerid, "PhoneChanges", GetPVarInt(playerid, "PhoneChanges") - 1);
		format(msg, sizeof(msg), "* Congratulations on your new phone number! (%d number change(s) left)",GetPVarInt(playerid, "PhoneChanges"));
		SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		setPhoneNumber(playerid, number);
	}
	return 1;
}
setupVIPMenuState(playerid, curpage = 1) {
	new remaining_amount, pages, numcars;
	new start_index = (curpage-1) * (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);

	new vehstr[32],vehprice[32];

	clearModelSelectMenu(playerid);
	if(start_index < 0) {
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	#define AddVIPCarItemToPreviewer(%1) numcars = sizeof(%1); \
		for(new i=start_index;i<sizeof(%1);i++) { \
			format(vehstr,sizeof(vehstr),"%s",VehiclesName[%1[i][EVIPCarModelID]-400]); \
			format(vehprice,sizeof(vehprice), "~%c~$%s",GetMoneyEx(playerid)>=%1[i][EVIPCarPrice]?'g':'r',getNumberString(%1[i][EVIPCarPrice])); \
			addItemToModelSelectMenu(playerid, %1[i][EVIPCarModelID], vehstr,vehprice); \
		}

	AddVIPCarItemToPreviewer(VIPVehicles)


	remaining_amount = numcars % (MAX_MODEL_SELECT_ROWS * MAX_MODEL_SELECT_COLS);
	pages = floatround(float(numcars) / (float(MAX_MODEL_SELECT_ROWS) * float(MAX_MODEL_SELECT_COLS)), floatround_ceil);

	if(curpage > pages) {
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot change pages any more.");
		return;
	}
	VIPMenuState[playerid][EVIPS_CurPage] = curpage;
	VIPMenuState[playerid][EVIPS_PageCount] = pages;
	VIPMenuState[playerid][EVIPS_RemainingCount] = remaining_amount;
	VIPMenuState[playerid][EVIPS_InPreviewMode] = 0;

	new page_str[32];
	format(page_str,sizeof(page_str), "Page %d of %d",curpage,pages);
	launchModelSelectMenu(playerid, numcars, "VIP Menu", page_str, "OnVIPMenuSelect", -25.0000, 0.0000, -55.2600);
}
forward OnVIPMenuSelect(playerid, action, modelid, title[]);
public OnVIPMenuSelect(playerid, action, modelid, title[]) {
	if(action == EModelPreviewAction_LaunchPreview) {
		if(modelid != -1) {
			clearModelSelectMenu(playerid);
			launchModelPreviewMenu(playerid, modelid, 3, 6);
			VIPMenuState[playerid][EVIPS_InPreviewMode] = modelid;
		}
	} else if(action == EModelPreviewAction_Accept) {
		modelid = VIPMenuState[playerid][EVIPS_InPreviewMode];
		new price = getModelValue(modelid);
		new num = getNumOwnedCars(playerid);
		clearModelSelectMenu(playerid);
		CancelSelectTextDrawEx(playerid);
		if(num >= GetPVarInt(playerid, "MaxCars")) {
			SendClientMessage(playerid, X11_TOMATO_2, "You cannot own any more cars");
			return 0;
		}
		if(GetMoneyEx(playerid) >= price) {
			new msg[128];
			GiveMoneyEx(playerid, -price);
			new Float:X,Float:Y,Float:Z,Float:A;
			GetPlayerPos(playerid, X, Y, Z);
			GetPlayerFacingAngle(playerid, A);
			CreatePlayerCar(playerid, modelid, 0, 0, X, Y, Z, A, ELockType_Remote, 0, 1);
			format(msg, sizeof(msg), "Congratulations! You now own a %s!",VehiclesName[modelid-400]);
			SendClientMessage(playerid, COLOR_DARKGREEN, msg);
		} else {
			SendClientMessage(playerid, X11_RED, "You don't have enough money");
		}
	} else if(action == EModelPreviewAction_Next) {
		setupVIPMenuState(playerid, VIPMenuState[playerid][EVIPS_CurPage] + 1);
	} else if(action == EModelPreviewAction_Back) {
		setupVIPMenuState(playerid, VIPMenuState[playerid][EVIPS_CurPage] - 1);
	} else if(action == EModelPreviewAction_Exit) {
		if(VIPMenuState[playerid][EVIPS_InPreviewMode] != 0) {
			setupVIPMenuState(playerid, VIPMenuState[playerid][EVIPS_CurPage]);
		} else {
			clearModelSelectMenu(playerid);
			CancelSelectTextDrawEx(playerid);
		}
	}
	return 1;
}

stock GetDonateRank(rank) {
	new msg[32];
	switch(rank) {
		case 1: {
			msg = "Beta Tester";
		}
		case 2: {
			msg = "VIP";
		}
		case 3: {
			msg = "Populator";
		}
		case 4: {
			msg = "Trusted Donator";
		}
		case 5: {
			msg = "Bronze";
		}
		case 6: {
			msg = "Silver";
		}
		case 7: {
			msg = "Gold";
		}
		default: {
			msg = "None";
		}
	}
	return msg;
}
stock GetFightStyle(style) {
	new msg[32];
	switch(style) {
		case FIGHT_STYLE_NORMAL: {
			msg = "Normal";
		}
		case FIGHT_STYLE_BOXING: {
			msg = "Boxing";
		}
		case FIGHT_STYLE_KUNGFU: {
			msg = "Kung-Fu";
		}
		case FIGHT_STYLE_KNEEHEAD: {
			msg = "Kneehead";
		}
		case FIGHT_STYLE_GRABKICK: {
			msg = "Grabkick";
		}
		case FIGHT_STYLE_ELBOW: {
			msg = "Elbow";
		}
	}
	return msg;
}
