/*
	-PVARS USED BY THIS SCRIPT
	- ItemEditMode 1 - Edit Price, 2 - Move, 3 - Buy, 4 - Grab
*/
enum EDroppedItemType {
	EType_None, //0
	EType_Gun, //1
	EType_Pot, //2
	EType_Coke, //3
	EType_Meth, //4
	EType_MatsA, //5 
	EType_MatsB, //6
	EType_MatsC, //7 
	EType_Money, //8
	EType_Armour, //9
	EType_Drug, //10
};

enum {
	EItemsDialog_SelectOpt = EItems_Base + 1,
	EItems_SetPrice,
	//EItems_BuyItem,
	EItemsDialog_DoNothing
}

enum ItemFlags (<<= 1) {
	EItemProp_None = 0,
	EItemProp_ForSell = 1, //Can be sold
	EItemProp_GetWet, //Can get wet
	EItemProp_CanBeBurnt, //Can be burnt
};

enum EItemFlagsInfo {
	ItemFlags:EItemFlag, //drug flag
	EDrugFlagDesc[64], //description of the flag
};
new ItemFlagDesc[][EItemFlagsInfo] = 
{
	{EItemProp_None, "None"},
	{EItemProp_ForSell, "Can be sold"},
	{EItemProp_GetWet, "Can get wet"},
	{EItemProp_CanBeBurnt, "Can be burnt"}
};
#pragma unused ItemFlagDesc

enum EDroppableInfo {
	EDropName[32],
	EDropPVarName[32],
	EDroppedItemType:EDropType,
	EDropModelID,
};
new DroppableItems[][EDroppableInfo] = {
										{"Cash", "Money", EType_Money, 1550},
										{"Pot", "Pot", EType_Pot, 1578},
										{"Coke", "Coke", EType_Coke, 1575},
										{"Gun", "Gun", EType_Gun, 1575},
										{"Meth", "Meth", EType_Meth, 1576},
										{"Materials A", "MatsA", EType_MatsA, 2040},
										{"Materials B", "MatsB", EType_MatsB, 2040},
										{"Materials C", "MatsC", EType_MatsC, 2040},
										{"Armour", "Armour", EType_Armour, 373},
										{"Drugs", "Drugs", EType_Drug, 1575}
										};
enum EDroppedItemInfo {
	EDroppedItemType:EDIType,
	eDropperName[MAX_PLAYER_NAME],
	EDIValue,
	EDIObjectID,
	EDISQLID,
	EDIOwner,
	EDIExtraSQLID,
	Float:EDIObjPosX,
	Float:EDIObjPosY,
	Float:EDIObjPosZ,
	Float:EDIObjRotX,
	Float:EDIObjRotY,
	Float:EDIObjRotZ,
	EDIVW,
	EDIInt,
	ItemFlags:EDIFlags,
	EDIPrice,
	Text3D:EDITextLabel,
};
#define MAX_DROPPABLE_ITEMS 2500
new DroppedItems[MAX_DROPPABLE_ITEMS][EDroppedItemInfo];
YCMD:drop(playerid, params[], help) {
	new type[32], amount;
	new msg[128];
	if(IsOnDuty(playerid) || isInPaintball(playerid) || isOnMedicDuty(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this while on duty!");
		return 1;
	}
	if(isPlayerDying(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You cannot do this!");
		return 1;
	}
	if(isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this right now!");
		return 1;
	}
	if(!sscanf(params, "s[32]D(0)",type,amount)) {
		if(amount < 0 || amount > 99999) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Amount!");
			return 1;
		}
		for(new i=0;i<sizeof(DroppableItems);i++) {
			new Float:X, Float:Y, Float:Z, vw, interior;
			GetPlayerPos(playerid, X, Y, Z);
			interior = GetPlayerInterior(playerid);
			vw = GetPlayerVirtualWorld(playerid);
			if(strcmp(type, DroppableItems[i][EDropPVarName], true) == 0) {
				if(DroppableItems[i][EDropType] == EType_Money) {
					if(GetPVarInt(playerid, "Level") < 3) {
						SendClientMessage(playerid, X11_TOMATO_2, "You must be above level 3 to drop this!");
						return 1;
					}
					if(GetMoneyEx(playerid) <= 0) {
						SendClientMessage(playerid, X11_TOMATO_2, "You don't have any money!");
						return 1;
					}
				}
				if(DroppableItems[i][EDropType] == EType_Gun) {
					new gun, ammo;
					gun = GetPlayerWeaponEx(playerid);
					if(gun == 0 || isInPaintball(playerid)) {
						SendClientMessage(playerid, X11_TOMATO_2, "You cannot drop this!");
						return 1;
					}
					new slot = GetWeaponSlot(gun);
					GetPlayerWeaponDataEx(playerid, slot, gun, ammo);
					amount = encodeWeapon(gun, ammo);
					RemovePlayerWeapon(playerid, gun);
					Z -= 1.0;
					dropItem(playerid, DroppableItems[i][EDropType], amount, Float:X, Float:Y, Float:Z, interior, vw);
					new gunname[32];
					GetWeaponNameEx(gun, gunname, sizeof(gunname));
					format(msg, sizeof(msg), "* %s drops a %s on the ground",GetPlayerNameEx(playerid, ENameType_RPName),gunname);
					ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
					return 1;
				}
				if(DroppableItems[i][EDropType] == EType_Drug) {
					drugsShowGiveDrugMenu(playerid, -1, amount, 1);
					return 1;
				}
				new total = GetPVarInt(playerid, DroppableItems[i][EDropPVarName]);
				if(amount == 0) {
					amount = total;
					if(amount > 99999) amount = 99999;
				}
				if((amount > total || amount == 0) && DroppableItems[i][EDropType] != EType_Armour) {
					SendClientMessage(playerid, X11_TOMATO_2, "You don't have enough!");
					return 1;
				}
				total -= amount;
				SetPVarInt(playerid, DroppableItems[i][EDropPVarName], total);
				GiveMoneyEx(playerid, 0);  //sync money
				Z -= 1.0;
				if(DroppableItems[i][EDropType] == EType_Money) { //cash adjust
					Z += 0.5;
				}
				dropItem(playerid, DroppableItems[i][EDropType], amount, X, Y, Z, interior, vw);
				format(msg, sizeof(msg), "* %s drops some %s on the ground",GetPlayerNameEx(playerid, ENameType_RPName),DroppableItems[i][EDropName]);
				ProxMessage(30.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
				HintMessage(playerid, COLOR_DARKGREEN,"Use /pickupitem to pick an item up.");
				return 1;
			}
		}
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /drop [name] [amount]");
	}
	format(msg, sizeof(msg), "Items: ");
	new tempmsg[64];
	new i;
	for(i=0;i<sizeof(DroppableItems);i++) {
		format(tempmsg, sizeof(tempmsg), "%s, ", DroppableItems[i][EDropPVarName]);
		strcat(msg, tempmsg, sizeof(msg));
		if(i%4 == 0 && i != 0) {
			SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
			msg[0] = 0;
		}
	}
	new len = strlen(msg);
	if(i%4 != 0 && len > 2) {
		msg[len-2] = 0;
		SendClientMessage(playerid, COLOR_LIGHTBLUE, msg);
	}
	return 1;
}
YCMD:pickupitem(playerid, params[], help) {
	new i = getStandingDroppedItem(playerid);
	if(i == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item");
		return 1;
	}
	if(DroppedItems[i][EDIPrice] > 0) {
		SendClientMessage(playerid, X11_TOMATO_2, "This item is currently for sale and can't be picked up.");
		return 1;
	}
	playerGetDroppedItem(playerid, i);
	return 1;
}
YCMD:grabitem(playerid, params[], help) {
	SendClientMessage(playerid, X11_WHITE, "Move your mouse over the item you want to grab and click it.");
	SelectObject(playerid);
	SetPVarInt(playerid, "ItemEditMode", 4); //Grab Item
	return 1;
}
YCMD:iteminfo(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Display information on a dropped item");
		return 1;
	}
	new i = getStandingDroppedItem(playerid);
	if(i == -1) {
		SendClientMessage(playerid, X11_TOMATO_2, "You must be standing on an item!");
		return 1;
	}
	new index = getDropTypeIndex(DroppedItems[i][EDIType]);
	format(query, sizeof(query), "* Item Type: %s",DroppableItems[index][EDropName]);
	SendClientMessage(playerid, X11_YELLOW, query);
	format(query, sizeof(query), "* Dropper: %s",DroppedItems[i][eDropperName]);
	SendClientMessage(playerid, X11_YELLOW, query);
	if(DroppedItems[i][EDIType] == EType_Gun) {
		new gun, ammo;
		decodeWeapon(DroppedItems[i][EDIValue], gun, ammo);
		new gunname[32];
		GetWeaponNameEx(gun, gunname, sizeof(gunname));
		format(query, sizeof(query), "* Gun: %s[%d] - %d",gunname,gun,ammo);
	} else {
		format(query, sizeof(query), "* Amount: %d",DroppedItems[i][EDIValue]);
	}
	SendClientMessage(playerid, X11_YELLOW, query);
	format(query, sizeof(query), "* Object ID: %d Item ID: %d",DroppedItems[i][EDIObjectID], i);
	SendClientMessage(playerid, X11_YELLOW, query);
	return 1;
}
YCMD:gotoitem(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Teleport to a dropped item");
		return 1;
	}
	new vws[10],ints[10];
	new item,interior,vw,Float:X,Float:Y,Float:Z;
	if(!sscanf(params, "d", item)) {
		if(item < 0 || item > MAX_DROPPABLE_ITEMS || DroppedItems[item][EDIObjectID] == 0) {
			SendClientMessage(playerid, X11_TOMATO_2, "Invalid Item!");
			return 1;
		}
		Streamer_GetArrayData(STREAMER_TYPE_OBJECT, DroppedItems[item][EDIObjectID], E_STREAMER_WORLD_ID, vws, sizeof(vws));
		Streamer_GetArrayData(STREAMER_TYPE_OBJECT, DroppedItems[item][EDIObjectID], E_STREAMER_INTERIOR_ID, ints, sizeof(ints));
		for(new i=0;i<sizeof(ints);i++) {
			if(vws[i] != 0) {
				vw = vws[i];
			}
			if(ints[i] != 0) {
				interior = ints[i];
			}
		}
		GetDynamicObjectPos(DroppedItems[item][EDIObjectID], X, Y, Z);
		Z += 1.0;
		SetPlayerVirtualWorld(playerid, vw);
		SetPlayerInterior(playerid, interior);
		SetPlayerPos(playerid, X, Y, Z);
		SendClientMessage(playerid, X11_ORANGE, "You have been teleported.");
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /gotoitem [itemid]");
	}
	return 1;
}
YCMD:edititem(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to edit an item");
		return 1;
	}
	ShowPlayerDialog(playerid, EItemsDialog_SelectOpt, DIALOG_STYLE_MSGBOX, "Item Menu:", "Would you like to move this item, or set it for sell?", "Move", "Sell");
	return 1;
}
YCMD:reloaditems(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Reloads the player items.");
		return 1;
	}
	new userid;
	new msg[128];
	if (!sscanf(params, "k<playerLookup_acc>", userid)) {
		if(!IsPlayerConnectEx(userid)) {
			SendClientMessage(playerid, X11_TOMATO_2 ,"User not found.");
			return 1;
		}
		format(msg, sizeof(msg), "AdmWarn: %s has reloaded %s's items.",GetPlayerNameEx(playerid,ENameType_AccountName),GetPlayerNameEx(userid,ENameType_AccountName));
		ABroadcast(X11_YELLOW, msg, EAdminFlags_AdminManage);
		reloadItemsForPlayer(userid);
	} else {
		SendClientMessage(playerid, X11_WHITE, "USAGE: /reloaditems [userid]");
	}
	return 1;
}
/*
YCMD:buyitem(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Allows you to buy an item");
		return 1;
	}
	//ShowPlayerDialog(playerid, EItemsDialog_SelectOpt, DIALOG_STYLE_MSGBOX, "Item Menu:", "Would you like to move this item, or set it for sell?", "Move", "Sell");
	SelectObject(playerid);
	SetPVarInt(playerid, "ItemEditMode", 3); //Buy Item
	return 1;
}
*/
stock itemsGetDropType(index) {
	new xret[64];
	index = getDropTypeIndex(DroppedItems[index][EDIType]);
	if(index != -1) {
		strcpy(xret, DroppableItems[index][EDropName]);
	}
	return strlen(xret) != 0 ? (xret) : ("None");
}
itemsOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused listitem
	//new msg[128];
	dialogstr[0] = 0;
	switch(dialogid) {
		case EItemsDialog_SelectOpt: {
			if(response) {
				SendClientMessage(playerid, X11_WHITE, "Click on an item to edit its position. Press ESC to exit edit mode.");
				SetPVarInt(playerid, "ItemEditMode", 2); //Move
			} else {
				SendClientMessage(playerid, X11_WHITE, "Click on an item to set it for sale. Press ESC to exit.");
				SetPVarInt(playerid, "ItemEditMode", 1); //Edit price
			}
			SelectObject(playerid);
		}
		case EItems_SetPrice: {
			if(!response) {
				SendClientMessage(playerid, X11_WHITE, "You've canceled the edit.");
				DeletePVar(playerid, "ItemTempIndex");
				DeletePVar(playerid, "ItemEditMode");
				return 1;
			}
			new index = GetPVarInt(playerid, "ItemTempIndex");
			new price = strval(inputtext);
			if(DroppedItems[index][EDIType] == EType_Money) {
				SendClientMessage(playerid, X11_TOMATO_2, "You can't put money for sale");
				DeletePVar(playerid, "ItemTempIndex");
				return 1;
			}
			DeletePVar(playerid, "ItemTempIndex");
			setItemPrice(price, index);
			SendClientMessage(playerid, X11_WHITE, "Price set!");
		}
		/*
		case EItems_BuyItem: {
			if(!response) {
				SendClientMessage(playerid, X11_WHITE, "You closed the buy menu.");
				DeletePVar(playerid, "ItemTempIndex");
				DeletePVar(playerid, "ItemEditMode");
				return 1;
			}
			new index = GetPVarInt(playerid, "ItemTempIndex");
			if(DroppedItems[index][EDropType] == EType_Drug) {
				pvarid = getUsableDrugSpot(playerid, DroppedItems[index][EDIExtraSQLID], STATE_ANYTHING);
				if(pvarid != -1) {
					playerGetDroppedItem(playerid, index);
					return 1;
				}
			}
			playerGetDroppedItem(playerid, index);
		}
		*/
	}
	return 1;
}

itemsOnPlayerSelectObject(playerid, objectid, modelid, Float:x, Float:y, Float:z) {
	#pragma unused x
	#pragma unused y
	#pragma unused z
	#pragma unused modelid
	new msg[128];
	new index = findItemByObjID(objectid);
	if(index != -1) {
		new aoverride = (EAdminFlags:GetPVarInt(playerid, "AdminFlags") & EAdminFlags_Scripter)?1:0;
		if(DroppedItems[index][EDIOwner] != GetPVarInt(playerid, "CharID") && !aoverride) {
			SendClientMessage(playerid, X11_TOMATO_2, "The item you're trying to edit is not yours.");
			return 1;
		}
		new editmode = GetPVarInt(playerid, "ItemEditMode");
		switch(editmode) {
			case 1: {
				format(msg, sizeof(msg), "Please enter a price for this item, its current price is $%s", getNumberString(DroppedItems[index][EDIPrice]));
				ShowPlayerDialog(playerid, EItems_SetPrice, DIALOG_STYLE_INPUT, "Item Menu:", msg, "Change", "Cancel");
				SetPVarInt(playerid, "ItemTempIndex", index);
			}
			case 2: {
				SetPVarInt(playerid, "ItemTempIndex", index);
				EditDynamicObject(playerid, objectid);		
			}
			/*
			case 3: {
				format(msg, sizeof(msg), "This item costs $%s, would you really like to buy it?", getNumberString(DroppedItems[index][EDIPrice]));
				ShowPlayerDialog(playerid, EItems_BuyItem, DIALOG_STYLE_MSGBOX, "Item Menu:", msg, "Yes", "No");
				SetPVarInt(playerid, "ItemTempIndex", index);
			}
			*/
			case 4: {
				new Float: X, Float: Y, Float: Z;
				GetDynamicObjectPos(objectid, X, Y, Z);
				if(!IsPlayerInRangeOfPoint(playerid, 3.0, X, Y, Z)) {
					SendClientMessage(playerid, X11_TOMATO_2, "You're not anywhere the item you're trying to pick up so you can't grab it.");
					return 1;
				}
				DeletePVar(playerid, "ItemEditMode");
				if(DroppedItems[index][EDIPrice] > 0) {
					SendClientMessage(playerid, X11_TOMATO_2, "This item is currently for sale and can't be picked up.");
					return 1;
				}
				playerGetDroppedItem(playerid, index);
				CancelEdit(playerid);	
			}
		}
	}
	return 0;	
}
itemsOnPlayerEditObject(playerid, objectid, response, Float:x, Float:y, Float:z, Float:rx, Float:ry, Float:rz) {
	if(response == EDIT_RESPONSE_UPDATE) {
		return 0;
	}
	if(GetPVarType(playerid, "ItemEditMode") != PLAYER_VARTYPE_NONE) {
		new index = findItemByObjID(objectid);
		new editmode = GetPVarInt(playerid, "ItemEditMode");
		if(editmode != 2) { //2 means move
			DeletePVar(playerid, "ItemEditMode");
			CancelEdit(playerid);
		}
		if(index != -1) {
			if(response == EDIT_RESPONSE_CANCEL) {
				GetDynamicObjectPos(objectid, x, y, z);
				GetDynamicObjectRot(objectid, rx, ry, rz);
				SetDynamicObjectPos(objectid, x, y, z);
				SetDynamicObjectRot(objectid, rx, ry, rz);
				DeletePVar(playerid, "ItemEditMode");
				SendClientMessage(playerid, X11_TOMATO_2, "Object Selection cancelled!");
				return 1;
			} 
			SetDynamicObjectPos(objectid, x, y, z);
			SetDynamicObjectRot(objectid, rx, ry, rz);
			SendClientMessage(playerid, COLOR_DARKGREEN, "Position Saved!");
			DroppedItems[index][EDIVW] = GetPlayerVirtualWorld(playerid);
			DroppedItems[index][EDIInt] = GetPlayerInterior(playerid);
			DroppedItems[index][EDIObjPosX] = x;
			DroppedItems[index][EDIObjPosY] = y;
			DroppedItems[index][EDIObjPosZ] = z;
			DroppedItems[index][EDIObjRotX] = rx;
			DroppedItems[index][EDIObjRotY] = ry;
			DroppedItems[index][EDIObjRotZ] = rz;
			
			new itemdesc[128];
			if(DroppedItems[index][EDIPrice] > 0) {
				if(DroppedItems[index][EDITextLabel] != Text3D:0) {
					DestroyDynamic3DTextLabel(DroppedItems[index][EDITextLabel]); //Destroy it and create a new one when we move it, otherwise it won't follow the object
				}
				format(itemdesc, sizeof(itemdesc), "{%s}[{%s}Item{%s}]\n Name: %s\nPrice: %d", getColourString(X11_WHITE),getColourString(COLOR_BRIGHTRED),getColourString(X11_WHITE), DroppedItems[index][EDIType] == EType_Drug ? drugsGetNameFromSQLID(DroppedItems[index][EDIExtraSQLID]) : itemsGetDropType(index), DroppedItems[index][EDIPrice]);
				DroppedItems[index][EDITextLabel] = CreateDynamic3DTextLabel(itemdesc, 0x00FF00AA, x, y, z+0.25, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, DroppedItems[index][EDIVW], DroppedItems[index][EDIInt]);
			}
			format(query, sizeof(query), "UPDATE `droppeditems` SET `x` = %f, `y` = %f, `z` = %f, `rotx` = %f, `roty` = %f, `rotz` = %f, `vw` = %d, `int` = %d WHERE `id` = %d",x,y,z,rx,ry,rz, GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid), DroppedItems[index][EDISQLID]);
			mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
		}
	}
	return 0;
}
setItemPrice(price, index) {
	query[0] = 0;
	DroppedItems[index][EDIPrice] = price;
	if(price > 0) { 
		DroppedItems[index][EDIFlags] |= EItemProp_ForSell;
	} else {
		DroppedItems[index][EDIFlags] &= ~EItemProp_ForSell;
		if(DroppedItems[index][EDITextLabel] != Text3D:0) {
			DestroyDynamic3DTextLabel(DroppedItems[index][EDITextLabel]);
		}
	} 
	new itemdesc[128], Float: X, Float: Y, Float: Z;
	GetDynamicObjectPos(DroppedItems[index][EDIObjectID], X, Y, Z);
	if(DroppedItems[index][EDIPrice] > 0) {
		//Let's just destroy the label and create it again, this will be moving anyways..
		if(DroppedItems[index][EDITextLabel] != Text3D:0) {
			DestroyDynamic3DTextLabel(DroppedItems[index][EDITextLabel]);
		}
		format(itemdesc, sizeof(itemdesc), "{%s}[{%s}Item{%s}]\n Name: %s\n Price: %d", getColourString(X11_WHITE),getColourString(COLOR_BRIGHTRED),getColourString(X11_WHITE), DroppedItems[index][EDIType] == EType_Drug ? drugsGetNameFromSQLID(DroppedItems[index][EDIExtraSQLID]) : itemsGetDropType(index), DroppedItems[index][EDIPrice]);
		DroppedItems[index][EDITextLabel] = CreateDynamic3DTextLabel(itemdesc, 0x00FF00AA, X, Y, Z+0.25, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, DroppedItems[index][EDIVW], DroppedItems[index][EDIInt]);
	}
	format(query, sizeof(query), "UPDATE `droppeditems` SET `price` = %d, `flags` = %d WHERE `id` = %d", DroppedItems[index][EDIPrice], _:DroppedItems[index][EDIFlags], DroppedItems[index][EDISQLID]);
	mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
}
findItemByObjID(objectid) {
	for(new i = 0; i < sizeof(DroppedItems); i++) {
		if(DroppedItems[i][EDISQLID] != 0) {
			if(DroppedItems[i][EDIObjectID] == objectid) {
				#if debug
				printf("findItemByObjID(%d)",i);
				#endif
				return i;
			}
		}
	}
	return -1;
}
loadItemsForPlayer(playerid) {
	query[0] = 0;
	format(query, sizeof(query), "SELECT `id`, `owner`, `x`, `y`, `z`, `rotx`, `roty`, `rotz`, `type`, `amount`, `extra_sqlid`, `vw`, `int`, `flags`,`price` FROM `droppeditems` WHERE `destroyed` = 0 AND `owner` = %d", GetPVarInt(playerid, "CharID"));
	mysql_function_query(g_mysql_handle, query, true, "OnLoadItems", "");
}
forward OnLoadItems();
public OnLoadItems() {
	new rows, fields;
	new id_string[64], itemdesc[128];
	cache_get_data(rows, fields);
	for(new index=0;index<rows;index++) {
		new i = findFreeDroppedItem();
		if(i == -1) {
			printf("Items Array IS FULL!\n");
			continue;
		}
		//if(DroppedItems[i][EDISQLID] != 0) continue; //We'll not be using this for reloading
		cache_get_row(index, 0, id_string); //unused but whatever
		DroppedItems[i][EDISQLID] = strval(id_string);
		
		cache_get_row(index, 1, id_string);
		DroppedItems[i][EDIOwner] = strval(id_string);
		
		cache_get_row(index, 2, id_string);
		DroppedItems[i][EDIObjPosX] = floatstr(id_string);
		
		cache_get_row(index, 3, id_string);
		DroppedItems[i][EDIObjPosY] = floatstr(id_string);
		
		cache_get_row(index, 4, id_string);
		DroppedItems[i][EDIObjPosZ] = floatstr(id_string);
		
		cache_get_row(index, 5, id_string);
		DroppedItems[i][EDIObjRotX] = floatstr(id_string);
		
		cache_get_row(index, 6, id_string);
		DroppedItems[i][EDIObjRotY] = floatstr(id_string);
		
		cache_get_row(index, 7, id_string);
		DroppedItems[i][EDIObjRotZ] = floatstr(id_string);
		
		cache_get_row(index, 8, id_string);
		DroppedItems[i][EDIType] = EDroppedItemType:strval(id_string);
		
		cache_get_row(index, 9, id_string);
		DroppedItems[i][EDIValue] = strval(id_string);

		cache_get_row(index, 10, id_string);
		DroppedItems[i][EDIExtraSQLID] = strval(id_string);
		
		cache_get_row(index, 11, id_string);
		DroppedItems[i][EDIVW] = strval(id_string);

		cache_get_row(index, 12, id_string);
		DroppedItems[i][EDIInt] = strval(id_string);

		cache_get_row(index, 13, id_string);
		DroppedItems[i][EDIFlags] = ItemFlags:strval(id_string);

		cache_get_row(index, 14, id_string);
		DroppedItems[i][EDIPrice] = strval(id_string);

		new weapon, ammo;
		if(DroppedItems[i][EDIType] == EType_Gun) {
			decodeWeapon(DroppedItems[i][EDIValue], weapon, ammo);
			if(DroppedItems[i][EDIObjRotX] == 0) {
				DroppedItems[i][EDIObjRotX] = 80.0; //Meh, correct errors like this on startup
			}
		}
		new model = getDroppedTypeModel(DroppedItems[i][EDIType],weapon);

		DroppedItems[i][EDIObjectID] = CreateDynamicObject(model, DroppedItems[i][EDIObjPosX], DroppedItems[i][EDIObjPosY], DroppedItems[i][EDIObjPosZ], DroppedItems[i][EDIObjRotX], DroppedItems[i][EDIObjRotY], DroppedItems[i][EDIObjRotZ], DroppedItems[i][EDIVW], DroppedItems[i][EDIInt]);
		
		if(DroppedItems[i][EDIPrice] > 0) {
			format(itemdesc, sizeof(itemdesc), "{%s}[{%s}Item{%s}]\n Name: %s\n Price: %d", getColourString(X11_WHITE),getColourString(COLOR_BRIGHTRED),getColourString(X11_WHITE), DroppedItems[i][EDIType] == EType_Drug ? drugsGetNameFromSQLID(DroppedItems[i][EDIExtraSQLID]) : itemsGetDropType(i), DroppedItems[i][EDIPrice]);
			DroppedItems[i][EDITextLabel] = CreateDynamic3DTextLabel(itemdesc, 0x00FF00AA, DroppedItems[i][EDIObjPosX], DroppedItems[i][EDIObjPosY], DroppedItems[i][EDIObjPosZ]+0.25, 5.0, INVALID_PLAYER_ID, INVALID_VEHICLE_ID, 0, DroppedItems[i][EDIVW], DroppedItems[i][EDIInt]);
		}	
	}
	return 1;
}
reloadItemsForPlayer(playerid) {
	for(new i=0; i<sizeof(DroppedItems); i++) {
		if(DroppedItems[i][EDIOwner] == GetPVarInt(playerid, "CharID")) {
			destroyDroppedItem(i, 0); //Destroy the item but don't set it to destroyed in the db
		}
	}
	loadItemsForPlayer(playerid);
}
itemsOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	DeletePVar(playerid, "ItemTempIndex");
	DeletePVar(playerid, "ItemEditMode");
	for(new i=0; i<sizeof(DroppedItems); i++) {
		if(DroppedItems[i][EDIOwner] == GetPVarInt(playerid, "CharID")) {
			destroyDroppedItem(i, 0); //Destroy the item but don't set it to destroyed in the db
		}
	}
}
playerGetDroppedItem(playerid, item) {
	new msg[256];
	new index = getDropTypeIndex(DroppedItems[item][EDIType]);
	if(DroppedItems[item][EDIType] == EType_Gun) {
		new gun, ammo;
		decodeWeapon(DroppedItems[item][EDIValue], gun, ammo);
		if(IsPlayerHoldingWeaponInSlot(playerid, GetWeaponSlot(gun))) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are currently holding a weapon in the same slot as this guns!");
			return 1;
		}		
		//GivePlayerWeaponEx(playerid, gun, ammo);
		if(GetPVarInt(playerid, "Level") < MINIMUM_GUN_LEVEL) {
			SendClientMessage(playerid, X11_TOMATO_2, "You are not allowed to get any guns until you reach level "#MINIMUM_GUN_LEVEL".");
			return 1;
		} else {
			GivePlayerWeaponEx(playerid, gun, ammo);
		}
		new gunname[32];
		GetWeaponNameEx(gun, gunname, sizeof(gunname));
		format(msg, sizeof(msg), "* %s picks up a %s",GetPlayerNameEx(playerid, ENameType_RPName),gunname);
		ProxMessage(5.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
		format(msg, sizeof(msg), "%s (Char ID: %d), picked up a %s with SQL ID %d, owned by Char ID %d, VW: %d, Int: %d", GetPlayerNameEx(playerid, ENameType_RPName), GetPVarInt(playerid, "CharID"), gunname, DroppedItems[item][EDISQLID], DroppedItems[item][EDIOwner], GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
		itemLog(msg);
		destroyDroppedItem(item);
		return 1;
	} else if(DroppedItems[item][EDIType] == EType_Armour) {
		new Float:armor;
		GetPlayerArmourEx(playerid, armor);
		armor += float(DroppedItems[item][EDIValue]);
		if(armor > MAX_ARMOUR) armor = MAX_ARMOUR;
		SetPlayerArmourEx(playerid, armor);
	} else if(DroppedItems[item][EDIType] == EType_Drug) {
		new pvarid = getUsableDrugSpot(playerid, DroppedItems[item][EDIExtraSQLID], DrugPVarStates:2); //STATE_ANYTHING
		if(pvarid != -1) {
			setDrugPVar(playerid, getDrugIDBySQLID(DroppedItems[item][EDIExtraSQLID]), pvarid, DroppedItems[item][EDIValue], 1); //EDrugs_Type_Increase
			format(msg, sizeof(msg), "* %s picks up some %s",GetPlayerNameEx(playerid, ENameType_RPName),drugsGetNameFromSQLID(DroppedItems[item][EDIExtraSQLID]));
			ProxMessage(5.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
			format(msg, sizeof(msg), "%s (Char ID: %d), picked up some %s with SQL ID %d, owned by Char ID %d, VW: %d, Int: %d", GetPlayerNameEx(playerid, ENameType_RPName), GetPVarInt(playerid, "CharID"), drugsGetNameFromSQLID(DroppedItems[item][EDIExtraSQLID]), DroppedItems[item][EDISQLID], DroppedItems[item][EDIOwner], GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
			itemLog(msg);
			destroyDroppedItem(item);
		}
		return 1;
	} else {
		new total = GetPVarInt(playerid, DroppableItems[index][EDropPVarName]);
		total += DroppedItems[item][EDIValue];
		SetPVarInt(playerid, DroppableItems[index][EDropPVarName], total);
	}
	format(msg, sizeof(msg), "* %s picks up some %s",GetPlayerNameEx(playerid, ENameType_RPName),DroppableItems[index][EDropName]);
	ProxMessage(5.0, playerid, msg, COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE,COLOR_PURPLE);
	GiveMoneyEx(playerid, 0); //sync money
	format(msg, sizeof(msg), "%s (Char ID: %d), picked up a %s with SQL ID %d, owned by Char ID %d, VW: %d, Int: %d", GetPlayerNameEx(playerid, ENameType_RPName), GetPVarInt(playerid, "CharID"), DroppableItems[index][EDropName], DroppedItems[item][EDISQLID], DroppedItems[item][EDIOwner], GetPlayerVirtualWorld(playerid), GetPlayerInterior(playerid));
	itemLog(msg);
	destroyDroppedItem(item);
	return 1;
}
destroyDroppedItem(item, setdestroyed = 1) {
	query[0] = 0;
	DestroyDynamicObject(DroppedItems[item][EDIObjectID]);
	if(DroppedItems[item][EDITextLabel] != Text3D:0) {
		DestroyDynamic3DTextLabel(DroppedItems[item][EDITextLabel]);
	}
	if(setdestroyed == 1) {
		format(query, sizeof(query), "UPDATE `droppeditems` SET `destroyed` = 1 WHERE `id` = %d", DroppedItems[item][EDISQLID]);
		mysql_function_query(g_mysql_handle, query, true, "EmptyCallback","");
	}
	DroppedItems[item][EDIObjectID] = 0;
	DroppedItems[item][EDISQLID] = 0;
	DroppedItems[item][EDIType] = EType_None;
	DroppedItems[item][EDIValue] = 0;
}
getStandingDroppedItem(playerid, Float:radi = 1.5) {
	new Float:X, Float:Y, Float:Z;
	new interior, vw;
	vw = GetPlayerVirtualWorld(playerid);
	interior = GetPlayerInterior(playerid);
	for(new i=0;i<sizeof(DroppedItems);i++) {
		if(DroppedItems[i][EDIObjectID] != 0 && DroppedItems[i][EDISQLID] != 0) {
			GetDynamicObjectPos(DroppedItems[i][EDIObjectID], X, Y, Z);
			if(interior == DroppedItems[i][EDIInt] && vw == DroppedItems[i][EDIVW]) {
				if(IsPlayerInRangeOfPoint(playerid, radi, X, Y, Z)) {
					return i;
				}
			}
		}
	}
	return -1;
}
findFreeDroppedItem() {
	for(new i=0;i<sizeof(DroppedItems);i++) {
		if(DroppedItems[i][EDIObjectID] == 0) {
			return i;
		}
	}
	return -1;
}
getDroppedTypeModel(EDroppedItemType:type, weapon = 0) {
	if(type == EType_Gun) {
		return gh_GetObjectID(weapon);
	}
	for(new i=0;i<sizeof(DroppableItems);i++) {
		if(DroppableItems[i][EDropType] == type) {
			return DroppableItems[i][EDropModelID];
		}
	}
	return -1;
}
getDropTypeIndex(EDroppedItemType:type) {
	for(new i=0;i<sizeof(DroppableItems);i++) {
		if(DroppableItems[i][EDropType] == type) {
			return i;
		}
	}
	return -1;
}
dropItem(dropper, EDroppedItemType:type, amount, Float:X, Float:Y, Float:Z, interior, vw, extra_sqlid = 0) {
	new index = findFreeDroppedItem();
	if(index == -1) return index;
	query[0] = 0;
	//format(query, sizeof(query), "INSERT INTO `droppeditems` SET `x` = %f, `y` = %f, `z` = %f, `rotx` = %f, `roty` = %f, `rotz` = %f, `type` = %d, `extra_sqlid` = %d `vw` = %d, `int` = %d",X,Y,Z,objidx,houseid,inside);
	format(query, sizeof(query), "INSERT INTO `droppeditems` SET `owner` = %d, `x` = %f, `y` = %f, `z` = %f, `type` = %d, `amount` = %d, `extra_sqlid` = %d, `vw` = %d, `int` = %d",GetPVarInt(dropper, "CharID"), X,Y,Z,_:type,amount,extra_sqlid,vw,interior);
	mysql_function_query(g_mysql_handle, query, true, "OnDropItem", "ddddfffddd", dropper, index, _:type, amount, X, Y, Z, interior, vw, extra_sqlid);
	return index;
}
forward OnDropItem(dropper, index, EDroppedItemType:type, amount, Float:X,Float:Y,Float:Z,interior,vw, extra_sqlid);
public OnDropItem(dropper, index, EDroppedItemType:type, amount, Float:X,Float:Y,Float:Z,interior,vw, extra_sqlid) {
	new sqlid = mysql_insert_id();
	DroppedItems[index][EDISQLID] = sqlid;
	DroppedItems[index][EDIOwner] = GetPVarInt(dropper, "CharID");
	DroppedItems[index][EDIExtraSQLID] = extra_sqlid; //For drugs and other things that need to be related somehow
	DroppedItems[index][EDIType] = type;
	DroppedItems[index][EDIObjPosX] = X;
	DroppedItems[index][EDIObjPosY] = Y;
	DroppedItems[index][EDIObjPosZ] = Z;
	DroppedItems[index][EDIInt] = interior;
	DroppedItems[index][EDIVW] = vw;
	DroppedItems[index][EDIFlags] = ItemFlags:EItemProp_None;

	new weapon = 0;
	new Float:rotationxgun = 0.0;
	if(type == EType_Gun) {
		new ammo;
		decodeWeapon(amount, weapon, ammo);
		rotationxgun = 80.0;
		DroppedItems[index][EDIObjRotX] = rotationxgun;
	}
	new Float:armor;
	GetPlayerArmourEx(dropper, armor);
	new model = getDroppedTypeModel(type,weapon);
	DroppedItems[index][EDIObjectID] = CreateDynamicObject(model, X, Y, Z,rotationxgun,0.0,0.0,vw,interior);
	if(type == EType_Armour) { 
		amount = floatround(armor);
		SetPlayerArmourEx(dropper, 0.0);
	}
	DroppedItems[index][EDIValue] = amount;
	DroppedItems[index][EDIType] = type;
	new name[MAX_PLAYER_NAME];
	format(name, sizeof(name), "%s",GetPlayerNameEx(dropper, ENameType_CharName));
	strmid(DroppedItems[index][eDropperName], name, 0, strlen(name), MAX_PLAYER_NAME);
	ShowScriptMessage(dropper, "You've dropped an ~r~item~w~, use ~r~\"/edititem\" ~w~to edit its position.");
	return index;
}