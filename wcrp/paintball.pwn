#define WIN_POINTS 15

enum EPaintballSpawns {
	Float:EPaintballX,
	Float:EPaintballY,
	Float:EPaintballZ,
	Float:EPaintballAngle,
};
new Float:PaintballSpawns[][EPaintballSpawns] = {
	{-975.90, 1089.71, 1344.96, 89.82},
	{-1018.54, 1020.92, 1343.48, 43.76},
	{-1030.92, 1079.13, 1343.13, 158.42},
	{-1067.22, 1092.64, 1343.19, 93.24},
	{-1045.37, 1055.72, 1343.38, 78.54},
	{-1066.02, 1029.89, 1343.14, 54.44},
	{-1130.73, 1029.29, 1345.72, 268.11},
	{-1090.07, 1048.77, 1343.53, 320.75},
	{-1112.01, 1094.29, 1341.85, 222.67},
	{-1053.96, 1059.80, 1341.35, 197.29}
};

enum {
	EPaintball_EquipDialog = EPaintball_Base + 1,
};


new PaintballWeapons[] = {24,27,31,34,29,22,23};


new PlayerText:paintballText[MAX_PLAYERS];

paintballGetLeader() {
	new highest,highestid = -1;
	foreach(Player, i) {
		if(GetPVarType(i, "PaintballPoints") != PLAYER_VARTYPE_NONE) {
			if(GetPVarInt(i, "PaintballPoints") > highest) {
				highest = GetPVarInt(i, "PaintballPoints");
				highestid = i;
			}
		}
	}
	return highestid;
}
stock getPaintballDrawText(playerid) {
	new ret[128];
	new leader = paintballGetLeader();
	format(ret, sizeof(ret), "~r~Kills: ~g~%d~n~~r~Deaths: ~g~%d~n~~n~~r~Leader: %s",GetPVarInt(playerid, "PaintballPoints"),GetPVarInt(playerid, "PaintballDeaths"),GetPlayerNameEx(leader, ENameType_RPName_NoMask));
	return ret;
}

putPlayerInPaintball(playerid) {
	if(IsOnDuty(playerid) || isInPaintball(playerid)) {
		SendClientMessage(playerid, X11_TOMATO_2, "You can't do this on duty!");
		return 1;
	}
	SetPVarInt(playerid, "PaintballPoints",0);
	SetPVarInt(playerid, "PaintballDeaths",-1);
	paintballRespawn(playerid);
	paintballText[playerid] = CreatePlayerTextDraw(playerid, 550.0, 168.0, getPaintballDrawText(playerid));
	PlayerTextDrawUseBox(playerid, paintballText[playerid], 0);
	PlayerTextDrawFont(playerid, paintballText[playerid], 1);
	PlayerTextDrawSetOutline(playerid, paintballText[playerid], 0);
	PlayerTextDrawSetShadow(playerid, paintballText[playerid], 0);
	PlayerTextDrawAlignment(playerid, paintballText[playerid], 3);
    PlayerTextDrawBackgroundColor(playerid, paintballText[playerid],0x000000FF);
	PlayerTextDrawColor(playerid, paintballText[playerid],0xFFFFFFFF);
	PlayerTextDrawShow(playerid, paintballText[playerid]);
	saveSQLGuns(playerid);
	saveGuns(playerid, "PaintballGuns");
	new Float:HP, Float:armour;
	GetPlayerHealth(playerid, HP);
	GetPlayerArmourEx(playerid, armour);
	SetPVarFloat(playerid, "PaintBallHP", HP);
	SetPVarFloat(playerid, "PaintBallArmour", armour);
	SendClientMessage(playerid, COLOR_DARKGREEN, "(( Do /quitpaintball to exit paintball. ))");
	return 0;
}
paintballAwardPoint(playerid) {
	new points = GetPVarInt(playerid, "PaintballPoints");
	points++;
	SetPVarInt(playerid, "PaintballPoints",points);
	PlayerTextDrawSetString(playerid, paintballText[playerid],getPaintballDrawText(playerid));
	if(points >= WIN_POINTS) {
		foreach(Player, i) {
			if(isInPaintball(i)) {
				OnExitPaintball(i, 1, playerid);
			}
		}
	}
}
showPaintballMenu(playerid) {
	new temptxt[256];
	dialogstr[0] = 0;
	new gunname[32];
	for(new i=0;i<sizeof(PaintballWeapons);i++) {
		GetWeaponNameEx(PaintballWeapons[i],gunname,sizeof(gunname));
		format(temptxt, sizeof(temptxt), "%s\n",gunname);
		strcat(dialogstr,temptxt,sizeof(dialogstr));
	}
	ShowPlayerDialog(playerid, EPaintball_EquipDialog, DIALOG_STYLE_LIST, "{00BFFF}Paintball Menu", dialogstr, "Ok", "Cancel");
}

paintballOnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) {
	#pragma unused inputtext
	switch(dialogid) {
		case EPaintball_EquipDialog: {
			if(!response) {
				return 1;
			}
			if(!isInPaintball(playerid)) {
				return 1;
			}
			GivePlayerWeaponEx(playerid, PaintballWeapons[listitem], -1);
		}
	}
	return 1;
}
YCMD:quitpaintball(playerid, params[], help) {
	if(help) {
		SendClientMessage(playerid, X11_WHITE, "Quits paintball");
		return 1;
	}
	if(GetPVarType(playerid,"PaintballPoints") == PLAYER_VARTYPE_NONE) {
		SendClientMessage(playerid, X11_TOMATO_2, "You aren't in paintball");
		return 1;
	}
	OnExitPaintball(playerid);
	return 1;
}
paintballRespawn(playerid) {
	new index = RandomEx(0, sizeof(PaintballSpawns));
	SetPlayerPos(playerid, PaintballSpawns[index][EPaintballX], PaintballSpawns[index][EPaintballY], PaintballSpawns[index][EPaintballZ]);
	SetPlayerInterior(playerid, 10);
	SetPlayerVirtualWorld(playerid, 10);
	showPaintballMenu(playerid);
	new deaths = GetPVarInt(playerid, "PaintballDeaths");
	PlayerTextDrawSetString(playerid, paintballText[playerid],getPaintballDrawText(playerid));
	deaths++;
	SetPVarInt(playerid, "PaintballDeaths", deaths);
}
isInPaintball(playerid) {
	if(GetPVarType(playerid, "PaintballPoints") != PLAYER_VARTYPE_NONE) {
		return 1;
	} else {
		return 0;
	}
}
OnExitPaintball(playerid, roundover = 0, winner = -1) {
	new points = GetPVarInt(playerid, "PaintballPoints");
	new deaths = GetPVarInt(playerid, "PaintballDeaths");
	new msg[128];
	format(msg, sizeof(msg), "* Paintball Score: %d, Deaths: %d", points, deaths);
	SendClientMessage(playerid, X11_ORANGE, msg);
	if(roundover == 1 && winner != -1) {
		format(msg, sizeof(msg), "* Round over, Winner: %s, Points: %d",GetPlayerNameEx(winner, ENameType_RPName_NoMask), GetPVarInt(winner, "PaintballPoints"));
		SendClientMessage(playerid, X11_ORANGE, msg);
	} else if(roundover == 1) {
		SendClientMessage(playerid, X11_ORANGE, "* Round over");
	}
	SetPlayerPos(playerid, 1310.033081, -1367.739013, 13.539746);
	SetPlayerInterior(playerid, 0);
	SetPlayerVirtualWorld(playerid, 0);
	DeletePVar(playerid, "PaintballPoints");
	DeletePVar(playerid, "PaintballDeaths");
	PlayerTextDrawHide(playerid, paintballText[playerid]);
	PlayerTextDrawDestroy(playerid, paintballText[playerid]);
	restoreGuns(playerid, "PaintballGuns");
	SetPlayerHealthEx(playerid, GetPVarFloat(playerid, "PaintBallHP"));
	SetPlayerArmourEx(playerid, GetPVarFloat(playerid, "PaintBallArmour"));
	DeletePVar(playerid, "PaintBallHP");
	DeletePVar(playerid, "PaintBallArmour");
	
	DeletePVar(playerid, "XPaintBall");
	DeletePVar(playerid, "YPaintBall");
	DeletePVar(playerid, "ZPaintBall");
	DeletePVar(playerid, "VWPaintBall");
	DeletePVar(playerid, "IntPaintBall");
}
paintballOnPlayerDisconnect(playerid, reason) {
	#pragma unused reason
	if(isInPaintball(playerid)) {
		OnExitPaintball(playerid);
	}
}
paintballOnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid) {
	#pragma unused newinteriorid
	#pragma unused oldinteriorid
	#pragma unused playerid
	/*
	if(isInPaintball(playerid) && oldinteriorid != 0)
		OnExitPaintball(playerid);
	*/
}